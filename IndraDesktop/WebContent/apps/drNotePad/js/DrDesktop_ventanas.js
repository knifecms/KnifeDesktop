var conDwrConnectionTimeout=5000;


/*OBJETO GLOBAL drWindow (pertenece al objeto window que cargue este js)*/

var drWindow={
   /*(+)*/    getDesktopWindow  :public_getDesktopWindow
   /*(+)*/  , getStructuralWindow  :public_getStructuralWindow   
   /*(+)*/  , getXDesktopObj  :public_getXDesktopObj
   /*(+)*/  , getXDesktopWindowObj  :public_getXDesktopWindowObj
   /*(+)*/  , getDesktopConfig  :public_getDesktopConfig
   /*(-)*/  , setDesktopConfigValue  :private_setDesktopConfigValue
   /*(-)*/  , getDesktopConfigValue  :private_getDesktopConfigValue
   /*(+)*/  , getEnginesConfig  :public_getEnginesConfig
   /*(+)*/  , getFileTypesConfig  :public_getFileTypesConfig
   /*(+)*/  , getApplicationsConfig  :public_getApplicationsConfig
   /*(+)*/  , getApplicationsGroupsConfig  :public_getApplicationsGroupsConfig   
   /*(+)*/ /* , getSkinGroupsConfig  :public_getSkinGroupsConfig  */
   /*(+)*/  , getSkins  :public_getSkins  
   /*(+)*/  , getApplicationIds: public_getApplicationIds
   /*(+)*/  , getTopMargin  :public_getTopMargin
   /*(+)*/  , getBottomMargin  :public_getBottomMargin
   /*(+)*/  , getClientWidth  :public_getClientWidth
   /*(+)*/  , getClientHeight  :public_getClientHeight
   /*(+)*/  , getId  :public_getId
   /*(+)*/  , getWindowIds  :public_getWindowIds
   /*(+)*/  , getWindowData  :public_getWindowData
   /*(+)*/  , getNativeWindow  :public_getNativeWindow
   /*(+)*/  , getNativeIframe  :public_getNativeIframe
   /*(+)*/  , getRegistry  :public_getRegistry
   /*(+)*/  , getDaemonRegistry  :public_getDaemonRegistry
   /*(+)*/  , getSkin  :public_getSkin
   /*(+)*/  , setSkin  :public_setSkin
   /*(+)*/  , arrangeAllWindows  :public_arrangeAllWindows
   /*(+)*/  , generateId  :public_generateId
   /*(+)*/  , open  :public_open
   /*(+)*/  , openApplication  :public_openApplication
   /*(+)*/  , showHelp  :public_showHelp
   /*(+)*/  , openWindowArray  :public_openWindowArray
   /*(+)*/  , reload  :public_reload
   /*(+)*/  , close  :public_close  
   /*(+)*/  , setTitle  :public_setTitle
   /*(+)*/  , setTop  :public_setTop
   /*(+)*/  , setLeft  :public_setLeft
   /*(+)*/  , setWidth  :public_setWidth
   /*(+)*/  , setHeight  :public_setHeight
   /*(+)*/  , setStatus  :public_setStatus
   /*(+)*/  , showMessage  :public_showMessage
   /*(+)*/  , showConfirm  :public_showConfirm
   /*(+)*/  , getStatus  :public_getStatus
   /*(+)*/  , getWidth  :public_getWidth
   /*(+)*/  , getHeight  :public_getHeight
   /*(+)*/  , getTop  :public_getTop
   /*(+)*/  , getLeft  :public_getLeft
   /*(+)*/  , resizeTo  :public_resizeTo
   /*(+)*/  , moveTo  :public_moveTo
   /*(+)*/  , moveResizeTo  :public_moveResizeTo
   /*(+)*/  , createDaemon  :public_createDaemon
   /*(+)*/  , killDaemons  :public_killDaemons
   /*(+)*/  , sessionSaveAndRestart :public_sessionSaveAndRestart
   /*(+)*/  , sessionRestart :public_sessionRestart
   /*(+)*/  , getPublicMethods: public_getPublicMethods
   /*(+)*/  , toFront: public_toFront
   /*(+)*/  , toBack: public_toBack
   /*(+)*/  , bindDesktopResize: public_bindDesktopResize
   /*(+)*/  , getUserData: public_getUserData
   /*(+)*/  , getCookie: public_getCookie
   /*(+)*/  , setCookie: public_setCookie
   /*(+)*/  , deleteCookie: public_deleteCookie
   /*(+)*/  , openFile: public_openFile
   /*(+)*/  , getSkinParameters: public_getSkinParameters
   /*(+)*/  , getTaskbarData: public_getTaskbarData
   /*(+)*/  , setTaskbarData: public_setTaskbarData
   /*(+)*/  , show: public_show
   /*(+)*/  , hide: public_hide
   /*(+)*/  , changeVirtualDesktop: public_changeVirtualDesktop
   /*(+)*/  , getVirtualDesktopsData: public_getVirtualDesktopsData
   /*(+)*/  , getCurrentVirtualDesktop: public_getCurrentVirtualDesktop
   /*(+)*/  , setVirtualDesktop: public_setVirtualDesktop
   /*(+)*/  , getVirtualDesktop: public_getVirtualDesktop
   /*(-)*/  , inVirtualDesktop: private_inVirtualDesktop
   /*(+)*/  , getSkinData: public_getSkinData
   /*(+)*/  , setSkinData: public_setSkinData
   /*(+)*/  , getInnerWidth: public_getInnerWidth
   /*(+)*/  , getInnerHeight: public_getInnerHeight
   /*(+)*/  , setNoDrag: public_setNoDrag
   /*(+)*/  , getNoDrag: public_getNoDrag
   /*(+)*/  , setNoResize: public_setNoResize
   /*(+)*/  , getNoResize: public_getNoResize
   /*(+)*/  , setGlobalNoDrag: public_setGlobalNoDrag
   /*(+)*/  , getGlobalNoDrag: public_getGlobalNoDrag
   /*(+)*/  , setGlobalNoResize: public_setGlobalNoResize
   /*(+)*/  , getGlobalNoResize: public_getGlobalNoResize
   /*(+)*/  , setGlobalNoClose: public_setGlobalNoClose
   /*(+)*/  , getGlobalNoClose: public_getGlobalNoClose
   /*(+)*/  , setGlobalNoContextMenu: public_setGlobalNoContextMenu
   /*(+)*/  , getGlobalNoContextMenu: public_getGlobalNoContextMenu
   /*(-)*/  , setGlobalBoolean: private_setGlobalBoolean
   /*(-)*/  , getGlobalBoolean: private_getGlobalBoolean
   /*(-)*/  , refreshWindowObjectProperties: private_refreshWindowObjectProperties
   /*(-)*/  , getApplicationsWithHelp:private_getApplicationsWithHelp
   /*(-)*/  , hasHelp: private_hasHelp
   /*(+)*/  , getDesktopProperties: public_getDesktopProperties  
   /*(-)*/  , initConfig: private_initConfig
   /*(+)*/  , focus: public_focus
   /*(+)*/  , blur: public_blur
   /*(+)*/  , hasFocus: public_hasFocus
   /*(+)*/  , getWindowExist: public_getWindowExist
   /*(+)*/  , getSkinMeta: public_getSkinMeta
   /*(-)*/  , prepareSkinParameterChange: private_prepareSkinParameterChange
   /*(-)*/  , getSkinParameterChanges: private_getSkinParameterChanges
   /*(+)*/  , getSkinUrl: public_getSkinUrl
   /*(-)*/  , getMenuObject: private_getMenuObject
   /*(-)*/  , setMenuObject: private_setMenuObject
            , getApplicationExists: public_getApplicationExists
            , alert: public_alert
            , setStatusAllWindows: public_setStatusAllWindows
   }


function public_getPublicMethods() {  //Para que se pueda llamar desde consola y la consola sepa que parametros tiene cada m�todo y cuales hacer p�blicos ...
   var subCommands={
      createDaemon:{args:[
           {name:"id", info:"El id de la ventana"}
          ,{name:"milliseconds", info: "Milisegundos entre llamada y llamada"}
          ,{name:"script", info:"M�todo JavaScript a invocar si lo hay. Si no lo hay se recarga el contenido de la ventana"}]
         ,info:"Crea un proceso autom�tico (daemon) que actualiza una ventana o ejecuta una funci�n Javascript en esa ventana cada cierto intervalo de tiempo."}
      ,killDaemons:{args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Elimina todos los procesos autom�ticos (daemons) asociados a una cierta ventana."}
      ,getDesktopWindow:{args:[], info:"Retorna la ventana en la que reside el escritorio.",  complexResult:true} /* complexResult:true sirve para que no se convierta a json (da problemas con atributos recursivos) */
      ,getStructuralWindow:{args:[], complexResult:true, info:"Retorna el objeto window dentro de cuyo frameset reside el objeto window del escritorio."}
      ,getXDesktopObj:{args:[], complexResult:true, info:"Retorna el objeto xDT de xDesktop."}
      ,getXDesktopWindowObj:{args:[], complexResult:true, info:"Retorna el objeto xDTwin de xDesktop."}
      ,getDesktopConfig:{args:[], info:"Retorna el objeto de configuraci�n del desktop."}
      ,getEnginesConfig:{args:[], info:"Retorna el objeto de configuraci�n de los engines."}
      ,getApplicationIds:{args:[], info:"Retorna los identificadores de las aplicaciones registradas."}
      ,getApplicationsConfig:{args:[], info:"Retorna el objeto de configuraci�n de las aplicaciones registradas."}
      ,getApplicationsGroupsConfig:{args:[], info:"Retorna los datos almacenados sobre los grupos de aplicaciones existentes."}
      ,getTopMargin:{args:[], info:"Retorna el margen superior (depende del skin)."}
      ,getBottomMargin:{args:[], info:"Retorna el margen inferior (depende del skin)."}
      ,getClientWidth:{args:[], info:"Retorna el ancho de la pantalla."}
      ,getClientHeight:{args:[], info:"Retorna el alto de la pantalla. Tiene en cuenta los margenes superior e inferior del escritorio."}
      ,getId:{args:[], info:"Si es llamado desde un html que ha sido cargado dentro de una ventana del escritorio retorna el id de la ventana."}
      ,getWindowIds:{args:[{name:"noWidgets",info:""},{name:"onlyCurrentDesktop",info:""}], info:"Retorna un array con los id de todas las ventanas que existen en el escritorio."}
      ,getWindowData:{args:[{name:"id", info:"Identificador de la ventana (opcional). Si no se especifica se retorna un array con los datos de todas las ventanas abiertas."}], info:"Retorna informaci�n actualizada sobre todas las ventanas abiertas."}
      ,getNativeWindow:{args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], complexResult:true, info:"Retorna el objeto Window contenido dentro de la ventana con ese id"}
      ,getNativeIframe:{args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], complexResult:true, info:"Retorna el objeto iframe estructural de la ventana con ese id."}
      ,getRegistry:{args:[],info:"Retorna el objeto interno de control de las ventanas."}
      ,getDaemonRegistry:{args:[], info:"Retorna el objeto registro de los procesos Daemon existentes."}
      ,getSkin:{args:[], info:"Retorna el codigo del skin actual."}
      ,setSkin:{args:[{name:"skin", info:"El identificador del nuevo SKIN. Por ejemplo, XP, OSXA, NPS, EST, etc. Para que el cambio sea efectivo hay que llamar a drWindow.sessionSaveAndRestart()."}], info:"Establece el skin. Para que el cambio sea efectivo hay que llamar a drWindow.sessionSaveAndRestart()."}
      ,getSkins:{args:[], info:"Retorna un objeto con los skins registrados y el valor de sus par�metros de configuraci�n."}
      ,arrangeAllWindows:{args:[{name: "startX", info:""},{name: "startY", info:""},{name: "offsetX", info:""},{name: "offsetY", info:""}], info:"Coloca en orden todas las ventanas (a la izquierda en cascada)"}
      ,generateId:{args:[], info:"Genera un id aleatorio que no exista para ninguna ventana. No suele ser necesario llamar a este m�todo directamente ya que el m�todo drWindow.open() ya genera un id aleatorio para la nueva ventana si no le pasamos ninguno expl�citamente."}
      ,open:{args:[
          {name: "id", info:"Id que representar� a la ventana. Si se establece un id la ventana ser� �nica (al abrir otra vez con el mismo id la ventana actual se recargar� con el nuevo contenido y no aparecer� una ventana nueva). Si no se establece un id entonces es conveniente guardar en una variable el id de la ventana generada que es retornado por el m�todo drWindow.open."}
         ,{name: "url", info:"Es la URL del contenido de la ventana."}
         ,{name: "title", info:"Titulo de la ventana"}
         ,{name: "position", info:"Una cadena de caracteres que describe la posici�n de la ventana. Puede ser 'x,y','center','center,y','x,center','n','s','e','w','ne','nw','se','sw', 'idventana:T|B|L|R:Offset' (si quieres colocarla relativa a otra ventana), 'rXX,YY' (por ejemplo r10,5 la coloca pegada a la derecha 10 pixeles y en el y 5), 'XX,bYY' (por ejemplo 5,b10 la coloca en el x 5 y pegada abajo 10 p�xeles) o 'rXX,bYY'"}
         ,{name: "testigo", info:"objeto javascript a pasar al abrir la ventana a la funci�n de recogida de testigo existente en el HTML contenido en la ventana si dicha funci�n existe. El paso de testigo es una funcionalidad utilizada para recuperar el estado de las ventanas automaticamente al volver a iniciar la sesi�n de un usuario. Si al cerrar una ventana existe una cierta funci�n javascript de paso de testigo entonces la funci�n es llamada y el testigo almacenado para cuando se recupere la sesi�n. El paso de testigo se explica en otro apartado de la documentaci�n."}
         ,{name: "testigoJson", info:"Igual que el par�metro 'testigo' salvo porque este par�metro no es un objeto cualquiera javascript si no una cadena de caracteres que debe ser evaluada cono objeto json antes de pasarselo a la funci�n de recogida de testigo. "}
         ,{name: "helpUrl", info:"URL a abrir cuando se pulse en el bot�n de ayuda existente en la ventana."}
         ,{name: "idPadre", info:"Id de la ventana que se va  aconsiderar padre de esta (opcional)."}
         ,{name: "status", info:"puede ser 'maximized' (la ventana se crea maximizada) o 'minimized' (aparece minimizada)"}
         ,{name: "widget", info:"Si es 'true' entonces la ventana es un widget (no tiene marco, el fondo es transparente y activa noTaskbar=true y noHide=true)."}
         ,{name: "noTaskbar", info:"Si es 'true' entonces la ventana no aparece en la barra de tareas"}
         ,{name: "sessionSave", info:"Si es 'false' entonces la ventana no se salvar� en la sesi�n."}
         ,{name: "noHide", info:"Si es 'true' entonces el contenido de la ventana no se oculta cuando se mueve o se cambia de tama�o otra ventana."}
         ,{name: "virtualDesktop", info:"Identificador del desktop virtual en que se abre la ventana. Por defecto es el desktop activo."}
         ,{name: "showInAllDesktops", info:"Si es 'true' entonces la ventana o widget aparece siempre en todos los desktops."}
         ,{name: "width", info:"Ancho de la ventana interna en pixeles incluyendo el borde exterior. Esta propiedad se ignora si existe 'innerWidth' y produce tama�os de ventana iguales independientemente del Skin."}
         ,{name: "height", info:"Alto de la ventana interna en pixeles incluyendo el borde exterior. Esta propiedad se ignora si existe 'innerHeight' y produce tama�os de ventana iguales independientemente del Skin."}
         ,{name: "innerWidth", info:"Ancho de la ventana interna (iframe) en pixeles sin incluir el borde exterior. Esta propiedad siempre tiene preferencia sobre 'width' y produce tama�os de ventana distintos segun el Skin."}
         ,{name: "innerHeight", info:"Alto de la ventana interna (iframe) en pixeles sin incluir el borde exterior. Esta propiedad siempre tiene preferencia sobre 'height' y produce tama�os de ventana distintos segun el Skin."}
         ,{name: "noResize", info:"Si es 'true' entonces la ventana no puede cambiar su tama�o arrastrando aunque si puede maximizarse o minimizarse."}
         ,{name: "noDrag", info:"Si es 'true' entonces la ventana no puede puede ser arrastrada y quedar� fijada en la posici�n que tenga."}
         ,{name: "parameter", info:"Si su valor es &quot;AAA = BBB&quot; entonces envia un par�metro AAA con valor BBB por el m�todo 'get'. Se pueden definir tantos &quot;-parameter&quot; como se necesite."}
         ]
         , info:"Abre una nueva ventana o recarga una ventana existente. Es necesario que exista como m�nimo una propiedad url, el resto son opcionales."}
      ,openApplication:{args:[
          {name: "application", info:"Identificador de la aplicaci�n a abrir"}
         ,{name: "id", info:"Si establecemos un id entonces la ventana se abrir� con ese id (y por tanto si existia ya no se crea si no que se recarga)."}
         ,{name: "title", info:"Titulo de la ventana"}
         ,{name: "position", info:"Una cadena de caracteres que describe la posici�n de la ventana. Puede ser 'x,y','center','center,y','x,center','n','s','e','w','ne','nw','se','sw', 'idventana:T|B|L|R:Offset' (si quieres colocarla relativa a otra ventana), 'rXX,YY' (por ejemplo r10,5 la coloca pegada a la derecha 10 pixeles y en el y 5), 'XX,bYY' (por ejemplo 5,b10 la coloca en el x 5 y pegada abajo 10 p�xeles) o 'rXX,bYY'"}
         ,{name: "testigo", info:"objeto javascript a pasar al abrir la ventana a la funci�n de recogida de testigo existente en el HTML contenido en la ventana si dicha funci�n existe. El paso de testigo es una funcionalidad utilizada para recuperar el estado de las ventanas automaticamente al volver a iniciar la sesi�n de un usuario. Si al cerrar una ventana existe una cierta funci�n javascript de paso de testigo entonces la funci�n es llamada y el testigo almacenado para cuando se recupere la sesi�n. El paso de testigo se explica en otro apartado de la documentaci�n."}
         ,{name: "testigoJson", info:"Igual que el par�metro 'testigo' salvo porque este par�metro no es un objeto cualquiera javascript si no una cadena de caracteres que debe ser evaluada cono objeto json antes de pasarselo a la funci�n de recogida de testigo. "}
         ,{name: "helpUrl", info:"URL a abrir cuando se pulse en el bot�n de ayuda existente en la ventana."}
         ,{name: "relativeHelpUrl", info:"URL a abrir cuando se pulse en el bot�n de ayuda de la ventana. La URL es relativa al directorio donde queda la ayuda instalada tras subir un DAP."}
         ,{name: "idPadre", info:"Id de la ventana que se va  aconsiderar padre de esta (opcional)."}
         ,{name: "status", info:"puede ser 'maximized' (la ventana se crea maximizada) o 'minimized' (aparece minimizada)"}
         ,{name: "widget", info:"Si es 'true' entonces la ventana es un widget (no tiene marco, el fondo es transparente y activa noTaskbar=true y noHide=true)."}
         ,{name: "noTaskbar", info:"Si es 'true' entonces la ventana no aparece en la barra de tareas"}
         ,{name: "single", info:"Si es igual a 'true' entonces se establece un id unico para la aplicaci�n."}
         ,{name: "sessionSave", info:"Si es 'false' entonces la ventana no se salvar� en la sesi�n."}
         ,{name: "noHide", info:"Si es 'true' entonces el contenido de la ventana no se oculta cuando se mueve o se cambia de tama�o otra ventana."}
         ,{name: "virtualDesktop", info:"Identificador del desktop virtual en que se abre la ventana. Por defecto es el desktop activo."}
         ,{name: "showInAllDesktops", info:"Si es 'true' entonces la ventana o widget aparece siempre en todos los desktops."}
         ,{name: "width", info:"Ancho de la ventana interna en pixeles incluyendo el borde exterior. Esta propiedad se ignora si existe 'innerWidth' y produce tama�os de ventana iguales independientemente del Skin."}
         ,{name: "height", info:"Alto de la ventana interna en pixeles incluyendo el borde exterior. Esta propiedad se ignora si existe 'innerHeight' y produce tama�os de ventana iguales independientemente del Skin."}
         ,{name: "innerWidth", info:"Ancho de la ventana interna (iframe) en pixeles sin incluir el borde exterior. Esta propiedad siempre tiene preferencia sobre 'width' y produce tama�os de ventana distintos segun el Skin."}
         ,{name: "innerHeight", info:"Alto de la ventana interna (iframe) en pixeles sin incluir el borde exterior. Esta propiedad siempre tiene preferencia sobre 'height' y produce tama�os de ventana distintos segun el Skin."}
         ,{name: "noResize", info:"Si es 'true' entonces la ventana no puede cambiar su tama�o arrastrando aunque si puede maximizarse o minimizarse."}
         ,{name: "noDrag", info:"Si es 'true' entonces la ventana no puede puede ser arrastrada y quedar� fijada en la posici�n que tenga."}
         ,{name: "parameter", info:"Si su valor es &quot;AAA = BBB&quot; entonces envia un par�metro AAA con valor BBB por el m�todo 'get'. Se pueden definir tantos &quot;-parameter&quot; como se necesite."}
         ]
         , info:"Abre una ventana y carga dentro una aplicaci�n definida previamente en los ficheros descriptores de aplicacion del escritorio. El par�metro application que es obligatorio."}
      ,reload:{args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Recarga el contenido de una ventana."}
      ,close:{args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Cierra la ventana con ese id. Si no se especifica id se entiende que se est� llamando desde dentro de una ventana y se quiere cerrar esa misma ventana."}
      ,setTitle:{args:[{name: "title", info:"Nuevo titulo."},{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Establece el t�tulo de una ventana"}
      ,setTop:{args:[{name: "top", info:""},{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Establece el y de una ventana"}
      ,setLeft:{args:[{name: "left", info:""},{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Establece el x de una ventana"}
      ,setWidth:{args:[{name: "width", info:""},{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Establece el ancho de una ventana"}
      ,setHeight:{args:[{name: "height", info:""},{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Establece el alto de una ventana"}
      ,setStatus:{args:[{name: "status", info:"Puede ser null (normal), 'maximized' o 'minimized'"},{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Establece el estado de una ventana (maximizada/minimizada)"}
      ,showMessage:{args:[{name: "message", info:"Mensaje a mostrar"}], info:"Muestra un mensaje en un dialogo del escritorio."}
      ,showConfirm:{args:[{name: "message", info:"Mensaje a mostrar"}], info:"Muestra un dialogo de confirmaci�n del escritorio con un cierto mensaje y retorna true o false."}
      ,getTop:{args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Retorna la posici�n y de una ventana."}
      ,getLeft:{args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Retorna la posici�n x de una ventana."}
      ,getStatus:{args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Retorna el estado de una ventana ('maximized', 'minimized', o null si no est� ni maximizada ni minimizada)."}
      ,resizeTo:{args:[{name: "width", info:""},{name: "height", info:""},{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Establece el ancho y alto de una ventana."}
      ,moveTo:{args:[{name: "left", info:""},{name: "top", info:""},{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Establece la posici�n x e y de una ventana."}
      ,moveResizeTo:{args:[{name: "width", info:""},{name: "height", info:""},{name: "left", info:""},{name: "top", info:""},{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Establece el ancho, alto y la posici�n x e y de una ventana."}
      ,sessionSaveAndRestart:{args:[], info: "Salva la sesion y refresca el escritorio."}
      ,sessionRestart:{args:[], info:"Refresca el escritorio sin salvar la sesi�n (se perderan los cambios)."}
      ,toFront:{args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Hace la ventana visible sobre todas las dem�s."}
      ,toBack:{args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Hace que la ventana quede detr�s de cualquier otra ventana abierta."}
      ,bindDesktopResize:{args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."},{name:"handlerName", info:"Nombre de la funci�n javascript a ejecutar."}],info:"Hace que se ejecute una funci�n javascript en el documento residente en la ventana cuando el escritorio cambia de tama�o."}
      ,getUserData:{args:[{name:"cache",info:"Si vale true se retornan los datos cacheados al principio de sesi�n, si no se va a servidor a por ellos."} ,{name:"async",info:"Si vale true (y el par�metro cache no vale true) entonces se va a servidor a por los datos asincronamente y en ese caso se ha de informar tambi�n el par�metro callback. Si la llamada es sincrona el navegador se bloquear� hasta que acabe la comunicaci�n."} ,{name:"callback",info:"Si el par�metro cache no vale true  y el par�metro async vale true en entonces este par�metro de tipo Function es la funci�n js a la que se llamar� con los datos cuando retorne la llamada al servidor."} ] ,info:"Retorna los datos del usuario en sesi�n."}
      ,getCookie:{args:[{name:"name",info:"The name of the cookie"}],info:"Retorna el valor de una cookie."}
      ,deleteCookie:{args:[{name:"name",info:"The name of the cookie"}],info:"Elimina una cookie."}
      ,setCookie:{args:[
            {name:"name",info:"The name of the cookie"}
           ,{name:"value",info:"Value of the cookie"}
           ,{name:"expires",info:"Either an integer specifying the expiration date from now on in days or a Date object. If set to null or omitted, the cookie will be a session cookie. If a negative value is specified the cookie will be deleted."}
           ,{name:"path",info:"The value of the path atribute of the cookie (default: path of page that created the cookie)."}
           ,{name:"domain",info:"The value of the domain attribute of the cookie (default: domain of page that created the cookie)."}
           ,{name:"secure",info:"If true, the secure attribute of the cookie will be set and the cookie transmission will require a secure protocol (like HTTPS)."}
         ],info:"Crea una cookie o establece su valor."}
      ,hide:{info:"Oculta una ventana.",args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}]}
      ,show:{info:"Muestra una ventana que estaba oculta.",args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}]}
      ,changeVirtualDesktop:{info:"Cambia el desktop virtual activo. Al principio siempre se trabaja en el desktop virtual '0'.",args:[{name:"virtualDesktop", info:"Identificador del desktop virtual. Su valor por defecto es '0'"}]}
      ,getCurrentVirtualDesktop:{info:"Retorna el identificador del desktop virtual activo.",args:[]}
      ,setVirtualDesktop:{info:"Establece el dektop virtual al que pertenece una ventana.",args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."},{name:"virtualDesktop", info:"Identificador del desktop virtual."}]}
      ,getVirtualDesktop:{info:"Retorna el identificador del dektop virtual al que pertenece una ventana.",args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}]}
      ,getVirtualDesktopsData:{info:"Retorna un Array con un objeto por cada desktop virtual existente que tenga ventanas. Cada objeto tiene dos propiedades: id con el identificador del desktop virtual y windowIds con un array de identificadores con las ventanas activas en ese desktop.", args:[]}
      ,getHeight:{args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Retorna el alto de una ventana."}
      ,getWidth:{args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}], info:"Retorna el ancho de una ventana."}
      ,getInnerWidth:{info:"Retorna para una ventana el ancho de su ventana interna (iframe) sin incluir el borde exterior", args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}]}
      ,getInnerHeight:{info:"Retorna para una ventana el alto de su ventana interna (iframe) sin incluir el borde exterior", args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}]}
      ,setNoDrag:{info:"", args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."},{name:"noDrag",info:""}]}
      ,getNoDrag:{info:"", args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}]}
      ,setNoResize:{info:"", args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."},{name:"noResize",info:""}]}
      ,getNoResize:{info:"", args:[{name:"id", info:"Identificador de la ventana. Por defecto es el de la ventana desde la que se ejecuta el m�todo."}]}
      ,showHelp:{info:"", args:[{name:"windowId", info:""},{name:"locale", info:""},{name:"url", info:""}]}
      ,setGlobalNoDrag: {info:"", args:[{name:"globalNoDrag", info:""}]}
      ,getGlobalNoDrag: {info:"", args:[]}
      ,setGlobalNoResize: {info:"", args:[{name:"globalNoResize", info:""}]}
      ,getGlobalNoResize: {info:"", args:[]}
      ,setGlobalNoClose: {info:"", args:[{name:"globalNoClose", info:""}]}
      ,getGlobalNoClose: {info:"", args:[]}
      ,setGlobalNoContextMenu: {info:"", args:[{name:"globalNoContextMenu", info:""}]}
      ,getGlobalNoContextMenu: {info:"", args:[]}
      ,getDesktopProperties: {info:"", args:[]}
      ,getSkinParameters: {info:"", args:[{name:"skin", info:""}]}
      ,focus: {info:"", args:[{name:"id", info:""},{name:"noZIndexChange", info:""},{name:"zIndex", info:""}]}
      ,blur:{info:"", args:[]}
      ,hasFocus: {info:"",args:[{name:"id", info:""}]}
      ,getWindowExist: {info:"",args:[{name:"id", info:""}]}
      ,getSkinUrl: {info:"",args:[{name:"skin", info:""}]}
      ,getMenuObject:{info:"",args:[]}
      ,setStatusAllWindows:{info:"",args:[{name:"status", info:""},{name:"onlyCurrentDesktop", info:""}]}
      }
   var publicMethods={
      info:"Objeto para la manipulaci�n de ventanas y elementos del escritorio."
      ,subCommands:subCommands
      }
   return publicMethods
   }

function public_setStatusAllWindows(args)   {
   if (args==null)
      args={}
   var status=args.status, onlyCurrentDesktop=drEvaluaBooleanoString(args.onlyCurrentDesktop);
   var ventanasAMinim=drWindow.getWindowIds({noWidgets:"true",onlyCurrentDesktop:onlyCurrentDesktop})
   for (var r=0; r<ventanasAMinim.length; r++)  {
      if (drWindow.getStatus({id:ventanasAMinim[r]})!=status)  {
         drWindow.setStatus({id:ventanasAMinim[r], status:status})   
      }
   }
}

function private_getMenuObject() {
   return this.getDesktopWindow().aMenu
}

function private_setMenuObject(obj) {
   this.getDesktopWindow().aMenu=obj
}


function  public_getSkinUrl(args){
   if (args==null)
      args={}
   if (args.skin==null)
      args.skin=this.getSkin();
   return this.getDesktopProperties()["urlSkins"]+'/'+args.skin
}



function public_blur() {
   var ww=this.getDesktopWindow()
   if (ww.drDesktop_windowIdWithFocus!=null)  {
      var skinData=this.getSkinData()
      if (skinData && skinData.onWindowChangeFocusStatus!=null && this.getWindowExist({id:ww.drDesktop_windowIdWithFocus})) { //hago el blur si existe la ventana todavia 
         skinData.onWindowChangeFocusStatus({blur:true, id:ww.drDesktop_windowIdWithFocus})
      }
   }
   ww.drDesktop_windowIdWithFocus=null
}

function public_focus(params) {
   if (params==null)
      params={}
   var id=params.id, noZIndexChange=params.noZIndexChange, zIndex=params.zIndex;
   if (id==null )
      id=this.getId()
   if (!this.getWindowExist({id:id}))
      return false
   if (!noZIndexChange)
      this.getXDesktopObj().moveWindowToFront({wName:id, zIndex:zIndex}) 
   if (this.hasFocus({id:id}))
      return id
   var skinData=this.getSkinData()
   var ww=this.getDesktopWindow()
   if (ww.drDesktop_windowIdWithFocus!=null)  {
      if (skinData && skinData.onWindowChangeFocusStatus!=null && this.getWindowExist({id:ww.drDesktop_windowIdWithFocus})) { //hago el blur si existe la ventana todavia 
         skinData.onWindowChangeFocusStatus({blur:true, id:ww.drDesktop_windowIdWithFocus})
      }
   }
   ww.drDesktop_windowIdWithFocus=id
   if (skinData && skinData.onWindowChangeFocusStatus!=null)      {
      skinData.onWindowChangeFocusStatus({focus:true, id:id})
   }
   return id;
}


function public_hasFocus(params) {
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   if (id==null)
      return false
   var ww=this.getDesktopWindow()
   if (ww.drDesktop_windowIdWithFocus==id)  
      return true
   return false
}


function private_initConfig() {//se ejecuta una vez al principio
   //Preproceso los atributos de URL y de URL de ayuda de las aplicaciones
   var relativeHelpAttrName="relativeHelpUrl"
   var helpAttrName="helpUrl"
   /*if (params && params.locale)
      helpAttrName="relativeHelpUrl_"+params.locale*/
   var apps=this.getApplicationsConfig()
   for (app in apps)  {
      var appCfg=apps[app]
      if (appCfg[relativeHelpAttrName]!=null)  {
         var idGrupoAplicacion=appCfg.grupo
         var nombreGrupoApp= this.getApplicationsGroupsConfig()[idGrupoAplicacion].name
         appCfg[helpAttrName]=this.getDesktopProperties()["urlHelpRoot"] +"/"+ nombreGrupoApp +"/"+appCfg[relativeHelpAttrName]
      }
      if (appCfg["relativeUrl"]!=null)  {
         var idGrupoAplicacion=appCfg.grupo
         var nombreGrupoApp= this.getApplicationsGroupsConfig()[idGrupoAplicacion].name
         appCfg["url"]=this.getDesktopProperties()["urlAppRoot"] +"/"+ nombreGrupoApp +"/"+appCfg["relativeUrl"]
      }
   }
}


function public_setNoDrag(params){
   if (params==null)
      params={}
   var id=params.id,noDrag=params.noDrag
   if (id==null)
      id=this.getId()
   this.getXDesktopObj().prop(id,"wNoDrag",drEvaluaBooleanoString(noDrag))
}

function public_getNoDrag(params){
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   return this.getXDesktopObj().prop(id,"wNoDrag")
}

function public_setNoResize(params){
   if (params==null)
      params={}
   var id=params.id,noResize=params.noResize
   if (id==null)
      id=this.getId()
   this.getXDesktopObj().prop(id,"wNoResize",drEvaluaBooleanoString(noResize))
}

function public_getNoResize(params){
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   return this.getXDesktopObj().prop(id,"wNoResize")
}

function public_setGlobalNoDrag(params){
   if (params==null)
      params={}
   var globalNoDrag=params.globalNoDrag
   this.setGlobalBoolean({name:"globalNoDrag", value:globalNoDrag})
}

function public_setGlobalNoResize(params){
   if (params==null)
      params={}
   var globalNoResize=params.globalNoResize
   this.setGlobalBoolean({name:"globalNoResize", value:globalNoResize})
}

function public_setGlobalNoClose(params){
   if (params==null)
      params={}
   var globalNoClose=params.globalNoClose
   this.setGlobalBoolean({name:"globalNoClose", value:globalNoClose})
}

function public_setGlobalNoContextMenu(params){
   if (params==null)
      params={}
   var globalNoContextMenu=params.globalNoContextMenu
   this.setGlobalBoolean({name:"globalNoContextMenu", value:globalNoContextMenu})
}

function public_getGlobalNoDrag(){
   return this.getGlobalBoolean({name:"globalNoDrag"})
}

function public_getGlobalNoResize(){
   return this.getGlobalBoolean({name:"globalNoResize"})
}

function public_getGlobalNoClose(){
   return this.getGlobalBoolean({name:"globalNoClose"})
}

function public_getGlobalNoContextMenu(){
   return this.getGlobalBoolean({name:"globalNoContextMenu"})
}


function private_setGlobalBoolean(params) {
   if (params==null)
      params={}
   var name=params.name, value=drEvaluaBooleanoString(params.value)
   this.setDesktopConfigValue({name:name, value:value})
}

function private_getGlobalBoolean(params) {
   if (params==null)
      params={}
   var name=params.name
   return drEvaluaBooleanoString( this.getDesktopConfigValue({name:name}) )
}

function public_setSkinData(args) { //params: var skin=args.skin, data=args.data
   var ww=this.getDesktopWindow();
   if (args==null)
      args={}
   if (args.skin==null)
      args.skin=this.getSkin() //Por defecto el skin activo
   var skin=args.skin, data=args.data
   ww["oSkin_"+skin]=args.data
}

function public_getSkinData(args) {//params: var skin=args.skin
   //Este m�todo retorna oSkin_XX si existe (ese objeto se crea si se llama a la funci�n oSkinFactory.createSkin) si no es null
   var ww=this.getDesktopWindow();
   if (args==null || args.skin==null)
      args={skin:this.getSkin()} //Por defecto el skin activo
   var skin=args.skin
   return ww["oSkin_"+skin]
}

function public_getTaskbarData ()   {
   return this.getDesktopWindow()["oDatosBotonera_"+this.getSkin()]
}

function public_setTaskbarData (args)   {
   if (args.data)
      this.getDesktopWindow()["oDatosBotonera_"+this.getSkin()]=args.data
}

function public_getSkinParameters(args) {
   if (args==null || args.skin==null)
      var skin=this.getSkin();
   else
      var skin=args.skin
   var skins=this.getSkins();
   for (var r=0; r<skins.length; r++)
      if (skins[r].id==skin)
         return skins[r].parameters
}

function private_prepareSkinParameterChange(args)  {
   var name=args.name, value=args.value
   var ww=this.getDesktopWindow()
   if (ww.oSkinParameterChanges==null)
      ww.oSkinParameterChanges={skin:this.getSkin(), changes:{}}
   ww.oSkinParameterChanges.changes[name]=value
     // alert(name+":"+value)
}

function private_getSkinParameterChanges()  {
   var ww=this.getDesktopWindow()
   if (ww.oSkinParameterChanges==null)
      return {}
   else
      return ww.oSkinParameterChanges
}


function public_getSkinMeta(args) {
   if (args==null || args.skin==null)
      var skin=this.getSkin();
   else
      var skin=args.skin
   var skins=this.getSkins();
   for (var r=0; r<skins.length; r++)
      if (skins[r].id==skin)
         return skins[r].paramMeta
}

function public_getFileTypesConfig()   {
   return this.getDesktopWindow().oTipos
}

function public_openFile(args) { //Abre una nueva ventana con los argumentos. 
   //la URL es la de la aplicacion mas el par�metro "file="args.file  con una ruta RELATIVA a su fileSystem (ojo seguridad)

   //var extension=drExtraeExtension(rutaFichero);
   var extensionMatch=args.file.match(/.([^.]+)$/)
   var extension=null
   if (extensionMatch!=null)
      extension=extensionMatch[1]
   if (extension==null || extension=="")
      return 
   var extData=this.getFileTypesConfig()[extension]
   if (extData==null)	{
		//alert("La extensi�n '"+extension+"' no est� asociada a ninguna aplicaci�n.");
		return;
		}
   args.application=extData.aplicacion
   if (args.parameters==null)
      args.parameters=[]
   args.parameters[args.parameters.length]={name:"file", value:args.file}
   return this.openApplication(args);
}


function public_deleteCookie(args){ //args.name requerido
   drWindow.getDesktopWindow().$.cookie(args.name, null);
}

function public_getCookie(args)   {//args.name requerido
   return drWindow.getDesktopWindow().$.cookie(args.name);
}

function public_setCookie(args)   {//args.name args.value requeridos
   drWindow.getDesktopWindow().$.cookie(args.name, args.value, args);
}

function public_getUserData(args) {
   /*casos: 
      cache:true --> uso los que tengo cacheados
      async:false, cache:false --> voy a por ellos sincronizadamente
      async:true, cache:false, callback:[function] --> voy a por ellos asincronamente
   */
   if (args==null)
      args={}
   var ww=drWindow.getDesktopWindow();
   if (args.cache)   {
      return ww.drUserData //TO-DO
      }
   else if (args.async)   { 
      ww.ConfigurationService.getUserConfig({
         async:false
         ,callback:args.callback //necesito un argumento callback 
         ,timeout:conDwrConnectionTimeout
         ,errorHandler:conDwrServicesInit_errorHandler
      });
      }
   else  {
      var objResp={}
		ww.ConfigurationService.getUserConfig({
         async:false
         ,callback:function (obj)   { objResp.userData=obj } //Utilizo una closure, unica solucion en dwr 2.0
         ,timeout:conDwrConnectionTimeout
         ,errorHandler:conDwrServicesInit_errorHandler
      });
      ww.drUserData=objResp.userData
      return objResp.userData;
      }
   return null;
   }

function conDwrServicesInit_errorHandler(message)	{
	drAlert("Ha ocurrido un problema t�cnico.\nDescripci�n del problema:\n  [ " + message+" ]"); 
	//debugger
}

function public_getUserData_ret(obj,objResp)   {
   //alert(obj.nombre)
   //debugger
   //alert(obj.nombre)
   //objResp.ww.pepe=obj
   //objResp=obj
   }

function public_showMessage(params) {
   if (params.message==null)
      params.message="- empty -"
	if (params.sistemMessage && !oDesktopConfig.mensajesDelSistema)
		return
	if (window.showModalDialog)	{
		var objDatos={	
			texto:params.message
			,titulo:"Desktop Dialog Box"
			,botones:[{texto:"Aceptar", retorna:1, iconoN:"img/drIcoAceptarN.gif", iconoS:"img/drIcoAceptarS.gif"}]
			}
		abreVentanaModalDesktop(objDatos)
		}
	else
		alert(params.message)
   }

function public_showConfirm(params) {
   if (params.message==null)
      params.message="- empty -"
	if (window.showModalDialog)	{
		var objDatos={	
			texto:params.message
			,titulo:"Desktop Dialog Box"
         ,botones:[
				{texto:"Aceptar", retorna:true, iconoN:"img/drIcoAceptarN.gif", iconoS:"img/drIcoAceptarS.gif"}
				,{texto:"Cancelar", retorna:false, iconoN:"img/drIcoCancelarN.gif", iconoS:"img/drIcoCancelarS.gif"}
            ]
			}
		return abreVentanaModalDesktop(objDatos)
		}
	else
		return confirm(params.message)
   }


function public_sessionSaveAndRestart() {
	top.deseaSalir=true;
   //cierraSesionPrevio
   var objDesktopConfig=this.getDesktopConfig()
   var objDesktopWindow=this.getDesktopWindow()
	if (objDesktopWindow.temaTrasReiniciar!=null)	{
      objDesktopConfig.margSupAnt=drWindow.getXDesktopWindowObj().marginTop(drWindow.getSkin())
      this.getDesktopConfig().tema=objDesktopWindow.temaTrasReiniciar
		}
	objDesktopWindow.cierraSesionVentanas()
	objDesktopWindow.cierraSesionSkin()
	objDesktopWindow.cierraSesionIconos()
	//cierraSesionConfiguracion
   objDesktopWindow.document.fcerrar.configuracion.value=objDesktopWindow.objetoAJSONString(objDesktopConfig)
	objDesktopWindow.document.fcerrar.submit();
   }

function public_sessionRestart() {
   this.getStructuralWindow().document.location.reload();
   }

function public_toFront(params)  {
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   this.getXDesktopObj().moveToFrontWindow(id)
   }

function public_toBack(params)  {
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   //Obtengo la lista de las ventanas que hay ordenadas seg�n su z-index...
   var idsVentanas=this.getWindowIds()
   var listaDatosVentanas=[], cbeObject=this.getXDesktopObj().cbe
   for (var r=0; r<idsVentanas.length; r++)  
      listaDatosVentanas[listaDatosVentanas.length]={id:idsVentanas[r],zindex:cbeObject(idsVentanas[r]).zIndex()}
   listaDatosVentanas=listaDatosVentanas.sort(function (a,b){
      if (a.zindex>b.zindex)
         return 1
      return -1
      })
   for (var r=0; r<listaDatosVentanas.length; r++) {
      var idVentana=listaDatosVentanas[r].id
      if (idVentana!=id)
         this.toFront({id:idVentana})
      }
   }


function public_reload(params)   {
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   windowApl=this.getNativeWindow({id:id})
   if (windowApl!=null) { //Lo normal, el obj Window existe ...
      windowApl.location.reload(); 
      return true
      }
   return false
   }


function public_bindDesktopResize(args) {
   if (args==null)
      args={}
   if (args.id==null)
      args.id=this.getId()
   if (args.handlerName==null)
      args.handlerName="onDesktopResize"//Por defecto ...
   //args.windowApl=this.getNativeWindow(args)
   //args.func=args.windowApl[args.handlerName]
   var ww=this.getDesktopWindow()
   if (ww.onDesktopResize_global_handlers==null)
      ww.onDesktopResize_global_handlers=[]
   var encontrado=false, indice=0
   while (indice<ww.onDesktopResize_global_handlers.length && !encontrado) {
      var argsHandler=ww.onDesktopResize_global_handlers[indice]
      if (argsHandler.id==args.id && argsHandler.handlerName==args.handlerName)
         encontrado=true;
      indice++
      }
   if (!encontrado)
      ww.onDesktopResize_global_handlers[ww.onDesktopResize_global_handlers.length]=args
   ww.$(ww).bind("resize",ww.onDesktopResize_global)//Solo lo va a hacer una vez ...
   }

function onDesktopResize_global()   {
   for (var r=0; r<onDesktopResize_global_handlers.length; r++)   {
      var args=onDesktopResize_global_handlers[r]
      var windowApl=drWindow.getNativeWindow(args) 
      if (windowApl!=null) {
         var func=windowApl[args.handlerName]
         if (func!=null)
            func()
         else  {
            //elimino esta referencia
            }
         }
      else  {
         //elimino esta referencia
         }
      }
   }

function public_killDaemons(args) {//id (mata todos los demonios de una ventana)
   if (args==null)
      args={}
   var id=args.id
   if (id==null)
      id=this.getId()
   var arrayDaemons=drWindow.getDaemonRegistry().saca({"idVentana":id}) //Elimino el daemon
   if (arrayDaemons!=null) //Si es igual a null tendriamos un proceso erroneo ...
      for (var r=0; r<arrayDaemons.length; r++) 
         drWindow.getDesktopWindow().clearInterval(arrayDaemons[r].idInterval);
   }

function public_createDaemon(args)   {//id, milliseconds, script
   if (args==null)
      args={}
   var idVentana=args.id
   if (idVentana==null)
      idVentana=this.getId()
   var scriptVentana=args.script
   if (scriptVentana!=null)   {
      var demonioALLamar=function ()   {
         var windowApl=drWindow.getNativeWindow({id:idVentana})
         if (windowApl!=null) { //Lo normal, el obj Window existe ...
            var func=windowApl[scriptVentana]
            if (func!=null)
               func()
            }
         else  {//Si el obj window no existe ...
            if (!drWindow.getRegistry().existe(idVentana))  {//Y la ventana no est� registrada ...
               drWindow.killDaemons({id:idVentana})
               /*
               var arrayDaemons=drWindow.getDaemonRegistry().saca({"idVentana":idVentana}) //Elimino el daemon
               if (arrayDaemons!=null) //Si es igual a null tendriamos un proceso erroneo ...
                  for (var r=0; r<arrayDaemons.length; r++) 
                     window.clearInterval(arrayDaemons[r].idInterval);
               */
               }
            }
         }
      }
   else  {
      var demonioALLamar=function ()   {
         var exito=drWindow.reload({id:idVentana})
         if (!exito) {//Si el obj window no existe ...
            if (!drWindow.getRegistry().existe(idVentana))  {//Y la ventana no est� registrada ...
               drWindow.killDaemons({id:idVentana})
               /*
               var arrayDaemons=drWindow.getDaemonRegistry().saca({"idVentana":idVentana}) //Elimino el daemon
               if (arrayDaemons!=null) //Si es igual a null tendriamos un proceso erroneo ...
                  for (var r=0; r<arrayDaemons.length; r++) 
                     window.clearInterval(arrayDaemons[r].idInterval);
               */
               }
            }
         }
      }
   var idInterval=drWindow.getDesktopWindow().setInterval(demonioALLamar, args.milliseconds);
   drWindow.getDaemonRegistry().mete({"idVentana":idVentana, "idInterval":idInterval, "milliseconds":args.milliseconds, "script":args.script})
   }

function public_getApplicationIds() {
   var oAplicacion=this.getApplicationsConfig()
   var resp=[]
   for (a in oAplicacion)  {
      if (a+""!="")
         resp[resp.length]=a
      }
   return resp
   }

function public_setTop(params) {
   this.moveResizeTo(params)
   }

function public_setLeft(params) {
   this.moveResizeTo(params)
   }

function public_setWidth(params) {
   this.moveResizeTo(params)
   }

function public_setHeight(params) {
   this.moveResizeTo(params)
   }

function public_resizeTo(params) {
   this.moveResizeTo(params)
   }

function public_moveTo(params) {
   this.moveResizeTo(params)
   }

function public_getInnerWidth(args) {
   var iFrame=this.getNativeIframe(args)
   return iFrame.offsetWidth
   //return $(iFrame).innerWidth()//No va en FF
}

function public_getInnerHeight(args) {
   var iFrame=this.getNativeIframe(args)
   //return $(iFrame).innerHeight() //No va en FF
   return iFrame.offsetHeight
}

function public_getWidth(params) {
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   return parseIntSecure(this.getXDesktopObj().prop(id,"wWidth"))
   }

function public_getHeight(params) {
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   return parseIntSecure(this.getXDesktopObj().prop(id,"wHeight"))
   }

function public_getTop(params) { 
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   var propTop=this.getXDesktopObj().prop(id,"wY")
   if ((""+propTop).match(/^b(\d+)$/))
      return propTop
   var propIgnorarMargenes=this.getXDesktopObj().prop(id,"wIgnoreMargins");
   if (propIgnorarMargenes!=null) {
      var ignorarMargenes=drEvaluaBooleanoString(propIgnorarMargenes)
      }
   if (ignorarMargenes) 
      return parseIntSecure(propTop)
   else
      return parseIntSecure(propTop)-this.getTopMargin();
   }

function public_getLeft(params) {
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   var propLeft=this.getXDesktopObj().prop(id,"wX")
   if ((""+propLeft).match(/^r(\d+)$/))
      return propLeft
   return parseIntSecure(propLeft)
   }

function public_moveResizeTo(params) {
   if (params==null)
      params={}
   var id=params.id
   var l=params.left
   var t=params.top
   var h=params.height
   var w=params.width
   var oXD=this.getXDesktopObj()
   if (id==null)
      id=this.getId()
   l=(l!=null)?l:oXD.prop(id,"wX")
   w=(w!=null)?w:oXD.prop(id,"wWidth")
   h=(h!=null)?h:oXD.prop(id,"wHeight")
   if (t!=null)   {   
      var propIgnorarMargenes=oXD.prop(id,"wIgnoreMargins");
      if (propIgnorarMargenes!=null) { 
         //propIgnorarMargenes=propIgnorarMargenes.toUpperCase()
         //var ignorarMargenes=(propIgnorarMargenes=="S"||propIgnorarMargenes=="TRUE"||propIgnorarMargenes=="Y" )
         var ignorarMargenes=drEvaluaBooleanoString(propIgnorarMargenes)
         }
      t=drPreProcesaTop(t,h,ignorarMargenes)
      }
   else
      t=oXD.prop(id,"wY")
   //Recoloco
   if (oXD.prop(id,"wStat")!="MAX") {
      var elCbe=oXD.cbe(id)
      elCbe.resizeTo(w,h);
      elCbe.moveTo(l,t);
      }
   oXD.prop(id,"wWidth",w)
   oXD.prop(id,"wHeight",h)
   oXD.prop(id,"wX",l)
   oXD.prop(id,"wY",t)
   }

function drPreProcesaTop(top,height,ignorarMargenes)	{ //top puede ser algo como 20, b20 o center
   if (top==null)
      return 
   var ignorarMargenes=drEvaluaBooleanoString(ignorarMargenes)
   if (ignorarMargenes) {
      var marginTop=0
      var clientHeight=drWindow.getDesktopWindow().document.body.clientHeight
      }
   else  {  
      var marginTop=drWindow.getTopMargin(); //xDTwin.marginTop(oDesktopConfig.tema)
      var clientHeight=drWindow.getDesktopWindow().document.body.clientHeight - marginTop - drWindow.getBottomMargin()
      }
   if (top=="center")  {
      var puntoMedio=Math.round(clientHeight/2)+marginTop
      return puntoMedio-Math.round(height/2);
      }
   if (!isNaN(parseInt(top)))	
      return parseInt(top) + marginTop    		//Le sumo el margen superior.
   return top
	}

function public_getDesktopWindow()  {//Retorna la ventana en la que reside el escritorio
   if (this.desktopWindow==null) {
      if (window.top.f1==null){
         if (window.dialogArguments!=null && window.dialogArguments.openerWindow!=null)
            this.desktopWindow=window.dialogArguments.openerWindow.top.f1
      }else
         this.desktopWindow=window.top.f1
   }
   return this.desktopWindow   //Presuponemos una cierta estructura en el top, si surge la necesidad de modificar la estructura habr� que tocar esto.
   }
   
function public_getStructuralWindow()  {//Retorna la ventana en la que reside el escritorio
   return window.top
   }  

function public_getXDesktopObj()  {
   return this.getDesktopWindow().xDT
   }

function public_getXDesktopWindowObj()  {
   return this.getDesktopWindow().xDTwin
   }

function public_getDesktopConfig()  {
   return this.getDesktopWindow().oDesktopConfig
   }

function private_setDesktopConfigValue(args)  {
   var name=args.name, value=args.value
   this.getDesktopWindow().oDesktopConfig[name]=value
}

function private_getDesktopConfigValue(args)  {
   var name=args.name
   return this.getDesktopWindow().oDesktopConfig[name]
}

function public_getDesktopProperties() {
   return this.getDesktopWindow().oDesktopProperties
   }

function public_getEnginesConfig()	{
   return this.getDesktopWindow().oMotores
   }

function public_getApplicationsConfig()	{
   return this.getDesktopWindow().oAplicaciones
   }
   
function public_getApplicationsGroupsConfig()	{
   return this.getDesktopWindow().oGruposAplicaciones
   }   

/*
function public_getSkinGroupsConfig()	{ //Solo los skins que estan asociados a grupos (util para desinstalar grupos)
   return this.getDesktopWindow().oSkinsGrupos
   }   */

function public_getSkins() {
   return this.getDesktopWindow().oSkins
   }

function public_getSkin() {
   return this.getDesktopConfig().tema
   }

function public_setSkin(params) { //No se activa hasta reiniciar
   var tema=params.skin
   this.getDesktopWindow().temaTrasReiniciar=tema
   //this.getDesktopConfig().tema=tema
   }

function public_getTopMargin()   {
   return this.getXDesktopWindowObj().marginTop(this.getSkin())
   }

function public_getBottomMargin()   {
   return this.getXDesktopWindowObj().marginBottom(this.getSkin())
   }

function public_getClientWidth() {
   return this.getDesktopWindow().document.body.clientWidth
   }

function public_getClientHeight() {
   return this.getDesktopWindow().document.body.clientHeight - this.getTopMargin() - this.getBottomMargin()
   }

function public_getRegistry() {
	if (this.getDesktopWindow().oRegistroVentanas==null)	
		this.getDesktopWindow().oRegistroVentanas=new clDrRegistroVentanas()
   return this.getDesktopWindow().oRegistroVentanas
   }

function public_getDaemonRegistry() {
	if (this.getDesktopWindow().oDaemons==null)	
		this.getDesktopWindow().oDaemons=new clDrRegistroDaemons()
   return this.getDesktopWindow().oDaemons
   }


/*
function public_getRegistry() {
   return this.getDesktopWindow().oRegistroVentanas
   }*/

function public_generateId()  {
   var losID="|"+ this.getWindowIds().join("|") +"|"
   var nuevoId="drWin"+Math.round(Math.random(33)*100000000)
   while (losID.indexOf("|"+nuevoId+"|")!=-1)	
      nuevoId="drWin"+Math.round(Math.random(33)*100000000)
   return nuevoId
   }

function public_getWindowIds(args) {
   if (args==null)
      args={}
   var noWidgets=drEvaluaBooleanoString(args.noWidgets), onlyCurrentDesktop=drEvaluaBooleanoString(args.onlyCurrentDesktop);
	//Retornamos las ventanas que existen (solo los ID)...
	if (this.getRegistry()==null)
		return []
	var arrObj=this.getRegistry().getLista();
	var arrId=[]
   if (!noWidgets && !onlyCurrentDesktop){
   	for (var r=0; r<arrObj.length; r++)
	   	arrId[arrId.length]=arrObj[r].id
   } else if (noWidgets && onlyCurrentDesktop) {
      currentDesktop=drWindow.getCurrentVirtualDesktop()
   	for (var r=0; r<arrObj.length; r++){
         var elid=arrObj[r].id
	   	if (!arrObj[r].widget && this.inVirtualDesktop({id:elid, virtualDesktop:currentDesktop})){
            arrId[arrId.length]=elid
         }
      }
   } else if (!noWidgets && onlyCurrentDesktop) {
      currentDesktop=drWindow.getCurrentVirtualDesktop()
   	for (var r=0; r<arrObj.length; r++){
         var elid=arrObj[r].id
	   	if (this.inVirtualDesktop({id:elid, virtualDesktop:currentDesktop})){
            arrId[arrId.length]=elid
         }
      }
   } else if (noWidgets && !onlyCurrentDesktop) {
   	for (var r=0; r<arrObj.length; r++){
         var elid=arrObj[r].id
	   	if (!arrObj[r].widget){
            arrId[arrId.length]=elid
         }
      }
   }
	return arrId
   }


function private_refreshWindowObjectProperties(ventana) {
   var xD=this.getXDesktopObj()
   //Actualizo las propiedades din�micas
   ventana.width = this.getWidth({id:ventana.id})
   ventana.height = this.getHeight({id:ventana.id})
   if (ventana.innerWidth!=null)
      ventana.innerWidth=this.getInnerWidth({id:ventana.id})
   if (ventana.innerHeight!=null)
      ventana.innerHeight=this.getInnerHeight({id:ventana.id})
   ventana.position = this.getLeft({id:ventana.id}) +","+ this.getTop({id:ventana.id}) //No uso wPos porque al regenerar puede que no existan las ventanas desde las que te colocaste con un offset
   ventana.url = xD.prop(ventana.id,'wUrl')
   if (xD.prop(ventana.id,'wTitle')!=null)
      ventana.title = xD.prop(ventana.id,'wTitle')
   if (xD.prop(ventana.id,'drUrlInfo')!=null)
      ventana.helpUrl = xD.prop(ventana.id,'drUrlInfo')
   if (xD.prop(ventana.id,'wNoDrag')!=null)
      ventana.noDrag = xD.prop(ventana.id,'wNoDrag')
   if (xD.prop(ventana.id,'wNoResize')!=null)
      ventana.noResize = xD.prop(ventana.id,'wNoResize')
   var wStat=xD.prop(ventana.id,"wStat")
   if (wStat=="min")
      ventana.status ="minimized"
   else if (wStat=="MAX")
      ventana.status ="maximized"
   else
      ventana.status=null
   //actualizo la informaci�n de los DAEMON
   var regDemoniosEsaVentana=this.getDaemonRegistry().dame({idVentana:ventana.id})
   if (regDemoniosEsaVentana!=null) {
      var nuevoDaemonsWindow=[]
      for (var t=0; t<regDemoniosEsaVentana.length; t++) {
         var reg=regDemoniosEsaVentana[t]
         nuevoDaemonsWindow[t]={"milliseconds":reg.milliseconds,"script":reg.script,"miFuncion":reg.miFuncion}
         }
      ventana.daemons=nuevoDaemonsWindow
      }
   else
      ventana.daemons=null
   try   {
      var nativeWindow=this.getNativeWindow({id:ventana.id})
      if (ventana.DrDesktopDameTestigo && isFunction(ventana.DrDesktopDameTestigo) && window.objetoAJSONString!=null)	{
         var oTestigo=ventana.DrDesktopDameTestigo()
         var testigoJson=objetoAJSONString(oTestigo,true)
         ventana.testigoJson=testigoJson
         }
      } catch(e)  {}
   return ventana
}


function public_getWindowExist(args)  {
   var id=null
   if (args!=null && args.id!=null)
      id=args.id
   if (id==null)
      id=this.getId();
   if (id==null)
      return false;
   var registroVentanas = drWindow.getRegistry()
   if (registroVentanas==null)
      return false
   return registroVentanas.existe(id)

}

function public_getWindowData(args)  {
   var id=null
   if (args!=null && args.id!=null)
      id=args.id
   var registroVentanas = drWindow.getRegistry()
   if (registroVentanas==null){ // Si no hay ventanas retorno null o []
      if (id!=null)
         return null
      else
         return []
   }
   var ventanas=registroVentanas.getLista();
   if (id!=null) {//Si hay id doy los datos actualizados de una ventana
      //Busco el objeto ventana adecuado
      var indice=-1;
      for (var r=0;r<ventanas.length && indice<0; r++)  
         if (ventanas[r].id==id)
            indice=r
      if (indice==-1) //Si he dado el indice de una ventana que no existe retorno null
         return
      var ventana=ventanas[indice]
      //Actualizo sus propiedades
      return this.refreshWindowObjectProperties(ventana)
   }
   for (var r=0; r<ventanas.length; r++)	//Si no hay id entonces retorno un array con todas las ventanas
      ventanas[r]=this.refreshWindowObjectProperties(ventanas[r])
   return ventanas
}

function public_arrangeAllWindows(params)  {
   if (params!=null) {
      var startX=params.startX
      var startY=params.startY
      var offsetX=params.offsetX
      var offsetY=params.offsetY
      }
   this.getXDesktopObj().arrangeAllWindows(startX, startY+this.getTopMargin() ,offsetX,offsetY)
}



function public_open(params)	{
   var fParams={id:dameIdQueNoColisione(),width:300,height:200} //Valores por defecto si me pasan undefineds ...
   fParams=drWindow.getDesktopWindow().$.extend(fParams, params)  
   //Preprocesamos el par�metro "parameter" si lo hay (facilidad para la consola)
   if (fParams.parameter!=null)  {
      if (!isArray(fParams.parameter)) //caso -parameter aaa=bbb --> "aaa=bbb"
         fParams.parameter=[fParams.parameter]
      //ahora proceso cada par�metro  // -parameter aaa=bbb -parameter ccc=ddd --> ["aaa=bbb","ccc=ddd"]
      var ret=[]
      for (r=0; r<fParams.parameter.length; r++)   {
         var componentes=fParams.parameter[r].replace(/(^\s+\b)|(\b\s+$)/gi,"") 
         componentes=componentes.split(/\b\s*=\s*\b/gi)
         ret[ret.length]={name:componentes[0], value:componentes[1]}
      }
      fParams.parameters=ret
   }
   //Aplicamos lo par�metros a la url si los hay
   if (fParams.parameters!=null && fParams.parameters.length>0)  {
      if (isString(fParams.parameters))   
         fParams.parameters=JSONStringAObjeto(fParams.parameters)
      if (fParams.url.indexOf("?")==-1)
         fParams.url=fParams.url+"?"
      else
         fParams.url=fParams.url+"&"
      var noSoyPrimero=false
      for (var r=0;r<fParams.parameters.length;r++)	{
         if (noSoyPrimero)
            fParams.url=fParams.url+"&"
         noSoyPrimero=true
         if (fParams.parameters[r].nombre!=null)
            fParams.url=fParams.url+escape(fParams.parameters[r].nombre)+"="+escape(fParams.parameters[r].valor)
         else
            fParams.url=fParams.url+escape(fParams.parameters[r].name)+"="+escape(fParams.parameters[r].value)
         }
      }
   fParams.parameters=null //Ya est�n aplicados a la URL
   var skinData=this.getSkinData()
   if (fParams.innerWidth!=null && skinData.windowChromeWidth!=null) {
      fParams.width=drCalculaValorPixeles(fParams.innerWidth) + skinData.windowChromeWidth
   } else   {
      fParams.width=drCalculaValorPixeles(fParams.width)
   }
   if (fParams.innerHeight!=null && skinData.windowChromeHeight!=null) {
      fParams.height=drCalculaValorPixeles(fParams.innerHeight) + skinData.windowChromeHeight
   } else   {
      fParams.height=drCalculaValorPixeles(fParams.height)
   }
   if (!drEvaluaBooleanoString(fParams.ignoreMargins))
      fParams.height=drConsideraAltoMaximo(fParams.height)
   fParams.position=drCalculaPosicion(fParams.position,fParams.height,fParams.width,fParams.ignoreMargins)
   if (fParams.idPadre==null)
      fParams.idPadre=fParams.id
   fParams.widget=drEvaluaBooleanoString(fParams.widget)
   fParams.noTaskbar=drEvaluaBooleanoString(fParams.noTaskbar)
   fParams.noResize=drEvaluaBooleanoString(fParams.noResize)
   fParams.noDrag=drEvaluaBooleanoString(fParams.noDrag)
   fParams.noHide=drEvaluaBooleanoString(fParams.noHide)
   fParams.sessionSave=drEvaluaBooleanoString(fParams.sessionSave)
   fParams.showInAllDesktops=drEvaluaBooleanoString(fParams.showInAllDesktops)

   if (fParams.widget)  {
      fParams.noTaskbar=true
      fParams.noHide=true
      }
   if (fParams.virtualDesktop==null)  {
      var currentVD=this.getCurrentVirtualDesktop()
      if (currentVD==null)
         fParams.virtualDesktop=0
      else
         fParams.virtualDesktop=currentVD
      }
   fParams.showInAllDesktops=drEvaluaBooleanoString(fParams.showInAllDesktops)

   var virtualDesktop=fParams.virtualDesktop
   //Registro la ventana (antes de abrir para que al abrir los datos ya esten ya que se usan para la barra de tareas)
   this.getRegistry().mete(fParams);
   //Abrimos la ventana
   var parametrosParaElSkin=fParams //estos par�metros llegan hasta el skin
   win = this.getXDesktopObj().addWindow(fParams.id, fParams.title, fParams.width, fParams.height, fParams.position, fParams.skin, parametrosParaElSkin, fParams.noTaskbar, fParams.noHide, fParams.ignoreMargins, fParams.widget, fParams.zIndex, fParams.noResize, fParams.noDrag)
   this.getXDesktopObj().window.onClose(fParams.id,"DruidaDesregistraVentana(wName);var result=DruidaVerificaOnWindowClose(wName);result");
   if (fParams.widget)
      this.getXDesktopObj().url(fParams.id,fParams.url,"no");
   else
      this.getXDesktopObj().url(fParams.id,fParams.url);
   //"Retocamos" la ventana
   elIFRAME=drWindow.getNativeIframe({"id":fParams.id})
   elIFRAME.drIdVentana=fParams.id
   if (fParams.testigo!=null)	{ //Preparo el paso del testigo
		elIFRAME.testigo=fParams.testigo
		elIFRAME.onreadystatechange=function(){
			if (this.readyState=="complete"){
				var elWindow=this.contentWindow
				if (elWindow.DrDesktopTomaTestigo!=null &&  typeof elWindow.DrDesktopTomaTestigo == 'function')	{
					elWindow.DrDesktopTomaTestigo(this.testigo)
					}
				}
			}
		}
	else if (fParams.testigoJson!=null)	{ //Preparo el paso del testigo
		elIFRAME.testigoJson=fParams.testigoJson
		elIFRAME.onreadystatechange=function(){
			if (this.readyState=="complete"){
				var elWindow=this.contentWindow
				if (elWindow.DrDesktopTomaTestigo!=null &&  typeof elWindow.DrDesktopTomaTestigo == 'function')	{
					elWindow.DrDesktopTomaTestigo(JSONStringAObjeto(this.testigoJson))
					}
				}
			}
		}
   //Si hay urlAyuda guardo la informaci�n
   if (fParams.urlAyuda!=null)	
		this.getXDesktopObj().prop(fParams.id,"drUrlInfo",fParams.urlAyuda)
   this.getXDesktopObj().show(fParams.id); //MUESTRO LA VENTANA
   this.setStatus({id: fParams.id, status: fParams.status})

   //daemons puede ser un array o un string (json con el array)
   if (fParams.daemons!=null) {
      if (isString(fParams.daemons))   
         demonios=JSONStringAObjeto(fParams.daemons)
      else
         demonios=fParams.daemons
      for (var r=0; r<demonios.length; r++) {
         var datosDaemon=demonios[r]
         datosDaemon.id=fParams.id
         this.getDesktopWindow().drWindow.createDaemon(datosDaemon);
         }
      }
   //Creo la capa que tapara el iframe
   if (window.iframeDragDivMode) {
      this.getXDesktopObj().creaCapaParaIframe(fParams.id)
      this.getXDesktopObj().ocultaCapaParaIframe(fParams.id)
   }
   //Mando la ventana a su desktop virtual
   this.setVirtualDesktop({id:fParams.id, virtualDesktop:fParams.virtualDesktop});
   this.focus({id:fParams.id, noZIndexChange:true, zIndex:fParams.zIndex}) //No cambio el zIndex porque la funcion ya lo hace
   return fParams.id
   }

function public_openWindowArray(obj)   {
   arrObj=obj.windows;
   if (obj.startingSession)   {
      //Recupero el estado de ciertas variables.
      //Para asegurar que las ventanas �nicas sigan siendo �nicas tengo que modificar el array de aplicaciones. 
      for (var r=0; r<arrObj.length; r++)    {
         var esUnica=drEvaluaBooleanoString(arrObj[r].single)
         //Si es unica genero un id
         if (esUnica &&  arrObj[r].application!=null && arrObj[r].id !=null)   {
            var oAplicacion=this.getApplicationsConfig()[arrObj[r].application]
            if (oAplicacion!=null)
               oAplicacion.id=arrObj[r].id
            }
         }
      }
   for (var r=0; r<arrObj.length; r++) {
      this.open(arrObj[r]);
      }
   }

function public_getApplicationExists(args){
   if (this.getApplicationsConfig()[args.application]!=null)
      return true
   return false
}

function public_alert(args){
   drAlert(args.message)
}

function public_openApplication(obj)	{
   var sinonimos=[["aplicacion","application"],["parametros","parameters"],["motor","engine"],["unica","single"],["titulo","title"],["ancho","width"],["alto","height"],["anchoMinimo","minWidth"],["altoMinimo","minHeight"],["posicion","position"], ["urlAyuda","helpUrl"]]
   var propiedadesAVolcar=["js","id" ,"url","title","width","height","minWidth","minHeight","maxWidth","maxHeight","position","onClose","chromeType","testigo","testigoJson","helpUrl","skin","parameters","status","noTaskbar","widget","single", "daemons", "sessionSave","imageFamily", "noHide", "ignoreMargins", "zIndex","virtualDesktop","showInAllDesktops", "innerWidth", "innerHeight", "noResize","noDrag", "relativeHelpUrl","parameter"]
   obj=drAplicaSinonimos(sinonimos,obj)
   //Resuelvo el objeto oAplicacion
   var oAplicacion=this.getApplicationsConfig()[obj.application]
	if (oAplicacion==null)	{
		drAlert("La aplicaci�n identificada como <b>'"+obj.application+"'</b> no est� registrada.");
		return
		}
   oAplicacion=drAplicaSinonimos(sinonimos,oAplicacion)
   //Resuelvo el objeto Motor si lo hay
	if (oAplicacion.engine!=null)	{
		oAplicacion.url=this.getEnginesConfig()[oAplicacion.engine].url
      }
   var unica=drEvaluaBooleanoString(oAplicacion.single)
   //var unica=(oAplicacion.single==null)?"":(""+oAplicacion.single).toUpperCase()
   //Si es unica genero un id
   
   //if (oAplicacion.id==null && (unica=="S"||unica=="TRUE"||unica=="Y" ))  { 
   if (oAplicacion.id==null && unica)  { 
      oAplicacion.id=this.generateId() //Esto permanece entre llamadas sucesivas ya que modifico la aplicacion
   }
   //Vuelco en OBJ las propiedades de oAplicacion
   obj=drAplicaPropiedadesPorDefecto(propiedadesAVolcar,obj,oAplicacion)

   //Si es un javascript lo ejecuto
   if (obj.js!=null)	{
      with(this.getDesktopWindow())   {
   		eval(obj.js)
      }
      return
      }
   return this.open(obj)
   }


function drGetUrlParameters() {
   var searchStr=document.location.search
   if (searchStr.charAt(0)=='?')
      searchStr=searchStr.substr(1)
   var ret={}
   var searchStrBlocks=searchStr.split("&")
   for (var r=0; r<searchStrBlocks.length;r++)  {
      var bloque=searchStrBlocks[r]
      var componentes=bloque.split("=")
      if (componentes.length==1)
         ret[unescape(bloque)]=""
      else
         ret[unescape(componentes[0])]=unescape(componentes[1])
   }
   return ret
}

function private_getApplicationsWithHelp(params)   {
   var helpAttrName="helpUrl"
   if (params && params.locale)
      helpAttrName="helpUrl_"+params.locale
   var ret=[]
   var appsConfig=this.getApplicationsConfig()
   for (app in appsConfig)  {
      var appCfg=appsConfig[app]
      if (appCfg[helpAttrName]!=null)  {
         ret[ret.length]={description:appCfg.description, icontext:appCfg.icontext, title:appCfg.title, application:app, processedHelpUrl:appCfg[helpAttrName]}
      }
   }
   return ret
}

function private_hasHelp(args)   {
   if (args==null)
      args={}
   var params=args.params, id=args.id
   if (params!=null) {
      //if (params.locale!=null) //TO-DO: multiidioma
      return (params.helpUrl!=null)
   }
   if (id==null)
      id=this.getId()
   var windowData=this.getWindowData({id:id})
   return (windowData.helpUrl!=null)
}

function public_showHelp(params){
   if (params==null)
      params={}
   var paramsOpen={}
   var url=params.url, id=params.id,  windowId=params.windowId, locale=params.locale, globalLocale=window.globalLocale, width=params.width, height=params.height, title=params.title, position=params.position;
   if (windowId==null)
      windowId=this.getId()
   //Locale -----------
   if (locale==null)
      locale=globalLocale //que tambi�n puede ser null ...
   //me ocupo del ancho, alto y posicion -----------
   if (width!=null)
      paramsOpen.width=width
   else 
      paramsOpen.width=400;
   if (height!=null)
      paramsOpen.height=height
   else if (windowId!=null)   
      paramsOpen.height=this.getHeight({id:windowId})
   else
      paramsOpen.height=400;
   if (position!=null)
      paramsOpen.position=position
   else if (windowId!=null)   {
      var left=this.getLeft({id:windowId})+ this.getWidth({id:windowId}) + 1
      var top=this.getTop({id:windowId})
      paramsOpen.position=left+","+top
   } else
      paramsOpen.position="center,center"
   //me ocupo de la URL -----------
   var laUrl=null
   var application=null,texto="";
   if (url!=null){
      laUrl=url
   } else if (windowId!=null){ //Si hay ventana asociada ...
      //Tomo la url (en funcion tambi�n del locale)
      var windowData=this.getWindowData({id:windowId})
      if (locale!=null) {
         if (windowData["helpUrl_"+locale]!=null)
            laUrl=windowData["helpUrl_"+locale]
         else
            laUrl=windowData.helpUrl
      } else   {
         laUrl=windowData.helpUrl
      }
      if (windowData.application!=null)
         application=windowData.application
      else  {
         if (windowData.title!=null)  
            texto=windowData.title
         else if (windowData.iconText!=null)
            texto=windowData.iconText
         else if (windowData.description!=null)
            texto=windowData.description
         else 
            texto=windowData.application
      }

   }
   paramsOpen.parameters=[{name:"url", value:laUrl},{name:"application", value:application},{name:"texto", value:texto}]
   // ----------- Otros parametros
   if (id!=null)
      paramsOpen.id=id
   if (title!=null)
      paramsOpen.title=title
   else
      paramsOpen.title="Ayuda"
   //return this.open(paramsOpen);
   paramsOpen.application="helpExplorer"
   return this.openApplication(paramsOpen);
}


function public_setTitle	(params)	{
   if (params==null)
      params={}
   var id=params.id
   var titulo=params.title
   if (id==null)
      id=this.getId();
   if (titulo==null)
      titulo="";
   var objxDT=this.getXDesktopObj()
   objxDT.window.title(id, titulo);
   var skin=this.getSkin();
   var laFuncSetTitleSkin=this.getDesktopWindow()["skin_"+skin+"_setTitle"]
   if (laFuncSetTitleSkin!=null && jQuery.isFunction(laFuncSetTitleSkin))  {
      params.id=id;
      params.title=titulo
      laFuncSetTitleSkin(params)
      }
   else  {
      var oDivTit=this.getDesktopWindow().document.getElementById(id+"_wTitle")
	   if (oDivTit!=null)	
		   oDivTit.innerHTML=titulo
      }
	}

function public_getStatus(params)   {
   if (params==null)
      params={}
   if (params.id==null)
      params.id=this.getId();
   var xD=drWindow.getXDesktopObj()
   var wStat=xD.prop(params.id,"wStat")
   if (wStat=="min")
      return "minimized"
   if (wStat=="MAX")
      return "maximized"

   }

function public_setStatus(params)   {
   if (params==null)
      params={}
   if (params.id==null)
      params.id=this.getId();
   if (params.status=="maximized") {
      this.getXDesktopObj().maximizeWindow(params.id)
      return 
      }
   if (params.status=="minimized") {
      this.getXDesktopObj().minimizeWindow(params.id)
      return 
      }
   var status=this.getStatus(params)
   if (status=="minimized") {
      this.getXDesktopObj().taskbar(params.id)
      return 
      }
   }

function public_hide (params)  {
   if (params==null)
      params={}
   var id=params.id
   var xDT=this.getXDesktopObj()
   var xDTwin = drWindow.getXDesktopWindowObj()
   if (xDTwin.property(id,"wTotallyHidden")==true)
      return
   xDTwin.property(id,"wTotallyHidden",true);
   xDT.hideWindow(id)
   xDT.taskbar()
}

function public_show (params)  {
   if (params==null)
      params={}
   var id=params.id
   var xDT = this.getXDesktopObj()
   var xDTwin = drWindow.getXDesktopWindowObj()
   if (xDTwin.property(id,"wTotallyHidden")==false || xDTwin.property(id,"wTotallyHidden")==null)
      return
   xDTwin.property(id,"wTotallyHidden",false);
   if(xDTwin.property(id,"wStat")!="min")
      xDT.showWindow(id)
   xDT.taskbar()
}

function public_changeVirtualDesktop(params) {
   if (params==null)
      params={}
   var virtualDesktop=params.virtualDesktop
   if (virtualDesktop==null)
      virtualDesktop=0;
   this.getDesktopWindow().virtualDesktop=virtualDesktop //cambiamos el valor
   var xD=this.getXDesktopObj()
   var idsVentanas=this.getWindowIds()
   for (var r=0; r<idsVentanas.length; r++)	{//Repaso las ventanas. Por cada ventana, si no est� en el desktop la oculto, si no, la muestro ...
      if ( this.inVirtualDesktop({id:idsVentanas[r], virtualDesktop:virtualDesktop}) )//Ojo esto llama a getCurrentVirtualDesktop. Tenemos que haber cambiado el valor antes.
         this.show({id:idsVentanas[r]})
      else
         this.hide({id:idsVentanas[r]})
   }
}

function public_getVirtualDesktopsData() {
   //repaso las ventanas y creo una lista de desktops existentes con sus ventanas asociadas.
   var ventanas=this.getWindowIds()
   var oDesktopsIndex={}
   for(var r=0; r<ventanas.length; r++)   {
      var idVD=this.getVirtualDesktop({id:ventanas[r]})
      if (oDesktopsIndex[idVD]==null)  {
         oDesktopsIndex[idVD]={id:idVD, windowIds:[ventanas[r]]}
      } else   {
         var arrVent=oDesktopsIndex[idVD].windowIds
         arrVent[arrVent.length]=ventanas[r]
      }
   }
   oDesktops=[]
   for (var i in oDesktopsIndex) 
      oDesktops[oDesktops.length]=oDesktopsIndex[i]
   return oDesktops
}

function public_getCurrentVirtualDesktop(args) {
   var virtualDesktop=this.getDesktopWindow().virtualDesktop
   if (virtualDesktop==null)
      return 0;
   return (virtualDesktop)
}

function public_getVirtualDesktop (params) {
   if (params==null)
      params={}
   if (params.id==null)
      params.id=this.getId()
   var ret=this.getXDesktopObj().prop(params.id,'wWirtualDesktop')
   if (ret==null)
      return 0 //Por defecto estamos en la 0 siempre
   return ret;
}

function private_inVirtualDesktop(params)  {
   if (params==null)
      params={}
   if (params.id==null)
      params.id=this.getId();
   if (this.getRegistry().getVentana(params.id).showInAllDesktops) //showInAllDesktops==true? --> ret true
      return true
   if (params.virtualDesktop==null)
      params.virtualDesktop=0
   var vDVent = this.getVirtualDesktop({id:params.id});
   return (""+vDVent==""+params.virtualDesktop) //Comparo cadenas para que funcione desde consola
}

function public_setVirtualDesktop (params) {
   if (params==null)
      params={}
   var id=params.id, virtualDesktop=params.virtualDesktop
   if (id==null)
      id=this.getId();
   if (virtualDesktop==null)
      virtualDesktop=0;
   var ret=this.getXDesktopObj().prop(id,'wWirtualDesktop',virtualDesktop) //establezco ...
   //si la ventana no est� en sesi�n deber�a desaparecer.
   if ( this.inVirtualDesktop({id:id, virtualDesktop:this.getCurrentVirtualDesktop()}) )  //Ojo eso llama a getVirtualDesktop, hay que establecer el valor antes
      this.show({id:id})
   else
      this.hide({id:id})
   return ret
}

function public_close(params){
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId();
	this.getXDesktopObj().deleteWindow(id);
	}

function public_getId	()	{
	if (window.frameElement!=null && window.frameElement.drIdVentana!=null)
		return window.frameElement.drIdVentana
   var ventanaInspeccionada=window; 
   while(ventanaInspeccionada.parent!=ventanaInspeccionada) {
      ventanaInspeccionada=ventanaInspeccionada.parent
      if (ventanaInspeccionada.frameElement!=null && ventanaInspeccionada.frameElement.drIdVentana!=null)
         return ventanaInspeccionada.frameElement.drIdVentana
      }

	return null	
	}

function public_getNativeWindow(params)	{
   var Iframe=this.getNativeIframe(params)
   if (Iframe!=null)
      return Iframe.contentWindow
   }

function public_getNativeIframe(params)	{
   if (params==null)
      params={}
   if (params.id==null)
      params.id=this.getId()
   var id=params.id
   var ww=drWindow.getDesktopWindow()
   return ww.$("#xDTiF_"+id).get(0)
   //return drWindow.getDesktopWindow().document.getElementsByName('xDTiF_'+id)[0]
   }



// -----------------------------------------------------


function drAbreFichero (rutaFichero,idVentana,titulo,url,ancho,alto,posicion,testigo,elOnClose,urlAyuda,anchoMinimo,altoMinimo)	{
	/*TO-DO: parametro url*/
	var extension=drExtraeExtension(rutaFichero);
	if (drWindow.getDesktopWindow().oTipos[extension] ==null)	{
		alert("La extensi�n '"+extension+" no est� asociada a ninguna aplicaci�n.");
		return;
		}
	var idAplicacion=drWindow.getDesktopWindow().oTipos[extension].aplicacion
	var oAplicacion=drWindow.getDesktopWindow().oAplicaciones[idAplicacion]
	var oMotores=drWindow.getDesktopWindow().oMotores
	var laUrlFinal=oAplicacion.url+"?file="+escape(rutaFichero)
	//alert(laUrlFinal)
	if (oAplicacion.motor!=null)	{
		//Esto habr?a que hacerlo pasar por un punto ?nico (por el usuario, etc ?)
		laUrlFinal=oMotores[oAplicacion.motor].url
		}
	if (laUrlFinal!=null)	{
      var unica=drEvaluaBooleanoString(oAplicacion.unica)
		//if (oAplicacion.unica!=null && (oAplicacion.unica=="S"||oAplicacion.unica=="s"||oAplicacion.unica=="true") && oAplicacion.idVentana==null)
      if (unica && oAplicacion.idVentana==null)
			oAplicacion.idVentana=dameIdQueNoColisione()
		//Tomo los valores de la aplicaci?n
		ancho=(ancho==null)?oAplicacion.ancho:ancho;
		alto=(alto==null)?oAplicacion.alto:alto;	
		anchoMinimo=(anchoMinimo==null)?oAplicacion.anchoMinimo:anchoMinimo;		
		altoMinimo=(altoMinimo==null)?oAplicacion.altoMinimo:altoMinimo;			
		posicion=(posicion==null)?oAplicacion.posicion:posicion;	
		testigo=(testigo==null)?oAplicacion.testigo:testigo;
		idVentana=(idVentana==null)?oAplicacion.idVentana:idVentana;
		elOnClose=(elOnClose==null)?oAplicacion.elOnClose:elOnClose;	
		//url=(url==null)?oAplicacion.url:url;	
		titulo=(titulo==null)?oAplicacion.titulo:titulo;		
		urlAyuda=(urlAyuda==null)?oAplicacion.urlAyuda:urlAyuda;	
		
		//Aplicamos los par?metros
		if (oAplicacion.parametros!=null && oAplicacion.parametros.length>0)	{
			laUrlFinal=laUrlFinal+"?"
			for (var r=0;r<oAplicacion.parametros.length;r++)	{
				if (r>0)
					laUrlFinal=laUrlFinal+"&"
				laUrlFinal=laUrlFinal+escape(oAplicacion.parametros[r].nombre)+"="+escape(oAplicacion.parametros[r].valor)
				}
			}
			
		return drAbreVentana(idVentana,titulo,laUrlFinal,ancho,alto,posicion,testigo,elOnClose,urlAyuda,anchoMinimo,altoMinimo)	/*TO-DO: retorne el id de la ventana*/
		}
	else if (oAplicacion.js!=null)	{
		eval(oAplicacion.js)
		}
	}

// ------------------------------------------------------------
function drEvaluaBooleanoString(valor)   {
   if (valor==null)
      return false
   if (valor==true)
      return true
   else if (valor==false)
      return false
   else  {
      var cad=(valor+"").toLowerCase()
      if (cad=="true")
         return true
      else
         return false
   }
}

function drCalculaPosicion(posicion,alto,ancho,ignorarMargenes)	{
	//Si viene algo tipo center,** o **,center o center,center reconstruyo, si no lo dejo como estaba ...
	//Tambi�n modifica el "y" para que tenga en cuenta el margen superior
	// TO-DO: contemplar a�adir el margen en las otras opciones (n,s,etc)
	var xDTwin=drWindow.getXDesktopWindowObj()
	var oDesktopConfig=drWindow.getDesktopConfig()
   var ignorarMargenes=drEvaluaBooleanoString(ignorarMargenes)
   if (ignorarMargenes) {
      var marginTop=0
      var marginBottom=0
      }
   else  {
      var marginTop=drWindow.getTopMargin(); //xDTwin.marginTop(oDesktopConfig.tema)
      var marginBottom=drWindow.getBottomMargin(); // xDTwin.marginBottom(oDesktopConfig.tema)
      }
   if (posicion==null)
      return
   if (posicion=='center')
   	posicion='center,center'
   var hayCentradoHorizontal=(posicion.search( /center\s*,/ )!=-1);
   var hayCentradoVertical=(posicion.search( /,\s*center/ )!=-1);
   var hayDosPartes=(posicion.indexOf(',')!=-1)
   if (!hayDosPartes)
   	return posicion
   if (!hayCentradoHorizontal && !hayCentradoVertical)	{
   	var posX=posicion.substr(0,posicion.indexOf(','))
	   var posY=posicion.substr(posicion.indexOf(',')+1)
		if (!isNaN(parseInt(posY)))	
   		posY=parseInt(posY) + marginTop    		//Le sumo el margen superior.
      return posX+","+posY
      }
   else if (hayCentradoHorizontal && hayCentradoVertical) { 
      var posY=Math.round((drWindow.getDesktopWindow().document.body.clientHeight-alto-marginTop-marginBottom)/2 ) + marginTop
      var posX=Math.round( (drWindow.getDesktopWindow().document.body.clientWidth-ancho)/2 )
      return posX+","+posY
      }
   else if (hayCentradoHorizontal)  {
   	var posX=Math.round( (drWindow.getDesktopWindow().document.body.clientWidth-ancho)/2 )
   	var posY=posicion.substr(posicion.indexOf(',')+1)
   	if (!isNaN(parseInt(posY)))	
   		posY=parseInt(posY) + marginTop //Le sumo el margen superior.
      return posX+","+posY
      }
   else  {//hayCentradoVertical
	   var posY=Math.round((drWindow.getDesktopWindow().document.body.clientHeight-alto-marginTop-marginBottom)/2 ) + marginTop
      return posicion.replace (/center/,posY)
      }
	}


function drIncrementaYEnPosition(position,incrementoY)	{
	var xDTwin=drWindow.getXDesktopWindowObj()
	var oDesktopConfig=drWindow.getDesktopConfig()
   if (position==null)
      return
   var hayDosPartes=(position.indexOf(',')!=-1)
   if (!hayDosPartes)
   	return position
  	var posX=position.substr(0,position.indexOf(','))
	var posY=position.substr(position.indexOf(',')+1)
	if (!isNaN(parseInt(posY)))	
   	posY=parseInt(posY) + incrementoY
   return posX+","+posY
	}

function drCalculaValorPixeles(valor)	{
	var xDTwin=drWindow.getXDesktopWindowObj()
	var oDesktopConfig=drWindow.getDesktopConfig()
	//Para convertir valores como "clientWidth-200"
	if (valor==null || valor=="")
		return valor
   var valorPI=parseInt(valor)
	if (!isNaN(valorPI))
		return valorPI
	var objxDT=drWindow.getXDesktopObj()
	var clientWidth=drWindow.getDesktopWindow().document.body.clientWidth
	var clientHeight=drWindow.getDesktopWindow().document.body.clientHeight-xDTwin.marginTop(oDesktopConfig.tema)-xDTwin.marginBottom(oDesktopConfig.tema)
	eval("valor=("+valor+")")
	return valor;
	}

function drConsideraAltoMaximo(alto)	{
	var xDTwin=drWindow.getXDesktopWindowObj()
	var oDesktopConfig=drWindow.getDesktopConfig()
	var clientHeight=drWindow.getDesktopWindow().document.body.clientHeight-xDTwin.marginTop(oDesktopConfig.tema)-xDTwin.marginBottom(oDesktopConfig.tema)
	if (alto>clientHeight)
		alto=clientHeight
	return alto; 
	}



function DruidaRegistraVentana(id,idPadre)	{
	//si idPadre es null lo calcula
	if (drWindow.getDesktopWindow().oRegistroVentanas==null)	
		drWindow.getDesktopWindow().oRegistroVentanas=new clDrRegistroVentanas()
	//alert("se abre ID: "+id+ " desde ID: "+ drWindow.getId())
	if (idPadre!=null)
		drWindow.getDesktopWindow().oRegistroVentanas.mete(id, idPadre);
	else
		drWindow.getDesktopWindow().oRegistroVentanas.mete(id, drWindow.getId());	
	}

function DruidaDesregistraVentana(id)	{
	drWindow.getDesktopWindow().oRegistroVentanas.saca(id);
	return true;
	}

function DruidaVerificaOnWindowClose(id)	{
	var elWindow=drWindow.getNativeIframe({"id":id}).contentWindow
	if (elWindow.DrDesktopOnWindowClose!=null &&  typeof elWindow.DrDesktopOnWindowClose == 'function')
		return elWindow.DrDesktopOnWindowClose()
	else
		return true
	}

function cierraSesionSkin()   {
   var cambiosSkin=drWindow.getSkinParameterChanges()
   document.fcerrar.skinParameters.value=objetoAJSONString(cambiosSkin)
}

function cierraSesionVentanas(){
   var ventanas=drWindow.getWindowData();
   var ventanasASalvar=[]
   for (var r=0; r<ventanas.length; r++)  {
      if(drEvaluaBooleanoString(ventanas[r].sessionSave))
         ventanasASalvar[ventanasASalvar.length]=ventanas[r]
      }
   document.fcerrar.ventanas.value=objetoAJSONString(ventanasASalvar)

   /*
	window.ventanasSinEstado=[]
	var ventanas=DruidaVentanas();
	for (var r=0; r<ventanas.length; r++)	{
		ventanas[r].ancho = drWindow.getXDesktopObj().prop(ventanas[r].id,'wWidth')
		ventanas[r].alto = drWindow.getXDesktopObj().prop(ventanas[r].id,'wHeight')
		ventanas[r].posicion = drWindow.getXDesktopObj().prop(ventanas[r].id,'wX')+","+drWindow.getXDesktopObj().prop(ventanas[r].id,'wY') //No uso wPos porque al regenerar puede que no existan las ventanas desde las que te colocaste con un offset
		ventanas[r].url = drWindow.getXDesktopObj().prop(ventanas[r].id,'wUrl')
		ventanas[r].titulo = drWindow.getXDesktopObj().prop(ventanas[r].id,'wTitle')
		ventanas[r].urlAyuda = drWindow.getXDesktopObj().prop(ventanas[r].id,'drUrlInfo')
		ventanas[r].anchoMinimo = drWindow.getXDesktopObj().prop(ventanas[r].id,'minWidth')
		ventanas[r].altoMinimo = drWindow.getXDesktopObj().prop(ventanas[r].id,'minHeight')		
		//Pero lo necesito para llamar a su evento si lo hay ...
		var ventana=drWindow.getNativeWindow({"id":ventanas[r].id})
		if (ventana.DrDesktopDameTestigo && isFunction(ventana.DrDesktopDameTestigo) && window.objetoAJSONString!=null)	{
			var oTestigo=ventana.DrDesktopDameTestigo()
			var testigo=objetoAJSONString(oTestigo,true)
			ventanas[r].testigo=testigo
			}
		else	{
			ventanas[r].testigo=null
			ventanasSinEstado[ventanasSinEstado.length]=ventanas[r].titulo
			}
		}
	document.fcerrar.ventanas.value=objetoAJSONString(ventanas)
	if (window.ventanasSinEstado!=null && window.ventanasSinEstado.length>0)	{
		if (ventanasSinEstado.length>1)	{
			if (oDesktopConfig.animacionesActivas)
				drOscureceFondo(true)			
			//Hay 1 ventana que se guardar? como <span class="drTextoResaltado" title="Aplicaciones sin estado: Al abrir el escritorio estas ventanas se recuperar?n tal y como estaban al abrirse y sin tener en cuenta la posible evoluci�n posterior de su contenido"> aplicaci�n sin estado</span>:  Frases ingeniosas
			drAlertSistema ("Hay "+ventanasSinEstado.length+' ventanas que se guardar�n como <span class="drTextoResaltado" title="Aplicaciones sin estado: Al abrir el escritorio estas ventanas se recuperar�n tal y como estaban al abrirse y sin tener en cuenta la posible evoluci�n posterior de su contenido"> aplicaciones sin estado</span>: '+ventanasSinEstado.join (", ") )
			}
		if (ventanasSinEstado.length==1)	{
			if (window.animacionesActivas)
				drOscureceFondo(true)			
			//Hay 1 ventana que se guardar? como <span class="drTextoResaltado" title="Aplicaciones sin estado: Al abrir el escritorio estas ventanas se recuperar?n tal y como estaban al abrirse y sin tener en cuenta la posible evoluci�n posterior de su contenido"> aplicacion sin estado</span>:  Frases ingeniosas
			drAlertSistema ('Hay 1 ventana que se guardar� como <span class="drTextoResaltado" title="Aplicaciones sin estado: Al abrir el escritorio estas ventanas se recuperar�n tal y como estaban al abrirse y sin tener en cuenta la posible evoluci�n posterior de su contenido"> aplicacion sin estado</span>: '+ventanasSinEstado[0])
			}
		}	*/
	}

function DruidaSonVentanasFamilia(id1,id2)	{
	//Busco en la jerarquia para ver si son ASCENDENTES O DESCENDIENTES un de la otra (hermanos no se consideran)
	if (drWindow.getDesktopWindow().oRegistroVentanas==null)
		return false
	return drWindow.getDesktopWindow().oRegistroVentanas.sonFamilia(id1,id2)
	}

function isFunction(a)  {      
	return typeof a == 'function'; 
	}	

function DruidaVentanas()	{
	//Retornamos los objetos ventana que existen (obj de clDrRegistroVentanas) ...
	if (drWindow.getDesktopWindow().oRegistroVentanas==null)
		return []
	return drWindow.getDesktopWindow().oRegistroVentanas.getLista();
	}


//  ************************ OBJETO REGISTRO DE LOS DAEMONS
function clDrRegistroDaemons()	{
   //indizados y agrupados por idVentana tengo un array de objetos con los datos del daemon (idInterval:"")
   this.lista={}

   this.mete=function (params)   {//idVentana, idInterval, milliseconds, script
      if (this.lista[params.idVentana]==null)
         this.lista[params.idVentana]=new Array();
      var arr=this.lista[params.idVentana] //lista daemons de esa ventana
      arr[arr.length]=params
      }

   this.saca=function (params)   {//idVentana
      if (this.lista[params.idVentana]==null)
         return null
      var arr=this.lista[params.idVentana] //lista daemons de esa ventana
      this.lista=eliminaObjetoDelHash({hash:this.lista, indice:params.idVentana})
      return arr;
      }

   this.dame=function (params)   {//idVentana
      if (this.lista[params.idVentana]==null)
         return null
      var arr=this.lista[params.idVentana] //lista daemons de esa ventana
      return arr;
      }

   }

function eliminaObjetoDelHash(params)  {//{hash:xxx, indice:xxx}
   var ret={}
   for (a in params.hash)
      if (a!=params.indice)
         ret[a]=params.hash[a]
   return ret
   }

//  ************************ OBJETO REGISTRO DE LAS VENTANAS
		
function clDrRegistroVentanas()	{//clase del Objeto controlador de las ventanas que existen
	//Restricci�n: con el mismo ID solo debe existir una ventana ... TO-DO: varios
	this.lventanas=[]

	/* + mete(id) */
	//this.mete=function (id, idPadre)	{
   this.mete=function (nuevaVentana)	{
		//idpadre puede ser null
		//var nuevaVentana={"id":id, "idPadre": idPadre}
		var i=this.indiceVentana(nuevaVentana.id)
		if (i==-1)
			this.lventanas[this.lventanas.length]=nuevaVentana
		else
			this.lventanas[i]=nuevaVentana
		}

	/* + saca(id) */
	this.saca=function (id)	{
		//Tengo que arreglar los idPadre de los hijos si se da el caso
		//Por cada hijo cuyo idPadre sea el id, ya que se van a quedar huerfanos les doy el id de su "abuelo"		
		var i=this.indiceVentana(id)
		if (i==-1)
			return
		var elIdPadre=this.lventanas[i].idPadre
		for (var r=0;r<this.lventanas.length; r++)
			if (this.lventanas[r].idPadre==id)
				this.lventanas[r].idPadre=elIdPadre
		//Regenero el array 
		var nuevoArrVent=[];
		for (var r=0;r<this.lventanas.length; r++)
			if (this.lventanas[r].id!=id)
				nuevoArrVent[nuevoArrVent.length]=this.lventanas[r]
		this.lventanas=nuevoArrVent
		}
		
	/* + getVentana(id) */
	this.getVentana=function (id)	{
		var i= this.indiceVentana(id)
		if (i==-1)
			return null
		else
			return this.lventanas[i]
		}


	/* + getlista() */
	this.getLista=function ()	{
		return this.lventanas
		}

	/* + existe(id) */
	this.existe=function (id)	{
		if (this.indiceVentana(id)==-1)
			return false
		else
			return true
		}
		
	this.indiceVentana=function (id)	{
		for (var r=0;r<this.lventanas.length;r++)
			if (this.lventanas[r].id == id)
				return r
		return -1
		}
		
	this.sonFamilia=function (id1,id2)	{
		if (this.descendienteDe(id1,id2))
			return true
		if (this.descendienteDe(id2,id1))
			return true
		return false
		}
		
	this.descendienteDe=function (id1,id2)	{
		//Verifica si la ventana con id=id2 desciende de la de id=id1
		var v2=this.getVentana(id2);	
		if (v2==null)
			return false
		var unIdPadre=v2.idPadre
		while (unIdPadre!=null)	{
			if (unIdPadre==id1)
				return true
			unIdPadre=this.getVentana(unIdPadre).idPadre
			}
		return false
		}		
	}		

function drAplicaPropiedadesPorDefecto(props, obj, objDefecto)  {
   for (var r=0; r<props.length; r++)  
      if (obj[props[r]]==null)
         obj[props[r]]=objDefecto[props[r]]
   return obj
   }

function drAplicaSinonimos(props, obj)  {
   for (var r=0; r<props.length; r++){
      if (obj[props[r][1]]==null && obj[props[r][0]]!=null)   {
         obj[props[r][1]]=obj[props[r][0]]
         obj[props[r][0]]=null;
         }
      }
   return obj
   }

function drExtraeExtension(rutaCompleta)	{
	var i=rutaCompleta.lastIndexOf('.');
	if (i==-1)
		return ""
	else	
		return rutaCompleta.substr(i+1);
	}	
	
function drAlertSistema (txt)	{ //Pueden no aparecer segun la configuraci�n
	drAlert_general(txt, true)
	}
	
function drAlert (txt)	{
	drAlert_general(txt, false)
	}	
  
function drAlert_general(txt, esMensajeDelSistema)	{
	if (esMensajeDelSistema && !oDesktopConfig.mensajesDelSistema)
		return
	if (window.showModalDialog)	{
		var objDatos={	
			texto:txt
			,titulo:"Desktop Dialog Box"
			,botones:[{texto:"Aceptar", retorna:1, iconoN:"img/drIcoAceptarN.gif", iconoS:"img/drIcoAceptarS.gif"}]
			}
		abreVentanaModalDesktop(objDatos)
		}
	else
		alert(txt)
	}
	
function drCuadroConfirm(elTexto, textosBotones)	{
	if (window.showModalDialog)	{
		if (textosBotones==null)
			textosBotones=["Aceptar","Cancelar"]
		var objDatos={	
			texto:elTexto
			,titulo:"Desktop Dialog Box"
			,botones:[
				{texto:textosBotones[0], retorna:1, iconoN:"img/drIcoAceptarN.gif", iconoS:"img/drIcoAceptarS.gif"}
				,{texto:textosBotones[1], retorna:0, iconoN:"img/drIcoCancelarN.gif", iconoS:"img/drIcoCancelarS.gif"}
				]
			}
		return abreVentanaModalDesktop(objDatos)
		}
	else
		return confirm(elTexto)
	}	

function abreVentanaModalDesktop(objDatos)	{ //SOLO IE
	//return drWindow.getDesktopWindow().showModalDialog("DrDesktopModal.html",objDatos, "dialogWidth:380px;dialogHeight:190px;center:1;resizable:1;scroll:0;status:0;unadorned:1" )
   objDatos.openerWindow=window
   return drWindow.getDesktopWindow().showModalDialog("DrDesktopModal.html",objDatos, "dialogWidth:370px;dialogHeight:164px;center:1;resizable:1;scroll:0;status:0;unadorned:1" )
}

function drTraza(msg, mostrar)   {
   if (mostrar)   {
      alert(window.trazas.join("\n"))
      window.clipboardData.setData('Text'," -------DRTRAZAS:----------\n"+window.trazas.join("\n - "));
      window.trazas=[]
      window.timeout_trazas=null
      return
      }
   if (window.trazas==null)
      window.trazas=[]
   if (window.trazas.length>0 && window.trazas[window.trazas.length-1][1]==msg)
      window.trazas[window.trazas.length-1][0]++
   else
      window.trazas[window.trazas.length]=[1,msg]
   if (window.timeout_trazas!=null)
      clearTimeout(window.timeout_trazas)
   window.timeout_trazas=setTimeout("drTraza(null,true)",1500)
   }


function parseIntSecure(string)  {
   var ret=parseInt(string)
   if (isNaN(ret))
      return 0
   else
      return ret
   }

function isString(a)  {     return typeof a == 'string'; }

function isArray(a)  {
   return isObject(a) && a.constructor.toString() == Array.toString();   //Esto es para que no falle si los arrays estan en distintas ventanas
}

// ------------------- ALIAS mantenidos por compatibilidad con anteriores versiones. Tambi�n se utilizan desde alguna otra librer�a js.

window.drDesktopWin=drWindow.getDesktopWindow()
	
function drModificaTituloVentana	(idVentana, titulo)	{
   drWindow.setTitle({title:titulo,id:idVentana});
	}

function drCierraVentana(id)	{
   drWindow.close({"id":id});
	}

function dameIdQueNoColisione()	{ 
   return drWindow.generateId();
   }

function drDameMiId()	{
   return drWindow.getId();
	}

function DruidaDameMiIdVentana()	{
   return drWindow.getId();
	}

function drDameIdVentanas()	{
   return drWindow.getWindowIds()
	}

function DruidaVentanasId()	{
   return drWindow.getWindowIds()
	}	

function drAbreAplicacion(idAplicacion,idVentana,titulo,url,ancho,alto,posicion,testigo,elOnClose,urlAyuda,losParametros,anchoMinimo,altoMinimo,skin)	{
	/*TO-DO: parametro url*/
   return drWindow.openApplication({application:idAplicacion,id:idVentana,title:titulo,url:url,width:ancho, height:alto, position:posicion, testigo:testigo, elOnClose:elOnClose, urlAyuda:urlAyuda,parameters:losParametros,minWidth:anchoMinimo,minHeight:altoMinimo,skin:skin  })
	}

function drAbreVentana(id,titulo,url,ancho,alto,posicion,testigo,elOnClose,urlAyuda,anchoMinimo,altoMinimo,skin)	{
   return drWindow.open({id:id,title:titulo,url:url,width:ancho,height:alto,position:posicion,testigo:testigo,elOnClose:elOnClose,urlAyuda:urlAyuda,minWidth:anchoMinimo,minHeight:altoMinimo,skin:skin}  )
	}

function DruidaAbreVentana(id,titulo,url,ancho,alto,posicion,testigo,idPadre,elOnClose,urlAyuda,anchoMinimo,altoMinimo,skin) {
   return drWindow.open({id:id,title:titulo,url:url,width:ancho,height:alto,position:posicion,testigoJson:testigo,idPadre:idPadre,elOnClose:elOnClose,urlAyuda:urlAyuda,minWidth:anchoMinimo,minHeight:altoMinimo,skin:skin}  )
	}

function DruidaDameWindowVentana(id)	{
   return drWindow.getNativeWindow({"id":id})
	}
	
function DruidaDameIframeVentana(id)	{
   return drWindow.getNativeIframe({"id":id});
	}	

// ---------------- ALIAS que llaman a otros ALIAS

function DruidaRegeneraVentana(id,titulo,url,ancho,alto,posicion,testigo,idPadre,urlAyuda,anchoMinimo,altoMinimo)	{
	//Utilizada por el desktop para generar las ventanas al iniciar
	//Funci?n creada para mejorar el futuro desarrollo / mantenimiento
	DruidaAbreVentana(id,titulo,url,ancho,alto,posicion,testigo,idPadre,null,urlAyuda,anchoMinimo,altoMinimo)
	}

function drAbreAplicacionObj(oInfo){
	drAbreAplicacion(oInfo.idAplicacion,oInfo.idVentana,oInfo.titulo,oInfo.url,oInfo.ancho,oInfo.alto,oInfo.posicion,oInfo.testigo,oInfo.elOnClose,oInfo.urlAyuda,oInfo.parametros,oInfo.anchoMinimo,oInfo.altoMinimo,oInfo.skin)
	}