var conDwrConnectionTimeout=15000;

$(function()   {	//inicio
   //userHasFileinfoTest()
   procesaParametrosUrl()
   $("#contenedorGeneral").css({visibility:"visible"}) //Mejora la "experiencia" haciendo que aparezca todo de golpe
   inicializaListaFicheros();
   inicializaBotonera();
   actualizaListaFicheros();
   inicializaRestoDeComponentes()
   preparaLayout();
})

/*function userHasFileinfoTest()   {
   userData=drWindow.getUserData()
}*/

function procesaParametrosUrl()  {
   window.UrlParams=drGetUrlParameters()
}

function inicializaRestoDeComponentes()   {
   $("#btnRecargar").bind("mousedown",actualizaListaFicheros);
}

function preparaLayout()   {
   $(window).bind("resize",recolocaLayout)
   recolocaLayout()
}

function recolocaLayout(ev)  {
   setTimeout(recolocaLayout_timeout,0)
}
function recolocaLayout_timeout()  {
   var availW=$(window).width()
   var availH=$(window).height() - 49 //ajustar si se cambia el dise�o
   $("#lista").width(availW).height(availH)
   $("#rutaDirectorio").scrollLeft(10000)
}

// ---- historial de navegaci�n 

function computaHistorial(ruta)  {
   if (window.histNav==null)
      window.histNav={rutas:[], indice:0}
   var indiceNuevaRuta=histNav.rutas.length + histNav.indice
   if (indiceNuevaRuta>0 && histNav.rutas[indiceNuevaRuta-1]==ruta)   {
      return
   }
   if (indiceNuevaRuta<histNav.rutas.length) { //Navego despues de haber retrocedido -> elimino historial
      var nuevoRutas=[]
      for(var r=0; r<indiceNuevaRuta; r++)
         nuevoRutas[r]=histNav.rutas[r]
      histNav.rutas=nuevoRutas;
      histNav.indice=0;
   }
   histNav.rutas[indiceNuevaRuta]=ruta
}

function historialAtras()  {
   if (window.histNav==null)
      return
   if (histNav.indice + histNav.rutas.length <= 1)
      return
   histNav.indice--
   var indiceRutaADevolver=histNav.rutas.length + histNav.indice - 1
   return histNav.rutas[indiceRutaADevolver]
}

function historialAdelante()  {
   if ( histNav.indice>=0)
      return
   var indiceRutaADevolver=histNav.rutas.length + histNav.indice
   histNav.indice++
   return histNav.rutas[indiceRutaADevolver]
}

function navegarAtras() {
	if (comunicando())   {
      msgErrorComunicaciones();
		return 
   }
   var ruta=historialAtras()
   if (ruta!=null)
      navegarADirectorio(ruta)
}

function navegarAdelante() {
	if (comunicando())   {
      msgErrorComunicaciones();
		return 
   }
   var ruta=historialAdelante()
   if (ruta!=null)
      navegarADirectorio(ruta)
}

/*Cuadro de ruta de directorios --------------------- */
function actualizaRutaDirectorios(relativePath) {
   computaHistorial(relativePath)
   window.relativePath=relativePath
   var indChain=0;
   var dirs=relativePath.match(/[^\\\/]+/g)
   if (dirs==null)
      dirs=[]
   var htmlSepDir="<img class=\"sepDirectorios\" src=\"img/icoSepDirectorio.gif\">"
   html=[]
   if (dirs.length==0)
      html[html.length]=drResources.get({bundle:"drExplorer",key:"drExplorer.txt11"})+htmlSepDir
   else  {
      var id="dr_path_chain_"+(indChain)
      indChain++
      html[html.length]="<span id=\""+id+"\" class=\"subdirectorio\">"+drResources.get({bundle:"drExplorer",key:"drExplorer.txt11"})+"</span>"+htmlSepDir
      $(window).data(id,"")
      }
   var rutaRelativaCreciente=""
   for (var r=0; r<dirs.length-1; r++)   {
      var id="dr_path_chain_"+(indChain)
      indChain++
      html[html.length]="<span id=\""+id+"\" class=\"subdirectorio\">"+dirs[r]+"</span>"+htmlSepDir
      if (rutaRelativaCreciente.length>0)
         rutaRelativaCreciente=rutaRelativaCreciente+"\\"+dirs[r]
      else
         rutaRelativaCreciente=dirs[r];
      $(window).data(id,rutaRelativaCreciente)
   }
   if (dirs.length>0){
      html[html.length]=dirs[dirs.length-1]+htmlSepDir
   }
   $("#rutaDirectorio").html(html.join(''))
   $("#rutaDirectorio span.subdirectorio").bind("mouseover",function (e)  {
      $(e.target).addClass("subdirectorio_rollover")
   }).bind("mouseout",function (e)  {
      $(e.target).removeClass("subdirectorio_rollover")
   }).bind("mousedown",function (e)  {
      navegarADirectorio($(window).data($(e.target).attr("id")));
   })
   //Ahora lo coloco "al final"
   $("#rutaDirectorio").scrollLeft(10000)
}

function estoyEnDirectorioRaiz() {
   if (window.relativePath=="")
      return true
}

/*Botonera ------------------------ */

function inicializaBotonera() {
   var oIconosIzq={
      claseBtn: "btnIcono"
      ,claseBtnRollOver:"btnIconoRollover"
      ,claseBtnMarcado:"btnIconoMarcado"
      ,claseListaValores:"listaValores"
      ,claseBotonera:""
      ,imgFondo:null
      ,noEstirar:true
      ,altura:26
      ,botones: [
         {img:"img/btnSubirDir.gif", js:"btnSubirDirectorio()", tooltip:drResources.get({bundle:"drExplorer",key:"drExplorer.txt36"})}
         ,{img:"img/btnAtras.gif", js:"navegarAtras()", tooltip:drResources.get({bundle:"drExplorer",key:"drExplorer.txt37"})}
         ,{img:"img/btnAdelante.gif", js:"navegarAdelante()", tooltip:drResources.get({bundle:"drExplorer",key:"drExplorer.txt38"})}
      ]
   }
   drGeneraBotoneraIconos("botoneraIzquierda", oIconosIzq)
}


function btnSubirDirectorio() {
   if (!estoyEnDirectorioRaiz())
      navegarASubdirectorio('..')
}




function iniciaBloqueoComunicaciones() {$("#loadingMsg").css({display:"inline"}); window.dr_comunicando=true }
function comunicando(){return (window.dr_comunicando==true)}
function terminaBloqueoComunicaciones() {$("#loadingMsg").css({display:"none"}); window.dr_comunicando=false }
function msgErrorComunicaciones() { alert(drResources.get({bundle:"drExplorer",key:"drExplorer.txt39"})) }


/* Operaciones con los ficheros ----------------------- */


window.relativePath=""
window.clipBoard={sourcepath:null, sourceRows:null, cut:false,indexedNames:null, names:null}

// ------------------ CUT COPY PASTE
function elementoEstaCopiado() {
   return false
}
function elementoEstaCortado() {
   return false
}

// navegar ---

function navegarASubdirectorio(subDir) {//nombre de un subdirectorio sin el "/" delante ni detr�s
   window.relativePath=window.relativePath+"/"+subDir
   actualizaListaFicheros()
}

function navegarADirectorio(dir) {//nombre de un directorio sin el "/" delante ni detr�s
   window.relativePath=dir
   actualizaListaFicheros()
}

function actualizaListaFicheros()	{
	if (comunicando())   {
      msgErrorComunicaciones();
		return 
   }
	iniciaBloqueoComunicaciones()
	DrExplorerService.getDirectory(window.relativePath,{ async:true 
      ,callback:actualizaListaFicheros_callback
      ,timeout:conDwrConnectionTimeout
      ,errorHandler:function(msg){
      	alert(msg);
         actualizaRutaDirectorios("");
         actualizaListaFicherosCon([]);
         terminaBloqueoComunicaciones();
      }
   })
}
function actualizaListaFicheros_callback (obj)	{
	if (obj==null)
		return //sustituir por limpiar la lista
	else if (obj.errorDeOperacion)	{
		alert(drResources.get({bundle:"drExplorer",key:"drExplorer.txt40"})+" '"+obj.excepcionError.message+"'")
      terminaBloqueoComunicaciones();
      return
	}
   actualizaRutaDirectorios(obj.relativePath)
   actualizaListaFicherosCon(obj.files)
	terminaBloqueoComunicaciones()
}

// --------- (operacionFicheros_callback)
 
function operacionFicheros_callback(obj)  {
   if (obj.errorDeOperacion)	{
		alert(drResources.get({bundle:"drExplorer",key:"drExplorer.txt41"})+" '"+obj.excepcionError.message+"'")
	} else   {
      if (obj.relativePath!=null)
         actualizaRutaDirectorios(obj.relativePath)
      if (obj.files!=null)
         actualizaListaFicherosCon(obj.files)
   }
	terminaBloqueoComunicaciones()
}

function inicializaListaFicheros()  {
}

function actualizaListaFicherosCon (data){
   window.elementReference={}
   if (!estoyEnDirectorioRaiz()){
      var nuevoData=[{name:"..", directory:true}]
      for (var r=0; r<data.length; r++) 
         nuevoData[nuevoData.length]=data[r]
      data=nuevoData
   }  
   for (var r=0; r<data.length; r++)   { //creo las referencias
      var elem= data[r]
      elem.elementId="element_"+r
      elementReference[elem.elementId]=elem
   }
   var html=[]
   for (var r=0; r<data.length; r++)   { //Directorios primero
      var elem= data[r]
      if (elem.directory && !elem.hidden)  {
         html[html.length]=getHtmlDirectoryElement(elem)
      }
   }

   //var isImageRE=/(\.jpg|\.png|\.gif)$/gi
   for (var r=0; r<data.length; r++)   {
      var elem= data[r]
      if (!elem.directory  && !elem.hidden)  {
         var indPto=elem.name.lastIndexOf(".")
         if (indPto!=-1){
            var ext=elem.name.substr(indPto+1).toLowerCase()
            if (ext=="jpg" || ext=="gif" || ext=="png")
            //if (isImageRE.test(elem.name))
               html[html.length]=getHtmlImageElement(elem)
         }
      }
   }
   $("#lista").html(html.join(''))
   $(".directoryContainer , .imgContainer").hover(function(){
      $(this).addClass("imgHover")
   },function(){
      $(this).removeClass("imgHover")
   })
   $(".directoryContainer").bind("mousedown",function(){
      var elem=window.elementReference[$(this).attr("id")]
      navegarASubdirectorio(elem.name)
   })
   $(".imgContainer").bind("mousedown",function(){
      var elem=window.elementReference[$(this).attr("id")]
      var file=window.relativePath +"/"+ elem.name
      var dataReturned={inputId:UrlParams.inputId,windowId:UrlParams.windowId, file:file}
      drEvents.trigger({type:"onDesktopFileSelect",data:dataReturned})
      drWindow.close()
   })
}

function getHtmlDirectoryElement(elem){
   var html=[]
   var txtEscapado=DrEscHTML(elem.name)
   html[html.length]="<div id=\""+elem.elementId+"\" class=\"directoryContainer\" title=\""+txtEscapado+"\">"
   html[html.length]=   "<div class=\"folderIcon\"></div>"
   html[html.length]=   "<div class=\"imgLabel\" >"+txtEscapado+"</div>"
   html[html.length]="</div>";
   return html.join('')
}

function getHtmlImageElement(elem){
   var html=[]
   var txtEscapado=DrEscHTML(elem.name)
   var useThumbnails=false //TO-DO
   var primerCaracter=window.relativePath.charAt(0)
   var imgSrc;
   if (primerCaracter=='\\' || primerCaracter=='/')
      imgSrc= drWindow.getDesktopProperties()["urlUserRoot"] + "/" + window.relativePath.substr(1) +"/"+ elem.name
   else
      imgSrc= drWindow.getDesktopProperties()["urlUserRoot"] + "/" + window.relativePath +"/"+ elem.name
   html[html.length]="<div id=\""+elem.elementId+"\" class=\"imgContainer\" title=\""+txtEscapado+"\">"
   html[html.length]=   "<table class=\"imgCanvasContainer\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"
   html[html.length]=      "<tr>"
   html[html.length]=         "<td valign=\"middle\" align=\"center\">"
   html[html.length]=            "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"
   html[html.length]=               "<tr>"
   html[html.length]=                  "<td class=\"imgCanvas11\"></td>"
   html[html.length]=                  "<td class=\"imgCanvas12\"></td>"
   html[html.length]=                  "<td class=\"imgCanvas13\"></td>"
   html[html.length]=               "</tr>"
   html[html.length]=               "<tr>"
   html[html.length]=                  "<td class=\"imgCanvas21\"></td>"
   if (useThumbnails)
      html[html.length]=                  "<td><img src=\""+imgSrc+"?thumbnail=true\" ></td>"
   else
      html[html.length]=                  "<td><img src=\""+imgSrc+"\" width=\"94\" height=\"90\"></td>"
   html[html.length]=                  "<td class=\"imgCanvas23\"  valign=\"top\" ><div class=\"imgCanvas23b\"></div></td>"
   html[html.length]=               "</tr>"
   html[html.length]=               "<tr>"
   html[html.length]=                  "<td class=\"imgCanvas31\"></td>"
   html[html.length]=                  "<td class=\"imgCanvas32\" align=\"left\"><div class=\"imgCanvas32b\"></div></td>"
   html[html.length]=                  "<td class=\"imgCanvas33\"></td>"
   html[html.length]=               "</tr>"
   html[html.length]=            "</table>"
   html[html.length]=         "</td>"
   html[html.length]=      "</tr>"
   html[html.length]=   "</table>"
   html[html.length]=   "<div class=\"imgLabel\" >"+txtEscapado+"</div>"
   html[html.length]="</div>"
   return html.join('')
}

function DrEscHTML (cad) {
   if (cad!=null)
      return cad.replace(/&/gi,"&amp;").replace(/</gi,"&lt;").replace(/"/gi,"&quot;"); 
   return "";
}