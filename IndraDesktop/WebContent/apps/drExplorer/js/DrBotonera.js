
   /*
   PAGINA DE EJEMPLO: 
   <SCRIPT SRC="js/DrBotononera.js" TYPE="text/javascript"></SCRIPT>
   <style>
   .btnIcono{cursor:hand;margin:1px;border:none}
   .btnIconoRollover{cursor:hand;border:1px solid white;margin:0px;border-right:1px solid #808080;border-bottom:1px solid #808080}
   .btnIconoMarcado{cursor:hand;border:1px solid #808080;margin:0px;border-right:1px solid white;border-bottom:1px solid white}
   </style>
   <script>
      function elOnload()  {
         var oIconos={
            claseBtn: "btnIcono"
            ,claseBtnRollOver:"btnIconoRollover"
            ,claseBtnMarcado:"btnIconoMarcado"
            ,imgFondo:"img/fondoIconos.gif"
            ,altura:26
            ,botones: [
               {img:"img/icoGuardar.gif", js:"alert(1)", seleccionable:false}
               ,{img:"img/icoBuscar.gif", js:"alert(2)", seleccionable:false}
               ,{}
               ,{img:"img/icoWrap.gif", js:"alert(this.pulsado)", seleccionable:true}
               ]
            }
         drGeneraBotoneraIconos("contenedor", oIconos)   
         }
   </script>
   <body onload="elOnload()">
   <div id="contenedor"></div>
   */

   function drGeneraBotoneraIconos(idCont, oIconos)   {
      var oContenedor=document.getElementById(idCont)
      oContenedor.innerHTML=drGeneraBotoneraIconos_generaHTML(idCont,oIconos)
      oContenedor.dBotones=oIconos.botones;
      }

   function drGeneraBotoneraIconos_generaHTML(idCont,oIconos) {
      html=[]
      html[html.length]="<table " + (oIconos.claseBotonera!=null?"class=\""+oIconos.claseBotonera+"\" ":" style=\"border-right:1px solid #808080\" ")  + (!oIconos.noEstirar?"width=\"100%\" ":"") + "height=\""+oIconos.altura+"\"  border=\"0\" cellpadding=\"0\" cellspacing=\"0\" background=\""+oIconos.imgFondo+"\">";
      html[html.length]="  <tr>";
      html[html.length]="	 <td><img style=\"visibility:hidden;width:2px\"></td>";
      for (var v=0;v<oIconos.botones.length; v++)  {
         var btn=oIconos.botones[v]
         if (btn.img!=null || btn.valores!=null) {
            var javascr="";
            var id="";
            if (btn.id)
               id=" ID=\""+btn.id+"\" "
            if (btn.js!=null) {
               javascr="eval(document.getElementById('"+idCont+"').dBotones["+v+"].js);"
               }
            if (btn.seleccionable)  {
               html[html.length]="<td><img "+id+" class=\""+oIconos.claseBtn+"\" src=\""+btn.img+"\" onmousedown=\"onmousedown_btn(this,'S')\" onmouseup=\""+javascr+"\" onmouseover=\"onmouseover_btn(this,'S')\"  onmouseout=\"onmouseout_btn(this,'S')\"></td>";
               //seleccionable=\"S\" no lo coge firefox
               }
            else if (btn.valores)  {
               if (btn.img!=null) {
                  html[html.length]="<td style='padding-left:4px;'>"
                  html[html.length]="<img class=\""+oIconos.claseBtn+"\" src=\""+btn.img+"\" align=\"absmiddle\">";
                  html[html.length]="</td><td style='padding-right:4px;'>"
                  }
               else  {
                  html[html.length]="<td style='padding-right:4px; padding-left:4px;'>"
                  }
               html[html.length]="<select onchange=\"this.valor=this.value;"+javascr+"\" "+id+" class=\""+oIconos.claseListaValores+"\">"
               for (var r=0; r<btn.valores.length; r++)   {
                  html[html.length]="<option value='"+btn.valores[r].valor+"'>"+btn.valores[r].descripcion+"</option>"
                  }
               html[html.length]="</select></td>"
               }
            else  {
               html[html.length]="<td><img "+id+" "+ (btn.tooltip!=null?"title=\""+btn.tooltip+"\" ":"")  + " class=\""+oIconos.claseBtn+"\" src=\""+btn.img+"\" onmousedown=\"onmousedown_btn(this)\" onmouseup=\""+javascr+"onmouseup_btn(this)\"  onmouseover=\"onmouseover_btn(this)\"  onmouseout=\"onmouseout_btn(this)\"></td>";
               }
            }
         else
            html[html.length]="    <td><img src=\"img/barraSep.gif\"></td>";
         }
      if (!oIconos.noEstirar)
         html[html.length]="    <td width=9000>&nbsp;</td>";
      html[html.length]="  </tr>";
      html[html.length]="</table>";
      return html.join('')
      }

   function onmouseover_btn(oImg, seleccionable)	{
      if (!oImg.pulsado)	{
         oImg.className="btnIconoRollover"
         }
      }
   function onmouseout_btn(oImg, seleccionable)	{
      if (!oImg.pulsado)	{
         oImg.className="btnIcono"
         }
      }	
      
   function onmousedown_btn(oImg, seleccionable)	{
      if (seleccionable=='S')	{
         if (oImg.pulsado)	{
            oImg["pulsado"]=null
            onmouseover_btn(oImg)
            }
         else	{
            oImg["pulsado"]=true
            oImg.className="btnIconoMarcado"
            }
         }
      else	{
         oImg.className="btnIconoMarcado"
         }
      }	

   function onmouseup_btn(oImg)	{
      oImg.className="btnIconoRollover"
      }
