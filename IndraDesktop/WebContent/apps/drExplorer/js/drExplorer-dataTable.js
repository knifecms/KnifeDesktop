/*data tble D:\ECLIPSE\WORK_SPACES\CALLISTO\DruidaDesktopV2\WebContent\apps\drExplorer\dev\yui_2.6.0\yui\examples\datatable\index.html*/

function tamanyoDeseadoLista()   {
   var availW=$(window).width()-2
   var availH=$(window).height()- 74 //ajustar si se cambia el dise�o
   return {"wpx":availW+"px" , "hpx":availH+"px", w:availW, h:availH}
}

function inicializaListaFicheros()  {
   var tam=tamanyoDeseadoLista()
   window.myColumnDefs = [
      {key:"name", minWidth:40, width:270, label: drResources.get({bundle:"drExplorer",key:"drExplorer.txt1"}) , sortable:true, resizeable:true, sortOptions:{sortFunction:sortByFilename}, formatter:nameFileFormatter},
      {key:"length", label: drResources.get({bundle:"drExplorer",key:"drExplorer.txt2"}) , sortable:true, resizeable:true, formatter:lengthFileFormatter},
      {key:"attribs", label:drResources.get({bundle:"drExplorer",key:"drExplorer.txt3"}), sortable:true, resizeable:true},
      {key:"lastModifiedDate", label:drResources.get({bundle:"drExplorer",key:"drExplorer.txt4"}),formatter:dateFileFormatter , sortable:true, sortOptions:{sortFunction:sortByDate},resizeable:true}/*formatter:YAHOO.widget.DataTable.formatDate, sortOptions:{defaultDir:YAHOO.widget.DataTable.CLASS_DESC}*/
   ];
   var myDataSource = new YAHOO.util.DataSource([]);
   myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
   myDataSource.responseSchema = { fields: ["name","attribs","lastModifiedDate"] };
   window.myDataTable = new YAHOO.widget.DataTable("lista", myColumnDefs, myDataSource, {sortedBy:{key:"name"}, MSG_EMPTY:" ", initialLoad:false, draggableColumns:true, scrollable:true, width:tam.wpx, height:tam.hpx});//
   // Subscribe to events for row selection   
   window.myDataTable.subscribe("rowMouseoverEvent", myDataTable.onEventHighlightRow);
   window.myDataTable.subscribe("rowMouseoutEvent", myDataTable.onEventUnhighlightRow);
   window.myDataTable.subscribe("rowSelectEvent", myDataTable.clearTextSelection );
   window.myDataTable.subscribe("theadCellDblclickEvent", myDataTable.clearTextSelection );
   window.myDataTable.subscribe("theadCellClickEvent", myDataTable.clearTextSelection );
   //window.myDataTable.subscribe("rowClickEvent", myDataTable.onEventSelectRow);

   window.myDataTable.subscribe("rowMousedownEvent", myDataTable_rowMousedownEvent);
   
   window.myDataTable.subscribe("rowDblclickEvent", onRowDblclickEvent);
   //window.myDataTable.set("MSG_EMPTY", " ") //msg cuando no hay ficheros
   window.myDataTable.render();
   window.myDataTable.focus();
}

function myDataTable_rowMousedownEvent(ev){
   myDataTable.onEventSelectRow(ev);
   window.setTimeout("myDataTable.clearTextSelection()",0);
}

/* Acctiones de navegaci�n ----------------------- */

function onRowDblclickEvent(oArgs) {
   window.myDataTable.clearTextSelection();
   var elRecord=window.myDataTable.getRecord(oArgs.target)
   var nombre=elRecord.getData("name")
   var esDir=elRecord.getData("directory")
   if (esDir)  {
      navegarASubdirectorio(nombre)
   } else   { //Fichero
      //En el futuro si tiene ciertas extensiones lo abro con el engine adecuado
      //downloadlastSelectedFile();
      openLastSelectedFile();
   }
}


/* metodos para actualizar din�micamente la lista :  ------------------------------------ */

function deseleccionarTodoListaFicheros() {
   window.myDataTable.unselectAllRows()
}

function recargarListaFicheros() {
   window.myDataTable.render()
}

function actualizaListaFicherosCon (datos)  {   //Cambiar los datos de la tabla
   datos=preprocesaDatos(datos)
   window.myDataTable.initializeTable();
   window.myDataTable.addRows(datos)
   window.myDataTable.sortColumn( window.myDataTable.getColumn ("name") )
}
   
function preprocesaDatos(datos) {
   var ret=[];
   for(var r=0; r<datos.length; r++)   {
      datos[r].attribs= (datos[r].directory?"d":"") + (datos[r].hidden?"H":"") + (datos[r].canRead?"R":"") + (datos[r].canWrite?"W":"")
      datos[r].lastModifiedDate= new Date(datos[r].lastModified)
      if (datos[r].name!="CVS") //hacer gen�rico por configuraci�n (TO-DO)
         ret[ret.length]=datos[r]
   }
   //datos[datos.length]={directory:true, name:"..",  lastModified:new Date()}
   return ret;
}
   

/*M�todos para formatear y ordenar la lista : ------------------------------------ */

function dateFileFormatter(elCell, oRecord, oColumn, oData) {
   date=oRecord.getData("lastModifiedDate")
   if (date!=null)
      elCell.innerHTML =dateFormat(date, 'dd/mm/yy HH:MM')
}

function nameFileFormatter(elCell, oRecord, oColumn, oData) {
   if (oData==null)
      return
   var htmlEstilo="",elInnerHTML=[]
   

   /*
   elInnerHTML[elInnerHTML.length]="<div class=\"nameCellContainer\">"
   if (elementoEstaCortado(oRecord))
      elInnerHTML[elInnerHTML.length]="<img src=\"img/cutMark.gif\" class=\"cutMark\">"
   if (elementoEstaCopiado(oRecord))
      elInnerHTML[elInnerHTML.length]="<img src=\"img/copyMark.gif\" class=\"copyMark\">"
   if (oRecord.getData("hidden"))
      htmlEstilo=' style="-moz-opacity:0.5;filter:alpha(opacity: 50);opacity:0.5;" '
   if (oRecord.getData("directory"))
      elInnerHTML[elInnerHTML.length]="<img src=\"img/icoDirectory.gif\" "+htmlEstilo+" align=\"absmiddle\">"+oData;
   else
      elInnerHTML[elInnerHTML.length]="<img src=\"img/icoFile.gif\" "+htmlEstilo+" align=\"absmiddle\">"+oData;
   elInnerHTML[elInnerHTML.length]="</div>"

   */
   //<div class="xxx" style="background-image: url(../img/icoDirectory.gif);"><img src="../img/cutMark.gif" width="10" height="16" align="absmiddle">Hola que tal</div>

   if (oRecord.getData("directory"))
      elInnerHTML[elInnerHTML.length]="<div class=\"xxx\" style=\"background-image: url(img/icoDirectory.gif);\">"
   else 
      elInnerHTML[elInnerHTML.length]="<div class=\"xxx\" style=\"background-image: url(img/icoFile.gif)\">"
   if (elementoEstaCortado(oRecord))
      elInnerHTML[elInnerHTML.length]="<img src=\"img/cutMark.gif\" align=\"absmiddle\">"+oData
   else if (elementoEstaCopiado(oRecord))
      elInnerHTML[elInnerHTML.length]="<img src=\"img/copyMark.gif\" align=\"absmiddle\">"+oData
   else {
      elInnerHTML[elInnerHTML.length]="<img src=\"img/noMark.gif\" align=\"absmiddle\">"+oData
   }
   elInnerHTML[elInnerHTML.length]="</div>"

   elCell.innerHTML=elInnerHTML.join('')
}

function lengthFileFormatter(elCell, oRecord, oColumn, oData) {
   var bytes=oRecord.getData("length")
   var valor
   if (bytes==null || oRecord.getData("directory"))
      valor ="-"
   else if (bytes>=1073741824)  {
      valor=Math.ceil(bytes/1073741824)+" GB";
   }
   else if (bytes>=1048576)  {
      valor=Math.ceil(bytes/1048576)+" MB";
   }
   else if (bytes>=1024)  {
      valor=Math.ceil(bytes/1024)+" KB";
   }else {
      valor=bytes+" bytes";
   }
   elCell.innerHTML =valor
}

function sortByFilename (a, b, desc) { //Ponemos los directorios separados
    if(!YAHOO.lang.isValue(a))   
        return (!YAHOO.lang.isValue(b)) ? 0 : 1;   
    else if(!YAHOO.lang.isValue(b)) 
        return -1;   
    // First compare by directory   
    var compState = YAHOO.util.Sort.compare(!a.getData("directory"), !b.getData("directory"),desc);   
    // If directories are equal, then compare by name   
    if (compState!=0)
      return compState
    else {
      if (a.getData("name")-b.getData("name")>0)
         return 1
      else
         return 0
    }
}

function sortByDate (a, b, desc) { //Ponemos los directorios separados
    if(!YAHOO.lang.isValue(a))   
        return (!YAHOO.lang.isValue(b)) ? 0 : 1;   
    else if(!YAHOO.lang.isValue(b)) 
        return -1;   
    // First compare by directory   
    var comp = YAHOO.util.Sort.compare;   
    var compState = comp(!a.getData("directory"), !b.getData("directory"),desc);   
    // If directories are equal, then compare by name   

    return (compState !== 0) ? compState : comp(a.getData("lastModifiedDate"), b.getData("lastModifiedDate"),desc);   
}
   
/*
 * Date Format 1.2.2
 * (c) 2007-2008 Steven Levithan <stevenlevithan.com>
 * MIT license
 * Includes enhancements by Scott Trenda <scott.trenda.net> and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && (typeof date == "string" || date instanceof String) && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date();
		if (isNaN(date)) throw new SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab",
		"Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};