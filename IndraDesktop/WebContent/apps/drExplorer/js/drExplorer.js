var conDwrConnectionTimeout=15000;

$(function()   {	//inicio
   //procesaParametrosUrl()
   $("#contenedorGeneral").css({visibility:"visible"}) //Mejora la "experiencia" haciendo que aparezca todo de golpe
   inicializaListaFicheros();
   inicializaBotonera();
   actualizaListaFicheros();
   inicializaRestoDeComponentes()
   preparaLayout();
   inicializaBotonUploadReal();
   inicializaMenuDerecho();
})

/*
function procesaParametrosUrl()  {
   var params=drGetUrlParameters()
   if (params && params.mode=="imageSelector"){
      alert("imageSelector!!")
   }
}*/

function inicializaMenuDerecho () {
   var opcMenuDesktop=[
      {"txt":drResources.get({bundle:"drExplorer",key:"drExplorer.txt5"}), "js":"copiarElementosSeleccionados()"}
      ,{"txt":drResources.get({bundle:"drExplorer",key:"drExplorer.txt6"}), "js":"cortarElementosSeleccionados()"}   
      ,{"txt":drResources.get({bundle:"drExplorer",key:"drExplorer.txt7"}), "js":"pegarElementos()"} 
      ,{}
      ,{"txt":drResources.get({bundle:"drExplorer",key:"drExplorer.txt8"}), "js":"$(document).trigger('click');crearNuevaCarpeta()"} 
      ,{}
      ,{"txt":drResources.get({bundle:"drExplorer",key:"drExplorer.txt9"}), "js":"$(document).trigger('click');eliminarElementosSeleccionados()"} 
      ,{"txt":drResources.get({bundle:"drExplorer",key:"drExplorer.txt10"}), "js":"$(document).trigger('click');renameSelectedElements()"}
      ]
   asociaMenuDerecho(document.getElementById("lista"), opcMenuDesktop)
   //asociaMenuDerecho(document, opcMenuDesktop)
}

function inicializaRestoDeComponentes()   {
   $("#btnRecargar").bind("mousedown",actualizaListaFicheros);
}

function preparaLayout()   {
   $(window).bind("resize",recolocaLayout)
   recolocaLayout()
}

function recolocaLayout(ev)  {
   setTimeout(recolocaLayout_timeout,0)
}
function recolocaLayout_timeout()  {
   //Lista: ------------
   var tam=tamanyoDeseadoLista()
   window.myDataTable.set("width",tam.wpx);
   window.myDataTable.set("height",tam.hpx);
   $("#lista .yui-dt-hd").width(tam.w)
   $("#lista .yui-dt-bd").width(tam.w)
   $("#lista .yui-dt-bd").height(tam.h)
   window.myDataTable._syncScroll()
   //path -------------
   $("#rutaDirectorio").scrollLeft(10000)
}

function actualizaListaFicheros_TAPON()   {
    actualizaListaFicherosCon([{"canRead":true,"canWrite":true,"directory":true,"hidden":false,"lastModified":1224502162406,"name":"dir1","relativePath":"\\ECLIPSE\\WORK_SPACES\\CALLISTO\\DruidaDesktopV2\\scripts\\tmp\\usuario\\dir1"},{"canRead":true,"canWrite":true,"directory":false,"hidden":false,"lastModified":1222940957187,"name":"juntaExtremadura_test.dap","relativePath":"\\ECLIPSE\\WORK_SPACES\\CALLISTO\\DruidaDesktopV2\\scripts\\tmp\\usuario\\juntaExtremadura_test.dap"},{"canRead":true,"canWrite":true,"directory":true,"hidden":false,"lastModified":1222862622859,"name":"juntaExtremadura_test_1298979978","relativePath":"\\ECLIPSE\\WORK_SPACES\\CALLISTO\\DruidaDesktopV2\\scripts\\tmp\\usuario\\juntaExtremadura_test_1298979978"},{"canRead":true,"canWrite":true,"directory":true,"hidden":false,"lastModified":1222862688109,"name":"juntaExtremadura_test_1966308826","relativePath":"\\ECLIPSE\\WORK_SPACES\\CALLISTO\\DruidaDesktopV2\\scripts\\tmp\\usuario\\juntaExtremadura_test_1966308826"},{"canRead":true,"canWrite":true,"directory":false,"hidden":false,"lastModified":1222955790718,"name":"null380486510.app.xml","relativePath":"\\ECLIPSE\\WORK_SPACES\\CALLISTO\\DruidaDesktopV2\\scripts\\tmp\\usuario\\null380486510.app.xml"},{"canRead":true,"canWrite":true,"directory":false,"hidden":false,"lastModified":1222941414531,"name":"Ofimatica.dap","relativePath":"\\ECLIPSE\\WORK_SPACES\\CALLISTO\\DruidaDesktopV2\\scripts\\tmp\\usuario\\Ofimatica.dap"}])
    actualizaRutaDirectorios("aaaa/bbbbb\\cc/dddddd")
}

// ---- historial de navegaci�n 

function computaHistorial(ruta)  {
   if (window.histNav==null)
      window.histNav={rutas:[], indice:0}
   var indiceNuevaRuta=histNav.rutas.length + histNav.indice
   if (indiceNuevaRuta>0 && histNav.rutas[indiceNuevaRuta-1]==ruta)   {
      return
   }
   if (indiceNuevaRuta<histNav.rutas.length) { //Navego despues de haber retrocedido -> elimino historial
      var nuevoRutas=[]
      for(var r=0; r<indiceNuevaRuta; r++)
         nuevoRutas[r]=histNav.rutas[r]
      histNav.rutas=nuevoRutas;
      histNav.indice=0;
   }
   histNav.rutas[indiceNuevaRuta]=ruta
   /*
   histNav.rutas[indiceNuevaRuta]=ruta
   //Si quedan elementos en el array rehago el array
   if (indiceNuevaRuta<histNav.rutas.length) {
      histNav.indice=0;
   }
   */
}

function historialAtras()  {
   if (window.histNav==null)
      return
   if (histNav.indice + histNav.rutas.length <= 1)
      return
   histNav.indice--
   var indiceRutaADevolver=histNav.rutas.length + histNav.indice - 1
   //alert(histNav.rutas[indiceRutaADevolver])
   return histNav.rutas[indiceRutaADevolver]
}

function historialAdelante()  {
   if ( histNav.indice>=0)
      return
   var indiceRutaADevolver=histNav.rutas.length + histNav.indice
   histNav.indice++
   return histNav.rutas[indiceRutaADevolver]
}

function navegarAtras() {
	if (comunicando())   {
      msgErrorComunicaciones();
		return 
   }
   var ruta=historialAtras()
   if (ruta!=null)
      navegarADirectorio(ruta)
}

function navegarAdelante() {
	if (comunicando())   {
      msgErrorComunicaciones();
		return 
   }
   var ruta=historialAdelante()
   if (ruta!=null)
      navegarADirectorio(ruta)
}

/*Cuadro de ruta de directorios --------------------- */
function actualizaRutaDirectorios(relativePath) {
   computaHistorial(relativePath)
   window.relativePath=relativePath
   var indChain=0;
   var dirs=relativePath.match(/[^\\\/]+/g)
   if (dirs==null)
      dirs=[]
   var htmlSepDir="<img class=\"sepDirectorios\" src=\"img/icoSepDirectorio.gif\">"
   html=[]
   if (dirs.length==0)
      html[html.length]= drResources.get({bundle:"drExplorer",key:"drExplorer.txt11"}) +htmlSepDir
   else  {
      var id="dr_path_chain_"+(indChain)
      indChain++
      html[html.length]="<span id=\""+id+"\" class=\"subdirectorio\">"+ drResources.get({bundle:"drExplorer",key:"drExplorer.txt11"}) +"</span>"+htmlSepDir
      $(window).data(id,"")
      }
   var rutaRelativaCreciente=""
   for (var r=0; r<dirs.length-1; r++)   {
      var id="dr_path_chain_"+(indChain)
      indChain++
      html[html.length]="<span id=\""+id+"\" class=\"subdirectorio\">"+dirs[r]+"</span>"+htmlSepDir
      if (rutaRelativaCreciente.length>0)
         rutaRelativaCreciente=rutaRelativaCreciente+"\\"+dirs[r]
      else
         rutaRelativaCreciente=dirs[r];
      $(window).data(id,rutaRelativaCreciente)
   }
   if (dirs.length>0){
      html[html.length]=dirs[dirs.length-1]+htmlSepDir
   }
   $("#rutaDirectorio").html(html.join(''))
   $("#rutaDirectorio span.subdirectorio").bind("mouseover",function (e)  {
      $(e.target).addClass("subdirectorio_rollover")
   }).bind("mouseout",function (e)  {
      $(e.target).removeClass("subdirectorio_rollover")
   }).bind("mousedown",function (e)  {
      navegarADirectorio($(window).data($(e.target).attr("id")));
   })
   //Ahora lo coloco "al final"
   $("#rutaDirectorio").scrollLeft(10000)
}

function estoyEnDirectorioRaiz() {
   if (window.relativePath=="")
      return true
}

/*Botonera ------------------------ */

function inicializaBotonera() {
   var oIconosIzq={
      claseBtn: "btnIcono"
      ,claseBtnRollOver:"btnIconoRollover"
      ,claseBtnMarcado:"btnIconoMarcado"
      ,claseListaValores:"listaValores"
      ,claseBotonera:""
      ,imgFondo:null
      ,noEstirar:true
      ,altura:26
      ,botones: [
         {img:"img/btnSubirDir.gif", js:"btnSubirDirectorio()", tooltip: drResources.get({bundle:"drExplorer",key:"drExplorer.txt12"}) }
         ,{img:"img/btnAtras.gif", js:"navegarAtras()", tooltip: drResources.get({bundle:"drExplorer",key:"drExplorer.txt13"}) }
         ,{img:"img/btnAdelante.gif", js:"navegarAdelante()", tooltip: drResources.get({bundle:"drExplorer",key:"drExplorer.txt14"}) }
      ]
   }
   var oIconosDcha={
      claseBtn: "btnIcono"
      ,claseBtnRollOver:"btnIconoRollover"
      ,claseBtnMarcado:"btnIconoMarcado"
      ,claseListaValores:"listaValores"
      ,claseBotonera:""
      ,imgFondo:null
      ,noEstirar:true
      ,altura:26
      ,botones: [
         {img:"img/btnUpload.gif", js:"alert('click')", tooltip: drResources.get({bundle:"drExplorer",key:"drExplorer.txt15"}) }
         ,{img:"img/btnDownload.gif", js:"downloadlastSelectedFile()", tooltip: drResources.get({bundle:"drExplorer",key:"drExplorer.txt16"}) }
         ,{img:"img/btnNuevaCarp.gif", js:"crearNuevaCarpeta()", tooltip: drResources.get({bundle:"drExplorer",key:"drExplorer.txt17"}) }
         ,{}
         ,{img:"img/btnCopy.gif", js:"copiarElementosSeleccionados()", tooltip: drResources.get({bundle:"drExplorer",key:"drExplorer.txt18"}) }
         ,{img:"img/btnCut.gif", js:"cortarElementosSeleccionados()", tooltip: drResources.get({bundle:"drExplorer",key:"drExplorer.txt19"}) }
         ,{img:"img/btnPaste.gif", js:"pegarElementos()", tooltip: drResources.get({bundle:"drExplorer",key:"drExplorer.tx20"}) }
         ,{}
         ,{img:"img/btnElimFich.gif", js:"eliminarElementosSeleccionados()", tooltip: drResources.get({bundle:"drExplorer",key:"drExplorer.txt21"}) }
         ,{img:"img/btnRenombrarFich.gif", js:"renameSelectedElements();", tooltip: drResources.get({bundle:"drExplorer",key:"drExplorer.txt22"}) }
      ]
   }
   drGeneraBotoneraIconos("botoneraIzquierda", oIconosIzq)
   drGeneraBotoneraIconos("botoneraDerecha", oIconosDcha)
}


function btnSubirDirectorio() {
   if (!estoyEnDirectorioRaiz())
      navegarASubdirectorio('..')
}




function iniciaBloqueoComunicaciones() {$("#loadingMsg").css({display:"inline"}); window.dr_comunicando=true }
function comunicando(){return (window.dr_comunicando==true)}
function terminaBloqueoComunicaciones() {$("#loadingMsg").css({display:"none"}); window.dr_comunicando=false }
function msgErrorComunicaciones() { alert( drResources.get({bundle:"drExplorer",key:"drExplorer.txt23"}) ) }


/* Operaciones con los ficheros ----------------------- */


window.relativePath=""
window.clipBoard={sourcepath:null, sourceRows:null, cut:false,indexedNames:null, names:null}

// ------------------ CUT COPY PASTE

function elementoEstaCopiado(oRecord) {
   if (window.clipBoard.indexedNames==null || window.clipBoard.cut==true || window.clipBoard.sourcepath != window.relativePath)
      return false
   //hay elementos cortados en el directorio actual
   if (window.clipBoard.indexedNames[oRecord.getData("name")])
      return true
}

function elementoEstaCortado(oRecord) {
   if (window.clipBoard.indexedNames==null || window.clipBoard.cut==false || window.clipBoard.sourcepath != window.relativePath)
      return false
   //hay elementos cortados en el directorio actual
   if (window.clipBoard.indexedNames[oRecord.getData("name")])
      return true
}

function pegarElementos()  {//Pega los elementos en el directorio actual
   if (window.clipBoard.indexedNames==null)
      return
   if (window.clipBoard.cut && window.clipBoard.sourcepath==window.relativePath){
      alert( drResources.get({bundle:"drExplorer",key:"drExplorer.txt24"}) )
      return;
   }
   
   if (comunicando())   {
      msgErrorComunicaciones();
		return 
   }
	iniciaBloqueoComunicaciones()
	DrExplorerService.copyMoveElements(window.clipBoard.sourcepath,window.relativePath,window.clipBoard.names,window.clipBoard.cut, { 
      async:true 
      ,callback:actualizaListaFicheros_callback
      ,timeout:conDwrConnectionTimeout
      ,errorHandler:function(msg){
      	alert(msg);
         actualizaRutaDirectorios("");
         actualizaListaFicherosCon([]);
         terminaBloqueoComunicaciones();
      }
   })

   if (window.clipBoard.cut) 
      window.clipBoard={sourcepath:null, sourceRows:null, cut:false,indexedNames:null, names:null}
}

function copiarElementosSeleccionados(){
   cortarCopiarElementosSeleccionados(false)
}

function cortarElementosSeleccionados(){
   cortarCopiarElementosSeleccionados(true)
}

function cortarCopiarElementosSeleccionados(cortar)   {
   var laLista=window.myDataTable
   var seleccionados=laLista.getSelectedRows()
   if (seleccionados.length==0)  {
      alert( drResources.get({bundle:"drExplorer",key:"drExplorer.txt25"}) )
      return
   }
   var indiceRapido={}, listaNombres=[]
   for (var r=0; r<seleccionados.length; r++)   {
      var nombre=laLista.getRecord(seleccionados[r]).getData("name")
      listaNombres[r]=nombre
      indiceRapido[nombre]=true
   }
   window.clipBoard={ sourcepath:window.relativePath, sourceRows:seleccionados, cut:cortar, indexedNames:indiceRapido, names:listaNombres }
   deseleccionarTodoListaFicheros()
   recargarListaFicheros()
}

// navegar ---

function navegarASubdirectorio(subDir) {//nombre de un subdirectorio sin el "/" delante ni detr�s
   window.relativePath=window.relativePath+"/"+subDir
   actualizaListaFicheros()
}

function navegarADirectorio(dir) {//nombre de un directorio sin el "/" delante ni detr�s
   window.relativePath=dir
   actualizaListaFicheros()
}

function actualizaListaFicheros()	{
	if (comunicando())   {
      msgErrorComunicaciones();
		return 
   }
	iniciaBloqueoComunicaciones()
	DrExplorerService.getDirectory(window.relativePath,{ async:true 
      ,callback:actualizaListaFicheros_callback
      ,timeout:conDwrConnectionTimeout
      ,errorHandler:function(msg){
      	alert(msg);
         actualizaRutaDirectorios("");
         actualizaListaFicherosCon([]);
         terminaBloqueoComunicaciones();
      }
   })
}
function actualizaListaFicheros_callback (obj)	{
	if (obj==null)
		return //sustituir por limpiar la lista
	else if (obj.errorDeOperacion)	{
		alert( drResources.get({bundle:"drExplorer",key:"drExplorer.txt26"}) +" '"+obj.excepcionError.message+"'")
      //actualizaRutaDirectorios("");
      //actualizaListaFicherosCon([]);
      terminaBloqueoComunicaciones();
      return
	}
   actualizaRutaDirectorios(obj.relativePath)
   actualizaListaFicherosCon(obj.files)
	terminaBloqueoComunicaciones()
}

// nueva carpeta ---

function crearNuevaCarpeta()  {
   var nuevoNombre=""
   while (nuevoNombre=="")
      nuevoNombre = window.prompt( drResources.get({bundle:"drExplorer",key:"drExplorer.txt27"}) ,"")
	if (comunicando())   {
      msgErrorComunicaciones();
		return 
   }
	iniciaBloqueoComunicaciones()
	DrExplorerService.createNewDirectory(window.relativePath,nuevoNombre,{ async:true 
      ,callback:operacionFicheros_callback 
      ,timeout:conDwrConnectionTimeout 
      ,errorHandler:function(msg){ alert(msg);terminaBloqueoComunicaciones();}
   })
}


// eliminar ---

function eliminarElementosSeleccionados() {
   var laLista=window.myDataTable
   var seleccionados=laLista.getSelectedRows()
   var listaTextoNombres=[]
   if (seleccionados.length==0)  {
      alert( drResources.get({bundle:"drExplorer",key:"drExplorer.txt25"}) )
      return
   }
   for (var r=0; r<seleccionados.length; r++)   {
      var record=laLista.getRecord(seleccionados[r])
      if (record.getData("directory"))
         listaTextoNombres[listaTextoNombres.length]="'"+laLista.getRecord(seleccionados[r]).getData("name")+"' "+ drResources.get({bundle:"drExplorer",key:"drExplorer.txt28"})
      else
         listaTextoNombres[listaTextoNombres.length]="'"+laLista.getRecord(seleccionados[r]).getData("name")+"'"
   }
   var textoNombres=listaTextoNombres.join(" , ")
   var i=textoNombres.lastIndexOf(',')
   if (i!=-1)
      textoNombres=textoNombres.substr(0,i)+"y"+textoNombres.substr(i+1)
   if (!window.confirm( drResources.get({bundle:"drExplorer",key:"drExplorer.txt29"}) +"\n"+textoNombres))
      return
	if (comunicando())   {
      msgErrorComunicaciones();
		return 
   }
   var listaNombres=[]
   for (var r=0; r<seleccionados.length; r++)
      listaNombres[listaNombres.length]=laLista.getRecord(seleccionados[r]).getData("name")
	iniciaBloqueoComunicaciones()
	DrExplorerService.deleteElements (window.relativePath,listaNombres,{ async:true 
      ,callback:operacionFicheros_callback 
      ,timeout:conDwrConnectionTimeout 
      ,errorHandler:function(msg){ alert(msg);terminaBloqueoComunicaciones();}
   })
}


// --------- (operacionFicheros_callback)
 
function operacionFicheros_callback(obj)  {
   if (obj.errorDeOperacion)	{
		alert( drResources.get({bundle:"drExplorer",key:"drExplorer.txt30"}) +" '"+obj.excepcionError.message+"'")
	} else   {
      if (obj.relativePath!=null)
         actualizaRutaDirectorios(obj.relativePath)
      if (obj.files!=null)
         actualizaListaFicherosCon(obj.files)
   }
	terminaBloqueoComunicaciones()
}

// -------------- UPLOAD de ficheros

function inicializaBotonUploadReal (){
   var onClickBtnUp=function(){
      /*window.valor=$("#fileToUpload").val();alert(window.valor);*/
      $("#rutaDestino").val(window.relativePath)
      $('#formularioFichero').trigger("submit");
      }
   $boton=generaBotonFileUpload({idForm:"formularioFichero", onClick:onClickBtnUp})
   $boton.css({ right:"208px", top:"25px",height:"22px",width:"22px"})
   preparaAjaxFormulario();
}

function preparaAjaxFormulario() {
   var formularioFichero_beforeSubmit=function (formData, jqForm, options) { // formData is an array;
      if ($("#fileToUpload").val()=="")
         return false;// to prevent the form from being submitted; 
      if (comunicando())
         return false;
      iniciaBloqueoComunicaciones();
      //$("#loadingMsg").css({display:"inline"})
      return true; 
   } 
   var formularioFichero_success=function (data, statusText)  { 
      //$("#loadingMsg").css({display:"none"})
      terminaBloqueoComunicaciones()
      if (data.ok==false)   {
         alert(data.error)
      }
      //Ahora RECARGO la lista volviendo a ir a servidor. Con DWR3.0 solo deber�a ir una vez ...
      actualizaListaFicheros();
      //$("#formularioFichero").resetForm()
   }
   var formularioFichero_error=function (XMLHttpRequest, textStatus, errorThrown) {
      terminaBloqueoComunicaciones()
      alert("Error: "+errorThrown.name+": "+errorThrown.message)
      actualizaListaFicheros();
   }
   var options = { 
      //target:        '#output1',   // target element(s) to be updated with server response 
      beforeSubmit:  formularioFichero_beforeSubmit   // pre-submit callback 
      ,success:      formularioFichero_success   // post-submit callback 
      ,url:       "jsp/UploadFile.jsp"         // override for form's 'action' attribute 
      ,type:      "post"        // 'get' or 'post', override for form's 'method' attribute 
      ,dataType:  "json"        // 'xml', 'script', or 'json' (expected server response type) 
      ,clearForm: true        // clear all form fields after successful submit 
      ,resetForm: true        // reset the form after successful submit 
      // $.ajax options can be used here too, for example:  timeout:   3000 
      ,error:formularioFichero_error
   }; 
   $('#formularioFichero').ajaxForm(options); 

}


function generaBotonFileUpload(params) {
   var $form=$("#"+params.idForm);
   var $newContainer=$("<div/>", document);
   $newContainer.attr("id",params.idForm+"_container")
   $newContainer.css({
      "-moz-opacity":0, filter:"alpha(opacity: 0)", opacity:"0",
      position:"absolute",overflow:"hidden"
   })
   $form.wrap($newContainer)
   var $inputFiles=$("#"+params.idForm+" input[type='file']")
   $inputFiles.css({position:"absolute", right:"0px", visibility:"visible"})
   $newContainer=$("#"+params.idForm+"_container") //refrescamos
   $newContainer.bind("click", function(){ setTimeout(params.onClick,0) })
   return $newContainer
}

// ----------------- file OPEN
function openLastSelectedFile()  {
   var record=recordUltimoElemSelNoDir()
   if (record==null) {
      alert( drResources.get({bundle:"drExplorer",key:"drExplorer.txt31"}) )
      return;
   }
   var rutaRelativaFich=window.relativePath+"/"+record.getData("name")
   //var rutaRelativaFich=window.relativePath+"\\"+record.getData("name")
   //alert(rutaRelativaFich)
   var id=drWindow.openFile({file:rutaRelativaFich})
   if (id==null)
      downloadlastSelectedFile()
}

function recordUltimoElemSelNoDir() {
   var laLista=window.myDataTable
   var seleccionados=laLista.getSelectedRows()
   if (seleccionados.length==0)  
      return
   var record=laLista.getRecord(laLista.getLastSelectedRecord())
   if (record.getData("directory")) {
      //busco el primer elemento no directorio
      var ind=0;
      while (ind<seleccionados.length && laLista.getRecord(seleccionados[ind]).getData("directory"))
         ind++
      if (ind==seleccionados.length)
         return
      record=laLista.getRecord(seleccionados[ind])
   }
   return record
}

// ----------------- file DOWNLOAD

function downloadlastSelectedFile()   {
   var record=recordUltimoElemSelNoDir()
   if (record==null) {
      alert( drResources.get({bundle:"drExplorer",key:"drExplorer.tx31"}) )
      return;
   }
   var seleccionados=window.myDataTable.getSelectedRows()
   if (seleccionados.length>1)  {
      alert( drResources.get({bundle:"drExplorer",key:"drExplorer.txt32"}) +"'"+record.getData("name")+"').")
   }
   /*
   var laLista=window.myDataTable
   var seleccionados=laLista.getSelectedRows()
   if (seleccionados.length==0)  {
      alert("Debe seleccionar un fichero.")
      return
   }
   var record=laLista.getRecord(laLista.getLastSelectedRecord())
   if (record.getData("directory")) {
      //busco el primer elemento no directorio
      var ind=0;
      while (ind<seleccionados.length && laLista.getRecord(seleccionados[ind]).getData("directory"))
         ind++
      if (ind==seleccionados.length){
         alert("Debe seleccionar un fichero, los directorios no son descargables.")
         return
      }
      record=laLista.getRecord(seleccionados[ind]) //Primer elemento no directorio
   }
   if (seleccionados.length>1)  {
      alert("No se puede descargar mas que un fichero a la vez as� que se descargar� solo un elemento de los seleccionados ('"+record.getData("name")+"').")
   }*/
   downloadFile( {file:record.getData("name"), path:window.relativePath } )
}

function downloadFile(datos)   {
   //var datos={path:"dir1\\este no existe",file:"btnDownload.gif" }
   window.urlArrDat=[];
   $.each(datos, function(i, val) {
      window.urlArrDat[window.urlArrDat.length]=escape(i)+"="+escape(val)
   });
   $("#elIframe").get(0).src="jsp/DownloadFile.jsp?"+window.urlArrDat.join("&")
}


// ----------------- file RENAME

function renameSelectedElements()   {
   var laLista=window.myDataTable
   var seleccionados=laLista.getSelectedRows()
   if (seleccionados.length==0)  {
      alert( drResources.get({bundle:"drExplorer",key:"drExplorer.txt25"}) )
      return
   }
   var nombresACambiar=[]
   for (var r=0; r<seleccionados.length; r++)   {
      record=laLista.getRecord(seleccionados[r])
      var nombre=record.getData("name")
      if (record.getData("directory"))
         var texto= drResources.get({bundle:"drExplorer",key:"drExplorer.txt33"}) +" '"+nombre+"'";
      else
         var texto= drResources.get({bundle:"drExplorer",key:"drExplorer.txt34"}) +" '"+nombre+"'";
      //Error si algun nombre repetido
      var nuevoNombre=nombre, siguePidiendoNombre=true;
      while (siguePidiendoNombre){
         siguePidiendoNombre=false;
         nuevoNombre=window.prompt(texto,nombre);
         if (nombre==nuevoNombre)
            siguePidiendoNombre=true
         for (var i=0; i<nombresACambiar.length; i++) {
            if (nuevoNombre==nombresACambiar[i][1])   {
               alert("'"+nuevoNombre+ drResources.get({bundle:"drExplorer",key:"drExplorer.txt35"}) );
               siguePidiendoNombre=true
            }
         }
      }
      if (nuevoNombre==null)
         return //cancel
      nombresACambiar[nombresACambiar.length]=[nombre,nuevoNombre]
   }
   if (comunicando())   {
      msgErrorComunicaciones();
		return 
   }
	iniciaBloqueoComunicaciones()
	DrExplorerService.renameElements(window.relativePath,nombresACambiar,{
      async:true 
      ,callback:operacionFicheros_callback 
      ,timeout:conDwrConnectionTimeout 
      ,errorHandler:function(msg){ alert(msg);terminaBloqueoComunicaciones();}
   })
}
