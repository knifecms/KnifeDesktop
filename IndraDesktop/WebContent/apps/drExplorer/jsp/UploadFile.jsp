<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"  autoFlush="true"  buffer="none" %>
<%@ page import="es.indra.druida.desktop.apps.drexplorer.UploadFile" %>
<%@ page import="es.indra.druida.desktop.utils.DesktopWriter" %>

<%

	try	{
		UploadFile uploadf=new UploadFile();
		uploadf.procesaUploadFichero(request);
		%>
		<textarea>
		{ok:true}
		</textarea>
		<%
	} catch (Exception e)	{
		%>
		<textarea>
		{ok:false,error: <%=DesktopWriter.generaCadenaJavascript("Error: "+ e.getMessage(), true)%>}
		</textarea>
		<%
	}

%>