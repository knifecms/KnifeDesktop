
function flexifyContent(fixSizesHandler)  {
   $("body").css({overflow:"hidden"});
   $("html").css({overflow:"hidden"});
   fixSizesHandler()
   $(window).bind("resize",function (ev){
      if (window.waitingForResizeDaemon != null){
         window.clearTimeout(window.waitingForResizeDaemon)
      }  
      window.waitingForResizeDaemon=setTimeout(function (){ 
         window.waitingForResizeDaemon=null;
         fixSizesHandler();
      },50)
   })
}

function getScreenSize(){
   var _window=$(window)
   return {w:_window.width(),h:_window.height()}
}

function quitaEspacios(val){
   return val.replace(/^\s+/,'').replace(/\s+$/,'');
}


function iniciaBloqueoComunicaciones() {
   //$("#loadingMsg").css({display:"inline"});
   window.dr_comunicando=true 
}
function comunicando(){
   return (window.dr_comunicando==true)
}
function terminaBloqueoComunicaciones() {
   //$("#loadingMsg").css({display:"none"});
   window.dr_comunicando=false
}