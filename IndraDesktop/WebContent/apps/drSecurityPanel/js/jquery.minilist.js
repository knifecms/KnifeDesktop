// Objeto creado para el IndraDesktop


(function($) {
   $.fn.createMiniList = function(args) {
      var defaultArgs = {columns:[]};
      var args=$.extend(defaultArgs,args);
      return this.each(function() {
         //Asocio los eventos
         $(this).mouseover(function(e){
            var miniList_tr=$(e.target).closest(".miniList_tr")
            if (miniList_tr.length>0){
               miniList_tr.addClass("listHover")
            }
         }).mouseout(function(e){
            var miniList_tr=$(e.target).closest(".miniList_tr")
            if (miniList_tr.length>0){
               miniList_tr.removeClass("listHover")
            }
         }).mousedown(function(e){
            var miniList_tr=$(e.target).closest(".miniList_tr")
            if (miniList_tr.length>0){
               if (miniList_tr.hasClass("listSelected"))
                  miniList_tr.removeClass("listSelected")
               else
                  miniList_tr.addClass("listSelected")
            }
         }).data("miniListDefinition",args)

      })
   }

   $.fn.populateMiniList = function(args) {
      var defaultArgs = {data:[]};
      //var args=$.extend(defaultArgs,args);
      return this.each(function() {
         var $lista=$(this)
         var miniListDefinition=$lista.data("miniListDefinition")
         miniListDefinition=$.extend(miniListDefinition,args);
         $lista.data("miniListDefinition",miniListDefinition)
         var htmlbg=[] ,html=[], data=miniListDefinition.data, columns=miniListDefinition.columns ;
         for (var c=0; c<columns.length; c++)   {
            if (columns[c].bgClass)
               htmlbg[htmlbg.length]='<div class="miniList_bgTd '+columns[c].bgClass+'"></div>'
         }

         for (var r=0; r<data.length; r++)  {
            html[html.length]='<div class="miniList_tr" id="miniList_tr_'+r+'">'
            for (var c=0; c<columns.length; c++)   {
               if (columns[c].dataId){
                  var dato=data[r][columns[c].dataId]
                  if (dato==null || dato=="")
                     dato="&nbsp;"
                  html[html.length]='<div class="'+columns[c].className+' miniList_td" id="miniList_td_'+r+'_'+c+'">'+dato+'</div>'
               }
            }
            html[html.length]='</div>'
         }
         $lista.find(".miniList_bg").html(htmlbg.join(''))
         $lista.find(".miniList_table").html(html.join(''))
         for (var r=0; r<data.length; r++)  {
            var $tableRow=$lista.find("#miniList_tr_"+r)
            $tableRow.data("dataIndex",r)
            for (var c=0; c<columns.length; c++)   
               if (columns[c].dataId){
                  $tableRow.find("#miniList_td_"+r+"_"+c).attr("title",data[r][columns[c].dataId])
               }
         }
      });
   };

   $.fn.getSelectedRowsMiniList = function(args) {
      var ret=[]
      var $lista=$(this)
      var miniListDefinition=$lista.data("miniListDefinition")
      var $filasSelecc=$lista.find("div.listSelected")
      for (var r=0; r<$filasSelecc.length; r++){
         var index=$filasSelecc.eq(r).data("dataIndex")
         ret[ret.length]=miniListDefinition.data[index]
      }
      return ret
   };

})(jQuery);


/*
ejemplo:
         var miniListColumns=[ {className:"col1", bgClass:"col_bg1", dataId:"col1"}, {className:"col2", dataId:"col2"} ]
         var miniListData=[
            {col1:"aaaa1", col2:"bbb", value:"ccc"}
            ,{col1:'<IMG src="http://localhost:8080/DruidaDesktopV2/drDesktop/img/icons/widget.gif" style="margin:0px; padding:0px; vertical-align: middle; WIDTH: 17px; HEIGHT: 17px"> AenaChangeVDWidget', col2:"bbb2", value:"ccc2"}
            ,{col1:"aaaa3", col2:"bbb3", value:"ccc3"}
         ]
         $("#listaRapida").createMiniList({columns:miniListColumns}).populateMiniList({data:miniListData});


      <div id="listaRapida" class="miniList">
         <div class="miniList_bg">

         </div>
         <div class="miniList_table">
        
         </div>
      </div>
*/