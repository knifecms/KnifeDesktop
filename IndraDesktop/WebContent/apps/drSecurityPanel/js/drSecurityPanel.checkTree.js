

function CheckTree(args){
   this.args=args
   this.container=args.container
   /*
   var jsonData=[]
   for (var r=0; r<values.length; r++)   {
      jsonData[jsonData.length]={attributes:{id:"CHECK_"+r},data:labels[r]}
   }
   */
   this.treeConf={
      ui:{ 
         theme_path :"css/jsTree/themes/"
         ,theme_name :"checkbox" 
         ,dots:false
      }
      ,data:{type:"json",json:[]}
      ,callback : { 
         onchange : this.onchange
      }
   }
   
   if (args.theme_path!=null)
      this.treeConf.ui.theme_path=args.theme_path

   this.container.data("CheckTreeData",this);

   this.setOptions=function (args){
      var options=args.options
      var jsonData=[]
      for (var r=0; r<options.length; r++){
         jsonData[r]={attributes:{id:"CHECK_"+r},data:options[r].label}
      }
      this.treeConf.data.json=jsonData
      this.container.tree(this.treeConf);
      for (var r=0; r<options.length; r++){
         this.container.find("#CHECK_"+r).data("value",options[r].v)
      }
   }
   

   this.onchange=function (NODE, TREE_OBJ) {
      var $this = $(NODE).is("li")?$(NODE):$(NODE).parent();
      if($this.children("a.unchecked").size() == 0) {
         TREE_OBJ.container.find("a").addClass("unchecked");
      }
      $this.children("a").removeClass("clicked");
      if($this.children("a").hasClass("checked")) {
         $this.find("li").andSelf().children("a").removeClass("checked").removeClass("undetermined").addClass("unchecked");
         var state = 0;
      }
      else {
         $this.find("li").andSelf().children("a").removeClass("unchecked").removeClass("undetermined").addClass("checked");
         var state = 1;
      }
      $this.parents("li").each(function () { 
         if(state == 1) {
            if($(this).find("a.unchecked, a.undetermined").size() - 1 > 0) {
               $(this).parents("li").andSelf().children("a").removeClass("unchecked").removeClass("checked").addClass("undetermined");
               return false;
            }
            else $(this).children("a").removeClass("unchecked").removeClass("undetermined").addClass("checked");
         }
         else {
            if($(this).find("a.checked, a.undetermined").size() - 1 > 0) {
               $(this).parents("li").andSelf().children("a").removeClass("unchecked").removeClass("checked").addClass("undetermined");
               return false;
            }
            else $(this).children("a").removeClass("checked").removeClass("undetermined").addClass("unchecked");
         }
      });
   }
}