window.securityData={users:[],securityConstraints:[]}

$(function(){
   I18N()
   getDataAndCreateTree();
   ocultaFormularios();
   preparaFormularios();
   flexifyContent();
   prepareButtons();
   prepareLists();
   prepareDialogBoxes();
})

function I18N(){
   $("#btnMinimizar").attr("title",drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt3"}))
   $("#btnMaximizar").attr("title",drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt4"}))
   $("#btnNuevoUsuario").attr("title",drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt5"}))
   $("#btnNuevaRestr").attr("title",drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt6"}))
   $("#btnEliminar").attr("title",drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt7"}))
   $("#btnRecuperar").attr("title",drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt8"}))
}

function creaCopiaDatosSeguridad(secData){
   var copia={users:[],securityConstraints:[]}
   for (var i=0; i<secData.users.length; i++){
      copia.users[i]={}
      var user=secData.users[i]
      for (a in secData.users[i])
         copia.users[i][a] = user[a]
      if (user.roles!=null){
         var nuevoRoles=[]
         for (var j=0; j<user.roles.length; j++)
            nuevoRoles[j]=user.roles[j]
         copia.users[i].roles=nuevoRoles
      }
         
   }
   for (var i=0; i<secData.securityConstraints.length; i++){
      var constr=secData.securityConstraints[i]
      copia.securityConstraints[i]={}
      for (a in constr)
         copia.securityConstraints[i][a] = constr[a]
      if (constr.applications!=null){
         var nuevoArr=[]
         for (var j=0; j<constr.applications.length; j++)
            nuevoArr[j]=constr.applications[j]
         copia.securityConstraints[i].applications=nuevoArr
      }
      if (constr.authorizedRoles!=null){
         var nuevoArr=[]
         for (var j=0; j<constr.authorizedRoles.length; j++)
            nuevoArr[j]=constr.authorizedRoles[j]
         copia.securityConstraints[i].authorizedRoles=nuevoArr
      }
   }
   return copia
}

// ------------------- Dialog Boxes

function prepareDialogBoxes() {
   $("#existingRoleDialogBox, #AppGrpDialogBox, #AppsDialogBox").bind("mousedown",function(event){
      event.stopPropagation() 
   }).bind("keydown",function(event){
      event.stopPropagation() 
   })
   $("#existingRoleDialogBoxContainer, #AppGrpDialogCont, #AppsDialogCont").css({display:"none"})
}

function showDialogBox(args){
   args=args==null?{}:args;
   var container=args.container
   container.css({display:"inline"})
   setTimeout(function(){
      $(document).bind("mousedown",hideDialogBox).bind("keydown",hideDialogBox).data("dialogBoxContainer",container)
   },0)
}

function hideDialogBox()   {
   var dialogBoxContainer=$(document).data("dialogBoxContainer")
   $(dialogBoxContainer).css({display:"none"})
   $(document).unbind("mousedown",hideDialogBox).unbind("keydown",hideDialogBox)
}

// ------------------- Lists

function prepareLists() {
   var miniListColumns=
   $("#listaGrupos").createMiniList({columns:[ 
      {className:"col1", bgClass:"col_bg1", dataId:"id"}
      ,{className:"col2", dataId:"descripcion"} 
   ]})
   $("#listaAplicaciones").createMiniList({columns:[
      {className:"col0", bgClass:"col_bg0", dataId:"icon"}
      ,{className:"col1", bgClass:"col_bg1", dataId:"id"}
      ,{className:"col2", dataId:"descripcion"}
   ]})
   $("#existingRolesList").createMiniList({columns:[
      {className:"existingRoles_col1", dataId:"id"} 
   ]})
}

// ------------------- Evevntos de los campos de formulario -------------------------------------------------------------------------------

function preparaFormularios() {
   //Los campos a medio actualizar a veces hay que actualizarlos (p.ej antes de salvar)
   $("#user_name, #user_surname, #user_motherSurname, #user_fileSystemRoot, #user_roles, #constr_description, #authorizedRoles, #applicationGroups, #applications, #user_newPasswd, #user_confirmPasswd, #user_oldPasswd")
      .focus(function(ev){ window.campoEditandose=this })
   //onchange
   $("#user_name").change(function(ev){ $("#userForm").data("userData").name=quitaEspacios($(this).val())})
   $("#user_surname").change(function(ev){ $("#userForm").data("userData").surname=quitaEspacios($(this).val())} )
   $("#user_motherSurname").change(function(ev){ $("#userForm").data("userData").motherSurname=quitaEspacios($(this).val())} )
   $("#user_fileSystemRoot").change(function(ev){ $("#userForm").data("userData").fileSystemRoot=quitaEspacios($(this).val())} )
   $("#constr_description").change(function(ev){ $("#constrForm").data("constrData").description=quitaEspacios($(this).val())})

   //#authorizedRoles, #applicationGroups, #applications
   $("#user_roles").change(function(ev){ $("#userForm").data("userData").roles=stringToArray($(this).val()) })
   $("#authorizedRoles").change(function(ev){
      var authRolesArr=stringToArray($(this).val())
      $("#constrForm").data("constrData").authorizedRoles=authRolesArr
   })
   $("#applicationGroups").change(function(ev){ $("#constrForm").data("constrData").applicationGroups=stringToArray($(this).val()) } )
   $("#applications").change(function(ev){ $("#constrForm").data("constrData").applications=stringToArray($(this).val()) } )


   //password
   $("#btnChangePasswd").click(function (){
      $("#user_oldPasswd_block, #user_newPasswd_block, #user_confirmPasswd_block").css({display:"block"})
      $("#btnChangePasswdContainer").css({display:"none"})
   })
   $("#btnSetPasswd").click(function (){
      $("#user_newPasswd_block, #user_confirmPasswd_block").css({display:"block"})
      $("#btnSetPasswdContainer").css({display:"none"})
   })
   /*$("#changePasswdBtn").click(function (){
      $("#user_oldPasswd_block, #user_newPasswd_block, #user_confirmPasswd_block").css({display:"block"})
      $(this).css({display:"none"})
   })
   $("#setPasswdBtn").click(function (){
      $("#user_newPasswd_block, #user_confirmPasswd_block").css({display:"block"})
      $(this).css({display:"none"})
   })*/
   $("#user_newPasswd").change(function(){
      $("#userForm").data("userData").newPassword=quitaEspacios($(this).val())
   })
   $("#user_confirmPasswd").change(function(){
      $("#userForm").data("userData").confirmPassword=quitaEspacios($(this).val())
   })
   $("#user_oldPasswd").change(function(){
      $("#userForm").data("userData").password=quitaEspacios($(this).val())
   })

   //Ayuda
   $("#user_roles, #authorizedRoles, #applicationGroups, #applications").focus(function(){show( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt40"}) )})//"Introduzca en la caja de texto una lista de identificadores separados por comas."
   $("#user_name, #user_surname, #user_motherSurname, #user_fileSystemRoot, #constr_description, #user_newPasswd, #user_confirmPasswd, #user_oldPasswd").focus(function(){show( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt41"}) )})//"Introduzca en la caja de texto el valor deseado."
}

function quitaEspacios(val){
   return val.replace(/^\s+/,'').replace(/\s+$/,'');
}

function stringToArray(val){
   var valor=val.replace(/\s*,\s*/g,',').replace(/\s+$/g,'').replace(/^\s+/g,'')
   if (valor=="")
      return []
   return valor.split(/,|\s+/g)
}

function mapeaDatosUsuarioAFormulario(userData) {
   var id=userData.id, name=userData.name, surname=userData.surname, motherSurname=userData.motherSurname
      ,fileSystemRoot=userData.fileSystemRoot, roles=userData.roles, hasPassword=userData.hasPassword
      ,password=userData.password, newPassword=userData.newPassword, confirmPassword=userData.confirmPassword;
   id=(id==null)?"":id;
   name=(name==null)?"":name;
   surname=(surname==null)?"":surname;
   motherSurname=(motherSurname==null)?"":motherSurname;
   fileSystemRoot=(fileSystemRoot==null)?"":fileSystemRoot;
   roles=(roles==null)?[]:roles;
   $("#userForm").data("userData",userData)
   $("#user_id").val(id)
   $("#user_name").val(name)
   $("#user_surname").val(surname)
   $("#user_motherSurname").val(motherSurname)
   $("#user_fileSystemRoot").val(fileSystemRoot)
   //checkTree_setCheckedNodes($("#user_roles_container"),roles)
   $("#user_roles").val(roles.join(", "))
   //$("#rightPanelTitleLabel").html("Detalle usuario: ["+id+"] "+name+" "+motherSurname)
   //Controlo los campos de password
   $("#btnChangePasswdContainer, #btnSetPasswdContainer, #user_oldPasswd_block, #user_newPasswd_block, #user_confirmPasswd_block").css({display:"none"})
   if (password!=null || newPassword!=null || confirmPassword!=null) {
      $("#user_oldPasswd").val(password)
      $("#user_newPasswd").val(newPassword)
      $("#user_confirmPasswd").val(confirmPassword)
      if (hasPassword){
         $("#btnChangePasswd").click()
      } else   {
         $("#btnSetPasswd").click()
      }
   } else   {
      $("#user_oldPasswd, #user_newPasswd, #user_confirmPasswd").val("")
      if (hasPassword){
         $("#btnChangePasswdContainer").css({display:"block"})
      } else   {
         $("#btnSetPasswdContainer").css({display:"block"})
      }
   }
   var roleOptions=[]
   for (var i=0; i<roles.length; i++)
      roleOptions[i]={label:roles[i], value:roles[i]}
   show( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt42"}) )//"Edite en el formulario los valores deseados para el usuario."
}

function mapeaDatosRestriccionAFormulario(constrData){
   $("#constrForm").data("constrData",constrData)
   var constr_id=constrData.id
   var description=constrData.description==null?"":constrData.description
   var authRoles=constrData.authorizedRoles
   var applicationGroups=constrData.applicationGroups
   var applications=constrData.applications
   authRoles=(authRoles==null)?[]:authRoles;
   applicationGroups=(applicationGroups==null)?[]:applicationGroups;
   applications=(applications==null)?[]:applications;
   //checkTree_setCheckedNodes($("#authorizedRoles_container"),authRoles)
   $("#constr_id").val(constr_id)
      //debugger
   $("#constr_description").val(description)
   $("#authorizedRoles").val(authRoles.join(", "))
   //checkTree_setCheckedNodes($("#applicationGroups_container"),applicationGroups)
   $("#applicationGroups").val(applicationGroups.join(", "))
   //checkTree_setCheckedNodes($("#applications_container"),applications)
   $("#applications").val(applications.join(", "))
   //$("#rightPanelTitleLabel").html("Detalle restricci�n: "+authRoles.join(", "))
   $("#rightPanelTitleLabel").html("Detalle Restricci�n")
   show( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt60"}) )//"Edite en el formulario los valores deseados para la restricci�n."
}

function verificaCamposCorrectos()  {
   for (var i=0 ; i<window.securityData.users.length; i++)  {
      var datosUsu=window.securityData.users[i]
      if (datosUsu.newPassword && datosUsu.newPassword!=""){
         if (datosUsu.confirmPassword!=datosUsu.newPassword){
            alert( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt43"}) + datosUsu.id+  drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt44"}) ) //"No se ha podido salvar la informaci�n. El campo 'New Password' y el campo 'Confirm New Password' del usuario '" "' no coinciden."
            return false
         }
      }
   }
   return true;
}

function restauraCamposPassword()   {
   $("#user_oldPasswd, #user_newPasswd, #user_confirmPasswd").val("")
   for (var i=0; i<window.securityData.users.length; i++)   {
      var user=window.securityData.users[i]
      if (user.newPassword!=null)
         user.hasPassword=true;
      user.newPassword=null;
      user.confirmPassword=null;
      user.password=null;
      $("#btnChangePasswdContainer, #btnSetPasswdContainer, #user_oldPasswd_block, #user_newPasswd_block, #user_confirmPasswd_block").css({display:"none"})
      if (user.hasPassword){
         $("#btnChangePasswdContainer").css({display:"block"})
      } else   {
         $("#btnSetPasswdContainer").css({display:"block"})
      }
   }
}

// ------------------- Arbol Elementos -----------------------------------------------------------------------------------------------------

function userNodeTreeData(userData) {
   this.attributes={id:"USER_"+userData.id}
   this.data={title:userData.id, icon:'css/images/icoUsuario.gif'}
   this.realData=userData
}

function constrNodeTreeData(constrData) {
   this.attributes={id:"CONSTR_"+constrData.id}
   this.data={title:constrData.id, icon:'css/images/icoRestr.gif'}
   this.realData=constrData
}

function createJsonDataForJsTreeFrom(data){
   userNodes=[]
   constraintNodes=[]
   for (var r=0; r<data.users.length; r++)   {
      var userData=data.users[r]
      userNodes[userNodes.length]=new userNodeTreeData(userData)
   }
   for (var r=0; r<data.securityConstraints.length; r++)   {
      var nodeData=data.securityConstraints[r]
      constraintNodes[constraintNodes.length]=new constrNodeTreeData(nodeData)//{attributes:{id:"CONSTR_"+r},data:{title:nodeData.id}, realData:nodeData}
   }
   var jsonData=[
      {attributes:{id:"ROOT_USERS"},state:"open",data:{title:"Users",icon:'css/images/icoUserGroup.gif'},children:userNodes}
      ,{attributes:{id:"ROOT_CONSTR"},state:"open",data:{title:"Constraints",icon:'css/images/icoRestrGroup.gif'},children:constraintNodes}
      //,{attributes:{id:"ROOT_ROLES"},data:"Existing Roles"}
    ]
    return jsonData;
}

function createDataOnNodes(data){
   //debugger
   for (var r=0; r<data.users.length; r++)   {
      var userData=data.users[r]
      $("#USER_"+userData.id).data("userIndex",r).data("type","USER")
   }
   for (var r=0; r<data.securityConstraints.length; r++)   {
      var constrData=data.securityConstraints[r]
      $("#CONSTR_"+constrData.id).data("constraintIndex",r).data("type","CONSTR")
   }
}

function createTree()   {
   var jsonData=createJsonDataForJsTreeFrom(window.securityData)
   var treeConf={
      ui:{
         theme_path :"css/jsTree/themes/"
         ,theme_name :"desktop"
         /*,context:[
            {
               id		: "ok"
               ,label	: "test entry"
               ,icon	: "css/jsTree/themes/remove.png"
               ,visible : function (NODE, TREE_OBJ){
                  alert($(NODE).metadata().atr1)
                  return true
               }
               ,action: function (NODE, TREE_OBJ) {
                  alert('"' + NODE.children("a").text() + '" - "' + TREE_OBJ.container.attr("id") + '"');
               } 
            }
         ]*/
         ,context:false
      }
      ,data:{
         type:"json"
         ,json:jsonData
      }
      ,callback:{
         onselect : onTreeSelect
      }
   }
   $("#leftPanelTree").tree(treeConf);
   createDataOnNodes(window.securityData)
}

function refreshTree(){
   var treeRef=$.tree_reference('leftPanelTree')
   var jsonData=createJsonDataForJsTreeFrom(window.securityData)
   treeRef.settings.data.json=jsonData
   treeRef.refresh();
   createDataOnNodes(window.securityData)
}

function ocultaFormularios(){
   $("#userForm, #constrForm").css({display:"none"})
}

function onTreeSelect(NODE,TREE_OBJ) { 
  ocultaFormularios()
  var nodeType=$(NODE).data("type")
  if (nodeType=="CONSTR"){
     //createAutorizedRoleTree({container:$("#authorizedRoles_container")})
     //createAppGroupsTree({container:$("#applicationGroups_container")})
     //createAppTree({container:$("#applications_container")})
     $("#constrForm").css({display:"block"})
     var constraintIndex=$(NODE).data("constraintIndex")
     var constrData=window.securityData.securityConstraints[constraintIndex]
     mapeaDatosRestriccionAFormulario(constrData)
     $("#rightPanelTitleLabel").html(drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt61"}))//"Detalle Restricci�n"
  } else if (nodeType=="USER"){
     //createRoleTree({container:$("#user_roles_container")})
     $("#userForm").css({display:"block"})
     var userIndex=$(NODE).data("userIndex")
     var userData=window.securityData.users[userIndex]
     $("#rightPanelTitleLabel").html(drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt62"}))//"Detalle Usuario"
     mapeaDatosUsuarioAFormulario(userData)
  } else {
     $("#rightPanelTitleLabel").html(drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt9"}))//"Detalle Elemento Seleccionado"
  }
}


function IdExists(arrayObj, id){
   for (var i=0; i<arrayObj.length; i++)   {
      if (arrayObj[i].id==id){
         return true
      }
   }
   return false
}

function crearNuevoUsuario()  {
   var suggIndex=1   
   var sugId="user"+suggIndex
   while (IdExists(window.securityData.users, sugId)){
      suggIndex++;
      sugId="user"+suggIndex;
   }
   var nuevoId=window.prompt( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt45"}) ,sugId) // "Elija un ID para el nuevo usuario. Este identificador es tambi�n el login del usuario y no podr� ser modificado posteriormente."
   if (nuevoId==null)
      return
   else if (nuevoId.length<4) {
      alert( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt46"}) ) // "El ID debe tener al menos 4 caracteres."
      return
   }
   if (IdExists(window.securityData.users,nuevoId)){
      alert( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt47"}) )//"El ID no puede ser igual a ningun otro ID que exista."
      return
   }
   var nuevoUserData={id:nuevoId,hasPassword:false, roles:[], refresh:true}  //{id:"usuario",name:"Usuario",surname:"Invitado",motherSurname:"An�nimo",hasPassword:true,fileSystemRoot:"/ECLIPSE/WORK_SPACES/CALLISTO/DruidaDesktopV2/userFileSystem/usuario",roles:["fileAccessUser","globalManager","traexAdmin"]}
   var newIndex=window.securityData.users.length
   window.securityData.users[newIndex]=nuevoUserData
   $.tree_reference('leftPanelTree').create(new userNodeTreeData(nuevoUserData), $("#ROOT_USERS"))
   $("#USER_"+nuevoUserData.id).data("userIndex",newIndex).data("type","USER")
}

function crearNuevaRestriccion()  {
   var suggIndex=1   
   var sugId="Constraint"+suggIndex
   while (IdExists(window.securityData.securityConstraints, sugId)){
      suggIndex++;
      sugId="Constraint"+suggIndex;
   }
   var nuevoId=window.prompt( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt48"}) ,sugId) //"Elija un ID para la nueva restricci�n. Este identificador no podr� ser modificado posteriormente."
   if (nuevoId==null)
      return
   else if (nuevoId.length<4) {
      alert( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt49"}) )//"El ID debe tener al menos 4 caracteres."
      return
   }
   if (IdExists(window.securityData.securityConstraints,nuevoId)){
      alert( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt50"}) )//"El ID no puede ser igual a ningun otro ID que exista."
      return
   }
   var nuevaConstrData={id:nuevoId, authorizedRoles:[],applications:[], applicationGroups:[]} 
   var newIndex=window.securityData.securityConstraints.length
   window.securityData.securityConstraints[newIndex]=nuevaConstrData
   $.tree_reference('leftPanelTree').create(new constrNodeTreeData(nuevaConstrData), $("#ROOT_CONSTR"))
   $("#CONSTR_"+nuevaConstrData.id).data("constraintIndex",newIndex).data("type","CONSTR")
}

function recuperarDatos(){
   if (drWindow.showConfirm({message:  drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt51"}) })) {//'Se van a recuperar los datos de seguridad que hab�a al abrir la aplicaci�n perdiendose as� todos los cambios. �Confirma la operaci�n?'
      window.securityData=creaCopiaDatosSeguridad(window.securityData_restore)
      ocultaFormularios();
      refreshTree()
   }
}

function eliminarElementoSeleccionado() {
   var selectedNode=$.tree_reference('leftPanelTree').selected
   if (selectedNode!=null){
      var type=$(selectedNode).data("type")
      if (type=="CONSTR"|| type=="USER"){
         if (type=="CONSTR")  {
            var constraintIndex=$(selectedNode).data("constraintIndex")
            var constrData=window.securityData.securityConstraints[constraintIndex]
            var msg= drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt52"}) + ": "+constrData.id //"�Confirma que desea eliminar esta Restricci�n?"
            if (drWindow.showConfirm({message:msg})) {
               $.tree_reference('leftPanelTree').remove(selectedNode)
               //window.securityData.securityConstraints[constraintIndex]=null;
               window.securityData.securityConstraints=eliminaElementoDeArray(window.securityData.securityConstraints,constraintIndex)
               createDataOnNodes(window.securityData)
            }
         } else if (type=="USER")   {
            var userIndex=$(selectedNode).data("userIndex")
            var userData=window.securityData.users[userIndex]
            var msg=drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt53"})+ ": "+ userData.id //"�Confirma que desea eliminar este usuario?"
            if (drWindow.showConfirm({message:msg})) {
               $.tree_reference('leftPanelTree').remove(selectedNode)
               //window.securityData.users[userIndex]=null;
               window.securityData.users=eliminaElementoDeArray(window.securityData.users,userIndex);
               createDataOnNodes(window.securityData)
            }
         }
      }
   }
}

function eliminaElementoDeArray(arr,ind){
   var ret=[]
   if (arr==null)
      return
   for (a=0; a<arr.length; a++)  {
      if (a!=ind)
         ret[ret.length]=arr[a]
   }
   return ret
}

// ------------------- Manejo de arboles CHECK -----------------------------------------------------------------------------------------------------
/*
function onchangeCheckboxTree(NODE, TREE_OBJ) {
   if(TREE_OBJ.settings.ui.theme_name == "checkbox") {
      var $this = $(NODE).is("li") ? $(NODE) : $(NODE).parent();
      if($this.children("a.unchecked").size() == 0) {
         TREE_OBJ.container.find("a").addClass("unchecked");
      }
      $this.children("a").removeClass("clicked");
      if($this.children("a").hasClass("checked")) {
         $this.find("li").andSelf().children("a").removeClass("checked").removeClass("undetermined").addClass("unchecked");
         var state = 0;
      }
      else {
         $this.find("li").andSelf().children("a").removeClass("unchecked").removeClass("undetermined").addClass("checked");
         var state = 1;
      }
      $this.parents("li").each(function () { 
         if(state == 1) {
            if($(this).find("a.unchecked, a.undetermined").size() - 1 > 0) {
               $(this).parents("li").andSelf().children("a").removeClass("unchecked").removeClass("checked").addClass("undetermined");
               return false;
            }
            else $(this).children("a").removeClass("unchecked").removeClass("undetermined").addClass("checked");
         }
         else {
            if($(this).find("a.checked, a.undetermined").size() - 1 > 0) {
               $(this).parents("li").andSelf().children("a").removeClass("unchecked").removeClass("checked").addClass("undetermined");
               return false;
            }
            else $(this).children("a").removeClass("checked").removeClass("undetermined").addClass("unchecked");
         }
      });
   }

}

function checkTree_setCheckedNodes(container,values) {
   //limpio 
   var allValues=container.data("values")
   container.find("a").removeClass("checked").removeClass("undetermined").addClass("unchecked");
   if (values==null || values.length==0)
      return 
   //marco
   for (var r=0; r<values.length; r++) {
      var ind=indiceDeElementoEn(values[r],allValues)
      container.find("#CHECK_"+ind).children("a").removeClass("unchecked").removeClass("undetermined").addClass("checked");
   }
}

function indiceDeElementoEn(elem, array){
   for (var r=0; r<array.length; r++) {
      if (array[r]==elem)
         return r
   }
   return -1
}

function checkTree_create(args){
   var container=args.container, values=args.values, labels=args.labels, input=args.input;
   if (labels==null)
      labels=values
   var jsonData=[]
   for (var r=0; r<values.length; r++)   {
      jsonData[jsonData.length]={attributes:{id:"CHECK_"+r},data:labels[r]}
   }
   var treeConf={
      ui:{ theme_path :"css/jsTree/themes/" ,theme_name :"checkbox" }
      ,data:{type:"json",json:jsonData}
      ,callback : { 
         onchange : onchangeCheckboxTree
      }
   }
   for (var r=0; r<values.length; r++)   {
      $("#CHECK_"+r).data("value",values[r])
   }
   container.tree(treeConf);
   container.data("values",values)
   container.data("input",input)
}
*/
// ------------------- Botones -----------------------------------------------------------------------------------------------------

function prepareRollovers (jqObj) {
   jqObj.hover(function(){$(this).addClass("hover")}, function(){$(this).removeClass("hover")})
}

function prepareButtons()  {
   prepareRollovers($("#btnSaveChanges, #btnMinimizar, #btnMaximizar, #btnRecuperar, #btnNuevoUsuario, #btnNuevaRestr, #btnEliminar, #btnAddRole, #btnChangePasswd, #btnSetPasswd, #btnAddExistingRoles, #btnAddExistingRolesCancel, #btnAddAuthRole, #btnAddAppGrp, #btnAddApps, #btnAddAppGrpCancel, #btnAddAppGrpOk, #btnAddAppsOk, #btnAddAppsCancel"));
   $("#btnSaveChanges").mousedown(saveChanges)
   $("#btnNuevoUsuario").mousedown(crearNuevoUsuario)
   $("#btnNuevaRestr").mousedown(crearNuevaRestriccion)
   $("#btnEliminar").mousedown(eliminarElementoSeleccionado)

   $("#btnAddRole").mousedown(addExistingRole);
   $("#btnAddExistingRoles").mousedown(addExistingRole_dialogOk);
   $("#btnAddExistingRolesCancel").mousedown(addExistingRole_dialogCancel);
   $("#btnAddAuthRole").mousedown(addAuthRole);

   $("#btnAddAppGrp").mousedown(addAppGrp);
   $("#btnAddAppGrpOk").mousedown(addAppGrp_dialogOk);
   $("#btnAddAppGrpCancel").mousedown(addAppGrp_dialogCancel);

   $("#btnAddApps").mousedown(addApps);
   $("#btnAddAppsOk").mousedown(addApps_dialogOk);
   $("#btnAddAppsCancel").mousedown(addApps_dialogCancel);

   
   $("#btnMaximizar").click(function (){
      var tree_reference=$.tree_reference('leftPanelTree')
      tree_reference.open_branch($('#ROOT_USERS'));
      tree_reference.open_branch($('#ROOT_CONSTR'));
   });

   $("#btnMinimizar").click(function (){
      var tree_reference=$.tree_reference('leftPanelTree')
      tree_reference.close_branch($('#ROOT_USERS'));
      tree_reference.close_branch($('#ROOT_CONSTR'));
   });
   
   $("#btnRecuperar").click(recuperarDatos)

}

function saveChanges()  {
   if (window.campoEditandose!=null)
      $(window.campoEditandose).change();
   if (!verificaCamposCorrectos())
      return
   saveTreeData();
}


function addExistingRole(){
   var rolesToRemove=stringToArray( $("#user_roles").val() )
   var existingRoles=getExistingRoles({rolesToRemove:rolesToRemove})
   var listaRoles_data=[]
   for (var r=0; r<existingRoles.length; r++){
      listaRoles_data[r]={id:existingRoles[r]}
   }
   $("#existingRolesList").populateMiniList({data:listaRoles_data});
   $("#btnAddExistingRoles").data("targetField",$("#user_roles"))
   showDialogBox({container:$("#existingRoleDialogBoxContainer")})
}

function addAuthRole(){
   var rolesToRemove=stringToArray( $("#authorizedRoles").val() )
   var existingRoles=getExistingRoles({rolesToRemove:rolesToRemove})
   var listaRoles_data=[]
   for (var r=0; r<existingRoles.length; r++){
      listaRoles_data[r]={id:existingRoles[r]}
   }
   $("#existingRolesList").populateMiniList({data:listaRoles_data});
   $("#btnAddExistingRoles").data("targetField",$("#authorizedRoles"))
   showDialogBox({container:$("#existingRoleDialogBoxContainer")})
}

function addExistingRole_dialogOk(){
   targetField=$("#btnAddExistingRoles").data("targetField")
   var roles=stringToArray( targetField.val() )
   var datosRolesSelecc = $("#existingRolesList").getSelectedRowsMiniList()
   for (var r=0; r<datosRolesSelecc.length; r++) 
      roles[roles.length]=datosRolesSelecc[r].id
   targetField.val(roles.join(", ")).change()
   hideDialogBox()
}

function addExistingRole_dialogCancel(){
   hideDialogBox()
}



function addAppGrp() {
   //Creo datos para la lista de grupos
   var existingAppGrp=stringToArray( $("#applicationGroups").val() )
   var listaGrupos_data=[]
   var grupos=drWindow.getApplicationsGroupsConfig()
   for (var r=0; r<grupos.length; r++) {
      var grupo=grupos[r]
      var ind=indiceDeElementoEn(grupo.name,existingAppGrp)
      if (ind==-1)
         listaGrupos_data[listaGrupos_data.length]={id:grupo.name, descripcion:grupo.descripcion}
   }
   $("#listaGrupos").populateMiniList({data:listaGrupos_data});
   showDialogBox({container:$("#AppGrpDialogCont")})
}
function addAppGrp_dialogOk(){
   var datosElemSelecc = $("#listaGrupos").getSelectedRowsMiniList()
   targetField=$("#applicationGroups")
   var groups=stringToArray( targetField.val() )
   for (var r=0; r<datosElemSelecc.length; r++) 
      groups[groups.length]=datosElemSelecc[r].id
   targetField.val(groups.join(", ")).change()
   hideDialogBox()
}
function addAppGrp_dialogCancel(){
   hideDialogBox()
}

function addApps() {
   //Creo datos para la lista de aplicaciones
   var existingApps=stringToArray( $("#applications").val() )
   var aplicaciones=drWindow.getApplicationsConfig()
   var listaApps_data=[]
   var drDesktopWin = drWindow.getDesktopWindow()
   for (var id in aplicaciones)   {
      var ind=indiceDeElementoEn(id,existingApps)
      if (ind==-1){
         var aplicacion=aplicaciones[id]
         var descr=aplicacion.descripcion
         if (descr==null)
            descr="";
         if (descr=="" && aplicacion.title!=null)
            descr=drResources.replaceI18NValues(aplicacion.title)
         if (descr=="" && aplicacion.iconText!=null)
            descr=aplicacion.iconText;
         var icon="";
         if (aplicacion.icon!=null && aplicacion.icon!=""){
            var iconsrc=drDesktopWin.urlRaizIconos()+'/'+aplicacion.icon
            icon='<IMG src="'+iconsrc+'" class="listIcon">'
         }
         listaApps_data[listaApps_data.length]={id:id, descripcion:descr, icon:icon}
      }
   }
   $("#listaAplicaciones").populateMiniList({data:listaApps_data});
   showDialogBox({container:$("#AppsDialogCont")})
}
function addApps_dialogOk(){
   var datosElemSelecc = $("#listaAplicaciones").getSelectedRowsMiniList()
   targetField=$("#applications")
   var apps=stringToArray( targetField.val() )
   for (var r=0; r<datosElemSelecc.length; r++) 
      apps[apps.length]=datosElemSelecc[r].id
   targetField.val(apps.join(", ")).change()
   hideDialogBox()
}
function addApps_dialogCancel(){
   hideDialogBox()
}

function getExistingRoles(args){
   if (args==null)
      args={}
   var rolesToRemove=args.rolesToRemove
   securityData=window.securityData
   var roles={} //Lo indexo para eliminar repeticiones
   for (var r=0; r<securityData.users.length; r++) {
      var userRoles=securityData.users[r].roles
      if (userRoles!=null){
         for (var g=0; g<userRoles.length; g++) {
            var rol=userRoles[g]
            if (roles[rol]==null)
               roles[rol]=true
         }
      }
   }
   for (var r=0; r<securityData.securityConstraints.length; r++) {
      var constrRoles=securityData.securityConstraints[r].authorizedRoles
      if (constrRoles!=null){
         for (var g=0; g<constrRoles.length; g++) {
            var rol=constrRoles[g]
            if (roles[rol]==null)
               roles[rol]=true
         }
      }
   }
   if (rolesToRemove!=null)
      for (var r=0; r<rolesToRemove.length; r++) 
         roles[rolesToRemove[r]]=null
   var ret=[]
   for (var i in roles){
      if (roles[i]!=null)
         ret[ret.length]=i
   }
   return ret;
}

function indiceDeElementoEn(elem, array){
   for (var r=0; r<array.length; r++) {
      if (array[r]==elem)
         return r
   }
   return -1
}
// ------------------- Flexible Content -----------------------------------------------------------------------------------------------------


function flexifyContent()  {
   $("body").css({overflow:"hidden"});
   $("html").css({overflow:"hidden"});
   fixSizesAndPosition()
   $(window).bind("resize",function (ev){
      if (window.waitingForResizeDaemon != null){
         window.clearTimeout(window.waitingForResizeDaemon)
      }  
      window.waitingForResizeDaemon=setTimeout(function (){ 
         window.waitingForResizeDaemon=null;
         fixSizesAndPosition();
      },50)
   })
   $("#splitter").draggable({axis:"x", containment:"#bodyContainer", stop:function (ev, ui)  {
      var xoffset=ui.position.left
      $("#leftPanel").width( xoffset)
      $("#rightPanel").css({left: xoffset+4}).width(getScreenSize().w- 4 - $("#leftPanel").width())
   }})
}

function getScreenSize(){
   /*
   var _body=$("body")
   var _html=$("html")
   var _window=$(window)
   _savedHtmlOverflow = _html.css("overflow");
   _savedBodyOverflow = _body.css("overflow");
   _html.css({overflow:"hidden"});
   _body.css({overflow:"hidden"});
   var globalWidth=_window.width()
   var globalHeight=_window.height()
   _html.css({overflow:_savedHtmlOverflow});
   _body.css({overflow:_savedBodyOverflow});
   return {w:globalWidth,h:globalHeight}
   */
   var _window=$(window)
   return {w:_window.width(),h:_window.height()}
}

function fixSizesAndPosition(){
   var screenSize=getScreenSize()
   $("#rightPanel").width(screenSize.w - 4 - $("#leftPanel").width())
   $("#bodyContainer").height(screenSize.h - 34);
   $("#leftPanelTree").height(screenSize.h - 58);
   $("#rightPanelForm").height(screenSize.h - 133);
}

 // ------------------- DWR -----------------------------------------------------------------------------------------------------

 
function iniciaBloqueoComunicaciones() {
   //$("#loadingMsg").css({display:"inline"});
   window.dr_comunicando=true 
}
function comunicando(){
   return (window.dr_comunicando==true)
}
function terminaBloqueoComunicaciones() {
   //$("#loadingMsg").css({display:"none"});
   window.dr_comunicando=false
}
function msgErrorComunicaciones() { 
   alert( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt54"}) )//"La acci�n no se ha podido realizar porque existe una comunicaci�n en curso."
}

var conDwrConnectionTimeout=15000;

function getDataAndCreateTree() {
   if (comunicando())   {
      msgErrorComunicaciones();
		return 
   }
   iniciaBloqueoComunicaciones()
   SecurityService.getSecurityObject( {
      async:true 
      ,callback:getDataAndCreateTree_callback
      ,timeout:conDwrConnectionTimeout
      ,errorHandler:function(msg){
         //drWindow.showMessage({message:msg})
         alert(msg);
         drWindow.show("Ha ocurrido un error.");
         terminaBloqueoComunicaciones();
      }
   });
}

function getDataAndCreateTree_callback(obj)  {
   if (obj!=null  && obj.errorDeOperacion)	{
      show( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt55"}) );//"Ha ocurrido un error."
		alert( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt56"}) +obj.excepcionError.message+"'") //"Se ha producido un error t�cnico.\nMensaje de error: '"
      terminaBloqueoComunicaciones();
      return
	}
   terminaBloqueoComunicaciones();
   window.securityData=obj.securityConfiguration;
   window.securityData_restore=creaCopiaDatosSeguridad(window.securityData)
   createTree()
}

function show(txt)   {
   $("#textLabel").html(txt)
}

function saveTreeData (){
   if (drWindow.showConfirm({message: drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt57"}) })) { //"�Confirma que desea salvar definitivamente los cambios en el fichero de seguridad?"
      if (comunicando())   {
         msgErrorComunicaciones();
         return 
      }
      iniciaBloqueoComunicaciones()
      SecurityService.setSecurityObject(window.securityData,{
         async:true 
         ,callback:saveTreeData_callback
         ,timeout:conDwrConnectionTimeout
         ,errorHandler:function(msg){
            alert(msg);
            //drWindow.showMessage({message:msg})
            show("Ha ocurrido un error.");
            terminaBloqueoComunicaciones();
         }
      });
   }
}

function saveTreeData_callback(obj)  {
   if (obj!=null  && obj.errorDeOperacion)	{
      show( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt55"}) );//"Ha ocurrido un error."
		alert( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt56"}) +obj.excepcionError.message+"'") //"Se ha producido un error t�cnico.\nMensaje de error: '"
      terminaBloqueoComunicaciones();
      return
	}
   terminaBloqueoComunicaciones();
   show( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt58"}) ); //"Los datos se han salvado correctamente."
   drWindow.showMessage({message: drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt58"}) }) //"Los datos se han salvado correctamente."
   restauraCamposPassword()
}