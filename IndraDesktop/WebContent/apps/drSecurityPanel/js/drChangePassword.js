$(function(){

   $("#userForm").data("userData",{});
   preparaFormularios();
   //flexifyContent(fixSizesAndPosition)
   fixSizesAndPosition();
   preparaBotones();
   getUserHasPassword();

   //$(window).bind("resize", function(){alert("resize")})

   /*

   getDataAndCreateTree();
   ocultaFormularios();
   preparaFormularios();
   flexifyContent();
   prepareButtons();
   prepareLists();
   prepareDialogBoxes();
   */

   //$("#userForm, #constrForm").css({display:"none"})
})

function preparaBotones(){
   $("#btnSaveChanges").hover(function(){$(this).addClass("hover")}, function(){$(this).removeClass("hover")})
   $("#btnSaveChanges").click(saveChanges);
}

function saveChanges()  {
   if (window.campoEditandose!=null)
      $(window.campoEditandose).change();
   if (!verificaCamposCorrectos())
      return
   saveData();
}

function verificaCamposCorrectos()  {
   var userData=$("#userForm").data("userData")
   if (userData.hasPassword && (userData.password==null||userData.password=="") ){
      alert( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt66"}) ) // "Old Password" field cannot be empty.
      $("#user_oldPasswd").get(0).focus();
      return false
   } 
   if (userData.confirmPassword!=userData.newPassword){
      alert( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt67"})) //"No se ha podido salvar la informaci�n. El campo 'New Password' y el campo 'Confirm New Password' del usuario '" "' no coinciden."
      $("#user_newPasswd, #user_confirmPasswd").val("").change();
      $("#user_newPasswd").get(0).focus();
      return false
   }
   return true;
}

function preparaFormularios() {


   //Los campos a medio actualizar a veces hay que actualizarlos (p.ej antes de salvar)
   $("#user_newPasswd, #user_confirmPasswd, #user_oldPasswd").focus(function(ev){ window.campoEditandose=this })

   //password
   /*
   $("#btnChangePasswd").click(function (){
      $("#user_oldPasswd_block, #user_newPasswd_block, #user_confirmPasswd_block").css({display:"block"})
      $("#btnChangePasswdContainer").css({display:"none"})
   })
   $("#btnSetPasswd").click(function (){
      $("#user_newPasswd_block, #user_confirmPasswd_block").css({display:"block"})
      $("#btnSetPasswdContainer").css({display:"none"})
   })*/

   $("#user_newPasswd").change(function(){
      $("#userForm").data("userData").newPassword=quitaEspacios($(this).val())
   })
   $("#user_confirmPasswd").change(function(){
      $("#userForm").data("userData").confirmPassword=quitaEspacios($(this).val())
   })
   $("#user_oldPasswd").change(function(){
      $("#userForm").data("userData").password=quitaEspacios($(this).val())
   })

}

function getUserHasPassword_callback(obj){
   if (obj!=null  && obj.errorDeOperacion)	{
		alert( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt56"}) +obj.excepcionError.message+"'") //"Se ha producido un error t�cnico.\nMensaje de error: '"
      terminaBloqueoComunicaciones();
      return
	}
   terminaBloqueoComunicaciones();
   $("#userForm").data("userData", {hasPassword:obj.hasPassword})
   mapeaDatosUsuarioAFormulario()
}

function mapeaDatosUsuarioAFormulario(userData){
   var userData=$("#userForm").data("userData")
   if (userData.hasPassword){
      $("#user_oldPasswd_block").css({"display":"block"})
      $("#user_oldPasswd").get(0).focus()
   } else {
      $("#user_oldPasswd_block").css({"display":"none"})
      $("#user_newPasswd").get(0).focus()
   }
   limpiaFormulario();
}

function limpiaFormulario(){
   $("#user_oldPasswd, #user_newPasswd, #user_confirmPasswd").val("").change();
}

function fixSizesAndPosition(){
   $("body").css({overflow:"hidden"});
   $("html").css({overflow:"hidden"});
   var screenSize=getScreenSize()
   //$("#rightPanelForm").height(screenSize.h - $("#globalTitle").height() - $("#rightPanelTitle").height() - $("#rightPanelButtons").height() );  
   $("#rightPanelForm").height(screenSize.h - 93 ); 
}

// COMUNICACIONES: getUserHasPassword -------------------------------

function getUserHasPassword() {
   if (comunicando())   {
      alert( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt54"}) ) //"La acci�n no se ha podido realizar porque existe una comunicaci�n en curso."
		return 
   }
   iniciaBloqueoComunicaciones()
   SecurityService.getUserHasPassword( {
      async:true 
      ,callback:getUserHasPassword_callback
      ,timeout:conDwrConnectionTimeout
      ,errorHandler:function(msg){
         alert(msg);
         terminaBloqueoComunicaciones();
      }
   });
}

//COMUNICACIONES: saveData

function saveData (){
   var userData=$("#userForm").data("userData")
   var newPassword,password;
   if (userData.hasPassword){
      password=userData.password;
   }
   newPassword=userData.newPassword;
   if (comunicando())   {
      msgErrorComunicaciones();
      return 
   }
   iniciaBloqueoComunicaciones()
   SecurityService.setPassword(password,newPassword,{
      async:true 
      ,callback:saveData_callback
      ,timeout:conDwrConnectionTimeout
      ,errorHandler:function(msg){
         alert(msg);
         terminaBloqueoComunicaciones();
         limpiaFormulario();
      }
   });
   
}

function saveData_callback(obj)  {
   if (obj!=null  && obj.errorDeOperacion)	{
		alert( drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt56"}) +obj.excepcionError.message+"'") //"Se ha producido un error t�cnico.\nMensaje de error: '"
      terminaBloqueoComunicaciones();
      limpiaFormulario();
      return
	}
   terminaBloqueoComunicaciones();
   drWindow.showMessage({message: drResources.get({bundle:"drSecurityPanel",key:"secPanel.txt58"}) }) //"Los datos se han salvado correctamente."

   $("#userForm").data("userData", {hasPassword:true})
   mapeaDatosUsuarioAFormulario()
}