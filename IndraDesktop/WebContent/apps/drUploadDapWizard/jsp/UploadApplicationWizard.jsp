<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"  autoFlush="true"  buffer="none" %>
<%@ page import="es.indra.druida.desktop.apps.uploadAppWizard.UploadApplicationWizard" %>
<%@page import="es.indra.druida.desktop.configurator.ResourcesService" %>
<%@page import="java.util.ResourceBundle" %>
         
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
   <SCRIPT type='text/javascript' src='../../../config/domain.js'></SCRIPT><!-- DOMAIN -->
<SCRIPT type="text/javascript" src="../../../drDesktop/js/DrDesktop_resources.js"></SCRIPT>
<SCRIPT type="text/javascript" src="../../../drDesktop/js/DrDesktop_ventanas.js"></SCRIPT>
<SCRIPT type="text/javascript" src="../../../drDesktop/js/DrJson.js"></SCRIPT>
<SCRIPT type="text/javascript" src="../../../drDesktop/js/jquery.js"></SCRIPT>
<STYLE>
	body {font-family: "Courier New";font-size: 12px;color:white;background: black; }
	.targetName {color:#7c7cff}
	.taskName {color:#7c7cff}
	.errorMessage {color:red}	
	a {text-decoration:none;font-family: "Courier New";font-size: 12px;color:white;}
	a:hover {color:#FFCC00;}
</STYLE>
<%
	UploadApplicationWizard uploadAppWiz=new UploadApplicationWizard();
	Boolean correcto=uploadAppWiz.procesaUploadFichero(request);
	ResourceBundle bundle=ResourcesService.getResourceBundle("core",request);
	
	if (!correcto)	{
%><%=bundle.getString("drUploadDapWizard.txt2")%> <br><br><% 
	} else {
		%><%=bundle.getString("drUploadDapWizard.txt3")%> <br><br><%
		
		uploadAppWiz.ejecutaScriptSobreFichero(out, bundle);
	}
	
%><br>
<%=bundle.getString("drUploadDapWizard.txt4")%> <a href="#" onmousedown="drWindow.close()">[<%=bundle.getString("drUploadDapWizard.txt5")%>]</a> <%=bundle.getString("drUploadDapWizard.txt6")%> <a href="#" id="documentEnd" onmousedown="drWindow.sessionSaveAndRestart()">[<%=bundle.getString("drUploadDapWizard.txt7")%>]</a>.
<br>
<br>
<script>
	$("#documentEnd").get(0).focus();
</script>

