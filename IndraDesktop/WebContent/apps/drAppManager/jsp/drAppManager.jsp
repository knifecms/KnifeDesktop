<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"  autoFlush="true"  buffer="1kb" %>
<%@ page import="es.indra.druida.desktop.configurator.DesktopConfig" %>
<%@ page import="es.indra.druida.desktop.ant.AntJspLauncher" %>
<%@ page import="org.apache.tools.ant.Project" %>
<%@ page import="java.util.HashMap" %>
<%@page import="es.indra.druida.desktop.configurator.ResourcesService" %>
<%@page import="java.util.ResourceBundle" %>
<%
	ResourceBundle bundle=ResourcesService.getResourceBundle("core",request);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<SCRIPT type='text/javascript' src='../../../config/domain.js'></SCRIPT><!-- DOMAIN -->
<SCRIPT type="text/javascript" src="../../../drDesktop/js/DrDesktop_resources.js"></SCRIPT>
<SCRIPT type="text/javascript" src="../../../drDesktop/js/DrDesktop_ventanas.js"></SCRIPT>
<SCRIPT type="text/javascript" src="../../../drDesktop/js/DrJson.js"></SCRIPT>
<STYLE>
	body {font-family: "Courier New";font-size: 12px;color:white;background: black; }
	.targetName {color:#7c7cff}
	.taskName {color:#7c7cff}
	.errorMessage {color:red}	
	.errorMessage {color:red}	
	a {text-decoration:none;font-family: "Courier New";font-size: 12px;color:white;}
	a:hover {color:#FFCC00;}
</STYLE>

	<%=bundle.getString("drAppManager.txt2_1")%><br><br><%

	//PARAMETROS
	String appName=(String)request.getParameter("appName");

	DesktopConfig dConfig= DesktopConfig.getInstance();
	AntJspLauncher antLauncher= new AntJspLauncher();
	int messageLevel=Project.MSG_INFO;
	//int messageLevel=Project.MSG_VERBOSE; // MSG_ERR, MSG_WARN, MSG_INFO, MSG_VERBOSE, MSG_DEBUG
	String buildFileName=dConfig.get("scriptDir")+"/drAppManager.ant.xml";
	HashMap<String, String> props=new HashMap<String, String>();
	props.put("appName",appName);
	props.put("applicationsXmlDir",dConfig.get("appDefRoot")); 
	props.put("applicationsDir",dConfig.get("appRoot"));
	props.put("iconsDir",dConfig.get("iconsDir") ); 
	props.put("appRegisterFile",dConfig.get("root")+"/WEB-INF/drDesktop-registeredApp.xml" );
	props.put("skinsDir",dConfig.get("skinsDir") ); 
	props.put("skinsFile",dConfig.get("skinsFile") );
	props.put("helpDir",dConfig.get("helpDir") );
	String classesDir=dConfig.get("classesDir");
	if (classesDir==null)
		props.put("resourcesDir",dConfig.get("root")+"/WEB-INF/classes/resources" );
	else
		props.put("resourcesDir",classesDir+"/resources" );
	
	//I18N
	props.put("drAppManager.scriptTxt1", bundle.getString("drAppManager.scriptTxt1"));
	props.put("drAppManager.scriptTxt2", bundle.getString("drAppManager.scriptTxt2"));
	props.put("drAppManager.scriptTxt3", bundle.getString("drAppManager.scriptTxt3"));	
	
	
	antLauncher.setBuildFileName(buildFileName);
	antLauncher.setMessageOutputlevel(messageLevel);
	antLauncher.setProperties(props);
	antLauncher.setJspOut(out);
	//Boolean ok=antLauncher.launchDefaultTarget(buildFileName, props, out, messageLevel);
	Boolean ok=antLauncher.launch();

%><br>

<%=bundle.getString("drAppManager.txt3")%> <a href="#" onmousedown="drWindow.close()">[<%=bundle.getString("drAppManager.txt4")%>]</a> <%=bundle.getString("drAppManager.txt5")%> <a href="#" id="finDelDocumento" onmousedown="drWindow.sessionSaveAndRestart()">[<%=bundle.getString("drAppManager.txt6")%>]</a>.
<br>
<br>

<script>
	document.getElementById("finDelDocumento").focus();
</script>

