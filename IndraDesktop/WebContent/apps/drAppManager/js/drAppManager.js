$(document).ready ( function () {
   elOnload();
});

function elOnload()  {

   //Lista de grupos ----------
   losDatos=construyeDatosparaLaListaDeGrupos()
   $('#laListaGrupos').flexigrid({
      width:'auto'
      ,height:92
      ,title: drResources.get({bundle:"core",key:"drAppManager.txt7"})
      ,singleSelect: true
      ,colModel : [
         {display: 'ID', name : 'id', width : 135, sortable : true, align: 'left'}
         ,{display: drResources.get({bundle:"core",key:"drAppManager.txt8"}) , name : 'descripcion', width : 336, sortable : true, align: 'left'}
      ]
      ,resizable:true
      ,onRowSelected: laLista_onClick
   });
   //Meto los datos
   $('#laListaGrupos').get(0).grid.addDataBeanArray(losDatos)

   //Lista de aplicaciones ----------
   $('#laListaAplicaciones').flexigrid({
      width:'auto'
      ,height:92
      ,title:  drResources.get({bundle:"core",key:"drAppManager.txt9"}) 
      ,singleSelect: true
      ,colModel : [
         {display: 'ID', name : 'id', width : 135, sortable : true, align: 'left'}
         ,{display:  drResources.get({bundle:"core",key:"drAppManager.txt8"}) , name : 'descripcion', width : 260, sortable : true, align: 'left'}
         ,{display:  drResources.get({bundle:"core",key:"drAppManager.txt10"}) , name : 'tipo', width : 80, sortable : true, align: 'left'}
      ]
      ,resizable:true
   });

   //Boton ---------------
   $("#btnBorrar").bind("click", btnBorrar_onClick)
}


function btnBorrar_onClick()  {
   if (window.grupoSeleccionado==null) {
      drWindow.showMessage({message: drResources.get({bundle:"core",key:"drAppManager.txt11"}) });
      }
   else  {
      var aplIds=[]
      for(var r=0; r<window.grupoSeleccionado.aplicaciones.length; r++)  {
         aplIds[aplIds.length]="\""+window.grupoSeleccionado.aplicaciones[r].id+"\""
         }
      if (drWindow.showConfirm({message: drResources.get({bundle:"core",key:"drAppManager.txt12"}) +" \""+window.grupoSeleccionado.id+"\" "+ drResources.get({bundle:"core",key:"drAppManager.txt13"}) +" ["+aplIds.join(',')+"]. "+ drResources.get({bundle:"core",key:"drAppManager.txt14"}) }))
         drWindow.openApplication({"application":"drAppManager_BajaApp", parameters:[{nombre:"appName", valor:window.grupoSeleccionado.id}]})
      }
}

function laLista_onClick (id, obj, objPadre, dataRow)	{
   window.grupoSeleccionado=dataRow
   $('#laListaAplicaciones').get(0).grid.setTitle( drResources.get({bundle:"core",key:"drAppManager.txt15"}) +" : "+dataRow.id)
   $('#laListaAplicaciones').get(0).grid.addDataBeanArray(dataRow.aplicaciones)
   $("#btnBorrar").html( drResources.get({bundle:"core",key:"drAppManager.txt16"}) +" \""+dataRow.id+"\"")
}


function construyeDatosparaLaListaDeGrupos() {
   //Tenemos drWindow.getApplicationsGroupsConfig() que retorna [{"name":"allexandria","descripcion":"Druida"}, ... ]
   //Tenemos drWindow.getApplicationsConfig() que retorna {"Instituto":{"description":"Pagina princip...","height":...,...,"grupo":0},...}"
   //Quiero [{"id":"allexandria","descripcion":"Druida", aplicaciones:[{id:"Instituto","descripcion":"Pagina princip..."}]}]
   var grupos=drWindow.getApplicationsGroupsConfig()
   var aplicaciones=drWindow.getApplicationsConfig()
   var skinsGrupos=drWindow.getSkins()
   var ret=[]
   for (var r=0; r<grupos.length; r++) {
      var grupo=grupos[r]
      ret[r]={id:grupo.name, descripcion:grupo.descripcion, aplicaciones:[]}
   }
   //for (var r=0; r<aplicaciones.length; r++)   {
   for (var id in aplicaciones)   {
      var aplicacion=aplicaciones[id]
      var indiceGrupo=aplicacion.grupo
      var listaAppsGrupo=ret[indiceGrupo].aplicaciones
      var descr=aplicacion.descripcion
      if (descr==null)
         descr="";
      if (descr=="" && aplicacion.title!=null)
         descr=aplicacion.title;
      if (descr=="" && aplicacion.iconText!=null)
         descr=aplicacion.iconText;
      ret[indiceGrupo].aplicaciones[ret[indiceGrupo].aplicaciones.length]={id:id, descripcion:descr, tipo:"application"}
   }
   for (var r=0; r<skinsGrupos.length; r++)   {
      var skin=skinsGrupos[r]
      ret[skin.group].aplicaciones[ret[skin.group].aplicaciones.length]={id:skin.id, descripcion:skin.description, tipo:"skin"}
   }
   return ret
}