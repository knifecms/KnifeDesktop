<%@ page import="es.indra.druida.desktop.configurator.checks.ConfigurationChecks" %>
<%@ page import="es.indra.druida.desktop.configurator.checks.CheckResult" %>
<%
	
	ConfigurationChecks configurationChecks=new ConfigurationChecks();
	configurationChecks.setRequest(request);
	CheckResult checkResult=configurationChecks.doDesktopConfigCheck();
	if (!checkResult.hasErrors())
		response.sendRedirect("DrDesktop_Login.jsp");
	else {
		String[] messages=checkResult.getCheckMessages();
		for (int r=0; r<messages.length; r++)
			System.out.println("ERROR:" +messages[r]);
	} 
	
%>
