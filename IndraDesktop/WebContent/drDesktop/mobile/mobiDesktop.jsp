<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="utf-8"
	import="es.indra.druida.desktop.configurator.DesktopUserConfig" 
	import="es.indra.druida.desktop.configurator.beans.Application"
	import="es.indra.druida.desktop.configurator.beans.Icon"
    import="es.indra.druida.desktop.configurator.DesktopConfig" 
	import="es.indra.druida.desktop.configurator.beans.Skin"
	import="es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader" 
	import="es.indra.druida.desktop.configurator.beans.ConfigProperty"
	import="java.io.File"
	import="java.util.Locale"
	import="es.indra.druida.desktop.configurator.ResourcesService"
	import="java.util.ResourceBundle"
	import="es.indra.druida.desktop.configurator.beans.AppGroup"
	import="es.indra.druida.desktop.configurator.beans.Application"
	import="es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader"
	import="es.indra.druida.desktop.mobile.MobileDesktop"
	
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%

	DesktopConfig _config = DesktopConfig.getSingleton();
	boolean estoyEnSesion=request.getSession().getAttribute("userConfig")!=null;
	ResourceBundle bundle=ResourcesService.getResourceBundle("core",request);
	if (estoyEnSesion)	{
		
		MobileDesktop.startDesktop(request, response, _config, pageContext);
		pageContext.setAttribute("iconosLength",((Object[])pageContext.getAttribute("iconos")).length);

	//Convertir cerrar en un link que solo existe aqui ¿? porque no queremos que target sea distinto

%><?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title><%=bundle.getString("mobileLogin.txt6")%></title>
		<style>
			body{background-color:#000000;color:#FFFFFF;font-family:verdana,arial;font-size:x-small}
			a { color:white; font-family:verdana,arial;font-size:x-small; text-decoration: none; }
			table {width:100%}
		</style>
	</head>
	<body>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<c:forEach items="${iconos}" step="2" var="icon" varStatus="status">
				<tr>
					<td align="center">
						<a href="${icon.url}" target="new"><img border="0" src="${icon.iconUrl}"></img></a><br/><c:out value="${icon.text}"/><br/>
					</td>
					<td align="center">
						<c:if test="${(status.index+1) lt iconosLength}">
							<a href="${iconos[status.index+1].url}" target="new"><img border="0" src="${iconos[status.index+1].iconUrl}"></img></a><br/><c:out value="${iconos[status.index+1].text}" /><br/>
						</c:if>
						<c:if test="${(status.index+1) ge iconosLength}">
							<a href="mobi?closeSession=true"><img border="0" src="<%= _config.get("urlIcons") %>/salir.gif"></img></a><br/><%=bundle.getString("mobileDesktop.txt1")%><br/>
						</c:if>						
					</td>
				</tr>
			</c:forEach>
			<c:if test="${iconosLength%2 eq 0}">
				<tr>
					<td align="center">
						<a href="mobi?closeSession=true"><img border="0" src="<%= _config.get("urlIcons") %>/salir.gif"></img></a><br/><%=bundle.getString("mobileDesktop.txt1")%><br/>
					</td>
					<td></td>
				</tr>
			</c:if>
		</table>

		<!-- 
		<a href="mobi?closeSession=true"><img border="0" src="<%= _config.get("urlIcons") %>/salir.gif"></img></a><br/><%=bundle.getString("mobileDesktop.txt1")%><br/>		
		 -->
		
   </body>
</html>

		<!--  link a la parte superior -->
		<%
	} else {
		%><%=bundle.getString("index.txt1")%><% //No esta en sesión
	}


String acceptHeader = request.getHeader("accept");
String contentType="text/html";

if (acceptHeader.indexOf("application/vnd.wap.xhtml+xml") != -1)
  contentType="application/vnd.wap.xhtml+xml";
else if (acceptHeader.indexOf("application/xhtml+xml") != -1)
  contentType="application/xhtml+xml";

response.setContentType(contentType);

%>