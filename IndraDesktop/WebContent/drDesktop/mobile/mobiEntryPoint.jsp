<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="utf-8"%>
<%@ page import="es.indra.druida.desktop.configurator.AbstractDesktopConfigurationClass"%>
<%@ page import="es.indra.druida.desktop.configurator.DesktopConfig" %>
<%@ page import="es.indra.druida.desktop.configurator.DesktopUserConfig" %>
<%

	DesktopConfig desktopConfig = DesktopConfig.getSingleton();
	// reflection
	AbstractDesktopConfigurationClass configClase = desktopConfig.invokeDesktopConfigurationClass(request, response);
	//configClase.process();

	boolean cerrarSesion=request.getParameter("closeSession")!=null;
	boolean heRecibidoUnLogin=request.getParameter("logar")!=null;

	if (cerrarSesion){
		request.getSession().removeAttribute("userConfig");
		%><jsp:include page="drDesktop/mobile/mobiLogin.jsp"/><%
	}else if (heRecibidoUnLogin){
		DesktopUserConfig newUser=configClase.createUser();
		if (newUser==null){
			%><jsp:include page="drDesktop/mobile/mobiAuthFailed.jsp"/><%
		}else{
			configClase.setUserObject(newUser);
			//generateUserFilesIfNecesary(); //Creo el directorio del usuario si no existe   ---------------------------------
			%><jsp:include page="drDesktop/mobile/mobiDesktop.jsp"/><%
		}
	} else if (configClase.isUserLogged()){
		%><jsp:include page="drDesktop/mobile/mobiDesktop.jsp"/><%
	} else	{//Si no he recibido un login y tampoco estoy en sesión pido login
		%><jsp:include page="drDesktop/mobile/mobiLogin.jsp"/><%
	}

%>