<%@ page import="es.indra.druida.desktop.configurator.ResourcesService" %><%@ page import="java.util.ResourceBundle" %><%
	ResourceBundle bundle=ResourcesService.getResourceBundle("core",request);
%><?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title><%=bundle.getString("mobileLogin.txt6")%></title>
		<style>
			body{background-color:#000000;color:#FFFFFF;font-family:verdana,arial;font-size:x-small}
			input{font-family:verdana,arial;font-size:x-small}
		</style>
	</head>
	<body>
		<br/>
		<form name="form1" method="post" action="mobi">
			<div align="center">
				<h2><%=bundle.getString("mobileLogin.txt1")%><%-- DESKTOP LOGIN:  --%></h2>
				<br/><%=bundle.getString("mobileLogin.txt5")%><%-- El usuario o la contraseņa no es correcta.  --%>
				<br/>
				<br/><%=bundle.getString("mobileLogin.txt2")%>:<%-- Usuario --%>
				<br/><input id="login" type="text" name="login" value="anonymous"/>
				<br/>
				<br/><%=bundle.getString("mobileLogin.txt3")%>:<%-- Clave: --%>
				<br/><input id="password" type="text" name="password"/>
				<br/>
				<br/><input id="Submit" name="Submit" type="submit" value="<%=bundle.getString("mobileLogin.txt4")%>"/>
				<br/><input type="hidden" name="logar"/>
			</div>
		</form>
	</body>
</html><%

	String acceptHeader = request.getHeader("accept");
	String contentType="text/html";

	if (acceptHeader.indexOf("application/vnd.wap.xhtml+xml") != -1)
	  contentType="application/vnd.wap.xhtml+xml";
	else if (acceptHeader.indexOf("application/xhtml+xml") != -1)
	  contentType="application/xhtml+xml";

	response.setContentType(contentType);

%>