<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ page import="org.json.JSONArray" %>
<%@ page import="org.json.JSONObject" %>
<!--  page import="es.indra.druida.desktop.DesktopVentanas" -->
<!--  page import="es.indra.druida.desktop.DesktopIconos"  -->
<!--  page import="es.indra.druida.desktop.DesktopFichConfigUsuario" -->

<%@ page import="es.indra.druida.desktop.configurator.DesktopConfig" %>
<%@ page import="es.indra.druida.desktop.configurator.DesktopUserConfig" %>
<%@ page import="es.indra.druida.desktop.utils.DesktopWriter" %>
<%@ page import="org.w3c.dom.Document" %>
<%@ page import="es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.Icon" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.Window" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.SkinChanges" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.converters.IconConverter" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.converters.WindowConverter" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.converters.CustomConfigurationConverter" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.converters.SkinChangesConverter" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.ConfigProperty" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%
   if (request.getSession().getAttribute("userConfig")!=null){
   DesktopUserConfig userConfig = (DesktopUserConfig) request.getSession().getAttribute("userConfig"); 
  
   boolean cerrarSesion=(request.getParameter("cerrarSesion")!=null && !((String)request.getParameter("cerrarSesion")).equals("N"));
   boolean guardar=(request.getParameter("guardar")!=null && !((String)request.getParameter("guardar")).equals("N"));
	
   String[] roles=userConfig.getRoles();
   if (roles!=null){
	   for (int i=0; i<roles.length && guardar; i++)
		   if (roles[i].toLowerCase().equals("nosessionsave"))
			   guardar=false;
   }

	if (guardar) {
		String jsonVentanas=request.getParameter("ventanas");
		String jsonIconos=request.getParameter("iconos");
		String jsonConfig=request.getParameter("configuracion");	
		String jsonSkin=request.getParameter("skinParameters");
		
		if (jsonVentanas!=null)	{ //VENTANAS
			Window[] windows=WindowConverter.jsonToBeanArray(jsonVentanas);
			AbstractLoader windowLoader=DesktopConfig.getSingleton().getWindowLoader(request, response);
			windowLoader.setObjects(windows);
		}
		if (jsonIconos!=null)	{ //ICONOS
			Icon[] icons=IconConverter.jsonToBeanArray(jsonIconos);
			AbstractLoader iconLoader=DesktopConfig.getSingleton().getIconLoader(request, response);
			iconLoader.setObjects(icons);
		}
		if (jsonConfig!=null)	{ //CONFIGURACIÓN
			ConfigProperty[] props=CustomConfigurationConverter.jsonToBeanArray(jsonConfig);
			AbstractLoader customConfigLoader=DesktopConfig.getSingleton().getCustomConfigurationLoader(request, response);
			customConfigLoader.setObjects(props);
		}	
		if (jsonSkin!=null)	{ //CONFIGURACIÓN SKIN
			SkinChanges[] cambios=new SkinChanges[1];
			cambios[0]=SkinChangesConverter.jsonToBean(jsonSkin);
			if (cambios[0]!=null)	{
				//AbstractLoader customConfigLoader=DesktopConfig.getSingleton().getSkinChangesLoader(request, response);
				AbstractLoader customLoader=DesktopConfig.getSingleton().getCustomSkinParametersLoader(request, response);
				customLoader.setObjects(cambios);
			}
		}	
   }
	//Y VACIAMOS LA SESIÓN
	if (cerrarSesion){
		//System.out.println("cerrarSesion["+request.getParameter("cerrarSesion")+"]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");		
		request.getSession().removeAttribute("userConfig");
	}
}
%>
<SCRIPT type='text/javascript' src='../../config/domain.js'></SCRIPT><!-- DOMAIN -->
<script>top.document.location.href= '../..'</script>