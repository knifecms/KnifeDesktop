<%@ page import="es.indra.druida.desktop.configurator.DesktopUserConfig" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<head>

	<LINK REL="stylesheet" TYPE="text/css" HREF="../temas/WindowsXP/css/estilo.css">
	
	<script language="JavaScript" src="../js/DrDesktop_funciones.js"></script>
	
	<LINK REL="stylesheet" TYPE="text/css" HREF="../css/DrDesktop_botones.css">
	
	<script language="JavaScript" src="../js/DrDesktop_botones.js"></script>	
	
	<script>  
	
		imgSrcBase='../img'
		window.losBotones=[
		  {texto:"Aceptar", js:"document.acceso.submit()", iconoN:"../img/drIcoAceptarN.gif", iconoS:"../img/drIcoAceptarS.gif"}
		  ,{texto:"Salir del Escritorio", js:"window.close();parent.close();" , iconoN:"../img/drIcoCancelarN.gif", iconoS:"../img/drIcoCancelarS.gif"}
		  ]
		registraIdBotones(losBotones)
	
	
	function elOnload()	{
		document.acceso.username.focus()
		}
		  
	</script>

	
</head>


<BODY bgcolor="#8F4784" onload="elOnload()">

<form method="POST" name="acceso" action="../DrDesktop_index.jsp">
<!-- <input name="$jslink.ActionKey" type="hidden" value="$config.getString("action.login")">  -->

<table bgcolor="#8F4784" width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">

	<tr>
		<td width="33%" height="33%"></td>
		<td width="33%" height="33%"></td>
		<td width="33%" height="33%"></td>
	</tr>

	<tr>
		<td width="33%" height="33%"></td>

		<td width="33%" height="33%">
			<table width="485" border="3" cellspacing="0" cellpadding="0" bordercolor="#6F1B62">
				<tr>
					<td>
						<table width="485" height="240" border="0" cellspacing="0" cellpadding="0" background="../temas/WindowsXP/images/login/drTbLogin2.gif" >
							<tr>
								<td rowspan="3"><img src="../temas/WindowsXP/images/login/drTbLogin1.gif" width="121" height="240"></img></td>
								<td width="3000" valign="middle"> 
									<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left">
										<tr>
											<td class="LoginTitulo">Bienvenido</td>
										</tr>
										<tr>
											<td height="15" nowrap class="LoginTitulo2"><b>Por favor, introduzca su usuario y clave de acceso.</b></td>
										</tr>
										<tr>
											<td background="../temas/WindowsXP/images/login/drTbLogin3.gif"><img src="../temas/WindowsXP/images/login/drTbLogin3.gif" width="3" height="9"></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="middle" height="100">
									<table border="0" cellspacing="0" cellpadding="0" align="center">
										<tr>
											<td height="30"><img src="../temas/WindowsXP/images/login/drTbLoginIco1.gif" width="78" height="27" align="absmiddle"></td>
											<td><input tabIndex=1 value="" type="text" name="username" maxlength="20" class="CajasTexto" tabindex="1" style="width:150;font-size:10" onkeydown="if(event.keyCode==13) window.setTimeout('document.acceso.password.focus()',10)"></td>
										</tr>
										<tr>
											<td height="30"><img src="../temas/WindowsXP/images/login/drTbLoginIco2.gif" width="78" height="27" align="absmiddle"></td>
											<td><input tabIndex=2 value="" name="password" class="CajasTexto" maxlength="20"  type="password" tabindex="0"  style="width:150; font-size:10" onkeydown="if(event.keyCode==13)document.acceso.submit()"></td>
										</tr>
										<tr height="15">
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td></td>
											<td  align="center"><A HREF="#" class="LoginTitulo3">Date de alta en el sistema</A></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr height="40">
								<td height="40" valign="middle" align="center">
									<!-- <img src="../temas/WindowsXP/images/login/s.gif" width="67" height="1"><input type="image" border="0" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('submit','','../temas/WindowsXP/images/login/drBtnAceptarS.gif',1)" name="submit" src="../temas/WindowsXP/images/login/drBtnAceptarN.gif" alt="Pulse para acceder a la aplicación"> -->
								
								      <script>      	document.write(generaHtmlBotones(losBotones))      	</script>
								
								</td>
							</tr>
						</table>
					</td>
					</tr>
				</table>
			</td>

		<td width="33%" height="33%"></td>
	</tr>

	<tr>
		<td width="33%" height="33%"></td>
		<td width="33%" height="33%"></td>
		<td width="33%" height="33%"></td>
	</tr>

</table>

</form>

</BODY>
</HTML>