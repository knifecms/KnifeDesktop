<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="javax.servlet.http.*"%>
<%@ page import="es.indra.druida.desktop.configurator.DesktopConfig"%>
<%@ page import="es.indra.druida.desktop.configurator.checks.ConfigurationChecks"%>
<%@ page import="java.io.FileInputStream"%>
<%@ page import="java.io.BufferedReader"%>
<%@ page import="java.io.BufferedWriter"%>
<%@ page import="java.io.FileWriter"%>
<%@ page import="java.io.InputStreamReader"%>
<%@ page import="java.lang.StringBuffer" %>
<%@page import="es.indra.druida.desktop.configurator.ResourcesService" %>
<%@page import="java.util.ResourceBundle" %>
<%@page import="es.indra.druida.desktop.utils.DesktopWriter" %>
<%

String fase="1";
if (request.getParameter("fase")!=null)
	fase=(String)request.getParameter("fase");

if (fase.equals("1")){ // -------------------------------------- FASE 1
	if (getClass().getResource("/DrDesktop.properties")==null) { 
		ResourceBundle bundle=ResourcesService.getResourceBundle("repairConfig",request);
		%><%=bundle.getString("repairConfig_jsp.txt1")%><%
	}else{
		ServletContext cont= request.getSession().getServletContext();
		String rutaProperties=getClass().getResource("/DrDesktop.properties").getPath();
		int i=rutaProperties.lastIndexOf("/WEB-INF");
		String rootPath=rutaProperties.substring(0,i);
		String contextName=cont.getServletContextName();
		String defaultTmpDir=DesktopConfig.getSingleton().get("tmpDir");
		int i2=rutaProperties.lastIndexOf("/DrDesktop.properties");		
		String classesDir=rutaProperties.substring(0,i2);
		if (defaultTmpDir==null){
			defaultTmpDir="/var/tmp";
		}
		%><%@ include file="repairConfig.html" %><%
		%><SCRIPT>
			$("#root").val("<%=rootPath%>");
			$("#contextName").val("<%=contextName%>");
			$("#propertiesFile").val("<%=rutaProperties%>");
			$("#tmpDir").val("<%=defaultTmpDir%>");									
			$("#classesDir").val("<%=classesDir%>");				
			$("#getServerInfo").html("<%=cont.getServerInfo()%>");	
			$(".contextNameLabel").html("<%=contextName%>");
		</SCRIPT><%
	}
	
	
	
}else if (fase.equals("2")){ // -------------------------------------- FASE 2
	
	String root=null;
	String contextName=null;
	String propertiesFile=null;	
	String tmpDir=null;
	String classesDir=null;
	
	if(request.getParameter("root")!=null)
		root=request.getParameter("root");
	if(request.getParameter("contextName")!=null)
		contextName=request.getParameter("contextName");
	if(request.getParameter("propertiesFile")!=null)
		propertiesFile=request.getParameter("propertiesFile");
	if(request.getParameter("tmpDir")!=null)
		tmpDir=request.getParameter("tmpDir");	
	if(request.getParameter("classesDir")!=null)
		classesDir=request.getParameter("classesDir");		
	
	ConfigurationChecks configurationChecks=new ConfigurationChecks();
	configurationChecks.setRequest(request);	

	configurationChecks.verificaRutaExiste(root, "Related desktop property: \"root\".");
	configurationChecks.verificaRutaExiste(root+"/WEB-INF", "Related desktop property: \"root\".");
	configurationChecks.verificaRutaExiste(root+"/WEB-INF/templates/newUser", "Related desktop property: \"root\".");
	configurationChecks.verificaRutaExiste(tmpDir, "Related desktop property: \"tmpDir\".");
	configurationChecks.verificaFicheroExiste(propertiesFile, "This configuration file must exist.");	
	configurationChecks.verificaFicheroExiste(root+"/WEB-INF/templates/DrDesktop.properties.template", "Related desktop property: \"root\".");
	configurationChecks.verificaRutaExiste(classesDir, "Related desktop property: \"classesDir\".");
	configurationChecks.verificaRutaExiste(classesDir + "/resources", "Related desktop property: \"classesDir\".");	

	if (configurationChecks.checkResult.hasErrors())	{
		ServletContext cont= request.getSession().getServletContext();
		
		%><%@ include file="repairConfig.html" %><%
		%><SCRIPT>var msgs=[<%
		                    
		  String[] messages=configurationChecks.checkResult.getCheckMessages();
        for (int r=0; r<messages.length; r++){
  			if (r>0)
  				out.print(","); 	  
  			out.print(DesktopWriter.generaCadenaJavascript(messages[r],true));
        }
        
  		%>];writeMessages(msgs);
			$("#root").val("<%=root%>");
			$("#contextName").val("<%=contextName%>");
			$("#propertiesFile").val("<%=propertiesFile%>");
			$("#tmpDir").val("<%=tmpDir%>");			
			$("#classesDir").val("<%=classesDir%>");											
			$("#getServerInfo").html("<%=cont.getServerInfo()%>");	
			$(".contextNameLabel").html("<%=cont.getServletContextName()%>");	  		
  		</SCRIPT><%
  		
	}else{
		
		String line;
		FileInputStream fis = new FileInputStream(root+"/WEB-INF/templates/DrDesktop.properties.template");
		BufferedReader reader=new BufferedReader ( new InputStreamReader(fis));
		StringBuffer sb = new StringBuffer();
		while((line = reader.readLine()) != null) {
			line = line.replaceAll("%root%",root).replaceAll("%contextName%",contextName).replaceAll("%tmpDir%",tmpDir).replaceAll("%classesDir%",classesDir);
			sb.append(line+"\n");
		}
		reader.close();
		BufferedWriter outbuff=new BufferedWriter ( new FileWriter(propertiesFile));
		outbuff.write(sb.toString());
		outbuff.close();
		%><%@ include file="repairConfigEnd.html" %><%
		%><SCRIPT>
			$(".serverInfo").html("<%=request.getSession().getServletContext().getServerInfo()%>");				
		</SCRIPT><%		
	}
	
}

%>