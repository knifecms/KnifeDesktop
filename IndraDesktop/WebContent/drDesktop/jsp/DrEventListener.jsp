<%@page import="es.indra.druida.events.ScriptEventListener"%>
<%@page import="es.indra.druida.desktop.configurator.ResourcesService" %>
<%@page import="java.util.ResourceBundle" %>
<%@page import="es.indra.druida.desktop.utils.DesktopWriter" %>
<%
	String script=null;
	Object oId=request.getParameter("id");
	Object oXml=request.getParameter("xml");
	//TO-DO: xml por defecto
	if (oId==null || oXml==null)	{
		ResourceBundle bundle=ResourcesService.getResourceBundle("core",request);
		script="alert("+DesktopWriter.generaCadenaJavascript(bundle.getString("eventListener.txt1"))+");";	
	}
	else	{
		//script=scriptEventListener.generateRegisterScript((String)oXml,(String)oId );
		//System.out.println(getServletContext().getRealPath("/")+"\WEB-INF");
		String realXmlPath=getServletContext().getRealPath("/")+"/WEB-INF/"+(String)oXml;
		script=ScriptEventListener.getInstance().generateRegisterScript(realXmlPath,(String)oId);
	}
%>
<%=script%>