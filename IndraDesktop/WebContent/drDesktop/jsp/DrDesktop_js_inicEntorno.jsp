<%@ page import="es.indra.druida.desktop.configurator.beans.ConfigProperty"%>
<%@ page import="es.indra.druida.desktop.configurator.DesktopConfig" %>


<%@ page import="java.io.File"%>
<%@ page import="es.indra.druida.desktop.DesktopAplicaciones" %>
<!-- page import="es.indra.druida.desktop.DesktopFichConfigUsuario" -->
<!--  @ page import="es.indra.druida.desktop.DesktopFichConfigGeneral"  -->
<%@ page import="es.indra.druida.desktop.utils.DesktopWriter" %>
<%@ page import="es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.Window" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.Skin" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.Configuration" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.ConfigProperty" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.converters.WindowConverter" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.converters.SkinConverter" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.converters.ConfigurationConverter" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.converters.CustomConfigurationConverter" %>

<%
	//--- CONFIGURACIÓN DEL ESCRITORIO
	//String oDesktopConfig=DesktopFichConfigUsuario.getScriptObject(request.getSession());
	ConfigProperty[] props=null;
	Object oConfigProperties= request.getSession().getAttribute("configProperties");
	if (oConfigProperties!=null){
		props=(ConfigProperty[])oConfigProperties;
	}else{
		AbstractLoader configLoader=DesktopConfig.getSingleton().getCustomConfigurationLoader(request, response);
		props=(ConfigProperty[])configLoader.getObjects();
	}
	String jsonCustomConfig=CustomConfigurationConverter.beanArrayToJson(props);

	//--- VENTANAS INICIALES
	AbstractLoader windowLoader=DesktopConfig.getSingleton().getWindowLoader(request, response); 
	Window[] windows=(Window[])windowLoader.getObjects();
	String jsonWindows=WindowConverter.beanArrayToJson(windows);
	//--- Información de los SKINS
	Skin[] skins = DesktopConfig.getSingleton().getSkins();
	String jsonSkins=SkinConverter.beanArrayToJson(skins);
	// --- Custom Skin Parameters
	AbstractLoader cSPLoader=DesktopConfig.getSingleton().getCustomSkinParametersLoader(request, response); 
	Skin[] customizedSkins=(Skin[])cSPLoader.getObjects();
	String jsonCustomSkins=SkinConverter.beanArrayToJson(customizedSkins);
	//oMotores y oTipos
	String jsonGlobalProperties=ConfigurationConverter.beanToJson( DesktopConfig.getSingleton().getConfiguration() );
	
%>

oDesktopConfig=<%=jsonCustomConfig%>

function generarVentanasIniciales()	{
	var vRecuperar=<%=jsonWindows%>
	drWindow.openWindowArray({windows:vRecuperar, startingSession:true});

}

oSkins = <%=jsonSkins%>
oCustomSkins = <%=jsonCustomSkins%>
actualizaSkinsConValoresCustom();

// **** APLICACIONES EXISTENTES
<%	DesktopAplicaciones.writeScriptObject(out,request); //oAplicaciones, oGruposAplicaciones %>

// --- MOTORES EXISTENTES
oGlobalProperties = <%=jsonGlobalProperties%>
oMotores = oGlobalProperties.engines;
oTipos = oGlobalProperties.fileTypes;

// --- otras propiedades
oDesktopProperties={
	root:<%=DesktopWriter.generaCadenaJavascript(DesktopConfig.getSingleton().get("root"))%>
	,userRoot:<%=DesktopWriter.generaCadenaJavascript(DesktopConfig.getSingleton().get("userRoot"))%>
	,appDefRoot:<%=DesktopWriter.generaCadenaJavascript(DesktopConfig.getSingleton().get("appDefRoot"))%>
	,urlIcons:<%=DesktopWriter.generaCadenaJavascript(DesktopConfig.getSingleton().get("urlIcons"))%>
	,urlSkins:<%=DesktopWriter.generaCadenaJavascript(DesktopConfig.getSingleton().get("urlSkins"))%>
	,urlHelpRoot:<%=DesktopWriter.generaCadenaJavascript(DesktopConfig.getSingleton().get("urlHelpRoot"))%>	
	,urlAppRoot:<%=DesktopWriter.generaCadenaJavascript(DesktopConfig.getSingleton().get("urlAppRoot"))%>	
	,urlUserRoot:<%=DesktopWriter.generaCadenaJavascript(DesktopConfig.getSingleton().get("urlUserRoot"))%>	
}
