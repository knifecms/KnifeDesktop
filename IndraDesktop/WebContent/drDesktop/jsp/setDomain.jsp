<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" import="es.indra.druida.desktop.configurator.DesktopConfig" import="es.indra.druida.desktop.utils.DesktopWriter" %><%

	try	{
		String scriptDomain=DesktopConfig.getSingleton().get("scriptDomain");
		if (scriptDomain!=null){
			%>document.domain="<%=scriptDomain%>";<%
		}
	} catch (Exception e)	{
		%>alert(<%= DesktopWriter.generaCadenaJavascript("Technical Error setting domain: "+ e.getMessage(), true) %>)<%
	}

%>