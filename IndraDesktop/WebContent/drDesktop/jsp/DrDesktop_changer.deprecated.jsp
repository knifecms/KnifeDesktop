<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="es.indra.druida.desktop.configurator.DesktopConfig" %>
<html>
<head>
   <meta http-equiv="Content-Language" content="es">
   <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<!-- 
   <link rel="stylesheet" type="text/css" href="temas/WindowsXP/css/style.css">
   
   <LINK REL="stylesheet" TYPE="text/css" HREF="xDesktop/x-desktop.css">
 -->
<!-- 
   <script language="Javascript1.2" type="text/javascript">

       var LOADED = 0;
       var LOADMESSAGE = "Cross-Browser Core Library";
       
   </script>
 -->

   <!-- librer�as xdesktop 
   <script type='text/javascript' src='xDesktop/cbe_core.js'></script>
   <script>LOADMESSAGE = "Cross-Browser Event Library";</script>
   <script type='text/javascript' src='xDesktop/cbe_event.js'></script>
   <script>LOADMESSAGE = "Cross-Browser Slide Library";</script>
   <script type='text/javascript' src='xDesktop/cbe_slide.js'></script>
   <script>LOADMESSAGE = "x-desktop WIN skin";</script>
   <script type='text/javascript' src='xDesktop/x-desktop_skin_WIN.js'></script>
   <script>LOADMESSAGE = "x-desktop OSX skin";</script>
   <script type='text/javascript' src='xDesktop/x-desktop_skin_OSX.js'></script>
   <script>LOADMESSAGE = "x-desktop OSXA skin";</script>
   <script type='text/javascript' src='xDesktop/x-desktop_skin_OSXA.js'></script>
   <script>LOADMESSAGE = "x-desktop KDE skin";</script>
   <script type='text/javascript' src='xDesktop/x-desktop_skin_KDE.js'></script>
   <script>LOADMESSAGE = "x-desktop GNOME skin";</script>
   <script type='text/javascript' src='xDesktop/x-desktop_skin_GNOME.js'></script>
   <script>LOADMESSAGE = "x-desktop SBO skin";</script>
   <script type='text/javascript' src='xDesktop/x-desktop_skin_SBO.js'></script>
   <script>LOADMESSAGE = "x-desktop XP skin";</script>
   <script type='text/javascript' src='xDesktop/x-desktop_skin_XP.js'></script>
   <script>LOADMESSAGE = "x-desktop AEON skin";</script>
   <script type='text/javascript' src='xDesktop/x-desktop_skin_AEON.js'></script>
   <script>LOADMESSAGE = "x-desktop REDHAT skin";</script>
   <script type='text/javascript' src='xDesktop/x-desktop_skin_REDHAT.js'></script>
   <script>LOADMESSAGE = "x-desktop NONE skin";</script>
   <script type='text/javascript' src='xDesktop/x-desktop_skin_NONE.js'></script>
   <script>LOADMESSAGE = "x-desktop SUSE skin";</script>
   <script type='text/javascript' src='xDesktop/x-desktop_skin_SUSE.js'></script>
   <script>LOADMESSAGE = "x-desktop SYSG skin";</script>
   <script type='text/javascript' src='xDesktop/x-desktop_skin_SYSG.js'></script>
   <script>LOADMESSAGE = "x-desktop IDOTS2 skin";</script>
   <script type='text/javascript' src='xDesktop/x-desktop_skin_IDOTS2.js'></script>
   <script>LOADMESSAGE = "x-desktop core library";</script>
   <script type='text/javascript' src='xDesktop/x-desktop_core.js'></script>
   <script>LOADMESSAGE = "x-desktop ICON skin";</script>
   <script type='text/javascript' src='xDesktop/x-desktop_skin_ICONO.js'></script>
   <script>LOADMESSAGE = "x-desktop inititializion";</script>-->

   <!-- Cargamos el men� -->
   <!--
   <script type='text/javascript' src='jsp/desktopGenerator.jsp'></script>
   <script>LOADMESSAGE = "cargamos el men� desplegable...";</script>
   

   <script type='text/javascript' src='DruidaDesktopJS/DruidaDesktopCore.js'></script>
   <script>LOADMESSAGE = "cargamos el core del Escritorio...";</script>


   <script type='text/javascript' src='DruidaDesktopJS/DruidaClock.js'></script>
   <script>LOADMESSAGE = "cargamos el reloj...";</script>
 -->
	<script type='text/javascript' src='../js/jquery.js'></script>
	<script type='text/javascript' src='../js/DrDesktop_ventanas.js'></script>

</head>

<body style="margin:0px;" onload="elOnload()">

   <script>
   
   	wEscritorio=window.top.f1
   
   	function elOnload()	{
   		
   		document.f1.modoSinErrores.value=wEscritorio.oDesktopConfig.modoSinErrores?"S":"N"
   		document.f1.mensajesDelSistema.value=wEscritorio.oDesktopConfig.mensajesDelSistema?"S":"N"
   		document.f1.animacionesActivas.value=wEscritorio.oDesktopConfig.animacionesActivas?"S":"N"
   		document.f1.navegacionRestringida.value=wEscritorio.oDesktopConfig.navegacionRestringida?"S":"N"
   		//document.f1.hayImgFondo.value=wEscritorio.oDesktopConfig.hayImgFondo?"S":"N"
   		//if (wEscritorio.oDesktopConfig.srcImgFondo)
   			//document.f1.srcImgFondo.value=wEscritorio.oDesktopConfig.srcImgFondo
		var skins=drWindow.getSkins();
		var html=[]
		html[html.length]='<SELECT size="1" name="tema" ONCHANGE="cambia_a(this.value) ">'
		for (var r=0; r<skins.length; r++)	{
			html[html.length]='<OPTION VALUE="'+DrEscHTML(skins[r].id)+'">'+DrEscHTML(skins[r].description)+'</OPTION>'
		}
		html[html.length]='</SELECT>'
		$("#skinListContainer").html(html.join(''))
		document.f1.tema.value=drWindow.getSkin(); //wEscritorio.oDesktopConfig.tema
   		}
   
      function cambia_a(_estilo){
      	// wEscritorio.xDT.desktop.skin(_estilo);
         // wEscritorio.oDesktopConfig.tema=_estilo;
         wEscritorio.temaTrasReiniciar=_estilo
     		if (wEscritorio.drCuadroConfirm("�Desea cerrar la sesi�n actual y volver a entrar para poder ver los cambios?", ["Si","No"]))	
     			wEscritorio.cerrarSesion(true)	
         }


	function DrEscHTML (cad) {
	   if (cad!=null)
		  return cad.replace(/&/gi,"&amp;").replace(/</gi,"&lt;").replace(/"/gi,"&quot;"); 
	   return "";
	   }

   </script>

	<form name="f1" action="">
	
		<table>
			<tr>
				<td>Tema de escritorio:</td>
				<td id="skinListContainer">
					<SELECT size="1" name="tema" ONCHANGE="cambia_a(this.value) ">
					</SELECT>
				</td>				
			</tr>
			<tr>
				<td>Modo de Depuraci�n:</td>
				<td>
					<SELECT size="1" name="modoSinErrores" ONCHANGE="wEscritorio.oDesktopConfig.modoSinErrores=this.value=='S'?true:false">
						<OPTION VALUE="N">Activado</OPTION> 
						<OPTION VALUE="S">Desactivado</OPTION> 
					</SELECT>
				</td>				
			</tr>	
			<tr>
				<td>Mensajes informativos del Sistema:</td>
				<td>
					<SELECT size="1" name="mensajesDelSistema" ONCHANGE="wEscritorio.oDesktopConfig.mensajesDelSistema=this.value=='S'?true:false">
						<OPTION VALUE="S">Activado</OPTION> 
						<OPTION VALUE="N">Desactivado</OPTION> 
					</SELECT>
				</td>				
			</tr>	
			<tr>
				<td>Animaciones:</td>
				<td>
					<SELECT size="1" name="animacionesActivas" ONCHANGE="wEscritorio.oDesktopConfig.animacionesActivas=this.value=='S'?true:false">
						<OPTION VALUE="S">Activado</OPTION> 
						<OPTION VALUE="N">Desactivado</OPTION> 
					</SELECT>
				</td>				
			</tr>
			<!-- 
			<tr>
				<td>Imagen de Fondo:</td>
				<td>
					<SELECT size="1" name="hayImgFondo" ONCHANGE="wEscritorio.oDesktopConfig.hayImgFondo=this.value=='S'?true:false">
						<OPTION VALUE="S">Personalizada</OPTION> 
						<OPTION VALUE="N">Por defecto</OPTION> 
					</SELECT>
					<input type="text"name="srcImgFondo" ONCHANGE="wEscritorio.oDesktopConfig.srcImgFondo=this.value"/>
				</td>				
			</tr>			
				 -->
			<tr>
				<td style="cursor:hand" title="si no est� activo la navegaci�n puede afectar al escritorio, si est� activo algunas p�ginas no se podr�n navegar y los enlaces se abrir�n en ventana nueva">Navegaci�n por Internet en modo restringido:</td>
				<td>
					<SELECT size="1" name="navegacionRestringida" ONCHANGE="wEscritorio.oDesktopConfig.navegacionRestringida=this.value=='S'?true:false">
						<OPTION VALUE="S">Activado </OPTION> 
						<OPTION VALUE="N">Desactivado</OPTION> 
					</SELECT>
				</td>				
			</tr>												
		</table>
	</form>
	
	<input type="button" value="cerrar la sesi�n" onmouseup="wEscritorio.cerrarSesion()">
	
</body>
</html>