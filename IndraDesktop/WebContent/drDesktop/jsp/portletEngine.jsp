<%@ page import="java.net.URL" %>
<%@ page import="es.indra.druida.desktop.configurator.DesktopUserConfig"%>
<%@ page import="es.indra.druida.desktop.utils.DesktopWriter"%>
<%@ page import="es.indra.druida.desktop.configurator.DesktopConfig"%>
<%@ page import="es.indra.druida.desktop.configurator.beans.Configuration"%>
<%@ page import="es.indra.druida.desktop.configurator.beans.Engine"%>
<%@ page import="es.indra.druida.desktop.configurator.beans.Parameter"%>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.util.logging.Logger"%>


<html>
	<head>
		<script>
		
			<%
			if (request.getParameter("context")==null || request.getParameter("name")==null){
				Logger.getLogger("porletEngine.jsp").severe("Error t�cnico: Falta alg�n par�metro obligatorio del portlet.");
				%>	alert("Error t�cnico: Falta alg�n par�metro obligatorio del portlet.");  <%
			}else{
				//Busco la configuraci�n del engine
				String plutoViewerUrl=DesktopConfig.getSingleton().getEngineConfigParam("portletEngine","plutoViewerUrl");
				if (plutoViewerUrl!=null){
					//Tomo los par�metros del portlet
					String context=(String)request.getParameter("context");
					String name=(String)request.getParameter("name");
					String pUrl=plutoViewerUrl + "/" + context + "/" + name;
					%>	document.location.href=<%=DesktopWriter.generaCadenaHtml(pUrl)%>;   <%
				} else {
					Logger.getLogger("porletEngine.jsp").severe("Error t�cnico: Falta alg�n par�metro de configuraci�n del motor de portlets.");					
					%>	alert("Error t�cnico: Falta alg�n par�metro de configuraci�n del motor de portlets.");  <%
				}
			}
			%>
		</script>
	</head>
	<body></body>
</html>