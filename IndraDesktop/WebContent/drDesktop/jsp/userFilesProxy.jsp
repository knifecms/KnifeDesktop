<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"  %><%@ page import="es.indra.druida.desktop.apps.drexplorer.DownloadFile" import="es.indra.druida.desktop.utils.DesktopWriter" %><%

	try	{
		String userFileRelativePath=request.getPathInfo();
		String queryString=request.getQueryString();
		DownloadFile downloadf=new DownloadFile();
		downloadf.procesaDownloadFichero(userFileRelativePath, queryString, request, response);
	} catch (Exception e)	{
		%><SCRIPT>alert(<%= DesktopWriter.generaCadenaJavascript("Error: "+ e.getMessage(), true) %>)</SCRIPT><%
	} 


	/*
	http://localhost:8080/DruidaDesktopV2/userRoot/misfondos/fondo1.jpg?thumbnail=true
	http://localhost:8080/DruidaDesktopV2/userRoot/green.gif?thumbnail=true
	http://localhost:8080/DruidaDesktopV2/userRoot/ejemplo.png?thumbnail=true
	http://localhost:8080/DruidaDesktopV2/userRoot/ejemplo.gif?thumbnail=true
	System.out.println("1:"+req.getContextPath());
	System.out.println("2:"+req.getLocalAddr());
	System.out.println("3:"+req.getLocalName());
	System.out.println("4:"+req.getPathInfo());
	System.out.println("5:"+req.getPathTranslated());
	System.out.println("6:"+req.getQueryString());
	System.out.println("7:"+req.getRemoteAddr());
	System.out.println("8:"+req.getRemoteHost());
	System.out.println("9:"+req.getRemoteUser());
	System.out.println("0:"+req.getRequestURI());
	System.out.println("1:"+req.getServletPath());
	System.out.println("2:"+req.getAttributeNames().toString());

	1:/DruidaDesktopV2
	2:127.0.0.1
	3:localhost
	4:/kaspa/pp/fondo1.jpg
	5:D:\ECLIPSE\WORK_SPACES\CALLISTO\.metadata\.plugins\org.eclipse.wst.server.core\tmp0\webapps\DruidaDesktopV2\kaspa\pp\fondo1.jpg
	6:null
	7:127.0.0.1
	8:127.0.0.1
	9:null
	0:/DruidaDesktopV2/userRoot/kaspa/pp/fondo1.jpg
	1:/userRoot
	2:org.apache.catalina.util.Enumerator@1a0d866
	
	*/

%>