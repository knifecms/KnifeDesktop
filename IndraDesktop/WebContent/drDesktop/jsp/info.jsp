<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
     

<%@ page import="javax.servlet.http.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%

	HttpServletRequest req= request;
	HttpServletResponse res= response;	
	HttpSession ses=req.getSession();
	ServletContext cont= req.getSession().getServletContext();
	;
%>

<%=	req.getLocalName() %> <br>
<%=	req.getContextPath() %> <br>
<%=	req.getRequestURL().toString() %> <br>
<%=	req.getServerName() %> <br>
<%=	req.getServerPort() %> <br>
<%=	req.getServletPath() %> <br>
<%=	cont.getRealPath("/WEB-INF") %> <br>
<%=	cont.getServerInfo() %> <br>
<%=	cont.getServletContextName() %> <br>
<%=	cont.getResource("/WEB-INF/").getPath() %> <br>
<%=	cont.getResource("/WEB-INF/").getFile() %> <br>
<%=	getClass().getResource("/DrDesktop.properties").getPath() %> <br>
<%=	getClass().getResource("/").getPath() %> <br>

<hr>
<br><br>

<% if (getClass().getResource("/DrDesktop.properties")!=null) { 
		String rutaProperties=getClass().getResource("/DrDesktop.properties").getPath();
		int i=rutaProperties.lastIndexOf("/WEB-INF");
		String rootPath=rutaProperties.substring(0,i);
%>
Context name: <%=	cont.getServletContextName() %><br/><br/>
DrDesktop.properties file to overwrite: <%=rutaProperties%><br/><br/>
Ruta a usar como raiz del despliegue:  <%=rootPath%><br/><br/>

Tras los cambios debes tirar el servidor (<%= cont.getServerInfo() %>) y volverlo a reiniciar para que coja los cambios. 
<% }%>



<!-- 
localhost 
/DruidaDesktopV2 
http://localhost:8080/DruidaDesktopV2/drDesktop/info.jsp 
localhost 
8080 
/drDesktop/info.jsp 
D:\ECLIPSE\WORK_SPACES\CALLISTO\.metadata\.plugins\org.eclipse.wst.server.core\tmp0\webapps\DruidaDesktopV2\WEB-INF 
Apache Tomcat/5.5.25 
DruidaDesktopV2 
/localhost/DruidaDesktopV2/WEB-INF/ 
/localhost/DruidaDesktopV2/WEB-INF/ 
/D:/ECLIPSE/WORK_SPACES/CALLISTO/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/webapps/DruidaDesktopV2/WEB-INF/classes/DrDesktop.properties 
/D:/ECLIPSE/WORK_SPACES/CALLISTO/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/webapps/DruidaDesktopV2/WEB-INF/classes/ 


Ruta del DrDesktop.properties: /D:/ECLIPSE/WORK_SPACES/CALLISTO/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/webapps/DruidaDesktopV2/WEB-INF/classes/DrDesktop.properties 
Nombre del war: DruidaDesktopV2 
Si hay fichero DrDesktop.properties 
-->

<!-- 

resultado en tomcat en una m�quina unix: 

MADARRPPTRAEXT2.site 
/escritorioempleado 
http://192.168.13.126:8081/escritorioempleado/drDesktop/info.jsp 
192.168.13.126 
8081 
/drDesktop/info.jsp 
/home/escritorio/apache-tomcat-6.0.14/webapps/escritorioempleado/WEB-INF 
Apache Tomcat/6.0.14 
escritorioempleado 
/localhost/escritorioempleado/WEB-INF/ 
/localhost/escritorioempleado/WEB-INF/ 
/home/escritorio/apache-tomcat-6.0.14/webapps/escritorioempleado/WEB-INF/classes/DrDesktop.properties 
/home/escritorio/apache-tomcat-6.0.14/webapps/escritorioempleado/WEB-INF/classes/ 


Ruta del DrDesktop.properties: /home/escritorio/apache-tomcat-6.0.14/webapps/escritorioempleado/WEB-INF/classes/DrDesktop.properties 
Nombre del war: escritorioempleado 
Si hay fichero DrDesktop.properties 


 -->

</body>
</html>