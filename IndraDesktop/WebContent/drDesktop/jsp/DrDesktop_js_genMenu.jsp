<%@ page import="es.indra.druida.desktop.configurator.DesktopConfig" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.MenuItem" %>
<%@ page import="es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.converters.MenuConverter" %>
<%
	AbstractLoader loader=DesktopConfig.getSingleton().getMenuLoader(request, response); 
	MenuItem[] menuItems=(MenuItem[])loader.getObjects();
	String jsonMenu=MenuConverter.beanArrayToJson(menuItems);
%>
var aMenu=<%=jsonMenu%>;