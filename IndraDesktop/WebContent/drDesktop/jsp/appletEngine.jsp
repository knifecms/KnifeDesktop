<%@ page import="java.net.URL" %>
<%@ page import="es.indra.druida.desktop.configurator.DesktopUserConfig"%>
<%@ page import="es.indra.druida.desktop.utils.DesktopWriter"%>
<%@ page import="java.util.Enumeration"%>
<body style="overflow:auto">
<applet <% 

	if (request.getParameter("codebase")!=null) {
		%> codebase=<%=DesktopWriter.generaCadenaHtml(request.getParameter("codebase"))%><% 
	}

	if (request.getParameter("archive")!=null) {
		%> archive=<%=DesktopWriter.generaCadenaHtml(request.getParameter("archive"))%><% 
	}
	
	if (request.getParameter("code")!=null) {
		%>	code=<%=DesktopWriter.generaCadenaHtml(request.getParameter("code"))%><% 
	}
	
	if (request.getParameter("name")!=null) {
		%>	name=<%=DesktopWriter.generaCadenaHtml(request.getParameter("name"))%><% 
	}else{
		%> name="applet_engine"<%
	}

	if (request.getParameter("width")!=null) {
		%>	width=<%=DesktopWriter.generaCadenaHtml(request.getParameter("width"))%><% 
	}else{
		%> width="100%"<%
	}
	
	if (request.getParameter("height")!=null) {
		%>	height=<%=DesktopWriter.generaCadenaHtml(request.getParameter("height"))%><% 
	}else{
		%> height="100%"<%
	}
	
	%>><%
	
	Enumeration params=request.getParameterNames();
	for ( ; params.hasMoreElements() ;) {
			String param=(String) params.nextElement();
			if (param.indexOf("appletParam_")==0)	{
				%><param name=<%=
					DesktopWriter.generaCadenaHtml(param.substring(12))
					%> value=<%=
					DesktopWriter.generaCadenaHtml(request.getParameter(param))
					%>><%
			}
	   }	
	
	
	 %></applet>