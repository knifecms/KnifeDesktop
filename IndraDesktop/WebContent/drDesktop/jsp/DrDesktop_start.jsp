<%@ page import="es.indra.druida.desktop.configurator.DesktopConfig" %>
<%
	//Si estoy validado entro en el escritorio directamente
	if (request.getSession().getAttribute("userConfig")!=null){
		//No se hace un forward por que no est�n en la misma ruta y las URL relativas no encontrarian luego los ficheros
		%><script>document.location.href='../DrDesktop_index.jsp'</script><%
	}
	else	{
		//Si no estoy validado, voy a la p�gina de login configurada si la hay, si no a la p�gina por defecto
		DesktopConfig _miEscritorio = DesktopConfig.getSingleton();
		if (_miEscritorio.get("urlLogin")!=null)	{
			%><script>document.location.href='<%=_miEscritorio.get("urlLogin")%>'</script><%
		}
		else	{
			request.getRequestDispatcher("DrDesktop_login.jsp").forward(request, response);
		}
	}
%>

