<%@page import="es.indra.druida.desktop.configurator.AbstractDesktopConfigurationClass"%>
<%@ page import="es.indra.druida.desktop.configurator.DesktopConfig" %>
<%@ page import="es.indra.druida.desktop.configurator.checks.ConfigurationChecks" %>
<%@ page import="es.indra.druida.desktop.configurator.checks.CheckResult" %>
<%@ page import="es.indra.druida.desktop.utils.DesktopWriter" %>
<%

	ConfigurationChecks configurationChecks=new ConfigurationChecks();
	configurationChecks.setRequest(request);
	
	CheckResult checkResult=configurationChecks.doDesktopConfigCheck();
	if (checkResult.hasErrors())	{
		//Meto la p�gina de incidencias:
		%><%@ include file="loginMessages.html" %><%
		String[] messages=checkResult.getCheckMessages();
		%><SCRIPT>var msgs=[<%
      for (int r=0; r<messages.length; r++){
			if (r>0)
				out.print(","); 	  
			out.print(DesktopWriter.generaCadenaJavascript(messages[r],true));
      }
		%>];writeMessages(msgs)</SCRIPT><%
	} else {

		//DesktopUserConfig _UserConfig;
		DesktopConfig desktopConfig = DesktopConfig.getSingleton();
	
		// reflection
		AbstractDesktopConfigurationClass configClase = desktopConfig.invokeDesktopConfigurationClass(request, response);
		configClase.process();
		//response.sendRedirect("DrDesktop_index.jsp");
		
	}
	
%>