<!-- 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
 -->

<%@ page import="es.indra.druida.desktop.configurator.DesktopUserConfig" %>
<%@ page import="es.indra.druida.desktop.configurator.DesktopConfig" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.Skin" %>
<%@ page import="es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.ConfigProperty" %>
<%@ page import="java.io.File" %>
<%@ page import="java.util.Locale" %>
<%@ page import="es.indra.druida.desktop.configurator.ResourcesService" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.AppGroup" %>
<%@ page import="es.indra.druida.desktop.configurator.beans.Application" %>
<%

		//inicio los datos del usuario
		DesktopUserConfig userConfig;
		String usuario="";
		//String roles=""; //TO-DO
		boolean haHabidoError=false;
		DesktopConfig _miEscritorio = DesktopConfig.getSingleton();
		//Verifico si ya estoy iniciado en sesi�n o es la primera vez ...
		boolean estoyEnSesion=request.getSession().getAttribute("userConfig")!=null;
		ResourceBundle bundle=null;
		
		if (estoyEnSesion)	{
			//Sigo adelante ...
			DesktopUserConfig desktopUserConfig=(DesktopUserConfig)(request.getSession().getAttribute("userConfig"));
			usuario=desktopUserConfig.getId();
			//Meto en sesi�n el locale seleccionado si lo hay
			//Para ello cargo el XML con las propiedades del usuario
			AbstractLoader configLoader=_miEscritorio.getCustomConfigurationLoader(request, response);
			ConfigProperty[] props=(ConfigProperty[])configLoader.getObjects();
			request.getSession().setAttribute("configProperties",props); //Suponemos que hay pocos y que asi me ahorro leer luego del XML
			Locale selectedLocale=null;
			for (int i=0; i<props.length; i++)	{
				ConfigProperty prop=props [i];
				if (prop.getName().equals("locale") && prop.getValue()!=null && !prop.getValue().equals("default")){
					//selectedLocale=prop.getValue();
					String[] selectedLocaleParts=prop.getValue().split("_");
					if (selectedLocaleParts.length==0)
						selectedLocale=null;
					if (selectedLocaleParts.length==1)
						selectedLocale=new Locale(selectedLocaleParts[0]);
					else if (selectedLocaleParts.length==2)
						selectedLocale=new Locale(selectedLocaleParts[0],selectedLocaleParts[1]);
					else if (selectedLocaleParts.length>2)
						selectedLocale=new Locale(selectedLocaleParts[0],selectedLocaleParts[1],selectedLocaleParts[2]);					
				}
			}
			request.getSession().setAttribute("selectedLocale",selectedLocale);
			//Ahora internacionalizo los c�digos de algunas aplicaciones. 
			/*
			Application[] lasAplicaciones = desktopUserConfig.getApplications();
			Application[] lasAplicacionesI18N = new Application[lasAplicaciones.length];			
			for (int i=0; i<lasAplicaciones.length; i++){
				Application app=lasAplicaciones[i];
				Application appI18N=new Application(app);
				appI18N.setTitle(ResourcesService.replaceI18NValues(app.getTitle(),request));
				appI18N.setIconText(ResourcesService.replaceI18NValues(app.getIconText(),request));
				appI18N.setDescription(ResourcesService.replaceI18NValues(app.getDescription(),request));
				lasAplicacionesI18N[i]=appI18N;
			}		
			desktopUserConfig.setApplicationsI18N(lasAplicacionesI18N);
			*/
			bundle=ResourcesService.getResourceBundle("core",request);
		}
		else	{
			bundle=ResourcesService.getResourceBundle("core",request);
				%>
				<script>
					alert(<%=es.indra.druida.desktop.utils.DesktopWriter.generaCadenaJavascript(bundle.getString("index.txt1"))%>)
				</script>
				<% 
				haHabidoError=true;
			}

		
	 	if (!haHabidoError)	{
	 		
			%>
			
<html>
<head>
	<meta http-equiv="Content-Language" content="es">
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<title><%=bundle.getString("index.txt2")%></title>

   <link rel="icon" href="favicon.ico"  /> 
   <link rel="shortcut icon" href="favicon.ico"  />
   
	<LINK REL="stylesheet" TYPE="text/css" HREF="css/drDesktop.css">

	<!-- DOMAIN -->
	<script type='text/javascript' src='../config/domain.js'></script>

	<!-- librer�as DWR -->
	<script type='text/javascript' src='../dwr/engine.js'> </script>
	<script type='text/javascript' src='../dwr/interface/ConfigurationService.js'> </script>
	<script type='text/javascript' src='../dwr/interface/ResourcesService.js'> </script>
	<script type='text/javascript' src='../dwr/interface/SaveSessionService.js'> </script>

	<script type='text/javascript' src='js/DrDesktop_resources.js'></script>	<!--  lo primero -->	


	<!-- <script type='text/javascript' src='js/jquery.js'></script> -->
	<script type='text/javascript' src='js/jquery-1.3.2.min.js'></script>
	<!-- <script type='text/javascript' src='js/jquery-1.3.2.js'></script>	 -->
	<script type='text/javascript' src='js/jquery.cookie.js'></script>

	<!-- arquitectura -->
	<script type='text/javascript' src='js/DrDesktop_skinFactory.js'></script>
	
	<!-- JS de los skins -->	
	<%Skin[] skins=_miEscritorio.getSkins();
	for (int i=0;i<skins.length;i++) {
		String skinName=skins[i].getId();%>
		
		<script type='text/javascript' src='<%=_miEscritorio.get("urlSkins")%>/<%=skinName%>/js/desktop_skin.js'></script>
		
	<%}%>
	
	<script type='text/javascript' src='js/DrDesktop-core.js'></script>
	<script type='text/javascript' src='js/DrJson.js'></script>
	<script type='text/javascript' src='js/DrDesktop_main.js'></script>
	<script type='text/javascript' src='js/DrDesktop_menu.js'></script>
	<script type='text/javascript' src='js/DrDesktop_eventos.js'></script>
	<script type='text/javascript' src='js/DrDesktop_iconos.js'></script>
	<script type='text/javascript' src='js/DrDesktop_ventanas.js'></script>   
	<script type='text/javascript' src='js/DrDesktop_menuDerecho.js'></script>      
	<script type='text/javascript' src='jsp/DrDesktop_js_genMenu.jsp'></script>
	<script type='text/javascript' src='jsp/DrDesktop_js_inicEntorno.jsp'></script>   
	<script type='text/javascript' src='jsp/DrDesktop_js_genIconos.jsp'></script>
	
	<!-- estilos del skin actual-->
	<script>document.write('<link rel="stylesheet" type="text/css" href="<%=_miEscritorio.get("urlSkins")%>/'+oDesktopConfig.tema+'/css/style.css">')</script>
		
</head>

<body style="margin:0px;">
	

	<div id="BARRA_MENU" style="position:absolute; z-index: 1000000000; left: 0px; top: 0px; width:100%; visibility: visible"></div>
	
	<%
	String contenidoPersonalizado=_miEscritorio.get("includeContenido");
	if (contenidoPersonalizado!=null){ 
	%>
		<jsp:include flush="true" page="<%=contenidoPersonalizado%>"></jsp:include>
	<% } %>	

	<DIV id='divGrupoIconos' style='width:100%;height:100%'></DIV>

	<script>
	   document.write(generaHtmlMenu(NIVELMAXIMOMENU));
	</script>

	<form id="fcerrar" method="POST" name="fcerrar" action="jsp/DrDesktop_cerrarSesion.jsp">
		<input type="hidden" id="ventanas" name="ventanas" value="[]">
		<input type="hidden" id="iconos" name="iconos" value="[]">
		<input type="hidden" id="configuracion" name="configuracion" value="{}">
		<input type="hidden" id="skinParameters" name="skinParameters" value="{}">
		<input type="hidden" id="guardar" name="guardar" value="S">		
		<input type="hidden" id="cerrarSesion" name="cerrarSesion" value="S">			
	</form>

	<!--
	<DIV id="laCapaOscura" style="z-index:9999999999999999;display:none;filter:blendTrans(duration=5) alpha(opacity=60); width:100%; height:100%; background-color:#410A38; position:absolute; left:0px; top:0px;"></DIV>
	<DIV style="display:none" id="capaGenericaParaCustomizaciones"></DIV>
	-->
	
</body>
</html>


<% } //fin del else   %>