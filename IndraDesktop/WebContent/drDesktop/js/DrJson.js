function JSONStringAObjeto (strr)	{
	try	{
		eval("var obj="+strr)
		return obj
		}
	catch (e)	{
		return null
		}
	}

function objetoAJSONString (obj, usarComillasSimples)	{
	if (usarComillasSimples==true)
		return objetoAJSONString_2(obj,DrGeneraStringJsComillasSimples)
	else
		return objetoAJSONString_2(obj,DrGeneraStringJs)
	}
	
function objetoAJSONString_2 (obj, funcGeneraStringJs)	{
	var ret=[]
   if (isString(obj)) {ret[ret.length]=funcGeneraStringJs(obj)}
   else if (isNumber(obj) || isBoolean(obj)) {ret[ret.length]=obj+""}
   else if (isArray(obj)) {
      ret[ret.length]="["
      for (var r= 0; r<obj.length; r++) {
         if (r>0)
            ret[ret.length]=","
         ret[ret.length]=objetoAJSONString_2(obj[r], funcGeneraStringJs)
         }
      ret[ret.length]="]"
      }
   else if (isObject(obj) && !isFunction(obj)) {
      ret[ret.length]="{"
      var soyElPrimero=true
      for (o in obj) {
         if (!isFunction(obj[o])) {
	         if (!soyElPrimero)
	            ret[ret.length]=","         
            ret[ret.length]=funcGeneraStringJs(o)+':'
            ret[ret.length]=objetoAJSONString_2(obj[o], funcGeneraStringJs)
            soyElPrimero=false
            }
         }
      ret[ret.length]="}"
      }
   //else if (isUndefined(obj) || isNull(obj) || isFunction(obj)) {ret[ret.length]="null"}
   else {ret[ret.length]="null"}
 	return ret.join('')
   }

function isArray(a)  {  return isObject(a) && a.constructor.toString() == Array.toString(); } //Esto es para que no falle si los arrays estan en distintas ventanas
function isBoolean(a)  {      return typeof a == 'boolean'; }
function isFunction(a)  {      return typeof a == 'function'; }
function isNull(a)  {      return typeof a == 'object' && !a; }
function isNumber(a)  {      return typeof a == 'number' && isFinite(a); }
function isObject(a)  {     return (typeof a == 'object' && !!a) || isFunction(a); }
function isString(a)  {     return typeof a == 'string'; }
function isUndefined(a)  {     return typeof a == 'undefined'; }

function DrGeneraStringJs (cad) {
   if (cad==null)
      return
   return '"' + ( cad.replace(/\\/gi,"\\\\") .replace(/"/gi,"\\\"") .replace(/\n/gi,"\\n") .replace(/\r/gi,"\\r") .replace(/\f/gi,"\\f") .replace(/\t/gi,"\\t") ) +  '"'
   }
   
function DrGeneraStringJsComillasSimples (cad) {
   if (cad==null)
      return
   return "'" + ( cad.replace(/\\/gi,"\\\\") .replace(/'/gi,"\\'") .replace(/\n/gi,"\\n") .replace(/\r/gi,"\\r") .replace(/\f/gi,"\\f") .replace(/\t/gi,"\\t") ) +  "'";
   }   
 
