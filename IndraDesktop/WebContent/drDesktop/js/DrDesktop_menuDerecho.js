/* (+) asociaMenuDerecho(objeto_dom_a_asociar, matriz_de_opciones) */

/* ejemplo matriz:
   var opcMenuDesktop=[
      {"txt":"Organizar iconos", "js":"opcionMenu1();alert(33)"}
      ,{...}
      ]
*/

//Requiere jquery

var anchoMinimoMenus =174

window.drContextMenus={}

function asociaMenuDerecho (obj, opciones, bottomMargin)   {
   //TO-DO: El mayor ID que exista?
   var nuevoId="drMnu"+Math.round(Math.random(33)*100000000)
   var elHtml=dameHtmlMenuDerechoEscritorio(nuevoId,"#8BADCD",opciones)
   $("body").append(elHtml)
   $(obj).showMenu({
      opacity:0.95,
      query: "#"+nuevoId,
      zindex: 999999999,
      bottomMargin: bottomMargin
   });
   drContextMenus[nuevoId]=opciones
   $("#"+nuevoId).hide()
}


function dameHtmlMenuDerechoEscritorio(id, colorROver, opciones)  {
   var html=[]
   html[html.length]='<DIV id="'+id+'" style="width:'+anchoMinimoMenus+'px; border:1px solid #D4D0C8; border-bottom: 1px solid #404040; border-right: 1px solid #404040;">'
   html[html.length]='<DIV style="padding:2 px; background-color:#D4D0C8;border:1px solid white; border-bottom: 1px solid #808080; border-right: 1px solid #808080;">'
   html[html.length]='<table width='+ (anchoMinimoMenus-2) +' style="font-family: Tahoma;font-size: 11px;cursor:pointer;" border="0" cellspacing="0" cellpadding="0">'
   for (var r=0;r<opciones.length; r++)   {
   	if (opciones[r].txt==null)	{
	      html[html.length]='<tr>'
   	   html[html.length]='<td height="2" style="padding:0px;padding-left:2px;padding-right:2px">'
      	html[html.length]='<img style="margin-top:3px;margin-bottom:3px;border-top:1px solid #808080;border-bottom:1px solid #FFFFFF;height:0px; width:100%">'
	      html[html.length]="</td></tr>"
   		}
   	else	{
	      html[html.length]='<tr height="17" onmouseover="this.style.background=\''+colorROver+'\';" onmouseout="this.style.background=\'\'" onmousedown="eval(drContextMenus[\''+id+'\']['+r+'].js)">'
   	   html[html.length]='<td nowrap style="padding-left:16px;padding-right:16px">'
      	html[html.length]=opciones[r].txt
	      html[html.length]="</td></tr>"
	      }
      }
   html[html.length]='</table>'
   html[html.length]="</DIV></DIV>"
   return (html.join(''))
   }

//Modified from  jquery.jcontext.1.0.js Requires: jQuery 1.2+

(function($) {
	$.fn.showMenu = function(options) {
		var opts = $.extend({}, $.fn.showMenu.defaults, options);

		$(this).bind("contextmenu",function(e){
         if (window.contextmenu_registered==null)
            contextmenu_registered=[]
         contextmenu_registered[contextmenu_registered.length]=$(opts.query)
         for (var r=0; r<contextmenu_registered.length; r++)
            contextmenu_registered[r].hide();
         if (drWindow && drWindow.getGlobalNoContextMenu())
            return false
         window.xMenuDerecho=e.pageX;
         window.yMenuDerecho=e.pageY; 
         window.objMenuDerecho=this;
         var wh=$(window).height();
         if (opts.bottomMargin!=null)
            wh=wh-opts.bottomMargin
         var ww=$(window).width();
         var mh=$(opts.query).height();
         var mw=$(opts.query).width();
         var ml=e.pageX;
         var mt=e.pageY
         if ((mt+mh)>wh)
            mt=wh-mh-1
         if ((ml+mw)>ww)
            ml=ww-mw-1
			$(opts.query).show().css({
				top:mt+"px",
				left:ml+"px",
				position:"absolute",
				opacity: opts.opacity,
				zIndex: opts.zindex
			});
			return false;
		});

		$(document).bind("click",function(e){
			$(opts.query).hide();
         if (drWindow && drWindow.getGlobalNoContextMenu())
            return false
		});

	};
	
	$.fn.showMenu.defaults = {
		zindex: 2000,
		query: document,
		opacity: 1.0
	};
})(jQuery);