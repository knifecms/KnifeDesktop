

window.iframeDragDivMode=true
window.DaemonSesion_mseg=300000

function drDesktop_onload()	{
   generaObjetosIniciales();//demonios y registro ventanas
   precalculaValoresPropiedadesAplicaciones() //Preproceso los atributos de URL
   precalculaI18N();
   calculaValoresSkin();
	generarLaBarraDeMenu();  
	//recolocarLaBarraDeBotones(); 
	recolocaBarraCustom() 
   generarWidgetsDelSkin();
	generarLosIconos();
	cargaVariablesIniciales();   
	precargaImagenes();
	asociarEventos();
	limpiarVariables();
   //setTimeout(cacheaMenuInicio,0);//Para evitar problemas de colocaci�n debidos a que no se ha cargado la imagen todav�a...
   lanzarReadySkin()
   generarVentanasIniciales()
   lanzaDaemonSesion({mseg:DaemonSesion_mseg})

}


function lanzaDaemonSesion(args){
	var mseg=args.mseg
	var ww=drWindow.getDesktopWindow();
	ww.SESSION_DAEMON=setInterval (function(){
		if (window.sesslostmessshown){
			clearInterval(window.SESSION_DAEMON)
			return
		}
		window.ConfigurationService.isUserInSession({
         async:true
         ,callback:function(userInSession){
				if (window.sesslostmessshown){
					clearInterval(window.SESSION_DAEMON)
					return
				}
	         if (userInSession==false){
	         	window.sesslostmessshown=true	         
	         	clearInterval(window.SESSION_DAEMON)	         	
		         var recargar=drWindow.showConfirm({message: drResources.get({bundle:"core",key:"ventanasJs.txt6"}) }) //La sesi�n parece haberse perdido. Para solucionar este problema recargue la ventana del escritorio y vuelva a logarse. �Desea recargar ahora?
	   	   	if (recargar)
	      			drWindow.sessionRestart()
	        	}
         }
         ,timeout:5000
         ,errorHandler:function (message)	{
				//drAlert( drResources.get({bundle:"core",key:"ventanasJs.txt1"}) + "\n  [ " + message+" ]"); 
			}
      });
	}, mseg)
}

function precalculaI18N(){
	drWindow.preprocessI18N()
	drMenu.preprocessI18N()
	//debugger
}

function lanzarReadySkin() {
   var skData=drWindow.getSkinData()
   if (skData!=null && skData.onReady!=null)
      skData.onReady()
}

function generarWidgetsDelSkin() {
   var params=drWindow.getSkinParameters();
   if (params!=null && params.widgets!=null) {
      var widgets=params.widgets.replace(/^\s+\b/,"").replace(/\b\s+$/,"").split(/\b\s*,\s*\b/gi);
      for (var r=0; r<widgets.length; r++)   {
         drWindow.openApplication({ application:widgets[r], sessionSave:false });
      }
   }
}

function precalculaValoresPropiedadesAplicaciones()   {
   drWindow.initConfig()
}

function calculaValoresSkin() {
   var skinData=drWindow.getSkinData()
   if (skinData==null)  //No se ha usado oSkinFactory.createSkin para el skin
      skinData={}
   if (skinData.windowChromeWidth==null || skinData.windowChromeHeight==null) {
      var argsVentana={id:"testChromeBordersSizeWindow", url:"about:blank", width:"1000", height:"1000",position:"-2000,-2000",noTaskbar:true, ignoreMargins:true}
      drWindow.open(argsVentana)
      var iFrame=drWindow.getNativeIframe(argsVentana), $iframe=$(iFrame)
      if (skinData.windowChromeWidth==null)
         skinData.windowChromeWidth=drWindow.getWidth(argsVentana) - $iframe.width();
      if (skinData.windowChromeHeight==null)
         skinData.windowChromeHeight=drWindow.getHeight(argsVentana) - $iframe.height();
      drWindow.close(argsVentana)
   }
   //alert(skinData.windowChromeWidth+","+skinData.windowChromeHeight)
   drWindow.setSkinData({data:skinData})
}

function actualizaSkinsConValoresCustom() {
   window.oDefaultSkinParameters={}
   var idSkinActual=drWindow.getSkin();
   var skinCorresp=dameSkinConId(idSkinActual);
   if (skinCorresp.parameters)
      for (p in skinCorresp.parameters)
         oDefaultSkinParameters[p]=skinCorresp.parameters[p];
   //volcamos sobre oSkins los valores de las propiedades de oCustomSkins
   if (window.oCustomSkins!=null){
      for (var r=0; r<oCustomSkins.length; r++) {
         var customSkin=oCustomSkins[r]
         var skinCorresp=dameSkinConId(customSkin.id);
         if (skinCorresp!=null){
            if (customSkin.parameters!=null && skinCorresp.parameters==null)
               skinCorresp.parameters={}
            for (p in customSkin.parameters)
               skinCorresp.parameters[p]=customSkin.parameters[p];
         }
      }
   }
}

function dameSkinConId(id)   {
   var skins=drWindow.getSkins();
   for (var r=0; r<skins.length; r++)  {
      if (skins[r].id==id)
         return skins[r]
   }
}


function generaObjetosIniciales()   {
   oDaemons=new clDrRegistroDaemons();
   //oRegistroVentanas=new clDrRegistroVentanas()
   }

function limpiarVariables()	{
 	if (drWindow.getDesktopConfig().margSupAnt!=null)
      drWindow.getDesktopConfig().margSupAnt=null
	}

function generarLaBarraDeMenu()	{
   var HTML=null;
   var funcionGetHtml=window["barra_"+drWindow.getSkin()]
   if (funcionGetHtml!=null)
      HTML=funcionGetHtml()
   if (HTML!=null)
      $("#BARRA_MENU").html(HTML)
   else
      $("#BARRA_MENU").remove();
}

	
function recolocaBarraCustom()	{
   var oDatosBotonera=drWindow.getTaskbarData()
   var $sep_custom=$("#SEP_BARRA_CUSTOM")       //document.getElementById("SEP_BARRA_CUSTOM")
   $sep_custom.css({width:"0px"})      //sep_custom.style.width=0
   var skinParam=drWindow.getSkinParameters()
   if (skinParam && skinParam.taskbarCustomBlockWidget!=null)   {
      var taskbarCustomBlockWidgetPosition="r5,b0"
      if (skinParam.taskbarCustomBlockWidgetPosition !=null)
         taskbarCustomBlockWidgetPosition=skinParam.taskbarCustomBlockWidgetPosition
      var nuevoId=drWindow.openApplication({application: skinParam.taskbarCustomBlockWidget, sessionSave:false, position:taskbarCustomBlockWidgetPosition, zIndex:2000000000})
      var elAncho=drWindow.getWidth({id:nuevoId}) + 10
      $sep_custom.css({width:elAncho+"px"})         //sep_custom.style.width= elAncho
	}
   var $taskbarBtnStart=$("#taskbarBtnStart")
   if ($taskbarBtnStart.length>0){
      oDatosBotonera.marginLeftBotonera=$taskbarBtnStart.width()
      drWindow.setTaskbarData(oDatosBotonera);
   }
   if (noHayBarra()) {
      drDesktop.get$Win({id:"dTaskbar"}).css({visibility:"hidden"})
   }
}

function cargaVariablesIniciales()	{
	var sep_custom=document.getElementById("SEP_BARRA_CUSTOM")	
	if (sep_custom!=null) 
		window.anchoStatus=sep_custom.offsetWidth -12; //Se usa para calcular el ancho de la capa de botones minimizados
	else
		window.anchoStatus=0

   //Valores relativos a roles particulares de usuario
   try{
      var datosUsu=drWindow.getUserData()
      var roles=datosUsu.roles
      if (roles!=null && roles.length>0){
         var i=dameIndiceEnArray(roles,"noSessionSave")
         if (i!=-1){
            window.noSessionSave=true;
         }
      }
      //{"id":"usuario","motherSurname":"An�nimo","name":"Usuario","roles":["userAdmin"],"surname":"Invitado"}
   } catch(e){}
}

function dameIndiceEnArray(elarray, valor) {
   for (var i=0;i<elarray.length;i++)
      if (elarray[i]==valor)
         return i
   return -1
}

function noHayIconos()  {
   var propSkin=drWindow.getSkinParameters();
   if (propSkin!=null)  {
      var noIcons=drEvaluaBooleanoString(propSkin.noIcons)
      if (noIcons)
         return true
   }
}

function noHayBarra()  {
   var propSkin=drWindow.getSkinParameters();
   if (propSkin!=null)  {
      var noTaskbar=drEvaluaBooleanoString(propSkin.noTaskbar)
      if (noTaskbar)
         return true
   }
}

function asociarEventos()	{

   var opcMenuDesktop=[]
   opcMenuDesktop[opcMenuDesktop.length]={"txt": drResources.get({bundle:"core",key:"mainJs.txt1"}) , "js":"drWindow.openApplication({'application':'nuevoAccesoDirecto'})"}
   if (drWindow.getApplicationsConfig()["drDirAccManager"]!=null)
      opcMenuDesktop[opcMenuDesktop.length]={"txt": drResources.get({bundle:"core",key:"mainJs.txt1_2"}) , "js":"drWindow.openApplication({'application':'drDirAccManager'})"}
   opcMenuDesktop[opcMenuDesktop.length]={"txt": drResources.get({bundle:"core",key:"mainJs.txt2"}) , "js":"drOrganizarIconos()"} 
   opcMenuDesktop[opcMenuDesktop.length]={}
   opcMenuDesktop[opcMenuDesktop.length]={"txt": drResources.get({bundle:"core",key:"mainJs.txt3"}) , "js":"drWindow.openApplication({'application':'ejecutarAplicacion'})"}      
   if (drWindow.getApplicationsConfig()["drConsole"]!=null)
      opcMenuDesktop[opcMenuDesktop.length]={"txt": drResources.get({bundle:"core",key:"mainJs.txt4"}) , "js":"drWindow.openApplication({'application':'drConsole'})"}   
   if (drWindow.getApplicationsConfig()["drWindowManager"]!=null)
      opcMenuDesktop[opcMenuDesktop.length]={"txt": drResources.get({bundle:"core",key:"mainJs.txt14"}) , "js":"drWindow.openApplication({'application':'drWindowManager'})"}
   if (drWindow.getApplicationsConfig()["drConfigPanel"]!=null){
      opcMenuDesktop[opcMenuDesktop.length]={}	
      opcMenuDesktop[opcMenuDesktop.length]={"txt": drResources.get({bundle:"core",key:"mainJs.txt5"}) , "js":"drWindow.openApplication({'application':'drConfigPanel'})"}
   }

   var opcMenuDesktopSinIconos=[]
   opcMenuDesktopSinIconos[opcMenuDesktopSinIconos.length]={"txt": drResources.get({bundle:"core",key:"mainJs.txt3"}) , "js":"drWindow.openApplication({'application':'ejecutarAplicacion'})"}
   if (drWindow.getApplicationsConfig()["drConsole"]!=null)
      opcMenuDesktopSinIconos[opcMenuDesktopSinIconos.length]={"txt": drResources.get({bundle:"core",key:"mainJs.txt4"}) , "js":"drWindow.openApplication({'application':'drConsole'})"}
   if (drWindow.getApplicationsConfig()["drWindowManager"]!=null)
      opcMenuDesktopSinIconos[opcMenuDesktopSinIconos.length]={"txt": drResources.get({bundle:"core",key:"mainJs.txt14"}) , "js":"drWindow.openApplication({'application':'drWindowManager'})"}
   if (drWindow.getApplicationsConfig()["drConfigPanel"]!=null){
      opcMenuDesktopSinIconos[opcMenuDesktopSinIconos.length]={}
      opcMenuDesktopSinIconos[opcMenuDesktopSinIconos.length]={"txt": drResources.get({bundle:"core",key:"mainJs.txt5"}) , "js":"drWindow.openApplication({'application':'drConfigPanel'})"}
   }


	//Menu derecho
	var laImagen=document.getElementById("fondo_escritorio")
	if (laImagen==null)
		laImagen=drDesktop.get$Win({id:"dDesktop"}).get(0)
	/*LITE no tiene menu derecho*/
   if (noHayIconos())
   	asociaMenuDerecho(laImagen, opcMenuDesktopSinIconos, drWindow.getBottomMargin())
   else
	   asociaMenuDerecho(laImagen, opcMenuDesktop, drWindow.getBottomMargin())
	if (oDesktopConfig.modoSinErrores)	{
		window.onerror=function (msg,url,l)	{
			txt= drResources.get({bundle:"core",key:"mainJs.txt6"}) + " <br><br>"
			txt+= msg + "<br>"
			txt+="URL: " + url + "<br>"
			txt+= drResources.get({bundle:"core",key:"mainJs.txt7"}) +" " + l + "<br><br>"
			txt+= drResources.get({bundle:"core",key:"mainJs.txt8"}) +"<br><br>"
			drAlertSistema(txt)
			return true
			}
		}	
   $(window).bind("scroll",onBodyScroll)
   //$(window).bind("scroll",mifunc)
   //document.body.onscroll=onBodyScroll
	}

function onBodyScroll() {//Esto arregla problemas cuando un elemento fuera de la pantalla recibe el foco
   var body=document.body
   if (body.scrollTop!=0)
      body.scrollTop=0;
   if (body.scrollLeft!=0)
      body.scrollLeft=0;
   }

function generaMenuDerechoEscritorio()	{
	}

function DrEscHTML (cad) {
   if (cad!=null)
      return cad.replace(/&/gi,"&amp;").replace(/</gi,"&lt;").replace(/"/gi,"&quot;"); 
   return "";
   }

function precargaImagenes()	{
	//busco las imagenes del menu
	if (aMenu!=null)	{
		for (var i=0;i<aMenu.length;i++)	{
			if (aMenu[i].img!=null)
				precargaUnaImagen(aMenu[i].img)
			}
		}
	}

function precargaUnaImagen(img)	{
	var im = new Image;
	im.src = img; 
	}


top.deseaSalir=false
window.onunload=function ()	{
	if (!top.deseaSalir)	{
		if (drWindow.getDesktopConfig().animacionesActivas)
			drOscureceFondo(true)	
		if (drCuadroConfirm( drResources.get({bundle:"core",key:"mainJs.txt9"}) , [ drResources.get({bundle:"core",key:"mainJs.txt10"}) , drResources.get({bundle:"core",key:"mainJs.txt11"}) ]))	{
         drWindow.sessionSaveAndRestart()
      } else{
         drOscureceFondo(false)
      }
	}
}
	


function cerrarSesion(recargar)	{
   if (recargar)
      drWindow.sessionSaveAndRestart()
   else
      drWindow.sessionSaveAndClose()
	}


function preguntaYCierra()  {
   if (drWindow.getDesktopConfig().animacionesActivas)
      drOscureceFondo(true)	
   if (drCuadroConfirm( drResources.get({bundle:"core",key:"mainJs.txt12"}) , [ drResources.get({bundle:"core",key:"mainJs.txt10"}) , drResources.get({bundle:"core",key:"mainJs.txt11"}) ]))	
      drWindow.sessionSaveAndClose();
   else
      drOscureceFondo(false)
}

function preguntaYSalSinGuardar()  {
   if (drWindow.getDesktopConfig().animacionesActivas)
      drOscureceFondo(true)	
   if (drCuadroConfirm( drResources.get({bundle:"core",key:"mainJs.txt13"}) , [ drResources.get({bundle:"core",key:"mainJs.txt10"}) , drResources.get({bundle:"core",key:"mainJs.txt11"}) ]))	
      drWindow.sessionClose();
   else
      drOscureceFondo(false)
}

function salirSinGuardar()	{
   drWindow.sessionClose();
}  


function drOscureceFondo(bool)  {
	if (!window.laCapaOscura || !laCapaOscura.filters)
		return 
	if (window.capaYaOscurecida)
		return
	window.capaYaOscurecida=true
   /*if (!window.laCapaOscura)   {
      document.body.insertAdjacentHTML("beforeEnd",'<DIV id="laCapaOscura" style="display:none;filter:blendTrans(duration=5) alpha(opacity=50); width:100%; height:100%; background-color:black;"></DIV>')
      }*/
      /*
   if (bool)   {
	   if (!laCapaOscura.filters)
	   	return
      laCapaOscura.style.display="inline"
      if (laCapaOscura.filters.blendTrans.status == 2) 
         laCapaOscura.filters.blendTrans.stop();
      laCapaOscura.filters.blendTrans.apply();
      laCapaOscura.style.visibility="visible";
      laCapaOscura.filters.blendTrans.play();
      }
   else  {
      alert("oculto")
      laCapaOscura.style.display="none"
      laCapaOscura.style.visibility="hidden";
      }*/
   }	
   
