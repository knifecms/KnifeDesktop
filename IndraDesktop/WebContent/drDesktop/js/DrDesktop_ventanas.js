var conDwrConnectionTimeout=5000;


/*OBJETO GLOBAL drWindow (pertenece al objeto window que cargue este js)*/

var drWindow={
   /*(+)*/    getDesktopWindow  :public_getDesktopWindow
   /*(+)*/  , getStructuralWindow  :public_getStructuralWindow   
   /*(+)*/ // , getXDesktopObj  :public_getXDesktopObj
   /*(+)*/  , getDrDesktopObj  :public_getDrDesktopObj
   /*(+)*/ // , getXDesktopWindowObj  :public_getXDesktopWindowObj
   /*(+)*/  , getDesktopConfig  :public_getDesktopConfig
   /*(-)*/  , setDesktopConfigValue  :private_setDesktopConfigValue
   /*(-)*/  , getDesktopConfigValue  :private_getDesktopConfigValue
   /*(+)*/  , getEnginesConfig  :public_getEnginesConfig
   /*(+)*/  , getFileTypesConfig  :public_getFileTypesConfig
   /*(+)*/  , getApplicationsConfig  :public_getApplicationsConfig
   /*(+)*/  , getApplicationsGroupsConfig  :public_getApplicationsGroupsConfig   
   /*(+)*/  , getSkins  :public_getSkins  
   /*(+)*/  , getApplicationIds: public_getApplicationIds
   /*(+)*/  , getTopMargin  :public_getTopMargin
   /*(+)*/  , getBottomMargin  :public_getBottomMargin
   /*(+)*/  , getClientWidth  :public_getClientWidth
   /*(+)*/  , getClientHeight  :public_getClientHeight
   /*(+)*/  , getId  :public_getId
   /*(+)*/  , getWindowIds  :public_getWindowIds
   /*(+)*/  , getWindowData  :public_getWindowData
   /*(+)*/  , getNativeWindow  :public_getNativeWindow
   /*(+)*/  , getNativeIframe  :public_getNativeIframe
   /*(+)*/ // , getRegistry  :public_getRegistry
   /*(+)*/  , getDaemonRegistry  :public_getDaemonRegistry
   /*(+)*/  , getSkin  :public_getSkin
   /*(+)*/  , setSkin  :public_setSkin
   /*(+)*/  , arrangeAllWindows  :public_arrangeAllWindows
   /*(+)*/  , generateId  :public_generateId
   /*(+)*/  , open  :public_open
   /*(+)*/  , openApplication  :public_openApplication
   /*(+)*/  , showHelp  :public_showHelp
   /*(+)*/  , openWindowArray  :public_openWindowArray
   /*(+)*/  , reload  :public_reload
   /*(+)*/  , close  :public_close  
   /*(+)*/  , setTitle  :public_setTitle
   /*(+)*/  , setTop  :public_setTop
   /*(+)*/  , setLeft  :public_setLeft
   /*(+)*/  , setWidth  :public_setWidth
   /*(+)*/  , setHeight  :public_setHeight
   /*(+)*/  , setStatus  :public_setStatus
   /*(+)*/  , showMessage  :public_showMessage
   /*(+)*/  , showConfirm  :public_showConfirm
   /*(+)*/  , getStatus  :public_getStatus
   /*(+)*/  , getWidth  :public_getWidth
   /*(+)*/  , getHeight  :public_getHeight
   /*(+)*/  , getTop  :public_getTop
   /*(+)*/  , getLeft  :public_getLeft
   /*(+)*/  , resizeTo  :public_resizeTo
   /*(+)*/  , moveTo  :public_moveTo
   /*(+)*/  , moveResizeTo  :public_moveResizeTo
   /*(+)*/  , createDaemon  :public_createDaemon
   /*(+)*/  , killDaemons  :public_killDaemons
   /*(+)*/  , sessionSaveAndRestart :public_sessionSaveAndRestart
   /*(+)*/  , sessionRestart :public_sessionRestart
   /*(+)*/  , sessionSaveAndClose :public_sessionSaveAndClose
   /*(+)*/  , sessionClose :public_sessionClose
   /*(+)*/  , getPublicMethods: drWindow_public_getPublicMethods
   /*(+)*/  , toFront: public_toFront
   /*(+)*/  , toBack: public_toBack
   /*(+)*/  , bindDesktopResize: public_bindDesktopResize
   /*(+)*/  , getUserData: public_getUserData
   /*(+)*/  , getCookie: public_getCookie
   /*(+)*/  , setCookie: public_setCookie
   /*(+)*/  , deleteCookie: public_deleteCookie
   /*(+)*/  , openFile: public_openFile
   /*(+)*/  , getSkinParameters: public_getSkinParameters
   /*(+)*/  , getTaskbarData: public_getTaskbarData
   /*(+)*/  , setTaskbarData: public_setTaskbarData
   /*(+)*/  , show: public_show
   /*(+)*/  , hide: public_hide
   /*(+)*/  , changeVirtualDesktop: public_changeVirtualDesktop
   /*(+)*/  , getVirtualDesktopsData: public_getVirtualDesktopsData
   /*(+)*/  , getCurrentVirtualDesktop: public_getCurrentVirtualDesktop
   /*(+)*/  , setVirtualDesktop: public_setVirtualDesktop
   /*(+)*/  , getVirtualDesktop: public_getVirtualDesktop
   /*(-)*/  , inVirtualDesktop: private_inVirtualDesktop
   /*(+)*/  , getSkinData: public_getSkinData
   /*(+)*/  , setSkinData: public_setSkinData
   /*(+)*/  , getInnerWidth: public_getInnerWidth
   /*(+)*/  , getInnerHeight: public_getInnerHeight
   /*(+)*/  , setNoDrag: public_setNoDrag
   /*(+)*/  , getNoDrag: public_getNoDrag
   /*(+)*/  , setNoResize: public_setNoResize
   /*(+)*/  , getNoResize: public_getNoResize
   /*(+)*/  , setGlobalNoDrag: public_setGlobalNoDrag
   /*(+)*/  , getGlobalNoDrag: public_getGlobalNoDrag
   /*(+)*/  , setGlobalNoResize: public_setGlobalNoResize
   /*(+)*/  , getGlobalNoResize: public_getGlobalNoResize
   /*(+)*/  , setGlobalNoClose: public_setGlobalNoClose
   /*(+)*/  , getGlobalNoClose: public_getGlobalNoClose
   /*(+)*/  , setGlobalNoContextMenu: public_setGlobalNoContextMenu
   /*(+)*/  , getGlobalNoContextMenu: public_getGlobalNoContextMenu
   /*(-)*/  , setGlobalBoolean: private_setGlobalBoolean
   /*(-)*/  , getGlobalBoolean: private_getGlobalBoolean
   /*(-)*/  , refreshWindowObjectProperties: private_refreshWindowObjectProperties
   /*(-)*/  , getApplicationsWithHelp:private_getApplicationsWithHelp
   /*(-)*/  , hasHelp: private_hasHelp
   /*(+)*/  , getDesktopProperties: public_getDesktopProperties  
   /*(-)*/  , initConfig: private_initConfig
   /*(+)*/  , focus: public_focus
   /*(+)*/  , blur: public_blur
   /*(+)*/  , hasFocus: public_hasFocus
   /*(+)*/  , getWindowExist: public_getWindowExist
   /*(+)*/  , getSkinMeta: public_getSkinMeta
   /*(-)*/  , prepareSkinParameterChange: private_prepareSkinParameterChange
   /*(-)*/  , getSkinParameterChanges: private_getSkinParameterChanges
   /*(+)*/  , getSkinUrl: public_getSkinUrl
   /*(-)*/  , getMenuObject: private_getMenuObject
   /*(-)*/  , setMenuObject: private_setMenuObject
            , getApplicationExists: public_getApplicationExists
            , alert: public_alert
            , setStatusAllWindows: public_setStatusAllWindows
            , preprocessI18N: private_preprocessI18N
   }


function private_preprocessI18N(){
	//App groups
	var appgrp=this.getApplicationsGroupsConfig()
	for(var i=0; i<appgrp.length; i++){
		appgrp[i].descripcion=drResources.replaceI18NValues(appgrp[i].descripcion)
	}
	//Apps
	var apps=this.getApplicationsConfig()
	for (var a in apps)	{
		apps[a].iconText = drResources.replaceI18NValues(apps[a].iconText)
		apps[a].description = drResources.replaceI18NValues(apps[a].description)
	}
	//Skins
	var skins = drWindow.getSkins()
   for (var i=0; i<skins.length; i++){
      if (skins[i].description!=null)
         skins[i].description=drResources.replaceI18NValues(skins[i].description)
   }
	var skinMeta = drWindow.getSkinMeta()
	for (var a in skinMeta){
		var propMeta=skinMeta[a]
		if (propMeta.description!=null)
			propMeta.description=drResources.replaceI18NValues(propMeta.description)
		if (propMeta.valueDescriptions!=null)
			propMeta.valueDescriptions=drResources.replaceI18NValues(propMeta.valueDescriptions)		
	}
	
	
}

function public_setStatusAllWindows(args)   {
   if (args==null)
      args={}
   var status=args.status, onlyCurrentDesktop=drEvaluaBooleanoString(args.onlyCurrentDesktop);
   var ventanasAMinim=drWindow.getWindowIds({noWidgets:"true",onlyCurrentDesktop:onlyCurrentDesktop})
   for (var r=0; r<ventanasAMinim.length; r++)  {
      if (drWindow.getStatus({id:ventanasAMinim[r]})!=status)  {
         drWindow.setStatus({id:ventanasAMinim[r], status:status})   
      }
   }
}

function private_getMenuObject() {
   return this.getDesktopWindow().aMenu
}

function private_setMenuObject(obj) {
   this.getDesktopWindow().aMenu=obj
}


function  public_getSkinUrl(args){
   if (args==null)
      args={}
   if (args.skin==null)
      args.skin=this.getSkin();
   return this.getDesktopProperties()["urlSkins"]+'/'+args.skin
}



function public_blur() {
   this.getDrDesktopObj().blur()
}

function public_focus(params) { //{[id],[noZIndexChange],[zIndex]}
   if (params==null)
      params={}
   if (params.id==null )
      params.id=this.getId()
   return (this.getDrDesktopObj().focus(params))
}

function public_hasFocus(params) { //{[id]}
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   return this.getDrDesktopObj().hasFocus({id:id})
}


function private_initConfig() {//se ejecuta una vez al principio
   //Preproceso los atributos de URL y de URL de ayuda de las aplicaciones
   var relativeHelpAttrName="relativeHelpUrl"
   var helpAttrName="helpUrl"
   var apps=this.getApplicationsConfig()
   for (app in apps)  {
      var appCfg=apps[app]
      if (appCfg[relativeHelpAttrName]!=null)  {
         var idGrupoAplicacion=appCfg.grupo
         var nombreGrupoApp= this.getApplicationsGroupsConfig()[idGrupoAplicacion].name
         appCfg[helpAttrName]=this.getDesktopProperties()["urlHelpRoot"] +"/"+ nombreGrupoApp +"/"+appCfg[relativeHelpAttrName]
      }
      if (appCfg["relativeUrl"]!=null)  {
         var idGrupoAplicacion=appCfg.grupo
         var nombreGrupoApp= this.getApplicationsGroupsConfig()[idGrupoAplicacion].name
         appCfg["url"]=this.getDesktopProperties()["urlAppRoot"] +"/"+ nombreGrupoApp +"/"+appCfg["relativeUrl"]
      }
   }
}


function public_setNoDrag(params){
   if (params==null)
      params={}
   var id=params.id,noDrag=params.noDrag
   if (id==null)
      id=this.getId()
   this.getDrDesktopObj().wData({id:id, property:"wNoDrag", value: drEvaluaBooleanoString(noDrag)})
}

function public_getNoDrag(params){
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   return this.getDrDesktopObj().wData({id:id, property:"wNoDrag"})
}

function public_setNoResize(params){
   if (params==null)
      params={}
   var id=params.id,noResize=params.noResize
   if (id==null)
      id=this.getId()
   this.getDrDesktopObj().wData({id:id, property:"wNoResize", value: drEvaluaBooleanoString(noResize)})
}

function public_getNoResize(params){
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   return this.getDrDesktopObj().wData({id:id, property:"wNoResize"})
}

function public_setGlobalNoDrag(params){
   if (params==null)
      params={}
   var globalNoDrag=params.globalNoDrag
   this.setGlobalBoolean({name:"globalNoDrag", value:globalNoDrag})
}

function public_setGlobalNoResize(params){
   if (params==null)
      params={}
   var globalNoResize=params.globalNoResize
   this.setGlobalBoolean({name:"globalNoResize", value:globalNoResize})
}

function public_setGlobalNoClose(params){
   if (params==null)
      params={}
   var globalNoClose=params.globalNoClose
   this.setGlobalBoolean({name:"globalNoClose", value:globalNoClose})
}

function public_setGlobalNoContextMenu(params){
   if (params==null)
      params={}
   var globalNoContextMenu=params.globalNoContextMenu
   this.setGlobalBoolean({name:"globalNoContextMenu", value:globalNoContextMenu})
}

function public_getGlobalNoDrag(){
   return this.getGlobalBoolean({name:"globalNoDrag"})
}

function public_getGlobalNoResize(){
   return this.getGlobalBoolean({name:"globalNoResize"})
}

function public_getGlobalNoClose(){
   return this.getGlobalBoolean({name:"globalNoClose"})
}

function public_getGlobalNoContextMenu(){
   return this.getGlobalBoolean({name:"globalNoContextMenu"})
}


function private_setGlobalBoolean(params) {
   if (params==null)
      params={}
   var name=params.name, value=drEvaluaBooleanoString(params.value)
   this.setDesktopConfigValue({name:name, value:value})
}

function private_getGlobalBoolean(params) {
   if (params==null)
      params={}
   var name=params.name
   return drEvaluaBooleanoString( this.getDesktopConfigValue({name:name}) )
}

function public_setSkinData(args) { //params: var skin=args.skin, data=args.data
   var ww=this.getDesktopWindow();
   if (args==null)
      args={}
   if (args.skin==null)
      args.skin=this.getSkin() //Por defecto el skin activo
   var skin=args.skin, data=args.data
   ww["oSkin_"+skin]=args.data
}

function public_getSkinData(args) {//params: var skin=args.skin
   //Este m�todo retorna oSkin_XX si existe (ese objeto se crea si se llama a la funci�n oSkinFactory.createSkin) si no es null
   /*
   var ww=this.getDesktopWindow();
   if (args==null || args.skin==null)
      args={skin:this.getSkin()} //Por defecto el skin activo
   var skin=args.skin
   return ww["oSkin_"+skin]
   */
   return this.getDrDesktopObj().getSkinData()
}

function public_getTaskbarData ()   {
   return this.getDesktopWindow()["oDatosBotonera_"+this.getSkin()]
}

function public_setTaskbarData (args)   {
   if (args.data)
      this.getDesktopWindow()["oDatosBotonera_"+this.getSkin()]=args.data
}

function public_getSkinParameters(args) {
   if (args==null || args.skin==null)
      var skin=this.getSkin();
   else
      var skin=args.skin
   var skins=this.getSkins();
   for (var r=0; r<skins.length; r++)
      if (skins[r].id==skin)
         return skins[r].parameters
}

function private_prepareSkinParameterChange(args)  {
   var name=args.name, value=args.value
   var ww=this.getDesktopWindow()
   if (ww.oSkinParameterChanges==null)
      ww.oSkinParameterChanges={skin:this.getSkin(), changes:{}}
   ww.oSkinParameterChanges.changes[name]=value
     // alert(name+":"+value)
}

function private_getSkinParameterChanges()  {
   var ww=this.getDesktopWindow()
   if (ww.oSkinParameterChanges==null)
      return {}
   else
      return ww.oSkinParameterChanges
}


function public_getSkinMeta(args) {
   if (args==null || args.skin==null)
      var skin=this.getSkin();
   else
      var skin=args.skin
   var skins=this.getSkins();
   for (var r=0; r<skins.length; r++)
      if (skins[r].id==skin)
         return skins[r].paramMeta
}

function public_getFileTypesConfig()   {
   return this.getDesktopWindow().oTipos
}

function public_openFile(args) { //Abre una nueva ventana con los argumentos. 
   //la URL es la de la aplicacion mas el par�metro "file="args.file  con una ruta RELATIVA a su fileSystem (ojo seguridad)

   //var extension=drExtraeExtension(rutaFichero);
   var extensionMatch=args.file.match(/.([^.]+)$/)
   var extension=null
   if (extensionMatch!=null)
      extension=extensionMatch[1]
   if (extension==null || extension=="")
      return 
   var extData=this.getFileTypesConfig()[extension]
   if (extData==null)	{
		//alert("La extensi�n '"+extension+"' no est� asociada a ninguna aplicaci�n.");
		return;
		}
   args.application=extData.aplicacion
   if (args.parameters==null)
      args.parameters=[]
   args.parameters[args.parameters.length]={name:"file", value:args.file}
   return this.openApplication(args);
}


function public_deleteCookie(args){ //args.name requerido
   drWindow.getDesktopWindow().$.cookie(args.name, null);
}

function public_getCookie(args)   {//args.name requerido
   return drWindow.getDesktopWindow().$.cookie(args.name);
}

function public_setCookie(args)   {//args.name args.value requeridos
   drWindow.getDesktopWindow().$.cookie(args.name, args.value, args);
}

function public_getUserData(args) {
   /*casos: 
      cache:true --> uso los que tengo cacheados
      async:false, cache:false --> voy a por ellos sincronizadamente
      async:true, cache:false, callback:[function] --> voy a por ellos asincronamente
   */
   if (args==null)
      args={}
   var ww=drWindow.getDesktopWindow();
   if (args.cache)   {
      return ww.drUserData //TO-DO
      }
   else if (args.async)   { 
      ww.ConfigurationService.getUserConfig({
         async:true
         ,callback:args.callback //necesito un argumento callback 
         ,timeout:conDwrConnectionTimeout
         ,errorHandler:conDwrServicesInit_errorHandler
      });
      }
   else  {
      var objResp={}
		ww.ConfigurationService.getUserConfig({
         async:false
         ,callback:function (obj)   { objResp.userData=obj } //Utilizo una closure, unica solucion en dwr 2.0
         ,timeout:conDwrConnectionTimeout
         ,errorHandler:conDwrServicesInit_errorHandler
      });
      
      if (objResp.userData==null){
      	if (!window.sesslostmessshown)	{
	      	window.sesslostmessshown=true;
	      	var recargar= drWindow.showConfirm({message: drResources.get({bundle:"core",key:"ventanasJs.txt6"}) }) //La sesi�n parece haberse perdido. Para solucionar este problema recargue la ventana del escritorio y vuelva a logarse. �Desea recargar ahora?
   	   	if (recargar)
      			this.sessionRestart()
      		
      	}
     		objResp.userData={}
      }
            
      ww.drUserData=objResp.userData
      return objResp.userData;
      }
   return null;
   }

function conDwrServicesInit_errorHandler(message)	{
	drAlert( drResources.get({bundle:"core",key:"ventanasJs.txt1"}) + "\n  [ " + message+" ]"); 
}

/*
function public_getUserData_ret(obj,objResp)   {
   //alert(obj.nombre)
   //debugger
   //alert(obj.nombre)
   //objResp.ww.pepe=obj
   //objResp=obj
}*/

function public_showMessage(params) {
   if (params.message==null)
      params.message="- empty -"
	if (params.sistemMessage && !oDesktopConfig.mensajesDelSistema)
		return
	//if (window.showModalDialog)	{
   if (jQuery.browser.msie){
		var objDatos={	
			texto:params.message
			,titulo:"Desktop Dialog Box"
			,botones:[{texto: drResources.get({bundle:"core",key:"ventanasJs.txt2"}) , retorna:1, iconoN:"img/drIcoAceptarN.gif", iconoS:"img/drIcoAceptarS.gif"}]
			}
		abreVentanaModalDesktop(objDatos)
		}
	else
		alert(params.message)
   }

function public_showConfirm(params) {
   if (params.message==null)
      params.message="- empty -"
	//if (window.showModalDialog)	{
   if (jQuery.browser.msie){
		var objDatos={	
			texto:params.message
			,titulo:"Desktop Dialog Box"
         ,botones:[
				{texto: drResources.get({bundle:"core",key:"ventanasJs.txt2"}) , retorna:true, iconoN:"img/drIcoAceptarN.gif", iconoS:"img/drIcoAceptarS.gif"}
				,{texto: drResources.get({bundle:"core",key:"ventanasJs.txt3"}) , retorna:false, iconoN:"img/drIcoCancelarN.gif", iconoS:"img/drIcoCancelarS.gif"}
            ]
			}
		return abreVentanaModalDesktop(objDatos)
		}
	else
		return confirm(params.message)
   }

/* OLD METHOD
function public_sessionSaveAndRestart(cerrarSesion,guardar) {
   var objDesktopWindow=this.getDesktopWindow()
   if (cerrarSesion==null)
      cerrarSesion=false
   if (guardar==null)
      guardar=true
   //alert("CIERRO OK cerrarSesion:["+cerrarSesion+", guardar:"+guardar)
	top.deseaSalir=true;
   if (guardar){
      //cierraSesionPrevio
      var objDesktopConfig=this.getDesktopConfig()
      objDesktopWindow.cierraSesionVentanas()
      objDesktopWindow.cierraSesionSkin()
      objDesktopWindow.cierraSesionIconos()
      if (objDesktopWindow.temaTrasReiniciar!=null)	{
         objDesktopConfig.margSupAnt=this.getDrDesktopObj().getTopMargin({skin:drWindow.getSkin()})
         this.getDesktopConfig().tema=objDesktopWindow.temaTrasReiniciar
         }
      //objDesktopWindow.document.fcerrar.configuracion.value=objDesktopWindow.objetoAJSONString(objDesktopConfig)
      objDesktopWindow.$("#configuracion").val(objDesktopWindow.objetoAJSONString(objDesktopConfig))
      objDesktopWindow.$("#guardar").val("S")
   } else
      objDesktopWindow.$("#guardar").val("N")   
   if (cerrarSesion)
      objDesktopWindow.$("#cerrarSesion").val("S")
   else
      objDesktopWindow.$("#cerrarSesion").val("N")

   objDesktopWindow.$("#fcerrar").get(0).submit();
   }*/

if (window.conDwrConnectionTimeout==null)
   window.conDwrConnectionTimeout=15000;

function public_sessionSaveAndRestart(cerrarSesion,guardar) { //defecto: false,true
   var objDesktopWindow=this.getDesktopWindow()
   if (window!=objDesktopWindow)
      return objDesktopWindow.drWindow.sessionSaveAndRestart(cerrarSesion,guardar)

   if (cerrarSesion==null)
      cerrarSesion=false
   if (guardar==null)
      guardar=true
	top.deseaSalir=true;
   if (guardar){
      //cierraSesionPrevio
      var objDesktopConfig=this.getDesktopConfig()
      objDesktopWindow.cierraSesionVentanas()
      objDesktopWindow.cierraSesionSkin()
      objDesktopWindow.cierraSesionIconos()
      if (objDesktopWindow.temaTrasReiniciar!=null)	{
         objDesktopConfig.margSupAnt=this.getDrDesktopObj().getTopMargin({skin:drWindow.getSkin()})
         this.getDesktopConfig().tema=objDesktopWindow.temaTrasReiniciar
      }
      objDesktopWindow.$("#configuracion").val(objDesktopWindow.objetoAJSONString(objDesktopConfig))
   }
   //Llamo a DWR
   var DWRParams={
      async:false 
      ,callback:function(obj){
         if (obj!=null  && obj.errorDeOperacion)	{
            drWindow.showMessage({"message":  drResources.get({bundle:"core",key:"ventanasJs.txt1"}) +" '"+obj.excepcionError.message+"'"  })
         }
         top.document.location = ""+top.document.location; //.reload() no va bien en FF
      }
      ,timeout:conDwrConnectionTimeout
      ,errorHandler:function(msg){
         drWindow.showMessage({"message":  drResources.get({bundle:"core",key:"ventanasJs.txt1"}) +" '"+msg+"'"  })
         top.document.location = ""+top.document.location; //.reload() no va bien en FF
      }
   }
   SaveSessionService.saveSession(cerrarSesion, guardar, $("#ventanas").val(), $("#iconos").val(), $("#configuracion").val(), $("#skinParameters").val(), DWRParams)
}

function public_sessionRestart() {
   this.sessionSaveAndRestart(false,false)
   }

function public_sessionSaveAndClose() {
   this.sessionSaveAndRestart(true,true)
   }

function public_sessionClose() {
   //this.getStructuralWindow().document.location.reload();
   this.sessionSaveAndRestart(true,false)
   }

function public_toFront(params)  {
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   this.getDrDesktopObj().bringWindowToFront({id:id})
   }


function public_toBack(params)  {
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   //Obtengo la lista de las ventanas que hay ordenadas seg�n su z-index...
   //Esto falla con ventanas que tengan z-index especial (se lo carga)
   var idsVentanas=this.getWindowIds()
   var listaDatosVentanas=[]//, cbeObject=this.getXDesktopObj().cbe
   var drDesktop=this.getDrDesktopObj()
   for (var r=0; r<idsVentanas.length; r++)  {
      var fixedZIndex=this.getDrDesktopObj().wData({id:idsVentanas[r], property:"fixedZIndex"})
      if (!fixedZIndex){
         var zindex=parseInt(drDesktop.get$Win({id:idsVentanas[r]}).css('zIndex'))
         listaDatosVentanas[listaDatosVentanas.length]={id:idsVentanas[r],zindex: zindex}
      }
   }
   listaDatosVentanas=listaDatosVentanas.sort(function (a,b){
      if (a.zindex>b.zindex)
         return 1
      return -1
      })
   for (var r=0; r<listaDatosVentanas.length; r++) {
      var idVentana=listaDatosVentanas[r].id
      if (idVentana!=id)
         this.toFront({id:idVentana})
      }
   }


function public_reload(params)   {
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   windowApl=this.getNativeWindow({id:id})
   if (windowApl!=null) { //Lo normal, el obj Window existe ...
      windowApl.location.reload(); 
      return true
      }
   return false
   }


function public_bindDesktopResize(args) {
   if (args==null)
      args={}
   if (args.id==null)
      args.id=this.getId()
   if (args.handlerName==null)
      args.handlerName="onDesktopResize"//Por defecto ...
   //args.windowApl=this.getNativeWindow(args)
   //args.func=args.windowApl[args.handlerName]
   var ww=this.getDesktopWindow()
   if (ww.onDesktopResize_global_handlers==null)
      ww.onDesktopResize_global_handlers=[]
   var encontrado=false, indice=0
   while (indice<ww.onDesktopResize_global_handlers.length && !encontrado) {
      var argsHandler=ww.onDesktopResize_global_handlers[indice]
      if (argsHandler.id==args.id && argsHandler.handlerName==args.handlerName)
         encontrado=true;
      indice++
      }
   if (!encontrado)
      ww.onDesktopResize_global_handlers[ww.onDesktopResize_global_handlers.length]=args
   ww.$(ww).bind("resize",ww.onDesktopResize_global)//Solo lo va a hacer una vez ...
   }

function onDesktopResize_global()   {
   for (var r=0; r<onDesktopResize_global_handlers.length; r++)   {
      var args=onDesktopResize_global_handlers[r]
      var windowApl=drWindow.getNativeWindow(args) 
      if (windowApl!=null) {
         var func=windowApl[args.handlerName]
         if (func!=null)
            func()
         else  {
            //elimino esta referencia
            }
         }
      else  {
         //elimino esta referencia
         }
      }
   }

function public_killDaemons(args) {//id (mata todos los demonios de una ventana)
   return this.getDrDesktopObj().killDaemons( drWindow.getDesktopWindow().$.extend({window:window},args) )
   }

function public_createDaemon(args)   {//id, milliseconds, script
   return this.getDrDesktopObj().createDaemon( drWindow.getDesktopWindow().$.extend({window:window},args) )
   }

function public_getApplicationIds() {
   var oAplicacion=this.getApplicationsConfig()
   var resp=[]
   for (a in oAplicacion)  {
      if (a+""!="")
         resp[resp.length]=a
      }
   return resp
   }

function public_setTop(params) {
   this.moveResizeTo(params)
   }

function public_setLeft(params) {
   this.moveResizeTo(params)
   }

function public_setWidth(params) {
   this.moveResizeTo(params)
   }

function public_setHeight(params) {
   this.moveResizeTo(params)
   }

function public_resizeTo(params) {
   this.moveResizeTo(params)
   }

function public_moveTo(params) {
   this.moveResizeTo(params)
   }

function public_getInnerWidth(args) {
   return this.getWidth(args) - this.getSkinData().windowChromeWidth

 //this.getSkinData().windowChromeWidth
 //this.getSkinData().windowChromeHeight

 //  var iFrame=this.getNativeIframe(args)
 //  return iFrame.offsetWidth
   //return $(iFrame).innerWidth()//No va en FF
}

function public_getInnerHeight(args) {
   return this.getHeight(args) - this.getSkinData().windowChromeHeight
   //return this.getHeight() - this.getSkinData().windowChromeHeight

   //var iFrame=this.getNativeIframe(args)
   //return $(iFrame).innerHeight() //No va en FF
   //return iFrame.offsetHeight
}

function public_getWidth(params) {
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   return parseIntSecure(this.getDrDesktopObj().wData({id:id, property:"wWidth"}))
   }

function public_getHeight(params) {
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   return parseIntSecure(this.getDrDesktopObj().wData({id:id, property:"wHeight"}))
   }

function public_getTop(params) { 
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   var propTop=this.getDrDesktopObj().wData({id:id, property:"wY"})
   if ((""+propTop).match(/^b(\d+)$/))
      return propTop
   var propIgnorarMargenes=this.getDrDesktopObj().wData({id:id, property:"wIgnoreMargins"})
   if (propIgnorarMargenes!=null) {
      var ignorarMargenes=drEvaluaBooleanoString(propIgnorarMargenes)
      }
   if (ignorarMargenes) 
      return parseIntSecure(propTop)
   else
      return parseIntSecure(propTop)-this.getTopMargin();
   }

function public_getLeft(params) {
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId()
   var propLeft=this.getDrDesktopObj().wData({id:id, property:"wX"})
   if ((""+propLeft).match(/^r(\d+)$/))
      return propLeft
   return parseIntSecure(propLeft)
   }

function public_moveResizeTo(params) {
   if (params==null)
      params={}
   var id=params.id
   var l=params.left
   var t=params.top
   var h=params.height
   var w=params.width
   //var oXD=this.getXDesktopObj()
   var drDesktop=this.getDrDesktopObj()
   if (id==null)
      id=this.getId()
   l=(l!=null)?l:drDesktop.wData({id:id, property:"wX"})
   w=(w!=null)?w:drDesktop.wData({id:id, property:"wWidth"})
   h=(h!=null)?h:drDesktop.wData({id:id, property:"wHeight"})
   if (t!=null)   {   
      var propIgnorarMargenes=drDesktop.wData({id:id, property:"wIgnoreMargins"})
      if (propIgnorarMargenes!=null) { 
         var ignorarMargenes=drEvaluaBooleanoString(propIgnorarMargenes)
         }
      t=drPreProcesaTop(t,h,ignorarMargenes)
      }
   else
      t=drDesktop.wData({id:id, property:"wY"})
   //Recoloco
   if (drDesktop.wData({id:id, property:"wStat"}) !="MAX") {
      drDesktop.get$Win({id:id}).mvTo({left:l,top:t}).rszTo({width:w,height:h})
      }
   drDesktop.setWDataArray(id, [["wWidth",w],["wHeight",h],["wX",l],["wY",t]])
   }

function drPreProcesaTop(top,height,ignorarMargenes)	{ //top puede ser algo como 20, b20 o center
   if (top==null)
      return 
   var ignorarMargenes=drEvaluaBooleanoString(ignorarMargenes)
   if (ignorarMargenes) {
      var marginTop=0
      var clientHeight=drWindow.getDesktopWindow().document.body.clientHeight
      }
   else  {  
      var marginTop=drWindow.getTopMargin(); 
      var clientHeight=drWindow.getDesktopWindow().document.body.clientHeight - marginTop - drWindow.getBottomMargin()
      }
   if (top=="center")  {
      var puntoMedio=Math.round(clientHeight/2)+marginTop
      return puntoMedio-Math.round(height/2);
      }
   if (!isNaN(parseInt(top)))	
      return parseInt(top) + marginTop    		//Le sumo el margen superior.
   return top
	}

function public_getDesktopWindow()  {//Retorna la ventana en la que reside el escritorio
   if (this.desktopWindow==null) {
      if (window.top.f1==null){
         if (window.dialogArguments!=null && window.dialogArguments.openerWindow!=null)
            this.desktopWindow=window.dialogArguments.openerWindow.top.f1
      }else
         this.desktopWindow=window.top.f1
   }
   return this.desktopWindow   //Presuponemos una cierta estructura en el top, si surge la necesidad de modificar la estructura habr� que tocar esto.
   }
   
function public_getStructuralWindow()  {//Retorna la ventana en la que reside el escritorio
   return window.top
   }  

function public_getDrDesktopObj()  {
   return this.getDesktopWindow().drDesktop
   }

function public_getDesktopConfig()  {
   return this.getDesktopWindow().oDesktopConfig
   }

function private_setDesktopConfigValue(args)  {
   var name=args.name, value=args.value
   this.getDesktopWindow().oDesktopConfig[name]=value
}

function private_getDesktopConfigValue(args)  {
   var name=args.name
   return this.getDesktopWindow().oDesktopConfig[name]
}

function public_getDesktopProperties() {
   return this.getDesktopWindow().oDesktopProperties
   }

function public_getEnginesConfig()	{
   return this.getDesktopWindow().oMotores
   }

function public_getApplicationsConfig()	{
   return this.getDesktopWindow().oAplicaciones
   }
   
function public_getApplicationsGroupsConfig()	{
   return this.getDesktopWindow().oGruposAplicaciones
   }   

/*
function public_getSkinGroupsConfig()	{ //Solo los skins que estan asociados a grupos (util para desinstalar grupos)
   return this.getDesktopWindow().oSkinsGrupos
   }   */

function public_getSkins() {
   return this.getDesktopWindow().oSkins
   }

function public_getSkin() {
   return this.getDesktopConfig().tema
   }

function public_setSkin(params) { //No se activa hasta reiniciar
   var tema=params.skin
   this.getDesktopWindow().temaTrasReiniciar=tema
   //this.getDesktopConfig().tema=tema
   }

function public_getTopMargin()   {
   return this.getDrDesktopObj().getTopMargin({skin:this.getSkin()})
   }

function public_getBottomMargin()   {
   return this.getDrDesktopObj().getBottomMargin({skin:this.getSkin()})
   }

function public_getClientWidth() {
   return this.getDesktopWindow().document.body.clientWidth
   }

function public_getClientHeight() {
   return this.getDesktopWindow().document.body.clientHeight - this.getTopMargin() - this.getBottomMargin()
   }

function public_getDaemonRegistry() {
   return this.getDrDesktopObj().getDaemonRegistry({window:window})
   /*
	if (this.getDesktopWindow().oDaemons==null)	
		this.getDesktopWindow().oDaemons=new clDrRegistroDaemons()
   return this.getDesktopWindow().oDaemons
   */
   }

function public_generateId()  {
   var losID="|"+ this.getWindowIds().join("|") +"|"
   var nuevoId="drWin"+Math.round(Math.random(33)*100000000)
   while (losID.indexOf("|"+nuevoId+"|")!=-1)	
      nuevoId="drWin"+Math.round(Math.random(33)*100000000)
   return nuevoId
   }

function public_getWindowIds(args) {
   return this.getDrDesktopObj().getWindowIds( drWindow.getDesktopWindow().$.extend({window:window},args) )
   }


function private_refreshWindowObjectProperties(ventana, savingSession) {
  // var xD=this.getXDesktopObj()
   var drDesktop=this.getDrDesktopObj()
   var id=ventana.id
   //Actualizo las propiedades din�micas
   ventana.width = this.getWidth({id:ventana.id})
   ventana.height = this.getHeight({id:ventana.id})
   if (ventana.innerWidth!=null)
      ventana.innerWidth=this.getInnerWidth({id:ventana.id})
   if (ventana.innerHeight!=null)
      ventana.innerHeight=this.getInnerHeight({id:ventana.id})
   ventana.position = this.getLeft({id:ventana.id}) +","+ this.getTop({id:ventana.id}) //No uso wPos porque al regenerar puede que no existan las ventanas desde las que te colocaste con un offset
   ventana.url = drDesktop.wData({id:id, property:"wUrl"})
   if (savingSession){
	   ventana.title = ventana.sessionSaveTitle
	} else {
	   if (drDesktop.wData({id:id, property:"wTitle"})!=null)
	      ventana.title = drDesktop.wData({id:id, property:"wTitle"})
	}
   if (drDesktop.wData({id:id, property:"drUrlInfo"})!=null)
      ventana.helpUrl = drDesktop.wData({id:id, property:"drUrlInfo"})
   if (drDesktop.wData({id:id, property:"wNoDrag"})!=null)
      ventana.noDrag = drDesktop.wData({id:id, property:"wNoDrag"})
   if (drDesktop.wData({id:id, property:"wNoResize"})!=null)
      ventana.noResize = drDesktop.wData({id:id, property:"wNoResize"})
   var wStat=drDesktop.wData({id:ventana.id, property:"wStat"})
   if (wStat=="min" || wStat=="min-max")
      ventana.status ="minimized"
   else if (wStat=="MAX")
      ventana.status ="maximized"
   else
      ventana.status=null
   ventana.virtualDesktop=drWindow.getVirtualDesktop({id:id})
   //actualizo la informaci�n de los DAEMON
   var regDemoniosEsaVentana=this.getDaemonRegistry().dame({idVentana:ventana.id})
   if (regDemoniosEsaVentana!=null) {
      var nuevoDaemonsWindow=[]
      for (var t=0; t<regDemoniosEsaVentana.length; t++) {
         var reg=regDemoniosEsaVentana[t]
         nuevoDaemonsWindow[t]={"milliseconds":reg.milliseconds,"script":reg.script,"miFuncion":reg.miFuncion}
         }
      ventana.daemons=nuevoDaemonsWindow
      }
   else
      ventana.daemons=null
   try   {
      //var nativeWindow=this.getNativeWindow({id:ventana.id})
      if (ventana.DrDesktopDameTestigo && isFunction(ventana.DrDesktopDameTestigo) && window.objetoAJSONString!=null)	{
         var oTestigo=ventana.DrDesktopDameTestigo()
         var testigoJson=objetoAJSONString(oTestigo,true)
         ventana.testigoJson=testigoJson
         }
      } catch(e)  {}
   return ventana
}


function public_getWindowExist(args)  {
   var id=null
   if (args!=null && args.id!=null)
      id=args.id
   if (id==null)
      id=this.getId();
   if (id==null)
      return false;
   return drWindow.getDrDesktopObj().cList.windowExists(id)
}

function public_getWindowData(args)  {
	if (args==null)
		args={}
	var id=args.id, savingSession=args.savingSession; 
   //var ventanas=registroVentanas.getLista();
   var ventanas=drWindow.getDrDesktopObj().cList.getPropertyArray("wOpenArgs")
   if (id!=null) {//Si hay id doy los datos actualizados de una ventana
      //Busco el objeto ventana adecuado
      var indice=-1;
      for (var r=0;r<ventanas.length && indice<0; r++)  
         if (ventanas[r].id==id)
            indice=r
      if (indice==-1) //Si he dado el indice de una ventana que no existe retorno null
         return
      var ventana=ventanas[indice]
      //Actualizo sus propiedades
      return this.refreshWindowObjectProperties(ventana, savingSession)
   }
   for (var r=0; r<ventanas.length; r++)	//Si no hay id entonces retorno un array con todas las ventanas
      ventanas[r]=this.refreshWindowObjectProperties(ventanas[r], savingSession)
   return ventanas
}

function public_arrangeAllWindows(params)  {
   if (params!=null) {
      var startX=params.startX
      var startY=params.startY
      var offsetX=params.offsetX
      var offsetY=params.offsetY
      }
   this.getDrDesktopObj().arrangeAllWindows({left:startX,top:startY,offsetLeft:offsetX,offsetTop:offsetY})
}



function public_open(params)	{
   return this.getDrDesktopObj().open(params,window)
}

function public_openWindowArray(obj)   {
   arrObj=obj.windows;
   if (obj.startingSession)   {
      //Recupero el estado de ciertas variables.
      //Para asegurar que las ventanas �nicas sigan siendo �nicas tengo que modificar el array de aplicaciones. 
      for (var r=0; r<arrObj.length; r++)    {
         var esUnica=drEvaluaBooleanoString(arrObj[r].single)
         //Si es unica genero un id
         if (esUnica &&  arrObj[r].application!=null && arrObj[r].id !=null)   {
            var oAplicacion=this.getApplicationsConfig()[arrObj[r].application]
            if (oAplicacion!=null)
               oAplicacion.id=arrObj[r].id
            }
         }
      }
   for (var r=0; r<arrObj.length; r++) {
      this.open(arrObj[r]);
      }
   }

function public_getApplicationExists(args){
   if (this.getApplicationsConfig()[args.application]!=null)
      return true
   return false
}

function public_alert(args){
   drAlert(args.message)
}

function public_openApplication(obj)	{
   var sinonimos=[["aplicacion","application"],["parametros","parameters"],["motor","engine"],["unica","single"],["titulo","title"],["ancho","width"],["alto","height"],["anchoMinimo","minWidth"],["altoMinimo","minHeight"],["posicion","position"], ["urlAyuda","helpUrl"]]
   var propiedadesAVolcar=["js","id" ,"url","title","width","height","minWidth","minHeight","maxWidth","maxHeight","position","onClose","chromeType","testigo","testigoJson","helpUrl","skin","parameters","status","noTaskbar","widget","single", "daemons", "sessionSave","imageFamily", "noHide", "ignoreMargins", "zIndex","virtualDesktop","showInAllDesktops", "innerWidth", "innerHeight", "noResize","noDrag", "relativeHelpUrl","parameter"]
   obj=drAplicaSinonimos(sinonimos,obj)
   //Resuelvo el objeto oAplicacion
   var oAplicacion=this.getApplicationsConfig()[obj.application]
	if (oAplicacion==null)	{
		drAlert("La aplicaci�n identificada como <b>'"+obj.application+"'</b> no est� registrada.");
		return
		}
   oAplicacion=drAplicaSinonimos(sinonimos,oAplicacion)
   //Resuelvo el objeto Motor si lo hay
	if (oAplicacion.engine!=null)	{
		oAplicacion.url=this.getEnginesConfig()[oAplicacion.engine].url
      }
   var unica=drEvaluaBooleanoString(oAplicacion.single)
   //var unica=(oAplicacion.single==null)?"":(""+oAplicacion.single).toUpperCase()
   //Si es unica genero un id
   
   //if (oAplicacion.id==null && (unica=="S"||unica=="TRUE"||unica=="Y" ))  { 
   if (oAplicacion.id==null && unica)  { 
      oAplicacion.id=this.generateId() //Esto permanece entre llamadas sucesivas ya que modifico la aplicacion
   }
   //Vuelco en OBJ las propiedades de oAplicacion
   obj=drAplicaPropiedadesPorDefecto(propiedadesAVolcar,obj,oAplicacion)

   //Si es un javascript lo ejecuto
   if (obj.js!=null)	{
      with(this.getDesktopWindow())   {
   		eval(obj.js)
      }
      return
      }
   return this.open(obj)
   }


function drGetUrlParameters() {
   var searchStr=document.location.search
   if (searchStr.charAt(0)=='?')
      searchStr=searchStr.substr(1)
   var ret={}
   var searchStrBlocks=searchStr.split("&")
   for (var r=0; r<searchStrBlocks.length;r++)  {
      var bloque=searchStrBlocks[r]
      var componentes=bloque.split("=")
      if (componentes.length==1)
         ret[unescape(bloque)]=""
      else
         ret[unescape(componentes[0])]=unescape(componentes[1])
   }
   return ret
}

function private_getApplicationsWithHelp(params)   {
   var helpAttrName="helpUrl"
   if (params && params.locale)
      helpAttrName="helpUrl_"+params.locale
   var ret=[]
   var appsConfig=this.getApplicationsConfig()
   for (app in appsConfig)  {
      var appCfg=appsConfig[app]
      if (appCfg[helpAttrName]!=null)  {
         ret[ret.length]={description:appCfg.description, icontext:appCfg.icontext, title:drResources.replaceI18NValues(appCfg.title), application:app, processedHelpUrl:appCfg[helpAttrName]}
      }
   }
   return ret
}

function private_hasHelp(args)   {
   if (args==null)
      args={}
   var params=args.params, id=args.id
   if (params!=null) {
      //if (params.locale!=null) //TO-DO: multiidioma
      return (params.helpUrl!=null)
   }
   if (id==null)
      id=this.getId()
   var windowData=this.getWindowData({id:id})
   return (windowData.helpUrl!=null)
}

function public_showHelp(params){
   if (params==null)
      params={}
   var paramsOpen={}
   var url=params.url, id=params.id,  windowId=params.windowId, locale=params.locale, globalLocale=window.globalLocale, width=params.width, height=params.height, title=params.title, position=params.position;
   if (windowId==null)
      windowId=this.getId()
   //Locale -----------
   if (locale==null)
      locale=globalLocale //que tambi�n puede ser null ...
   //me ocupo del ancho, alto y posicion -----------
   if (width!=null)
      paramsOpen.width=width
   else 
      paramsOpen.width=400;
   if (height!=null)
      paramsOpen.height=height
   else if (windowId!=null)   
      paramsOpen.height=this.getHeight({id:windowId})
   else
      paramsOpen.height=400;
   if (position!=null)
      paramsOpen.position=position
   else if (windowId!=null)   {
      var left=this.getLeft({id:windowId})+ this.getWidth({id:windowId}) + 1
      var top=this.getTop({id:windowId})
      paramsOpen.position=left+","+top
   } else
      paramsOpen.position="center,center"
   //me ocupo de la URL -----------
   var laUrl=null
   var application=null,texto="";
   if (url!=null){
      laUrl=url
   } else if (windowId!=null){ //Si hay ventana asociada ...
      //Tomo la url (en funcion tambi�n del locale)
      var windowData=this.getWindowData({id:windowId})
      if (locale!=null) {
         if (windowData["helpUrl_"+locale]!=null)
            laUrl=windowData["helpUrl_"+locale]
         else
            laUrl=windowData.helpUrl
      } else   {
         laUrl=windowData.helpUrl
      }
      if (windowData.application!=null)
         application=windowData.application
      else  {
         if (windowData.title!=null)  
            texto=drResources.replaceI18NValues(windowData.title)
         else if (windowData.iconText!=null)
            texto=windowData.iconText
         else if (windowData.description!=null)
            texto=windowData.description
         else 
            texto=windowData.application
      }

   }
   paramsOpen.parameters=[{name:"url", value:laUrl},{name:"application", value:application},{name:"texto", value:texto}]
   // ----------- Otros parametros
   if (id!=null)
      paramsOpen.id=id
   if (title!=null)
      paramsOpen.title=title
   else
      paramsOpen.title="Ayuda"
   //return this.open(paramsOpen);
   paramsOpen.application="helpExplorer"
   return this.openApplication(paramsOpen);
}


function public_setTitle	(params)	{
	//sessionSaveTitle
   if (params==null)
      params={}
   var id=params.id
   var titulo=params.title
   if (id==null)
      id=this.getId();
   if (titulo==null)
      titulo="";
   this.getDrDesktopObj().setGetTitle({id:id,title:titulo})
   //this.getRegistry().getVentana(id).sessionSaveTitle=titulo
   drWindow.getDrDesktopObj().wData({id:id, property:"wOpenArgs"}).sessionSaveTitle=titulo
   var skin=this.getSkin();
   var laFuncSetTitleSkin=this.getDesktopWindow()["skin_"+skin+"_setTitle"]
   if (laFuncSetTitleSkin!=null && jQuery.isFunction(laFuncSetTitleSkin))  {
      params.id=id;
      params.title=titulo
      laFuncSetTitleSkin(params)
      }
   else  {
      var oDivTit=this.getDesktopWindow().document.getElementById(id+"_wTitle")
	   if (oDivTit!=null)	
		   oDivTit.innerHTML=titulo
      }
	}

function public_getStatus(params)   {
   return this.getDrDesktopObj().getStatus( drWindow.getDesktopWindow().$.extend({window:window},params) )
}

function public_setStatus(params)   {
   return this.getDrDesktopObj().setStatus( drWindow.getDesktopWindow().$.extend({window:window},params) )
}

function public_hide (params)  { //{id}
   this.getDrDesktopObj().hide(params)
}

function public_show (params)  {//{id}
   this.getDrDesktopObj().show(params)
}

function public_changeVirtualDesktop(params) {
   return this.getDrDesktopObj().changeVirtualDesktop( drWindow.getDesktopWindow().$.extend({window:window},params) )
}

function public_getVirtualDesktopsData() {
   return this.getDrDesktopObj().getVirtualDesktopsData( {window:window} )
}

function public_getCurrentVirtualDesktop(params) {
   return this.getDrDesktopObj().getCurrentVirtualDesktop( drWindow.getDesktopWindow().$.extend({window:window},params) )
}

function public_getVirtualDesktop (params) {
   return this.getDrDesktopObj().getVirtualDesktop( drWindow.getDesktopWindow().$.extend({window:window},params) )
}

function private_inVirtualDesktop(params)  {
   return this.getDrDesktopObj().inVirtualDesktop( drWindow.getDesktopWindow().$.extend({window:window},params) )
}

function public_setVirtualDesktop (params) {
   return this.getDrDesktopObj().setVirtualDesktop( drWindow.getDesktopWindow().$.extend({window:window},params) )
}

function public_close(params){
   if (params==null)
      params={}
   var id=params.id
   if (id==null)
      id=this.getId();
   this.getDrDesktopObj().removeWindow({id:id})
	}

function public_getId	()	{
	if (window.frameElement!=null && window.frameElement.drIdVentana!=null)
		return window.frameElement.drIdVentana
   var ventanaInspeccionada=window; 
   while(ventanaInspeccionada.parent!=ventanaInspeccionada) {
      ventanaInspeccionada=ventanaInspeccionada.parent
      if (ventanaInspeccionada.frameElement!=null && ventanaInspeccionada.frameElement.drIdVentana!=null)
         return ventanaInspeccionada.frameElement.drIdVentana
      }

	return null	
	}

function public_getNativeWindow(params)	{
   return this.getDrDesktopObj().getNativeWindow( drWindow.getDesktopWindow().$.extend({window:window},params) )
   /*
   var Iframe=this.getNativeIframe(params)
   if (Iframe!=null)
      return Iframe.contentWindow*/
   }

function public_getNativeIframe(params)	{
   return this.getDrDesktopObj().getNativeIframe( drWindow.getDesktopWindow().$.extend({window:window},params) )
   /*
   if (params==null)
      params={}
   if (params.id==null)
      params.id=this.getId()
   var id=params.id
   var ww=drWindow.getDesktopWindow()
   return ww.$("#drIfr_"+id).get(0)
   //return drWindow.getDesktopWindow().document.getElementsByName('drIfr_'+id)[0]
   */
   }



// -----------------------------------------------------


function drAbreFichero (rutaFichero,idVentana,titulo,url,ancho,alto,posicion,testigo,elOnClose,urlAyuda,anchoMinimo,altoMinimo)	{
	/*TO-DO: parametro url*/
	var extension=drExtraeExtension(rutaFichero);
	if (drWindow.getDesktopWindow().oTipos[extension] ==null)	{
		alert(drResources.get({bundle:"core",key:"ventanasJs.txt4"}) +extension+ drResources.get({bundle:"core",key:"ventanasJs.txt5"}));
		return;
		}
	var idAplicacion=drWindow.getDesktopWindow().oTipos[extension].aplicacion
	var oAplicacion=drWindow.getDesktopWindow().oAplicaciones[idAplicacion]
	var oMotores=drWindow.getDesktopWindow().oMotores
	var laUrlFinal=oAplicacion.url+"?file="+escape(rutaFichero)
	//alert(laUrlFinal)
	if (oAplicacion.motor!=null)	{
		//Esto habr?a que hacerlo pasar por un punto ?nico (por el usuario, etc ?)
		laUrlFinal=oMotores[oAplicacion.motor].url
		}
	if (laUrlFinal!=null)	{
      var unica=drEvaluaBooleanoString(oAplicacion.unica)
		//if (oAplicacion.unica!=null && (oAplicacion.unica=="S"||oAplicacion.unica=="s"||oAplicacion.unica=="true") && oAplicacion.idVentana==null)
      if (unica && oAplicacion.idVentana==null)
			oAplicacion.idVentana=dameIdQueNoColisione()
		//Tomo los valores de la aplicaci?n
		ancho=(ancho==null)?oAplicacion.ancho:ancho;
		alto=(alto==null)?oAplicacion.alto:alto;	
		anchoMinimo=(anchoMinimo==null)?oAplicacion.anchoMinimo:anchoMinimo;		
		altoMinimo=(altoMinimo==null)?oAplicacion.altoMinimo:altoMinimo;			
		posicion=(posicion==null)?oAplicacion.posicion:posicion;	
		testigo=(testigo==null)?oAplicacion.testigo:testigo;
		idVentana=(idVentana==null)?oAplicacion.idVentana:idVentana;
		elOnClose=(elOnClose==null)?oAplicacion.elOnClose:elOnClose;	
		//url=(url==null)?oAplicacion.url:url;	
		titulo=(titulo==null)?oAplicacion.titulo:titulo;		
		urlAyuda=(urlAyuda==null)?oAplicacion.urlAyuda:urlAyuda;	
		
		//Aplicamos los par?metros
		if (oAplicacion.parametros!=null && oAplicacion.parametros.length>0)	{
			laUrlFinal=laUrlFinal+"?"
			for (var r=0;r<oAplicacion.parametros.length;r++)	{
				if (r>0)
					laUrlFinal=laUrlFinal+"&"
				laUrlFinal=laUrlFinal+escape(oAplicacion.parametros[r].nombre)+"="+escape(oAplicacion.parametros[r].valor)
				}
			}
			
		return drAbreVentana(idVentana,titulo,laUrlFinal,ancho,alto,posicion,testigo,elOnClose,urlAyuda,anchoMinimo,altoMinimo)	/*TO-DO: retorne el id de la ventana*/
		}
	else if (oAplicacion.js!=null)	{
		eval(oAplicacion.js)
		}
	}

// ------------------------------------------------------------
function drEvaluaBooleanoString(valor)   {
   if (valor==null)
      return false
   if (valor==true)
      return true
   else if (valor==false)
      return false
   else  {
      var cad=(valor+"").toLowerCase()
      if (cad=="true")
         return true
      else
         return false
   }
}

function drCalculaPosicion(posicion,alto,ancho,ignorarMargenes)	{
	//Si viene algo tipo center,** o **,center o center,center reconstruyo, si no lo dejo como estaba ...
	//Tambi�n modifica el "y" para que tenga en cuenta el margen superior
	// TO-DO: contemplar a�adir el margen en las otras opciones (n,s,etc)
	var oDesktopConfig=drWindow.getDesktopConfig()
   var ignorarMargenes=drEvaluaBooleanoString(ignorarMargenes)
   if (ignorarMargenes) {
      var marginTop=0
      var marginBottom=0
      }
   else  {
      var marginTop=drWindow.getTopMargin(); 
      var marginBottom=drWindow.getBottomMargin(); 
      }
   if (posicion==null)
      return
   if (posicion=='center')
   	posicion='center,center'
   var hayCentradoHorizontal=(posicion.search( /center\s*,/ )!=-1);
   var hayCentradoVertical=(posicion.search( /,\s*center/ )!=-1);
   var hayDosPartes=(posicion.indexOf(',')!=-1)
   if (!hayDosPartes)
   	return posicion
   if (!hayCentradoHorizontal && !hayCentradoVertical)	{
   	var posX=posicion.substr(0,posicion.indexOf(','))
	   var posY=posicion.substr(posicion.indexOf(',')+1)
		if (!isNaN(parseInt(posY)))	
   		posY=parseInt(posY) + marginTop    		//Le sumo el margen superior.
      return posX+","+posY
      }
   else if (hayCentradoHorizontal && hayCentradoVertical) { 
      var posY=Math.round((drWindow.getDesktopWindow().document.body.clientHeight-alto-marginTop-marginBottom)/2 ) + marginTop
      var posX=Math.round( (drWindow.getDesktopWindow().document.body.clientWidth-ancho)/2 )
      return posX+","+posY
      }
   else if (hayCentradoHorizontal)  {
   	var posX=Math.round( (drWindow.getDesktopWindow().document.body.clientWidth-ancho)/2 )
   	var posY=posicion.substr(posicion.indexOf(',')+1)
   	if (!isNaN(parseInt(posY)))	
   		posY=parseInt(posY) + marginTop //Le sumo el margen superior.
      return posX+","+posY
      }
   else  {//hayCentradoVertical
	   var posY=Math.round((drWindow.getDesktopWindow().document.body.clientHeight-alto-marginTop-marginBottom)/2 ) + marginTop
      return posicion.replace (/center/,posY)
      }
	}


function drIncrementaYEnPosition(position,incrementoY)	{
	var oDesktopConfig=drWindow.getDesktopConfig()
   if (position==null)
      return
   var hayDosPartes=(position.indexOf(',')!=-1)
   if (!hayDosPartes)
   	return position
  	var posX=position.substr(0,position.indexOf(','))
	var posY=position.substr(position.indexOf(',')+1)
	if (!isNaN(parseInt(posY)))	
   	posY=parseInt(posY) + incrementoY
   return posX+","+posY
	}

function drCalculaValorPixeles(valor)	{
	var oDesktopConfig=drWindow.getDesktopConfig()
	//Para convertir valores como "clientWidth-200"
	if (valor==null || valor=="")
		return valor
   var valorPI=parseInt(valor)
	if (!isNaN(valorPI))
		return valorPI
	var clientWidth=drWindow.getDesktopWindow().document.body.clientWidth
	var clientHeight=drWindow.getDesktopWindow().document.body.clientHeight- this.getDrDesktopObj().getTopMargin({skin:oDesktopConfig.tema}) - this.getDrDesktopObj().getBottomMargin({skin:oDesktopConfig.tema})
	eval("valor=("+valor+")")
	return valor;
	}

function drConsideraAltoMaximo(alto)	{
	var oDesktopConfig=drWindow.getDesktopConfig()
	var clientHeight=drWindow.getDesktopWindow().document.body.clientHeight- this.getDrDesktopObj().getTopMargin({skin:oDesktopConfig.tema}) - this.getDrDesktopObj().getBottomMargin({skin:oDesktopConfig.tema})
	if (alto>clientHeight)
		alto=clientHeight
	return alto; 
	}


/*
function DruidaRegistraVentana(id,idPadre)	{
	//si idPadre es null lo calcula
	if (drWindow.getDesktopWindow().oRegistroVentanas==null)	
		drWindow.getDesktopWindow().oRegistroVentanas=new clDrRegistroVentanas()
	if (idPadre!=null)
		drWindow.getDesktopWindow().oRegistroVentanas.mete(id, idPadre);
	else
		drWindow.getDesktopWindow().oRegistroVentanas.mete(id, drWindow.getId());	
	}*/
/*
function DruidaDesregistraVentana(id)	{
	drWindow.getDesktopWindow().oRegistroVentanas.saca(id);
	return true;
	}
*/
/*
function DruidaVerificaOnWindowClose(id)	{
	var elWindow=drWindow.getNativeIframe({"id":id}).contentWindow
	if (elWindow.DrDesktopOnWindowClose!=null &&  typeof elWindow.DrDesktopOnWindowClose == 'function')
		return elWindow.DrDesktopOnWindowClose()
	else
		return true
	}
*/
function cierraSesionSkin()   {
   var cambiosSkin=drWindow.getSkinParameterChanges()
   document.fcerrar.skinParameters.value=objetoAJSONString(cambiosSkin)
}

function cierraSesionVentanas(){
   var ventanas=drWindow.getWindowData({savingSession:true});
   var ventanasASalvar=[]
   for (var r=0; r<ventanas.length; r++)  {
      if(drEvaluaBooleanoString(ventanas[r].sessionSave))
         ventanasASalvar[ventanasASalvar.length]=ventanas[r]
   }
   /*
   for (var r=0; r<ventanasASalvar.length; r++)  {
		ventanasASalvar[r].title=ventanasASalvar[r].sessionSaveTitle
   } */  
   document.fcerrar.ventanas.value=objetoAJSONString(ventanasASalvar)

   /*
	window.ventanasSinEstado=[]
	var ventanas=DruidaVentanas();
	for (var r=0; r<ventanas.length; r++)	{
		ventanas[r].ancho = drWindow.getXDesktopObj().prop(ventanas[r].id,'wWidth')
		ventanas[r].alto = drWindow.getXDesktopObj().prop(ventanas[r].id,'wHeight')
		ventanas[r].posicion = drWindow.getXDesktopObj().prop(ventanas[r].id,'wX')+","+drWindow.getXDesktopObj().prop(ventanas[r].id,'wY') //No uso wPos porque al regenerar puede que no existan las ventanas desde las que te colocaste con un offset
		ventanas[r].url = drWindow.getXDesktopObj().prop(ventanas[r].id,'wUrl')
		ventanas[r].titulo = drWindow.getXDesktopObj().prop(ventanas[r].id,'wTitle')
		ventanas[r].urlAyuda = drWindow.getXDesktopObj().prop(ventanas[r].id,'drUrlInfo')
		ventanas[r].anchoMinimo = drWindow.getXDesktopObj().prop(ventanas[r].id,'minWidth')
		ventanas[r].altoMinimo = drWindow.getXDesktopObj().prop(ventanas[r].id,'minHeight')		
		//Pero lo necesito para llamar a su evento si lo hay ...
		var ventana=drWindow.getNativeWindow({"id":ventanas[r].id})
		if (ventana.DrDesktopDameTestigo && isFunction(ventana.DrDesktopDameTestigo) && window.objetoAJSONString!=null)	{
			var oTestigo=ventana.DrDesktopDameTestigo()
			var testigo=objetoAJSONString(oTestigo,true)
			ventanas[r].testigo=testigo
			}
		else	{
			ventanas[r].testigo=null
			ventanasSinEstado[ventanasSinEstado.length]=ventanas[r].titulo
			}
		}
	document.fcerrar.ventanas.value=objetoAJSONString(ventanas)
	if (window.ventanasSinEstado!=null && window.ventanasSinEstado.length>0)	{
		if (ventanasSinEstado.length>1)	{
			if (oDesktopConfig.animacionesActivas)
				drOscureceFondo(true)			
			//Hay 1 ventana que se guardar? como <span class="drTextoResaltado" title="Aplicaciones sin estado: Al abrir el escritorio estas ventanas se recuperar?n tal y como estaban al abrirse y sin tener en cuenta la posible evoluci�n posterior de su contenido"> aplicaci�n sin estado</span>:  Frases ingeniosas
			drAlertSistema ("Hay "+ventanasSinEstado.length+' ventanas que se guardar�n como <span class="drTextoResaltado" title="Aplicaciones sin estado: Al abrir el escritorio estas ventanas se recuperar�n tal y como estaban al abrirse y sin tener en cuenta la posible evoluci�n posterior de su contenido"> aplicaciones sin estado</span>: '+ventanasSinEstado.join (", ") )
			}
		if (ventanasSinEstado.length==1)	{
			if (window.animacionesActivas)
				drOscureceFondo(true)			
			//Hay 1 ventana que se guardar? como <span class="drTextoResaltado" title="Aplicaciones sin estado: Al abrir el escritorio estas ventanas se recuperar?n tal y como estaban al abrirse y sin tener en cuenta la posible evoluci�n posterior de su contenido"> aplicacion sin estado</span>:  Frases ingeniosas
			drAlertSistema ('Hay 1 ventana que se guardar� como <span class="drTextoResaltado" title="Aplicaciones sin estado: Al abrir el escritorio estas ventanas se recuperar�n tal y como estaban al abrirse y sin tener en cuenta la posible evoluci�n posterior de su contenido"> aplicacion sin estado</span>: '+ventanasSinEstado[0])
			}
		}	*/
	}

function DruidaSonVentanasFamilia(id1,id2)	{
	//Busco en la jerarquia para ver si son ASCENDENTES O DESCENDIENTES un de la otra (hermanos no se consideran)
   if (DruidaEsVentanaDescendienteDe(id1,id2))
      return true
   if (DruidaEsVentanaDescendienteDe(id2,id1))
      return true
   return false
   /*
	if (drWindow.getDesktopWindow().oRegistroVentanas==null)
		return false
	return drWindow.getDesktopWindow().oRegistroVentanas.sonFamilia(id1,id2)
   */
}

function DruidaEsVentanaDescendienteDe(id1,id2){
	//Verifica si la ventana con id=id2 desciende de la de id=id1
   var drDesktop = drWindow.getDrDesktopObj()
   var v2=drDesktop.wData({id:id2, property:"wOpenArgs"})
   if (v2==null)
      return false
   var unIdPadre=v2.idPadre
   while (unIdPadre!=null)	{
      if (unIdPadre==id1)
         return true
      unIdPadre=drDesktop.wData({id:unIdPadre, property:"wOpenArgs"}).idPadre
      }
   return false
}


function isFunction(a)  {      
	return typeof a == 'function'; 
	}	

function DruidaVentanas()	{
	//Retornamos los objetos ventana que existen (obj de clDrRegistroVentanas) ...
   return drWindow.getDrDesktopObj().cList.getPropertyArray("wOpenArgs")
   /*
	if (drWindow.getDesktopWindow().oRegistroVentanas==null)
		return []
	return drWindow.getDesktopWindow().oRegistroVentanas.getLista();
   */
	}


//  ************************ OBJETO REGISTRO DE LOS DAEMONS
function clDrRegistroDaemons()	{
   //indizados y agrupados por idVentana tengo un array de objetos con los datos del daemon (idInterval:"")
   this.lista={}

   this.mete=function (params)   {//idVentana, idInterval, milliseconds, script
      if (this.lista[params.idVentana]==null)
         this.lista[params.idVentana]=new Array();
      var arr=this.lista[params.idVentana] //lista daemons de esa ventana
      arr[arr.length]=params
      }

   this.saca=function (params)   {//idVentana
      if (this.lista[params.idVentana]==null)
         return null
      var arr=this.lista[params.idVentana] //lista daemons de esa ventana
      this.lista=eliminaObjetoDelHash({hash:this.lista, indice:params.idVentana})
      return arr;
      }

   this.dame=function (params)   {//idVentana
      if (this.lista[params.idVentana]==null)
         return null
      var arr=this.lista[params.idVentana] //lista daemons de esa ventana
      return arr;
      }

   }

function eliminaObjetoDelHash(params)  {//{hash:xxx, indice:xxx}
   var ret={}
   for (a in params.hash)
      if (a!=params.indice)
         ret[a]=params.hash[a]
   return ret
   }

//  ************************ OBJETO REGISTRO DE LAS VENTANAS
		
/*
function clDrRegistroVentanas_2()	{//clase del Objeto controlador de las ventanas que existen
	//Restricci�n: con el mismo ID solo debe existir una ventana ... TO-DO: varios
	this.lventanas=[]


	//this.mete=function (id, idPadre)	{
   this.mete=function (nuevaVentana)	{
		//idpadre puede ser null
		//var nuevaVentana={"id":id, "idPadre": idPadre}
		var i=this.indiceVentana(nuevaVentana.id)
		if (i==-1)
			this.lventanas[this.lventanas.length]=nuevaVentana
		else
			this.lventanas[i]=nuevaVentana
		}


	this.saca=function (id)	{
		//Tengo que arreglar los idPadre de los hijos si se da el caso
		//Por cada hijo cuyo idPadre sea el id, ya que se van a quedar huerfanos les doy el id de su "abuelo"		
		var i=this.indiceVentana(id)
		if (i==-1)
			return
		var elIdPadre=this.lventanas[i].idPadre
		for (var r=0;r<this.lventanas.length; r++)
			if (this.lventanas[r].idPadre==id)
				this.lventanas[r].idPadre=elIdPadre
		//Regenero el array 
		var nuevoArrVent=[];
		for (var r=0;r<this.lventanas.length; r++)
			if (this.lventanas[r].id!=id)
				nuevoArrVent[nuevoArrVent.length]=this.lventanas[r]
		this.lventanas=nuevoArrVent
		}
		

	this.getVentana=function (id)	{
		var i= this.indiceVentana(id)
		if (i==-1)
			return null
		else
			return this.lventanas[i]
		}



	this.getLista=function ()	{
		return this.lventanas
		}


	this.existe=function (id)	{
		if (this.indiceVentana(id)==-1)
			return false
		else
			return true
		}
		
	this.indiceVentana=function (id)	{
		for (var r=0;r<this.lventanas.length;r++)
			if (this.lventanas[r].id == id)
				return r
		return -1
		}
		
	this.sonFamilia=function (id1,id2)	{
		if (this.descendienteDe(id1,id2))
			return true
		if (this.descendienteDe(id2,id1))
			return true
		return false
		}
		
	this.descendienteDe=function (id1,id2)	{
		//Verifica si la ventana con id=id2 desciende de la de id=id1
		var v2=this.getVentana(id2);	
		if (v2==null)
			return false
		var unIdPadre=v2.idPadre
		while (unIdPadre!=null)	{
			if (unIdPadre==id1)
				return true
			unIdPadre=this.getVentana(unIdPadre).idPadre
			}
		return false
		}		
	}		

function clDrRegistroVentanas()	{
   var drDesktop=drWindow.getDrDesktopObj()

   this.mete=function (nuevaVentana)	{
      drDesktop.wData({id:nuevaVentana.id, property:"wOpenArgs", value: nuevaVentana})
	}
	this.saca=function (id)	{
      drDesktop.wData({id:id, property:"wOpenArgs", value: null})
	}
	this.getVentana=function (id)	{
		return drDesktop.wData({id:id, property:"wOpenArgs"})
	}
	this.getLista=function ()	{
      return drDesktop.cList.getPropertyArray("wOpenArgs")
      
      var ret=[], IDs=drDesktop.cList.getAllId(drDesktop.lastReservedWin)
		for (var i=0; i<IDs.length; i++) 
         ret[ret.length]=drDesktop.wData({id:IDs[i], property:"wOpenArgs"})
      return ret
	}
	this.existe=function (id)	{
	   return drDesktop.cList.windowExists(id)
	}
		
	this.sonFamilia=function (id1,id2)	{
		if (this.descendienteDe(id1,id2))
			return true
		if (this.descendienteDe(id2,id1))
			return true
		return false
		}
		
	this.descendienteDe=function (id1,id2)	{
		//Verifica si la ventana con id=id2 desciende de la de id=id1
		var v2=drDesktop.wData({id:id2, property:"wOpenArgs"})
		if (v2==null)
			return false
		var unIdPadre=v2.idPadre
		while (unIdPadre!=null)	{
			if (unIdPadre==id1)
				return true
			unIdPadre=drDesktop.wData({id:unIdPadre, property:"wOpenArgs"}).idPadre
			}
		return false
		}		
	}		
*/

function drAplicaPropiedadesPorDefecto(props, obj, objDefecto)  {
   for (var r=0; r<props.length; r++)  
      if (obj[props[r]]==null)
         obj[props[r]]=objDefecto[props[r]]
   return obj
}

function drAplicaSinonimos(props, obj)  {
   for (var r=0; r<props.length; r++){
      if (obj[props[r][1]]==null && obj[props[r][0]]!=null)   {
         obj[props[r][1]]=obj[props[r][0]]
         obj[props[r][0]]=null;
         }
      }
   return obj
   }

function drExtraeExtension(rutaCompleta)	{
	var i=rutaCompleta.lastIndexOf('.');
	if (i==-1)
		return ""
	else	
		return rutaCompleta.substr(i+1);
	}	
	
function drAlertSistema (txt)	{ //Pueden no aparecer segun la configuraci�n
	drAlert_general(txt, true)
	}
	
function drAlert (txt)	{
	drAlert_general(txt, false)
	}	
  
function drAlert_general(txt, esMensajeDelSistema)	{
	if (esMensajeDelSistema && !oDesktopConfig.mensajesDelSistema)
		return
	//if (window.showModalDialog)	{
   if (jQuery.browser.msie){
		var objDatos={	
			texto:txt
			,titulo:"Desktop Dialog Box"
			,botones:[{texto:"Aceptar", retorna:1, iconoN:"img/drIcoAceptarN.gif", iconoS:"img/drIcoAceptarS.gif"}]
			}
		abreVentanaModalDesktop(objDatos)
		}
	else
		alert(txt)
	}
	
function drCuadroConfirm(elTexto, textosBotones)	{
	//if (window.showModalDialog)	{
   if (jQuery.browser.msie){
		if (textosBotones==null)
			textosBotones=["Aceptar","Cancelar"]
		var objDatos={	
			texto:elTexto
			,titulo:"Desktop Dialog Box"
			,botones:[
				{texto:textosBotones[0], retorna:1, iconoN:"img/drIcoAceptarN.gif", iconoS:"img/drIcoAceptarS.gif"}
				,{texto:textosBotones[1], retorna:0, iconoN:"img/drIcoCancelarN.gif", iconoS:"img/drIcoCancelarS.gif"}
				]
			}
		return abreVentanaModalDesktop(objDatos)
		}
	else
		return confirm(elTexto)
	}	

function abreVentanaModalDesktop(objDatos)	{ //SOLO IE
	//return drWindow.getDesktopWindow().showModalDialog("DrDesktopModal.html",objDatos, "dialogWidth:380px;dialogHeight:190px;center:1;resizable:1;scroll:0;status:0;unadorned:1" )
   objDatos.openerWindow=window
   return drWindow.getDesktopWindow().showModalDialog("DrDesktopModal.html",objDatos, "dialogWidth:370px;dialogHeight:164px;center:1;resizable:1;scroll:0;status:0;unadorned:1" )
}

function drTraza(msg, mostrar)   {
   if (mostrar)   {
      alert(window.trazas.join("\n"))
      window.clipboardData.setData('Text'," -------DRTRAZAS:----------\n"+window.trazas.join("\n - "));
      window.trazas=[]
      window.timeout_trazas=null
      return
      }
   if (window.trazas==null)
      window.trazas=[]
   if (window.trazas.length>0 && window.trazas[window.trazas.length-1][1]==msg)
      window.trazas[window.trazas.length-1][0]++
   else
      window.trazas[window.trazas.length]=[1,msg]
   if (window.timeout_trazas!=null)
      clearTimeout(window.timeout_trazas)
   window.timeout_trazas=setTimeout("drTraza(null,true)",1500)
   }


function parseIntSecure(string)  {
   var ret=parseInt(string)
   if (isNaN(ret))
      return 0
   else
      return ret
   }

function isString(a)  {     return typeof a == 'string'; }

function isArray(a)  {
   return isObject(a) && a.constructor.toString() == Array.toString();   //Esto es para que no falle si los arrays estan en distintas ventanas
}

// ------------------- ALIAS mantenidos por compatibilidad con anteriores versiones. Tambi�n se utilizan desde alguna otra librer�a js.

window.drDesktopWin=drWindow.getDesktopWindow()
	
function drModificaTituloVentana	(idVentana, titulo)	{
   drWindow.setTitle({title:titulo,id:idVentana});
	}

function drCierraVentana(id)	{
   drWindow.close({"id":id});
	}

function dameIdQueNoColisione()	{ 
   return drWindow.generateId();
   }

function drDameMiId()	{
   return drWindow.getId();
	}

function DruidaDameMiIdVentana()	{
   return drWindow.getId();
	}

function drDameIdVentanas()	{
   return drWindow.getWindowIds()
	}

function DruidaVentanasId()	{
   return drWindow.getWindowIds()
	}	

function drAbreAplicacion(idAplicacion,idVentana,titulo,url,ancho,alto,posicion,testigo,elOnClose,urlAyuda,losParametros,anchoMinimo,altoMinimo,skin)	{
	/*TO-DO: parametro url*/
   return drWindow.openApplication({application:idAplicacion,id:idVentana,title:titulo,url:url,width:ancho, height:alto, position:posicion, testigo:testigo, elOnClose:elOnClose, urlAyuda:urlAyuda,parameters:losParametros,minWidth:anchoMinimo,minHeight:altoMinimo,skin:skin  })
	}

function drAbreVentana(id,titulo,url,ancho,alto,posicion,testigo,elOnClose,urlAyuda,anchoMinimo,altoMinimo,skin)	{
   return drWindow.open({id:id,title:titulo,url:url,width:ancho,height:alto,position:posicion,testigo:testigo,elOnClose:elOnClose,urlAyuda:urlAyuda,minWidth:anchoMinimo,minHeight:altoMinimo,skin:skin}  )
	}

function DruidaAbreVentana(id,titulo,url,ancho,alto,posicion,testigo,idPadre,elOnClose,urlAyuda,anchoMinimo,altoMinimo,skin) {
   return drWindow.open({id:id,title:titulo,url:url,width:ancho,height:alto,position:posicion,testigoJson:testigo,idPadre:idPadre,elOnClose:elOnClose,urlAyuda:urlAyuda,minWidth:anchoMinimo,minHeight:altoMinimo,skin:skin}  )
	}

function DruidaDameWindowVentana(id)	{
   return drWindow.getNativeWindow({"id":id})
	}
	
function DruidaDameIframeVentana(id)	{
   return drWindow.getNativeIframe({"id":id});
	}	

// ---------------- ALIAS que llaman a otros ALIAS

function DruidaRegeneraVentana(id,titulo,url,ancho,alto,posicion,testigo,idPadre,urlAyuda,anchoMinimo,altoMinimo)	{
	//Utilizada por el desktop para generar las ventanas al iniciar
	//Funci?n creada para mejorar el futuro desarrollo / mantenimiento
	DruidaAbreVentana(id,titulo,url,ancho,alto,posicion,testigo,idPadre,null,urlAyuda,anchoMinimo,altoMinimo)
	}

function drAbreAplicacionObj(oInfo){
	drAbreAplicacion(oInfo.idAplicacion,oInfo.idVentana,oInfo.titulo,oInfo.url,oInfo.ancho,oInfo.alto,oInfo.posicion,oInfo.testigo,oInfo.elOnClose,oInfo.urlAyuda,oInfo.parametros,oInfo.anchoMinimo,oInfo.altoMinimo,oInfo.skin)
	}









// -------------------------

function drWindow_public_getPublicMethods() { 
   var subCommands={
      createDaemon:{args:[
           {name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt1"}) }
          ,{name:"milliseconds", info:  drResources.get({bundle:"drWHelp",key:"drWHelp.txt2"}) }
          ,{name:"script", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt3"}) }]
         ,info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt4"}) }
      ,killDaemons:{args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt5"}) }], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt6"}) }
      ,getDesktopWindow:{args:[], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt7"}) ,  complexResult:true
         , exampleLines:['if (drWindow.getDesktopWindow()==null)','   alert("not inside desktop");']} /* complexResult:true sirve para que no se convierta a json en la consola (da problemas con atributos recursivos) */
      ,getApplicationIds:{args:[], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt8"}) , exampleLines:['alert("number of registered app: "','   + drWindow.getApplicationIds().length);']}
      ,getApplicationsConfig:{args:[], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt9"}) }
      ,getApplicationsGroupsConfig:{args:[], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt10"}) }
      ,getTopMargin:{args:[], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt11"}) }
      ,getBottomMargin:{args:[], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt12"}) }
      ,getClientWidth:{args:[], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt13"}) , exampleLines:['drWindow.setWidth({width:(drWindow.getClientWidth()-94)});']}
      ,getClientHeight:{args:[], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt14"}) }
      ,getId:{args:[], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt15"}) , exampleLines:['//close actual window:', 'drWindow.close( {id:drWindow.getId()} );']}
      ,getWindowIds:{args:[
         {name:"noWidgets",info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt16"}) }
         ,{name:"onlyCurrentDesktop",info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt17"}) }
      ], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt18"}) , exampleLines:['alert("number of windows in this desktop: "','   +drWindow.getWindowIds({onlyCurrentDesktop:true}).length);']}
      ,getWindowData:{args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt19"}) }]
         ,info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt20"}) , exampleLines:['var isSingle = drWindow.getWindowData(','   {"id": drWindow.getId()} ).single;']}
      ,getNativeWindow:{args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt21"}) }], complexResult:true
         ,info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt22"}) , exampleLines:['var win=drWindow.getNativeWindow({id:"drWin34568376"});','alert(win.document.location.href);']}
      ,getNativeIframe:{args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt21"}) }], complexResult:true
         ,info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt23"}) }
      ,getSkin:{args:[], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt24"}) }
      ,getSkins:{args:[], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt25"}) , exampleLines:['var skinsData=drWindow.getSkins();','var firstSkin=skinsData[0];','alert(firstSkin.description);','alert(firstSkin.parameters.userBackgroundImage);']}
      ,arrangeAllWindows:{args:[{name: "startX", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt26"}) }
         ,{name: "startY", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt27"}) }
         ,{name: "offsetX", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt28"}) }
         ,{name: "offsetY", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt29"}) }
      ], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt30"}) , exampleLines:['drWindow.arrangeAllWindows({startX:"10",startY:"10"', '   ,offsetX:"30",offsetY:"50"', '});']}
      ,setSkin:{args:[{name:"skin", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt31"}) }]
         ,info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt32"}) 
         , exampleLines:['drWindow.setSkin("XP");','drWindow.sessionSaveAndRestart();']
      }
      ,generateId:{args:[], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt33"}) }
      ,open:{args:[
          {name: "id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt34"}) }
         ,{name: "url", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt35"}) }
         ,{name: "title", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt36"}) }
         ,{name: "position", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt37"}) }
         ,{name: "ignoreMargins", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt38"}) }
         ,{name: "helpUrl", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt39"}) }
         ,{name: "status", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt40"}) }
         ,{name: "widget", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt41"}) }
         ,{name: "noTaskbar", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt42"}) }
         ,{name: "sessionSave", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt43"}) }
         ,{name: "virtualDesktop", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt44"}) }
         ,{name: "showInAllDesktops", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt45"}) }
         ,{name: "width", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt46"}) }
         ,{name: "height", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt47"}) }
         ,{name: "innerWidth", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt48"}) }
         ,{name: "innerHeight", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt49"}) }
         ,{name: "noResize", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt50"}) }
         ,{name: "noDrag", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt51"}) }
         ,{name: "parameters", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt52"}) }
         ]
         , info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt53"}) 
         , exampleLines:['var newWindowId=drWindow.open({','   url:"http://www.google.com", title:"google search"','   ,innerHeight:"300", innerWidth:"600", position:"center,center"','})']}
      ,openApplication:{args:[
          {name: "application", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt54"}) }
         ,{name: "id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt55"}) }
         ,{name: "title", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt56"}) }
         ,{name: "position", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt57"}) }
         ,{name: "ignoreMargins", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt58"}) }
         ,{name: "helpUrl", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt59"}) }
         ,{name: "relativeHelpUrl", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt60"}) }
         ,{name: "status", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt61"}) }
         ,{name: "widget", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt62"}) }
         ,{name: "noTaskbar", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt63"}) }
         ,{name: "single", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt64"}) }
         ,{name: "sessionSave", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt65"}) }
         ,{name: "virtualDesktop", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt66"}) }
         ,{name: "showInAllDesktops", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt67"}) }
         ,{name: "width", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt68"}) }
         ,{name: "height", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt69"}) }
         ,{name: "innerWidth", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt70"}) }
         ,{name: "innerHeight", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt71"}) }
         ,{name: "noResize", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt72"}) }
         ,{name: "noDrag", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt73"}) }
         ,{name: "parameters", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt74"}) }
         ]
         ,info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt75"}) 
         ,exampleLines:['// Suppose that there is a file WEB-INF/applications/someApp.xml','// with a set of pre-defined values for a "drCalculator" application','var newWindowId=drWindow.open({application:"drCalculator"});']}
      ,reload:{args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt77"}) }
      ,close:{args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt78"}) 
         ,exampleLines:['//Closes the window executing this code:','drWindow.close()']}
      ,setTitle:{args:[{name: "title", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt79"}) },{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]
         , info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt80"}) , exampleLines:['drWindow.setTitle({title:"User details: "+userName})']}
      ,setTop:{args:[{name: "top", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt81"}) },{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]
         , info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt82"}) , exampleLines:['drWindow.setTop({top:30})']}
      ,setLeft:{args:[{name: "left", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt83"}) },{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]
         , info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt84"}) , exampleLines:['drWindow.setLeft({id:"drWin29013384", left:30})']}
      ,setWidth:{args:[{name: "width", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt85"}) },{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]
         , info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt86"}) , exampleLines:['drWindow.setWidth({width:600})']}
      ,setHeight:{args:[{name: "height", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt87"}) },{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]
         , info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt88"}) , exampleLines:['drWindow.setHeight({id:"drWin29013384", height:400})']}
      ,setStatus:{args:[{name: "status", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt89"}) },{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]
         , info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt90"}) , exampleLines:['drWindow.setStatus({status:"minimized"})','drWindow.setStatus({id:"drWin29013384", status:"maximized"})']}
      ,showMessage:{args:[{name: "message", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt91"}) }], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt92"}) 
         , exampleLines:['drWindow.showMessage({message:"Operation exceeded time."})']}
      ,showConfirm:{args:[{name: "message", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt91"}) }], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt94"}) 
         , exampleLines:['if (drWindow.showConfirm({message:"You confirm the delete operation?"}))','   alert("deleting ...")']}
      ,getTop:{args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt95"}) 
         , exampleLines:['drWindow.setTop({top: drWindow.getTop()+25 })']}
      ,getLeft:{args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt96"}) 
         , exampleLines:['alert(drWindow.getLeft({id:"drWin29013384"}))']}
      ,getStatus:{args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]
         , info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt97"}) , exampleLines:['//De-minimize a window', 'if (drWindow.getStatus()=="minimized")','   drWindow.setStatus({status: null});']}
      ,resizeTo:{args:[{name: "width", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt98"}) },{name: "height", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt99"}) },{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]
         , info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt100"}) , exampleLines:['drWindow.resizeTo({width:400, height:300})']}
      ,moveTo:{args:[{name: "left", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt101"}) },{name: "top", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt102"}) },{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]
         , info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt103"}) , exampleLines:['drWindow.resizeTo({top:100, left:100})']}
      ,moveResizeTo:{args:[{name: "width", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt104"}) },{name: "height", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt105"}) },{name: "left", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt106"}) },{name: "top", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt107"}) },{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]
         , info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt108"}) }
      ,sessionSaveAndRestart:{args:[], info:  drResources.get({bundle:"drWHelp",key:"drWHelp.txt109"}) }
      ,sessionRestart:{args:[], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt110"}) , exampleLines:['drWindow.setSkin("XP");','drWindow.sessionSaveAndRestart();']}
      ,toFront:{args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt111"}) }
      ,toBack:{args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt112"}) }
      ,bindDesktopResize:{args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }
         ,{name:"handlerName", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt113"}) }],info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt114"}) 
         , exampleLines:['//Resizes this widget to fit width ','//every time the desktop resizes:','','drWindow.bindDesktopResize({handlerName:"onDesktopResize"});','onDesktopResize();','','function onDesktopResize()  {','   drWindow.setWidth({"width":(drWindow.getClientWidth()-325)});','}']}
      ,getUserData:{args:[
         {name:"cache",info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt115"}) }
         ,{name:"async",info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt116"}) }
         ,{name:"callback",info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt117"}) }
         ] ,info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt118"}) ,exampleLines:['//syncronous example (it blocks the execution until data is received)','userData=drWindow.getUserData();','alert(userData.id+" :"+userData.name+" "+userData.surname);','','//asyncronous example','var handler=function(userData){','   alert(userData.id+" :"+userData.name+" "+userData.surname);','}','drWindow.getUserData({async:true, callback:handler});']}
      ,getCookie:{args:[{name:"name",info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt119"}) }],info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt120"}) }
      ,deleteCookie:{args:[{name:"name",info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt121"}) }],info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt122"}) }
      ,setCookie:{args:[
            {name:"name",info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt123"}) }
           ,{name:"value",info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt124"}) }
           ,{name:"expires",info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt125"}) }
           ,{name:"path",info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt126"}) }
           ,{name:"domain",info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt127"}) }
           ,{name:"secure",info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt128"}) }
         ],info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt129"}) }
      ,changeVirtualDesktop:{info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt132"}) 
         ,args:[{name:"virtualDesktop", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt133"}) }
         ], exampleLines:['//Change to another virtual desktop:','drWindow.changeVirtualDesktop({virtualDesktop:"secondDesktop"})','//Return to default desktop','drWindow.changeVirtualDesktop()']}
      ,getCurrentVirtualDesktop:{info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt134"}) ,args:[]}
      ,setVirtualDesktop:{info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt135"}) ,args:[
         {name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }
         ,{name:"virtualDesktop", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt136"}) }]}
      ,getVirtualDesktop:{info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt137"}) ,args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]}
      ,getVirtualDesktopsData:{info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt138"}) , args:[]
         , exampleLines:['var data=drWindow.getVirtualDesktopsData();','alert("there are "+data.length+"different desktops now");']}
      ,getHeight:{args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt139"}) }
      ,getWidth:{args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt140"}) }
      ,getInnerWidth:{info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt141"}) , args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]}
      ,getInnerHeight:{info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt142"}) , args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]}
      ,setNoDrag:{info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt143"}) , args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }
         ,{name:"noDrag",info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt144"}) }]}
      ,getNoDrag:{info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt145"}) , args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]}
      ,setNoResize:{info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt146"}) , args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }
         ,{name:"noResize",info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt147"}) }]}
      ,getNoResize:{info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt148"}) , args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]}
      ,setGlobalNoDrag: {info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt149"}) , args:[{name:"globalNoDrag", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt150"}) }]}
      ,getGlobalNoDrag: {info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt151"}) , args:[]}
      ,setGlobalNoResize: {info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt152"}) , args:[{name:"globalNoResize", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt153"}) }]}
      ,getGlobalNoResize: {info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt154"}) , args:[]}
      ,setGlobalNoClose: {info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt155"}) , args:[{name:"globalNoClose", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt156"}) }]}
      ,getGlobalNoClose: {info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt157"}) , args:[]}
      ,setGlobalNoContextMenu: {info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt158"}) , args:[{name:"globalNoContextMenu", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt159"}) }]}
      ,getGlobalNoContextMenu: {info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt160"}) , args:[]}
      ,focus: {info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt161"}) , args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }
      ],exampleLines:['var newWindowId=drWindow.open({url:"http://www.google.com"});','drWindow.focus({id:newWindowId});']}
      ,blur:{info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt162"}) , args:[]}
      ,hasFocus: {info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt163"}) ,args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt76"}) }]
         ,exampleLines:['if (!drWindow.hasFocus())','   drWindow.focus();']}
      ,getWindowExist: {info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt164"}) ,args:[{name:"id", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt165"}) }]}
      ,setStatusAllWindows:{info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt166"}) ,args:[{name:"status", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt167"}) },{name:"onlyCurrentDesktop", info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt168"}) }]}
      ,sessionSaveAndClose:{args:[], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt169"}) , exampleLines:['drWindow.setSkin("XP");','drWindow.sessionSaveAndRestart();']}
      ,sessionClose:{args:[], info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt170"}) , exampleLines:['drWindow.setSkin("XP");','drWindow.sessionSaveAndRestart();']}
      }
   var publicMethods={
      info: drResources.get({bundle:"drWHelp",key:"drWHelp.txt171"}) 
      ,subCommands:subCommands
      }
   return publicMethods

   }



/*

function drWindow_public_getPublicMethods() { 
   var subCommands={
      createDaemon:{args:[
           {name:"id", info:"Window id"}
          ,{name:"milliseconds", info: "Milliseconds between consecutive calls"}
          ,{name:"script", info:"JavaScript method to invoke [optional]. If there is no method to invoke createDaemon will reload window content in every call."}]
         ,info:"This method creates an automatic process (daemon) which updates (reload) the contents of a window or run a JavaScript function inside that window every certain time interval. See 'killDaemons'."}
      ,killDaemons:{args:[{name:"id", info:"Window id. Default value: the id of the window executing this method."}], info:"destroys the automatic processes (daemons) associated with a certain window. See 'createDaemon'."}
      ,getDesktopWindow:{args:[], info:"Returns the native window in which the desktop resides or null if invoked from outside the desktop.",  complexResult:true
         , exampleLines:['if (drWindow.getDesktopWindow()==null)','   alert("not inside desktop");']} 
      //,getStructuralWindow:{args:[], complexResult:true, info:"Retorna el objeto window dentro de cuyo frameset reside el objeto window del escritorio."}
      //,getXDesktopObj:{args:[], complexResult:true, info:"Retorna el objeto xDT de xDesktop."}
      //,getXDesktopWindowObj:{args:[], complexResult:true, info:"Retorna el objeto xDTwin de xDesktop."}
      //,getDesktopConfig:{args:[], info:"Retorna el objeto de configuraci�n del desktop."}
      //,getEnginesConfig:{args:[], info:"Retorna el objeto de configuraci�n de los engines."}
      ,getApplicationIds:{args:[], info:"Returns an array of strings containing the id of the registered applications.", exampleLines:['alert("number of registered app: "','   + drWindow.getApplicationIds().length);']}
      ,getApplicationsConfig:{args:[], info:"Returns an oject containing the data of the registered applications."}
      ,getApplicationsGroupsConfig:{args:[], info:"Returns an oject containing the data of the registered application groups."}
      ,getTopMargin:{args:[], info:"Returns the top margin of the desktop in pixels (it deppends of the current skin)."}
      ,getBottomMargin:{args:[], info:"Returns the bottom margin of the desktop in pixels (it deppends of the current skin)."}
      ,getClientWidth:{args:[], info:"It returns the actual desktop width in pixels.", exampleLines:['drWindow.setWidth({width:(drWindow.getClientWidth()-94)});']}
      ,getClientHeight:{args:[], info:"It returns the actual desktop useful height in pixels (real height - bottom margin - top margin)."}
      ,getId:{args:[], info:"If it is called from an html inside a desktop window it returns the id of that window.", exampleLines:['//close actual window:', 'drWindow.close( {id:drWindow.getId()} );']}
      ,getWindowIds:{args:[
         {name:"noWidgets",info:"If it is true returns only the ids of no-widget windows."}
         ,{name:"onlyCurrentDesktop",info:"If it is true returns only the ids of windows opened in current virtual desktop."}
      ], info:"Returns an array with all the ids of every opened window in the desktop.", exampleLines:['alert("number of windows in this desktop: "','   +drWindow.getWindowIds({onlyCurrentDesktop:true}).length);']}
      ,getWindowData:{args:[{name:"id", info:"[optional] Window id. If not specified returns an array with the data of all opened windows."}]
         ,info:"Returns information about opened windows.", exampleLines:['var isSingle = drWindow.getWindowData(','   {"id": drWindow.getId()} ).single;']}
      ,getNativeWindow:{args:[{name:"id", info:"[optional] Window Identifier (String). Default value: id of the window the method is executed from."}], complexResult:true
         ,info:"Returns the native window object inside a desktop window.", exampleLines:['var win=drWindow.getNativeWindow({id:"drWin34568376"});','alert(win.document.location.href);']}
      ,getNativeIframe:{args:[{name:"id", info:"[optional] Window Identifier (String). Default value: id of the window the method is executed from."}], complexResult:true
         ,info:"Returns the native structural iframe of a window."}
      //,getRegistry:{args:[],info:"Retorna el objeto interno de control de las ventanas."}
      //,getDaemonRegistry:{args:[], info:"Retorna el objeto registro de los procesos Daemon existentes."}
      ,getSkin:{args:[], info:"Returns the identifier of the actual skin. See setSkin() and getSkins()."}
      ,getSkins:{args:[], info:"Returns an object with the registered skins and its configuration parameters. See setSkin() and getSkin().", exampleLines:['var skinsData=drWindow.getSkins();','var firstSkin=skinsData[0];','alert(firstSkin.description);','alert(firstSkin.parameters.userBackgroundImage);']}
      ,arrangeAllWindows:{args:[{name: "startX", info:"Starting left position in pixels."}
         ,{name: "startY", info:"Starting top position in pixels."}
         ,{name: "offsetX", info:"Step in horizontal position in pixels."}
         ,{name: "offsetY", info:"Step in vertical position in pixels."}
      ], info:"This method repositions all windows in a cascade stack.", exampleLines:['drWindow.arrangeAllWindows({startX:"10",startY:"10"', '   ,offsetX:"30",offsetY:"50"', '});']}
      ,setSkin:{args:[{name:"skin", info:'Skin identifier (string). For instance: "XP", "OSXA", "NPS", "EST", etc.'}]
         ,info:"Sets the skin. For the change to be effective drWindow.sessionSaveAndRestart() method must be called. See getSkin() and getSkins()."
         , exampleLines:['drWindow.setSkin("XP");','drWindow.sessionSaveAndRestart();']
      }
      ,generateId:{args:[], info:"Generates a random window identifier which does not yet exist for any window. Is usually not necessary to call this method directly because the method drWindow.open() generates a random id for the new window if you do not use any explicit id."}
      ,open:{args:[
          {name: "id", info:"[optional] window identifier (string). If we call the open method multiple times using the same id then the window will be reused. If id argument is not specified then a new window will be opened and a random id generated. The open method returns the id of the new window."}
         ,{name: "url", info:"URL of the html content of the window."}
         ,{name: "title", info:"[optional] Title of the window."}
         ,{name: "position", info:"[optional] String describing the position of the window. Can be 'x,y','center','center,y','x,center','n','s','e','w','ne','nw','se','sw', 'windowId:T|B|L|R:Offset' (in case you want the position relative to another existing window), 'rXX,YY' (eg 'r10,5' places it 10 pixeles to right margin and 5 pixels to top margin), 'XX,bYY' (eg 5,b10 places it 5 pixels from left and 10 pixels from bottom) or 'rXX,bYY'"}
         ,{name: "ignoreMargins", info:"[optional] If it is true then the position parameter ignores skin top and bottom margins."}
         //,{name: "testigo", info:"[optional] objeto javascript a pasar al abrir la ventana a la funci�n de recogida de testigo existente en el HTML contenido en la ventana si dicha funci�n existe. El paso de testigo es una funcionalidad utilizada para recuperar el estado de las ventanas automaticamente al volver a iniciar la sesi�n de un usuario. Si al cerrar una ventana existe una cierta funci�n javascript de paso de testigo entonces la funci�n es llamada y el testigo almacenado para cuando se recupere la sesi�n. El paso de testigo se explica en otro apartado de la documentaci�n."}
         //,{name: "testigoJson", info:"[optional] Igual que el par�metro 'testigo' salvo porque este par�metro no es un objeto cualquiera javascript si no una cadena de caracteres que debe ser evaluada cono objeto json antes de pasarselo a la funci�n de recogida de testigo. "}
         ,{name: "helpUrl", info:"[optional] URL to open when you click on the help button of the window."}
         //,{name: "idPadre", info:"[optional] Id de la ventana que se va  aconsiderar padre de esta (opcional)."}
         ,{name: "status", info:"[optional] Initial status. Could be 'maximized' or 'minimized'"}
         ,{name: "widget", info:"[optional] If it is true then the window is a widget (don't have chrome, background could be transparent and the window doesn't appear In the taskbar). If transparency is needed the css background color of the html loaded in the window must be set to 'transparent'."}
         ,{name: "noTaskbar", info:"[optional] If it is true then the window doesn't appear In the taskbar."}
         ,{name: "sessionSave", info:"[optional] If it is false then the window will not be saved when desktop sesion is saved."}
         //,{name: "noHide", info:"[optional] Si es 'true' entonces el contenido de la ventana no se oculta cuando se mueve o se cambia de tama�o otra ventana."}
         ,{name: "virtualDesktop", info:"[optional] Identifier of the virtual desktop this window belongs to (string). Default value: active virtual desktop."}
         ,{name: "showInAllDesktops", info:"[optional] If it is true then the window will be seen in all virtual desktops."}
         ,{name: "width", info:"[optional] Width in pixels of the window including chrome. If innerWidth is specified this argument is ignored."}
         ,{name: "height", info:"[optional] Height in pixels of the window including chrome. If innerHeight is specified this argument is ignored."}
         ,{name: "innerWidth", info:"[optional] Width in pixels of the window inner content without chrome."}
         ,{name: "innerHeight", info:"[optional] Height in pixels of the window inner content without chrome."}
         ,{name: "noResize", info:"[optional] If it is true the user can't change window's width or height by dragging its bottom-right corner. However the window can be maximized or minimized."}
         ,{name: "noDrag", info:"[optional]  If it is true the user can't change window's position by dragging it."}
         ,{name: "parameters", info:'[optional] parameters to be added to the url (get method). It is an array of javascript objects. Every object must have a "name" and a "value" property. eg: [{name:"param1",value:"value1"},{name:"param2",value:"value2"}].'}
         ]
         , info:"Opens an url in a new window or reloads an existing one. The 'url' parameter is needed, the rest are optional. It returns the identifier of the window."
         , exampleLines:['var newWindowId=drWindow.open({','   url:"http://www.google.com", title:"google search"','   ,innerHeight:"300", innerWidth:"600", position:"center,center"','})']}
      ,openApplication:{args:[
          {name: "application", info:"Application identifier."}
         ,{name: "id", info:"[optional] Window identifier (string). If we call the open method multiple times using the same id then the window will be reused. If id argument is not specified then a new window will be opened and a random id generated. The open method returns the id of the new window."}
         ,{name: "title", info:"[optional] Title of the window."}
         ,{name: "position", info:"[optional] String describing the position of the window. Can be 'x,y','center','center,y','x,center','n','s','e','w','ne','nw','se','sw', 'windowId:T|B|L|R:Offset' (in case you want the position relative to another existing window), 'rXX,YY' (eg 'r10,5' places it 10 pixeles to right margin and 5 pixels to top margin), 'XX,bYY' (eg 5,b10 places it 5 pixels from left and 10 pixels from bottom) or 'rXX,bYY'"}
         ,{name: "ignoreMargins", info:"[optional] If it is true then the position parameter ignores skin top and bottom margins."}
         //,{name: "testigo", info:"objeto javascript a pasar al abrir la ventana a la funci�n de recogida de testigo existente en el HTML contenido en la ventana si dicha funci�n existe. El paso de testigo es una funcionalidad utilizada para recuperar el estado de las ventanas automaticamente al volver a iniciar la sesi�n de un usuario. Si al cerrar una ventana existe una cierta funci�n javascript de paso de testigo entonces la funci�n es llamada y el testigo almacenado para cuando se recupere la sesi�n. El paso de testigo se explica en otro apartado de la documentaci�n."}
         //,{name: "testigoJson", info:"Igual que el par�metro 'testigo' salvo porque este par�metro no es un objeto cualquiera javascript si no una cadena de caracteres que debe ser evaluada cono objeto json antes de pasarselo a la funci�n de recogida de testigo. "}
         ,{name: "helpUrl", info:"[optional] URL to open when you click on the help button of the window."}
         ,{name: "relativeHelpUrl", info:"[optional] URL to open when you click on the help button of the window. This URL is relative to the application group help directory."}
         //,{name: "idPadre", info:"Id de la ventana que se va  aconsiderar padre de esta (opcional)."}
         ,{name: "status", info:"[optional] Initial status. Could be 'maximized' or 'minimized'"}
         ,{name: "widget", info:"[optional] If it is true then the window is a widget (don't have chrome, background could be transparent and the window doesn't appear In the taskbar). If transparency is needed the css background color of the html loaded in the window must be set to 'transparent'."}
         ,{name: "noTaskbar", info:"[optional] If it is true then the window doesn't appear In the taskbar."}
         ,{name: "single", info:"If it is true then a unique id for the application is created and consecutive calls to openApplication() with the same application identifier will reload the window instead of opening a new one."}
         ,{name: "sessionSave", info:"[optional] If it is false then the window will not be saved when desktop sesion is saved."}
         //,{name: "noHide", info:"Si es 'true' entonces el contenido de la ventana no se oculta cuando se mueve o se cambia de tama�o otra ventana."}
         ,{name: "virtualDesktop", info:"[optional] Identifier of the virtual desktop this window belongs to (string). Default value: active virtual desktop."}
         ,{name: "showInAllDesktops", info:"[optional] If it is true then the window will be seen in all virtual desktops."}
         ,{name: "width", info:"[optional] Width in pixels of the window including chrome. If innerWidth is specified this argument is ignored."}
         ,{name: "height", info:"[optional] Height in pixels of the window including chrome. If innerHeight is specified this argument is ignored."}
         ,{name: "innerWidth", info:"[optional] Width in pixels of the window inner content without chrome."}
         ,{name: "innerHeight", info:"[optional] Height in pixels of the window inner content without chrome."}
         ,{name: "noResize", info:"[optional] If it is true the user can't change window's width or height by dragging its bottom-right corner. However the window can be maximized or minimized."}
         ,{name: "noDrag", info:"[optional]  If it is true the user can't change window's position by dragging it."}
         ,{name: "parameters", info:'[optional] parameters to be added to the url (get method). It is an array of javascript objects. Every object must have a "name" and a "value" property. eg: [{name:"param1",value:"value1"},{name:"param2",value:"value2"}].'}
         ]
         ,info:"Opens a window using a set of parameters previously defined as an 'application' in some desktop application descriptor file (xml files under %appDefRoot% desktop directory). 'application' parameter is not optional. All arguments used in drWindow.openApplication() will overwrite correspondign application parameters. It returns the identifier of the window."
         ,exampleLines:['// Suppose that there is a file WEB-INF/applications/someApp.xml','// with a set of pre-defined values for a "drCalculator" application','var newWindowId=drWindow.open({application:"drCalculator"});']}
      ,reload:{args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}], info:"Reloads the content of a desktop window."}
      ,close:{args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}], info:"It closes the window."
         ,exampleLines:['//Closes the window executing this code:','drWindow.close()']}
      ,setTitle:{args:[{name: "title", info:"New title for the window."},{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]
         , info:"Sets the title of a window.", exampleLines:['drWindow.setTitle({title:"User details: "+userName})']}
      ,setTop:{args:[{name: "top", info:"Top coordinate in pixels."},{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]
         , info:"Sets the top coordinate of a window.", exampleLines:['drWindow.setTop({top:30})']}
      ,setLeft:{args:[{name: "left", info:"Left coordinate in pixels."},{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]
         , info:"Sets the left coordinate of a window.", exampleLines:['drWindow.setLeft({id:"drWin29013384", left:30})']}
      ,setWidth:{args:[{name: "width", info:"Width in pixels."},{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]
         , info:"Sets the width of a window.", exampleLines:['drWindow.setWidth({width:600})']}
      ,setHeight:{args:[{name: "height", info:"Height in pixels."},{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]
         , info:"Sets the height of a window.", exampleLines:['drWindow.setHeight({id:"drWin29013384", height:400})']}
      ,setStatus:{args:[{name: "status", info:"Can be null (default), 'maximized' o 'minimized'"},{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]
         , info:"Sets the status of a window (maximized/minimized).", exampleLines:['drWindow.setStatus({status:"minimized"})','drWindow.setStatus({id:"drWin29013384", status:"maximized"})']}
      ,showMessage:{args:[{name: "message", info:"Message to show."}], info:"It shows a modal desktop message dialog window."
         , exampleLines:['drWindow.showMessage({message:"Operation exceeded time."})']}
      ,showConfirm:{args:[{name: "message", info:"Message to show."}], info:"It shows a modal desktop confirm dialog window and it returns true or false."
         , exampleLines:['if (drWindow.showConfirm({message:"You confirm the delete operation?"}))','   alert("deleting ...")']}
      ,getTop:{args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}], info:"Returns window top position in pixels."
         , exampleLines:['drWindow.setTop({top: drWindow.getTop()+25 })']}
      ,getLeft:{args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}], info:"Returns window left position in pixels."
         , exampleLines:['alert(drWindow.getLeft({id:"drWin29013384"}))']}
      ,getStatus:{args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]
         , info:'Returns window status("maximized", "minimized", or null if no maximized or minimized).', exampleLines:['//De-minimize a window', 'if (drWindow.getStatus()=="minimized")','   drWindow.setStatus({status: null});']}
      ,resizeTo:{args:[{name: "width", info:"Width in pixels."},{name: "height", info:"Height in pixels."},{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]
         , info:"Sets the width and height of a window.", exampleLines:['drWindow.resizeTo({width:400, height:300})']}
      ,moveTo:{args:[{name: "left", info:"Left coordinate in pixels."},{name: "top", info:"Top coordinate in pixels."},{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]
         , info:"Sets the top and left position of a window.", exampleLines:['drWindow.resizeTo({top:100, left:100})']}
      ,moveResizeTo:{args:[{name: "width", info:"Width in pixels."},{name: "height", info:"Height in pixels."},{name: "left", info:"Left coordinate in pixels."},{name: "top", info:"Top coordinate in pixels."},{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]
         , info:"Sets the width, height, top position and left position of a window."}
      ,sessionSaveAndRestart:{args:[], info: "Saves desktop session (opened windows, icons and configuration) and restarts."}
      ,sessionRestart:{args:[], info:"Restart the desktop without saving changes (changes will be lost).", exampleLines:['drWindow.setSkin("XP");','drWindow.sessionSaveAndRestart();']}
      ,toFront:{args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}], info:"Brings a window to the front of all windows."}
      ,toBack:{args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}], info:"Put this window to the back of all windows."}
      ,bindDesktopResize:{args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}
         ,{name:"handlerName", info:"Name of the javascript function to execute.  The function must belong to the native window object inside the desktop window refered by 'id' parameter."}],info:"Assign a javascript function to be called every time the desktop resizes."
         , exampleLines:['//Resizes this widget to fit width ','//every time the desktop resizes:','','drWindow.bindDesktopResize({handlerName:"onDesktopResize"});','onDesktopResize();','','function onDesktopResize()  {','   drWindow.setWidth({"width":(drWindow.getClientWidth()-325)});','}']}
      ,getUserData:{args:[
         {name:"cache",info:"[optional] If it is true then data is stored in a cache in the first call and successive calls will return the same data."}
         ,{name:"async",info:"[optional] If it is true (and cache paremeter is not true) then the data are retrieved from server asynchronously. If async is true callback parameter must be used. Default value: false (if async is false execution is blocked untill data is received)."}
         ,{name:"callback",info:"[optional] If cache parameter is not true and async is true then this parameter is a javascript function (Function javascript type) to be called with user data as parameter."}
         ] ,info:"It calls server and returns the user data.",exampleLines:['//syncronous example (it blocks the execution until data is received)','userData=drWindow.getUserData();','alert(userData.id+" :"+userData.name+" "+userData.surname);','','//asyncronous example','var handler=function(userData){','   alert(userData.id+" :"+userData.name+" "+userData.surname);','}','drWindow.getUserData({async:true, callback:handler});']}
      ,getCookie:{args:[{name:"name",info:"The name of the cookie"}],info:"Returns cookie value."}
      ,deleteCookie:{args:[{name:"name",info:"The name of the cookie"}],info:"Removes a cookie."}
      ,setCookie:{args:[
            {name:"name",info:"The name of the cookie"}
           ,{name:"value",info:"Value of the cookie"}
           ,{name:"expires",info:"Either an integer specifying the expiration date from now on in days or a Date object. If set to null or omitted, the cookie will be a session cookie. If a negative value is specified the cookie will be deleted."}
           ,{name:"path",info:"The value of the path atribute of the cookie (default: path of page that created the cookie)."}
           ,{name:"domain",info:"The value of the domain attribute of the cookie (default: domain of page that created the cookie)."}
           ,{name:"secure",info:"If true, the secure attribute of the cookie will be set and the cookie transmission will require a secure protocol (like HTTPS)."}
         ],info:"Creates a cookie or sets its value."}
      //,hide:{info:"Oculta una ventana.",args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]}
      //,show:{info:"Muestra una ventana que estaba oculta.",args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]}
      ,changeVirtualDesktop:{info:"Changes active virtual desktop. Initial virtual desktop is always '0'. There are infinite virtual desktops, one per identifier."
         ,args:[{name:"virtualDesktop", info:"[optional] Virtual desktop identifier (String). Default value: '0'."}
         ], exampleLines:['//Change to another virtual desktop:','drWindow.changeVirtualDesktop({virtualDesktop:"secondDesktop"})','//Return to default desktop','drWindow.changeVirtualDesktop()']}
      ,getCurrentVirtualDesktop:{info:"Returns the identifier of the current active virtual desktop.",args:[]}
      ,setVirtualDesktop:{info:"Sets the virtual desktop a window belongs to. The window will only be seen in the appropriate virtual desktop",args:[
         {name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}
         ,{name:"virtualDesktop", info:"Virtual desktop identifier (String)."}]}
      ,getVirtualDesktop:{info:"Returns the identifier of the virtual desktop a window belongs to.",args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]}
      ,getVirtualDesktopsData:{info:"Returns an array with an object for each existing virtual dektop with windows. Every object has two properties: id (virtual desktop identifier) and windowIds (an array of window identifiers).", args:[]
         , exampleLines:['var data=drWindow.getVirtualDesktopsData();','alert("there are "+data.length+"different desktops now");']}
      ,getHeight:{args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}], info:"Returns window height in pixels including chrome."}
      ,getWidth:{args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}], info:"Returns window width in pixels including chrome."}
      ,getInnerWidth:{info:"Returns window width in pixels not including chrome.", args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]}
      ,getInnerHeight:{info:"Returns window height in pixels not including chrome.", args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]}
      ,setNoDrag:{info:"Sets the no-drag status for a window.", args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}
         ,{name:"noDrag",info:"If it is true then the window can't change its position."}]}
      ,getNoDrag:{info:"Get no-drag status of a window.", args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]}
      ,setNoResize:{info:"Sets the no-resize status for a window.", args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}
         ,{name:"noResize",info:"If it is true then the user can't change window's width or height by dragging its bottom-right corner."}]}
      ,getNoResize:{info:"Get no-resize status of a window.", args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]}
      //,showHelp:{info:"", args:[{name:"windowId", info:""},{name:"locale", info:""},{name:"url", info:""}]}
      ,setGlobalNoDrag: {info:"Sets the global no-drag status for all windows.", args:[{name:"globalNoDrag", info:"If it is true then no window can change its position."}]}
      ,getGlobalNoDrag: {info:"Get the global no-drag status.", args:[]}
      ,setGlobalNoResize: {info:"Sets the global no-resize status for all windows.", args:[{name:"globalNoResize", info:"If it is true then the user can't change any window's width or height by dragging its bottom-right corner."}]}
      ,getGlobalNoResize: {info:"Get the global no-resize status.", args:[]}
      ,setGlobalNoClose: {info:"Sets the global no-close status for all windows.", args:[{name:"globalNoClose", info:"If it is true then no window can be closed."}]}
      ,getGlobalNoClose: {info:"Get the global no-close status.", args:[]}
      ,setGlobalNoContextMenu: {info:"Sets the global no-context-menu status for the desktop", args:[{name:"globalNoContextMenu", info:"If it is true then no context menu will appear in the desktop when right-clicking on the background or in the icons."}]}
      ,getGlobalNoContextMenu: {info:"Get the global no-context-menu status.", args:[]}
      //,getDesktopProperties: {info:"", args:[]}
      //,getSkinParameters: {info:"", args:[{name:"skin", info:""}]}
      ,focus: {info:"Gives the focus to a window. See blur and hasFocus methods.", args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}
         //,{name:"noZIndexChange", info:""}
         //,{name:"zIndex", info:""}
      ],exampleLines:['var newWindowId=drWindow.open({url:"http://www.google.com"});','drWindow.focus({id:newWindowId});']}
      ,blur:{info:"Removes the focus from the window that currently has it. see focus and hasFocus methods.", args:[]}
      ,hasFocus: {info:"Returns true if a window has the focus.",args:[{name:"id", info:"[optional, String] Window identifier. Default value: identifier of the window this method is executed from."}]
         ,exampleLines:['if (!drWindow.hasFocus())','   drWindow.focus();']}
      ,getWindowExist: {info:"Returns true if a window exist, otherwise returns false.",args:[{name:"id", info:"Window identifier (String)."}]}

      //,getSkinUrl: {info:"",args:[{name:"skin", info:""}]}
      //,getMenuObject:{info:"",args:[]}

      ,setStatusAllWindows:{info:"Sets the status of all windows ('maximized', 'minimized' or null for default behaviour).",args:[{name:"status", info:"Can be null (default), 'maximized' o 'minimized'"},{name:"onlyCurrentDesktop", info:"[optional] If it is true only the windows in this virtual desktop will change its status. Default valur: current virtual desktop."}]}

      ,sessionSaveAndClose:{args:[], info:"Saves the changes, closes actual session and restart the desktop.", exampleLines:['drWindow.setSkin("XP");','drWindow.sessionSaveAndRestart();']}
      ,sessionClose:{args:[], info:"Closes actual session and restart the desktop without saving changes (changes will be lost).", exampleLines:['drWindow.setSkin("XP");','drWindow.sessionSaveAndRestart();']}
      }
   var publicMethods={
      info:"drWindow object has methods to programatically manage windows, daemons, virtual desktops, skins, cookies and user session."
      ,subCommands:subCommands
      }
   return publicMethods

   }

   */