


oSkinFactory=   {

   /* --------- */
   getHtmlDefaultWidget:function (wName,wParams)   {
      html=[]
      html[html.length]="   <div id=\"" + wName + "iTD\" style=\"position:absolute; width:100%; height:100%; left: 0; top: 0; overflow:hidden\" >";
      html[html.length]="      <iframe width=\"100%\" height=\"100%\" src=\"widgetExample.html\" allowtransparency=\"true\" marginwidth=\"0\" marginheight=\"0\" frameborder=\"0\" scrolling=\"NO\"></iframe>";
      html[html.length]="   </div>";
      return html.join('');
   }

   /* --------- */
   ,initSkin:function(oSkin)   {
      drDesktop.addSkin({skin:oSkin.id, topMargin:oSkin.marginTop, bottomMargin:oSkin.marginBottom})
      var $global=$(window)
      drDesktop.get$Win({id:"dDesktop"}).rszTo({width:$global.width(), height:$global.height()})
   }

   /* --------- */
   ,getHtmlDefaultXPTaskbar:function (args) {
      var oSkin=args.oSkin;
      var propiedadesSkin=drWindow.getSkinParameters({skin:oSkin.id});
      var altoMenu=oSkin.marginBottom;
      if (propiedadesSkin && propiedadesSkin["startButtonLabel"])
         var startButtonLabel=propiedadesSkin["startButtonLabel"]
      else if (oSkin.startButtonLabel!=null)
         var startButtonLabel=oSkin.startButtonLabel
      else
         var startButtonLabel=drResources.get({bundle:"skins",key:"skins.txt1"});//Default
      html=[];
      html[html.length]="<div class=\"taskbarContainer\" style=\"height:"+altoMenu+"px;\" align=\"left\" >";
      html[html.length]="  <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse\" width=\"100%\" align=\"left\">";
      html[html.length]="    <tr>";
      html[html.length]="      <td>";
      html[html.length]="			<table onclick=\"muestraOcultaMenu();\" border=\"0\"  cellpadding=\"0\" cellspacing=\"0\" class=\"taskbarBtnStart\" align=\"left\" >";
      html[html.length]="				<tr>";
      html[html.length]="				  <td valign=\"middle\">"+startButtonLabel+"</td>";
      html[html.length]="				</tr>";
      html[html.length]="			</table>";
      html[html.length]="		 </td>	";
      html[html.length]="      <td width=\"1\" align=\"center\" valign=\"middle\"><div class=\"taskbarmenuDivider\"></div></td>";
      html[html.length]="      <td width=\"100%\"></td>";
      html[html.length]="      <td width=\"1\" align=\"center\" valign=\"middle\"><div class=\"taskbarmenuDivider\"></div></td>";
      html[html.length]="		<td><img id=\"SEP_BARRA_CUSTOM\" style=\"visibility:hidden;width:1px;height:1px;\"></td>"; //Esto es para dejar sitio luego (si quiero) para el reloj o lo que sea
      html[html.length]="    </tr>";
      html[html.length]="  </table>";
      html[html.length]="</div>";
      return html.join("")
   }

   /* --------- */
   ,getHtmlDefaultTaskbar:function (args) {
      var oSkin=args.oSkin;
      var propiedadesSkin=drWindow.getSkinParameters({skin:oSkin.id});
      if (propiedadesSkin && propiedadesSkin["startButtonLabel"])
         var startButtonLabel=propiedadesSkin["startButtonLabel"]
      else if (oSkin.startButtonLabel!=null)
         var startButtonLabel=oSkin.startButtonLabel
      else
         var startButtonLabel=drResources.get({bundle:"skins",key:"skins.txt1"});//Default
      html=[];
      html[html.length]="<div class=\"taskbarContainer\" >";
      html[html.length]="  <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse\" width=\"100%\" >";
      html[html.length]="    <tr>";
      html[html.length]="      <td>";
      html[html.length]="			<table id=\"taskbarBtnStart\" onclick=\"muestraOcultaMenu();\" border=\"0\"  cellpadding=\"0\" cellspacing=\"0\">";
      html[html.length]="				<tr>";
      html[html.length]="				  <td><div class=\"taskbarBtnStart1\"></div></td>";
      html[html.length]="				  <td class=\"taskbarBtnStart2 taskbarBtnStartLabel\">"+startButtonLabel+"</td>";
      html[html.length]="				  <td><div class=\"taskbarBtnStart3\"></div></td>";
      html[html.length]="				</tr>";
      html[html.length]="			</table>";
      html[html.length]="		</td>	";
      html[html.length]="      <td class=\"taskbarButtonsBg\" width=\"100%\"></td>";
      html[html.length]="      <td><div class=\"taskbarCustomBlock1\"></div></td>";
      html[html.length]="      <td class=\"taskbarCustomBlock2\"><img id=\"SEP_BARRA_CUSTOM\" style=\"visibility:hidden;width:1px;height:1px;\"></td>";
      html[html.length]="    </tr>";
      html[html.length]="  </table>";
      html[html.length]="</div>";
      return html.join("")
   }

   /* --------- */
   ,buildDefaultBackground:function(args){ //{containerId:"dDesktop",oSkin:oSkin_PRUEBA}
      var html=this.getHtmlDefaultBackground(args)
      this.putHtmlBackground({html:html,containerId:args.containerId})
   }

   /* --------- */
   ,putHtmlBackground:function(args){ //{html:html}
      var html=args.html, containerId="dDesktop";
      if(args.containerId!=null)
         containerId=args.containerId
      drDesktop.get$Win({id:containerId}).html(html).css('zIndex',0)
      drDesktop.showWindow({id:containerId})
   }

   /* --------- */
   ,getHtmlDefaultBackground:function(args){ //{oSkin:oSkin_PRUEBA}
      var urlBackgroundImage=null;
      var oSkin=args.oSkin;
      var iconpath = drWindow.getSkinUrl() //oDesktopProperties.urlSkins+'/'+oSkin.id;
      var backgroundImage=null //Default
      var expandWallpaper=false//default
      var backgroundColor="#000000" //Default
      var backgroundPosition="center center";
      if (oSkin.expandWallpaper)
         expandWallpaper=args.expandWallpaper
      if (oSkin.backgroundImage)
         urlBackgroundImage=iconpath+'/'+oSkin.backgroundImage //Si el skin define en su javascript un backgroundImage lo uso como valor por defecto
         //backgroundImage=oSkin.backgroundImage; 
      if (oSkin.backgroundColor)
         backgroundColor=oSkin.backgroundColor;
      var propiedadesSkin=drWindow.getSkinParameters({skin:oSkin.id}); //Tomo los valores de las propiedades configurables del skin
      if (propiedadesSkin) {
         if (propiedadesSkin["userBackgroundImage"] && propiedadesSkin["userBackgroundImage"].replace(/\s/g,"")!="") 
             urlBackgroundImage=drWindow.getDesktopProperties()["urlUserRoot"] + '/' + propiedadesSkin["userBackgroundImage"]
         else if (propiedadesSkin["backgroundImage"] && propiedadesSkin["backgroundImage"]!="") 
            urlBackgroundImage=iconpath + '/' + propiedadesSkin["backgroundImage"]
            //backgroundImage=propiedadesSkin["backgroundImage"];  //Si hay una propiedad configurable del skin con un backgroundImage la uso
         if (propiedadesSkin["backgroundColor"])
            backgroundColor=propiedadesSkin["backgroundColor"];
         if (propiedadesSkin["expandWallpaper"] && propiedadesSkin["expandWallpaper"]=="true")
            expandWallpaper=true;
         if (propiedadesSkin["backgroundPosition"])  
            backgroundPosition=propiedadesSkin["backgroundPosition"]
      }
      if (urlBackgroundImage==null || urlBackgroundImage=="")   { //Sin imagen
         return '<div id="fondo" style="position:absolute; width:100%; height:100%;  left: 0; top: 0; background-color: '+backgroundColor+';"></div>';
      } else if (expandWallpaper) { //Imagen que se expande
         return '<img id="fondo" style="position:absolute; width:100%; height:100%;  left: 0; top: 0;" src="'+urlBackgroundImage+'">';
      }else { //imagen centrada (por defecto)
         return '<div id="fondo" style="position:absolute; width:100%; height:100%;  left: 0; top: 0; background-color: '+backgroundColor+';background-image: url('+urlBackgroundImage+');background-repeat: no-repeat;	background-position:'+backgroundPosition+';"></div>';
      }
   }

   /* --------- */
   ,getTaskbarData:function (args) {
      var oSkin=args.oSkin;
      if (oSkin.taskbar!=null)
         var taskbarData=oSkin.taskbar
      else
         var taskbarData={marginBottom:2, marginLeft:161, marginRight:34,buttonHeight:23}
      var getHtmlButtonsFunction=taskbarData.getHtmlButtonsFunction
      if (getHtmlButtonsFunction==null)
         getHtmlButtonsFunction=this.getHtmlDefaultTaskbarButtons
      var options = {
         bh:taskbarData.buttonHeight
         ,marginBottomBotonera:taskbarData.marginBottom
         ,marginLeftBotonera:taskbarData.marginLeft
         ,marginRightBotonera:taskbarData.marginRight
         ,generaHtml:getHtmlButtonsFunction
      };
      if ($.browser.mozilla && options.bh<32)  { //FIX: El alto m�nimo para que aparezca el scroll en firefox es 32 ...
         var diferencia=32-options.bh
         if (options.marginBottomBotonera>diferencia)
            options.marginBottomBotonera-=diferencia
         else  {
            options.taskbarButtonImagePosition=32 - options.bh - options.marginBottomBotonera
            options.marginBottomBotonera=0;
         }
         //marginBottomBotonera
         options.bh=32
         
      }
      var scrollColors=taskbarData.scrollbarColors
      if (scrollColors!=null) {
         $.extend(options, {
            scrollbarFaceColor:scrollColors.face
            ,scrollbarShadowColor:scrollColors.shadow
            ,scrollbarHighlightColor:scrollColors.highLight
            ,scrollbar3dLightColor:scrollColors.c3dLight
            ,scrollbarDarkShadowColor:scrollColors.darkShadow
            ,scrollbarTrackColor:scrollColors.track
            ,scrollbarArrowColor:scrollColors.arrow
         });
      }
      var oDatosBotonera = $.extend({}, taskbarData, options);
      return oDatosBotonera
      //TO-DO: eliminar las referencias a estas props y usar directamente "oSkin_XXX" en vez de oDatosBotonera_XXX (requiere tocar las otras skins)
   }

   /* --------- */
   ,getHtmlDefaultTaskbarButtons:function (wins){
      html=[];
      for (var i=0;i<wins.length;i++) {
         winName = wins[i];
         winTitle = drDesktop.wData({id:winName, property:"wTitle"})
         if (this.taskbarButtonImagePosition!=null)   { //FIX para caso Firefox + buttonHeight<32
            html[html.length]="<div id=\"taskbarButton_" + winName + "\" onclick=\"drDesktotaskbar_p.btnClick({id:'"+winName+"'})\" class=\"taskbarButton\" style=\"height:"+this.bh+"px;float:left;position:relative;background-position:0px "+this.taskbarButtonImagePosition+"px;background-repeat: no-repeat;\">";	
            html[html.length]="  <div title=\""+winTitle+"\" class=\"taskbarButtonLabel\" style=\"margin-top:"+this.taskbarButtonImagePosition+"px\">"+winTitle+"</div>"; //TO-DO: escapar esto bien
            html[html.length]="</div>";
         } else   {
            html[html.length]="<div id=\"taskbarButton_" + winName + "\" onclick=\"drDesktop.taskbar_btnClick({id:'"+winName+"'})\" class=\"taskbarButton\" style=\"height:"+this.bh+"px;float:left;position:relative;\">";
            html[html.length]="  <div title=\""+winTitle+"\" class=\"taskbarButtonLabel\">"+winTitle+"</div>"; //TO-DO: escapar esto bien
            html[html.length]="</div>";
         }
      }
      return html.join("")
   }

   ,getIconPath:function (winId) {
      var app=null;
      var datosVent=drWindow.getDrDesktopObj().wData({id:winId, property:"wOpenArgs"})
      if (datosVent==null)
         return
      var app=datosVent.application
      if (app==null)
         return
      var iconImg=drWindow.getApplicationsConfig()[app].icon
      if (iconImg==null || iconImg=="")
         return null
      var iconPath=drWindow.getDesktopWindow().urlRaizIconos() + "/" +drWindow.getApplicationsConfig()[app].icon;
      return iconPath
   }

   /* --------- */
   ,getHtmlDefaultTaskbarButtonsWithIcons:function (wins){
      //var registroVentanas = drWindow.getRegistry()
      var indiceAplicaciones = drWindow.getApplicationsConfig()
      html=[];
      for (var i=0;i<wins.length;i++) {
         var app=null;
         var winName = wins[i];
         var winTitle = drDesktop.wData({id:winName, property:"wTitle"})
         var contenidoBtn=winTitle
         //var datosApl=registroVentanas.getVentana(winName)
         var datosApl=drWindow.getDrDesktopObj().wData({id:winName, property:"wOpenArgs"})
         if (datosApl!=null) //NULL para las recien creadas???
            var app=datosApl.application
         if (app!=null) {
            var iconPath=drWindow.getDesktopWindow().urlRaizIconos() + "/" +indiceAplicaciones[app].icon;
            contenidoBtn="<img src=\""+iconPath+"\" width=\"16\" height=\"16\" align=\"absmiddle\" style=\"margin-right:4px\">"+winTitle
            //contenidoBtn="<img align=\"absmiddle\" /> "+winTitle
         }

         if (this.taskbarButtonImagePosition!=null)   { //FIX para caso Firefox + buttonHeight<32
            html[html.length]="<div id=\"taskbarButton_" + winName + "\" onclick=\"drDesktop.taskbar_btnClick({id:'"+winName+"'})\" class=\"taskbarButton\" style=\"height:"+this.bh+"px;float:left;position:relative;background-position:0px "+this.taskbarButtonImagePosition+"px;background-repeat: no-repeat;\">";	
            html[html.length]="  <div title=\""+winTitle+"\" class=\"taskbarButtonLabel\" style=\"margin-top:"+this.taskbarButtonImagePosition+"px\">"+contenidoBtn+"</div>"; //TO-DO: escapar esto bien
            html[html.length]="</div>";
         } else   {
            html[html.length]="<div id=\"taskbarButton_" + winName + "\" onclick=\"drDesktop.taskbar_btnClick({id:'"+winName+"'})\" class=\"taskbarButton\" style=\"height:"+this.bh+"px;float:left;position:relative;\">";
            html[html.length]="  <div title=\""+winTitle+"\" class=\"taskbarButtonLabel\">"+contenidoBtn+"</div>"; //TO-DO: escapar esto bien
            html[html.length]="</div>";
         }
      }
      return html.join("")
   }


   /* --------- */
   ,getHtmlStartMenu3x1x3Chrome:function (args) { //{htmlContent}
      htmlContent=args.htmlContent
      html=[]
      html[html.length]="<table class=\"startMenu\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="  <tr>";
      html[html.length]="    <td height=\"1\">";
      html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="        <tr>";
      html[html.length]="          <td><div class=\"startMenu11\"></div></td>";
      html[html.length]="          <td class=\"startMenu12\" width=\"100%\" ></td>";
      html[html.length]="          <td><div class=\"startMenu13\"></div></td>";
      html[html.length]="        </tr>";
      html[html.length]="      </table>";
      html[html.length]="    </td>";
      html[html.length]="  </tr>";
      html[html.length]="  <tr>";
      html[html.length]="    <td>";
      html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="        <tr>";
      html[html.length]="          <td class=\"startMenu21\"><div class=\"startMenu21\"></div></td>";
      html[html.length]="          <td class=\"startMenu22\" width=\"100%\">";
      html[html.length]= htmlContent
      html[html.length]="          </td> ";
      html[html.length]="          <td class=\"startMenu23\"><div class=\"startMenu23\"></div></td>";
      html[html.length]="        </tr>";
      html[html.length]="      </table>";
      html[html.length]="    </td>";
      html[html.length]="  </tr>";
      html[html.length]="  <tr>";
      html[html.length]="    <td height=\"1\">";
      html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="        <tr>";
      html[html.length]="          <td><div class=\"startMenu31\"></div></td>";
      html[html.length]="          <td class=\"startMenu32\" width=\"100%\"></td>";
      html[html.length]="          <td><div class=\"startMenu33\"></div></td>";
      html[html.length]="        </tr>";
      html[html.length]="      </table>";
      html[html.length]="    </td>";
      html[html.length]="  </tr>";
      html[html.length]="</table>";
      return html.join('');
   }

   /* --------- */
   ,getHtmStartSubmenu2BordersChrome:function (args) { //{htmlContent}
      htmlContent=args.htmlContent
      html=[]
      html[html.length]="<div  class=\"startSubmenuOutherBorder\">";
      html[html.length]="  <div class=\"startSubmenuInnerBorder\">";
      html[html.length]= htmlContent
      html[html.length]="  </div>";
      html[html.length]="</div>";
      return html.join('');
   }

   /* --------- CHROMES -----------------------------------------------------------------*/

   ,getHtmlDefault3x3Chrome:function(args)  {//{wName}
      wName=args.wName
      //wParams=args.wParams
      html=[]
      html[html.length]="  <table class=\"chromeContainer\" width=\"100%\"  height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="    <tr>";
      html[html.length]="      <td class=\"chrome11\"></td>";
      html[html.length]="      <td class=\"chrome12\"></td>";
      html[html.length]="      <td class=\"chrome13\"></td>";
      html[html.length]="    </tr>";
      html[html.length]="    <tr>";
      html[html.length]="      <td class=\"chrome21\"></td>";
      html[html.length]="      <td class=\"chrome22\"><div id=\""+wName+"iTD\" style='width:100%; height:100%'></div></td>"; //un div para que el background sea opaco al arrastrar otras
      html[html.length]="      <td class=\"chrome23\"></td>";
      html[html.length]="    </tr>";
      html[html.length]="    <tr>";
      html[html.length]="      <td class=\"chrome31\"></td>";
      html[html.length]="      <td class=\"chrome32\"></td>";
      html[html.length]="      <td class=\"chrome33\"></td>";
      html[html.length]="    </tr>";
      html[html.length]="  </table>";
      return html.join('')
   }


   /* --------- */
   ,getHtmlDefault3x1x3Chrome:function(args)  {
      wName=args.wName
      //wParams=args.wParams
      html=[]
      html[html.length]="<table class=\"chromeContainer\" width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="  <tr>";
      html[html.length]="    <td  height=\"1\">";
      html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="        <tr>";
      html[html.length]="          <td><div class=\"chrome11\"></div></td>";
      html[html.length]="          <td class=\"chrome12\" width=\"100%\" ></td>";
      html[html.length]="          <td><div class=\"chrome13\"></div></td>";
      html[html.length]="        </tr>";
      html[html.length]="      </table>";
      html[html.length]="    </td>";
      html[html.length]="  </tr>";
      html[html.length]="  <tr>";
      html[html.length]="    <td>";
      html[html.length]="      <table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="        <tr>";
      html[html.length]="          <td><div class=\"chrome21\"></div></td>";
      html[html.length]="          <td class=\"chrome22\" width=\"100%\"><div id=\""+wName+"iTD\" style='width:100%; height:100%'></div></td>"; //un div para que el background sea opaco al arrastrar otras
      html[html.length]="          <td><div class=\"chrome23\"></div></td>";
      html[html.length]="        </tr>";
      html[html.length]="      </table>";
      html[html.length]="    </td>";
      html[html.length]="  </tr>";
      html[html.length]="  <tr>";
      html[html.length]="    <td height=\"1\">";
      html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="        <tr>";
      html[html.length]="          <td><div class=\"chrome31\"></div></td>";
      html[html.length]="          <td class=\"chrome32\" width=\"100%\"></td>";
      html[html.length]="          <td><div class=\"chrome33\"></div></td>";
      html[html.length]="        </tr>";
      html[html.length]="      </table>";
      html[html.length]="    </td>";
      html[html.length]="  </tr>";
      html[html.length]="  </table>";
      return html.join('')
   }

   /* --------- */
   ,getHtmlDefault3xNWithTitleAndButtonsChrome:function(args)  {
      var wName=args.wName
      var titulo=drDesktop.wData({id:wName, property:"wTitle"})
      //wParams=args.wParams
      html=[]
      html[html.length]="<table class=\"chromeContainer\" width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="  <tr>";
      html[html.length]="    <td  height=\"1\">";
      html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" >";
      html[html.length]="        <tr>";
      html[html.length]="          <td><div class=\"chrome11\"></div></td>";
      html[html.length]="          <td class=\"chrome12\" width=\"100%\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed\"><tr><td class=\"chromeTitle\" id=\""+wName+"_wTitle\">"+titulo+"</td></tr></table></td>";
      html[html.length]="          <td><div class=\"chromeBtnMin\" onmouseover=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'M1'})\" onmouseout=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'M0'})\"></div></td>";
      html[html.length]="          <td><div class=\"chromeBtnMax\" onmouseover=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'X1'})\" onmouseout=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'X0'})\"></div></td>";
      html[html.length]="          <td><div class=\"chromeBtnClose\" onmouseover=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'C1'})\" onmouseout=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'C0'})\"></div></td>";
      html[html.length]="          <td><div class=\"chrome13\"></div></td>";
      html[html.length]="        </tr>";
      html[html.length]="      </table>";
      html[html.length]="    </td>";
      html[html.length]="  </tr>";
      html[html.length]="  <tr>";
      html[html.length]="    <td>";
      html[html.length]="      <table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="        <tr>";
      html[html.length]="          <td><div class=\"chrome21\"></div></td>";
      html[html.length]="          <td class=\"chrome22\" width=\"100%\"><div id=\""+wName+"iTD\" style='width:100%; height:100%'></div></td>"; //un div para que el background sea opaco al arrastrar otras
      html[html.length]="          <td><div class=\"chrome23\"></div></td>";
      html[html.length]="        </tr>";
      html[html.length]="      </table>";
      html[html.length]="    </td>";
      html[html.length]="  </tr>";
      html[html.length]="  <tr>";
      html[html.length]="    <td height=\"1\">";
      html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="        <tr>";
      html[html.length]="          <td><div class=\"chrome31\"></div></td>";
      html[html.length]="          <td class=\"chrome32\" width=\"100%\"></td>";
      html[html.length]="          <td><div class=\"chrome33\"></div></td>";
      html[html.length]="        </tr>";
      html[html.length]="      </table>";
      html[html.length]="    </td>";
      html[html.length]="  </tr>";
      html[html.length]="  </table>";
      return html.join('')
   }

   /* --------- */
   ,getHtmlDefault3xNWithIconTitleAndButtonsChrome:function(args)  {
      var wName=args.wName
      var titulo=drDesktop.wData({id:wName, property:"wTitle"})
      var icono=this.getIconPath(wName)
      var wParams=args.wParams
      html=[]
      html[html.length]="<table class=\"chromeContainer\" width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="  <tr>";
      html[html.length]="    <td  height=\"1\">";
      html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" >";
      html[html.length]="        <tr>";
      //html[html.length]="          <td><div class=\"chrome11\"></div></td>";
      //html[html.length]="          <td><div class=\"chromeBtnHelp\" onmouseover=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'I1'})\" onmouseout=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'I0'})\"></td>";
      if (icono!=null)
         html[html.length]="          <td><div class=\"chrome11\"><img class=\"chromeIcon\" src=\""+icono+"\" width=\"16\" height=\"16\"></div></td>";
      else
         html[html.length]="          <td><div class=\"chrome11\"></div></td>";
      html[html.length]="          <td class=\"chrome12\" width=\"100%\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed\"><tr><td class=\"chromeTitle\" id=\""+wName+"_wTitle\">"+titulo+"</td></tr></table></td>";
      if (drWindow.hasHelp({params:wParams}))
         html[html.length]="          <td><div class=\"chromeBtnHelp\" onmouseover=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'I1'})\" onmouseout=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'I0'})\"></div></td>";
      html[html.length]="          <td><div class=\"chromeBtnMin\" onmouseover=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'M1'})\" onmouseout=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'M0'})\"></div></td>";
      html[html.length]="          <td><div class=\"chromeBtnMax\" onmouseover=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'X1'})\" onmouseout=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'X0'})\"></div></td>";
      html[html.length]="          <td><div class=\"chromeBtnClose\" onmouseover=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'C1'})\" onmouseout=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'C0'})\"></div></td>";
      html[html.length]="          <td><div class=\"chrome13\"></div></td>";
      html[html.length]="        </tr>";
      html[html.length]="      </table>";
      html[html.length]="    </td>";
      html[html.length]="  </tr>";
      html[html.length]="  <tr>";
      html[html.length]="    <td>";
      html[html.length]="      <table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="        <tr>";
      html[html.length]="          <td><div class=\"chrome21\"></div></td>";
      html[html.length]="          <td class=\"chrome22\" width=\"100%\"><div id=\""+wName+"iTD\" style='width:100%; height:100%'></div></td>"; //un div para que el background sea opaco al arrastrar otras
      html[html.length]="          <td><div class=\"chrome23\"></div></td>";
      html[html.length]="        </tr>";
      html[html.length]="      </table>";
      html[html.length]="    </td>";
      html[html.length]="  </tr>";
      html[html.length]="  <tr>";
      html[html.length]="    <td height=\"1\">";
      html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="        <tr>";
      html[html.length]="          <td><div class=\"chrome31\"></div></td>";
      html[html.length]="          <td class=\"chrome32\" width=\"100%\"></td>";
      html[html.length]="          <td><div class=\"chrome33\"></div></td>";
      html[html.length]="        </tr>";
      html[html.length]="      </table>";
      html[html.length]="    </td>";
      html[html.length]="  </tr>";
      html[html.length]="  </table>";
      return html.join('')
   }

   /* --------- */
   , getHtmlDefaultXPWindow:function (args){
      wName=args.wName
      wParams=args.wParams
      oSkin=args.oSkin
      if (wParams.widget)
         return oSkinFactory.getHtmlDefaultWidget(wName,wParams);
      var frame_bgcolor = "#0069FF";
      var frame_titleclass = "drWinTitle";
      var frame_borderwidth = 2;
      var frame_topheight = 27;
      var frame_bottomheight = 10;
      var frame_contentbgcolor = '#9CB6FF';
      var frame_dummypic = 'img/t.gif';
      var iconpath =  oDesktopProperties.urlSkins+'/PRUEBA/images';
      var frame_stylecolor = '#ffffff';
      var frame_border = 2;
      var frame_bordertype = "outset"; // solid, outset, inset
      var frame_style = 	'border-top: ' + frame_border + 'px ' + frame_stylecolor + ' ' + frame_bordertype + '; ' +
         'border-bottom: ' + frame_border + 'px ' + frame_stylecolor + ' ' + frame_bordertype + '; ' +
         'border-left: ' + frame_border + 'px ' + frame_stylecolor + ' ' + frame_bordertype + '; ' +
         'border-right: ' + frame_border + 'px ' + frame_stylecolor + ' ' + frame_bordertype + '; ' +
         'cursor:pointer;'
      var iconoAyuda='<td class=""><a class="" href="javascript: void(0)" onmouseover="' + "SwiImg('winleft_" + wName + "','" + iconpath + "/winleft_over.gif'); " + 'drDesktop.wData({id:' + "'" + wName + "',property:'wIcon',value:'I1'" +'})" ' +  'onmouseout="' + "SwiImg('winleft_" + wName + "','" + iconpath + "/winleft.gif');" + 'drDesktop.wData({id:' + "'" + wName + "',property:'wIcon',value:'I0'" + '})"'+ '><img name="winleft_' + wName + '" border="0" src="' + iconpath + '/winleft.gif"></a></td>'  
      return (
         '<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%"><tr>' +
         '<td align="left" valign="top" height="100%" width="100%" style="' + frame_style + '">' +
         '<table cellpadding="0" cellspacing="0" border="0" height="100% width="100%" bgcolor="' + frame_bgcolor + '">' +
         '<tr><td><img src="' + frame_dummypic + '" width="' + frame_borderwidth + '" height="' + frame_topheight + '" border="0"></td>' + 
              '<td width="100%" align="left" valign="middle" class="' + frame_titleclass + '">' + 
              '<table cellpadding="0" cellspacing="0" border="0" background="' + iconpath + '/wintitlebgr.gif"><tr>' +
                '<td><img src="' + frame_dummypic + '" width="1" border="0"></td>' + 
                iconoAyuda +
                '<td width="100%" align="left" valign="middle" class="' + frame_titleclass + '">&nbsp;<span  id="'+wName+'_wTitle">' + drDesktop.wData({id:wName, property:"wTitle"}) + '</span></td>' +
                '<td class="' + frame_titleclass + '"><a class="" href="javascript: void(0)" onmouseover="' + "SwiImg('winmin_" + wName + "','" + iconpath + "/winmin_over.gif'); " + 'drDesktop.wData({id:' + "'" + wName + "',property:'wIcon',value:'M1'" +'})" ' +  'onmouseout="' + "SwiImg('winmin_" + wName + "','" + iconpath + "/winmin.gif'); " + 'drDesktop.wData({id:' + "'" + wName + "',property:'wIcon',value:'M0'" + '})"'+ '><img name="winmin_' + wName + '" border="0" src="' + iconpath + '/winmin.gif"></a></td>' +
                '<td><img src="' + frame_dummypic + '" width="2" border="0"></td>' + 
                '<td class="' + frame_titleclass + '"><a class="" href="javascript: void(0)" onmouseover="' + "SwiImg('winmax_" + wName + "','" + iconpath + "/winmax_over.gif'); " + 'drDesktop.wData({id:' + "'" + wName + "',property:'wIcon',value:'X1'" +'})" ' +  'onmouseout="' + "SwiImg('winmax_" + wName + "','" + iconpath + "/winmax.gif'); " + 'drDesktop.wData({id:' + "'" + wName + "',property:'wIcon',value:'X0'" + '})"'+ '><img name="winmax_' + wName + '" border="0" src="' + iconpath + '/winmax.gif"></a></td>' +
                '<td><img src="' + frame_dummypic + '" width="2" border="0"></td>' + 
                '<td class="' + frame_titleclass + '"><a class="" href="javascript: void(0)" onmouseover="' + "SwiImg('winclose_" + wName + "','" + iconpath + "/winclose_over.gif'); " + 'drDesktop.wData({id:' + "'" + wName + "',property:'wIcon',value:'C1'" +'})" ' +  'onmouseout="' + "SwiImg('winclose_" + wName + "','" + iconpath + "/winclose.gif'); " + 'drDesktop.wData({id:' + "'" + wName + "',property:'wIcon',value:'C0'" + '})"'+ '><img name="winclose_' + wName + '" border="0" src="' + iconpath + '/winclose.gif"></a></td>' +
              '</tr></table>' +
              '</td>' + 
              '<td><img src="' + frame_dummypic + '" width="' + frame_borderwidth + '" height="' + frame_topheight + ' border="0"></td></tr>' +
         '<tr><td><img src="' + frame_dummypic + '" width="' + frame_borderwidth + '" border="0"></td><td align="left" valign="top" width="100%" height="100%" style="background: ' + frame_contentbgcolor +'; " id="' + wName + 'iTD' + '"></td><td><img src="' + frame_dummypic + '" width="' + frame_borderwidth + '" border="0"></td></tr>' +
         '<tr><td><img src="' + frame_dummypic + '" width="' + frame_borderwidth + '" height="' + frame_bottomheight + '" border="0"></td><td width="100%"></td><td><img src="' + frame_dummypic + '" width="' + frame_borderwidth + '" height="' + frame_bottomheight + '" border="0"></td></tr></table>' +
         '</td></tr></table>'
      );
   }

   /* --------- */

   ,getHtmlScriptWindowButton:function(args) {
      var wName=args.wName;
      if (args.id=="min") {
         if (args.event=="mouseover")
            return "drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'M1'})"
         else if (args.event=="mouseout")
            return "drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'M0'})"
      } else if (args.id=="max"){
         if (args.event=="mouseover")
            return "drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'X1'})"
         else if (args.event=="mouseout")
            return "drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'X0'})"
      } else if (args.id=="close"){
         if (args.event=="mouseover")
            return "drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'C1'})"
         else if (args.event=="mouseout")
            return "drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'C0'})"
      }
   }

   /* --------- */
   ,createSkin:function (oSkin) {
      var id=oSkin.id
      window["oSkin_"+id]=oSkin //para el futuro
      window["desktop_"+id]=oSkin.initSkinFunction
      window["barra_"+id]=oSkin.getHtmlTaskbarFunction
      window["skin_"+id]=oSkin.getHtmlWindowFunction
      window["oDatosBotonera_"+id]=this.getTaskbarData({oSkin:oSkin})
      window["oDatosMenuInicio_"+id]=this.getStartMenuData({oSkin:oSkin})
      if (oSkin.startMenu && oSkin.startMenu.getHtmlFunction)
         window["menuInicio_"+id]=oSkin.startMenu.getHtmlFunction
   }

   /* --------- */
   ,getStartMenuData:function (args) {
      var oSkin=args.oSkin;
      if (oSkin.startMenu==null)
         return
      var startMenuData=oSkin.startMenu

      var options = {
         separacionHorizontalEntreMenus:startMenuData.hOffset
         ,ajusteAlturaSubnivel:startMenuData.vOffset
      };
      for (var i in startMenuData)  {
         if (i.indexOf("vOffset_lv_")==0)
            options["ajusteAlturaSubnivel_"+i.substr(11)]=startMenuData[i]
      }
      var oDatosBotonera = $.extend({}, startMenuData, options);
      return oDatosBotonera
      //TO-DO: eliminar las referencias a estas props y usar directamente estas props (requiere tocar otros skins)
   }

   /* --------- */
   ,getHtmlStartMenuOptions: function(args)  { //{options, level}  retorna un HTML con las opciones del menu de inicio
      var options=args.options, level=args.level
      var html=[]
      html[html.length]="<table id='OPS_MENU_"+level+"' width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed\">";
      for (var r=0;r<options.length;r++) 
         html[html.length]=this.getHtmlStartMenuOptions_singleOption ({option:options[r],level:level,rowIndex:r})
      html[html.length]="</table>";
      return html.join('')
   }

   ,getHtmlStartMenuOptions_singleOption: function (args)   {
      var option=args.option, level=args.level, rowIndex=args.rowIndex
      if (option.tipo==1)
         return "" //Barra
      if (option.aplicacion) {
         var oAplicacion=drWindow.getApplicationsConfig()[option.aplicacion]
         var tooltip=dameTooltipDeAplicacion(oAplicacion)
         var texto=dameTextoDeAplicacion(oAplicacion)
         }
      else  {
         var tooltip=option.texto
         var texto=option.texto
         }
      if (option.img) 
         var urlIcono=dameUrlIcono(option.img)
      else if (option.aplicacion) 
         var urlIcono=dameUrlIconoDeAplicacion(oAplicacion)
      else if (option.hijos!=null) 
         urlIcono=dameUrlIconoDeCarpeta()
      var className="startMenuRow"
      if (option.hijos!=null)
         className+=" _fatherRow startMenu_fatherRow"     
      html=[]
      html[html.length]="  <tr id=\""+option.id+"\" class=\""+className+"\" onmouseover=\"oSkinFactory.defaultStartMenuOptionMouseOver(this,"+level+","+rowIndex+")\">";
      html[html.length]="    <td width=\"1\" class=\"startMenuIcon\"><img src=\""+urlIcono+"\"></td>";
      html[html.length]="    <td class=\"startMenuLabel\" title=\""+DrEscHTML(tooltip)+"\">"+texto+"</td>";
      html[html.length]="    <td class=\"startMenuRightSeparation\"></td>";
      html[html.length]="  </tr>";
      return html.join('');
   }

   ,defaultStartMenuOptionMouseOver:function(oTD,level,rowIndex)   { 
      var jqTd=$(oTD)
      var jqFilasTabla=$("#OPS_MENU_"+level+" TR");
      jqFilasTabla.removeClass("startMenu_fatherRow_rolloverRow").removeClass("startMenu_rolloverRow")
      if (jqTd.hasClass("_fatherRow"))
         jqTd.addClass("startMenu_fatherRow_rolloverRow") 
      else
         jqTd.addClass("startMenu_rolloverRow") 
   }

   /* --------- */

   ,getHtmlStartSubmenuOptions:function(args)  { //{options, level}  retorna un HTML con las subopciones del menu de inicio (peq)
      var options=args.options, level=args.level
      var html=[]
      html[html.length]="<table id='OPS_MENU_"+level+"' width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed;width:100%;\">";
      for (var r=0;r<options.length;r++) 
         html[html.length]=this.getHtmlStartSubmenuOptions_singleOption ({option:options[r],level:level,rowIndex:r, iconWidth:args.iconWidth, iconHeight:args.iconHeight })
      html[html.length]="</table>";
      return html.join('')
   }

   ,getHtmlStartSubmenuOptions_singleOption: function (args)   {
      var option=args.option, level=args.level, rowIndex=args.rowIndex, iconWidth=args.iconWidth, iconHeight=args.iconHeight
      iconWidth=(iconWidth==null)?16:iconWidth
      iconHeight=(iconHeight==null)?16:iconHeight
      if (option.tipo==1)
         return "" //Barra
      if (option.aplicacion) {
         var oAplicacion=drWindow.getApplicationsConfig()[option.aplicacion]
         var tooltip=dameTooltipDeAplicacion(oAplicacion)
         var texto=dameTextoDeAplicacion(oAplicacion)
         }
      else  {
         var tooltip=option.texto
         var texto=option.texto
         }
      if (option.img) 
         var urlIcono=dameUrlIcono(option.img)
      else if (option.aplicacion) 
         var urlIcono=dameUrlIconoDeAplicacion(oAplicacion)
      else if (option.hijos!=null) 
         urlIcono=dameUrlIconoDeCarpeta16x16()
      var className="startSubmenuRow"
      if (option.hijos!=null)
         className+=" _fatherRow startSubmenu_fatherRow"     
      html=[]
      html[html.length]="  <tr id=\""+option.id+"\" class=\""+className+"\" onmouseover=\"oSkinFactory.defaultStartSubmenuOptionMouseOver(this,"+level+","+rowIndex+")\">";
      html[html.length]="    <td width=\"1\" class=\"startSubmenuIcon\"><img width=\""+iconWidth+"\" height=\""+iconHeight+"\" src=\""+urlIcono+"\"></td>";
      html[html.length]="    <td class=\"startSubmenuLabel\" title=\""+DrEscHTML(tooltip)+"\">"+texto+"</td>";
      html[html.length]="    <td class=\"startSubmenuRightSeparation\"></td>";
      html[html.length]="  </tr>";
      return html.join('');
   }

   ,defaultStartSubmenuOptionMouseOver:function(oTD,level,rowIndex)   {
      var jqTd=$(oTD)
      var jqFilasTabla=$("#OPS_MENU_"+level+" TR");
      jqFilasTabla.removeClass("startSubmenu_fatherRow_rolloverRow").removeClass("startSubmenu_rolloverRow")
      if (jqTd.hasClass("_fatherRow"))
         jqTd.addClass("startSubmenu_fatherRow_rolloverRow") 
      else
         jqTd.addClass("startSubmenu_rolloverRow") 
   }

   ,defaultOnWindowChangeFocusStatus:function(args)   {
      if (args.focus)   {
         $("#"+args.id+"_wTitle").addClass("chromeTitleFocused").removeClass("chromeTitle")
         $("#taskbarButton_"+args.id).addClass("taskbarButtonFocused").removeClass("taskbarButton")
      } else {
         $("#"+args.id+"_wTitle").addClass("chromeTitle").removeClass("chromeTitleFocused")
         $("#taskbarButton_"+args.id).addClass("taskbarButton").removeClass("taskbarButtonFocused")
      }
   }

}




function SwiImg(obj,img) { // Switch Image
   img = img.replace(/^https?:\/\/[^\/]+\//i,'/');
   document.images[obj].src = img;
}