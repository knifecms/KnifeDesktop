var drEvents={

   bind:function(args)   {
      var type=args.type, functionName=args.functionName, parentObject=args.parentObject, parentWindowId=args.parentWindowId, familyOnly=args.familyOnly
      if (parentWindowId!=null)  
         parentObject=drWindow.getNativeWindow({"id":parentWindowId})
      if (familyOnly+"" ==true) //acepto strings (inv desde consola)
         familyOnly=true
      else
         familyOnly=false
      registraReceptorEventos(type,functionName,parentObject,familyOnly)
   }

   ,unbind:function(args)   {
      var type=args.type, parentObject=args.parentObject, parentWindowId=args.parentWindowId
      if (parentWindowId!=null)  
         parentObject=drWindow.getNativeWindow({"id":parentWindowId})
      liberaReceptorEventos(args.type,args.parentObject)
   }

   ,trigger:function(args)   {
      if (args==null)
         return
      if (args.jsonData!=null)   {
         try   {
            eval("args.data="+args.jsonData)
         } catch(e){}
      }
      lanzaEvento(args.type, args.data)
   }

   ,getPublicMethods:function()  {
   	if (window.drResources==null)
   		return null
      var publicMethods={
         info: drResources.get({bundle:"core",key:"eventsJsApi.txt1"}) 
         ,subCommands:{
            bind:{info: drResources.get({bundle:"core",key:"eventsJsApi.txt2"}) , args:[{name:"type", info: drResources.get({bundle:"core",key:"eventsJsApi.txt3"}) },{name:"functionName", info: drResources.get({bundle:"core",key:"eventsJsApi.txt4"}) },{name:"parentObject", info: drResources.get({bundle:"core",key:"eventsJsApi.txt5"}) },{name:"parentWindowId", info: drResources.get({bundle:"core",key:"eventsJsApi.txt6"}) },{name:"familyOnly", info: drResources.get({bundle:"core",key:"eventsJsApi.txt7"}) }]}
            ,unbind:{info: drResources.get({bundle:"core",key:"eventsJsApi.txt8"}) , args:[{name:"type", info: drResources.get({bundle:"core",key:"eventsJsApi.txt9"}) },{name:"parentObject", info: drResources.get({bundle:"core",key:"eventsJsApi.txt10"}) },{name:"parentWindowId", info: drResources.get({bundle:"core",key:"eventsJsApi.txt11"}) }]}
            ,trigger:{info: drResources.get({bundle:"core",key:"eventsJsApi.txt12"}) , args:[{name:"type", info: drResources.get({bundle:"core",key:"eventsJsApi.txt13"}) }, {name:"jsonData", info: drResources.get({bundle:"core",key:"eventsJsApi.txt14"}) }, {name:"data", info: drResources.get({bundle:"core",key:"eventsJsApi.txt15"}) }]}
         }
      }   		
   	/*	
      var publicMethods={
         info:"Objeto para la manipulaci�n de eventos."
         ,subCommands:{
            bind:{info:"Asocia una funci�n javascript (handler o manejador) a un tipo de evento.", args:[{name:"type", info:"Nombre del evento que identifica a un grupo de manejadores que ser�n llamados a la vez."},{name:"functionName", info:"Nombre de la funci�n javascript a invocar (sin parentesis). Se le pasar�n como par�metros los datos enviados con el evento."},{name:"parentObject", info:"[opcional] Objeto propietario de la funci�n. Por defecto es el objeto 'window' nativo en el contexto en que se ejecute 'drEvents.bind()'."},{name:"parentWindowId", info:"[opcional] Id de la ventana del escritorio cuyo objeto window nativo contenido es el propietario de la funci�n."},{name:"familyOnly", info:"[opcional] Si vale 'true' entonces solo se invocan los handlers de ventanas del escritorio descendientes o ancestros de esta."}]}
            ,unbind:{info:"Libera todos los handlers de un cierto tipo Asociados a un objeto (parentObject).", args:[{name:"type", info:"Nombre del evento que identifica a un grupo de manejadores que ser�n liberados."},{name:"parentObject", info:"[opcional] Objeto propietario de las funci�nes (handlers). Por defecto es el objeto 'window' nativo en el contexto en que se ejecute el m�todo."},{name:"parentWindowId", info:"[opcional] Id de la ventana del escritorio cuyo objeto window nativo contenido es el propietario de las funci�nes (handlers)."}]}
            ,trigger:{info:"Lanza un evento ejecutando todos los manejadores (handlers) asociados a dicho evento.", args:[{name:"type", info:"Nombre del evento que identifica el grupo de manejadores (handlers) que ser�n invocados."}, {name:"jsonData", info:"Cadena de texto que representa un objeto Javascript que se le pasar� a todos los manejadores (handlers). M�todo creado por conveniencia para lanzar eventos desde consola con datos complejos."}, {name:"data", info:"Objeto javascript que se le pasar� a todos los manejadores. Puede ser cualquier cosa, no necesariamente una cadena de caracteres."}]}
         }
      }*/
      return publicMethods
   }
}



//Si uso registraReceptorEventosSoloFamilia deber� estar en el dektop y usar tambi�n la librer�a DrDesktop_ventanas 


//Publicas

function registraReceptorEventos(evento, funcion, objetoPropietario, soloFamilia)  {
	if (objetoPropietario==null)
		objetoPropietario=window
	//Para un evento, un objeto propietario y una funci�n solo permito un registro para evitar errores
   confirmaExisteControlador()
   var regsEvento=CE.eventos[evento]
   if (regsEvento==null)	{
	   regsEvento=[]
      CE.eventos[evento]=regsEvento
      }
   var ind=buscaEnArrayLiseners(regsEvento, funcion, objetoPropietario)
   if (ind==-1){
   	if (soloFamilia)
	   	regsEvento[regsEvento.length]=[funcion,objetoPropietario,true]
	   else
		   regsEvento[regsEvento.length]=[funcion,objetoPropietario,false]
	   }

   lanzaEvento("_drDesktop_registraReceptorEventos", {evento:evento, funcion:funcion.toString(), soloFamilia:soloFamilia })
	//Si ya estaba registrado no lo vuelvo a registrar
   }
   
function registraReceptorEventosSoloFamilia   (evento, funcion, objetoPropietario)	{
	//Este m�todo ha de usarse en conjunci�n con la librer�a DrDesktop_ventanas.js
	registraReceptorEventos(evento, funcion, objetoPropietario, true)
	}
   
   
function liberaReceptorEventos(evento,objetoPropietario)   {
	if (objetoPropietario==null)
		objetoPropietario=window
	confirmaExisteControlador()
	var regsEvento=CE.eventos[evento]
	if (regsEvento!=null){
      var nuevoRegsEvento=[]
      for (var r=0;r<regsEvento.length;r++)	{
			if (regsEvento[r]!=null && regsEvento[r][1]!=objetoPropietario)
				nuevoRegsEvento[nuevoRegsEvento.length]=regsEvento[r]
         }
      CE.eventos[evento]=nuevoRegsEvento;
      /*
		for (var r=0;r<regsEvento.length;r++)	{
			if (regsEvento[r]!=null && regsEvento[r][1]==objetoPropietario)
				regsEvento[r]=null;
			}
      */
		}

	}

function confirmaExisteControlador() {
   if (top.controladorEventos==null)   
      top.controladorEventos={"eventos":{}}
   CE=top.controladorEventos
   }
   

function eliminaHandlersNoAccesibles(evento)  { //Verifica los handlers de un evento y elimina aquellos que no sean correctos (null, ventana cerrada, otro dominio, ...)
   var funcAsociadas=CE.eventos[evento]
   if (funcAsociadas==null) //El evento no tiene ya funciones?
      return
   //Hago un test r�pido para ver si hay que lanzar el limpiador o no
   var hayQueLimpiar=false, r=0; 
   while(r<funcAsociadas.length && !hayQueLimpiar) {
      var datosUnhandler=funcAsociadas[r]
      if (datosUnhandler!=null) {
         var laVentana=datosUnhandler[1]
         var nombreFuncion=datosUnhandler[0]
         if (laVentana.closed==true)   //pertenecia a una ventana cerrada
            hayQueLimpiar=true
         else try {laVentana[nombreFuncion]} 
	         catch (e) {hayQueLimpiar=true} //caso de problemas de seguridad entre maquinas distintas
         }
      r++;
      }
   if (hayQueLimpiar)   {//Si hemos detectado un solo problema lanzamos todo el proceso del limpiador.
      var nuevoFuncAsociadas=[]
      for (var r=0;r<funcAsociadas.length;r++)  {
         var datosUnhandler=funcAsociadas[r]
         var valido=true;
         if(datosUnhandler==null)
            valido=false
         else {
            var laVentana=datosUnhandler[1]
            var nombreFuncion=datosUnhandler[0]
            if (laVentana.closed==true)   //pertenecia a una ventana cerrada
               valido=false
            else try {laVentana[nombreFuncion]} 
               catch (e) {valido=false} //caso de problemas de seguridad entre maquinas distintas
            }
         if (valido) {
            nuevoFuncAsociadas[nuevoFuncAsociadas.length]=datosUnhandler;
            }
         }
      CE.eventos[evento]=nuevoFuncAsociadas;
      }
   }

function lanzaEvento(evento,datos)  {
   var contador=0;
   confirmaExisteControlador()
   eliminaHandlersNoAccesibles(evento);
   var funcAsociadas=CE.eventos[evento]
   if (funcAsociadas!=null)   { //El evento no tiene ya funciones ?
      //alert(funcAsociadas.length)
      for (var r=0;r<funcAsociadas.length;r++)  {//Ejecuto cada funci�n asociada
         var datosUnhandler=funcAsociadas[r]
         var laVentana=datosUnhandler[1]
         var nombreFuncion=datosUnhandler[0]
         if (datosUnhandler[2])	{
            //	si estoy aqu� he usado registraReceptorEventosSoloFamilia, esto ha de usarse en conjunci�n con la librer�a DrDesktop_ventanas.js
            var id_lanz=""
            var id_recep=""	      			
            id_lanz=DruidaDameMiIdVentana()
            id_recep=funcAsociadas[r][1].DruidaDameMiIdVentana()
            //alert("solo familia? ID_LANZADOR: "+id_lanz+", ID_RECEPTOR: "+id_recep)
            if (DruidaSonVentanasFamilia(id_lanz,id_recep)) {
               laVentana[nombreFuncion](datos)
               contador++
               }
            }
         else	{
            laVentana[nombreFuncion](datos)
            contador++
            }
         }
      }
   if (evento!="_drDesktop_lanzaEvento" && evento != "_drDesktop_registraReceptorEventos")
      lanzaEvento("_drDesktop_lanzaEvento", {evento:evento, datos:datos, contador:contador})
   }


function buscaEnArrayLiseners(regsEvento, funcion, objetoPropietario) {
	for (var r=0;r<regsEvento.length;r++)
		if (regsEvento[r]!=null && regsEvento[r][0]==funcion && regsEvento[r][1]==objetoPropietario)
			return r
	return -1
	}  
