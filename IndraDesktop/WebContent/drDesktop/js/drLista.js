function clDrLista(defLista) {
   this.defLista=defLista
   // Propiedades que se pueden definir en defLista 
   this.idContenedor=null
   this.clase=null
   this.claseTablaCabecera=null
   this.claseCapaDatos=null
   this.claseCeldaAdicionalCabecera=null
   this.claseCeldaScrollCabecera=null
   this.claseCeldaAdicionalDatos=null
   this.claseResaltado=null
   this.claseSeleccionado=null
   this.idColumnaDatosCodigo=null
   this.arr_columnas=null
   this.arr_filas=null
   this.f_usuario=null
   this.multiseleccion=false 
   this.esArbol=false
   this.ordenarPadresPrimero=false //Aplica si es un arbol
   this.idColumnaDatosCodigoPadre=null //Aplica si es un arbol
   //Estructura fila en columna con tabulada=true :
   // PADRE: (separador tabulacion) (imgAbrir/cerrar) (separador pxSeparacionRama) [(imgIconoRama) espacio]? texto
   // HIJO:  (separador tabulacion) (separador pxSeparacionHoja                  ) [(imgIconoHoja) espacio]? texto
   this.imgAbrirArbol=null //Opcional (si null no sale) //Aplica si es un arbol
   this.imgCerrarArbol=null //Opcional //Aplica si es un arbol
   this.imgAbrirArbolResaltada=null //Opcional (si null no sale / no aplica) //Aplica si es un arbol
   this.imgCerrarArbolResaltada=null //Opcional //Aplica si es un arbol
   this.imgIconoRama=null //Opcional (si null no sale) //Aplica si es un arbol
   this.imgIconoHoja=null //Opcional //Aplica si es un arbol
   this.dobleClickAbre=false //Aplica si es un arbol
   this.colorRollOverMenuDerecho="#8BADCD"
   this.anchoTabulacionArbol=19 //Aplica si es un arbol
   this.pxSeparacionRama=6   //Aplica si es un arbol   
   this.pxSeparacionHoja=15 //(6 de sep + 9px por la imagen de abrir/cerrar) //Aplica si es un arbol
   this.anchoMinimoMenuDerecho=130 

   // M�todos
   this.prepara=f_lista_prepara
   this.preparaPropiedades=f_lista_preparaPropiedades
   this.preparaArbol=f_lista_preparaArbol
   this.preparaArbol_recursiva=f_lista_preparaArbol_recursiva
   this.preparaArbol_recursiva_padresPrimero=f_lista_preparaArbol_recursiva_padresPrimero
   this.preparaHtml=f_lista_preparaHtml
   this.preparaVariablesCapas=f_lista_preparaVariablesCapas
   this.preparaCapas=f_lista_preparaCapas
   this.preparaEventos=f_lista_preparaEventos
   this.regeneraPropiedadesVisuales=f_lista_regeneraPropiedadesVisuales
   this.generaHtml=f_lista_generaHtml
   this.recoloca=f_lista_recoloca
   this.recalculaScroll=f_lista_recalculaScroll
   this.selecciona=f_lista_selecciona 
   this.seleccionaCodigo=f_lista_seleccionaCodigo
   this.seleccionaTodos=f_lista_seleccionaTodos
   this.seleccionaTodosSinEjecutar=f_lista_seleccionaTodosSinEjecutar
   this.deseleccionaTodos=f_lista_deseleccionaTodos
   this.seleccionaRegenerando=f_lista_seleccionaRegenerando
   this.resaltaFila=f_lista_resaltaFila
   this.quitaResaltadoFila=f_lista_quitaResaltadoFila
   this.sacaCodigoDeSeleccionados=f_lista_sacaCodigoDeSeleccionados
   this.meteCodigoEnSeleccionados=f_lista_meteCodigoEnSeleccionados
   this.codigoEstaSeleccionado=f_lista_codigoEstaSeleccionado
   this.dameArrayCodigos=f_lista_dameArrayCodigos
   this.dameArrayCodigosSeleccionados=f_lista_dameArrayCodigosSeleccionados
   this.ordena=f_lista_ordena 
   this.redimensiona=f_lista_redimensiona
   this.ajustaAlto=f_lista_ajustaAlto
   this.ocultaColumna=f_lista_ocultaColumna
   this.muestraColumna=f_lista_muestraColumna
   this.esVisibleColumna=f_lista_esVisibleColumna
   this.mueveColumna=f_lista_mueveColumna
   this.preparaParaRegenerar=f_lista_preparaParaRegenerar
   this.regenera=f_lista_regenera
   this.dameTrAsociadoAlCodigo=f_lista_dameTrAsociadoAlCodigo
   this.generaHtmlMenuDerechoCabecera=f_lista_generaHtmlMenuDerechoCabecera
   this.cierraRama=f_lista_cierraRama
   this.abreRama=f_lista_abreRama
   this.abreCierraRama=f_lista_abreCierraRama
   this.dameArrayCodigosDescendientesDe=f_lista_dameArrayCodigosDescendientesDe
   this.dameArrayCodigosDescendientesVisiblesDe=f_lista_dameArrayCodigosDescendientesVisiblesDe
   this.ocultaFilaConCodigo=f_lista_ocultaFilaConCodigo
   this.muestraFilaConCodigo=f_lista_muestraFilaConCodigo
   this.cierraArbol=f_lista_cierraArbol
   this.abreArbol=f_lista_abreArbol
   this.dameControladorRama=f_lista_dameControladorRama
   this.limpiarDatos=f_lista_limpiarDatos
   // eventos
   this.onmousedown=f_lista_onmousedown 
   this.ondblclick=f_lista_ondblclick
   this.onmouseover=f_lista_onmouseover 
   this.onmouseout=f_lista_onmouseout
   this.cabecera_ondragstart=f_lista_cabecera_ondragstart
   this.cabecera_onmousedown=f_lista_cabecera_onmousedown
   this.cabecera_onmouseup=f_lista_cabecera_onmouseup
   this.cabecera_onmouseover=f_lista_cabecera_onmouseover
   this.cabecera_onmouseout=f_lista_cabecera_onmouseout
   this.cabecera_onmousemove=f_lista_cabecera_onmousemove
   this.cabecera_alMoverControl=f_lista_cabecera_alMoverControl
   this.cabecera_alSoltarControl=f_lista_cabecera_alSoltarControl
   this.cabecera_oncontextmenu=f_lista_cabecera_oncontextmenu
   this.datos_onscroll=f_lista_datos_onscroll
   }


function clDrContenedorListas () {
   this.indice={}
   this.listas=[]
   this.registra=function (oLista)   {
      if (this.indice[oLista.idContenedor]==null)
         this.listas[this.listas.length]=oLista
      this.indice[oLista.idContenedor]=oLista
      }
   this.limpiarDatos=function ()  {
      for (var r=0; r<this.listas.length;r++)   
         this.listas[r].limpiarDatos()
      }
   }


/* funciones objetos LISTA (una vez en toda la p�gina) */
function f_lista_prepara() {
   this.preparaPropiedades()
   if (dameObjId(this.defLista.idContenedor)){
      this.preparaArbol();
      this.preparaHtml()
      this.preparaVariablesCapas()
      this.preparaCapas()
      this.preparaEventos()
      this.recalculaScroll()
      this.recoloca()
      this.regeneraPropiedadesVisuales()
      lista_version(this)
      }
   }

function f_lista_preparaPropiedades()  {
   for (var r in this.defLista)  
      this[r]=this.defLista[r]
   if (window.drObjListas==null) 
      window.drObjListas=new clDrContenedorListas()
   window.drObjListas.registra(this)
   }


function f_lista_preparaHtml()   {
   this.cpCONTENEDOR=dameObjId(this.idContenedor)
   this.cpCONTENEDOR.innerHTML=this.generaHtml()
   }

function f_lista_generaHtml()   {
   html=[]
   //Datos
   html[html.length]="      <DIV id=\""+this.idContenedor+"_DATOS\" class=\""+this.claseCapaDatos+"\" >";
   html[html.length]="         <table id=\""+this.idContenedor+"_TABLA_DATOS\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" ondblclick=\"drObjListas.indice."+this.idContenedor+".ondblclick(this)\" onmousedown=\"drObjListas.indice."+this.idContenedor+".onmousedown(this)\" onmouseover=\"drObjListas.indice."+this.idContenedor+".onmouseover(this)\" onmouseout=\"drObjListas.indice."+this.idContenedor+".onmouseout(this)\">";
   //Definicion columnas de datos
   for (var i=0; i<this.arr_columnas.length; i++)  {
      html[html.length]="           <COL class=\""+this.arr_columnas[i].claseDatos+"\" style=\"width:"+this.arr_columnas[i].ancho+"\"></COL>";
      }
   html[html.length]="           <COL class=\""+this.claseCeldaAdicionalDatos+"\" ID=\""+this.idContenedor+"_COL_ADIC\"></COL>";

   //Filas de datos
   var idcod=this.idColumnaDatosCodigo
   var filas=this.arr_filas
   var defCol=this.arr_columnas
   if (this.esArbol) { //Datos si es arbol
      for (var f=0; f<filas.length; f++)  {
         var objFila=filas[f]
         if (objFila[idcod]!=null)
            html[html.length]="           <tr COD=\""+DrEscHTML(objFila[idcod])+"\">";
         else
            html[html.length]="           <tr>";
         //columnas de datos de una fila
         for (var c=0; c<defCol.length; c++)  {
            if (defCol[c].tabulada) { //Si es una columna de tipo arbol
               var tabulacion="<img align=\"absmiddle\" style=\"visibility:hidden;height:1px;width:"+(this.arr_profundidades_filas[f] * this.anchoTabulacionArbol)+"px\" />"
               var controlador=[]
               var elCod=objFila[idcod]
               if (this.imgCerrarArbol!=null && elCod!=null)  { //Si hay boton de abrir / cerrar
                  if (this.imgCerrarArbolResaltada!=null)   {//Si tiene rollover
                     if (this.nodosIndexados[elCod].padre)  {
                        var elOnmouseout="var laLista=drObjListas.indice['"+this.idContenedor+"'];var elNodo=laLista.nodosIndexados['"+elCod+"']; if (elNodo!=null &amp;&amp; elNodo.abierto){this.src=laLista.imgCerrarArbol}else{this.src=laLista.imgAbrirArbol}"//Falta escapar la ' tambi�n TO-DO
                        var elOnmouseover="var laLista=drObjListas.indice['"+this.idContenedor+"'];var elNodo=laLista.nodosIndexados['"+elCod+"']; if (elNodo!=null &amp;&amp; elNodo.abierto){this.src=laLista.imgCerrarArbolResaltada}else{this.src=laLista.imgAbrirArbolResaltada}"
                        var elOnmousedown="drObjListas.indice."+this.idContenedor+".abreCierraRama('"+DrEscHTML(objFila[idcod])+"'); this.onmouseover(); DrCancelaEventos()"
                        controlador[controlador.length]="<img align=\"absmiddle\" id=\"iconoControladorRama\" src=\""+this.imgCerrarArbol+"\" onmouseover=\""+elOnmouseover+"\" onmouseout=\""+elOnmouseout+"\" onmousedown=\""+elOnmousedown+"\">" 
                        controlador[controlador.length]="<img align=\"absmiddle\" style=\"visibility:hidden;height:1px;width:"+this.pxSeparacionRama+"px\" />"
                        if (this.imgIconoRama!=null)  
                           controlador[controlador.length]=["<img src=\""+this.imgIconoRama+"\" align=absmiddle> "]
                        }
                     else  {
                        controlador[controlador.length]="<img align=\"absmiddle\" style=\"visibility:hidden;height:1px;width:"+this.pxSeparacionHoja+"px\" />"
                        if (this.imgIconoHoja!=null)  
                           controlador[controlador.length]=["<img src=\""+this.imgIconoHoja+"\" align=absmiddle> "]
                        }
                     }
                  else  {
                     if (this.nodosIndexados[elCod].padre)  {
                        controlador[controlador.length]="<img align=\"absmiddle\" id=\"iconoControladorRama\" src=\""+this.imgCerrarArbol+"\" onmousedown=\"drObjListas.indice."+this.idContenedor+".abreCierraRama('"+DrEscHTML(objFila[idcod])+"'); DrCancelaEventos()\">" //Falta escapar la ' tambi�n TO-DO
                        controlador[controlador.length]="<img align=\"absmiddle\" style=\"visibility:hidden;height:1px;width:"+this.pxSeparacionRama+"px\" />"
                        if (this.imgIconoRama!=null)  
                           controlador[controlador.length]=["<img src=\""+this.imgIconoRama+"\" align=absmiddle> "]
                        }
                     else  {
                        controlador[controlador.length]="<img align=\"absmiddle\" style=\"visibility:hidden;height:1px;width:"+this.pxSeparacionHoja+"px\" />"
                        if (this.imgIconoHoja!=null)  
                           controlador[controlador.length]=["<img src=\""+this.imgIconoHoja+"\" align=absmiddle> "]
                        }
                     }
                  
                  }
               html[html.length]="             <td>"+tabulacion+controlador.join('')+objFila[defCol[c].idColumnaDatos]+"</td>";
               }
            else  {
               var dato=objFila[defCol[c].idColumnaDatos]
               if (dato!=null && dato!="")
                  html[html.length]="             <td>"+dato+"</td>";
               else
                  html[html.length]="             <td>&nbsp;</td>";
               }
            }
         html[html.length]="             <td>&nbsp;</td>";
         html[html.length]="           </tr>";
         }
      }
   else { //Datos si no es arbol
      for (var f=0; f<filas.length; f++)  {
         var objFila=filas[f]
         if (objFila[idcod]!=null)
            html[html.length]="           <tr COD=\""+DrEscHTML(objFila[idcod])+"\">";
         else
            html[html.length]="           <tr>";
         //columnas de datos de una fila
         for (var c=0; c<defCol.length; c++)  {
            var dato=objFila[defCol[c].idColumnaDatos]
            if (dato!=null && dato!="")
               html[html.length]="             <td>"+dato+"</td>";
            else
               html[html.length]="             <td>&nbsp;</td>";
            }
         html[html.length]="             <td>&nbsp;</td>";
         html[html.length]="           </tr>";
         }
      }
   html[html.length]="         </table>";
   html[html.length]="      </DIV>";

   //Cabecera
   html[html.length]="     <DIV id=\""+this.idContenedor+"_CABECERA\" onselectstart=\"DrCancelaEventos();\">";
   html[html.length]="         <table class=\""+this.claseTablaCabecera+"\" id=\""+this.idContenedor+"_TABLA_CABECERA\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
   html[html.length]="           <tr>";
   if (!this.esArbol) {
      for (var i=0; i<this.arr_columnas.length; i++)  {//Si no hay arbol permito ordenar
         html[html.length]="<td indiceColumna=\""+i+"\" width=\""+this.arr_columnas[i].ancho+"\" class=\""+this.arr_columnas[i].claseCabecera+"\" orden=\""+this.arr_columnas[i].orden+"\">"
         //html[html.length]="<div style=width:100% onmousedown=\"if (event.button==1){window.drPrepParaOrdenar=true}else{window.drPrepParaOrdenar=false}\" onmouseup=\"if (window.drPrepParaOrdenar) drObjListas."+this.idContenedor+".ordena("+i+")\">"
         html[html.length]="<div style=width:100%>"
         html[html.length]= this.arr_columnas[i].textoCabecera
         html[html.length]="</div>"
         html[html.length]="</td>";
         }
      }
   else  {
      for (var i=0; i<this.arr_columnas.length; i++)  { //Si hay arbol no permito ordenar
         html[html.length]="<td indiceColumna=\""+i+"\" width=\""+this.arr_columnas[i].ancho+"\" class=\""+this.arr_columnas[i].claseCabecera+"\">"
         html[html.length]="<div style=width:100%>"
         html[html.length]= this.arr_columnas[i].textoCabecera
         html[html.length]="</div>"
         html[html.length]="</td>";
         }
      }
   html[html.length]="             <td class=\""+this.claseCeldaAdicionalCabecera+"\" id=\""+this.idContenedor+"_CELDA_ADIC\">&nbsp;</td>";
   html[html.length]="             <td class=\""+this.claseCeldaScrollCabecera+"\" id=\""+this.idContenedor+"_CELDA_SCR\">&nbsp;</td>";
   html[html.length]="           </tr>";
   html[html.length]="         </table>";
   html[html.length]="      </DIV>";

   //Genero los controles de columna
   for (var i=0; i<this.arr_columnas.length; i++)  {/*No pongo height:100% porque se ponen encima del scroll */
      html[html.length]="      <DIV id=\""+this.idContenedor+"_CTRL"+i+"\" style=\"z-index:200;filter:Alpha(opacity=16);position:absolute;width:8;cursor:col-resize;\" ondblclick=\"drObjListas.indice."+this.idContenedor+".redimensiona("+i+")\" onmousedown=\"agarraControl(this)\" onmouseup=\"sueltaControl()\" onselectstart=\"DrCancelaEventos();\">";
      html[html.length]="         <img style=\"width:100%;height:100%\"/>";
      html[html.length]="      </DIV>";
      }
   //Control de arrastre de columna
   html[html.length]="<DIV id=\""+this.idContenedor+"_CTRL_CAB\" style=\"overflow:hidden; position:absolute;background-color:#999999;filter:Alpha(opacity=30);display:none; border:1px solid black;\"><img style=\"width:1;height:1\"/></DIV>";
   //Marca de punto de inserci�n
   html[html.length]="<DIV id=\""+this.idContenedor+"_MARC_CAB\" style=\"overflow:hidden; position:absolute;background-color:#555555;filter:Alpha(opacity=70);display:none;\"><img style=\"width:1;height:1\"/></DIV>";

   return html.join('')
   }

function f_lista_preparaVariablesCapas()  {
   this.cpCONTENEDOR=dameObjId(this.idContenedor)
   this.cpDATOS=dameObjId(this.idContenedor+'_DATOS')
   this.cpCABECERA=dameObjId(this.idContenedor+'_CABECERA')
   this.tblCABECERA=dameObjId(this.idContenedor+'_TABLA_CABECERA')
   this.tblDATOS=dameObjId(this.idContenedor+'_TABLA_DATOS')
   this.celdaScoll=dameObjId(this.idContenedor+'_CELDA_SCR')
   this.celdaAdicional=dameObjId(this.idContenedor+'_CELDA_ADIC')
   this.columnaAdicional=dameObjId(this.idContenedor+'_COL_ADIC')
   this.controlCabecera=dameObjId(this.idContenedor+'_CTRL_CAB')
   this.marcaCabecera=dameObjId(this.idContenedor+'_MARC_CAB')      
   this.controladores=[]
   for (var i=0; i<this.arr_columnas.length; i++)
      this.controladores[this.controladores.length]=dameObjId(this.idContenedor+'_CTRL'+i)
   this.celdasCab=this.tblCABECERA.cells
   }

function f_lista_preparaCapas()  {
   this.cpDATOS.lista=this
   this.altoCabecera=this.tblCABECERA.offsetHeight
   this.cpCONTENEDOR.style.paddingTop=this.altoCabecera
   this.cpCABECERA.style.height=this.altoCabecera
   }

function f_lista_preparaEventos() {
   this.cpDATOS.onscroll=new Function ("obj","drObjListas.indice."+this.idContenedor+".datos_onscroll();");
   this.cpCONTENEDOR.onmousemove=mueveControl
   this.cpCONTENEDOR.onmouseup=sueltaControl
   this.cpCONTENEDOR.onresize=new Function ("obj","drObjListas.indice."+this.idContenedor+".recoloca();");
   for (var f=0;f<this.controladores.length;f++)   {
      this.controladores[f].alAgarrar=new Function ("obj","lista_marca(obj)");
      this.controladores[f].alSoltar=new Function ("obj","lista_estirar(drObjListas.indice."+this.idContenedor+".celdasCab["+f+"]);lista_estirar(drObjListas.indice."+this.idContenedor+".tblDATOS.getElementsByTagName('COL')["+f+"]);lista_desmarca(obj);drObjListas.indice."+this.idContenedor+".recoloca();");
      }
   //this.cpCABECERA.ondragstart=new Function ("obj","drObjListas.indice."+this.idContenedor+".cabecera_ondragstart();");
   this.cpCABECERA.onmouseover=new Function ("obj","drObjListas.indice."+this.idContenedor+".cabecera_onmouseover();");
   this.cpCABECERA.onmouseout=new Function ("obj","drObjListas.indice."+this.idContenedor+".cabecera_onmouseout();");
   this.cpCABECERA.onmousedown=new Function ("obj","drObjListas.indice."+this.idContenedor+".cabecera_onmousedown();");
   this.cpCABECERA.onmouseup=new Function ("obj","drObjListas.indice."+this.idContenedor+".cabecera_onmouseup();");
   this.cpCABECERA.onmousemove=new Function ("obj","drObjListas.indice."+this.idContenedor+".cabecera_onmousemove();");
   this.cpCABECERA.oncontextmenu=new Function ("obj","drObjListas.indice."+this.idContenedor+".cabecera_oncontextmenu();");
   }

function f_lista_generaHtmlMenuDerechoCabecera()  {
   var html=[]
   html[html.length]='<DIV style="width=1; border:1px solid #D4D0C8; border-bottom: 1px solid #404040; border-right: 1px solid #404040;">'
   html[html.length]='<DIV style="padding:2 px; background-color:#D4D0C8;border:1px solid white; border-bottom: 1px solid #808080; border-right: 1px solid #808080;">'
   html[html.length]='<table width='+this.anchoMinimoMenuDerecho+' style="font-family: Tahoma;font-size: 11px;cursor:pointer;" border="0" cellspacing="0" cellpadding="0">'
   var opciones=this.arr_columnas
   for (var r=0;r<opciones.length; r++)   {
         if (this.esVisibleColumna(r)) {
         html[html.length]='<tr height=17 onmouseover="this.style.background=\''+this.colorRollOverMenuDerecho+'\';" onmouseout="this.style.background=\'\'" onmousedown="with(parent){drObjListas.indice.'+this.idContenedor+'.ocultaColumna('+r+');}parent.oPopup.hide();">'
         html[html.length]='<td nowrap style=padding-left:8px;padding-right:16px>'
         html[html.length]='&#x25CF;&nbsp;&nbsp;'+opciones[r].textoCabecera
         html[html.length]="</td></tr>"
         }
      else  {
         html[html.length]='<tr height=17 onmouseover="this.style.background=\''+this.colorRollOverMenuDerecho+'\';" onmouseout="this.style.background=\'\'" onmousedown="with(parent){drObjListas.indice.'+this.idContenedor+'.muestraColumna('+r+');}parent.oPopup.hide();">'
         html[html.length]='<td nowrap style=padding-left:8px;padding-right:16px>'
         html[html.length]='&#x25CB;&nbsp;&nbsp;'+opciones[r].textoCabecera
         html[html.length]="</td></tr>"
         }
      }
   html[html.length]='</table>'
   html[html.length]="</DIV></DIV>"
   return (html.join(''))
   }

function  f_lista_cabecera_onmouseout()   {
   var elObj=DrDameAntecesorConTag(event.fromElement, 'TD');
   if (elObj!=null && elObj.indiceColumna!=null)   {
      var datosColumna=this.arr_columnas[elObj.indiceColumna]
      if (datosColumna.claseCabeceraResaltada!=null && datosColumna.claseCabecera!=null )  {
         elObj.className=datosColumna.claseCabecera
         }
      }
   }
function  f_lista_cabecera_onmouseover()   {
   var elObj=DrDameAntecesorConTag(event.toElement, 'TD');
   if (elObj.tagName!=null && elObj.indiceColumna!=null)   {
      var datosColumna=this.arr_columnas[elObj.indiceColumna]
      if (datosColumna.claseCabeceraResaltada!=null)  {
         elObj.className=datosColumna.claseCabeceraResaltada
         }
      }
   }

function f_lista_cabecera_oncontextmenu() {
   DrCancelaEventos()
   if (window.oPopup==null)
      window.oPopup = window.createPopup();
   window.xMenuDerecho=event.clientX;
   window.yMenuDerecho=event.clientY;      
   window.oPopBody = oPopup.document.body;
   oPopBody.innerHTML=this.generaHtmlMenuDerechoCabecera()
   oPopBody.rollOver=function (tr)   {
      tr.style.backgroundColor="red"
      }
   oPopup.show(0, 0, 0, 0);
   var realHeightPopup = oPopBody.scrollHeight ;
   var realWidthPopup = oPopBody.scrollWidth ;
   oPopup.hide();
   oPopup.show(event.clientX,event.clientY, realWidthPopup, realHeightPopup, document.body);
   return false;
   }

function  f_lista_cabecera_onmousedown()   {
   if (event.button!=1 )
      return
   var oTd=DrDameAntecesorConTag(event.srcElement, 'TD');
   if (oTd!=null && oTd.indiceColumna!=null) {
      this.preparadoParaDragstart=true
      if (!this.esArbol)
         window.drPrepParaOrdenar=true
      }
   }

function  f_lista_cabecera_onmouseup()   {
   if (event.button!=1)
      return
   var oTd=DrDameAntecesorConTag(event.srcElement, 'TD');
   if (oTd!=null && oTd.indiceColumna!=null) {
      if (window.drPrepParaOrdenar)
         this.ordena(oTd.indiceColumna)
      window.drPrepParaOrdenar=false
      this.preparadoParaDragstart=false
      }
   }



function  f_lista_cabecera_onmousemove()   {
   DrVaciaSeleccion()
   if (this.preparadoParaDragstart) 
      this.cabecera_ondragstart()
   this.preparadoParaDragstart=false
   }

function f_lista_cabecera_ondragstart()   {
   DrCancelaEventos()
   var elObj=DrDameAntecesorConTag(event.srcElement, 'TD');
   if (elObj!=null)  {
      this.controlCabecera.style.display="inline"
      this.controlCabecera.style.left=elObj.offsetLeft - this.cpCABECERA.scrollLeft
      this.controlCabecera.style.width=elObj.offsetWidth
      this.controlCabecera.style.height=elObj.offsetHeight
      this.controlCabecera.style.top=0
      this.controlCabecera.celdaAMover=elObj
      this.controlCabecera.alMover=new Function ("obj","drObjListas.indice."+this.idContenedor+".cabecera_alMoverControl(obj);");
      this.controlCabecera.alSoltar=new Function ("obj","drObjListas.indice."+this.idContenedor+".cabecera_alSoltarControl(obj);");
      agarraControl(this.controlCabecera)
      }
   }

function f_lista_cabecera_alMoverControl(obj)   {
   //obj es el control (la capa "controlCabecera")
   var anchoAcum=0
   var col=0
   //var puntoXUsado=(obj.celdaAMover.offsetLeft + window.distanciaX+(obj.offsetWidth/2)) //La mitad de la caja que muevo
   //var puntoXUsado=event.clientX //La posici�n del cursor
   var puntoXUsado=event.x+this.cpDATOS.scrollLeft
   while (anchoAcum<puntoXUsado && col <this.arr_columnas.length) {
      var elAnteriorAcumulado=anchoAcum 
      if (this.esVisibleColumna(col)) 
         anchoAcum+=this.celdasCab[col].offsetWidth
      col++
      }
   var columnaAnterior=(col>0)?col-1:0
   var elpuntoMedio=  elAnteriorAcumulado + (this.celdasCab[columnaAnterior].offsetWidth / 2)
   var estoyEnMitadAnterior=(elpuntoMedio>puntoXUsado)
   if (estoyEnMitadAnterior)
      col--
   if (col<0)
      col=0
   else if (col>this.arr_columnas.length)
      col=this.arr_columnas.length
   //Ya tengo en COL la posici�n donde deseo insertar esta columna (0 .. n�col)
   this.marcaCabecera.style.display="inline"
   this.marcaCabecera.style.width=4
   this.marcaCabecera.style.height=this.tblCABECERA.offsetHeight
   this.marcaCabecera.style.top=0
   this.marcaCabecera.style.left=((estoyEnMitadAnterior)?elAnteriorAcumulado-2:anchoAcum-2) - this.cpCABECERA.scrollLeft
   //status=window.distanciaX
   obj.columnaDestino=col

   //Restar scroll a lo que se pinta:  (lefts)
   }


function f_lista_cabecera_alSoltarControl(obj)  {
   //alert(obj.columnaDestino)
   this.marcaCabecera.style.display="none"
   this.controlCabecera.style.display="none"
   if (obj.columnaDestino!=null) //Esto para cuando saco menu derecho y pico fuera directamente, se produce un mouse down y mouse move que me lanza un ondragstart y un alSoltar todo seguido
      this.mueveColumna(obj.celdaAMover.indiceColumna,obj.columnaDestino)
   }

function f_lista_preparaParaRegenerar()   {
   //Recojo los anchos y las visibilidades antes de repintar TO-DO: visibilidades
   for (var r=0; r<this.arr_columnas.length; r++)  {
      if (this.esVisibleColumna(r))
         this.arr_columnas[r].ancho=this.celdasCab[r].offsetWidth
      else  {
         this.celdasCab[r].style.display="inline"
         this.arr_columnas[r].ancho=this.celdasCab[r].offsetWidth
         } 
      }
   this.cpDATOS_scrollTop=this.cpDATOS.scrollTop
   this.cpDATOS_scrollLeft=this.cpDATOS.scrollLeft
   //Si es arbol recojo el estado de las ramas
   if (this.esArbol) {
      this.codFilasACerrar=[]
      for (var r=0; r<this.nodos.length; r++)   {
         if (!this.nodosIndexados[this.nodos[r]].abierto)
            this.codFilasACerrar[this.codFilasACerrar.length]=this.nodos[r]
         }
      }
   }

function f_lista_regenera()   {
   this.prepara();
   }

function f_lista_mueveColumna(col,pos)  {
   //alert(col+","+pos)
   //return
   //  0  1  2  3  (posiciones)
   //   C0 C1 C3   (columnas)
   if (pos == col || pos==col+1)
      return
   this.preparaParaRegenerar()
   //y recoloco
   var nuevo_arr_col=[]
   for (var r=0; r<pos; r++)  {
      if (r!=col)
         nuevo_arr_col[nuevo_arr_col.length]=this.arr_columnas[r]
      }
   nuevo_arr_col[nuevo_arr_col.length]=this.arr_columnas[col]
   for (var r=pos; r<this.arr_columnas.length; r++)  {
      if (r!=col)
         nuevo_arr_col[nuevo_arr_col.length]=this.arr_columnas[r]
      }
   this.defLista.arr_columnas=nuevo_arr_col
   this.regenera();
   }



function f_lista_recoloca()  {//Recoloca los Controladores y las columnas especiales
   this.datos_onscroll()
   //this.alCambiarColumnas()
   this.recalculaScroll();
   for (var f=0;f<this.controladores.length;f++)   {
      //Verifico si corresponden a una columna oculta
      //if (this.celdasCab[f].style.display=="none")
      if (! this.esVisibleColumna(f))
         this.controladores[f].style.display="none"
      else  {
         this.controladores[f].style.display=""
         this.controladores[f].style.left=this.celdasCab[f].offsetLeft+this.celdasCab[f].offsetWidth-(this.controladores[f].offsetWidth/2)-this.cpCABECERA.scrollLeft
         this.controladores[f].xSinScroll=this.controladores[f].offsetLeft + this.cpCABECERA.scrollLeft
         this.controladores[f].style.top=this.celdasCab[f].offsetTop
         var alt=(this.celdasCab[f].offsetHeight+this.cpDATOS.clientHeight-3)
         alt=alt<0?0:alt
         this.controladores[f].style.height=alt
         }
      }
   }

function f_lista_recalculaScroll()  {
   /*var SCRV= (this.cpDATOS.clientWidth < this.cpDATOS.offsetWidth)
   var SCRH= (this.cpDATOS.clientHeight < this.cpDATOS.offsetHeight)
   this.celdaScoll.style.display=SCRV? "inline":"none"
   this.celdaAdicional.style.display=!SCRH? "inline":"none"
   this.columnaAdicional.style.display=!SCRH? "inline":"none"*/
   }



function f_lista_ondblclick() {
   if (!this.esArbol || !this.dobleClickAbre)   
      return
   DrVaciaSeleccion()
   var elObj=DrDameAntecesorConTag(event.srcElement, 'TR');
   if (elObj!=null)  
      this.abreCierraRama(elObj.COD)
   }


function f_lista_onmousedown(){
   if (event.button==2)
      window.drPrepParaOrdenar=false
   var elObj=DrDameAntecesorConTag(event.srcElement, 'TR');
   if (elObj.tagName!=null && window.event.button==1)
      this.selecciona(elObj)
   } 

function f_lista_onmouseover(){
   var elObj=DrDameAntecesorConTag(event.toElement, 'TD');
   if (elObj!=null)   {
      this.ultimoTdActivo=elObj;
      elObj.title=(elObj.scrollWidth>elObj.offsetWidth)?elObj.innerText:""
      elObj=DrDameAntecesorConTag(elObj, 'TR');
      if (elObj!=null)
         this.resaltaFila(elObj)
      }
   }   
function f_lista_onmouseout(){
   var elObj=DrDameAntecesorConTag(event.fromElement, 'TR');
   if (elObj.tagName!=null)
      this.quitaResaltadoFila(elObj)
   }


//Relacionado con las filas, multiselecci�n, etc
//Se requiere un CODIGO de fila para que se tenga en cuenta en la selecci�n 

function f_lista_sacaCodigoDeSeleccionados (cod) {
   if (this.codigosSeleccionados[cod]!=null) { //Solo saco un codigo si estaba
      this.codigosSeleccionados[cod]=null
      this.arrayCodigosSeleccionados=DrGeneraArraySinElemento(this.arrayCodigosSeleccionados, cod)
      }
   }

function f_lista_codigoEstaSeleccionado(cod)  { //retorna true o false
	if(this.multiseleccion)
	{
   if (this.codigosSeleccionados==null)
      return false
   if (this.codigosSeleccionados[cod])
      return true
	}
	 else
	{
		 
      if (this.seleccionado!=null && this.seleccionado.COD==cod) 
         return true
	}
   return false
   }

function f_lista_meteCodigoEnSeleccionados (cod) {
   if (this.arrayCodigosSeleccionados==null)   { //La primera vez
      this.codigosSeleccionados={}
      this.arrayCodigosSeleccionados=[]
      }
   if (this.codigosSeleccionados[cod]==null) { //Solo meto un codigo una vez
      this.codigosSeleccionados[cod]=true
      this.arrayCodigosSeleccionados[this.arrayCodigosSeleccionados.length]=cod
      }
   }

function f_lista_dameArrayCodigos ()   {
   var filas=this.arr_filas
   var ret=[]
   for (var r=0; r<filas.length; r++)  {
      var cod=this.arr_filas[r][this.idColumnaDatosCodigo]
      if (cod!=null)
         ret[ret.length]=cod
      }
   return ret
   }

function f_lista_dameArrayCodigosSeleccionados () {
   if (this.multiseleccion) { 
      if (this.arrayCodigosSeleccionados==null)
         return []
      return this.arrayCodigosSeleccionados
      }
   else  {//Selecci�n simple
      if (this.seleccionado!=null) 
         return [this.seleccionado.COD]
      else
         return []
      }
   }

function f_lista_regeneraPropiedadesVisuales()  { 
   //Tras pintar la lista regenera los codigos seleccionados
   var idcod=this.idColumnaDatosCodigo
   if (this.multiseleccion)   {
      for (var r=0; r<this.arr_filas.length; r++)  {
         var objFila=this.arr_filas[r]
         if (objFila[idcod]!=null && this.codigoEstaSeleccionado(objFila[idcod])) {
            //Busco el objeto tr asociado
            var oTr=this.dameTrAsociadoAlCodigo(objFila[idcod])
            this.seleccionaRegenerando(oTr)
            }
         }
      }
   else  {
      if (this.codigoSeleccionado!=null)  {
         var oTr=this.dameTrAsociadoAlCodigo(this.codigoSeleccionado)
         this.seleccionaRegenerando(oTr)
         }
      }
   //Oculto las columnas ocultas
   for (var r=0; r<this.arr_columnas.length; r++)  {
      var col=this.arr_columnas[r]
      if (!this.esVisibleColumna(r))
         this.ocultaColumna(r)
      }

   if (this.esArbol && this.codFilasACerrar!=null) {
      for (var r=0; r<this.codFilasACerrar.length; r++) 
         this.cierraRama(this.codFilasACerrar[r])
      }

   //Y muevo el scroll hasta donde estaba
   if (this.cpDATOS_scrollLeft!=null)
      this.cpDATOS.scrollLeft=this.cpDATOS_scrollLeft
   if (this.cpDATOS_scrollTop!=null)
      this.cpDATOS.scrollTop=this.cpDATOS_scrollTop

   }

function f_lista_seleccionaCodigo(codigo) {
   var oTr=this.dameTrAsociadoAlCodigo(codigo)
   if (oTr!=null)
      this.selecciona(oTr, true);
   }

function f_lista_seleccionaTodos (sinEjecutar) {
   if (!this.multiseleccion)
      return 
   var filas=this.tblDATOS.rows
   for (var r=0; r<filas.length; r++)  {
      var oTr=filas[r]
      if (!oTr.seleccionado)   {//Si la fila estaba marcada
         this.selecciona(oTr,null,sinEjecutar)
         }
      }
   }


function f_lista_seleccionaTodosSinEjecutar () {
   this.seleccionaTodos(true)
   }


function f_lista_deseleccionaTodos () {
   var codSel=this.dameArrayCodigosSeleccionados()
   for (var r=0; r<codSel.length; r++)
      this.seleccionaCodigo(codSel[r])
   }

function f_lista_selecciona(oTr, noVengoDeEventoRaton, noEjecutarFuncion){
   if (oTr.COD==null)
      return
   if (this.multiseleccion)   {
      if (oTr.seleccionado)   {//Si la fila estaba marcada
         oTr.seleccionado=false;
         if (noVengoDeEventoRaton)
            this.quitaResaltadoFila(oTr)
         else
            this.resaltaFila(oTr)
         this.sacaCodigoDeSeleccionados(oTr.COD)
         }
      else {//Si la fila no estaba marcada pero es susceptible de marcar ...
         oTr.seleccionado=true;
         oTr.className=this.claseSeleccionado
         this.meteCodigoEnSeleccionados(oTr.COD)
         }
      }
   else  {
      if (oTr.seleccionado)   {//Si pulso uno seleccionado en monoselecci�n, deselecciono
         oTr.seleccionado=false;
         if (noVengoDeEventoRaton)
            this.quitaResaltadoFila(oTr)
         else
            this.resaltaFila(oTr)
         this.seleccionado=null
         this.codigoSeleccionado=null
         }
      else  { //Si pulso otro en monoselecci�n, cambio el seleccionado
         if (this.seleccionado) { //Deselecciono el anterior
            this.seleccionado.className=""
            this.seleccionado.seleccionado=false;
            }
         this.seleccionado=oTr;
         this.codigoSeleccionado=oTr.COD
         oTr.seleccionado=true;
         oTr.className=this.claseSeleccionado
         }
      }
   if (this.f_usuario!=null && !noEjecutarFuncion)
      this.f_usuario(oTr.COD) //Ejecuto la funci�n del usuario
   }    
   
function f_lista_seleccionaRegenerando(oTr)  {//Solo regenera la parte visual y no ejecuta el javascript
   if (oTr.COD==null)
      return
   if (this.multiseleccion)   {
      oTr.seleccionado=true;
      oTr.className=this.claseSeleccionado
      }
   else  {
      this.seleccionado=oTr;
      oTr.seleccionado=true;
      oTr.className=this.claseSeleccionado
      }
   }

function f_lista_resaltaFila(oTr)   {
   if (!oTr.seleccionado) 
      oTr.className=this.claseResaltado
   }

function f_lista_quitaResaltadoFila(oTr)   {
   if (!oTr.seleccionado)
      oTr.className=""
   }








function f_lista_ordena(col) {//Ojo si vacia
   var arr=[]
   var tipoOrden=this.arr_columnas[col].orden
   if (tipoOrden==null || tipoOrden=="alfanumerico")   {
      for (var f=0;f<this.tblDATOS.rows.length;f++)  {
         var elIdColumnaDatos=this.arr_columnas[col].idColumnaDatos
         arr[f]=[this.arr_filas[f][elIdColumnaDatos],this.arr_filas[f]]
         }
      }
   else  {
      //TO-DO: fecha, numerico, etc
      }
   arr=arr.sort();
   if (this.arr_columnas[col].estaOrdenado) {
      arr=arr.reverse()
      this.arr_columnas[col].estaOrdenado=false
      }
   else
      this.arr_columnas[col].estaOrdenado=true

   var nuevo_Arr_filas=[]
   for (var r=0; r<arr.length; r++)
      nuevo_Arr_filas[nuevo_Arr_filas.length]=arr[r][1]
   this.preparaParaRegenerar()
   this.defLista.arr_filas=nuevo_Arr_filas
   this.regenera()
   /* //Infinitamente mas lento : 
   var arr=[]
   var celdaCab=this.tblCABECERA.rows[0].cells[col];
   switch (celdaCab.orden) {
         //ej:            case "dd-mm-aa": ... break;
      default: //Alfanumerico
         for (var f=0;f<this.tblDATOS.rows.length;f++)  
            arr[f]=[this.tblDATOS.rows[f].cells[col].innerText,this.tblDATOS.rows[f]]
      break;
      }
   arr=arr.sort();
   if (celdaCab.ordenado!=null) {
      arr=arr.reverse()
      celdaCab.ordenado=null
      }
   else  
      celdaCab.ordenado=true
   var anchos=[]
   for (var f=0;f<arr.length;f++)   {
      var ind=arr[f][1].rowIndex
      this.tblDATOS.moveRow(ind, arr.length-1)
      }*/
   }

function f_lista_redimensiona(col) {//Ojo si esta vacia
   var anchoMax=0;
   for (var f=0;f<this.tblDATOS.rows.length;f++)  {
      var anchos=this.tblDATOS.rows[f].cells[col].scrollWidth;
      if (anchoMax<anchos)
         anchoMax=anchos;
      }
   window.distanciaX=anchoMax-this.tblCABECERA.cells[col].offsetWidth
   lista_estirar(this.tblCABECERA.cells[col]);
   lista_estirar(this.tblDATOS.getElementsByTagName('COL')[col])
   this.recoloca();
   }

function f_lista_datos_onscroll() {
   this.cpCABECERA.scrollLeft=this.cpDATOS.scrollLeft
   for (var f=0;f<this.controladores.length;f++)   {
      if (this.controladores[f].xSinScroll!=null)
         this.controladores[f].style.left=this.controladores[f].xSinScroll-this.cpDATOS.scrollLeft
      }
   }
   

function f_lista_ajustaAlto()	{
	this.cpDATOS.style.height=this.cpCONTENEDOR.offsetHeight-this.cpCABECERA.offsetHeight //TO-DO: borde?
	}
	
function f_lista_ocultaColumna (ncol)  {
   this.arr_columnas[ncol].oculta=true
   var laCeldaCab=this.celdasCab[ncol]
   var elObjCOL=this.tblDATOS.getElementsByTagName('COL')[ncol]
   elObjCOL.style.display="none"
   laCeldaCab.style.display="none"
   this.recoloca()
   }

function f_lista_muestraColumna(ncol)  {
   this.arr_columnas[ncol].oculta=false
   var laCeldaCab=this.celdasCab[ncol]
   var elObjCOL=this.tblDATOS.getElementsByTagName('COL')[ncol]
   elObjCOL.style.display="inline"
   laCeldaCab.style.display="inline"
   this.recoloca()
   }

function f_lista_esVisibleColumna (ncol)  {
   //return this.celdasCab[ncol].style.display!="none"
   return !this.arr_columnas[ncol].oculta
   }

function f_lista_dameTrAsociadoAlCodigo(cod) {
   var indice=DrDameIndiceObjetoEnArrayPorPropiedad (this.arr_filas, this.idColumnaDatosCodigo, cod)
   if (indice!=-1)
      return this.tblDATOS.rows[indice]
   else
      return null
   }

// Relacionado con el comportamiento de arbol: 

function clDrNodo(codigo)  {
   this.codigo=codigo
   this.hijos=[]
   this.abierto=true
   this.fila=null
   }

function f_lista_preparaArbol()  {
   //partiendo del array de datos indexa los nodos, calcula las profundidades de cada fila y reordena los datos ocn la jerarquia
   if (!this.esArbol)
      return
   //Primero en una sola pasada indexo todos los nodos 
   var lasFilas=this.arr_filas
   //Vamos a manejar estos tres objetos para controlar el arbol: 
   this.nodosIndexados={} // nodos existentes indexados (inclusive los padres que no existan)
   this.nodosSinPadre=[] //array de nodos iniciales
   this.nodos=[] //array de codigos que existen
   //Hacemos una primera indexacion
   for (var r=0; r<lasFilas.length; r++)  {
      var laFila=lasFilas[r]
      var cod=laFila[this.idColumnaDatosCodigo]
      var codPadre=laFila[this.idColumnaDatosCodigoPadre]
      this.nodos[this.nodos.length]=cod
      if (this.nodosIndexados[cod]==null)
         this.nodosIndexados[cod]=new clDrNodo(cod)
      this.nodosIndexados[cod].fila=laFila
      if (codPadre==null)  {
         this.nodosSinPadre[this.nodosSinPadre.length]=this.nodosIndexados[cod]
         }
      else  {
         if (this.nodosIndexados[codPadre]==null)
            this.nodosIndexados[codPadre]=new clDrNodo(codPadre)
         this.nodosIndexados[codPadre].hijos[this.nodosIndexados[codPadre].hijos.length]=this.nodosIndexados[cod]
         }
      }

   //Ahora busco nodos cuyo padre no exista
   for (var r=0; r<lasFilas.length; r++)  {
      var laFila=lasFilas[r]
      var cod=laFila[this.idColumnaDatosCodigo]
      var codPadre=laFila[this.idColumnaDatosCodigoPadre]
      if (codPadre!=null && this.nodosIndexados[codPadre].fila == null)
         this.nodosSinPadre[this.nodosSinPadre.length]=this.nodosIndexados[cod]
      //aprovecho para clasificar los nodos en padres e hijos
      var elNodo=this.nodosIndexados[cod]
      if (elNodo.hijos.length>0)
         elNodo.padre=true
      else
         elNodo.padre=false
      }


   //Ahora ordeno un nuevo array con las filas colocadas con la jerarquia
   var nuevoArrayFilas=[]
   this.arr_profundidades_filas=[]
   if (this.ordenarPadresPrimero)   {
      for (var r=0; r<this.nodosSinPadre.length; r++) {
         var nodo=this.nodosSinPadre[r]
         if (nodo.padre)  {
            this.arr_profundidades_filas[this.arr_profundidades_filas.length]=0
            nuevoArrayFilas[nuevoArrayFilas.length]=nodo.fila
            this.preparaArbol_recursiva_padresPrimero(1,nuevoArrayFilas,this.arr_profundidades_filas, nodo)
            }
         }
      for (var r=0; r<this.nodosSinPadre.length; r++) {
         var nodo=this.nodosSinPadre[r]
         if (!nodo.padre)  {
            this.arr_profundidades_filas[this.arr_profundidades_filas.length]=0
            nuevoArrayFilas[nuevoArrayFilas.length]=nodo.fila
            this.preparaArbol_recursiva_padresPrimero(1,nuevoArrayFilas,this.arr_profundidades_filas, nodo)
            }
         }
      }
   else {
      for (var r=0; r<this.nodosSinPadre.length; r++) {
         var nodo=this.nodosSinPadre[r]
         this.arr_profundidades_filas[this.arr_profundidades_filas.length]=0
         nuevoArrayFilas[nuevoArrayFilas.length]=nodo.fila
         this.preparaArbol_recursiva(1,nuevoArrayFilas,this.arr_profundidades_filas, nodo)
         }
      }
   this.arr_filas=nuevoArrayFilas
   }

function f_lista_preparaArbol_recursiva(nivel,nuevoArrayFilas,arrayProfundidades, nodoPadre) {
   for (var r=0; r<nodoPadre.hijos.length; r++) {
      var nodo=nodoPadre.hijos[r]
      nuevoArrayFilas[nuevoArrayFilas.length]=nodo.fila
      arrayProfundidades[arrayProfundidades.length]=nivel
      this.preparaArbol_recursiva(nivel+1, nuevoArrayFilas, arrayProfundidades, nodo)
      }
   }

function f_lista_preparaArbol_recursiva_padresPrimero(nivel,nuevoArrayFilas,arrayProfundidades, nodoPadre) {
   for (var r=0; r<nodoPadre.hijos.length; r++) {
      var nodo=nodoPadre.hijos[r]
      if (nodo.padre)  {
         nuevoArrayFilas[nuevoArrayFilas.length]=nodo.fila
         arrayProfundidades[arrayProfundidades.length]=nivel
         this.preparaArbol_recursiva_padresPrimero(nivel+1, nuevoArrayFilas, arrayProfundidades, nodo)
         }
      }
   for (var r=0; r<nodoPadre.hijos.length; r++) {
      var nodo=nodoPadre.hijos[r]
      if (!nodo.padre)  {
         nuevoArrayFilas[nuevoArrayFilas.length]=nodo.fila
         arrayProfundidades[arrayProfundidades.length]=nivel
         this.preparaArbol_recursiva_padresPrimero(nivel+1, nuevoArrayFilas, arrayProfundidades, nodo)
         }
      }
   }

function f_lista_cierraRama(cod) {
   if (!this.nodosIndexados[cod].abierto)
      return
   this.nodosIndexados[cod].abierto=false
   var codigos=this.dameArrayCodigosDescendientesDe(cod)
   for (var r=0; r<codigos.length; r++)
      this.ocultaFilaConCodigo(codigos[r])
   var oImgControlador=this.dameControladorRama(cod)
   if (oImgControlador!=null)
      oImgControlador.src=this.imgAbrirArbol
   }

function f_lista_abreRama(cod) {
   if (this.nodosIndexados[cod].abierto)
      return
   this.nodosIndexados[cod].abierto=true
   var codigos=this.dameArrayCodigosDescendientesVisiblesDe(cod)
   for (var r=0; r<codigos.length; r++)
      this.muestraFilaConCodigo(codigos[r])
   var oImgControlador=this.dameControladorRama(cod)
   if (oImgControlador!=null)
      oImgControlador.src=this.imgCerrarArbol
   }

function f_lista_abreCierraRama(cod)   {
   if (this.nodosIndexados[cod].abierto)
      this.cierraRama(cod)
   else
      this.abreRama(cod)
   }

function f_lista_cierraArbol()   {
   for (var r=0; r<this.nodos.length; r++) 
      this.cierraRama(this.nodos[r])
   }

function f_lista_abreArbol()   {
   for (var r=0; r<this.nodos.length; r++) 
      this.abreRama(this.nodos[r])
   }

function f_lista_dameArrayCodigosDescendientesDe(cod, arrayParaAcumular)   {
   if (arrayParaAcumular==null){
      arrayParaAcumular=[]
      var primeraInvocacion=true
      }
   var hijos=this.nodosIndexados[cod].hijos
   for (var r=0; r<hijos.length; r++) {
      arrayParaAcumular[arrayParaAcumular.length]=hijos[r].codigo
      this.dameArrayCodigosDescendientesDe(hijos[r].codigo, arrayParaAcumular)
      }
   if (primeraInvocacion){
      return arrayParaAcumular
      }
   }

function f_lista_dameArrayCodigosDescendientesVisiblesDe(cod, arrayParaAcumular)   {
   if (arrayParaAcumular==null){
      arrayParaAcumular=[]
      var primeraInvocacion=true
      }
   var hijos=this.nodosIndexados[cod].hijos
   for (var r=0; r<hijos.length; r++) {
      arrayParaAcumular[arrayParaAcumular.length]=hijos[r].codigo
      if (this.nodosIndexados[hijos[r].codigo].abierto)
         this.dameArrayCodigosDescendientesDe(hijos[r].codigo, arrayParaAcumular)
      }
   if (primeraInvocacion){
      return arrayParaAcumular
      }
   }

function f_lista_ocultaFilaConCodigo(cod)  {
   var oTr=this.dameTrAsociadoAlCodigo(cod)
   oTr.style.display="none"
   }

function f_lista_muestraFilaConCodigo(cod)  {
   var oTr=this.dameTrAsociadoAlCodigo(cod)
   oTr.style.display="inline"
   }

function f_lista_dameControladorRama(cod) {
   var oTr=this.dameTrAsociadoAlCodigo(cod)
   if (oTr==null)
      return
   var oImgContr=oTr.all.namedItem('iconoControladorRama')
   if (oImgContr==null)
      return
   if (oImgContr.length!=null)
      return oImgContr[0]
   else
      return oImgContr
   }

function f_lista_limpiarDatos()  {
   this.preparaParaRegenerar()
   this.defLista.arr_filas=[]
   this.regenera()
   }

// **********************************
// Funciones de compatibilidad entre navegadores 

function lista_version(lista) {  //arreglos segun version
   if (clientInformation.appVersion.indexOf("MSIE 5")!=-1) 
      for (var f=0;f<lista.controladores.length;f++)   
         lista.controladores[f].style.cursor="move"
   if (clientInformation.appVersion.indexOf("MSIE 5.0")!=-1) 
      for (var f=0;f<lista.tblDATOS.cells.length;f++)   
         lista.tblDATOS.cells[f].noWrap=true;
   }

// **********************************
//Funciones de arrastrar / soltar

function agarraControl(obj)   {
   window.drPrepParaOrdenar=false //Evita ordenar los datos de la columna si lo que queremos es recolocar su posici�n o cambiar su ancho
   window.distanciaX=0
   window.xRatonInicial=event.clientX
   window.agarrado=obj
   obj.xInicial=obj.offsetLeft;
   if (obj.alAgarrar)
      obj.alAgarrar(obj)
   }
function sueltaControl()   {
   window.drMoviendoControl=false
   if (window.agarrado) {
      if (window.agarrado.alSoltar)
         window.agarrado.alSoltar(window.agarrado)
      window.agarrado=null
      }
   }
function mueveControl() {
   window.drMoviendoControl=true
   window.distanciaX=event.clientX - window.xRatonInicial
   if (window.agarrado!=null) {   
      window.agarrado.style.left=window.agarrado.xInicial + distanciaX
      if (window.agarrado.alMover)
         window.agarrado.alMover(window.agarrado)
      }
   }

// **********************************
//Funciones de utilidad general

function DrEscHTML (cad) {
   if (cad!=null)
      return cad.replace(/&/gi,"&amp;").replace(/</gi,"&lt;").replace(/"/gi,"&quot;"); 
   return "";
   }

function DrCancelaEventos()    {
    window.event.cancelBubble=true;
    window.event.returnValue=false;
    return false;
    }

function DrVaciaSeleccion()   {
   document.selection.empty();
   }

function dameObjId(id) {
   return document.getElementById(id);   
   }

function DrGeneraArraySinElemento(elArray, elm) {
   var ret=[]
   for (var r=0; r<elArray.length; r++)   
      if (elArray[r]!=elm)
         ret[ret.length]=elArray[r]
   return ret
   }

function DrDameIndiceObjetoEnArrayPorPropiedad (array, propiedad, valor)   {
   for (var r=0; r<array.length; r++)  
      if (array[r][propiedad]==valor)
         return r
   return -1      
   }

function DrDameAntecesorConTag(obj, tag) { //Navega en el DOM
   while (obj.tagName!=tag && obj.parentElement)
      obj=obj.parentElement
   if (obj.tagName==tag)   
      return obj
   return null
   }

// **********************************
//Relacionado con los "arrastradores":

function lista_marca(obj)  { 
   obj.style.borderLeft='1px solid black';
   obj.style.borderRight='1px solid black';
   obj.oldWidth=obj.offsetWidth;
   obj.style.width='3px'
   }

function lista_desmarca(obj)  {
   obj.style.borderLeft='none';
   obj.style.borderRight='none';
   obj.style.width=obj.oldWidth
   }

function lista_estirar(obj)   {
   if (obj.offsetWidth + window.distanciaX>1)
      obj.style.width=obj.offsetWidth + window.distanciaX + "px"
   else
      obj.style.width=2
   }

// ********************************
// Generaci�n din�mica de los estilos

function DrGeneraCssContenedorLista(idContenedor)  {
   css=[]
   css[css.length]="#"+idContenedor+" {height:100%; width:100%; overflow:hidden;position:relative;}"
   css[css.length]="#"+idContenedor+"_CABECERA {position:absolute;overflow:hidden; left:0px; top:0px;  width:100%;background-color:#CCCCCC;}"
   css[css.length]="#"+idContenedor+"_TABLA_CABECERA {table-layout:fixed;width:100%}"
   css[css.length]="#"+idContenedor+"_TABLA_CABECERA DIV, #TABLA_CABECERA TD {overflow: hidden;text-overflow:ellipsis;white-space:nowrap}"
   css[css.length]="#"+idContenedor+"_DATOS { height:100%; width:100%; overflow:auto}"
   css[css.length]="#"+idContenedor+"_TABLA_DATOS {width:100%;table-layout:fixed;}"
   css[css.length]="#"+idContenedor+"_TABLA_DATOS TD {width:100%;overflow: hidden;text-overflow:ellipsis;white-space:nowrap}"
   css[css.length]="#"+idContenedor+"_CELDA_SCR {width:16px;}"
   document.write("<STYLE>"+css.join('')+"</STYLE>")
   }