	var opcMenuIcono=[
	   {"txt": drResources.get({bundle:"core",key:"icons.txt1"}), "js":"eliminaIcono();"}
	   ,{}
   	,{"txt": drResources.get({bundle:"core",key:"icons.txt2"}), "js":"drOrganizarIconos()"}     
	   ]

	var snapIconos=30

	var drObjPadreIconos = window.top.f1

	function generarLosIconos()   {
		registrarLosIconosIniciales() 
		regenerarLosIconos('divGrupoIconos');
		}
	
	function regenerarLosIconos(idContenedor)	{
		regenerarElHtmlDeLosIconos(idContenedor);
		inicializarLosIconos();	
		}
		
	function registrarLosIconosIniciales ()   {
		if (oDesktopConfig.margSupAnt!=null)	{
			var diferenciaVertical= drDesktop.getTopMargin({skin:oDesktopConfig.tema}) - oDesktopConfig.margSupAnt
			for (var r=0; r<oIconosIniciales.length; r++)
				oIconosIniciales[r].y+=diferenciaVertical
			}
		for (var r=0; r<oIconosIniciales.length; r++){
			var oObjIco=oIconosIniciales[r]
			var id=dameIdIconoQueNoColisione()
			oObjIco.id=id;
			if (oObjIco.application!=null)	
				registraIconoAplicacion(id,oObjIco.application,oObjIco.x,oObjIco.y)
			else if (oObjIco.file!=null)
				registraIconoFichero(id,oObjIco.file,oObjIco.x,oObjIco.y)
			}
		}
	
   function noHayIconos()  {
      var propSkin=drWindow.getSkinParameters();
      if (propSkin!=null)  {
         var noIcons=drEvaluaBooleanoString(propSkin.noIcons)
         if (noIcons)
            return true
      }
   }
   
   function regenerarElHtmlDeLosIconos(idContenedorIconos)   {
      if (noHayIconos())
         return 
		var hmtlAGenerar=[]
		var losIconos=DruidaIconos();
		for (var r=0; r<losIconos.length; r++){
			var oObjIco=losIconos[r]
			if (oObjIco.application!=null)	
				hmtlAGenerar[hmtlAGenerar.length]=generaHtmlIconoAplicacion(oObjIco.id,oObjIco.application, oObjIco.x,oObjIco.y);
			else if (oObjIco.file!=null)
				hmtlAGenerar[hmtlAGenerar.length]=generaHtmlIconoFichero(oObjIco.id,oObjIco.file, oObjIco.x,oObjIco.y);
			}
		document.getElementById(idContenedorIconos).innerHTML=hmtlAGenerar.join('')
		}		
		
	function inicializarLosIconos()	{
      if (noHayIconos())
         return 
		var losIconos=DruidaIconos();
		for (var r=0; r<losIconos.length; r++){
			var oObjIco=losIconos[r]
			if (oObjIco.application!=null)	{
				inicializaIcono(oObjIco.id, oObjIco.application, oObjIco.x, oObjIco.y)
				}
			else if (oObjIco.file!=null){
				inicializaIconoFichero(oObjIco.id, oObjIco.file, oObjIco.x, oObjIco.y)
				}
			}
		}

	function dameIdIconoQueNoColisione()	{
		var nuevoId="drIco"+Math.round(Math.random(33)*100000000)
		while (drObjPadreIconos[nuevoId]!=null)
			nuevoId="drIco"+Math.round(Math.random(33)*100000000)
		return nuevoId
		}

	function inicializaIcono(id,idAplicacion,x,y)	{
		var oIcono=drObjPadreIconos.oIconos.dameIcono(id)
		var elObj=oIcono.dameObj()
		elObj.idIcono=id;
		hazArrastrableObjeto(elObj);
		if (window.asociaMenuDerecho!=null)	{ 		//Asocio men� derecho
			asociaMenuDerecho(elObj, opcMenuIcono)
			} 
		}
	
	function inicializaIconoFichero(id,rutaFichero,x,y)	{
		var oIcono=drObjPadreIconos.oIconos.dameIcono(id)
		var elObj=oIcono.dameObj()
		elObj.idIcono=id;
		hazArrastrableObjeto(elObj);
		if (window.asociaMenuDerecho!=null)	{ 		//Asocio men� derecho
			asociaMenuDerecho(elObj, opcMenuIcono)
			}
		}
		
	function generaNuevoIcono(idAplicacion,x,y)	{
		actualizaDatosIconos()
		registraIconoAplicacion(dameIdIconoQueNoColisione(),idAplicacion,x,y)
		regenerarLosIconos('divGrupoIconos');
		}
		
	function eliminaIcono()	{
		//Elimino el objeto desktop de control
		drObjPadreIconos.oIconos.saca(objMenuDerecho.idIcono);		
		//Elimino el objeto del DOM
      $(objMenuDerecho).remove()
		}		

	function hazArrastrableObjeto (elObj)	{
      $(elObj).bind("mousedown",mouseDownIcono).bind("selectstart", function(){return false})
   }

   function mouseDownIcono(e)  {
      e.preventDefault() 
      var $target=$(this)
      var eventData={oPageX:e.pageX, oPageY:e.pageY, $target:$target, oPosition:$target.position()}
      $(document).unbind("mouseup",mouseUpBodyDraggingIcon).unbind("mousemove",mouseMoveBodyDraggingIcon).bind("mouseup", eventData, mouseUpBodyDraggingIcon).bind("mousemove", eventData, mouseMoveBodyDraggingIcon)
      $target.css('zIndex',4)
      return false
   }
   function mouseMoveBodyDraggingIcon(e){
      e.preventDefault()
      var eData=e.data
      var left=eData.oPosition.left-eData.oPageX + e.pageX
      var top=eData.oPosition.top-eData.oPageY + e.pageY
      eData.$target.css({left: left+"px" ,top:top+"px"})
   }
   function mouseUpBodyDraggingIcon(e){
      e.preventDefault()
      var eData=e.data
      $(document).unbind("mouseup", mouseUpBodyDraggingIcon ).unbind("mousemove", mouseMoveBodyDraggingIcon )
      eData.$target.css('zIndex',3)
   }


   function getIconTooltip(oAplicacion,idAplicacion){
      if (oAplicacion.description!=null)
         return oAplicacion.description
      if (oAplicacion.title!=null)
         return drResources.replaceI18NValues(oAplicacion.title)
      if (oAplicacion.iconText!=null)
         return oAplicacion.iconText
      return idAplicacion
   }

   function getIconText(oAplicacion,idAplicacion){
      if (oAplicacion.iconText!=null)
         return oAplicacion.iconText
      if (oAplicacion.title!=null)
         return drResources.replaceI18NValues(oAplicacion.title)
      if (oAplicacion.description!=null)
         return oAplicacion.description
      return idAplicacion
   }

	function generaHtmlIconoAplicacion(id,idAplicacion,x,y)	{
      var oAplicacion=drWindow.getApplicationsConfig()[idAplicacion]
		if (oAplicacion!=null)	{
			var ancho=85
			var alto=79
			var img=oAplicacion.icon
			var texto=getIconText(oAplicacion,idAplicacion)
			var tooltip=getIconTooltip(oAplicacion,idAplicacion)
			var js="drAbreAplicacion('"+idAplicacion+"');" //TO-DO: escapar correctamente
			}
		else	{
			var ancho=85
			var alto=79
			var img="aucun.gif"
			var texto=idAplicacion
			var tooltip=""
			var js="alert('"+drResources.get({bundle:"core",key:"icons.txt3"}) + " "+idAplicacion+" "+drResources.get({bundle:"core",key:"icons.txt4"})+"');" //TO-DO: escapar correctamente		
			}
		return generaHtmlIcono(id,x,y,ancho,alto,img,texto,tooltip,js)
		}


	function generaDatosIconoFichero(fichero)	{
		var ret={}
		var extension=drExtraeExtension(fichero);
		if (oTipos[extension]==null)	{
			ret.img="aucun.gif"
			ret.texto=fichero
			ret.tooltip=""
			ret.js="alert('" + drResources.get({bundle:"core",key:"icons.txt5"}) + " "+extension+".');" 	
			}
		else {
			var idAplicacion=oTipos[extension].aplicacion
			var oAplicacion=oAplicaciones[idAplicacion]
			if (oAplicacion!=null)	{
				ret.texto = fichero
				ret.img=oAplicacion.icon
				ret.tooltip=(fichero)
				ret.js="drAbreFichero('"+fichero+"');" 
				}
			else	{
				ret.img="aucun.gif"
				ret.texto=fichero
				ret.tooltip=""
				ret.js="alert('"+drResources.get({bundle:"core",key:"icons.txt3"}) + " "+idAplicacion+" " + drResources.get({bundle:"core",key:"icons.txt6"}) + " "+extension+" " + drResources.get({bundle:"core",key:"icons.txt7"}) + "');" //TO-DO: escapar correctamente		
				}		
			}
		return ret
		}

	function generaHtmlIconoFichero(id,rutaFichero,x,y)	{
		var dat=generaDatosIconoFichero(rutaFichero)
		return generaHtmlIcono(id,x,y,85,79,dat.img,dat.texto,dat.tooltip,dat.js)
		}

	function drOrganizarIconosBuscaHueco(cuadricula, YCuadricula, XCuadricula,maxY,maxX)	{
		if (YCuadricula>=maxY)	//TO-DO: esto es mejorable
			YCuadricula=0;
		if (XCuadricula>=maxX)			
			XCuadricula=0;	
		if (cuadricula[YCuadricula]==null)	
			cuadricula[YCuadricula]=[]	
		var desisto=false; 
		while (cuadricula[YCuadricula][XCuadricula]!=null || desisto)	{
			XCuadricula++
			if (XCuadricula>maxX)	{
				XCuadricula=0;
				YCuadricula++;
				}
			if (YCuadricula>=maxY && XCuadricula>=maxX)	{
				YCuadricula=maxY;
				XCuadricula=maxX;
				desisto=true
				}
			}
		cuadricula[YCuadricula][XCuadricula]=true	
		return {x:XCuadricula,y:YCuadricula}
		}


	function drOrganizarIconos()	{
		var maxY=(Math.floor((document.body.offsetHeight-30) / 80))
      var minY=drDesktop.getTopMargin({skin:oDesktopConfig.tema})
		var maxX=(Math.floor(document.body.offsetWidth / 70)-1)
		var iconos=DruidaIconos();
		//busco el icono mas alto y los pongo todos justo bajo la cabecera
		var alturaMenor=8000
		for (var r=0; r<iconos.length; r++)	{
         var pageYIcon=iconos[r].get$icon().position().top
         if (pageYIcon<alturaMenor)
				alturaMenor=pageYIcon
		}
		var cuadricula=[]	
		for (var r=0; r<iconos.length; r++)	{
			var $icon=iconos[r].get$icon()
         $icon.css({ top: $icon.position().top-alturaMenor +"px"})
         var offset=$icon.offset();
			var XCuadricula=Math.round((offset.left)/70)	
			var YCuadricula=Math.round((offset.top-20)/80)
			var objPosicionXY=drOrganizarIconosBuscaHueco(cuadricula, YCuadricula, XCuadricula,maxY,maxX)
			var nuevoX=objPosicionXY.x*70
			var nuevoY=(objPosicionXY.y*80)+20	
         $icon.css({ left:nuevoX+"px", top:(nuevoY+minY)+"px"})
			iconos[r].x=nuevoX
			iconos[r].y=nuevoY+minY
		}
	}

	function registraIcono(id,idAplicacion,rutaFichero,x,y)	{		
		if (drObjPadreIconos.oIconos==null)
			drObjPadreIconos.oIconos=new clDrIconos()
		var elIcono=new clDrIcono(id,idAplicacion,rutaFichero,x,y)
		drObjPadreIconos.oIconos.mete(elIcono)
		return elIcono;
		}
	
	function registraIconoAplicacion(id,idAplicacion,x,y)	{		
		return registraIcono(id,idAplicacion,null,x,y);
		}
		
	function registraIconoFichero(id,rutaFichero,x,y)	{		
		return registraIcono(id,null,rutaFichero,x,y);
		}		

   function generaHtmlIcono(id,x,y,ancho,alto,img,texto,tooltip,js)  {
      var datosSkin=drWindow.getSkinData()
      if (datosSkin && datosSkin.getHtmlIcons)
         return datosSkin.getHtmlIcons({id:id, x:x, y:y, width:ancho, height:alto, imgSrc:urlRaizIconos()+'/'+img, label: texto, tooltip:tooltip, js:js})
      else{
         var ht=[]
         ht[ht.length]='<DIV ID="drIcn_'+id+'" style="cursor:pointer;Z-INDEX: 3; LEFT: '+x+'px; VISIBILITY: visible; WIDTH: '+ancho+'px; POSITION: absolute; TOP: '+y+'px; HEIGHT: '+alto+'px" ondblclick="'+DrEscHTML(js)+'">'
         ht[ht.length]='<FONT class="TexteIcon">'
         ht[ht.length]='<CENTER>'
         ht[ht.length]='<IMG SRC="'+urlRaizIconos()+'/'+img+'" ALT="'+tooltip+'" class="Transparant_niveau0" ID="" name="" >'
         ht[ht.length]='	<BR/>'
         ht[ht.length]='	<TABLE border="0" cellPadding="0" class="TexteIcon0" ID="" name="" width="'+ancho+'">'
         ht[ht.length]='		<TBODY>'
         ht[ht.length]='			<TR><TD align="center">'
         ht[ht.length]='					<DIV ID="" name="">'+texto+'</DIV>'
         ht[ht.length]='			</TD></TR>'
         ht[ht.length]='		<TBODY>'
         ht[ht.length]='	</TABLE>'
         ht[ht.length]='</CENTER>'
         ht[ht.length]='</FONT>'
         ht[ht.length]='</DIV>'
         return ht.join('');
      }
   }

	/*
   function inicializaIcono(ID) {
	   if (drObjPadreIconos.oIconos==null)
	   	return
      with (drObjPadreIconos.oIconos.dameIcono(ID).dameCbe()) {
         //moveTo('center');
         //zIndex(1);
         show();
         addEventListener('drag');
         addEventListener('dragStart', onDragIconStart, false);
         addEventListener('dragEnd', onDragIconEnd, false);
         }
		//Asocio men� derecho
		if (window.asociaMenuDerecho!=null)	{
			elIconoObj=drObjPadreIconos.oIconos.dameIcono(ID).dameObj()
			asociaMenuDerecho(elIconoObj, opcMenuIcono)
			}         
      }*/
      
	function DruidaIconos()	{ //Retorna un array de objetos icono
		if (drObjPadreIconos.oIconos==null)
			return []
		return drObjPadreIconos.oIconos.dameLista();
		}      
	 
	function actualizaDatosIconos()	{
		var iconos=DruidaIconos();
		for (var r=0; r<iconos.length; r++)	{
         var offset=iconos[r].get$icon().offset()
			iconos[r].x=offset.left
			iconos[r].y=offset.top
			}
		}
	 
	function cierraSesionIconos(){
      if (!noHayIconos())
      	actualizaDatosIconos()
		document.fcerrar.iconos.value=objetoAJSONString(DruidaIconos())
		//alert(objetoAJSONString(iconos))
		} 
		

//  ************************ OBJETOS
		
function clDrIconos()	{//clase del Objeto controlador de los iconos

	/* - */
	this.lIconos=[]
	
	/* + mete(obj) */
	this.mete=function (obj)	{
		var i=this.indiceDe(obj.id)
		if (i==-1)
			this.lIconos[this.lIconos.length]=obj
		else
			this.lIconos[i]=obj
		}

	/* + saca(id) */
	this.saca=function (id)	{
		var nuevoArr=[];
		for (var r=0;r<this.lIconos.length; r++)
			if (this.lIconos[r].id!=id)
				nuevoArr[nuevoArr.length]=this.lIconos[r]
		this.lIconos=nuevoArr	
		}
		
	this.indiceDe=function (id)	{
		for (var r=0;r<this.lIconos.length;r++)	
			if (this.lIconos[r].id == id)
				return r
		return -1
		}
		
	/* + dameLista() */
	this.dameLista=function ()	{
		return this.lIconos
		}

	/* + dameIcono() */
	this.dameIcono=function (id)	{
		var i=this.indiceDe(id)
		if (i!==-1)
			return this.lIconos[i]
		}
	}
	
function clDrIcono(id,idAplicacion,rutaFichero,x,y)	{
	//Solo ha de ser distinto de null uno, idAplicacion o rutaFichero
	this.id=id
	this.application=idAplicacion
	this.file=rutaFichero	
	this.x=x
	this.y=y
   //this.$icon=null

	/*this.dameCbe=function()	{
		return this.dameObj().cbe
	}*/	
   this.get$icon=function(){
      //if (this.$icon==null)
      //   this.$icon=$("#drIcn_"+this.id)
      //return this.$icon
      return $("#drIcn_"+this.id)
   }
	this.dameObj=function()	{
		//return document.getElementById("drIcn_"+this.id)
      return $("#drIcn_"+this.id).get(0)
   }									
}	
	
function urlRaizIconos()	{
	return oDesktopProperties.urlIcons
	}

function urlIconoPorDefecto()	{
	return oDesktopProperties.urlIcons+"/php.gif"
}
	