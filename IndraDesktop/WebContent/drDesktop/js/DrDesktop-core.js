

function createInitialWindows() {  //[REP] deberia usar aqui un indice de ventanas �?
   for (var i=0; i<=drDesktop.maxWindows; i++){
      var $winDiv=drDesktop.get$Win({index:i})
      drDesktop.refreshContainerData(i)
      $winDiv.css({"visibility":"hidden", clip:"rect(0px auto auto 0px)", left:"0px" ,top:"0px"}).width(0).height(0)
      if (i>2)
         $winDiv.bind("mousedown",drListeners.mouseDownVentana).bind("selectstart", function(){return false})
      if (i>1)
         $winDiv.bind("mouseup",drListeners.mouseUpVentana)
      
   }
   drDesktop.init({skin:oDesktopConfig.tema})
   drDesktop_onload();
}

$(createInitialWindows)


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++       ALIAS de xDTwin y xDT
//Objeto de ALIAS para mantener la compatibilidad del c�digo basado en el antiguo core

//xDTwin=new xDTwinAlias()
/*
function xDTwinAlias(){

   //M�todos usados exteriormente: ----------------------------------------------
   this.marginTop=function (skin){ return drDesktop.getTopMargin({skin:skin}) }
   this.marginBottom=function (skin){ return drDesktop.getBottomMargin({skin:skin}) }
   //this.property=function (windowId, propName, propValue){ return drDesktop.windowData({id:windowId, property:propName, value:propValue}) }

   //M�todos internos (se podr�an eliminar al final) ----------------------------------------------
   //this.addSkin=function(wskin,mTop,mBottom){ return drDesktop.addSkin({skin:wskin, topMargin:mTop, bottomMargin:mBottom}) }
   this.skin=function(wskin,wName,parametros){ return drDesktop.getHtmlChrome({skin:wskin, id:wName, parameters:parametros}) }

   this.property=function(windowName,wProp,wVal){ return drDesktop.wData({id:windowName, property:wProp, value: wVal}) }
   this.getWindowIndex=function(windowName){ return drDesktop.getWindowIndex({id:windowName}) }
   this.getAllWindows=function(index){ return drDesktop.getAllWindowId({index:index}) }
   this.wLength=function(){ return drDesktop.windowLength() }
   this.wHandle=function(index,param,val){ return drDesktop.setWindowData({index:index, property:param, value: val}) }
   this.wName=function(index){ return drDesktop.getWindowId({index:index}) }
   this.newWindow=function(windowName){ return drDesktop.newWindow({id:windowName}) }
   this.window= function(windowName){ return drDesktop.getWindowCbeData({id:windowName}) } //[REP] quitar
   this.windowalert=function(windowName){ return drDesktop.getWindowCbeData({id:windowName, showError:true}) }
   this.deleteWindowProperties=function(index){ return drDesktop.deleteWindowProperties({index:index}) }
   this.setProperty=function(windowName,wProp,wVal){ return drDesktop.setWindowData({id:windowName, property:wProp, value: wVal}) }
   this.setWindowHandle=function(i){ return drDesktop.refreshContainerData(i) }   
   this.lastWindow = function (mwin) { return drDesktop.maxWindowsData(mwin) }
   this.xDTdrag = function _xDTdrag() { return drDesktop.getNewZIndex() }
   this.noevent=function(noEv) { return drDesktop.getSetNoEvent(noEv) }
   this.initFunction=null;//Funcion de inicializacion
   this.setStartup=function(func) { this.initFunction = func }
   this.Startup=function() { this.initFunction() }
}*/

/*
function xDTAlias(){
   //for (a in _xDT)
   //   this[a]=_xDT[a]

   this.addSkin= function (wskin,mTop,mBottom) {  drDesktop.addSkin({skin:wskin, topMargin:mTop, bottomMargin:mBottom})  }  
   this.setSkin= function(wName,wSkin,parametros) { drDesktop.createWindowStructure({id:wName, skin:wSkin, parameters:parametros}) }
   this.prop = function(wName,wProp,wVal) { return drDesktop.wData({id:wName, property:wProp, value: wVal}) }
   this.cbe=function (wName) { return drDesktop.getWindowCbeData({id:wName, showError:true}) }
   this.url=function(wName,wUrl,wScroll){ drDesktop.loadUrl({id:wName, url:wUrl, scroll:wScroll})  }
   this.isHidden=function(wName){ return drDesktop.isWindowHiddenMoved({id:wName}) }
   this.hideWindow=function(wName){ return drDesktop.hideWindowMoving({id:wName}) }
   this.showWindow=function(wName,noReposition){ return drDesktop.showWindowMoving({id:wName, noReposition:noReposition}) }
   this.moveWindowToFront=function(args){ drDesktop.bringWindowToFront({id:args.wName, zIndex:args.zIndex})  }
   this.moveToFrontWindow=function(args){ drDesktop.bringWindowToFront({id:args.wName, zIndex:args.zIndex})  }
   this.ocultaCapaParaIframe=function(windowId){ drDesktop.hideIfrDragDiv({id:windowId})  }
   this.creaCapaParaIframe=function(windowId){ drDesktop.createIfrDragDiv({id:windowId})  }
   this.innerWindows=function(wName,stat) { drDesktop.setStatusIfrDragDivs({id:wName, status:stat}) }
   this.minimizeWindow=function (wName) { drDesktop.minimizeWindow({id:wName}) }
   this.maximizeWindow=function (wName) { drDesktop.maximizeWindow({id:wName}) }
   this.addWindow=function(wName,wTitle,wWidth,wHeight,wPos,wSkin,wParams,wNoTaskbar, wNoHide, wIgnoreMargins,wWidget,wZIndex, wNoResize,wNoDrag){ drDesktop.addWindow({id:wName, title:wTitle, width:wWidth, height:wHeight, position:wPos, skin:wSkin, parameters:wParams, noTaskbar:wNoTaskbar, noHide:wNoHide, noResize:wNoResize, noDrag:wNoDrag, ignoreMargins:wIgnoreMargins, widget:wWidget, zIndex:wZIndex}) }
   this.SetWindowPos=function (wName,wPos) { drDesktop.setWindowPosition({id:wName, position:wPos}) }
   this.deleteWindow=function (wName,wC) { drDesktop.removeWindow({id:wName, confirmed:wC}) }
   this.hide=function (wName) { drDesktop.hideWindow({id:wName}) } 
   this.show=function (wName) { drDesktop.showWindow({id:wName}) } 
   this.moveWindow=function(wTitle) { drDesktop.createMoveBox({label:wTitle}) }
   this.taskbar=function(rWin) { drDesktop.taskbar({id:rWin}) } 
   this.dSkin=function() {return drDesktop.skin }
   this.desktop={
      init:function(wSkin){ drDesktop.init({skin:wSkin})  }
      ,skin:function(wSkin){ drDesktop.init_applySkin({skin:wSkin})  }
   }
   this.positionWindow=function (wName){ drDesktop.repositionWindow({id:wName})}
   this.positionAllWindows=function (){ drDesktop.repositionAllWindows() }
   this.arrangeAllWindows=function(x,y,xoffset,yoffset,wName){ drDesktop.arrangeAllWindows({left:x,top:y,offsetLeft:xoffset,offsetTop:yoffset,ids:wName}) }
   this.window={
      title:function(wName,wTitle){ drDesktop.setGetTitle({id:wName,title:wTitle}) }
      ,onClose:function(wName,fcl){ drDesktop.setRemoveWindowHandler({id:wName, handler:fcl}) }
   }
}*/

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ drDesktop OBJECT

drDesktop=new DrDesktop()
function DrDesktop(args){

   //Atributos ----------------------------------------------------------------]]]
   this.skinsData={} //Registrar� los skins aqu� (objetos de tipo SkinData )
   this.cList=new ContainerList()
   this.noEvent=false;
   this.skin=null
   this.initialSkin=null
   this.maxWindows=30
   this.maxWindowsInc=50
   this.basicWPropArray=["wName","wTitle","wWidthOrg","wWidth","wHeightOrg","wHeight","wPos","wX","wY","wSkin","wIcon","wUrl","wScroll","zIndex","wStat","fClose","wVisible"]
   //this.firstWindowNames=["dDesktop","dTaskbar","dMove","dMessage","dSound","dUser","","","","dDummy","dCustom1","dCustom2","dCustom3","dCustom4","dCustom5"]
   this.firstWindowNames=["dDesktop","dTaskbar","dMove"]
   this.lastReservedWin=this.firstWindowNames.length;
   this.lastZIndex=100;
   this.validIdRgEx=/^[a-zA-Z][a-zA-Z0-9_]+$/
   this.minWinWidth=220
   this.minWinHeight=235

   $.extend(this, args); //Tomo los par�metros que me pasen

   //Constructor, esto hace que new DrDesktop tenga que ser llamado en TIEMPO DE CARGA
   //Son los contenedores iniciales para las ventanas
   document.write('<div id="drWin_0" class="drWinContainer"></div>');
   document.write('<div id="drWin_1" class="drTaskBar taskbarButtonsContainer"></div>');
   for (var i=2;i<=this.maxWindows;i++)
      document.write('<div id="drWin_' + i + '" class="drWinContainer"></div>');

   //M�todos ----------------------------------------------------------------]]]

   //Inicializacion ---------------------------------------------------------------------------------------------------------

   this.init=function(args){
      args = $.extend({},args);
      var skin=args.skin
      if (skin)
         this.skin=skin
      $(window).bind('resize',drListeners.desktopResize)
      this.init_applySkin(args)
      this.initialSkin=this.skin
   }

   this.init_applySkin=function(args){
      args = $.extend({},args);
      var skin=args.skin
      if (skin)
         this.skin=skin
      initSkinFunction=window["desktop_"+this.skin]
      initSkinFunction()
      this.taskbar_render()
      this.initialSkin=skin
   }

   //get / set de propiedades -----------------------------------------------------------------------------------------------------------

   this.maxWindowsData=function(maxw){
      if (maxw!=null)
         this.maxWindows = maxw;
      return this.maxWindows 
   }
   this.getSetNoEvent=function(noEvent){
      if (noEvent == true || noEvent == false)
         this.noEvent=noEvent
      return this.noEvent 
   }
   this.getNewZIndex=function(){  return this.lastZIndex++  }

   this.setRemoveWindowHandler=function(args){ // {id, handler[string/function]}
      var id=args.id, handler=args.handler
      if (id && (typeof(handler) == 'string' || typeof(handler) == 'function')) 
         this.wData({id:id, property:"fClose", value:handler})
   }

   //Operando con ventanas: CREACION (creacion, carga de url) ---------------------------------------------------------------------------


   this.open=function (args, openerWindow){// {..., window, ...}
      //Valores por defecto
      var fParams=$.extend({id:dameIdQueNoColisione(),width:300,height:200}, args)  
      fParams=open_preprocessArguments(fParams, openerWindow) //Preproceso los argumentos
      //drWindow.getRegistry().mete(fParams); //Registro la ventana (antes de abrir para que al abrir los datos ya esten ya que se usan para la barra de tareas)
      //Abro la ventana
      var wOpenArgs=$.extend({},fParams)
      wOpenArgs.parameters=fParams
      var win=open_addWindow(wOpenArgs)
      //RemoveHandler
      //drDesktop.setRemoveWindowHandler({id:fParams.id, handler:"open_unregisterWindow(id);var result=open_confirmWindowClose(id);result"})
      drDesktop.setRemoveWindowHandler({id:fParams.id, handler:"var result=open_confirmWindowClose(id);result"})
      //Cargo la URL
      if (fParams.widget)
         drDesktop.loadUrl({id:fParams.id, url:fParams.url, scroll:"no"})
      else
         drDesktop.loadUrl({id:fParams.id, url:fParams.url})
      //testigo
      elIFRAME=this.getNativeIframe({"id":fParams.id, window:openerWindow})
      elIFRAME.drIdVentana=fParams.id
      open_processBaton(elIFRAME,fParams) 
      if (fParams.urlAyuda!=null)	
         this.wData({id:fParams.id, property:"drUrlInfo", value:fParams.urlAyuda})
      this.showWindow({id:fParams.id})
      this.setStatus({id: fParams.id, status: fParams.status, window:openerWindow})
      //Proceso los daemons
      open_create_daemons(fParams, openerWindow)
      //Creo la capa que tapar� el iframe
      if (window.iframeDragDivMode) {      
         this.createIfrDragDiv({id:fParams.id})
         this.hideIfrDragDiv({id:fParams.id})
      }
      //Mando la ventana a su desktop virtual y la focalizo
      this.setVirtualDesktop({id:fParams.id, virtualDesktop:fParams.virtualDesktop, window:openerWindow});
      this.focus({id:fParams.id, noZIndexChange:true, zIndex:fParams.zIndex}) //No cambio el zIndex porque la funcion ya lo hace
      return fParams.id
   }

   function open_create_daemons(fParams, openerWindow){
      if (fParams.daemons!=null) {
         if (isString(fParams.daemons))   
            demonios=JSONStringAObjeto(fParams.daemons)
         else
            demonios=fParams.daemons
         for (var r=0; r<demonios.length; r++) {
            var datosDaemon=demonios[r]
            datosDaemon.id=fParams.id
            datosDaemon.window=openerWindow;
            drDesktop.createDaemon(datosDaemon);
         }
      }
   }

   function open_preprocessArguments(fParams,openerWindow){
      var skinData=drDesktop.getSkinData()
      //Preproceso "parameter" si lo hay (facilidad para la consola)
      if (fParams.parameter!=null)  {
         if (!isArray(fParams.parameter)) //caso -parameter aaa=bbb --> "aaa=bbb"
            fParams.parameter=[fParams.parameter]
         //ahora proceso cada par�metro  // -parameter aaa=bbb -parameter ccc=ddd --> ["aaa=bbb","ccc=ddd"]
         var ret=[]
         for (r=0; r<fParams.parameter.length; r++)   {
            var componentes=fParams.parameter[r].replace(/(^\s+\b)|(\b\s+$)/gi,"") 
            componentes=componentes.split(/\b\s*=\s*\b/gi)
            ret[ret.length]={name:componentes[0], value:componentes[1]}
         }
         fParams.parameters=ret
      }
      //Aplicamos los parametros a la URL
      if (fParams.parameters!=null && fParams.parameters.length>0)  {
         if (isString(fParams.parameters))   
            fParams.parameters=JSONStringAObjeto(fParams.parameters)
         if (fParams.url.indexOf("?")==-1)
            fParams.url=fParams.url+"?"
         else
            fParams.url=fParams.url+"&"
         var noSoyPrimero=false
         for (var r=0;r<fParams.parameters.length;r++)	{
            if (noSoyPrimero)
               fParams.url=fParams.url+"&"
            noSoyPrimero=true
            if (fParams.parameters[r].nombre!=null)
               fParams.url=fParams.url+escape(fParams.parameters[r].nombre)+"="+escape(fParams.parameters[r].valor)
            else
               fParams.url=fParams.url+escape(fParams.parameters[r].name)+"="+escape(fParams.parameters[r].value)
            }
         }
      //Calculo ancho y alto
      fParams.parameters=null //Ya est�n aplicados a la URL
      if (fParams.innerWidth!=null && skinData.windowChromeWidth!=null) {
         fParams.width=drCalculaValorPixeles(fParams.innerWidth) + skinData.windowChromeWidth
      } else   {
         fParams.width=drCalculaValorPixeles(fParams.width)
      }
      if (fParams.innerHeight!=null && skinData.windowChromeHeight!=null) {
         fParams.height=drCalculaValorPixeles(fParams.innerHeight) + skinData.windowChromeHeight
      } else   {
         fParams.height=drCalculaValorPixeles(fParams.height)
      }
      if (drWindow.getGlobalBoolean({name:"adjustWindowSizeOnOpen"})){
         var $win=$(window)
         var anchoMax= $win.width()
         var altoMax= $win.height()- drWindow.getTopMargin() - drWindow.getBottomMargin()
         if (fParams.width>anchoMax)
            fParams.width=anchoMax
         if (fParams.height>altoMax)
            fParams.height=altoMax
      }
      fParams.position=drCalculaPosicion(fParams.position,fParams.height,fParams.width,fParams.ignoreMargins)
      //Preproceso otros par�metros
      if (fParams.idPadre==null)
         fParams.idPadre=fParams.id
      fParams.widget=drEvaluaBooleanoString(fParams.widget)
      fParams.noTaskbar=drEvaluaBooleanoString(fParams.noTaskbar)
      fParams.noResize=drEvaluaBooleanoString(fParams.noResize)
      fParams.noDrag=drEvaluaBooleanoString(fParams.noDrag)
      fParams.noHide=drEvaluaBooleanoString(fParams.noHide)
      if (fParams.sessionSave!=null)
         fParams.sessionSave=drEvaluaBooleanoString(fParams.sessionSave)
      else
         fParams.sessionSave=true
      fParams.showInAllDesktops=drEvaluaBooleanoString(fParams.showInAllDesktops)
      if (fParams.widget)  {
         fParams.noTaskbar=true
         fParams.noHide=true
         }
      if (fParams.virtualDesktop==null)  {
         var currentVD=drDesktop.getCurrentVirtualDesktop({window:openerWindow}) //]
         if (currentVD==null)
            fParams.virtualDesktop=0
         else
            fParams.virtualDesktop=currentVD
         }
      fParams.showInAllDesktops=drEvaluaBooleanoString(fParams.showInAllDesktops)
      if (fParams.title!=null){
         fParams.sessionSaveTitle=fParams.title
         fParams.title=drResources.replaceI18NValues(fParams.title)
      }
      var virtualDesktop=fParams.virtualDesktop
      return fParams
   }


   function open_unregisterWindow(id)	{
      //drWindow.getDesktopWindow().oRegistroVentanas.saca(id);
      drDesktop.wData({id:id, property:"wOpenArgs", value: null})
      return true;
   }

   function open_confirmWindowClose(id)	{
      //var elWindow=drWindow.getNativeIframe({"id":id}).contentWindow
      var elWindow=drDesktop.getNativeIframe({window:window, "id":id}).contentWindow
      if (elWindow.DrDesktopOnWindowClose!=null &&  typeof elWindow.DrDesktopOnWindowClose == 'function')
         return elWindow.DrDesktopOnWindowClose()
      else
         return true
   }

   function open_processBaton(elIFRAME,fParams)  {
      if (fParams.testigo!=null)	{ //Preparo el paso del testigo
         elIFRAME.testigo=fParams.testigo
         elIFRAME.onreadystatechange=function(){
            if (this.readyState=="complete"){
               var elWindow=this.contentWindow
               if (elWindow.DrDesktopTomaTestigo!=null &&  typeof elWindow.DrDesktopTomaTestigo == 'function')	{
                  elWindow.DrDesktopTomaTestigo(this.testigo)
               }
            }
         }
      } else if (fParams.testigoJson!=null)	{ //Preparo el paso del testigo
         elIFRAME.testigoJson=fParams.testigoJson
         elIFRAME.onreadystatechange=function(){
            if (this.readyState=="complete"){
               var elWindow=this.contentWindow
               if (elWindow.DrDesktopTomaTestigo!=null &&  typeof elWindow.DrDesktopTomaTestigo == 'function')	{
                  elWindow.DrDesktopTomaTestigo(JSONStringAObjeto(this.testigoJson))
               }
            }
         }
      }
   }

   function open_addWindow(args){ // {id title width height position skin parameters noTaskbar noHide noResize noDrag ignoreMargins widget zIndex}
      var id=args.id, title=args.title, width=args.width, height=args.height, position=args.position, skin=args.skin, parameters=args.parameters, zIndex=args.zIndex
      var noTaskbar=args.noTaskbar, noResize=args.noResize, noDrag=args.noDrag, ignoreMargins=args.ignoreMargins, widget=args.widget //, noHide=args.noHide
      if (id) {
         if (!id.match(drDesktop.validIdRgEx)){
            alert('Invalid window ID: ' + id); 
            return 0 
         }
      } else 
         id="w"
      var origId=id
      if (! id.match(/^(dMessage|dUser|dSound|dCustom\d+)$/) )
         id=drDesktop.newWindow({id:id})
      if (id == "_REUSED_WIN_") { //reused window
         drDesktop.wData({id:origId, property:"wStat", value: "OK"})
         drDesktop.taskbar_btnClick({id:origId})
         return origId 
      }
      //drWindow.getRegistry().mete(parameters); //Registro la ventana (antes de abrir para que al abrir los datos ya esten ya que se usan para la barra de tareas)
      //drDesktop.wData({id:id, property:"wOpenArgs", value: parameters})
      if (!id)
         return 0;
      //Preproceso las propiedades
      skin=(!skin)?drDesktop.skin:skin; //[REP]
      position=(!position)?"center":position
      title=(!title)?" ":title;
      if (typeof(width) == 'string')
         width=parseInt(width)
      if (typeof(height) == 'string')
         width=parseInt(height)
      var anchoMinimo=drDesktop.minWinWidth
      var altoMinimo= drDesktop.minWinHeight
      var datosSkin=drDesktop.getSkinData() //[REP]
      if (datosSkin){
         if (datosSkin.minimunWindowWidth)
            anchoMinimo=datosSkin.minimunWindowWidth
         if (!$.browser.mozilla && datosSkin.minimunWindowHeight)
            altoMinimo=datosSkin.minimunWindowHeight
      }
      drDesktop.setWDataArray(id, [
         ["wSkin", skin],["wWidth", width],["wHeight", height],["wOpenArgs", parameters]//,["wExtraParams", parameters]
         ,["wIgnoreMargins", ignoreMargins],["wWidget", widget],["wTitle", title]
         ,["wWidthOrg", anchoMinimo],["wHeightOrg", altoMinimo]
         ,["wNoTaskbar", noTaskbar],["wNoResize", noResize],["wNoDrag", noDrag],["zIndex", zIndex]//,["wNoHide", noHide]
         ,["fixedZIndex",(zIndex!=null)?true:false]
      ])
      //Preparamos la ventana
      drDesktop.get$Win({"id":id}).css({backgroundColor:"transparent"}).rszTo({width:width, height:height})
      drDesktop.setWindowPosition({id:id,position:position});
      drDesktop.createWindowStructure({id:id, skin:skin, parameters:parameters})
      drDesktop.bringWindowToFront({id:id, zIndex:zIndex})
      drDesktop.taskbar_render()
      return id
   }

   function callRemoveWindowHandler(id,wC) {
      var wName=id;
      if (wC)
         return true;
      var f = drDesktop.wData({id:id, property:"fClose"})
      if ((typeof f) == 'function')
         return f(id);
      if ((typeof f) == 'string' && f.length)
         return eval(f);
      return true;
   }

   this.removeWindow=function (args){  //{id, confirmed}
      var id=args.id, confirmed=args.confirmed
      try{
         var cont=callRemoveWindowHandler(id,confirmed)
      }catch(e){
         confirmed=true;
      }
      if (this.cList.windowExists(id)&&(cont||confirmed)) {
         drDesktop.loadUrl({id:id, url:'about:blank'})
         this.hideWindow(args)
         this.get$Win(args).html("")
         this.deleteWindowProperties(args)
 		   drDesktop.taskbar_render()
 		}

   }

   this.createWindowStructure=function(args){ //{id, skin, parameters})
      var id=args.id, skin=args.skin //, parameters=args.parameters; 
      this.get$Win({"id":id}).html( drDesktop.getHtmlChrome(args) )
      //tomo el contenedor del iframe y creo un controlador CBE
      /*var ele = document.getElementById(id + 'iTD')                 
      if (ele) { 
         var cbe = new CrossBrowserElement();  //[-REP, quitar]
         cbeBindElement(cbe,ele);
         document.cbe.appendNode(cbe);
         drDesktop.wData({id:id, property:"innercbe", value: cbe})//[+REP, quitar]
      }*/
      //tomo el contenedor del iframe y creo un controlador JQ
      var JQIfrCont=$("#"+id+'iTD')
      drDesktop.wData({id:id, property:"JQIfrCont", value: JQIfrCont})
   }

   this.loadUrl=function(args) { //{id, url, scroll}
      var id=args.id, url=args.url, scroll=args.scroll;
      if (! this.cList.windowExists(id) ) {
         alert("Unknown window name: " + id + " !");
         return 
      }
      if (!url)
         url = this.wData({id:id, property:"wUrl"})
      scroll=(scroll=='no'||scroll==0||scroll==false)?"no":"auto"
      this.wData({id:id, property:"JQIfrCont"}).html('<iframe allowtransparency="true" id="drIfr_'+id+'" name="drIfr_'+id+'" src="'+url+'" width="100%" height="100%"  marginwidth="0" marginheight="0" frameborder="0" scrolling="'+scroll+'">')
      if ($.browser.msie) 
         $("#drIfr_"+id).attr({src:url, width:"100%", height:"100%"})
      this.wData({id:id, property:"wUrl", value:url})
      this.wData({id:id, property:"wScroll", value:scroll})
   }

   function isArray(a)  {
      return isObject(a) && a.constructor.toString() == Array.toString();   //Esto es para que no falle si los arrays estan en distintas ventanas
   }

   function isString(a)  {
      return typeof a == 'string';
   }

   function drCalculaValorPixeles(valor)	{
      var oDesktopConfig=drWindow.getDesktopConfig()
      //Para convertir valores como "clientWidth-200"
      if (valor==null || valor=="")
         return valor
      var valorPI=parseInt(valor)
      if (!isNaN(valorPI))
         return valorPI
      var clientWidth=drWindow.getDesktopWindow().document.body.clientWidth
      var clientHeight=drWindow.getDesktopWindow().document.body.clientHeight- drWindow.getDrDesktopObj().getTopMargin({skin:oDesktopConfig.tema}) - drWindow.getDrDesktopObj().getBottomMargin({skin:oDesktopConfig.tema})
      eval("valor=("+valor+")")
      return valor;
   }

   function drCalculaPosicion(posicion,alto,ancho,ignorarMargenes)	{
      //Si viene algo tipo center,** o **,center o center,center reconstruyo, si no lo dejo como estaba ...
      //Tambi�n modifica el "y" para que tenga en cuenta el margen superior
      // TO-DO: contemplar a�adir el margen en las otras opciones (n,s,etc)

      var oDesktopConfig=drWindow.getDesktopConfig()
      var ignorarMargenes=drEvaluaBooleanoString(ignorarMargenes)
      if (ignorarMargenes) {
         var marginTop=0
         var marginBottom=0
         }
      else  {
         var marginTop=drWindow.getTopMargin(); 
         var marginBottom=drWindow.getBottomMargin(); 
         }
      if (posicion==null)
         return
      if (posicion=='center')
         posicion='center,center'
      var hayCentradoHorizontal=(posicion.search( /center\s*,/ )!=-1);
      var hayCentradoVertical=(posicion.search( /,\s*center/ )!=-1);
      var hayDosPartes=(posicion.indexOf(',')!=-1)
      if (!hayDosPartes)
         return posicion
      var posX, posY;
      if (hayCentradoHorizontal){
         posX=Math.round( (drWindow.getDesktopWindow().document.body.clientWidth-ancho)/2 )
         if (posX<0)
            posX=0
      }else{
         posX=posicion.substr(0,posicion.indexOf(','))
      }
      if (hayCentradoVertical){
         posY=Math.round((drWindow.getDesktopWindow().document.body.clientHeight-alto-marginTop-marginBottom)/2)+marginTop
         if (posY<marginTop)
            posY=marginTop
      }else{
         posY=posicion.substr(posicion.indexOf(',')+1)
         if (!isNaN(parseInt(posY)))	
            posY=parseInt(posY) + marginTop    		//Le sumo el margen superior.
      }
      return posX+","+posY
      }

   function drEvaluaBooleanoString(valor)   {
      if (valor==null)
         return false
      if (valor==true)
         return true
      else if (valor==false)
         return false
      else  {
         var cad=(valor+"").toLowerCase()
         if (cad=="true")
            return true
         else
            return false
      }
   }

   //Operando con ventanas: MANIPULACION (minimizar, maximizar, mover, ocultar, mover delante, etc) -----------------------------------------------------------------------------

   this.setWindowPosition=function(args){ //{id, position}
      var id=args.id, position=args.position
      if (position.indexOf(",")!=-1){// r15,b10 o 15,20 
         position.match(/^([^,]+),([^,]+)$/)
         var l=RegExp.$1, t=RegExp.$2
         this.get$Win({"id":id}).mvTo({left:l, top:t})
         this.setWDataArray(id,[["wX",l], ["wY",t]])
      } else if (position.match(/(.+)\:([B|T|L|R])\:([-+]?\d+)$/)){
         var parentId=RegExp.$1, rPosit=RegExp.$2, offset=RegExp.$3
         if (!this.cList.windowExists(parentId)) {
            alert("Unknown Window: " + parentId);
            var $win=this.get$Win({id:id})
            $win.mvTo({left:"center"})
            var posit=$win.position()
            this.setWDataArray(id, [["wX", posit.left],["wY", posit.top]])
            return
         }
         var pLeft=parseInt(this.wData({id:parentId, property:"wX"}))
         var pTop=parseInt(this.wData({id:parentId, property:"wY"}))
         if (rPosit == "B") {
            var left=pLeft
            var top=pTop + this.wData({id:parentId, property:"wHeight"}) +  parseInt(offset)
         } else if (rPosit == "T") {
            var left=pLeft
            var top=pTop - this.wData({id:parentId, property:"wHeight"}) -  parseInt(offset)
         } else if (rPosit == "L") {
            var left=pLeft - this.wData({id:parentId, property:"wWidth"}) -  parseInt(offset)
            var top=pTop
         } else if (rPosit == "R") {
            var left=pLeft + this.wData({id:parentId, property:"wWidth"}) +  parseInt(offset)
            var top=pTop
         } 
         this.setWDataArray(id, [["wX", left],["wY", top]])
         this.get$Win({id:id}).mvTo({left:left, top:top})

      } else   { //"center"
      //debugger
         var $win=this.get$Win({id:id})
         this.get$Win({id:id}).mvTo({left:position})
         var wPosition=$win.position()
         this.setWDataArray(id,[["wX",wPosition.left], ["wY",wPosition.top], ["wPos",position]])
         this.repositionWindow({id:id})
      }
   }

   this.repositionWindow=function (args){ //{id}
      var id=args.id
      var estaMaximizada=this.wData({id:id, property:"wStat"})=="MAX"
      var ignorarMargenes=drEvaluaBooleanoString(this.wData({id:id, property:"wIgnoreMargins"}))
      if (estaMaximizada)  {
         var margenInferior=this.getBottomMargin({skin:this.skin})
         var margenSuperior=this.getTopMargin({skin:this.skin})
         $win=$(window)
         var altoMaximo=$win.height()-margenInferior-margenSuperior
         var anchoMaximo=$win.width()
         altoMaximo=aplicaMaximo(altoMaximo,0)
         anchoMaximo=aplicaMaximo(anchoMaximo,0)
         this.get$Win({id:id}).rszTo({width:anchoMaximo, height:altoMaximo})
      } else if (!this.isWindowHiddenMoved({id:id})) {   //Si est� oculta No la muevo (No se puede mover una ventana que se ha ocultado con "_hideWindow").
         var coordenadas={
            x:this.wData({id:id, property:"wX"}), y:this.wData({id:id, property:"wY"})
            ,w:this.wData({id:id, property:"wWidth"}), h:this.wData({id:id, property:"wHeight"})
         }
         if (!ignorarMargenes)
            var coordenadas=calculaPosicionCorrectaVentana(coordenadas)
         this.get$Win({id:id}).mvTo({left:coordenadas.x, top:coordenadas.y})
         this.wData({id:id, property:"wX",value:coordenadas.x})
         this.wData({id:id, property:"wY",value:coordenadas.y})
      }
   }
   function aplicaMaximo(valor, minimo) { //M�todo privado
      if (valor<minimo)
         return minimo
      return valor
   }

   this.repositionAllWindows=function (){ 
      var win=this.getAllWindowId({index:this.lastReservedWin})
      if( win != 0 ) 
         for (var a in win) 
            this.repositionWindow({id:win[a]})
   }


   function calculaPosicionCorrectaVentana(objCoordenadas)  { //M�todo auxiliar. objCoordenadas={x: xx, y:xx, w: xx, h:xx}
      $win=$(window)
      var altoVisible=$win.height()
      var anchoVisible=$win.width()
      var margenInferior=drDesktop.getBottomMargin({skin:drDesktop.skin})
      var margenSuperior=drDesktop.getTopMargin({skin:drDesktop.skin})
      var altoCabeceraVentana=25;
      if (window["oDatos_"+drDesktop.skin]!=null && window["oDatos_"+drDesktop.skin].altoCabeceraVentana!=null)   
         altoCabeceraVentana=window["oDatos_"+drDesktop.skin].altoCabeceraVentana
      objCoordenadas.y=aplicaMinimo(objCoordenadas.y, altoVisible-altoCabeceraVentana-margenInferior)
      objCoordenadas.y=aplicaMaximo(objCoordenadas.y, margenSuperior)
      objCoordenadas.x=aplicaMinimo(objCoordenadas.x, anchoVisible-altoCabeceraVentana)
      objCoordenadas.x=aplicaMaximo(objCoordenadas.x, altoCabeceraVentana-objCoordenadas.w )
      return objCoordenadas
   }

   this.minimizeWindow=function(args) { //{id}
      var id=args.id
      if (this.wData({id:id, property:"wStat"}) == "MAX") {//Si est� maximizada la desmaximizo primero
         this.maximizeWindow(args)
         this.wData({id:id, property:"wStat", value:"min-max"})
      } else {
         this.wData({id:id, property:"wStat", value:"min"})
      }
      this.hideWindowMoving(args);
      drDesktop.taskbar_render() //[REP]
   }

   this.maximizeWindow=function(args) { //{id} //alterna estado de maximizado
      var id=args.id
      if (this.cList.getIndex(id) < this.lastReservedWin)
         return;
      var wstatus=this.wData({id:id, property:"wStat"})
      if (wstatus=="MAX"){ //RECUPERAR, boton maximizar extando maximizado o doble click cabecera --> recupero estado anterior
         //debugger
         var $win=this.get$Win({"id":id})
         $win.rszTo({width: this.wData({id:id, property:"wWidth"}), height: this.wData({id:id, property:"wHeight"}) })
         $win.mvTo({left: this.wData({id:id, property:"wX"}), top:this.wData({id:id, property:"wY"}) })
         this.wData({id:id, property:"wStat", value:"OK"})
      } else  { //MAXIMIZAR
         var $global=$(window)
         var anchoMaximizado=$global.width() - 1
         var altoMaximizado=$global.height() - this.getBottomMargin({skin:this.skin}) - this.getTopMargin({skin:this.skin}) - 1
         var xMaximizado=0
         var yMaximizado=this.getTopMargin({skin:this.skin})
         var $win=this.get$Win({"id":id})
         $win.mvTo({left: xMaximizado, top:yMaximizado})
         $win.rszTo({width: anchoMaximizado, height:altoMaximizado})
         this.focus({id:id})
         this.wData({id:id, property:"wStat", value:"MAX"})
      }
   }


   this.focus=function(params) { //{id,[noZIndexChange],[zIndex]}
      var id=params.id, noZIndexChange=params.noZIndexChange, zIndex=params.zIndex;
      if (!this.cList.windowExists(id))
         return false
      if (!noZIndexChange)
         this.bringWindowToFront({id:id, zIndex:zIndex})
      if (this.hasFocus({id:id}))
         return id
      var skinData=this.getSkinData()
      if (window.drDesktop_windowIdWithFocus!=null)  {
         if (skinData && skinData.onWindowChangeFocusStatus!=null && this.cList.windowExists(window.drDesktop_windowIdWithFocus) ) { //hago el blur si existe la ventana todavia 
            skinData.onWindowChangeFocusStatus({blur:true, id:window.drDesktop_windowIdWithFocus})
         }
      }
      window.drDesktop_windowIdWithFocus=id
      if (skinData && skinData.onWindowChangeFocusStatus!=null)      {
         skinData.onWindowChangeFocusStatus({focus:true, id:id})
      }
      return id;
   }


   this.blur=function() {
      if (window.drDesktop_windowIdWithFocus!=null)  {
         var skinData=this.getSkinData()
         if (skinData && skinData.onWindowChangeFocusStatus!=null && this.cList.windowExists(window.drDesktop_windowIdWithFocus)) { //hago el blur si existe la ventana todavia 
            skinData.onWindowChangeFocusStatus({blur:true, id:window.drDesktop_windowIdWithFocus})
         }
         window.drDesktop_windowIdWithFocus=null
      }
   }

   this.hasFocus=function(params) {//{id}
      var id=params.id
      if (window.drDesktop_windowIdWithFocus==id)  
         return true
      return false
   }

   this.setGetTitle=function(args){
      var id=args.id, title=args.title
      if (id=="undefined" || !this.cList.windowExists(id) )
         return 0
      if (typeof(title) != 'undefined'){
         this.wData({id:id, property:"wTitle", value:title})
         $("#drTit_"+id).html(title)
         return title
      }
      return this.wData({id:id, property:"wTitle"})

   }

   this.hideWindowMoving=function(args)   { //{id}  Semejante a hide pero esta mueve tambi�n la ventana (fix)
      var id=args.id
      if (this.isWindowHiddenMoved({id:id}))
         return
      this.wData({id:id, property:"wMHidden", value:true})
      //drDesktop.get$Win({id:id}).css({"visibility":"hidden"}).mvBy({left:-10000})
      drDesktop.get$Win({id:id}).mvBy({left:-10000})
      if (this.hasFocus({id:id})) 
         this.blur()
   }

   this.showWindowMoving=function(args){ //{id, noReposition[opt]}  
      var id=args.id, noReposition=args.noReposition
      if (!this.isWindowHiddenMoved({id:id}))
         return
      this.wData({id:id, property:"wMHidden", value:false})
      drDesktop.get$Win({id:id}).css({"visibility":"inherit"}).mvBy({left: 10000})
      if (!noReposition)
         this.repositionWindow({id:id})
   }

   this.isWindowHiddenMoved=function(args) {   //{id}
      if (this.wData({id:args.id, property:"wMHidden"})==true)
         return true
      return false
   }


   this.hide=function (params)  { //Hides a window considering taskbar
      if (params==null)
         params={}
      var id=params.id
      if (this.wData({id:id, property:"wTotallyHidden"})==true) //wTotallyHidden no sale en la barra de tareas , HiddenMoved si sale
         return
      this.wData({id:id, property:"wTotallyHidden", value:true})
      if (!this.isWindowHiddenMoved({id:id})){
         this.hideWindowMoving({id:id})
         this.taskbar_render()
      }
   }

   this.show=function (params)  {//Shows a window considering taskbar
      if (params==null)
         params={}
      var id=params.id
      var wTotallyHidden=this.wData({id:id, property:"wTotallyHidden"})
      if (wTotallyHidden==false || wTotallyHidden ==null)
         return
      this.wData({id:id, property:"wTotallyHidden", value:false})
      if (this.isWindowHiddenMoved({id:id})){
         var wStat=this.wData({id:id, property:"wStat"})
         if( wStat !="min" && wStat !="min-max" )
            this.showWindowMoving({id:id})
         this.taskbar_render()
      }
   }


   this.hideWindow=function(args){
      var id=args.id
      if(!this.cList.windowExists(id))
         return
      this.wData({id:id, property:"wVisible", value:false})
      this.get$Win({id:id}).css({visibility:'hidden'})
      return true
   }

   this.showWindow=function(args){ //{id}  
      var id=args.id
      if(!this.cList.windowExists(id))
         return
      this.wData({id:id, property:"wVisible", value:true})
      this.get$Win({id:id}).css({visibility:'inherit'})
      return true
   }

   this.bringWindowToFront=function(args){ //{id, zIndex[opt]}
      var id=args.id, zIndex=args.zIndex;
      var fixedZIndex=this.wData({id:id, property:"fixedZIndex"})
      if (fixedZIndex && zIndex==null)
         return
      if (zIndex==null)
         zIndex=this.getNewZIndex()
      this.get$Win({id:id}).css({zIndex:zIndex})
      this.wData({id:id, property:"zIndex", value: zIndex})
   }

   this.arrangeAllWindows=function(args){ //{left, top, offsetLeft, offsetTop, ids}
      args=$.extend( {left:20, top:20, offsetLeft:20, offsetTop:20} ,args)
      var left=args.left, top=args.top, offsetLeft=args.offsetLeft, offsetTop=args.offsetTop, ids=args.ids
      if (ids)
         ids= ','+ids+','
      var win=drWindow.getWindowIds({noWidgets:true,onlyCurrentDesktop:true})
      var wLeft=left, wTop= top + this.getTopMargin({skin:this.skin})
      for (var i=0; i<win.length; i++){
         var id=win[i]
         if (this.wData({id:id, property:"wStat"}) == "MAX")
            continue
         if (ids && ids.indexOf(',' + id + ',') > -1 )
            continue
         this.bringWindowToFront({id:id}) 
         this.get$Win({id:id}).mvTo({left:wLeft,top:wTop}).rszTo({width:this.wData({id:id, property:"wWidth"}), height:this.wData({id:id, property:"wHeight"})})
         this.showWindow({id:id})
         this.setWDataArray(id,[["wX",wLeft], ["wY",wTop], ["wStat","OK"]])
         wTop += offsetTop;
         wLeft= (offsetLeft > 0) ? wLeft + offsetLeft : wLeft;
      }
      if (win.length>0)
         this.focus({id:win[win.length-1]})
      this.taskbar_render();
   }

   this.hideIfrDragDiv=function(args){ //{id} Aux
      var id=args.id;
      this.wData({id:id, property:"JQIfrCont"}).children("DIV#drIfrBlock"+id).css({display:"none"})
   }

   this.createIfrDragDiv=function(args) {
      var id=args.id;
      var $contenedor=this.wData({id:id, property:"JQIfrCont"})
      var $capaOcultarIframe=$contenedor.children("DIV#drIfrBlock"+id)
      if ($capaOcultarIframe.length==0)   {
         var $iframe=$contenedor.children("IFRAME")
         var $nuevaCapa=$("<div id=\"drIfrBlock"+id+"\" class=\"iframeDraggingDiv\" style=\"background-color:transparent; position:absolute;left:0px;top:0px;\"><TABLE height=\"100%\" width=\"100%\"><TR><TD></TD></TR></TABLE></div>")
         $nuevaCapa.width($iframe.width()).height($iframe.height())
         $contenedor.css({position:"relative"})
         $iframe.before($nuevaCapa)
      } else   {
         $capaOcultarIframe.css({display:"inline"})
      }
   }

   this.setStatusIfrDragDivs=function(args)  { //{id, status["hide"/"show"]}
      var status=args.status, id=args.id;
      var ventanasAModificar=drWindow.getWindowIds({noWidgets:false,onlyCurrentDesktop:true})
      if (status == "hide") {
         for (var r=0; r<ventanasAModificar.length; r++) {
            var windowId=ventanasAModificar[r]
            if (windowId == id)
               continue;
            this.createIfrDragDiv({id:windowId})
         }
      } else {
         for (var r=0; r<ventanasAModificar.length; r++) {
            var windowId=ventanasAModificar[r]
            if (windowId != id){
               this.hideIfrDragDiv({id:windowId})
            }
         }
      }
   }

   this.createMoveBox=function (args) { //{label}
      var label=args.label
      this.get$Win({id:"dMove"}).html('<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%"><tr><td class="drWinMoveWindow" align="center" valign="middle">' + label + '</td></tr></table>')
   }

   this.getStatus=function(params)   {
      var params=$.extend({},params)
      if (params.id==null)
         params.id=this.searchId({window:params.window})
      var wStat=this.wData({id:params.id, property:"wStat"})
      if (wStat=="min" || wStat=="min-max" )
         return "minimized"
      if (wStat=="MAX")
         return "maximized"
   }

   this.setStatus=function(params)   {
      var params=$.extend({},params)
      if (params.id==null)
         params.id=this.searchId({window:params.window})
      if (params.status=="maximized") {
         this.maximizeWindow({id:params.id})
         return 
      }
      if (params.status=="minimized") {
         this.minimizeWindow({id:params.id})
         return 
      }
      var status=this.getStatus(params)
      if (status=="minimized") {
         this.taskbar_btnClick({id:params.id})
         return 
      }
   }

   //Relacionado con ventanas: DAEMONS ------------------------------------------------------------------------------------



   this.getDaemonRegistry=function(args) { //{window}
      var ww=this.getDesktopWindow({window:args.window})
      if (ww.oDaemons==null)	
         ww.oDaemons=new clDrRegistroDaemons()
      return ww.oDaemons
   }

   this.killDaemons=function (args) {//{[id],window}
      var args=$.extend({},args)
      var id=args.id
      if (id==null)
         id=this.searchId({window:args.window})
      var arrayDaemons=this.getDaemonRegistry({window:args.window}).saca({"idVentana":id}) //Elimino el daemon
      var ww=this.getDesktopWindow({window:args.window})
      if (arrayDaemons!=null) //Si es igual a null tendriamos un proceso erroneo ...
         for (var r=0; r<arrayDaemons.length; r++) 
            ww.clearInterval(arrayDaemons[r].idInterval);
   }

   this.createDaemon=function (args)   {// {[id], milliseconds, [script], window}
      var args=$.extend({},args)
      var idVentana=args.id
      var ventanaQueLlama=args.window
      if (idVentana==null)
         idVentana=this.searchId({window:ventanaQueLlama})
      var scriptVentana=args.script
      if (scriptVentana!=null)   {
         var drDesktop=this
         var demonioALLamar=function ()   {
            var windowApl=drWindow.getNativeWindow({id:idVentana})
            if (windowApl!=null) { //Lo normal, el obj Window existe ...
               var func=windowApl[scriptVentana]
               if (func!=null)
                  func()
               }
            else  {//Si el obj window no existe ...
               if (!drDesktop.cList.windowExists(idVentana))  {//Y la ventana no est� registrada ...
                  drDesktop.killDaemons({id:idVentana, window:ventanaQueLlama})
               }
            }
         }
      } else  {
         var demonioALLamar=function ()   {
            var exito=drWindow.reload({id:idVentana})
            if (!exito) {//Si el obj window no existe ...
               if (!drDesktop.cList.windowExists(idVentana))  {//Y la ventana no est� registrada ...
                  drDesktop.killDaemons({id:idVentana, window:ventanaQueLlama})
               }
            }
         }
      }
      var idInterval=this.getDesktopWindow({window:ventanaQueLlama}).setInterval(demonioALLamar, args.milliseconds);
      this.getDaemonRegistry({window:ventanaQueLlama}).mete({"idVentana":idVentana, "idInterval":idInterval, "milliseconds":args.milliseconds, "script":args.script})
   }


   //  ************************ OBJETO REGISTRO DE LOS DAEMONS
   function clDrRegistroDaemons()	{
      //indizados y agrupados por idVentana tengo un array de objetos con los datos del daemon (idInterval:"")
      this.lista={}

      this.mete=function (params)   {//idVentana, idInterval, milliseconds, script
         if (this.lista[params.idVentana]==null)
            this.lista[params.idVentana]=new Array();
         var arr=this.lista[params.idVentana] //lista daemons de esa ventana
         arr[arr.length]=params
      }

      this.saca=function (params)   {//idVentana
         if (this.lista[params.idVentana]==null)
            return null
         var arr=this.lista[params.idVentana] //lista daemons de esa ventana
         this.lista=eliminaObjetoDelHash({hash:this.lista, indice:params.idVentana})
         return arr;
      }

      this.dame=function (params)   {//idVentana
         if (this.lista[params.idVentana]==null)
            return null
         var arr=this.lista[params.idVentana] //lista daemons de esa ventana
         return arr;
      }
   }

   function eliminaObjetoDelHash(params)  {//{hash:xxx, indice:xxx}
      var ret={}
      for (a in params.hash)
         if (a!=params.indice)
            ret[a]=params.hash[a]
      return ret
   }

   //Relacionado con ventanas:  -------------------------------------------------------------------------------------------

   this.searchId=function(args)	{ //{window} Returns window ID if opened from desktop
      var win=args.window
      if (win.frameElement!=null && win.frameElement.drIdVentana!=null)
         return win.frameElement.drIdVentana
      var ventanaInspeccionada=win; 
      while(ventanaInspeccionada.parent!=ventanaInspeccionada) {
         ventanaInspeccionada=ventanaInspeccionada.parent
         if (ventanaInspeccionada.frameElement!=null && ventanaInspeccionada.frameElement.drIdVentana!=null)
            return ventanaInspeccionada.frameElement.drIdVentana
      }
      return null	
   }

   this.getWindowIds=function(args) { //{window, [noWidgets], [onlyCurrentDesktop]}
      var args=$.extend({},args)
      var noWidgets=drEvaluaBooleanoString(args.noWidgets), onlyCurrentDesktop=drEvaluaBooleanoString(args.onlyCurrentDesktop);
      var arrObj=this.cList.getPropertyArray("wOpenArgs")
      var arrId=[]
      if (!noWidgets && !onlyCurrentDesktop){
         for (var r=0; r<arrObj.length; r++)
            arrId[arrId.length]=arrObj[r].id
      } else if (noWidgets && onlyCurrentDesktop) {
         currentDesktop=this.getCurrentVirtualDesktop({window:args.window})
         for (var r=0; r<arrObj.length; r++){
            var elid=arrObj[r].id
            if (!arrObj[r].widget && this.inVirtualDesktop({id:elid, virtualDesktop:currentDesktop, window:args.window})){
               arrId[arrId.length]=elid
            }
         }
      } else if (!noWidgets && onlyCurrentDesktop) {
         currentDesktop=this.getCurrentVirtualDesktop({window:args.window})
         for (var r=0; r<arrObj.length; r++){
            var elid=arrObj[r].id
            if (this.inVirtualDesktop({id:elid, virtualDesktop:currentDesktop, window:args.window})){
               arrId[arrId.length]=elid
            }
         }
      } else if (noWidgets && !onlyCurrentDesktop) {
         for (var r=0; r<arrObj.length; r++){
            var elid=arrObj[r].id
            if (!arrObj[r].widget){
               arrId[arrId.length]=elid
            }
         }
      }
      return arrId
   }

   this.getNativeIframe=function(params){// {window, [id]}
      var params=$.extend({virtualDesktop:0},params)
      if (params.id==null)
         params.id=this.searchId({window:params.window})
      var ww=drWindow.getDesktopWindow({window:params.window})
      return ww.$("#drIfr_"+params.id).get(0)
   }

   this.getNativeWindow=function(params)	{// {window, [id]}
      var Iframe=this.getNativeIframe(params)
      try{
         if (Iframe!=null)
            return Iframe.contentWindow
      }catch(e){
         return null
      }
   }

   //Relacionado con ventanas: ESCRITORIOS VIRTUALES: -------------------------------------------------------------------------------------------

   this.getVirtualDesktop=function (params) { //{[window/id]}
      var params=$.extend({},params)
      if (params.id==null)
         params.id=this.searchId(params)
      var ret=this.wData({id:params.id, property:"wWirtualDesktop"})
      if (ret==null)
         return 0 //Por defecto estamos en la 0 siempre
      return ret;
   }

   this.getCurrentVirtualDesktop=function(args) {//{window}
      var virtualDesktop=this.getDesktopWindow(args).virtualDesktop
      if (virtualDesktop==null)
         return 0;
      return (virtualDesktop)
   }

   this.inVirtualDesktop=function(params)  { //{[id/window], virtualDesktop}
      var params=$.extend({virtualDesktop:0},params)
      if (params.id==null)
         params.id=this.searchId(params)
      if (this.wData({id:params.id, property:"wOpenArgs"}).showInAllDesktops)
         return true
      var vDVent=this.getVirtualDesktop(params);
      return (""+vDVent==""+params.virtualDesktop) //Comparo cadenas para que funcione desde consola
   }

   
   this.changeVirtualDesktop=function(params) { //{virtualDesktop,window}
      var params=$.extend({virtualDesktop:0},params)
      var virtualDesktop=params.virtualDesktop
      var ww=this.getDesktopWindow({window:params.window})
      ww.virtualDesktop=virtualDesktop 
      var idsVentanas=this.getWindowIds({window:params.window})
      for (var r=0; r<idsVentanas.length; r++)	{//Repaso las ventanas. Por cada ventana, si no est� en el desktop la oculto, si no, la muestro ...
         if ( this.inVirtualDesktop({id:idsVentanas[r], virtualDesktop:virtualDesktop, window:params.window}) )//Ojo esto llama a getCurrentVirtualDesktop. Tenemos que haber cambiado el valor antes.
            this.show({id:idsVentanas[r]})
         else
            this.hide({id:idsVentanas[r]})
      }
      if (ww.drEvents){
         ww.drEvents.trigger({type:"ID_changeVD", data:virtualDesktop})
      }
   }


   this.getVirtualDesktopsData=function(params) { //{window}
      //repaso las ventanas y creo una lista de desktops existentes con sus ventanas asociadas.
      var ventanas=this.getWindowIds(params)
      var oDesktopsIndex={}
      for(var r=0; r<ventanas.length; r++)   {
         var idVD=this.getVirtualDesktop({id:ventanas[r]})
         if (oDesktopsIndex[idVD]==null)  {
            oDesktopsIndex[idVD]={id:idVD, windowIds:[ventanas[r]]}
         } else   {
            var arrVent=oDesktopsIndex[idVD].windowIds
            arrVent[arrVent.length]=ventanas[r]
         }
      }
      oDesktops=[]
      for (var i in oDesktopsIndex) 
         oDesktops[oDesktops.length]=oDesktopsIndex[i]
      return oDesktops
   }

   this.setVirtualDesktop=function(params) { //{window, [id],virtualDesktop} Establece el virtual desktop a una ventana
      var params=$.extend({virtualDesktop:0},params)
      var id=params.id, virtualDesktop=params.virtualDesktop
      if (id==null)
         id=this.searchId(params)
      var ret=this.wData({id:id, property:"wWirtualDesktop", value:virtualDesktop})
      //si la ventana no est� en sesi�n deber�a desaparecer.
      if ( this.inVirtualDesktop({id:id, window:params.window, virtualDesktop:this.getCurrentVirtualDesktop({window:params.window})}) )  //Ojo eso llama a getVirtualDesktop, hay que establecer el valor antes
         this.show({id:id})
      else
         this.hide({id:id})
      return ret
   }

   //Relacionado con la estructura del escritorio: -------------------------------------------------------------------------------------------

   this.getDesktopWindow=function(args)  { //{window} Le pasamos una ventana hija del escritorio. Retorna la ventana en la que reside el escritorio o null
      var win=args.window
      if (win.desktopWindow==null) {
         if (win.top.f1==null){
            if (win.dialogArguments!=null && win.dialogArguments.openerWindow!=null)
               win.desktopWindow=win.dialogArguments.openerWindow.top.f1
         }else
            win.desktopWindow=win.top.f1
      }
      return win.desktopWindow   //Presuponemos una cierta estructura en el top, si surge la necesidad de modificar la estructura habr� que tocar esto.
   }

   //Relacionado con la BARRA DE BOTONES : -------------------------------------------------------------------------------------------

   this.taskbar=function(args){ //{id [opc]}
      if (args !=null && args.id!=null)
         this.taskbar_btnClick(args)
      else
         this.taskbar_render()
   }
   
   this.taskbar_btnClick=function(args){ //{id }
      var id=args.id
      /*	
      click en ventana no focalizada no minimizada --> la focalizo, blur de la que lo tuviera antes
      click en ventana focalizada no minimizada --> la minimizo, no focalizo otra (windows xp lo hace) �?
      click en ventana no focalizada minimizada --> la recupera y la focaliza, blur de la que lo tuviera antes [X]
      click en ventana focalizada minimizada --> en windows solo se da a veces (si minimizo maximizada)
      */
      var wStat=this.wData({id:id, property:"wStat"})
      if (wStat=="min") {//Estaba minimizada
         drDesktop.showWindowMoving({id:id})
         this.wData({id:id, property:"wStat", value:"OK"}) //Ya no estar�a maximizada ni minimizada ...
         this.focus({id:id})
      }  else if (wStat=="min-max") {//Estaba minimizada pero antes maximizada
         drDesktop.showWindowMoving({id:id})
         this.wData({id:id, property:"wStat", value:"OK"}) //Ya no estar�a maximizada ni minimizada ...
         this.focus({id:id})
         this.maximizeWindow({id:id})
      } else {//No estaba minimizada
         if (this.hasFocus({id:id}))   {
            drDesktop.minimizeWindow({id:id})
            this.focus({id:'drDesktop_false_id'})
         } else  {
            this.focus({id:id})
         }
      }
   }

   this.taskbar_render=function (){
      var str = "", id = "", winTitle = "", wins=[]
      var oDatBot=window["oDatosBotonera_"+oDesktopConfig.tema] //[REP]
      if (oDatBot!=null)	{
         var bh=oDatBot.bh, tl = oDatBot.tl, marginLeftBtn=oDatBot.marginLeftBtn, marginBottomBtn=oDatBot.marginBottomBtn
         this.get$Win({id:"dTaskbar"}).css({
            scrollbarFaceColor:oDatBot.scrollbarFaceColor, scrollbarShadowColor:oDatBot.scrollbarShadowColor
            ,scrollbarHighlightColor:oDatBot.scrollbarHighlightColor, scrollbar3dLightColor:oDatBot.scrollbar3dLightColor		
            ,scrollbarDarkShadowColor:oDatBot.scrollbarDarkShadowColor, scrollbarTrackColor:oDatBot.scrollbarTrackColor
            ,scrollbarArrowColor:oDatBot.scrollbarArrowColor
         })
      } else	
         //var bh=20, tl = 15, marginLeftBtn=5, marginBottomBtn=40	
         return
      //Obtengo las ventanas [REP] sacar a metodo
      for (var i=this.lastReservedWin;i<=this.maxWindows;i++) {
         id = this.getWindowId({index:i})
         if (id){
            var wNoTaskbar=this.wData({id:id, property:"wNoTaskbar"})
            var wTotallyHidden=this.wData({id:id, property:"wTotallyHidden"})
            if (!wNoTaskbar && !wTotallyHidden)   
               wins[wins.length]=id
         }
      }
      //Recoloco la barra de menu
      var altoPantalla=$(window).height();
      var $barraMenu=$("#BARRA_MENU")
      $barraMenu.css({top:altoPantalla-$barraMenu.height()+"px"})
      //Recoloco la barra de botones
      $win=this.get$Win({id:"dTaskbar"})
      var noTaskbar=false
      var propSkin=drWindow.getSkinParameters();
      if (propSkin!=null)  
         var noTaskbar=drEvaluaBooleanoString(propSkin.noTaskbar)
      if ( noTaskbar ) { 
         $win.rszTo({width:20,height:20}).mvTo({left:0,top:0})
         this.hideWindow({id:"dTaskbar"})
         return
      }
      //Coloco el contenedor y construyo los componentes con el html
      var margenDerecho=5, margenIzquierdo=195, margenInferior=0
      if (oDatBot.marginBottomBotonera!=null)
         margenInferior=oDatBot.marginBottomBotonera
      if (oDatBot.marginLeftBotonera!=null)
         margenIzquierdo=oDatBot.marginLeftBotonera
      if (oDatBot.marginRightBotonera!=null){
         margenDerecho=oDatBot.marginRightBotonera
      }
      if (window.anchoStatus!=null)
         margenDerecho=window.anchoStatus + margenDerecho
      var altoPantalla=$(window).height() // document.cbe.height()
      var anchoPantalla=$(window).width() //document.cbe.width()
      $win.rszTo({width: anchoPantalla-margenDerecho-margenIzquierdo , height:bh})
      $win.mvTo({left: margenIzquierdo, top: altoPantalla-bh-margenInferior})
      $win.css({backgroundColor:'transparent'})
      //Genero el HTML
      $win.css({overflow:"hidden"}) //Fix para firefox
      $win.html(oDatBot.generaHtml(wins)).css({overflowY:"auto"}) //Construyo el HTML
      this.wData({id:"dTaskbar", property:"wWidth", value:$win.width() })
      this.wData({id:"dTaskbar", property:"wHeight", value:$win.height() })
      //Preparo el scroll por pasos
      var altoUnaFila=bh
      window.laCapaTaskBar=$win.get(0)
      laCapaTaskBar.scrollTop=0
      window.filaActualBotonera=0;
      $(laCapaTaskBar).bind("scroll", function (ev)  {
         if (window.noMoreMenuButtonsScroll) {
            laCapaTaskBar.scrollTop=(window.filaActualBotonera)*altoUnaFila
            return
         }
         var filaEnLaQueEstoy=laCapaTaskBar.scrollTop/altoUnaFila
         var filaSup=Math.floor(filaEnLaQueEstoy)
         var filaInf=Math.ceil(filaEnLaQueEstoy)
         if (filaSup==filaInf)
            return
         if (window.filaActualBotonera==filaSup)
            window.filaActualBotonera=filaInf;
         else if (window.filaActualBotonera==filaInf)
            window.filaActualBotonera=filaSup;
         laCapaTaskBar.scrollTop=(window.filaActualBotonera)*altoUnaFila
         window.noMoreMenuButtonsScroll=true
         setTimeout(function(){window.noMoreMenuButtonsScroll=false}, 200)
      })
      this.showWindow({id:"dTaskbar"})
   }


   //DATOS: Relacionados con la lista de ventanas (wData): -------------------

   this.wData=function (args) {
      var id=args.id, property=args.property, value=args.value
      var index=this.cList.getIndex(id)
      if (index == null)
         return
      if (property==null)
         return ""
      if (value == 0 || (value && value != '') )
         this.cList.set(index,property,value)
      return this.cList.get(index,property);
   }

   this.wGetAllData=function (args) {
      var id=args.id
      var index=this.cList.getIndex(id)
      if (index == null)
         return
      return this.cList.getAll(index);
   }

   this.setWDataArray=function(id,array){
      for (var i=0; i<array.length; i++)
         this.wData({id:id, property:array[i][0], value:array[i][1]})
   }

   this.getWindowIndex=function (args){//{id}(si no existe retorna null)
      var id=args.id
      return this.cList.getIndex(id)
   }
   this.getAllWindowId=function (args){//{index[op](min index)}
      var index=args.index
      return this.cList.getAllId(index)
   }
   this.windowLength=function(){ //(Ventanas construidas)
      return this.cList.getLength()
   }
   this.getWindowId=function (args){//{index}
      var index=args.index
      return this.cList.getId(index)
   }
   this.setWindowData=function (args){//{index/id, property, value}
      var index=args.index, id=args.id,  property=args.property, value=args.value
      if (index!=null){
         this.cList.set(index,property,value)
         return value
      } else if (id!=null){
         this.cList.set(this.cList.getIndex(id),property,value)
         return value
      }
   }
   this.newWindow=function (args){//{id}
      var id=args.id
      if (this.cList.getIndex(id) != null)
         return "_REUSED_WIN_"; //indow will be reused [REP]
      var index=this.cList.getFreeIndex(this.lastReservedWin,this.maxWindows)
      if (index==-1)
         var index=this.increaseMaxWindows().firstNewWin
      this.cList.associate(index,id)
      return id;
   }

   this.increaseMaxWindows=function(){
      var firstNewWin=this.maxWindows+1
      var lastNewWin=this.maxWindows+this.maxWindowsInc
      for (var i=firstNewWin; i<=lastNewWin; i++)  {
         $('<div id="drWin_' + i + '" class="drWinContainer"></div>').appendTo($(document.body))
         drDesktop.refreshContainerData(i)
         $winDiv=$("#drWin_"+i)
         $winDiv.css({"visibility":"hidden", clip:"rect(0px auto auto 0px)", left:"0px" ,top:"0px"}).width(0).height(0)
         $winDiv.bind("mousedown",drListeners.mouseDownVentana).bind("selectstart", function(){return false})
         $winDiv.bind("mouseup",drListeners.mouseUpVentana)
      }
      this.maxWindows=lastNewWin
      return {lastNewWin:lastNewWin, firstNewWin:firstNewWin}
   }

   this.deleteWindowProperties=function(args){//{index}
      var index=args.index, id=args.id
      if (index==null && id!=null)
         index=this.cList.getIndex(id)
      if (index < this.lastReservedWin)
         return
      //var tmpCbe = this.cList.get(index,"cbe") //[REP]
      this.cList.free(index)
      this.refreshContainerData(index)
      //this.cList.set(index,"cbe",tmpCbe)
   } 
   this.refreshContainerData=function(index){//{index}
      this.cList.createData(index,this.basicWPropArray)
      if (index<this.lastReservedWin){
         var newId=this.firstWindowNames[index]   //[REP]
         if (newId!=""){
            this.cList.associate(index,newId)
         }
      }
      //this.cList.set(index,"wIndex",index)
   }

   //DATOS: Relacionados con los SKINS (this.skinsData): -------------------------

   this.addSkin=function(args){
      var skin=args.skin
      this.addSkinData(args)
   }

   this.getTopMargin=function(args){
      return this.skinsData[args.skin].topMargin
   }
   this.getBottomMargin=function(args){
      return this.skinsData[args.skin].bottomMargin
   }
   this.addSkinData=function (args){
      var skin=args.skin, topMargin=args.topMargin, bottomMargin=args.bottomMargin
      this.skinsData[skin]=new SkinData(args)
   }
   this.getHtmlChrome=function(args){
      var skin=args.skin, id=args.id, parameters=args.parameters
      if (this.skinsData[skin].chromeFunction)
         return this.skinsData[skin].chromeFunction(id,parameters)
      else
         alert("Skin " + id + " is not available.")
   }

   //RELACIONADOS CON JQuery: -------------------------

   this.get$Win=function(args){ //{id/index}
      if (args.index==null)
         return $("#drWin_"+this.cList.getIndex(args.id))
      else
         return $("#drWin_"+args.index)
   }

   //Relacionados con objetos de datos din�micos "pregrabados": -------------------------

   this.getSkinData=function(args) { //{[skin]}
      if (args==null || args.skin==null)
         args={skin:this.skin} //Por defecto el skin activo
      var skin=args.skin
      return window["oSkin_"+skin]
   }
   
}








// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ SkinData OBJECT
function SkinData(args){ //Data of a skin, {chromeFunction, topMargin, bottomMargin}
   var skin=args.skin, topMargin=args.topMargin, bottomMargin=args.bottomMargin
   var skinFunc=window["skin_" + skin]
   if (jQuery.isFunction( skinFunc ))
      this.chromeFunction=skinFunc
   else
      alert("skin_" + skin + " function missing or invalid, not loaded");
   if(topMargin && topMargin>=0)
      this.topMargin=topMargin
   else
      this.topMargin=0; 
   if(bottomMargin && bottomMargin>=0)
      this.bottomMargin=bottomMargin
   else
      this.bottomMargin=0;
}

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ContainerList OBJECT
function ContainerList(){
   this.cData=[] //Aqu� estar�n los datos de cada contenedor ( cData[index]={xx,yy,zz, wname, ...} )
   this.cIndex={} //Esto ser� un array con los contenedores indexadas ( cIndex[id]=index )
   // --------------------------
   this.windowExists=function(id){
      var index=this.getIndex(id)
      if (this.cIndex[id] == null)
         return false
      return true
   }
   this.getIndex=function(id){ return this.cIndex[id] }
   this.getId=function(index){
      if (index < this.cData.length)
         return this.cData[index].wName
      return -1
   }
   this.set=function(index,property,value){ this.cData[index][property] = value }
   this.get=function(index,property){ return this.cData[index][property] }
   this.getAll=function(index){ return this.cData[index] }
   this.getAllId=function(minIndex){ 
      var ret=[];
      for(var id in this.cIndex) {
         if (minIndex!=null && this.cIndex[id] < minIndex) 
            continue 
         ret[ret.length]=id
      }
      return ret;
   }
   this.getPropertyArray=function(property){
      var ret=[], IDs=this.getAllId(drDesktop.lastReservedWin)
		for (var i=0; i<IDs.length; i++) 
         ret[ret.length]=this.cData[this.getIndex(IDs[i])][property]
      return ret
   }
   this.getLength=function(){ return this.cData.length }
   this.getFreeIndex=function(min,max){
      for (var i=min;i<=max;i++)
         if (this.cData[i].wName=="") //se da por hecho que existen todas hasta maxWindows
            return i
      return -1
   }
   this.associate=function(index,id){
      if (this.cData[index]!=null)
         this.cData[index].wName=id
      this.cIndex[id] = index
   }
   this.free=function(index){
      var id=this.getId(index)
      this.cIndex[id] = null
   }
   this.createData=function(index,array){
      var newData={}
      if (array!=null)
         for (var i=0; i<array.length; i++)
            newData[array[i]]=""
      this.cData[index]=newData
   }
}





// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++     LISTENERS

window.drListeners={
   dblClick_tt:300

   ,mouseUpVentana:function ()  { 
      //Para el doble click casero
      if(drListeners.dblClick_tId==null){
         drListeners.dblClick_tId=setTimeout( function(){drListeners.dblClick_tId=null} ,drListeners.dblClick_tt)
      } else   {
         clearTimeout(drListeners.dblClick_tId)
         drListeners.dblClick_tId=null;
         setTimeout(drListeners.dblClick,0)
      }
   }

   ,mouseDownVentana:function(e){
      var $target=$(this)
      var eventData={dragTarget:this,oWidth:$target.width(), oHeight:$target.height(), oPageX:e.pageX, oPageY:e.pageY, $target:$target, oPosition:$target.position()}
      $(document).unbind("mouseup",drListeners.mouseUpBodyDragging).unbind("mousemove",drListeners.mouseMoveBodyDragging).bind("mouseup", eventData, drListeners.mouseUpBodyDragging).bind("mousemove", eventData, drListeners.mouseMoveBodyDragging)
      e.preventDefault() 
      e.data=eventData
      drListeners.dragStartVentana(e)
      return false
   }

   ,mouseMoveBodyDragging:function (e){
      var eData=e.data
      /*var left=eData.oPosition.left-eData.oPageX + e.pageX
      var top=eData.oPosition.top-eData.oPageY + e.pageY
      eData.$target.css({left: left+"px" ,top:top+"px"})//,top:y+"px"*/
      e.preventDefault()
      drListeners.dragVentana(e)
   }

   ,mouseUpBodyDragging:function (e){
      var eData=e.data
      $(document).unbind("mouseup",drListeners.mouseUpBodyDragging).unbind("mousemove",drListeners.mouseMoveBodyDragging)
      e.preventDefault()
      drListeners.dragEndVentana(e)
   }

   ,dblClick:function(){//Doble click casero (no consigo que el dblclick nativo funcione bien, hay alg�n conflicto de eventos en FF)
      var wName=window.nombreVentanaPulsada //[REP]
      drDesktop.getSetNoEvent(true)//[REP]
      drDesktop.maximizeWindow({id:wName})
   }

   ,desktopResize:function() {
      var $win=$(window)
      var w = $win.width();//var w = document.cbe.width();
      var h = $win.height();//var h = document.cbe.height();
      drDesktop.get$Win({id:'dDesktop'}).rszTo({width:w,height:h})
      drDesktop.taskbar_render()
      drDesktop.repositionAllWindows()
   }

   ,dragStartVentana:function (e) { 
      var eData=e.data //   var eventData={dragTarget ,oWidth, oHeight,  oPageX, oPageY, $target, oPosition}
      drDesktop.getSetNoEvent(false)
      var id=drDesktop.cList.getId(parseInt(eData.dragTarget.id.replace(/drWin_/,'')))
      window.nombreVentanaPulsada=id   //[REP]
      var winData=drDesktop.wGetAllData({id:id})
      var noResize= winData.wNoResize||drWindow.getGlobalNoResize()
      var noDrag= winData.wNoDrag || drWindow.getGlobalNoDrag()
      var noClose= drWindow.getGlobalNoClose() 
      if (winData.wStat == "MAX" && ! winData.wIcon.match(/.1$/) ) { //Si maximizada y sobre un boton *1
         //status="MAX / 1"
         drDesktop.getSetNoEvent(true)
         return;
      } else if (winData.wIcon=="C1") { //sobre boton CERRAR
         drDesktop.getSetNoEvent(true)
         if (!noClose)  {
            //drDesktop.loadUrl({id:id, url:'about:blank'})
            drDesktop.removeWindow({id:id})
            }
         return
      } else if (winData.wIcon == "I1") { // sobre boton AYUDA
         drDesktop.getSetNoEvent(true)
         if (drWindow.hasHelp({id:id}))
            drWindow.showHelp({"windowId":id})
         return
      } else if (winData.wIcon == "M1") { // sobre boton MINIMIZAR
         drDesktop.getSetNoEvent(true)
         drDesktop.minimizeWindow({id:id})
         return;
      } else if (winData.wIcon == "X1") { // sobre boton MAXIMIZAR
         drDesktop.maximizeWindow({id:id})
         drDesktop.getSetNoEvent(true)
         return;
      }
      //RESIZE / ARRASTRE
      var target_offset=e.data.$target.offset()
      var marginToRight=target_offset.left + e.data.$target.width() - e.pageX
      var marginToBottom=target_offset.top + e.data.$target.height() - e.pageY

      if (!noResize && marginToRight<=20 && marginToBottom<= 20)   {//Pulso sobre RESIZE ?
         eData.$target.data("isResizing",true)
         hideWindowAndShowDMove(id,e)
      } else if (!noDrag)   {
         eData.$target.data("isResizing",false)
         hideWindowAndShowDMove(id,e)
      } else   { //Pico sobre una ventana no draggable en la zona de drag 
         drDesktop.bringWindowToFront({id:id})
         drDesktop.getSetNoEvent(true)
      }
   }

   ,dragVentana:function (e){// Esto se ejecuta en el ONMOUSEMOVE del cuadro de arrastre de la ventana
      var eData=e.data//   var eventData={dragTarget ,oWidth, oHeight,  oPageX, oPageY, $target, oPosition}
      if(eData.$target.data("isResizing")){
         var nuevoh=eData.oHeight-eData.oPageY + e.pageY
         var nuevow=eData.oWidth -eData.oPageX + e.pageX
         var nuevoh=aplicaMaximo(nuevoh, drDesktop.wData({id:"dMove", property:"wHeightOrg"}) )
         var nuevow=aplicaMaximo(nuevow, drDesktop.wData({id:"dMove", property:"wWidthOrg" }) )
         //status=eData.oHeight-eData.oPageY+" + "+e.pageY+"="+nuevoh
         drDesktop.get$Win({id:"dMove"}).width(nuevow).height(nuevoh)
      } else {
         var left=eData.oPosition.left-eData.oPageX + e.pageX
         var top=eData.oPosition.top-eData.oPageY + e.pageY
         var coord=getCorrectWindowLeftTop({l:left, t:top, w: drDesktop.wData({id:"dMove", property:"wOriginaldMoveWidth"}), h:drDesktop.wData({id:"dMove", property:"wOriginaldMoveHeight"}) })
         drDesktop.get$Win({id:"dMove"}).css({left:coord.l+"px" ,top:coord.t+"px"})
      }
   }

   ,dragEndVentana:function(e) {
      var eData=e.data //   var eventData={dragTarget ,oWidth, oHeight,  oPageX, oPageY, $target, oPosition}
      var id=drDesktop.cList.getId(parseInt(eData.dragTarget.id.replace(/drWin_/,'')))
      //if (id=="")
      //   return
      if ( drDesktop.getSetNoEvent() )
         return;
      var $wMove=drDesktop.get$Win({id:"dMove"})
      drDesktop.showWindowMoving({id:id})
      var altoDeseado=$wMove.height()
      var anchoDeseado=$wMove.width()
      var wMovePos=$wMove.position()
      eData.$target.css({clip:"rect(0px auto auto 0px)"}).width(anchoDeseado).height(altoDeseado).css({left: wMovePos.left +"px" ,top: wMovePos.top +"px", zIndex:9999999})
      drDesktop.focus({id:id})
      drDesktop.setWDataArray(id, [ ["wX", wMovePos.left],["wY", wMovePos.top],["wPos", wMovePos.left+","+wMovePos.top],["wWidth", anchoDeseado],["wHeight", altoDeseado] ])
      $wMove.width(10).height(10).css({left:"-100px",top:"-100px", zIndex:0})
      drDesktop.hideWindow({id:"dMove"})
      drDesktop.repositionWindow({id:id})
      drDesktop.setStatusIfrDragDivs({id:id, status:'show'})
      /*
      if(eData.$target.data("isResizing",true)){
         var f = drDesktop.wData({id:id, property:"fResize"})
         if ((typeof f) == 'function')
            return f(wName);
         if ((typeof f) == 'string' && f.length)
            return eval(f); 
      }else{
         var f = drDesktop.wData({id:id, property:"fMove"})
         if ((typeof f) == 'function')
            return f(wName);
         if ((typeof f) == 'string' && f.length)
            return eval(f); 
      }*/
   }
}


// ----------------------------------Funciones relacionadas con los LISTENERS:
function getCorrectWindowLeftTop(args)  { //{l, t, w, h}
   var $glob=$(window)
   var altoVisible=$glob.height()
   var anchoVisible=$glob.width()
   var margenInferior=drDesktop.getBottomMargin({skin:drDesktop.skin})
   var margenSuperior=drDesktop.getTopMargin({skin:drDesktop.skin})
   var altoCabeceraVentana=25;
   if (window["oDatos_"+drDesktop.skin]!=null && window["oDatos_"+drDesktop.skin].altoCabeceraVentana!=null)
      altoCabeceraVentana=window["oDatos_"+drDesktop.skin].altoCabeceraVentana
   args.t=aplicaMinimo(args.t, altoVisible-altoCabeceraVentana-margenInferior)
   args.t=aplicaMaximo(args.t, margenSuperior)
   args.l=aplicaMinimo(args.l, anchoVisible-altoCabeceraVentana)
   args.l=aplicaMaximo(args.l, altoCabeceraVentana-args.w )
   return args
}
function hideWindowAndShowDMove(id,e){
   drDesktop.hideWindowMoving({id:id})
   var dWData=drDesktop.wGetAllData({id:id})
   var $wMove=drDesktop.get$Win({id:"dMove"})
   $wMove.css({clip:"rect(0px auto auto 0px)"}).width(dWData.wWidth).height(dWData.wHeight).css({zIndex:9999999}).mvTo({left: dWData.wX ,top:dWData.wY})
   var ignorarMargenes=drEvaluaBooleanoString(dWData.wIgnoreMargins);
   elTop = (ignorarMargenes)? dWData.wY : dWData.wY - drDesktop.getTopMargin({skin:drDesktop.skin})
   var label=dWData.wTitle + "<br><br>left:" + dWData.wX + ', top' + elTop+ "<br><br>width: " + dWData.wWidth + ", height:" + dWData.wHeight
   drDesktop.createMoveBox({label:label})
   $wMove.css("backgroundColor","transparent")
   drDesktop.showWindow({id:"dMove"})
   drDesktop.setStatusIfrDragDivs({id:id, status:'hide'})
   drDesktop.setWDataArray("dMove", [
      ["wHeightOrg", dWData.wHeightOrg],["wWidthOrg", dWData.wWidthOrg],["wOriginalEventpageX", e.pageX],["wOriginalEventpageY", e.pageY]
      ,["wOriginaldMoveLeft", dWData.wX],["wOriginaldMoveTop", dWData.wY],["wOriginaldMoveWidth", dWData.wWidth],["wOriginaldMoveHeight", dWData.wHeight]
   ])
}
function aplicaMinimo(valor, maximo) {
   if (valor>maximo)
      return maximo
   return valor
}
function aplicaMaximo(valor, minimo) {
   if (valor<minimo)
      return minimo
   return valor
}

function drEvaluaBooleanoString(valor)   {
   if (valor==null)
      return false
   if (valor==true)
      return true
   else if (valor==false)
      return false
   else  {
      var cad=(valor+"").toLowerCase()
      if (cad=="true")
         return true
      else
         return false
   }
}


// ---------------------------------- Extension de JQUERY

jQuery.fn.extend({ 

   mvBy:function (args){ //{left, top}
      var top=args.top, left=args.left
      return this.each(function() { 
         var $this=$(this)
         var mvArgs=$this.position()
         if (args.left!=null)
            mvArgs.left=mvArgs.left+args.left
         if (args.top!=null)
            mvArgs.top=mvArgs.top+args.top
         $this.mvTo(mvArgs)
      }); 
   }

   ,mvTo: function(args) { //{left, top}, ej: {left:"r15", top:"20"}, ej: {left:"nw", top:"10"} ( margin 10 ) 
      var top=args.top, left=args.left, cardinalPos=null, margin=null, right=null, bottom=null;
      if ((left+"").charAt(0)=="r") {
         right=parseInt(left.substr(1))
         if (isNaN(right))
            right=0
      }
      if ((top+"").charAt(0)=="b") {
         bottom=parseInt(top.substr(1))
         if (isNaN(bottom))
            bottom=0
      }
      if (isFinite(left) || right!=null){  // ej: {left:"r15", top:"20"}
         return this.each(function() { 
            var $this=$(this), $parent=$this.offsetParent(), nLeft=left, nTop=top
            if (right!=null) {
               var w=$this.width(), pw=$parent.width(), psl=$parent.scrollLeft()
               nLeft=psl+pw-w-right
            }
            if (bottom!=null) {
               var h=$this.height(), ph=$parent.height(), pst=$parent.scrollTop()
               nTop=pst+ph-h-bottom
            }
            $this.css({left:nLeft+"px",top:nTop+"px"})
         }); 
      } else { //ej: {left:"nw", top:"10"}
         cardinalPos=left
         margin=right
         if (!margin)
            margin=0
         return this.each(function() { 
            var $this=$(this), $parent=$this.offsetParent(), x=null, y=null
            var pos=$this.position()
            var x=pos.left, y=pos.top, w=$this.width(), h=$this.height();
            var pw=$parent.width(), ph=$parent.height();
            var sx=$parent.scrollLeft(), sy=$parent.scrollTop();
            var right=sx + pw, bottom=sy + ph;
            var cenLeft=sx + Math.floor((pw-w)/2), cenTop=sy + Math.floor((ph-h)/2);
            sx +=margin;
            sy +=margin;
            right -=margin;
            bottom -=margin;
            switch (cardinalPos.toLowerCase()){
               case 'n': x=cenLeft; if (outside) y=sy - h; else y=sy; break;
               case 'ne': if (outside){x=right; y=sy - h;}else{x=right - w; y=sy;}break;
               case 'e': y=cenTop; if (outside) x=right; else x=right - w; break;
               case 'se': if (outside){x=right; y=bottom;}else{x=right - w; y=bottom - h}break;
               case 's': x=cenLeft; if (outside) y=sy - h; else y=bottom - h; break;
               case 'sw': if (outside){x=sx - w; y=bottom;}else{x=sx; y=bottom - h;}break;
               case 'w': y=cenTop; if (outside) x=sx - w; else x=sx; break;
               case 'nw': if (outside){x=sx - w; y=sy - h;}else{x=sx; y=sy;}break;
               case 'cen': case 'center': x=cenLeft; y=cenTop; break;
               case 'cenh': x=cenLeft; break;
               case 'cenv': y=cenTop; break;
            }
            $this.css({left:x+"px",top:y+"px"})
         }); 
      }



   }
   ,rszTo: function(args) {
      var width=args.width, height=args.height
      return this.each(function() { 
         $(this).css({clip:"rect(0px auto auto 0px)"}).width(width).height(height)
      });
   }


});










