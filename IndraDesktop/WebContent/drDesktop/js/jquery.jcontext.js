/* Copyright (c) 2008 Kean Loong Tan http://www.gimiti.com/kltan
 * Licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * Name: jContext
 * Version: 1.0 (April 28, 2008)
 * Requires: jQuery 1.2+
 */

(function(A){A.fn.showMenu=function(B){var C=A.extend({},A.fn.showMenu.defaults,B);A(this).bind("contextmenu",function(D){A(C.query).show().css({top:D.pageY+"px",left:D.pageX+"px",position:"absolute",opacity:C.opacity,zIndex:C.zindex});return false});A(document).bind("click",function(D){A(C.query).hide()})};A.fn.showMenu.defaults={zindex:2000,query:document,opacity:1}})(jQuery);

