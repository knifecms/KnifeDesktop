function applyParametersToUrl(url, parameters)  {
   if (url.indexOf("?")==-1)
      url=url+"?"
   else
      url=fParams.url+"&"
   var noSoyPrimero=false
   for (r in parameters)	{
      if (noSoyPrimero)
         url=url+"&"
      noSoyPrimero=true
      url=url+escape(r)+"="+escape(parameters[r])
   }
   return url
}

function drGetUrlParameters() {
   var searchStr=document.location.search
   if (searchStr.charAt(0)=='?')
      searchStr=searchStr.substr(1)
   var ret={}
   var searchStrBlocks=searchStr.split("&")
   for (var r=0; r<searchStrBlocks.length;r++)  {
      var bloque=searchStrBlocks[r]
      var componentes=bloque.split("=")
      if (componentes.length==1)
         ret[unescape(bloque)]=""
      else
         ret[unescape(componentes[0])]=unescape(componentes[1])
   }
   return ret
}