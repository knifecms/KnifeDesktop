var drTimeDate={
	getDate:public_drTimeDate_getDate
	,getTime:public_drTimeDate_getTime
}

function public_drTimeDate_getDate(args){
	var separator="/"
	var showYear=true
	var language="en"
	var txtMonth=false
	separator=(args.separator!=null)?args.separator:separator
	showYear=(args.showYear!=null)?args.showYear:showYear
	language=(args.language!=null)?args.language:language
	txtMonth=(args.txtMonth!=null)?args.txtMonth:txtMonth
	var d=new Date();	
	var dia=drDameCadenaDosCaracteres(d.getDate())
	if (txtMonth){
		if (language=="es")
			var mesesTxt=["Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"]
		else 
			var mesesTxt=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
		var mes=mesesTxt[d.getMonth()]
	} else	{
		var mes=drDameCadenaDosCaracteres(d.getMonth())
	}
	if (showYear)
		return dia+separator+mes+separator+d.getFullYear()
	else
		return dia+separator+mes
}

function public_drTimeDate_getTime(args)	{
	var d=new Date();
	var horas=drDameCadenaDosCaracteres(d.getHours())
	var minutos=drDameCadenaDosCaracteres(d.getMinutes())
  	return horas+":"+minutos			
}

function drDameCadenaDosCaracteres(numero)	{
	numero=numero+""
	if (numero.length < 2) 
		numero="0"+numero
	return numero
}