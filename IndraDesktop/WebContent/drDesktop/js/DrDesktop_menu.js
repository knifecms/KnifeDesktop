/* MENU */

var drMenu={
   show:public_show
   ,hide:public_hide
   ,showHide:public_showHide
   ,isOpen:public_isOpen
   ,waitAndClose:public_waitAndClose
   ,cancelClose:public_cancelClose
   ,getPublicMethods: public_getPublicMethods
   // ----- private
   ,getMenuData: private_getMenuData
   ,hideOptions:private_hideOptions
   ,nodeTypes:{NODO:0,BARRA:1} 
   ,preprocessI18N: private_drMenu_preprocessI18N
}

function public_getPublicMethods() {  //Para que se pueda llamar desde consola y la consola sepa que parametros tiene cada m�todo y cuales hacer p�blicos ...
   var subCommands={
      show:{args:[], info: drResources.get({bundle:"core",key:"menuJs.txt1"}) }
      ,hide:{args:[], info: drResources.get({bundle:"core",key:"menuJs.txt2"}) }
      ,showHide:{args:[], info: drResources.get({bundle:"core",key:"menuJs.txt3"}) }
      ,isOpen:{args:[], info: drResources.get({bundle:"core",key:"menuJs.txt4"}) }
      ,waitAndClose:{args:[], info: drResources.get({bundle:"core",key:"menuJs.txt5"}) }
      ,cancelClose:{args:[], info: drResources.get({bundle:"core",key:"menuJs.txt6"}) }
      }
   var publicMethods={
      info: drResources.get({bundle:"core",key:"menuJs.txt7"}) 
      ,subCommands: subCommands
      }
   return publicMethods
   }

function private_drMenu_preprocessI18N()	{
	/*for (var i=0; i<aMenu.length; i++){
		if (aMenu[i].texto!=null)
			aMenu[i].texto=drResources.replaceI18NValues(aMenu[i].texto)
	}*/
}

function private_getMenuData(){
   return drWindow.getDesktopWindow()["oDatosMenuInicio_"+drWindow.getSkin()]
}

function public_show(){
   if (window!=drWindow.getDesktopWindow()){
      drWindow.getDesktopWindow().drMenu.show();
      return;
   }
   var datosMenu=this.getMenuData()
   oDatosMnuInicio=(datosMenu!=null)?datosMenu:{};
   var align=oDatosMnuInicio.align
   align=(align==null)? "bottom":align   //"top" o "bottom"
   var opciones=dameSubOpciones(aMenu,"INICIO_RAIZ")
   opciones=aplicaI18NOpciones(opciones)
   var $MENU=$("#WEBSILOG_MENU0").html(generaHtmlSubopcionesMenu(opciones, 0))
   asociaEventosSiFuncionPersonalizada(opciones, 0)
   if (align=="top")
      var elTop=drWindow.getTopMargin()
   else 
      var elTop=$(window).height()-drWindow.getBottomMargin()-$MENU.height()
   if (oDatosMnuInicio.ajusteAlturaSubnivel_0 != null)
      elTop=elTop+oDatosMnuInicio.ajusteAlturaSubnivel_0
   else if (oDatosMnuInicio.ajusteAlturaSubnivel != null)
      elTop=elTop+oDatosMnuInicio.ajusteAlturaSubnivel
   $MENU.css({left:0, top:elTop, visibility:"visible"})
   window.MENUVISIBLE=true
}



function aplicaI18NOpciones(opciones) {
   for (var r=0; r<opciones.length; r++)  {
      if (opciones[r].texto!=null)  {
         var nuevaOpc={}
         for (a in opciones[r])
            nuevaOpc[a]=opciones[r][a]
         nuevaOpc.texto=drResources.replaceI18NValues(opciones[r].texto)
         opciones[r]=nuevaOpc;
      }
   }
   return opciones
}

function public_hide(){
   var ventana=drWindow.getDesktopWindow()
   ventana.MENUVISIBLE=false
   ventana.ocultaOpcionesMenu(0)
}

function public_showHide(){
   if (this.isOpen()) 
      ocultaMenu()
   else  
      muestraMenu()   
}

function public_isOpen(){
   return drWindow.getDesktopWindow().MENUVISIBLE
}

function public_waitAndClose(){
   drWindow.getDesktopWindow().TIMEOUTMENU=setTimeout("ocultaMenu()",MSEGCIERREMENU);
}

function public_cancelClose(){
   var ventana=drWindow.getDesktopWindow()
   if (ventana.TIMEOUTMENU!=null)
      clearTimeout(ventana.TIMEOUTMENU)
   ventana.TIMEOUTMENU=null;
}

function private_hideOptions(args)   {
   args=(args==null)?{}:args;
   var level=args.level //level es un int
   var laCapa=document.getElementById("WEBSILOG_MENU"+level)
   while (laCapa!=null){
      laCapa.style.visibility = "hidden";
      level++;
      laCapa=document.getElementById("WEBSILOG_MENU"+level)
   }
}



function muestraMenu()  {
   drMenu.show()
}
function ocultaMenu()   {
   drMenu.hide()
}
function dameEstadoVisibilidadMenu()  {
   return drMenu.isOpen()
}
function hideMenu()  {
   drMenu.hide()
}
function ocultaOpcionesMenu (nivel) {
   drMenu.hideOptions({level:nivel})
}
function muestraOcultaMenu() { //La llama el bot�n de inicio
   drMenu.showHide()
}
function preparaCierreMenu() {
   drMenu.waitAndClose();
}
function cancelaCierreMenu() {
   drMenu.cancelClose()
}

//CONSTANTES GLOBALES : 
cc={"NODO":0,"BARRA":1} 

//PARA SOBREESCRIBIR: 
NIVELMAXIMOMENU=5;
MSEGCIERREMENU=1000
var aMenu=[{tipo:cc.NODO, id:"INICIO_RAIZ", hijos:[]}]
var imgFoldersPorDefecto="dossiericon.gif"	
var imgNoExisteAplicacion= "aucun.gif"

oDatosMnuInicio={
   separacionHorizontalEntreMenus:-4
   ,ajusteAlturaSubnivel:6
   }




function abreSubOpcion(objTD, elIDOpcion, nivel)   { //mouseOverOpcPadre
   var opciones=dameSubOpciones(aMenu, elIDOpcion)
   var salidaHTML = generaHtmlSubopcionesMenu(opciones, nivel);
   var laCapa=document.getElementById("WEBSILOG_MENU"+nivel);
   var laCapaPadre=document.getElementById("WEBSILOG_MENU"+(nivel-1));
   var laCapaBarraMenu=document.getElementById("BARRA_MENU");
   var topCapaMenu=(laCapaBarraMenu==null)?0: laCapaBarraMenu.offsetTop
   laCapa.innerHTML=''   
   laCapa.innerHTML=salidaHTML
   asociaEventosSiFuncionPersonalizada(opciones,nivel) 
   laCapa.style.left=laCapaPadre.offsetLeft + laCapaPadre.offsetWidth + oDatosMnuInicio.separacionHorizontalEntreMenus ;
   var ajusteVert=oDatosMnuInicio.ajusteAlturaSubnivel
   if (oDatosMnuInicio["ajusteAlturaSubnivel_"+nivel]!=null)
      ajusteVert=oDatosMnuInicio["ajusteAlturaSubnivel_"+nivel]
   var elTop=laCapaPadre.offsetTop + objTD.offsetTop + ajusteVert;
   if (topCapaMenu<(elTop+laCapa.offsetHeight))  
      elTop=topCapaMenu - laCapa.offsetHeight
   laCapa.style.top=elTop;
   laCapa.style.visibility = "visible";
   ocultaOpcionesMenu(nivel+1)
   laCapa.IDOPMENU=elIDOpcion
   }

function generaHtmlSubopcionesMenu (opciones, nivel) {
   //�El estilo personaliza la estructura del men�?
   var funcionPersonalizada=window["menuInicio_"+drWindow.getSkin()];
   if (funcionPersonalizada!=null && $.isFunction(funcionPersonalizada))   {
      return funcionPersonalizada(opciones, nivel)
      }
   var ht=[]
   if (nivel==0)  {
      ht[ht.length]="<table class='table_opcionesMenu' id='OPS_MENU' onmouseout='preparaCierreMenu()' onmouseover='cancelaCierreMenu()' border='0' cellpadding='0' ><tr>"
      ht[ht.length]="<td class='barraDecoracionMenu'>&nbsp;</td>"
      ht[ht.length]="<td class='td_grupoOpciones'>"
      for (var r=0;r<opciones.length;r++) 
         ht[ht.length]=generaHtmlOpcionMenu (opciones[r],nivel)
         
      ht[ht.length]='</td></tr>'
      ht[ht.length]='</table>'
      }
   else  {
      ht[ht.length]="<table class='table_opcionesMenu' onmouseout='preparaCierreMenu()' onmouseover='cancelaCierreMenu()' border='0' cellpadding='0'><tr><td width='100%'>"
      for (var r=0;r<opciones.length;r++) 
         ht[ht.length]=generaHtmlOpcionMenu (opciones[r],nivel)
      ht[ht.length]='</td></tr></table>'
      }
   return ht.join('');
   }


function asociaEventosSiFuncionPersonalizada(opciones, nivel)  {
   var funcionPersonalizada=window["menuInicio_"+drWindow.getSkin()];
   if (funcionPersonalizada==null || !$.isFunction(funcionPersonalizada))   
      return 
   //$("#OPS_MENU_"+nivel).bind("mouseout",preparaCierreMenu).bind("mouseover",cancelaCierreMenu)
   $("#WEBSILOG_MENU"+nivel).bind("mouseout",preparaCierreMenu).bind("mouseover",cancelaCierreMenu)
   for (var r=0;r<opciones.length;r++) {
      var aOpmenu=opciones[r]
      if (aOpmenu!=null)   {
         var objetoFila=$("#"+aOpmenu.id)
         if (aOpmenu.aplicacion!=null) { //tiene aplicacion
            if (aOpmenu.hijos!=null)   {//tiene aplicacion y tiene hijos
               objetoFila.bind("mousedown", {aplicacion:aOpmenu.aplicacion}, function (ev) {  drAbreAplicacion(ev.data.aplicacion)    })  
               objetoFila.bind("mouseover", {id:aOpmenu.id,niv:(nivel+1)}, function (ev) {  abreSubOpcion(this,ev.data.id,ev.data.niv)  })
               }
            else  { //tiene aplicacion y No tiene hijos
               objetoFila.bind("mousedown", {aplicacion:aOpmenu.aplicacion}, function (ev) {  ocultaMenu(); drAbreAplicacion(ev.data.aplicacion)  })
               objetoFila.bind("mouseover", {niv:(nivel+1)}, function (ev) {  ocultaOpcionesMenu(ev.data.niv) })
               }
            }
         else  { //No tiene aplicaci�n asociada
            if (aOpmenu.hijos!=null)   { //... y Tiene hijos 
               objetoFila.bind("mouseover", {id:aOpmenu.id,niv:(nivel+1)}, function (ev) {  abreSubOpcion(this,ev.data.id,ev.data.niv)  })
               }
            else {                       //... no tiene hijos
               if (aOpmenu.js!=null) {       //... y tiene js asociado
                  var laAplicacion=aOpmenu.aplicacion
                  objetoFila.bind("mousedown", {js:aOpmenu.js},function (ev) {  ocultaMenu(); eval(ev.data.js) }) //closure
                  }
               objetoFila.bind("mouseover", {niv:(nivel+1)}, function (ev) {  ocultaOpcionesMenu(ev.data.niv) })
               }
            }
         }
      }
   }

function generaHtmlOpcionMenu (aOpmenu,nivel) {
   var ht=[]
   switch (aOpmenu.tipo)   {
      case cc.NODO:
      	if (aOpmenu.aplicacion!=null)	{
	      	generaHtmlOpcionMenuAplicacionAcumula(aOpmenu,nivel,ht)
      		}
      	else if  (aOpmenu.fichero!=null)	{
	      	generaHtmlOpcionMenuFicheroAcumula(aOpmenu,nivel,ht)
      		}
      	else 	{
      		generaHtmlOpcionMenuOldAcumula(aOpmenu,nivel,ht)
      		}      		
         break
      case cc.BARRA:
         ht[ht.length]="<hr class='separadorMenu' width='100%'>"
         break
      }
   return ht.join('');
   }


function getNodeTextAndImage(args){ //{node:{aplicacion:xx, img:xxx}} -->{text:xxx, imageUrl:xxx, noDirectAccess:xxx}
   var aOpmenu=args.node
   var oAplicacion=oAplicaciones[aOpmenu.aplicacion]
	if (oAplicacion!=null)	{
      if (oAplicacion.icon==null || oAplicacion.icon=="")
         return {noDirectAccess:true}
		var texto
		if (oAplicacion.iconText)
			texto=oAplicacion.iconText
		else if (oAplicacion.title)	
			texto=drResources.replaceI18NValues(oAplicacion.title)
		else
			texto=aOpmenu.aplicacion
		var img=urlRaizIconos()+'/'+oAplicacion.icon
      return {text:texto, imageUrl:img}
	} else {
      var texto=aOpmenu.aplicacion
      var img
		if (aOpmenu.img){
			img=urlRaizIconos()+'/'+aOpmenu.img
      } else
			img=urlRaizIconos()+'/'+imgNoExisteAplicacion
      return {text:texto, imageUrl:img}
   }
}


function generaHtmlOpcionMenuAplicacionAcumula (aOpmenu,nivel,ht)	{
	var oAplicacion=oAplicaciones[aOpmenu.aplicacion]
	if (oAplicacion!=null)	{
		var texto
		if (oAplicacion.iconText)
			texto=oAplicacion.iconText
		else if (oAplicacion.title)	
			texto=drResources.replaceI18NValues(oAplicacion.title)
		else
			texto=oAplicacion.id
		var img=urlRaizIconos()+'/'+oAplicacion.icon
		var tooltip=(oAplicacion.description?oAplicacion.description:texto)
		var js="drAbreAplicacion('"+aOpmenu.aplicacion+"');" //TO-DO: escapar correctamente
	   }
	else	{
		var texto=aOpmenu.aplicacion
		var img
		if (aOpmenu.img){
			img=urlRaizIconos()+'/'+aOpmenu.img
      }
		else if (aOpmenu.hijos!=null)
			img=urlRaizIconos()+'/'+imgFoldersPorDefecto
		else
			img=urlRaizIconos()+'/'+imgNoExisteAplicacion
		
		var tooltip=""
		var js="alert('La aplicaci�n "+texto+" no est� registrada');" //TO-DO: escapar correctamente
		}
   ht[ht.length]='<table title="'+DrEscHTML(tooltip)+'" ID="'+DrEscHTML(aOpmenu.id)+'" name="'+DrEscHTML(aOpmenu.id)+'"'
   if (js!=null && aOpmenu.hijos==null)
      ht[ht.length]=' onmousedown="ocultaMenu();'+DrEscHTML(js)+'"'
   else if (js!=null)
      ht[ht.length]=' onmousedown="'+DrEscHTML(js)+'"'
      
   ht[ht.length]=' border="0" style="border-collapse: collapse" width="100%" cellpadding="5" class="MenuNormal2"'
   if (aOpmenu.hijos!=null)   {
      ht[ht.length]=' onmouseover="this.className=\'MenuSelect2\';abreSubOpcion(this,\''+DrEscHTML(aOpmenu.id)+'\','+(nivel+1)+')"'
      ht[ht.length]=' onmouseout="this.className=\'MenuNormal2\';" height="20">'
      }
   else {
      ht[ht.length]=' onmouseover="this.className=\'MenuSelect2\'; ocultaOpcionesMenu('+(nivel+1)+')"'
      ht[ht.length]=' onmouseout="this.className=\'MenuNormal2\'" height="20">'
      }
   ht[ht.length]='<tr>'
   ht[ht.length]="<td width='10' align='center'><img src='"+img+"'></td>";
   ht[ht.length]="<td>"+DrEscHTML(texto)+"</td>";
   if (aOpmenu.hijos!=null)
      ht[ht.length]="<td class='flechaAbrirMenu'>&nbsp;</td>";
   ht[ht.length]="</tr>";
   ht[ht.length]="</table>";	
	return ht
	}

function generaHtmlOpcionMenuFicheroAcumula (aOpmenu,nivel,ht)	{
	var dat=generaDatosIconoFichero(aOpmenu.fichero)
	dat.img=urlRaizIconos()+'/'+dat.img;
	if (aOpmenu.img!=null)
		dat.img=aOpmenu.img
	if (dat.img==null && aOpmenu.hijos!=null)	
		img=urlRaizIconos()+'/'+imgFoldersPorDefecto
	dat.texto=aOpmenu.texto==null?dat.texto:aOpmenu.texto
	dat.js=aOpmenu.js==null?dat.js:aOpmenu.js	
   ht[ht.length]='<table title="'+DrEscHTML(dat.tooltip)+'" ID="'+DrEscHTML(aOpmenu.id)+'" name="'+DrEscHTML(aOpmenu.id)+'"'
   if (dat.js!=null && aOpmenu.hijos==null)
      ht[ht.length]=' onmousedown="ocultaMenu();'+DrEscHTML(dat.js)+'"'
   ht[ht.length]=' border="0" style="border-collapse: collapse" width="100%" cellpadding="5" class="MenuNormal2"'
   if (aOpmenu.hijos!=null)   {
      ht[ht.length]=' onmouseover="this.className=\'MenuSelect2\';abreSubOpcion(this,\''+DrEscHTML(aOpmenu.id)+'\','+(nivel+1)+')"'
      ht[ht.length]=' onmouseout="this.className=\'MenuNormal2\';" height="20">'
      }
   else {
      ht[ht.length]=' onmouseover="this.className=\'MenuSelect2\'; ocultaOpcionesMenu('+(nivel+1)+')"'
      ht[ht.length]=' onmouseout="this.className=\'MenuNormal2\'" height="20">'
      }
   ht[ht.length]='<tr>'
   ht[ht.length]="<td width='10' align='center'><img src='"+dat.img+"'></td>";
   ht[ht.length]="<td>"+DrEscHTML(dat.texto)+"</td>";
   if (aOpmenu.hijos!=null)
      ht[ht.length]="<td class='flechaAbrirMenu'>&nbsp;</td>";
   ht[ht.length]="</tr>";
   ht[ht.length]="</table>";
	return ht
	}
	

	
function generaHtmlOpcionMenuOldAcumula (aOpmenu,nivel,ht)	{
	var img
	if (aOpmenu.img!=null)
		img=urlRaizIconos()+'/'+aOpmenu.img
	else if (aOpmenu.hijos!=null)	
		img=urlRaizIconos()+'/'+imgFoldersPorDefecto
	//alert("generaHtmlOpcionMenuOldAcumula: "+aOpmenu.id+" "+img)			
   ht[ht.length]='<table title="'+DrEscHTML(aOpmenu.texto)+'" ID="'+DrEscHTML(aOpmenu.id)+'" name="'+DrEscHTML(aOpmenu.id)+'"'
   if (aOpmenu.js!=null && aOpmenu.hijos==null)
      ht[ht.length]=' onmousedown="ocultaMenu();'+DrEscHTML(aOpmenu.js)+'"'
   ht[ht.length]=' border="0" style="border-collapse: collapse" width="100%" cellpadding="5" class="MenuNormal2"'
   if (aOpmenu.hijos!=null)   {
      ht[ht.length]=' onmouseover="this.className=\'MenuSelect2\';abreSubOpcion(this,\''+DrEscHTML(aOpmenu.id)+'\','+(nivel+1)+')"'
      ht[ht.length]=' onmouseout="this.className=\'MenuNormal2\';" height="20">'
      }
   else {
      ht[ht.length]=' onmouseover="this.className=\'MenuSelect2\'; ocultaOpcionesMenu('+(nivel+1)+')"'
      ht[ht.length]=' onmouseout="this.className=\'MenuNormal2\'" height="20">'
      }
   ht[ht.length]='<tr>'
   ht[ht.length]="<td width='10' align='center'><img src='"+img+"'></td>";
   ht[ht.length]="<td>"+DrEscHTML(aOpmenu.texto)+"</td>";
   if (aOpmenu.hijos!=null)
      //ht[ht.length]="<td width='10' align='center'><img src='./temas/WindowsXP/images/menu/flechedroite.gif'></td>";
      ht[ht.length]="<td class='flechaAbrirMenu'>&nbsp;</td>";
   ht[ht.length]="</tr>";
   ht[ht.length]="</table>";
	return ht
	}	



function dameSubOpciones(elarray, elID) { //Retorna los nodos hijos de un nodo
   var indice=dameIndiceOpcionConID(elarray, elID);
   if (indice==-1)
      return []
   var losIds=elarray[indice].hijos;
   var appConfig=drWindow.getApplicationsConfig()
   var ret=[]
   for (var i=0;i<losIds.length;i++){
      var indice=dameIndiceOpcionConID(elarray, losIds[i]);
      if (indice!=-1)   {
         var opc=elarray[indice]
         //TO-DO: Falta considerar los grupos en los que todos los hijos tienen aplicaciones no existentes (prohibidas)
         var aplicExiste=(opc.aplicacion==null || (opc.aplicacion!=null && appConfig[opc.aplicacion]!=null))
         var padreSinHijos=false;
         if (opc.hijos!=null && opc.hijos.length>0){
            var hijos=dameSubOpciones(elarray,opc.id)
            if (hijos.length==0)
               var padreSinHijos=true
         }

         if (aplicExiste && !padreSinHijos){
            ret[ret.length]=elarray[indice]
         }

      }
   }
   return ret
}


function dameIndiceOpcionConID(elarray, elID) {
   for (var i=0;i<elarray.length;i++)
      if (elarray[i].id==elID)
         return i
   return -1
   }

function generaHtmlMenu(NIVELMAXIMOMENU)  {
   var ret=[]
   for (var v=0;v<NIVELMAXIMOMENU;v++) {
      if (document.getElementById("WEBSILOG_MENU"+v)==null)   
         ret[ret.length]=generaHtmlMenuUnNivel(v)
      }
   return ret.join('');
   }
   
function generaHtmlMenuUnNivel(nivel)   {
	//return "<DIV border='0' style='Z-INDEX: 99999999; LEFT: 230px; VISIBILITY: hidden; POSITION: absolute; bottom: 30px;' class='Menu' id='WEBSILOG_MENU"+nivel+"' name='WEBSILOG_MENU"+nivel+"' position='absolute' z-index='15'></DIV>"
	if (nivel==0)
		return "<DIV border='0' style='Z-INDEX: 1000000100; VISIBILITY: hidden; POSITION: absolute; top: 100px; left: 100px;' class='Menu' id='WEBSILOG_MENU"+nivel+"' name='WEBSILOG_MENU"+nivel+"' position='absolute' z-index='15'></DIV>"
	else
		return "<DIV border='0' style='Z-INDEX: 1000000100; VISIBILITY: hidden; POSITION: absolute; top: 100px; left: 100px;' class='SubMenu' id='WEBSILOG_MENU"+nivel+"' name='WEBSILOG_MENU"+nivel+"' position='absolute' z-index='15'></DIV>"
	}

function dameTextoDeAplicacion(oAplicacion){
   var texto
   if (oAplicacion==null)
      return "";
   if (oAplicacion.iconText)
      texto=oAplicacion.iconText
   else if (oAplicacion.title)	
      texto=drResources.replaceI18NValues(oAplicacion.title)
   else
      texto=oAplicacion.id
   return texto
   }

function dameTooltipDeAplicacion(oAplicacion){
   if (oAplicacion==null)
      return "not found";
   return(oAplicacion.description?oAplicacion.description:dameTextoDeAplicacion(oAplicacion))
   }

function dameIconoDeAplicacion(oAplicacion){
   return  "<img src='"+dameUrlIconoDeAplicacion(oAplicacion)+"'>"
   }

function dameUrlIconoDeAplicacion(oAplicacion){
   if (oAplicacion==null)
      return "xxx.gif";
   //return urlRaizIconos()+'/'+oAplicacion.icon
   return dameUrlIcono(oAplicacion.icon)
   }

function dameIconoDeCarpeta(){
   return  "<img src='"+dameUrlIconoDeCarpeta()+"'>"
   }

function dameIconoDeCarpeta16x16(){
   return  "<img src='"+dameUrlIconoDeCarpeta16x16()+"'>"
   }

function dameUrlIconoDeCarpeta(){
   //return imgsrc=urlRaizIconos()+'/'+ "dossiericon.gif"	
   return dameUrlIcono("dossiericon.gif")
   }

function dameUrlIconoDeCarpeta16x16(){
   //return imgsrc=urlRaizIconos()+'/'+ "dossiericon_16x16.gif"	
   return dameUrlIcono("dossiericon_16x16.gif")
   }

function dameUrlIcono(img){
   return urlRaizIconos()+'/'+ img
}


