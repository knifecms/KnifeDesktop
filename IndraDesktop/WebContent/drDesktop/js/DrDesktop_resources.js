var IDesktopWebOSVersion="1.42"

drResources={
	get:public_drResources_get
	,getPublicMethods:public_drResources_getPublicMethods
	,replaceI18NValues:public_replaceI18NValues
	,getVersion:public_getVersion
}

function public_getVersion(){
	return IDesktopWebOSVersion
}

function public_replaceI18NValues(value){
	if (value==null)
		return null;
	if (value.length>4 && value.charAt(0)=='$' && value.charAt(1) == '{' && value.charAt(value.length-1) == '}'){
		var code=value.substring(2,value.length-1);
		var indice=code.indexOf(",");
		if (indice==-1)
			return value;
		else{
			var bundle=code.substring(0,indice);
			var key=code.substring(indice+1);
			var ret=drResources.get({bundle:bundle,key:key})
			if (ret!=null)
				return ret
			else
				return value
		}		
	}
	return value
}

function extracted_getDesktopWindow(){ //Ojo, si cambiamos en _ventanas hemos de cambiar aqui
   if (this.desktopWindow==null) {
      if (window.top.f1==null){
         if (window.dialogArguments!=null && window.dialogArguments.openerWindow!=null)
            this.desktopWindow=window.dialogArguments.openerWindow.top.f1
      }else
         this.desktopWindow=window.top.f1
   }
   return this.desktopWindow   //Presuponemos una cierta estructura en el top, si surge la necesidad de modificar la estructura habr� que tocar esto.
}

function public_drResources_get(args){
	var cacheWindow=window;
	var dw=extracted_getDesktopWindow()
	if (dw!=null)
		cacheWindow=dw	
   else  { //No estamos bajo el desktop
      return "{"+args.bundle+", "+args.key+"}"
   }
	if (cacheWindow!=window){
		return cacheWindow.drResources.get(args) //esto asegura que lo siguiente solo se ejecuta en "cacheWindow" 
	}
	if (window.bundleCache==null){
		bundleCache=new BundleCache()
	}
	var bundle=args.bundle, key=args.key;
	return (bundleCache.get(bundle,key))
}

function public_drResources_getPublicMethods(){
   var subCommands={
      get:{ 
      	info:""
      	,args:[{name:"bundle", info:""},{name:"key", info: ""}]
      }
   }
   var publicMethods={
      info:""
      ,subCommands:subCommands
   }
   return publicMethods
}

// ---------------------------------------------------- BundleCache CLASS ----------------------------------------------------
// en consola drResources get -bundle login -key login.title

var conDwrConnectionTimeout=5000;

function BundleCache(){
	this.bundles={}
	
	this.get=function(bundle,key){
		if (this.bundles[bundle]!=null){
			return this.bundles[bundle][key]
		} else	{
	      var objResp={}
			ResourcesService.getBundle(bundle,{
	         async:false
	         ,callback:function (obj){objResp.bundle=obj} //Utilizo una closure, unica solucion en dwr 2.0
	         ,timeout:conDwrConnectionTimeout
	         ,errorHandler:function (message){
	         	alert("There has been a technical error.\nError message: "+message)
	         }
	      });
	      if (objResp.bundle!=null){
	      	this.bundles[bundle]=objResp.bundle
	      	return this.bundles[bundle][key]
			}	else	{
				this.bundles[bundle]={} //No sigo insistiendo
				return this.bundles[bundle][key]
			}
		}
	}
	
	
}




