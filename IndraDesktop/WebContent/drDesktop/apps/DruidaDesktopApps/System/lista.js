function clDrlista(defLista) {
   this.defLista=defLista

   this.idContenedor=null
   this.clase=null
   this.claseTablaCabecera=null
   this.claseCapaDatos=null
   this.claseCeldaAdicionalCabecera=null
   this.claseCeldaScrollCabecera=null
   this.claseCeldaAdicionalDatos=null
   this.claseResaltado=null
   this.claseSeleccionado=null
   this.idColumnaDatosCodigo=null
   this.arr_columnas=null
   this.arr_cabecera=null
   this.arr_filas=null
   this.f_usuario=null

   this.prepara=f_lista_prepara
   this.preparaPropiedades=f_lista_preparaPropiedades
   this.preparaHtml=f_lista_preparaHtml
   this.preparaVariablesCapas=f_lista_preparaVariablesCapas
   this.preparaCapas= f_lista_preparaCapas
   this.preparaEventosDocument=f_lista_preparaEventosDocument
   this.generaHtml=f_lista_generaHtml
   this.recoloca=f_lista_recoloca
   this.alCambiarDatos=f_lista_alCambiarDatos
   this.alCambiarColumnas=f_lista_alCambiarColumnas 
   this.onmousedown=f_lista_onmousedown 
   this.onmouseover=f_lista_onmouseover 
   this.onmouseout=f_lista_onmouseout 
   this.selecciona=f_lista_selecciona 
   this.ordena=f_lista_ordena 
   this.redimensiona=f_lista_redimensiona
   this.ajustaAlto=f_lista_ajustaAlto
}



/* funciones objetos LISTA (una vez en toda la p�gina) */
function f_lista_prepara() {
   this.preparaPropiedades()
   this.preparaHtml()
   this.preparaVariablesCapas()
   this.preparaCapas()
   this.preparaEventosDocument()
	this.ajustaAlto();    
   this.alCambiarDatos()
   this.recoloca()
   lista_version(this);
   }

function f_lista_preparaPropiedades()  {
   for (var r in this.defLista)  
      this[r]=this.defLista[r]
   if (window.drObjListas==null) 
      window.drObjListas={}
   drObjListas[this.idContenedor]=this
   }

function f_lista_preparaHtml()   {
   this.cpCONTENEDOR=dameObjId(this.idContenedor)
   this.cpCONTENEDOR.innerHTML=this.generaHtml()
   }

function f_lista_generaHtml()   {
	/*
	   document.styleSheets[0].addRule("#capaDeMiLista", "position:relative;overflow:hidden;boredr:1px solid red");
	   document.styleSheets[0].addRule("#capaDeMiLista_CABECERA", "width:100%; overflow:hidden");
	   document.styleSheets[0].addRule("#capaDeMiLista_TABLA_CABECERA", "table-layout:fixed;width:100%");
	   document.styleSheets[0].addRule("#capaDeMiLista_TABLA_CABECERA DIV", "overflow: hidden;text-overflow:ellipsis;white-space:nowrap");
	   document.styleSheets[0].addRule("#TABLA_CABECERA TD", "overflow: hidden;text-overflow:ellipsis;white-space:nowrap");
	   document.styleSheets[0].addRule("#capaDeMiLista_DATOS", "height:300;position:relative;width:100%;overflow:auto");
	   document.styleSheets[0].addRule("#capaDeMiLista_TABLA_DATOS", "width:100%;table-layout:fixed;");
	   document.styleSheets[0].addRule("#capaDeMiLista_TABLA_DATOS TD", "width:100%;overflow: hidden;text-overflow:ellipsis;white-space:nowrap");
	   document.styleSheets[0].addRule("#capaDeMiLista_CELDA_SCR", "width:16px;");
	   document.styleSheets[0].addRule("#capaDeMiLista_CTRL0", "z-index:200;filter:Alpha(opacity=16);position:absolute;width:8;cursor:col-resize;");
	   document.styleSheets[0].addRule("#capaDeMiLista_CTRL1", "z-index:200;filter:Alpha(opacity=16);position:absolute;width:8;cursor:col-resize;");
	   document.styleSheets[0].addRule("#capaDeMiLista_CTRL2", "z-index:200;filter:Alpha(opacity=16);position:absolute;width:8;cursor:col-resize;");
	*/
   html=[]

   //Cabecera
   html[html.length]="     <DIV id=\""+this.idContenedor+"_CABECERA\" onselectstart=\"calcelaSelect();\">";
   html[html.length]="         <table class=\""+this.claseTablaCabecera+"\" id=\""+this.idContenedor+"_TABLA_CABECERA\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
   html[html.length]="           <tr>";
   for (var i=0; i<this.arr_columnas.length; i++)  {
      html[html.length]="             <td width=\""+this.arr_columnas[i].ancho+"\" class=\""+this.arr_columnas[i].claseCabecera+"\" orden=\""+this.arr_columnas[i].orden+"\"><div><a href=\"javascript:void(null)\" onmousedown=\"drObjListas."+this.idContenedor+".ordena("+i+")\">"+this.arr_cabecera[i]+"</a></div></td>";
      }
   html[html.length]="             <td class=\""+this.claseCeldaAdicionalCabecera+"\" id=\""+this.idContenedor+"_CELDA_ADIC\">&nbsp;</td>";
   html[html.length]="             <td class=\""+this.claseCeldaScrollCabecera+"\" id=\""+this.idContenedor+"_CELDA_SCR\">&nbsp;</td>";
   html[html.length]="           </tr>";
   html[html.length]="         </table>";
   html[html.length]="      </DIV>";
   //Datos
   html[html.length]="      <DIV id=\""+this.idContenedor+"_DATOS\" class=\""+this.claseCapaDatos+"\" >";
   html[html.length]="         <table id=\""+this.idContenedor+"_TABLA_DATOS\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" onmousedown=\"drObjListas."+this.idContenedor+".onmousedown(this)\" onmouseover=\"drObjListas."+this.idContenedor+".onmouseover(this)\" onmouseout=\"drObjListas."+this.idContenedor+".onmouseout(this)\">";
   //Definicion columnas de datos
   for (var i=0; i<this.arr_columnas.length; i++)  {
      html[html.length]="           <COL class=\""+this.arr_columnas[i].claseDatos+"\" style=\"width:"+this.arr_columnas[i].ancho+"\"></COL>";
      }
   html[html.length]="           <COL class=\""+this.claseCeldaAdicionalDatos+"\" ID=\""+this.idContenedor+"_COL_ADIC\"></COL>";
   //Filas de datos
   var idcod=this.idColumnaDatosCodigo
   var filas=this.arr_filas
   var defCol=this.arr_columnas
   for (var f=0; f<filas.length; f++)  {
      var objFila=filas[f]
      if (objFila[idcod]!=null)
         html[html.length]="           <tr COD=\""+DrEscHTML(objFila[idcod])+"\">";
      else
         html[html.length]="           <tr>";
      //columnas de datos de una fila
      for (var c=0; c<defCol.length; c++)  {
         html[html.length]="             <td>"+objFila[defCol[c].idColumnaDatos]+"</td>";
         }
      html[html.length]="             <td>&nbsp;</td>";
      html[html.length]="           </tr>";
      }
   html[html.length]="         </table>";
   html[html.length]="      </DIV>";
   //Genero los controles de columna
   for (var i=0; i<this.arr_columnas.length; i++)  {
      html[html.length]="      <DIV id=\""+this.idContenedor+"_CTRL"+i+"\" ondblclick=\"drObjListas."+this.idContenedor+".redimensiona("+i+")\" onmousedown=\"agarraControl(this)\" onmouseup=\"sueltaControl()\" onselectstart=\"calcelaSelect();\">";
      html[html.length]="         <img style=\"width:100%;height:100%\"/>";
      html[html.length]="      </DIV>";
      }
   return html.join('')
   }

function f_lista_preparaVariablesCapas()  {
   this.cpCONTENEDOR=dameObjId(this.idContenedor)
   this.cpDATOS=dameObjId(this.idContenedor+'_DATOS')
   this.cpCABECERA=dameObjId(this.idContenedor+'_CABECERA')
   this.tblCABECERA=dameObjId(this.idContenedor+'_TABLA_CABECERA')
   this.tblDATOS=dameObjId(this.idContenedor+'_TABLA_DATOS')
   this.celdaScoll=dameObjId(this.idContenedor+'_CELDA_SCR')
   this.celdaAdicional=dameObjId(this.idContenedor+'_CELDA_ADIC')
   this.columnaAdicional=dameObjId(this.idContenedor+'_COL_ADIC')
   this.controladores=[]
   for (var i=0; i<this.arr_columnas.length; i++)
      this.controladores[this.controladores.length]=dameObjId(this.idContenedor+'_CTRL'+i)
   this.celdasCab=this.tblCABECERA.cells
   }

function f_lista_preparaCapas()  {
/*
   with(this.cpCONTENEDOR.style) {
      position="relative"
      overflow="hidden"
      }
   with(this.cpCABECERA.style) {
      zIndex=100; 
      top=100; 
      position="absolute"; 
      width="100%"; 
      overflow="hidden"
      }
   with(this.cpCABECERA.style) {
      zIndex=100; 
      top=100; 
      position="absolute"; 
      width="100%"; 
      overflow="hidden"
      }*/
   //this.cpCONTENEDOR.style.paddingTop=this.cpCABECERA.offsetHeight;

   this.cpDATOS.lista=this
   this.cpDATOS.onscroll=f_lista_cpDatos_onscroll
   }

function f_lista_preparaEventosDocument() {
   /* Eventos Generales (una vez en toda la p�gina). Podr�an valer para varios objetos*/
   
   if (window.arrDrListas==null)	
	   window.arrDrListas={}
  	window.arrDrListas[this.idContenedor]=this
  	
   if (window.eventospreparados)
      return;
   window.eventospreparados=true; 
   document.body.onmousemove=function () {
      var objTrasladado=window.agarrado
      window.distanciaX=event.clientX - window.xRatonInicial
      if (objTrasladado!=null)   
         objTrasladado.style.left=objTrasladado.xInicial + distanciaX
      }
   document.body.onmouseup=sueltaControl
  	window.onresize=function (){ //Se llama a cada instante ... TO-DO: optimizar esto para que se haga al dejar de arrastrar ...
  		//Se llama al mover --> optimizar para que solo se llame 1 vez
  		var arrLst=window.arrDrListas
 		for (var i in arrLst)	{
	 		var laLista=arrLst[i]
 			if (laLista.antiguoAncho!=laLista.cpCONTENEDOR.offsetWidth || laLista.antiguoAlto!=laLista.cpCONTENEDOR.offsetHeight)	{
 				laLista.antiguoAncho=laLista.cpCONTENEDOR.offsetWidth
 				laLista.antiguoAlto=laLista.cpCONTENEDOR.offsetHeight
		  		laLista.ajustaAlto(); 
   	  		laLista.alCambiarDatos()
	   	   laLista.recoloca()
	   	   }
	      }
      }
   }

function lista_estirar(obj)   {
   if (obj.offsetWidth + window.distanciaX>1)
      obj.style.width=obj.offsetWidth + window.distanciaX + "px"
   else
      obj.style.width=2
   }
function lista_marca(obj)  {
   obj.style.borderLeft='1px solid black';
   obj.style.borderRight='1px solid black';
   obj.oldWidth=obj.offsetWidth;
   obj.style.width='3px'
   }
function lista_desmarca(obj)  {
   obj.style.borderLeft='none';
   obj.style.borderRight='none';
   obj.style.width=obj.oldWidth
   }
function lista_version(lista) {  //arreglos segun version
   if (clientInformation.appVersion.indexOf("MSIE 5")!=-1) 
      for (var f=0;f<lista.controladores.length;f++)   
         lista.controladores[f].style.cursor="move"
   if (clientInformation.appVersion.indexOf("MSIE 5.0")!=-1) 
      for (var f=0;f<lista.tblDATOS.cells.length;f++)   
         lista.tblDATOS.cells[f].noWrap=true;
   }
function f_lista_recoloca()  {//Recoloca los Controladores
   this.alCambiarColumnas()
   for (var f=0;f<this.controladores.length;f++)   {
      this.controladores[f].style.left=this.celdasCab[f].offsetLeft+this.celdasCab[f].offsetWidth-(this.controladores[f].offsetWidth/2)-this.cpCABECERA.scrollLeft
      this.controladores[f].xSinScroll=this.controladores[f].offsetLeft + this.cpCABECERA.scrollLeft
      this.controladores[f].style.top=this.celdasCab[f].offsetTop
      this.controladores[f].style.height=(this.celdasCab[f].offsetHeight + this.cpDATOS.clientHeight -3)
      this.controladores[f].alAgarrar=new Function ("obj","lista_marca(obj)");
      this.controladores[f].alSoltar=new Function ("obj","lista_estirar(drObjListas."+this.idContenedor+".celdasCab["+f+"]);lista_estirar(drObjListas."+this.idContenedor+".tblDATOS.getElementsByTagName('COL')["+f+"]);lista_desmarca(obj);drObjListas."+this.idContenedor+".recoloca();");
      }
   }
function f_lista_alCambiarDatos()  {
   this.celdaScoll.style.display=(this.cpDATOS.clientWidth < this.cpDATOS.offsetWidth)?"inline":"none"  //hay scroll vert?
   }
function f_lista_alCambiarColumnas()  {
   var test=(this.cpDATOS.clientHeight < this.cpDATOS.offsetHeight)
   this.celdaAdicional.style.display=test?"none":"inline"
   this.columnaAdicional.style.display=test?"none":"inline"
   } 
function f_lista_onmousedown(){
   var elObj=event.srcElement;
   while (elObj.tagName!="TR" && elObj.parentElement)
      elObj=elObj.parentElement
   if (!elObj.seleccionado && elObj.tagName=="TR")  
      this.selecciona(elObj)
   }      
function f_lista_onmouseover(){
   var elObj=event.toElement;
   while (elObj.tagName!="TD" && elObj.parentElement)
      elObj=elObj.parentElement
   if (elObj.tagName=="TD")   {
      this.ultimoTdActivo=elObj;
      elObj.title=(elObj.scrollWidth>elObj.offsetWidth)?elObj.innerText:""
      if (elObj.parentElement)//claseResaltado]]
         elObj=elObj.parentElement
      if (!elObj.seleccionado && elObj.tagName=="TR") 
         elObj.className=this.claseResaltado
      }
   }   
function f_lista_onmouseout(){
   var elObj=event.fromElement;
   while (elObj.tagName!="TR" && elObj.parentElement)
      elObj=elObj.parentElement
   if (!elObj.seleccionado && elObj.tagName=="TR")
      elObj.className=""
   }
   
function f_lista_selecciona(obj){
   if (this.seleccionado) { 
      this.seleccionado.className=""
      this.seleccionado.seleccionado=false;
      }
   this.seleccionado=obj;
   obj.seleccionado=true;
   obj.className=this.claseSeleccionado
   if (obj.COD!=null && obj.COD!="") 
      this.f_usuario(obj.COD)
   }      
function f_lista_ordena(col) {//Ojo si vacia
   var arr=[],celdaCab=this.tblCABECERA.rows[0].cells[col];
   switch (celdaCab.orden) {
         //ej:            case "dd-mm-aa": ... break;
      default: //Alfanumerico
         for (var f=0;f<this.tblDATOS.rows.length;f++)  
            arr[f]=[this.tblDATOS.rows[f].cells[col].innerText,this.tblDATOS.rows[f]]
      break;
      }
   arr=arr.sort();
   if (celdaCab.ordenado!=null) {
      arr=arr.reverse()
      celdaCab.ordenado=null
      }
   else  
      celdaCab.ordenado=true
   var anchos=[]
   for (var f=0;f<arr.length;f++)   {
      var ind=arr[f][1].rowIndex
      this.tblDATOS.moveRow(ind, arr.length-1)
      }
   }
function f_lista_redimensiona(col) {//Ojo si esta vacia
   var anchoMax=0;
   for (var f=0;f<this.tblDATOS.rows.length;f++)  {
      var anchos=this.tblDATOS.rows[f].cells[col].scrollWidth;
      if (anchoMax<anchos)
         anchoMax=anchos;
      }
   window.distanciaX=anchoMax-this.tblCABECERA.cells[col].offsetWidth
   lista_estirar(this.tblCABECERA.cells[col]);
   lista_estirar(this.tblDATOS.getElementsByTagName('COL')[col])
   this.recoloca();
   }
function f_lista_cpDatos_onscroll() {
   this.lista.cpCABECERA.scrollLeft=this.scrollLeft
   for (var f=0;f<this.lista.controladores.length;f++)
      this.lista.controladores[f].style.left=this.lista.controladores[f].xSinScroll-this.scrollLeft
   }
   
function f_lista_ajustaAlto()	{
	this.cpDATOS.style.height=this.cpCONTENEDOR.offsetHeight-this.cpCABECERA.offsetHeight //TO-DO: borde?
	}
	

/* estos podr�an valer para varios objetos */
function dameObjId(id) {
   return document.getElementById(id);   
   }
function agarraControl(obj)   {
   window.distanciaX=0
   window.xRatonInicial=event.clientX
   window.agarrado=obj
   obj.xInicial=obj.offsetLeft;
   if (obj.alAgarrar)
      obj.alAgarrar(obj)
   }
function sueltaControl()   {
   if (window.agarrado) {
      if (window.agarrado.alSoltar)
         window.agarrado.alSoltar(window.agarrado)
      window.agarrado=null
      }
   }
function calcelaSelect()    {
    window.event.cancelBubble = true;
    window.event.returnValue = false;
    return false;
    }

function DrEscHTML (cad) {
   if (cad!=null)
      return cad.replace(/&/gi,"&amp;").replace(/</gi,"&lt;").replace(/"/gi,"&quot;"); 
   return "";
   }