
$(document).ready(onDocumentReady);

function onDocumentReady()  {
   var drDesktopWin = drWindow.getDesktopWindow()
   if (drDesktopWin!=null){
      window.oAplicaciones=drWindow.getApplicationsConfig()
      window.oGruposAplicaciones=drWindow.getApplicationsGroupsConfig()
      }
	//var quieroEj=getQuieroEjecutar()
	var losDatos=dameDatosAplicaciones()
   //alert(losDatos.length+","+quieroEj)
   inicializaLista(losDatos)
   preparaLayout();

}


// ------------------------ funcionalidad
function generaIcono(apl){
   with (drWindow.getDesktopWindow())  {
   	if (xMenuDerecho!=null && yMenuDerecho !=null)
			generaNuevoIcono(apl, xMenuDerecho - 40, yMenuDerecho - 15)
		else
			generaNuevoIcono(apl, 100, 100)
   }
}

// ------------------------ relativo al IF: lista

function inicializaLista(losDatos)  {
   //minWidth:40, width:270, sortOptions:{sortFunction:sortByFilename}, formatter:nameFileFormatter , formatter:lengthFileFormatter
   window.myColumnDefs = [
      {key:"nombre", width:175, label: drResources.get({bundle:"core",key:"appList.txt1"}) , sortable:true, resizeable:true,formatter:nameFormatter}
      ,{key:"grupo", width:250, label: drResources.get({bundle:"core",key:"appList.txt2"}) , sortable:true, resizeable:true}
      ,{key:"descripcion", label: drResources.get({bundle:"core",key:"appList.txt3"}) , sortable:true, resizeable:true}
   ];
   var myDataSource = new YAHOO.util.DataSource(losDatos);//
   myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
   myDataSource.responseSchema = { fields: ["numGrupo","cod","nombre","descripcion","grupo","motor","icon"] };
   //	      var nuevoDato={"img": laImg, "cod":apl , "nombre":elNombre, "descripcion":laDescripcion, "grupo":elGrupo, "motor":elMotor}
   var tam=tamanyoDeseadoLista()
   window.myDataTable = new YAHOO.widget.DataTable("lista", myColumnDefs, myDataSource, { sortedBy:{key:"grupo"}, selectionMode:"single", draggableColumns:true, scrollable:true, height:tam.hpx, width:tam.wpx}); //formatRow: myRowFormatter
   // Subscribe to events for row selection   
   window.myDataTable.subscribe("rowMouseoverEvent", myDataTable.onEventHighlightRow);
   window.myDataTable.subscribe("rowMouseoutEvent", myDataTable.onEventUnhighlightRow);
   window.myDataTable.subscribe("rowSelectEvent", myDataTable.clearTextSelection );
   window.myDataTable.subscribe("theadCellDblclickEvent", myDataTable.clearTextSelection );
   window.myDataTable.subscribe("theadCellClickEvent", myDataTable.clearTextSelection );
   window.myDataTable.subscribe("rowMousedownEvent", myDataTable_rowMousedownEvent);
   window.myDataTable.subscribe("rowDblclickEvent", myDataTable_onRowDblclickEvent);
   window.myDataTable.set("MSG_EMPTY", " ") //msg cuando no hay ficheros
   window.myDataTable.render();
   window.myDataTable.focus();
}

function myDataTable_onRowDblclickEvent(oArgs) {
   var elRecord=window.myDataTable.getRecord(oArgs.target)
   //window.myDataTable.clearTextSelection();
   if (getQuieroEjecutar()){
      drWindow.openApplication({application:elRecord.getData("cod")})
   } else   {
      generaIcono( elRecord.getData("cod") )
      drWindow.close();
   }
}

function myDataTable_rowMousedownEvent(ev){
   myDataTable.onEventSelectRow(ev);
   window.setTimeout("myDataTable.clearTextSelection()",0);
   var elRecord=window.myDataTable.getRecord(ev.target)
   if (getQuieroEjecutar()){
      drWindow.openApplication({application:elRecord.getData("cod")})
   } else   {
      generaIcono( elRecord.getData("cod") )
      drWindow.close();
   }
}

function nameFormatter(elCell, oRecord, oColumn, oData) {
   if (oData==null)
      return
   var htmlEstilo="",elInnerHTML=[]
   elInnerHTML[elInnerHTML.length]="<img src=\""+drDesktopWin.urlRaizIconos()+'/'+oRecord.getData("icon")+"\" style=\"height:17px;width:17px\" align=absmiddle > "
   elInnerHTML[elInnerHTML.length]=oData
   elCell.innerHTML=elInnerHTML.join('')
}

function myRowFormatter(elTr, oRecord) {  
   if (oRecord.getData('numGrupo')%2!=0)  {
      $(elTr).addClass("oddGroupRow");
   }
   return true;   
}; 

// ------------------------ recoloca el layout
function preparaLayout()   {
   $(window).bind("resize",recolocaLayout)
   recolocaLayout()
}
function recolocaLayout(ev)  {
   setTimeout(recolocaLayout_timeout,0)
}
function recolocaLayout_timeout()  {
   var tam=tamanyoDeseadoLista()
   window.myDataTable.set("width",tam.wpx);
   window.myDataTable.set("height",tam.hpx);
   $("#lista .yui-dt-hd").width(tam.w)
   $("#lista .yui-dt-bd").width(tam.w)
   $("#lista .yui-dt-bd").height(tam.h)
   window.myDataTable._syncScroll()
}
function tamanyoDeseadoLista()   {
   var availW=$(window).width()-2
   var availH=$(window).height()- 25 //ajustar si se cambia el dise�o
   return {"wpx":availW+"px" , "hpx":availH+"px", w:availW, h:availH}
}
// ------------------------ parametros de comportamiento
function getQuieroEjecutar()	{
	if(document.location.search=='?MODO=EJECUCION') //TO-DO: Mejorar esto para hacerlo mas gen�rico
		return true
	return false;
}



// ------------------------- datos origen
function dameDatosAplicaciones()	{
   var losDatos=[]
   for (apl in oAplicaciones)  {
      var laApl=oAplicaciones[apl]
      if (laApl.icon) { //SI NO HAY ATRIBUTO DE IMAGEN ENTONCES NO PERMITO CREAR UN ICONO
	      var elNombre=""
	      if (laApl.iconText!=null)
	         elNombre=laApl.iconText
	      else 
	         elNombre=apl
	      var laDescripcion=""
	      if (laApl.description!=null)
	         laDescripcion=laApl.description
	      else if (laApl.title!=null)
	         laDescripcion=drResources.replaceI18NValues(laApl.title)
	      var elMotor=""
	      if (laApl.engine!=null)
	         elMotor=laApl.engine
	      else if (laApl.js)
	         elMotor= drResources.get({bundle:"core",key:"appList.txt4"}) 
	      else if (laApl.url)
	         elMotor= drResources.get({bundle:"core",key:"appList.txt5"}) 
	      var elGrupo=""
	      if (laApl.grupo!=null)	
	      	elGrupo=oGruposAplicaciones[laApl.grupo].descripcion
	      var nuevoDato={"icon":laApl.icon , "cod":apl , "nombre":elNombre, "descripcion":laDescripcion, "grupo":elGrupo, "numGrupo":laApl.grupo, "motor":elMotor}
	      losDatos[losDatos.length]=nuevoDato
      }
   }
   return losDatos
}


























// ANTIGUO: 
      /*
function funcion_del_usuario_ejecucion (apl)	{
	if (event.button!=1) //boton izq
		return
	with (drDesktopWin)	{
		drAbreAplicacion(apl)
	}
}

function funcion_del_usuario(apl)   {
	if (event.button!=1) //boton izq
		return
   with (parent)  {
   	if (xMenuDerecho!=null && yMenuDerecho !=null)
			generaNuevoIcono(apl, xMenuDerecho - 40, yMenuDerecho - 15)
		else
			generaNuevoIcono(apl, 100, 100)
   }
   //Cierro esta ventana ...
   var idVentanaDesktop=DruidaDameMiIdVentana()
	if (idVentanaDesktop!=null)
		parent.drCierraVentana(idVentanaDesktop);
}*/
/*
function adaptarAltolista()   {
   window.voyACambiarAltoLista=null
   //Estiro el grid
   var nuevaAltura=$.clientCoords().height - 52 //TO-DO[ ] Menos la cabecera
   var p=$('#laLista').get(0).p
   var elGrid=$('#laLista').get(0).grid
   elGrid.bDiv.style.height = nuevaAltura + 'px';
   p.height = nuevaAltura;
   elGrid.fixHeight(nuevaAltura);
}*/
/*
function onRowSelected_generateIcon(id, obj, objPadre, dataRow)   {
   var apl=dataRow.cod
   with (drDesktopWin)  {
   	if (xMenuDerecho!=null && yMenuDerecho !=null)
			generaNuevoIcono(apl, xMenuDerecho - 40, yMenuDerecho - 15)
		else
			generaNuevoIcono(apl, 100, 100)
   }
   //Cierro esta ventana ...
   drWindow.close();
}*/
/*
function onRowSelected_execute(id, obj, objPadre, dataRow) {
   var apl=dataRow.cod
	with (drDesktopWin)	{
		drAbreAplicacion(apl)
	}
}*/
/*
$.clientCoords = function() {
     var dimensions = {width: 0, height: 0};
     if (document.documentElement) {
         dimensions.width = document.documentElement.offsetWidth;
         dimensions.height = document.documentElement.offsetHeight;
     } else if (window.innerWidth && window.innerHeight) {
         dimensions.width = window.innerWidth;
         dimensions.height = window.innerHeight;
     }
     return dimensions;
}

$(document).ready ( function () {
   elOnload();
});
*/