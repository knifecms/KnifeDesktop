function t(label)   {
   if (window.recuentoTiempos==null)
      window.recuentoTiempos=[]
   window.recuentoTiempos[window.recuentoTiempos.length]={date:new Date(), label:label}
}

function ver_t()   {
   var txt="";
   for (var r=1; r<recuentoTiempos.length; r++)
      txt+="\n["+recuentoTiempos[r-1].label+"-"+recuentoTiempos[r].label+"]= "+ (recuentoTiempos[r].date-recuentoTiempos[r-1].date) +"ms"
   txt+="\n"
   txt+="\n["+recuentoTiempos[0].label+"-"+recuentoTiempos[recuentoTiempos.length-1].label+"]= "+ (recuentoTiempos[recuentoTiempos.length-1].date-recuentoTiempos[0].date) +"ms"
   alert(txt)
}

function quieroEjecutar()	{
	if(document.location.search=='?MODO=EJECUCION') //TO-DO: Mejorar esto para hacerlo mas gen�rico
		return true
	return false;
}

function elOnload()  {
   var drDesktopWin = drWindow.getDesktopWindow()
   if (drDesktopWin!=null){
      window.oAplicaciones=drWindow.getApplicationsConfig()
      window.oGruposAplicaciones=drWindow.getApplicationsGroupsConfig()
      }
	quieroEj=quieroEjecutar()
   var laFuncion=((quieroEj)?onRowSelected_execute:onRowSelected_generateIcon)
	var losDatos=dameDatosAplicaciones()
   //Construyo el grid
   $('#laLista').flexigrid({
      width:'auto'
      ,height:'auto'
      ,title: 'Aplicaciones Registradas'
      ,singleSelect: true
      ,colModel : [
         {display: 'C�digo', name : 'cod', width : 80, sortable : true, align: 'center', hide: true},
         {display: '', name : 'img', width : 30, sortable : true, align: 'center'},
         {display: 'Nombre', name : 'nombre', width : 144, sortable : true, align: 'left'},
         {display: 'Descripci�n', name : 'descripcion', width : 282, sortable : true, align: 'left'},
         {display: 'Grupo', name : 'grupo', width : 149, sortable : true, align: 'left'},
         {display: 'Motor', name : 'motor', width : 113, sortable : true, align: 'left', hide:true}
      ]
      ,resizable:false
      ,onRowSelected: laFuncion
   });
   //Si cambia el tama�o estiro la lista en vertical
   $(window).bind("resize", function (ev)  {
      if (window.voyACambiarAltoLista!=null) {
         clearTimeout(window.voyACambiarAltoLista)
      }
      window.voyACambiarAltoLista=setTimeout(adaptarAltolista, 0)
   })
   adaptarAltolista();
   //Meto los datos
   $('#laLista').get(0).grid.addDataBeanArray(losDatos)
}

function dameDatosAplicaciones()	{
   var losDatos=[]
   for (apl in oAplicaciones)  {
      var laApl=oAplicaciones[apl]
      if (laApl.icon) { //SI NO HAY ATRIBUTO DE IMAGEN ENTONCES NO PERMITO CREAR UN ICONO
	      var elNombre=""
	      if (laApl.iconText!=null)
	         elNombre=laApl.iconText
	      else 
	         elNombre=apl
	      var laDescripcion=""
	      if (laApl.description!=null)
	         laDescripcion=laApl.description
	      else if (laApl.title!=null)
	         laDescripcion=laApl.title
	      var elMotor=""
	      if (laApl.engine!=null)
	         elMotor=laApl.engine
	      else if (laApl.js)
	         elMotor="Javascript Directo"
	      else if (laApl.url)
	         elMotor="URL Directa"
	      var elGrupo=""
	      if (laApl.grupo!=null)	
	      	elGrupo=oGruposAplicaciones[laApl.grupo].descripcion
	     	var laImg="<img src=\""+drDesktopWin.urlRaizIconos()+'/'+laApl.icon+"\" style=\"height:17px;width:17px\" align=absmiddle >"
	      var nuevoDato={"img": laImg, "cod":apl , "nombre":elNombre, "descripcion":laDescripcion, "grupo":elGrupo, "motor":elMotor}
	      losDatos[losDatos.length]=nuevoDato
      }
   }
   return losDatos
}
      
function funcion_del_usuario_ejecucion (apl)	{
	if (event.button!=1) //boton izq
		return
	with (drDesktopWin)	{
		drAbreAplicacion(apl)
	}
}

function funcion_del_usuario(apl)   {
	if (event.button!=1) //boton izq
		return
   with (parent)  {
   	if (xMenuDerecho!=null && yMenuDerecho !=null)
			generaNuevoIcono(apl, xMenuDerecho - 40, yMenuDerecho - 15)
		else
			generaNuevoIcono(apl, 100, 100)
   }
   //Cierro esta ventana ...
   var idVentanaDesktop=DruidaDameMiIdVentana()
	if (idVentanaDesktop!=null)
		parent.drCierraVentana(idVentanaDesktop);
}

function adaptarAltolista()   {
   window.voyACambiarAltoLista=null
   //Estiro el grid
   var nuevaAltura=$.clientCoords().height - 52 //TO-DO[ ] Menos la cabecera
   var p=$('#laLista').get(0).p
   var elGrid=$('#laLista').get(0).grid
   elGrid.bDiv.style.height = nuevaAltura + 'px';
   p.height = nuevaAltura;
   elGrid.fixHeight(nuevaAltura);
}

function onRowSelected_generateIcon(id, obj, objPadre, dataRow)   {
   var apl=dataRow.cod
   with (drDesktopWin)  {
   	if (xMenuDerecho!=null && yMenuDerecho !=null)
			generaNuevoIcono(apl, xMenuDerecho - 40, yMenuDerecho - 15)
		else
			generaNuevoIcono(apl, 100, 100)
   }
   //Cierro esta ventana ...
   drWindow.close();
}

function onRowSelected_execute(id, obj, objPadre, dataRow) {
   var apl=dataRow.cod
	with (drDesktopWin)	{
		drAbreAplicacion(apl)
	}
}

$.clientCoords = function() {
     var dimensions = {width: 0, height: 0};
     if (document.documentElement) {
         dimensions.width = document.documentElement.offsetWidth;
         dimensions.height = document.documentElement.offsetHeight;
     } else if (window.innerWidth && window.innerHeight) {
         dimensions.width = window.innerWidth;
         dimensions.height = window.innerHeight;
     }
     return dimensions;
}

$(document).ready ( function () {
   elOnload();
});
