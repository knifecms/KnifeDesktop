$(function(){
   var parametrosUrl=drGetUrlParameters();
   if (parametrosUrl.url!=null)  {
      abreUrl(parametrosUrl.url)
      }
   if ($.browser.mozilla)  {
      iniciaLista(dameDatosLista(), {wpx:"264px", hpx:"96px"})
      setTimeout("iniciaPanel()",0); //importante despues de la lista
   }else{
      iniciaLista(dameDatosLista(), {wpx:"266px", hpx:"98px"})
      iniciaPanel(); //importante despues de la lista
   }
   if (parametrosUrl.application!=null){
      //Selecciono en la lista en donde toque
      var recordASeleccionar=null
      var rs=window.myDataTable.getRecordSet()
      for (var r=0; r<rs.getLength() && recordASeleccionar==null; r++)   {
         var record=rs.getRecord(r)
         if (record.getData("application")==parametrosUrl.application)
            recordASeleccionar=record
      }
      if (recordASeleccionar!=null) {
         window.myDataTable.selectRow(recordASeleccionar)
         ponEtiqueta(recordASeleccionar.getData("desc"))
      } else   {//Pertenece a una aplicación pero sin ayuda
         //TO-DO
      }
   } else if (parametrosUrl.texto!=null)
      ponEtiqueta(parametrosUrl.texto)
   else
      ponEtiqueta("")

})

function abreUrl(url)   {
   if (window.anteriorUrl!=null){
      if (window.anteriorUrl==url)
         return
   }
   window.anteriorUrl=url
   var nuevaUrl="../../../proxy.html?url="+escape(url)
   var elIframe=$("#helpIframe").get(0)
   elIframe.src=nuevaUrl
}

function ponEtiqueta(txt)  {
   $("#etiqueta").html( drResources.get({bundle:"core",key:"helpExplorer.txt1"}) + '<img src="img/barraFlecha.gif" class="barraFlecha">'+txt)
}

function iniciaPanel()  {
   var $panel=$("#panelDeslizante")
   $panel.css({top: -$panel.height()+27})
   $("#btnBarra").click(function(){
      var $panel=$("#panelDeslizante")
      var nuevoValorAbierto=!$panel.data("abierto")
      if (nuevoValorAbierto){
         //$panel.data("seleccionados",window.myDataTable.getSelectedRows())
         abrePanel()
         }
      else {
         cierraPanel()
         //var antSelecc=$panel.data("seleccionados")
         var actSelecc=window.myDataTable.getSelectedRows()
         if (actSelecc.length>0) {
            var elRecord=window.myDataTable.getRecord(actSelecc[0])
            ponEtiqueta(elRecord.getData("desc"))
            abreUrl(elRecord.getData("url"))
            }
         }
   });
}

function abrePanel()  {
   var $panel=$("#panelDeslizante")
   $panel.data("abierto",(true))
   $("#btnBarra").get(0).src="img/barra3_2.gif"
   $panel.stop().animate({top:"0"}, 400);
}

function cierraPanel()  {
   var $panel=$("#panelDeslizante")
   $panel.data("abierto",(false))
   $("#btnBarra").get(0).src="img/barra3.gif"
   $panel.stop().animate({top:-$panel.height()+27}, 400);
}