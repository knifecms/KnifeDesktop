function dameDatosLista()  {
   var lista=drWindow.getApplicationsWithHelp()
   var ret=[]
   for (var r=0; r<lista.length; r++)   {
      var soloApp=false
      var elemento=lista[r]
      if (elemento.title!=null)  
         var name=elemento.title
      else if (elemento.iconText!=null)
         var name=elemento.iconText
      else if (elemento.description!=null)
         var name=elemento.description
      else {
         var name=elemento.application
         var soloApp=true
         }
      var application=null
      if (elemento.application)
         application=elemento.application
      if (soloApp)
         ret[ret.length]={name:name, url:elemento.processedHelpUrl, desc:name,application:application}
      else
         ret[ret.length]={name:elemento.application+" - "+name, url:elemento.processedHelpUrl, desc:name, application:application}
   }
   return ret;
}

function iniciaLista(datos,tam)  {
   window.myColumnDefs = [
      {key:"name", width:247, label: drResources.get({bundle:"core",key:"helpExplorer.txt2"}) , sortable:true, resizeable:false}
   ];
   var myDataSource = new YAHOO.util.DataSource(datos);
   myDataSource.responseType = YAHOO.util.DataSource.TYPE_JSARRAY;
   myDataSource.responseSchema = { fields: ["name","url","desc","application"] };
   window.myDataTable = new YAHOO.widget.DataTable("contenedorLista", myColumnDefs, myDataSource, {sortedBy:{key:"name"}, selectionMode:"single",  MSG_EMPTY:" ", initialLoad:true, draggableColumns:true, scrollable:true, width:tam.wpx, height:tam.hpx});
   window.myDataTable.set("COLOR_COLUMNFILLER","transparent");
   // Subscribe to events for row selection   
   window.myDataTable.subscribe("rowMouseoverEvent", myDataTable.onEventHighlightRow);
   window.myDataTable.subscribe("rowMouseoutEvent", myDataTable.onEventUnhighlightRow);
   window.myDataTable.subscribe("rowSelectEvent", myDataTable.clearTextSelection );
   window.myDataTable.subscribe("theadCellDblclickEvent", myDataTable.clearTextSelection );
   window.myDataTable.subscribe("theadCellClickEvent", myDataTable.clearTextSelection );

   window.myDataTable.subscribe("rowMousedownEvent", myDataTable_rowMousedownEvent);
   window.myDataTable.subscribe("rowDblclickEvent", onRowDblclickEvent);
   window.myDataTable.render();
   window.myDataTable.sortColumn( window.myDataTable.getColumn ("name") )
   window.myDataTable.focus();
}

function myDataTable_rowMousedownEvent(ev){
   myDataTable.onEventSelectRow(ev);
   window.setTimeout("myDataTable.clearTextSelection()",0);
}

function onRowDblclickEvent(oArgs) {
   window.myDataTable.clearTextSelection();
   var elRecord=window.myDataTable.getRecord(oArgs.target)
   cierraPanel()
   ponEtiqueta(elRecord.getData("desc"))
   abreUrl(elRecord.getData("url"))
}