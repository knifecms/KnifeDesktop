function gid(id) {
	try {
		return(document.getElementById(id));
	} catch(e) {
		alert("ERROR: Identificador "+id+" no encontrado!");
	}
}
function gidset(id,html) { gid(id).innerHTML=html; }
function gidval(id) { return(document.getElementById(id)?document.getElementById(id).value:false); }
function show(id) { gid(id).style.display="block"; }
function hide(id) { gid(id).style.display="none"; }
function view(id) { gid(id).style.visibility="visible"; }
function hidden(id) { gid(id).style.visibility="hidden"; }
function isshow(id) { if (gid(id).style.display=="block") return(true); else return(false); }
function isvisible(id) { if (gid(id).style.display!="none") return(true); else return(false); }
function isfunction(cfunction) { if ((typeof cfunction)=='function') return(true); }

function ietruebody() {
	return(
		(document.compatMode && document.compatMode!="BackCompat")
		?document.documentElement
		:document.body);
}

function setCookie(name,value,days) {
	var expires="";
	if (days) {
		var date=new Date();
		date.setTime(date.getTime()+(days*86400000));
		expires="; expires="+date.toGMTString();
	}
	document.cookie=name+"="+value+expires+"; path=/";
}

function getCookie(name) {
	var nameEQ=name+"=";
	var ca=document.cookie.split(';');
	for (i=0;i<ca.length;i++) {
		var c=ca[i];
		while (c.charAt(0)==' ') c=c.substring(1,c.length);
		if (c.indexOf(nameEQ)==0) return(c.substring(nameEQ.length,c.length));
	}
	return("");
}

function delCookie(name) {
	setCookie(name,"",-1);
}

function isie() {
	var ag="ua: "+navigator.userAgent;
	if (ag.indexOf("MSIE")>0) return(true);
	else return(false);
}

function ismoz() {
	var ag="ua: "+navigator.userAgent;
	if (ag.indexOf("Firefox")>0) return(true);
	else return(false);
}

// Parche al �nico fallo del FireFox que siguen sin solucionar
// al cambiar el tama�o de un table con height especificado,
// este no se autoredimensiona al disminuir su contenido.
// este problema suele aparecer al desarrollar aplicaciones AJAX.
function fffix(id) {
	gid(id).height="99%";
	setTimeout('gid("'+id+'").height="100%";',1);
}
