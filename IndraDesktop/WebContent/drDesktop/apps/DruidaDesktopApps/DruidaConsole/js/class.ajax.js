/**

	T�tulo..: VeryTinyAJAX 0.2d, Wrapper JavaScript simple a funciones XMLHTTP para AJAX
  Licencia: GPL (http://www.gnu.org/licenses/gpl.txt)
	Autores.: Pablo Rodr�guez Rey (mr.xkr -en- inertinc -punto- org)
	          http://mr.xkr.inertinc.org/
            Javier Gil Motos (cucaracha -en- inertinc -punto- org)
            http://cucaracha.inertinc.org/

	Agradecimientos a Cucaracha, por darme inter�s en el desarrollo de webs usando
	AJAX y proveerme del ejemplo b�sico con el que est� desarrollada esta librer�a.

*/


// declarar el objeto XML-HTTP global
var http;

// constantes para httpRequest
var hGET=0;
var hPOST=1;

// funciones auxiliares generales
function gid(id) { return(document.getElementById(id)); }
function gescape(torg) {
	var d=""+torg;
	try { var d=d.replace(/\?/gi,"%3F"); } catch(e) {}
	try { var d=d.replace(/&/gi,"%26"); } catch(e) {}
	try { var d=d.replace(/\+/gi,"%2B"); } catch(e) {}
	try { var d=d.replace(/ /gi,"+"); } catch(e) {}
	return(d);
}

// informaci�n de versi�n
function httpVersion() { return("VeryTinyAJAX/0.2d"); }

// crea el objeto XML-HTTP
function httpObject() {
	var xmlhttp;
	// comprobar que el navegador soporta XMLHttpRequest
	try { xmlhttp = new ActiveXObject("Msxml2.XMLHTTP"); }
	catch (e) { try { xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); }
	catch (e) { try { xmlhttp = new XMLHttpRequest(); }
	catch (e) { xmlhttp = false; } } }
	return(xmlhttp);
}

// estado de la petici�n

function httpState() {
	try { return(http.readyState); }
	catch (e) { return(5); }
}

// cadena de estado de la petici�n

function httpStateString() {
	try { var rs=http.readyState; }
	catch (e) { var rs=5; }
	switch (rs) {
	case 0: return("Uninitialized")
	case 1: return("Loading");
	case 2: return("Loaded");
	case 3: return("Interactive");
	case 4: return("Complete");
	case 5: return("Server Crashed");
	}
}

// Indicar si se ha completado la operaci�n

function httpComplete() {
	if (http.readyState==4) return(true);
	else return(false);
}

// Devolver los datos recibidos
function httpData() {
	return(http.responseText);
}

// Devolver los datos recibidos en formato documento XML
function httpXML() {
	return(http.responseXML);
}

// Devolver el estado del servidor
// Si se detecta error, el servidor no estar� disponible
function httpStatus() {
	try { return(http.status); }
	catch(e) { return(0); }
}

// Comprobar que la respuesta del servidor es la 200 (HTTP OK)
function httpError() {
	if (http.readyState==4) {
		try { var ok=(http.status!=200); }
		catch(e) { return(true); }
		return(ok);
	}
}

// Muestra un mensaje de error dependiendo del tipo de error encontrado
function httpErrorShow() {
	if (httpError()) {
		if (httpStatus()) alert("Se ha encontrado el error "+httpStatus()+" en el servidor.");
		else alert("El servidor no responde a la petici�n!\nPruebe dentro de unos instantes.");
	}
}

// Realizar un env�o de datos http
function httpSend(method, url, data, eventfunction) {
//alert(method);
//alert(url);
//alert(data);
//alert(eventfunction);
	var sdata=(data?data:"");
	var async=(eventfunction?true:false);
	http=httpObject();
	switch (method) {
	case 0: http.open("GET",url+"?"+sdata,async); sdata=null; break;
	case 1: http.open("POST",url,async); break;
	default: return(false);
	}
	if (async) http.onreadystatechange=eventfunction;
	http.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=ISO-8859-1"');
	http.send(sdata);
	return(true);
}

// Devuelve todos los campos y datos
// de un formulario tipo campo1=dato1&campo2=dato2&...
function httpFormFields(formObject) {
	var fields="";
	for (x=0;x<formObject.length;x++) {
		var val="";
		if (formObject[x].name) {
			switch (formObject[x].type) {
			case "checkbox": val=(formObject[x].checked?"1":"0"); break;
			case "button": case "select-one": case "text": case "textarea":
			default: val=formObject[x].value;
			}
			fields=fields+(x>0?"&":"")+formObject[x].name+"="+escape(val);
		}
	}
	return(fields);
}

// Habilitar o deshabilitar la posibilidad de introducci�n
// o modificaci�n de datos de un formulario completo.
function httpFormFieldsEnabled(formObject,isEnabled) {
	for (x=0;x<formObject.length;x++) {
		try { formObject[x].disabled=isEnabled; }
		catch(e) {}
	}
}

// Devuelve un objeto con el n�mero de variables
// creadas con la funci�n aset(nombre,valor);
function aget(fullData) {
	var d=fullData;
	var o=new Object();
	var n=0;
	while (true) {
		try {
			p0=d.indexOf("="); if (!p0) break;
			p1=d.indexOf("("); if (!p1) break;
			p2=d.indexOf(")"); if (!p2) break;
			agetname=d.substring(0,p1); if (!agetname) break;
			plength=parseInt(d.substring(p1+1,p2));
			agetdata=d.substring(p0+1,p0+plength+1);
			eval("o."+agetname+"=agetdata;");
			d=d.substring(p0+plength+2);
		} catch(e) {
			return(false);
		}
	}
	return(o);
}
