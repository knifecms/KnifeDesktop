<%@page import="java.io.FileReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="es.indra.druida.desktop.configurator.DesktopConfig"%>
<%@page import="es.indra.druida.desktop.configurator.DesktopUserConfig"%>
<%@page import="java.io.File"%>
<%

DesktopConfig _config = DesktopConfig.getSingleton();
DesktopUserConfig userConfig = (DesktopUserConfig) request.getSession().getAttribute("userConfig");
String usuario = userConfig.getId();

String _dir = _config.get("userRoot")+"/"+usuario;

if (request.getSession().getAttribute("DrdDirActual")!=null)
	_dir=(String)request.getSession().getAttribute("DrdDirActual");
else
	request.getSession().setAttribute("DrdDirActual",_dir);


//StringBuffer cadena=new StringBuffer("<getprompt><span style='color:#0FF;'>[</span>").append(_config.getUsuario()).append("<span style='color:#0FF;'>@</span>"+_dir+"<span style='color:#0FF;'>]</span></getprompt><getcwd></getcwd>");
StringBuffer cadena=new StringBuffer("<getprompt><span style='color:#0FF;'>[</span>").append(usuario).append("<span style='color:#0FF;'>]</span></getprompt><getcwd></getcwd>");

System.out.println("Comando: "+request.getParameter("cmd"));

if (request.getParameter("cmd").equals("help")){
	cadena=new StringBuffer("The available commands for <b>").append(usuario).append("</b> are: ls, cd, su, more, pwd, clear, help<br><getprompt><span style='color:#0FF;'>[</span>"+usuario+"<span style='color:#0FF;'>]</span></getprompt><getcwd></getcwd>");
}else if (request.getParameter("cmd").startsWith("su")){
	cadena=new StringBuffer("<span style='color:red;'>Password incorrect.</span><br><getprompt><span style='color:#0FF;'>[</span>").append(usuario).append("<span style='color:#0FF;'>]</span></getprompt><getcwd></getcwd>");
}else if (request.getParameter("cmd").equals("ls") || request.getParameter("cmd").equals("dir")){
	File _fActual = new File(_dir);
	cadena=new StringBuffer("");
	if (_fActual.isDirectory()){
		String[] _ficheros=_fActual.list();
		int _numFicheros=_ficheros.length;
		for (int i=0;i<_numFicheros;i++) {
			File _fLista=new File(_dir+"/"+_ficheros[i]);
			if (_fLista.isDirectory())
				cadena.append("<span style='color:#0F0;'>").append(_ficheros[i]).append("</span>\r\n");
			else
				cadena.append("<span style='color:#0FF;'>").append(_ficheros[i]).append("</span>\r\n");
		}
	}
	//cadena.append("<br><span style='background-color:#008;color:#FFF;'>Type <b>go</b> or <b>tab</b> to enter this page.</span><br><getprompt><span style='color:#0FF;'>[</span>").append(_config.getUsuario()).append("<span style='color:#0FF;'>]</span></getprompt><getcwd></getcwd>");
	cadena.append("<br><getprompt><span style='color:#0FF;'>[</span>").append(usuario).append("<span style='color:#0FF;'>]</span></getprompt><getcwd></getcwd>");
}else if (request.getParameter("cmd").startsWith("cd")){
	if (request.getParameter("cmd").length()>2){
		String _dirTempo=_dir+"/"+(request.getParameter("cmd").substring(2).trim());
		//System.out.println(_dirTempo);
		File _fg=new File(_dirTempo);
		if (_fg.exists()){
			//System.out.println("............."+_fg.getCanonicalPath());
			_dir=_fg.getCanonicalPath();
			request.getSession().setAttribute("DrdDirActual",_dir);
			cadena=new StringBuffer("<getprompt><span style='color:#0FF;'>[</span>").append(usuario).append("<span style='color:#0FF;'>]</span></getprompt><getcwd></getcwd>");
		}else{
			cadena=new StringBuffer("<span style='color:red;'>The dir ").append(request.getParameter("cmd").substring(2).trim()).append(" not exists.</span><br><getprompt><span style='color:#0FF;'>[</span>").append(usuario).append("<span style='color:#0FF;'>]</span></getprompt><getcwd></getcwd>");
		}
	}
}else if (request.getParameter("cmd").equals("pwd")){
	cadena=new StringBuffer("<span style='color:#0FF;'>").append(_dir).append("</span><br><getprompt><span style='color:#0FF;'>[</span>").append(usuario).append("<span style='color:#0FF;'>]</span></getprompt><getcwd></getcwd>");
}else if (request.getParameter("cmd").startsWith("more")){
	cadena=new StringBuffer("");
	if (request.getParameter("cmd").length()>4){
		File _fFichero=new File(_dir+"/"+(request.getParameter("cmd").substring(4).trim()));
		//System.out.println(_dir+"/"+(request.getParameter("cmd").substring(4).trim()));
		if (_fFichero.isFile()) {
			BufferedReader in= new BufferedReader(new FileReader(_dir+"/"+(request.getParameter("cmd").substring(4).trim())));
			String _s="";
			while((_s=in.readLine())!=null){
				//System.out.println(_s);
				cadena.append("<br><span style='color:#0FF;'>").append(_s).append("</span>");
			}
		}
		
	}
	cadena.append("<br><getprompt><span style='color:#0FF;'>[</span>").append(usuario).append("<span style='color:#0FF;'>]</span></getprompt><getcwd></getcwd>");
} else if (request.getParameter("cmd").length()>0){
	cadena=new StringBuffer("<span style='color:red;'>Command ").append(request.getParameter("cmd")).append(" not found.</span><br><getprompt><span style='color:#0FF;'>[</span>").append(usuario).append("<span style='color:#0FF;'>]</span></getprompt><getcwd></getcwd>");	
}
%>


<%= cadena.toString() %>