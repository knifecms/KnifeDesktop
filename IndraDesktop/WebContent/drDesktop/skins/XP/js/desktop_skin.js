oSkinFactory.createSkin({
   id:"XP"
   ,marginTop:0
   ,marginBottom:34
   ,backgroundImage:"" //par�metro del skin
   ,backgroundColor:"#004e98" //par�metro del skin
   ,expandWallpaper:false //par�metro del skin
   ,startButtonLabel:"Inicio" //par�metro del skin

   // ------------------
   ,taskbar:{
      marginBottom:2
      ,marginLeft:122
      ,marginRight:23
      ,buttonHeight:25
      ,getHtmlButtonsFunction:oSkinFactory.getHtmlDefaultTaskbarButtonsWithIcons  
      ,scrollbarColors:{face:"#3a80f4",shadow:"#3977d8",highLight:"#6494e0",c3dLight:"#aac4ee",darkShadow:"#2b6dd5",track:"#cfdef5",arrow:"#ffffff"}
   }
   ,startMenu:{
      hOffset:-5
      ,vOffset:0
      ,vOffset_lv_0:4
      ,vOffset_lv_1:63
      ,getHtmlFunction:function (opciones, nivel)   { //Lo llamo para cada nivel (0 primer nivel)
         if (nivel==0)  {
            var menuHtmlOpciones=oSkinFactory.getHtmlStartMenuOptions({options:opciones, level:nivel})
            var baseHtml=oSkinFactory.getHtmlStartMenu3x1x3Chrome({htmlContent:menuHtmlOpciones})
            var custom=XP_SKIN_getCustomHtmlstartMenu()
            return baseHtml+custom //construyo el marco alrededor de las opciones
         } else   {
            var menuHtmlOpciones=oSkinFactory.getHtmlStartSubmenuOptions({options:opciones, level:nivel})
            return oSkinFactory.getHtmStartSubmenu2BordersChrome({htmlContent:menuHtmlOpciones}) //construyo el marco alrededor de las opciones
         }
      }
   }
   // ------------------
   ,initSkinFunction:function () { 
      oSkinFactory.initSkin(oSkin_XP); //Inicializa el skin 
      oSkinFactory.buildDefaultBackground({containerId:"dDesktop",oSkin:oSkin_XP}); //construye el background
   }

   // ------------------
   ,getHtmlTaskbarFunction:function ()	{
      //Requisitos para personalizaci�n: 
      // onclick del bot�n de inicio ha de llamar a muestraOcultaMenu()
      return oSkinFactory.getHtmlDefaultTaskbar({oSkin:oSkin_XP})
   }

   // ------------------
   ,getHtmlWindowFunction: function (wName, wParams) {
      //Requisitos para personalizaci�n: 
      // - ocupar� el 100% en ancho y alto
      // - el contenedor del titulo tiene id=\""+wName+"_wTitle\"
      // - el contenedor del iframe se estira a lo m�ximo autom�ticamente y tiene id=\""+wName+"iTD\"
      // - el boton de minimizar tiene onmouseover=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'M1'})\" onmouseout=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'M0'})\"
      // - el boton de maximizar tiene onmouseover=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'X1'})\" onmouseout=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'X0'})\"
      // - el boton de cerrar tiene onmouseover=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'C1'})\" onmouseout=\"drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'C0'})\"
      if (wParams.widget)
         return oSkinFactory.getHtmlDefaultWidget(wName,wParams);
      //return oSkinFactory.getHtmlDefault3x1x3Chrome({wName:wName, wParams:wParams, oSkin:oSkin_XP})
      return oSkinFactory.getHtmlDefault3xNWithIconTitleAndButtonsChrome({wName:wName, wParams:wParams, oSkin:oSkin_XP})
   }
   ,onWindowChangeFocusStatus: function(args) {
      XP_SKIN_onWindowChangeFocusStatus(args)
   }
   ,windowChromeWidth:8 //Estos valores los sabe calcular din�micamente el escritorio si no est�n informados aunque es menos �ptimo
   ,windowChromeHeight:39
})





// ---------------

function XP_SKIN_getCustomHtmlstartMenu() {  //A�ado el usuario y los botones de cerrar al menu de inicio
   if (window.userNameCache==null)
      window.userNameCache=drWindow.getUserData().id;
   var html=[]
   html[html.length]="<div class=\"startMenuCustomUserLabel\">"
   html[html.length]= window.userNameCache
   html[html.length]="</div>"
   html[html.length]="<div class=\"startMenuCustomButtonsContainer\" onmouseover=\"$('#OPS_MENU_0 TR').removeClass('startMenu_fatherRow_rolloverRow').removeClass('startMenu_rolloverRow')\">"
   html[html.length]="  <div class=\"startMenuCustomCloseBtn\" onmousedown=\"preguntaYCierra()\"></div>"
   html[html.length]="  <div class=\"startMenuCustomExitBtn\" onmousedown=\"preguntaYSalSinGuardar()\"></div>"
   html[html.length]="</div>"
   return html.join('');
}

function XP_SKIN_onWindowChangeFocusStatus(args)   {
   if (args.focus)   {
      $("#"+args.id+"_wTitle").addClass("chromeTitleFocused").removeClass("chromeTitle")
      $("#taskbarButton_"+args.id).addClass("taskbarButtonFocused").removeClass("taskbarButton")
   } else {
      $("#"+args.id+"_wTitle").addClass("chromeTitle").removeClass("chromeTitleFocused")
      $("#taskbarButton_"+args.id).addClass("taskbarButton").removeClass("taskbarButtonFocused")
   }
}
