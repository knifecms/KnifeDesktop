oSkinFactory.createSkin({
   id:"GNOME"
   ,marginTop:24
   ,marginBottom:24
   ,backgroundImage:"" //par�metro del skin
   ,backgroundColor:"" //par�metro del skin
   ,expandWallpaper:false //par�metro del skin
   ,startButtonLabel:drResources.get({bundle:"skins",key:"gnomeSkin.txt1"}) //"Applications" //par�metro del skin
   // ------------------
   ,taskbar:{
      marginBottom:0 //separacion de los botones (de la barra de botones) con el margen inferior de la pantalla.
      ,marginLeft:34
      ,marginRight:88  //Margen adicional a la derecha del grupo de botones (adicional al espacio del widget si lo hay)
      ,buttonHeight:24  //Altura de un boton
      ,getHtmlButtonsFunction:GNOME_getHtmlTaskbarButtonsWithIcons
   }
   ,startMenu:{
      align:"top"
      ,hOffset:-2 //Desplazamiento horizontal de un men� sobre el anterior
      ,vOffset:0  //Desplazamiento generico vertical de un men� sobre el anterior
      ,vOffset_lv_0:-1  //Desplazamiento generico vertical de un men� sobre el anterior
      ,getHtmlFunction:GNOME_startMenu_getHtmlFunction
   }
   // ------------------
   ,initSkinFunction:function () { 
      oSkinFactory.initSkin(oSkin_GNOME); //Inicializa el skin 
      GNOME_buildBackground({containerId:"dDesktop",oSkin:oSkin_GNOME}); //construye el background
   }

   // ------------------
   ,getHtmlTaskbarFunction:function (){
      return GNOME_getHtmlTaskbar({oSkin:oSkin_XP})
      } 

   // ------------------
   ,getHtmlWindowFunction: function (wName, wParams) {
      if (wParams.widget)
         return oSkinFactory.getHtmlDefaultWidget(wName,wParams);
      return GNOME_getHtml3xNWithIconTitleAndButtonsChrome({wName:wName, wParams:wParams, oSkin:oSkin_GNOME})
   }
   ,onWindowChangeFocusStatus: oSkinFactory.defaultOnWindowChangeFocusStatus
   ,onReady:GNOME_bgScripts
})




// men�

function GNOME_startMenu_getHtmlFunction (opciones, nivel)   { //Lo llamo para cada nivel (0 primer nivel)
   var menuHtmlOpciones=oSkinFactory.getHtmlStartSubmenuOptions({options:opciones, level:nivel,iconWidth:25, iconHeight:25})
   if (nivel==0)  {
      html=[]
      html[html.length]="<div  class=\"startSubmenuOutherBorder\" style=\"width:180px\">";
      html[html.length]="  <div class=\"startSubmenuInnerBorder\">";
      html[html.length]= menuHtmlOpciones
      html[html.length]="  </div>";
      html[html.length]="</div>";
      return html.join('');
   } else   {
      return oSkinFactory.getHtmStartSubmenu2BordersChrome({htmlContent:menuHtmlOpciones}) //construyo el marco alrededor de las opciones
   }
}

// ventana

function GNOME_getHtml3xNWithIconTitleAndButtonsChrome(args)  {
   var wName=args.wName
   var titulo=drDesktop.wData({id:wName, property:"wTitle"})
   var icono=oSkinFactory.getIconPath(wName)
   var wParams=args.wParams
   html=[]
   html[html.length]="<table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"chromeContainer\">";
   html[html.length]="  <tr>";
   html[html.length]="    <td  height=\"1\">";
   html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" >";
   html[html.length]="        <tr>";
   if (icono!=null)
      html[html.length]="          <td><div class=\"chrome11\"><img class=\"chromeIcon\" src=\""+icono+"\" width=\"16\" height=\"16\"></div></td>";
   else
      html[html.length]="          <td><div class=\"chrome11\"></div></td>";
   html[html.length]=         "<td class=\"chrome12\" width=\"100%\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed\"><tr>"
   html[html.length]=            "<td class=\"chromeTitle\" id=\""+wName+"_wTitle\">"
   html[html.length]=               "<div class=\"chromeTitleText\">"
   html[html.length]=                  titulo
   html[html.length]=                  "<div class=\"chromeTitleBright\">"+titulo+"</div>"
   html[html.length]=               "</div>"
   html[html.length]=            "</td>"
   html[html.length]=         "</tr></table></td>";
   if (drWindow.hasHelp({params:wParams}))
      html[html.length]="          <td><div class=\"chromeBtnHelp\" onmouseover=\"$(this).addClass('chromeBtnHelpHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'I1'})\" onmouseout=\"$(this).removeClass('chromeBtnHelpHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'I0'})\"></div></td>";
   html[html.length]="          <td><div class=\"chromeBtnMin\" onmouseover=\"$(this).addClass('chromeBtnMinHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'M1'})\" onmouseout=\"$(this).removeClass('chromeBtnMinHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'M0'})\"></div></td>";
   html[html.length]="          <td><div class=\"chromeBtnMax\" onmouseover=\"$(this).addClass('chromeBtnMaxHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'X1'})\" onmouseout=\"$(this).removeClass('chromeBtnMaxHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'X0'})\"></div></td>";
   html[html.length]="          <td><div class=\"chromeBtnClose\" onmouseover=\"$(this).addClass('chromeBtnCloseHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'C1'})\" onmouseout=\"$(this).removeClass('chromeBtnCloseHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'C0'})\"></div></td>";
   html[html.length]="          <td><div class=\"chrome13\"></div></td>";
   html[html.length]="        </tr>";
   html[html.length]="      </table>";
   html[html.length]="    </td>";
   html[html.length]="  </tr>";
   html[html.length]="  <tr>";
   html[html.length]="    <td>";
   html[html.length]="      <table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
   html[html.length]="        <tr>";
   html[html.length]="          <td><div class=\"chrome21\"></div></td>";
   html[html.length]="          <td class=\"chrome22\" width=\"100%\"><div id=\""+wName+"iTD\" style='width:100%; height:100%'></div></td>"; //un div para que el background sea opaco al arrastrar otras
   html[html.length]="          <td><div class=\"chrome23\"></div></td>";
   html[html.length]="        </tr>";
   html[html.length]="      </table>";
   html[html.length]="    </td>";
   html[html.length]="  </tr>";
   html[html.length]="  <tr>";
   html[html.length]="    <td height=\"1\">";
   html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
   html[html.length]="        <tr>";
   html[html.length]="          <td><div class=\"chrome31\"></div></td>";
   html[html.length]="          <td class=\"chrome32\" width=\"100%\"></td>";
   html[html.length]="          <td><div class=\"chrome33\"></div></td>";
   html[html.length]="        </tr>";
   html[html.length]="      </table>";
   html[html.length]="    </td>";
   html[html.length]="  </tr>";
   html[html.length]="  </table>";
   return html.join('')
}


// Barra de botones

function getAnchoDeBoton(args){
   var anchoDeseado=args.anchoDeseado, numBotones=args.numBotones, anchoTotal=args.anchoTotal, anchoMinimo=args.anchoMinimo
   if (anchoDeseado*numBotones < anchoTotal)
      return {ancho:anchoDeseado, anchoPrimero:anchoDeseado}
   anchoDividido=Math.floor(anchoTotal/numBotones)
   if (anchoDividido>=anchoMinimo)
      return {ancho:anchoDividido, anchoPrimero:(anchoDividido+anchoTotal-(anchoDividido*numBotones)) }
   anchoTotal=anchoTotal-16 //(scroll)
   var conAnchoMinimoCaben=Math.floor(anchoTotal/anchoMinimo)
   var nuevoAncho= Math.floor(anchoTotal/conAnchoMinimoCaben)
   return {ancho:nuevoAncho, anchoPrimero:(nuevoAncho+anchoTotal-(nuevoAncho*conAnchoMinimoCaben)) }
}

function GNOME_getHtmlTaskbarButtonsWithIcons(wins){
   //var registroVentanas = drWindow.getRegistry()
   var indiceAplicaciones = drWindow.getApplicationsConfig()
   var datosAnchoBoton=getAnchoDeBoton({anchoDeseado:250, numBotones:wins.length, anchoTotal:$(".taskbarButtonsContainer").width(), anchoMinimo:100})
   var anchoBoton=datosAnchoBoton.ancho
   var anchoPrimerBoton=datosAnchoBoton.anchoPrimero
   var margenSuperior=($.browser.mozilla)?8:0;
   html=[];
   for (var i=0;i<wins.length;i++) {
      var app=null;
      var winName = wins[i];
      var winTitle = DrEscHTML( drDesktop.wData({id:winName, property:"wTitle"}) );
      //var datosApl=registroVentanas.getVentana(winName)
      var datosApl=drWindow.getDrDesktopObj().wData({id:winName, property:"wOpenArgs"})
      if (datosApl!=null) 
         var app=datosApl.application
      if (app!=null) 
         var iconPath=drWindow.getDesktopWindow().urlRaizIconos() + "/" +indiceAplicaciones[app].icon;
      else
         var iconPath=drWindow.getDesktopWindow().urlIconoPorDefecto()
      var anchoAUsar=((i==0)?anchoPrimerBoton:anchoBoton)
      html[html.length]="<div id=\"taskbarButton_" + winName + "\" onclick=\"drDesktop.taskbar_btnClick({id:'"+winName+"'})\" class=\"taskbarButton\" style=\"margin-top:"+margenSuperior+"px;height:24px;width:"+anchoAUsar+"px\" title=\""+winTitle+"\">;	";
      html[html.length]="   <div class=\"taskbarButton1\"></div>";
      html[html.length]="   <div class=\"taskbarButton3\" style=\"left:"+(anchoAUsar-8)+"px\"></div>";
      html[html.length]="   <img class=\"taskbarButtonIcon\" src=\""+iconPath+"\" width=\"15\" height=\"15\" align=\"absmiddle\">";
      html[html.length]="   <div class=\"taskbarButtonLabel\" style=\"width:"+(anchoAUsar-40)+"\">"+winTitle+"</div>";
      html[html.length]="</div>"
   }
   return html.join("")
}



// BACKGROUND ------------------

function GNOME_buildBackground(args){ //{containerId:"dDesktop",oSkin:oSkin_PRUEBA}
   var html=GNOME_getHtmlBackground(args)
   oSkinFactory.putHtmlBackground({html:html,containerId:args.containerId})
}

function GNOME_getHtmlBackground(args){ //{oSkin:oSkin_PRUEBA}
   var html=[]
   html[html.length]=GNOME_getHtmlBackgroundImage(args)
   html[html.length]="<div class=\"topBar\">";
   html[html.length]="   <div id=\"barra1btnInicio\" class=\"barra1btnInicio\"><div class=\"icoBtnInicio\"></div><div class=\"labelBtnInicio\">"+drResources.get({bundle:"skins",key:"gnomeSkin.txt1"})+"</div></div>";
   if (drWindow.getApplicationExists({application:"drExplorer"}))
      html[html.length]="   <div id=\"barra1btnFilesystem\" class=\"barra1btnInicio\"><div class=\"labelBtnInicio\">"+drResources.get({bundle:"skins",key:"gnomeSkin.txt2"})+"</div></div>";
   html[html.length]="   <div class=\"barraRightGroup\">";
   html[html.length]="      <div class=\"barra1Sep\"></div>";
   html[html.length]="      <div id=\"dateLabel\"></div>";
   html[html.length]="      <div id=\"barra1BtnClose\"></div>";
   html[html.length]="   </div>";
   html[html.length]="</div>";
   return html.join('')
}


function GNOME_getHtmlBackgroundImage(args){
   var oSkin=args.oSkin;
   var skinParams=drWindow.getSkinParameters()
   var userBackgroundImage="",backgroundColor="";
   userBackgroundImage=(oSkin.userBackgroundImage!=null)?oSkin.userBackgroundImage:userBackgroundImage
   backgroundColor=(oSkin.backgroundColor!=null)?oSkin.backgroundColor:backgroundColor
   userBackgroundImage=(skinParams.userBackgroundImage!=null)?skinParams.userBackgroundImage:userBackgroundImage
   backgroundColor=(skinParams.backgroundColor!=null)?skinParams.backgroundColor:backgroundColor
   if(userBackgroundImage!="" || backgroundColor!="")
      return oSkinFactory.getHtmlDefaultBackground(args)
   //Si no hay backgroundImage ni backgroundColor entonces aplico el valor de "predefinedBackground"
   var oSkin=args.oSkin;predefinedBackground=0;
   predefinedBackground=(skinParams.predefinedBackground!=null)?skinParams.predefinedBackground:predefinedBackground+""
   var html=[]
   if (predefinedBackground=="0"){
      html[html.length]='<img id="fondo" style="position:absolute; width:100%; height:100%;  left: 0px; top: 0px; z-index:1;" src="'+drWindow.getSkinUrl()+'/css/background/fondoGnome0.jpg">';
   } else if (predefinedBackground=="1"){
      html[html.length]='<img id="fondo" style="position:absolute; width:100%; height:100%;  left: 0px; top: 0px; z-index:1;" src="'+drWindow.getSkinUrl()+'/css/background/fondoGnome1.jpg">';
   } else if (predefinedBackground=="2"){
      html[html.length]='<img id="fondo" style="position:absolute; width:100%; height:100%;  left: 0px; top: 0px; z-index:1;" src="'+drWindow.getSkinUrl()+'/css/background/fondoGnome2.jpg">';
      html[html.length]='<div style="position:absolute; width:100%; height:100%;  left: 0px; top: 0px; z-index:1;"><table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" style="z-index:1"><tr><td align="center" valign="middle">';
      html[html.length]='<img src="'+drWindow.getSkinUrl()+'/css/background/gnomeLogo.png">';
      html[html.length]='</td></tr></table></div>";';
   } else if (predefinedBackground=="3"){
      html[html.length]='<div id="fondo" style="position:absolute; width:100%; height:100%;  left: 0; top: 0; background-color:#805b8a;background-image: url('+drWindow.getSkinUrl()+'/css/background/fondoGnome3.jpg);background-repeat: no-repeat;	background-position:bottom left;"></div>';
   }
   return html.join('');
}

function GNOME_getHtmlTaskbar(args)  {
   var oSkin=args.oSkin;
   html=[]
   html[html.length]="<div class=\"bottomBar\">";
   html[html.length]="   <div id=\"barra2BtnDesktop\"></div>";
   html[html.length]="   <div class=\"barra2Sep\"></div>";
   html[html.length]="   <div class=\"barraRightGroup\">";
   html[html.length]="      <div id=\"barra2BtnVD1\" class=\"btnVD VDSelected\"></div>";
   html[html.length]="      <div id=\"barra2BtnVD2\" class=\"btnVD\"></div>";
   html[html.length]="      <div id=\"barra2BtnVD3\" class=\"btnVD\"></div>";
   html[html.length]="   </div>";
   html[html.length]="</div>"
   return html.join('')
}

function GNOME_bgScripts() { //Activamos todos los botones
   $("#barra1btnInicio").hover(function(){
      $(this).addClass("barra1btnInicioHover").removeClass("barra1btnInicio")
      if (!drMenu.isOpen()) 
         drMenu.show()
      drMenu.cancelClose()
   },function(){
      if (!drMenu.isOpen())  
         $(this).removeClass("barra1btnInicioHover").addClass("barra1btnInicio")
      drMenu.waitAndClose()
      window.timeoutRollover_data={objBtn:this}
      window.timeoutRollover=setTimeout("quitaRolloverSiMenuOculto()",250);
   })

   $("#barra1btnFilesystem").hover(function(){
      if (drMenu.isOpen()){
         drMenu.hide()
         window.timeoutRollover=null;
         quitaRolloverSiMenuOculto()
      }
      $(this).addClass("barra1btnInicioHover").removeClass("barra1btnInicio")
   },function(){
      $(this).removeClass("barra1btnInicioHover").addClass("barra1btnInicio")
   })
   $("#barra1btnFilesystem").mousedown(function (){ drWindow.openApplication({application:"drExplorer"})});
   $("#barra1BtnClose, #barra2BtnDesktop").hover(function(){$(this).addClass("hover")},function(){$(this).removeClass("hover")})
   $("#barra1BtnClose").mousedown(function(){ drWindow.openApplication({application:"cerrarSesion"}) })
   $("#barra2BtnDesktop").mousedown(GNOME_hideShowAllWindows)
   $("#barra2BtnVD1, #barra2BtnVD2, #barra2BtnVD3").mousedown(function(){
      $(".btnVD").removeClass("VDSelected");
      $(this).addClass("VDSelected");
   })
   $("#barra2BtnVD1").mousedown(function(){drWindow.changeVirtualDesktop({"virtualDesktop":0})})
   $("#barra2BtnVD2").mousedown(function(){drWindow.changeVirtualDesktop({"virtualDesktop":1})})
   $("#barra2BtnVD3").mousedown(function(){drWindow.changeVirtualDesktop({"virtualDesktop":2})})
   GNOMEActualizaReloj()
   window.setInterval(GNOMEActualizaReloj, 60000);

   var ww=drWindow.getDesktopWindow()
   if (ww.drEvents){
      ww.drEvents.bind({type:"ID_changeVD", functionName:"onExternalVDChange", parentObject:window})
   }

}

function onExternalVDChange(vd){
   vd=parseInt(vd)
   if (vd>=0 && vd<=2){
      $(".btnVD").removeClass("VDSelected");
      $("#barra2BtnVD"+(vd+1)).addClass("VDSelected");
   }
}

function quitaRolloverSiMenuOculto(){
   if (!drMenu.isOpen()) {
      $(window.timeoutRollover_data.objBtn).removeClass("barra1btnInicioHover").addClass("barra1btnInicio")
      window.timeoutRollover=null
   } else {
      window.timeoutRollover=setTimeout("quitaRolloverSiMenuOculto()",250);
   }
}

function GNOME_hideShowAllWindows() {
   var hayNoMinim=false
   var ventanasAMinim=drWindow.getWindowIds({noWidgets:"true",onlyCurrentDesktop:true})
   for (var r=0; r<ventanasAMinim.length && !hayNoMinim; r++)
      if (drWindow.getStatus({id:ventanasAMinim[r]})!="minimized")  
         hayNoMinim=true
   if (hayNoMinim)
      drWindow.setStatusAllWindows({onlyCurrentDesktop:true, status:"minimized"}) 
   else
      drWindow.setStatusAllWindows({onlyCurrentDesktop:true}) 
}

function GNOMEActualizaReloj()	{
   var mesesTxt=["ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic"]
   var diasSemana=["dom","lun","mar","mi�","jue","vie","s�b"]
   var d=new Date();
   var diaSemana= diasSemana[d.getDay()]
   var dia=GNOMEDameCadenaDosCaracteres(d.getDate())
   var mes=mesesTxt[d.getMonth()]
   var horas=GNOMEDameCadenaDosCaracteres(d.getHours())
   var minutos=GNOMEDameCadenaDosCaracteres(d.getMinutes())
   $("#dateLabel").html(diaSemana+" "+dia+" de "+mes+", "+horas+":"+minutos)
}

function GNOMEDameCadenaDosCaracteres(numero)	{
   numero=numero+""
   if (numero.length < 2) 
      numero="0"+numero
   return numero
}

function skin_GNOME_setTitle(wParams)   {
   var jqCont=drWindow.getDesktopWindow().$("#"+wParams.id+"_wTitle")

   var html=[];
   html[html.length]=               "<div class=\"chromeTitleText\">"
   html[html.length]=                  wParams.title
   html[html.length]=                  "<div class=\"chromeTitleBright\">"+wParams.title+"</div>"
   html[html.length]=               "</div>"

   jqCont.html(html.join(''))
}