/* ================================ */
/* SKIN && DESKTOP Windows Standard */
/* ================================ */
function skin_WIN(wName, wParams) {
  if (wParams.widget)
     return skin_WIN_WIDGET(wName, wParams)
  else
     return skin_WIN_DEFAULT(wName, wParams)
}

function skin_WIN_DEFAULT (wName, wParams) {
  var frame_bgcolor = "#0069FF";
  var frame_titleclass = "xDT_wTitle";
  var frame_borderwidth = 2;
  var frame_topheight = 27;
  var frame_bottomheight = 10;
  var frame_contentbgcolor = '#9CB6FF';
  //var frame_dummypic = xDT.resPath() + 'images/blank.gif';
  var frame_dummypic = 'img/t.gif';
  //var iconpath = xDT.resPath() + 'skins/WIN';
  var iconpath =  oDesktopProperties.urlSkins+'/WIN/images';
  var frame_stylecolor = '#ffffff';
  var frame_border = 2;
  var frame_bordertype = "outset"; // solid, outset, inset
  var frame_style = 	'border-top: ' + frame_border + 'px ' + frame_stylecolor + ' ' + frame_bordertype + '; ' +
  			'border-bottom: ' + frame_border + 'px ' + frame_stylecolor + ' ' + frame_bordertype + '; ' +
  			'border-left: ' + frame_border + 'px ' + frame_stylecolor + ' ' + frame_bordertype + '; ' +
  			'border-right: ' + frame_border + 'px ' + frame_stylecolor + ' ' + frame_bordertype + '; ' +
  			'cursor:hand;'
  var iconoAyuda=''
  
//  if (xDT.prop(wName,"drUrlInfo")!=null)	{
		iconoAyuda='<td class=""><a class="" href="javascript: void(0)" onmouseover="' + "SwiImg('winleft_" + wName + "','" + iconpath + "/winleft_over.gif'); " + 'xDT.prop(' + "'" + wName + "','wIcon','I1'" +')" ' +  'onmouseout="' + "SwiImg('winleft_" + wName + "','" + iconpath + "/winleft.gif');" + 'xDT.prop(' + "'" + wName + "','wIcon','I0'" + ')"'+ '><img name="winleft_' + wName + '" border="0" src="' + iconpath + '/winleft.gif"></a></td>'  
//		}
  
  return (
  			'<table cellpadding="0" cellspacing="0" border="0" height="100%" width="100%"><tr>' +
  			'<td align="left" valign="top" height="100%" width="100%" style="' + frame_style + '">' +
  			'<table cellpadding="0" cellspacing="0" border="0" height="100% width="100%" bgcolor="' + frame_bgcolor + '">' +
  			'<tr><td><img src="' + frame_dummypic + '" width="' + frame_borderwidth + '" height="' + frame_topheight + '" border="0"></td>' + 
  			     '<td width="100%" align="left" valign="middle" class="' + frame_titleclass + '">' + 
  			     '<table cellpadding="0" cellspacing="0" border="0" background="' + iconpath + '/wintitlebgr.gif"><tr>' +
  			       '<td><img src="' + frame_dummypic + '" width="1" border="0"></td>' + 
  			       iconoAyuda +
  			       '<td width="100%" align="left" valign="middle" class="' + frame_titleclass + '">&nbsp;<span  id="'+wName+'_wTitle">' + xDT.prop(wName,"wTitle") + '</span></td>' +
  			       '<td class="' + frame_titleclass + '"><a class="" href="javascript: void(0)" onmouseover="' + "SwiImg('winmin_" + wName + "','" + iconpath + "/winmin_over.gif'); " + 'xDT.prop(' + "'" + wName + "','wIcon','M1'" +')" ' +  'onmouseout="' + "SwiImg('winmin_" + wName + "','" + iconpath + "/winmin.gif'); " + 'xDT.prop(' + "'" + wName + "','wIcon','M0'" + ')"'+ '><img name="winmin_' + wName + '" border="0" src="' + iconpath + '/winmin.gif"></a></td>' +
  			       '<td><img src="' + frame_dummypic + '" width="2" border="0"></td>' + 
  			       '<td class="' + frame_titleclass + '"><a class="" href="javascript: void(0)" onmouseover="' + "SwiImg('winmax_" + wName + "','" + iconpath + "/winmax_over.gif'); " + 'xDT.prop(' + "'" + wName + "','wIcon','X1'" +')" ' +  'onmouseout="' + "SwiImg('winmax_" + wName + "','" + iconpath + "/winmax.gif'); " + 'xDT.prop(' + "'" + wName + "','wIcon','X0'" + ')"'+ '><img name="winmax_' + wName + '" border="0" src="' + iconpath + '/winmax.gif"></a></td>' +
  			       '<td><img src="' + frame_dummypic + '" width="2" border="0"></td>' + 
  			       '<td class="' + frame_titleclass + '"><a class="" href="javascript: void(0)" onmouseover="' + "SwiImg('winclose_" + wName + "','" + iconpath + "/winclose_over.gif'); " + 'xDT.prop(' + "'" + wName + "','wIcon','C1'" +')" ' +  'onmouseout="' + "SwiImg('winclose_" + wName + "','" + iconpath + "/winclose.gif'); " + 'xDT.prop(' + "'" + wName + "','wIcon','C0'" + ')"'+ '><img name="winclose_' + wName + '" border="0" src="' + iconpath + '/winclose.gif"></a></td>' +
  			     '</tr></table>' +
  			     '</td>' + 
  			     '<td><img src="' + frame_dummypic + '" width="' + frame_borderwidth + '" height="' + frame_topheight + ' border="0"></td></tr>' +
  			'<tr><td><img src="' + frame_dummypic + '" width="' + frame_borderwidth + '" border="0"></td><td align="left" valign="top" width="100%" height="100%" style="background: ' + frame_contentbgcolor +'; " id="' + wName + 'iTD' + '"></td><td><img src="' + frame_dummypic + '" width="' + frame_borderwidth + '" border="0"></td></tr>' +
  			'<tr><td><img src="' + frame_dummypic + '" width="' + frame_borderwidth + '" height="' + frame_bottomheight + '" border="0"></td><td width="100%"></td><td><img src="' + frame_dummypic + '" width="' + frame_borderwidth + '" height="' + frame_bottomheight + '" border="0"></td></tr></table>' +
  			'</td></tr></table>'
         );
}

function skin_WIN_WIDGET(wName, wParams) {
   html=[]
   html[html.length]="   <div id=\"" + wName + "iTD\" style=\"position:absolute; width:100%; height:100%; left: 0; top: 0; overflow:hidden\" >";
   html[html.length]="      <iframe width=\"100%\" height=\"100%\" src=\"widgetExample.html\" allowtransparency=\"true\" marginwidth=\"0\" marginheight=\"0\" frameborder=\"0\" scrolling=\"NO\"></iframe>";
   html[html.length]="   </div>";
   return html.join('');
}


function desktop_WIN() {
   var iconpath = oDesktopProperties.urlSkins+'/WIN';
   var backgroundImage="Indra_01_neg_RGB_peq.jpg";
   var expandWallpaper=false
   var propiedadesSkin=drWindow.getSkinParameters({skin:'WIN'});
   if (propiedadesSkin && propiedadesSkin["userBackgroundImage"])
      backgroundImage=drWindow.getDesktopProperties()["urlUserRoot"] +'/'+propiedadesSkin["userBackgroundImage"];
   else if (propiedadesSkin && propiedadesSkin["backgroundImage"])
      backgroundImage=iconpath+'/'+propiedadesSkin["backgroundImage"];
   if (propiedadesSkin && propiedadesSkin["expandWallpaper"] && propiedadesSkin["expandWallpaper"]=="true")
     expandWallpaper=true;

   xDT.addSkin('WIN',0,31);
   xDT.taskbarColor("#467AA9","white",'#A8C0D7');
   xDT.cbe("dDesktop").resizeTo(document.cbe.width(),document.cbe.height());

   if (expandWallpaper) {
      xDT.cbe("dDesktop").innerHtml('<img id="fondo" style="position:absolute; width:100%; height:100%; z-index:1; left: 0; top: 0;" src="'+backgroundImage+'"></div>');
   }else {
      xDT.cbe("dDesktop").innerHtml('<div id="fondo" style="position:absolute; width:100%; height:100%; z-index:1; left: 0; top: 0; background-color: #000000;background-image: url('+backgroundImage+');background-repeat: no-repeat;	background-position: center center;"></div>');
   }

   //xDT.cbe("dDesktop").background('#ffffff','');	
   xDT.cbe("dDesktop").zIndex(0);
   xDT.show("dDesktop");

   /*
   xDT.addSkin('WIN',0,31);
   xDT.taskbarColor("#467AA9","white",'#A8C0D7');
   xDT.cbe("dDesktop").resizeTo(document.cbe.width(),document.cbe.height());
   xDT.cbe("dDesktop").innerHtml('<div id="fondo" style="position:absolute; width:100%; height:100%; z-index:1; left: 0; top: 0; background-color: #000000;background-image: url('+iconpath+'/Indra_01_neg_RGB_peq.jpg);background-repeat: no-repeat;	background-position: center center;"></div>');
   //xDT.cbe("dDesktop").background('#ffffff','');	
   xDT.cbe("dDesktop").zIndex(0);
   xDT.show("dDesktop");*/
}


function barra_WIN()	{
	var iconpath = oDesktopProperties.urlSkins+'/WIN/images';
	var altoMenu=30;
   var startButtonLabel="Druida";
   var propiedadesSkin=drWindow.getSkinParameters({skin:'WIN'});
   if (propiedadesSkin && propiedadesSkin["startButtonLabel"])
      startButtonLabel=propiedadesSkin["startButtonLabel"];

   html=[];

	html[html.length]="<div style=\"height:"+altoMenu+"px; filter:alpha(opacity=88); -moz-opacity:0.88; opacity: 0.88; background-image: url('" + iconpath + "/menu/barre_menu.jpg');\" align=\"left\" >";
	html[html.length]="  <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse\" bordercolor=\"#111111\" width=\"100%\" align=\"left\">";
	html[html.length]="    <tr>";
	html[html.length]="      <td>";
	
	html[html.length]="			<table onclick=\"muestraOcultaMenu();\" border=\"0\"  cellpadding=\"0\" cellspacing=\"0\" style=\"cursor:hand;border-collapse: collapse;background-image: url('" + iconpath + "/menu/bt_demarrer.jpg'); font-family: Arial; font-size: 13pt; color: #EEEEEE; font-style: italic; font-weight: bold;\" bordercolor=\"#111111\" width=\"147\" align=\"left\" height=\"30\">";//class=\"barre_btdem\"//background-image: url('../images/menu/bt_demarrer.jpg'); font-family: Arial; font-size: 13pt; color: #EEEEEE; font-style: italic; font-weight: bold; cursor:default; 
	html[html.length]="				<tr>";
	html[html.length]="				  <td width=\"147\" align=\"center\" valign=\"middle\">"+startButtonLabel+"</td>";
	html[html.length]="				</tr>";
	html[html.length]="			</table>";
	
	html[html.length]="		</td>	";
	html[html.length]="      <td width=\"20\" align=\"center\" valign=\"middle\"><img src=\"" + iconpath + "/menu/carre_separateur.gif\"></td>";
	html[html.length]="      <td width=\"100%\"></td>";
	html[html.length]="      <td width=\"20\" align=\"center\" valign=\"middle\"><img src=\"" + iconpath + "/menu/carre_separateur.gif\"></td>";
	//html[html.length]="      <td id=\"tdStatus\" width=\"20\" align=\"center\" valign=\"middle\" align=\"right\" >";//class=\"textehorloge\"
	//html[html.length]="      </td> ";
	html[html.length]="		<td><img id=\"SEP_BARRA_CUSTOM\" style=\"visibility:hidden;width:1px;height:1px;\"></td>";
	html[html.length]="    </tr>";
	html[html.length]="  </table>";
	html[html.length]="</div>";
	return html.join("")
	}

if ($.browser.msie)  {
   oDatosBotonera_WIN={
      bh:23 	// height of default task button
      ,marginBottomBotonera:2
      ,marginLeftBotonera:161
      ,marginRightBotonera:34
      ,scrollbarFaceColor:"#467AA9"
      ,scrollbarShadowColor:"#396C9A"		
      ,scrollbarHighlightColor:"#5F8BB4"			
      ,scrollbar3dLightColor:"#A8C0D7"				
      ,scrollbarDarkShadowColor:"#3C5E82"			
      ,scrollbarTrackColor:"#DCD8DC"			
      ,scrollbarArrowColor:"white"	
      ,generaHtml:function (wins)	{
         var iconpath =  oDesktopProperties.urlSkins+'/WIN/images';
         html=[];
         for (var i=0;i<wins.length;i++) {
            winName = wins[i];
            winTitle = xDT.prop(winName,"wTitle");
            html[html.length]="<div id=\"tb" + i + "\" onclick=\"xDT.taskbar('"+winName+"')\" style=\"cursor:hand; float:left; position:relative; width:160px; height:"+this.bh+"px; margin-left:6px; font-family:Tahoma,Verdana,Arial;font-size:11px;color:white; background-image:url("+ iconpath +"/menu/btnBarraN.gif)\">";
            html[html.length]="  <div title=\""+winTitle+"\" style=\"position:absolute; left:25px; top:5px; width:128px; text-overflow:ellipsis;white-space:nowrap;overflow:hidden;\">"+winTitle+"</div>"; //TO-DO: escapar esto bien
            html[html.length]="</div>";
            }
         return html.join("")
         }
      }
   }
else  { //Firefox: alto del boton no puede ser menor de 32 o si no no aparece el scroll ... 
   oDatosBotonera_WIN={
      bh:32 	//changed
      ,marginBottomBotonera:0 //changed
      ,marginLeftBotonera:161 
      ,marginRightBotonera:34
      ,generaHtml:function (wins)	{
         var iconpath =  oDesktopProperties.urlSkins+'/WIN/images';
         html=[];
         for (var i=0;i<wins.length;i++) {
            winName = wins[i];
            winTitle = xDT.prop(winName,"wTitle");
            html[html.length]="<div id=\"tb" + i + "\" onclick=\"xDT.taskbar('"+winName+"')\" style=\"cursor:hand; float:left; position:relative; width:160px; height:"+this.bh+"px; margin-left:6px; font-family:Tahoma,Verdana,Arial;font-size:11px;color:white; background-image:url("+ iconpath +"/menu/btnBarraN_FF.gif)\">"; //changed image
            html[html.length]="  <div title=\""+winTitle+"\" style=\"position:absolute; left:25px; top:11px; width:124px; text-overflow:ellipsis;white-space:nowrap;overflow:hidden;\">"+winTitle+"</div>"; //TO-DO: escapar esto bien
            html[html.length]="</div>";
            }
         return html.join("")
         }
      }
   }