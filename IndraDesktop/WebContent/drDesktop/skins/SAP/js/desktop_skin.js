oSkinFactory.createSkin({
   id:"SAP"
   ,marginTop:67
   ,marginBottom:25
   ,backgroundColor:"#9cbdce" //par�metro del skin
   ,startButtonLabel:"Menu" //par�metro del skin
   // ------------------
   ,taskbar:{
      marginBottom:4 //separacion de los botones (de la barra de botones) con el margen inferior de la pantalla.
      ,marginRight:26  //Margen adicional a la derecha del grupo de botones (adicional al espacio del widget si lo hay)
      ,buttonHeight:17  //Altura de un boton
      ,getHtmlButtonsFunction:BASIC_getHtmlTaskbarButtons
   }
   ,startMenu:{
      hOffset:-5 //Desplazamiento horizontal de un men� sobre el anterior
      ,vOffset:0  //Desplazamiento generico vertical de un men� sobre el anterior
      ,vOffset_lv_0:1  //Desplazamiento vertical del nivel 0
      ,vOffset_lv_1:4   //Desplazamiento del nivel 1 con respecto del 0 (en este caso es la cabecera dentro del men� de inicio)
      ,getHtmlFunction:function (opciones, nivel)   { //Lo llamo para cada nivel (0 primer nivel)
         if (nivel==0)  { //Primer nivel
            var menuHtmlOpciones=oSkinFactory.getHtmlStartMenuOptions({options:opciones, level:nivel})
            //return oSkinFactory.getHtmlStartMenu3x1x3Chrome({htmlContent:menuHtmlOpciones}) + SAP_SKIN_getCustomHtmlstartMenu() //Ejemplo simple de personalizaci�n del men� de inicio
            return oSkinFactory.getHtmlStartMenu3x1x3Chrome({htmlContent:menuHtmlOpciones}) //construyo el marco alrededor de las opciones
         } else   { //Resto de niveles
            var menuHtmlOpciones=oSkinFactory.getHtmlStartSubmenuOptions({options:opciones, level:nivel})
            return oSkinFactory.getHtmStartSubmenu2BordersChrome({htmlContent:menuHtmlOpciones}) //construyo el marco alrededor de las opciones
         }
      }
   }
   // ------------------
   ,initSkinFunction:function () { 
      oSkinFactory.initSkin(oSkin_SAP); //Inicializa el skin 
      oSkinFactory.buildDefaultBackground({containerId:"dDesktop",oSkin:oSkin_SAP}); //construye el background
   }

   // ------------------
   ,getHtmlTaskbarFunction:function ()	{
      return oSkinFactory.getHtmlDefaultTaskbar({oSkin:oSkin_SAP})
   }

   // ------------------
   ,getHtmlWindowFunction: function (wName, wParams) {
      if (wParams.widget)
         return oSkinFactory.getHtmlDefaultWidget(wName,wParams);
      return SAP_getHtmlDefault3xNWithTitleAndButtonsWithFocusChrome({wName:wName, wParams:wParams, oSkin:oSkin_SAP})
   }
   // ------------------- implementamos marcar boton ventana focalizada
   ,onWindowChangeFocusStatus: SAP_SKIN_onWindowChangeFocusStatus
})



function SAP_getHtmlDefault3xNWithTitleAndButtonsWithFocusChrome(args)  {
   var wName=args.wName
   var titulo=drDesktop.wData({id:wName, property:"wTitle"})
   var propiedadesSkin=drWindow.getSkinParameters({skin:"SAP"});
   var roundedCorners= drEvaluaBooleanoString(propiedadesSkin.roundedCorners)
   html=[]
   html[html.length]="<table class=\"chromeContainer\" width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
   html[html.length]="  <tr>";
   html[html.length]="    <td  height=\"1\">";
   html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" >";
   html[html.length]="        <tr>";
   html[html.length]="          <td><div class=\""+ (roundedCorners?"roundedChrome11":"chrome11") +"\"></div></td>";
   html[html.length]="          <td class=\"chrome12\" width=\"100%\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed\"><tr><td class=\"chromeTitle\" id=\""+wName+"_wTitle\">"+titulo+"</td></tr></table></td>";
   html[html.length]="          <td><div class=\"chromeBtnMin\" onmouseover=\"$(this).addClass('chromeBtnMinHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'M1'})\" onmouseout=\"$(this).removeClass('chromeBtnMinHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'M0'})\"></div></td>";
   html[html.length]="          <td><div class=\"chromeBtnMax\" onmouseover=\"$(this).addClass('chromeBtnMaxHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'X1'})\" onmouseout=\"$(this).removeClass('chromeBtnMaxHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'X0'})\"></div></td>";
   html[html.length]="          <td><div class=\"chromeBtnClose\" onmouseover=\"$(this).addClass('chromeBtnCloseHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'C1'})\" onmouseout=\"$(this).removeClass('chromeBtnCloseHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'C0'})\"></div></td>";
   html[html.length]="          <td><div class=\""+ (roundedCorners?"roundedChrome13":"chrome13") +"\"></div></td>";
   html[html.length]="        </tr>";
   html[html.length]="      </table>";
   html[html.length]="    </td>";
   html[html.length]="  </tr>";
   html[html.length]="  <tr>";
   html[html.length]="    <td>";
   html[html.length]="      <table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
   html[html.length]="        <tr>";
   html[html.length]="          <td><div class=\"chrome21\"></div></td>";
   html[html.length]="          <td class=\"chrome22\" width=\"100%\"><div id=\""+wName+"iTD\" style='width:100%; height:100%'></div></td>"; //un div para que el background sea opaco al arrastrar otras
   html[html.length]="          <td><div class=\"chrome23\"></div></td>";
   html[html.length]="        </tr>";
   html[html.length]="      </table>";
   html[html.length]="    </td>";
   html[html.length]="  </tr>";
   html[html.length]="  <tr>";
   html[html.length]="    <td height=\"1\">";
   html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
   html[html.length]="        <tr>";
   html[html.length]="          <td><div class=\""+ (roundedCorners?"roundedChrome31":"chrome31") +"\"></div></td>";
   html[html.length]="          <td class=\"chrome32\" width=\"100%\"></td>";
   html[html.length]="          <td><div class=\""+ (roundedCorners?"roundedChrome33":"chrome33") +"\"></div></td>";
   html[html.length]="        </tr>";
   html[html.length]="      </table>";
   html[html.length]="    </td>";
   html[html.length]="  </tr>";
   html[html.length]="  </table>";
   return html.join('')
}


function SAP_SKIN_onWindowChangeFocusStatus(args)   {
   if (args.focus)   {
      $("#"+args.id+"_wTitle").addClass("chromeTitleFocused").removeClass("chromeTitle")
      $("#taskbarButton_"+args.id).addClass("taskbarButtonFocused").removeClass("taskbarButton")
   } else {
      $("#"+args.id+"_wTitle").addClass("chromeTitle").removeClass("chromeTitleFocused")
      $("#taskbarButton_"+args.id).addClass("taskbarButton").removeClass("taskbarButtonFocused")
   }
}



function BASIC_getHtmlTaskbarButtons (wins){
      html=[];
      for (var i=0;i<wins.length;i++) {
         winName = wins[i];
         winTitle = drDesktop.wData({id:winName, property:"wTitle"})
         if (this.taskbarButtonImagePosition!=null)   { //FIX para caso Firefox + buttonHeight<32
            html[html.length]="<div id=\"taskbarButton_" + winName + "\" onclick=\"drDesktop.taskbar_btnClick({id:'"+winName+"'})\" class=\"taskbarButton\" style=\"margin-top:11px; margin-bottom:4px; float:left;position:relative;\">";	
            html[html.length]="  <div title=\""+winTitle+"\" class=\"taskbarButtonLabel\">"+winTitle+"</div>"; //TO-DO: escapar esto bien
            html[html.length]="</div>";
         } else   {
            html[html.length]="<div id=\"taskbarButton_" + winName + "\" onclick=\"drDesktop.taskbar_btnClick({id:'"+winName+"'})\" class=\"taskbarButton\" style=\"height:"+this.bh+"px;float:left;position:relative;\">";
            html[html.length]="  <div title=\""+winTitle+"\" class=\"taskbarButtonLabel\">"+winTitle+"</div>"; //TO-DO: escapar esto bien
            html[html.length]="</div>";
         }
      }
      return html.join("")
   }