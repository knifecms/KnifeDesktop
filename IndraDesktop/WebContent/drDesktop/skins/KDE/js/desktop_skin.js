oSkinFactory.createSkin({
   id:"KDE"
   ,marginTop:0
   ,marginBottom:38
   ,backgroundImage:"css/background/fondoAzul.jpg" //par�metro del skin
   ,backgroundColor:"#081e3e" //par�metro del skin
   ,expandWallpaper:false //par�metro del skin
   ,startButtonLabel:"" //par�metro del skin
   // ------------------
   ,taskbar:{
      marginBottom:0 //separacion de los botones (de la barra de botones) con el margen inferior de la pantalla.
      ,marginRight:4 //Margen adicional a la derecha del grupo de botones (adicional al espacio del widget si lo hay)
      ,buttonHeight:36  //Altura de un boton
      ,getHtmlButtonsFunction:oSkinFactory.getHtmlDefaultTaskbarButtonsWithIcons   
   }
   ,startMenu:{
      hOffset:-5.0 //Desplazamiento horizontal de un men� sobre el anterior
      ,vOffset:0.0  //Desplazamiento generico vertical de un men� sobre el anterior
      ,vOffset_lv_0:0  //Desplazamiento vertical del nivel 0
      ,vOffset_lv_1:63   //Desplazamiento del nivel 1 con respecto del 0 (en este caso es la cabecera dentro del men� de inicio)
      ,getHtmlFunction:function (opciones, nivel)   { //Lo llamo para cada nivel (0 primer nivel)
         if (nivel==0)  { //Primer nivel
            var menuHtmlOpciones=oSkinFactory.getHtmlStartMenuOptions({options:opciones, level:nivel})
            return oSkinFactory.getHtmlStartMenu3x1x3Chrome({htmlContent:menuHtmlOpciones}) + KDE_SKIN_getCustomHtmlstartMenu() //Ejemplo simple de personalizaci�n del men� de inicio
            //return oSkinFactory.getHtmlStartMenu3x1x3Chrome({htmlContent:menuHtmlOpciones}) //construyo el marco alrededor de las opciones
         } else   { //Resto de niveles
            var menuHtmlOpciones=oSkinFactory.getHtmlStartSubmenuOptions({options:opciones, level:nivel})
            return oSkinFactory.getHtmStartSubmenu2BordersChrome({htmlContent:menuHtmlOpciones}) //construyo el marco alrededor de las opciones
         }
      }
   }
   // ------------------
   ,initSkinFunction:function () { 
      oSkinFactory.initSkin(oSkin_KDE); //Inicializa el skin 
      oSkinFactory.buildDefaultBackground({containerId:"dDesktop",oSkin:oSkin_KDE}); //construye el background
   }

   // ------------------
   ,getHtmlTaskbarFunction:function ()	{
      return KDE_SKIN_getHtmlTaskbar({oSkin:oSkin_KDE}) //oSkinFactory.getHtmlDefaultTaskbar({oSkin:oSkin_KDE})
   }

   , getHtmlIcons:KDE_SKIN_getHtmlIcons
   // ------------------
   ,getHtmlWindowFunction: function (wName, wParams) {
      if (wParams.widget)
         return oSkinFactory.getHtmlDefaultWidget(wName,wParams);
      //return oSkinFactory.getHtmlDefault3xNWithIconTitleAndButtonsChrome({wName:wName, wParams:wParams, oSkin:oSkin_KDE})
      return KDE_SKIN_getHtmlWindow({wName:wName, wParams:wParams, oSkin:oSkin_KDE})
   }
   ,minimunWindowWidth:150
   ,minimunWindowHeight:70
   // ------------------- implementamos marcar boton ventana focalizada
   ,onWindowChangeFocusStatus: KDE_SKIN_onWindowChangeFocusStatus
   //,getHtmlDialogFunction:function (btn){return".."}  esto personalizar�a los ModalDialog, los CSS del estilo son cargados autom�ticamente.
   ,onReady:KDE_bgScripts
})



function KDE_SKIN_onWindowChangeFocusStatus(args)   {
   if (args.focus)   {
      $("#"+args.id+"_blurDiv").addClass("chromeBlurFocused")
      $("#"+args.id+"_wTitle").addClass("chromeTitleFocused").removeClass("chromeTitle")
      $("#taskbarButton_"+args.id).addClass("taskbarButtonFocused").removeClass("taskbarButton")
   } else {
      $("#"+args.id+"_blurDiv").removeClass("chromeBlurFocused")
      $("#"+args.id+"_wTitle").addClass("chromeTitle").removeClass("chromeTitleFocused")
      $("#taskbarButton_"+args.id).addClass("taskbarButton").removeClass("taskbarButtonFocused")
   }
}



function KDE_SKIN_getHtmlWindow(args){ // getHtmlDefault3xNWithIconTitleAndButtonsChrome con rollover y "blurDiv"
   var wName=args.wName
   var titulo=drDesktop.wData({id:wName, property:"wTitle"})
   var icono=oSkinFactory.getIconPath(wName)
   var wParams=args.wParams
   html=[]
   html[html.length]="<table class=\"chromeContainer\" width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
   html[html.length]="  <tr>";
   html[html.length]="    <td  height=\"1\">";
   html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" >";
   html[html.length]="        <tr>";
   if (icono!=null)
      html[html.length]="          <td><div class=\"chrome11\"><img class=\"chromeIcon\" src=\""+icono+"\" width=\"16\" height=\"16\"></div></td>";
   else
      html[html.length]="          <td><div class=\"chrome11\"></div></td>";
   html[html.length]="          <td class=\"chrome12\" width=\"100%\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed\"><tr><td class=\"chromeTitle\" id=\""+wName+"_wTitle\">"+titulo+"</td></tr></table></td>";
   if (drWindow.hasHelp({params:wParams}))
      html[html.length]="          <td><div class=\"chromeBtnHelp\" onmouseover=\"$(this).addClass('chromeBtnHelpHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'I1'})\" onmouseout=\"$(this).removeClass('chromeBtnHelpHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'I0'})\"></div></td>";
   html[html.length]="          <td><div class=\"chromeBtnMin\" onmouseover=\"$(this).addClass('chromeBtnMinHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'M1'})\" onmouseout=\"$(this).removeClass('chromeBtnMinHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'M0'})\"></div></td>";
   html[html.length]="          <td><div class=\"chromeBtnMax\" onmouseover=\"$(this).addClass('chromeBtnMaxHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'X1'})\" onmouseout=\"$(this).removeClass('chromeBtnMaxHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'X0'})\"></div></td>";
   html[html.length]="          <td><div class=\"chromeBtnClose\" onmouseover=\"$(this).addClass('chromeBtnCloseHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'C1'})\" onmouseout=\"$(this).removeClass('chromeBtnCloseHover');drDesktop.wData({id:'"+wName+"',property:'wIcon',value:'C0'})\"></div></td>";
   html[html.length]="          <td><div class=\"chrome13\"></div></td>";
   html[html.length]="        </tr>";
   html[html.length]="      </table>";
   html[html.length]="    </td>";
   html[html.length]="  </tr>";
   html[html.length]="  <tr>";
   html[html.length]="    <td>";
   html[html.length]="      <table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
   html[html.length]="        <tr>";
   html[html.length]="          <td><div class=\"chrome21\"></div></td>";
   html[html.length]="          <td class=\"chrome22\" width=\"100%\">"
   html[html.length]="             <div id=\""+wName+"iTD\" style='width:100%; height:100%'></div>"
   //html[html.length]="             <div id=\""+wName+"_blurDiv\" class=\"chromeBlur\"><TABLE height=\"100%\" width=\"100%\"><TR><TD></TD></TR></TABLE></div>"
   html[html.length]="          </td>";
   html[html.length]="          <td><div class=\"chrome23\"></div></td>";
   html[html.length]="        </tr>";
   html[html.length]="      </table>";
   html[html.length]="    </td>";
   html[html.length]="  </tr>";
   html[html.length]="  <tr>";
   html[html.length]="    <td height=\"1\">";
   html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
   html[html.length]="        <tr>";
   html[html.length]="          <td><div class=\"chrome31\"></div></td>";
   html[html.length]="          <td class=\"chrome32\" width=\"100%\"></td>";
   html[html.length]="          <td><div class=\"chrome33\"></div></td>";
   html[html.length]="        </tr>";
   html[html.length]="      </table>";
   html[html.length]="    </td>";
   html[html.length]="  </tr>";
   html[html.length]="  </table>";
   return html.join('')
}

function  KDE_SKIN_getHtmlTaskbar (args) {
   var oSkin=args.oSkin;
   html=[];
   html[html.length]="<div class=\"taskbarContainer\" >";
   html[html.length]="  <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:collapse\" width=\"100%\" >";
   html[html.length]="    <tr>";
   html[html.length]="      <td>";
   html[html.length]="			<table id=\"taskbarBtnStart\" border=\"0\"  cellpadding=\"0\" cellspacing=\"0\">";
   html[html.length]="				<tr>";
   html[html.length]="				  <td><div class=\"taskbarBtnStart1\" onclick=\"muestraOcultaMenu();\"></div></td>";
   html[html.length]="				  <td>"
   html[html.length]="				     <div class=\"taskbarBtnStart2\">"
   html[html.length]="				        <div id=\"btnCVD1\" class=\"btnCVD btnCVDSelected\" onclick=\"KDE_SKIN_btnCVD1_onclick(0,this)\"></div>"            
   html[html.length]="				        <div id=\"btnCVD2\" class=\"btnCVD\" onclick=\"KDE_SKIN_btnCVD1_onclick(1,this)\"></div>"            
   html[html.length]="				        <div id=\"btnCVD3\" class=\"btnCVD\" onclick=\"KDE_SKIN_btnCVD1_onclick(2,this)\"></div>"            
   html[html.length]="				        <div id=\"btnCVD4\" class=\"btnCVD\" onclick=\"KDE_SKIN_btnCVD1_onclick(3,this)\"></div>"            
   html[html.length]="				     </div>"
   html[html.length]="				  </td>";
   html[html.length]="				</tr>";
   html[html.length]="			</table>";
   html[html.length]="		</td>";
   html[html.length]="      <td class=\"taskbarButtonsBg\" width=\"100%\"></td>";
   html[html.length]="      <td><div class=\"taskbarCustomBlock1\"></div></td>";
   html[html.length]="      <td class=\"taskbarCustomBlock2\"><img id=\"SEP_BARRA_CUSTOM\" style=\"visibility:hidden;width:1px;height:1px;\"></td>";
   html[html.length]="    </tr>";
   html[html.length]="  </table>";
   html[html.length]="	<img src=\""+drWindow.getSkinUrl()+"/css/menu/taskbarBackgroundLine.gif\" class=\"taskbarBackgroundLine\">"
   html[html.length]="</div>";
   return html.join("")
}


function KDE_SKIN_btnCVD1_onclick(VD, obj){
   $(".btnCVD").removeClass("btnCVDSelected")
   $(obj).addClass("btnCVDSelected")
   drWindow.changeVirtualDesktop({"virtualDesktop":VD})
}

function KDE_SKIN_getHtmlIcons(args){
   var id=args.id, x=args.x, y=args.y, width=args.width, height=args.height, imgSrc=args.imgSrc, label=args.label, tooltip=args.tooltip, js=args.js
   var html=[]
   html[html.length]="<div ID=\"drIcn_"+id+"\" class=\"icon\" style=\"left:"+x+"px;top:"+y+"px;\" ondblclick=\""+DrEscHTML(js)+"\" title=\""+DrEscHTML(tooltip)+"\">";
   html[html.length]="	<div class=\"iconBg\"></div>";
   html[html.length]="	<div class=\"iconLabel\">"+DrEscHTML(label)+"</div>";
   html[html.length]="   <img src=\""+imgSrc+"\"  class=\"iconImage\">";
   html[html.length]="</div>"
   return html.join('')
}

// ---------------

function KDE_SKIN_getCustomHtmlstartMenu() {  //A�ado el usuario y los botones de cerrar al menu de inicio
   if (window.userNameCache==null)
      window.userNameCache=drWindow.getUserData().id;
   var html=[]
   html[html.length]="<div class=\"startMenuCustomUserLabel\">"
   html[html.length]= window.userNameCache
   html[html.length]="</div>"
   html[html.length]="<div class=\"startMenuCustomButtonsContainer\" onmouseover=\"$('#OPS_MENU_0 TR').removeClass('startMenu_fatherRow_rolloverRow').removeClass('startMenu_rolloverRow')\">"
   html[html.length]="  <div class=\"startMenuCustomCloseBtn\" onmousedown=\"preguntaYCierra()\"></div>"
   html[html.length]="  <div class=\"startMenuCustomExitBtn\" onmousedown=\"preguntaYSalSinGuardar()\"></div>"
   html[html.length]="</div>"
   return html.join('');
}

// ------------------

function KDE_bgScripts(){
   preloadSkinImages([
      "css/chrome/chrome12btnHelpHover.gif"
      ,"css/chrome/chrome12btnMinHover.gif"
      ,"css/chrome/chrome12btnMaxHover.gif"
      ,"css/chrome/chrome12btnCloseHover.gif"
   ])
   var ww=drWindow.getDesktopWindow()
   if (ww.drEvents){
      ww.drEvents.bind({type:"ID_changeVD", functionName:"onExternalVDChange", parentObject:window})
   }
}

function preloadSkinImages(imgArr){
   window._preloadedImages=[]
   var baseURL=drWindow.getDesktopProperties().urlSkins + "/" + drWindow.getSkin()+"/"
   for (var r=0; r<imgArr.length; r++){
      var completeImgURL= baseURL+imgArr[r]
      _preloadedImages[r]=new Image()
      _preloadedImages[r].src=completeImgURL
   }
}

function onExternalVDChange(vd){
   vd=parseInt(vd)
   if (vd>=0 && vd<=3){
      $(".btnCVD").removeClass("btnCVDSelected")
      $("#btnCVD"+(vd+1)).addClass("btnCVDSelected")
   }
}