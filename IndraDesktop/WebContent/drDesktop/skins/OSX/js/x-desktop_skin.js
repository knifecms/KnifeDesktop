oSkinFactory.createSkin({
   id:"OSX"
   ,marginTop:23
   ,marginBottom:24
   ,backgroundImage:"css/background/wallpaper_acqua.jpg" //par�metro del skin
   ,backgroundColor:"#5d85b7" //par�metro del skin
   ,expandWallpaper:true //par�metro del skin
   ,startButtonLabel:drResources.get({bundle:"skins",key:"osxSkin.txt1"}) //par�metro del skin
   // ------------------
   ,taskbar:{
      marginBottom:0 //separacion de los botones (de la barra de botones) con el margen inferior de la pantalla.
      ,marginRight:23  //Margen adicional a la derecha del grupo de botones (adicional al espacio del widget si lo hay)
      ,buttonHeight:40  //Altura de un boton
      ,getHtmlButtonsFunction:getHtmlTaskbarButtonsAsIcons
   }
   ,startMenu:{
      hOffset:-5 //Desplazamiento horizontal de un men� sobre el anterior
      ,vOffset:0  //Desplazamiento generico vertical de un men� sobre el anterior
      ,vOffset_lv_0:0  //Desplazamiento vertical del nivel 0
      ,vOffset_lv_1:7   //Desplazamiento del nivel 1 con respecto del 0 (en este caso es la cabecera dentro del men� de inicio)
      ,getHtmlFunction:function (opciones, nivel)   { //Lo llamo para cada nivel (0 primer nivel)
         if (nivel==0)  { //Primer nivel
            var menuHtmlOpciones=oSkinFactory.getHtmlStartMenuOptions({options:opciones, level:nivel})
            //return oSkinFactory.getHtmlStartMenu3x1x3Chrome({htmlContent:menuHtmlOpciones}) + OSX_SKIN_getCustomHtmlstartMenu() //Ejemplo simple de personalizaci�n del men� de inicio
            return oSkinFactory.getHtmlStartMenu3x1x3Chrome({htmlContent:menuHtmlOpciones}) //construyo el marco alrededor de las opciones
         } else   { //Resto de niveles
            var menuHtmlOpciones=oSkinFactory.getHtmlStartSubmenuOptions({options:opciones, level:nivel})
            return oSkinFactory.getHtmStartSubmenu2BordersChrome({htmlContent:menuHtmlOpciones}) //construyo el marco alrededor de las opciones
         }
      }
   }
   // ------------------
   ,initSkinFunction:function () { 
      oSkinFactory.initSkin(oSkin_OSX); //Inicializa el skin 
      oSkinFactory.buildDefaultBackground({containerId:"dDesktop",oSkin:oSkin_OSX}); //construye el background
   }

   // ------------------
   ,getHtmlTaskbarFunction:function ()	{
      return oSkinFactory.getHtmlDefaultTaskbar({oSkin:oSkin_OSX})
   }

   // ------------------
   ,getHtmlWindowFunction: function (wName, wParams) {
      if (wParams.widget)
         return oSkinFactory.getHtmlDefaultWidget(wName,wParams);
      //return oSkinFactory.getHtmlDefault3xNWithTitleAndButtonsChrome({wName:wName, wParams:wParams, oSkin:oSkin_OSX})
      return getHtmlDefault3xNWithTitleAndButtonsFirstChrome({wName:wName, wParams:wParams, oSkin:oSkin_OSX})
   }
   // ------------------- implementamos marcar boton ventana focalizada
   ,onWindowChangeFocusStatus: OSX_SKIN_onWindowChangeFocusStatus
})




function getHtmlDefault3xNWithTitleAndButtonsFirstChrome(args)  {
      var wName=args.wName
      var titulo=xDT.prop(wName,"wTitle");
      var icono=oSkinFactory.getIconPath(wName)
      var wParams=args.wParams
      html=[]
      html[html.length]="<table class=\"chromeContainer\" width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="  <tr>";
      html[html.length]="    <td  height=\"1\">";
      html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" >";
      html[html.length]="        <tr>";
      html[html.length]="          <td><div class=\"chrome11\"></td>";
      html[html.length]="          <td><div class=\"chromeBtnClose\" onmouseover=\"xDT.prop('"+wName+"','wIcon','C1'); $(this).addClass('chromeBtnCloseHover')\" onmouseout=\"xDT.prop('"+wName+"','wIcon','C0'); $(this).removeClass('chromeBtnCloseHover')\"></div></td>";
      html[html.length]="          <td><div class=\"chromeBtnMin\" onmouseover=\"xDT.prop('"+wName+"','wIcon','M1'); $(this).addClass('chromeBtnMinHover')\" onmouseout=\"xDT.prop('"+wName+"','wIcon','M0'); $(this).removeClass('chromeBtnMinHover')\"></div></td>";
      html[html.length]="          <td><div class=\"chromeBtnMax\" onmouseover=\"xDT.prop('"+wName+"','wIcon','X1'); $(this).addClass('chromeBtnMaxHover')\" onmouseout=\"xDT.prop('"+wName+"','wIcon','X0'); $(this).removeClass('chromeBtnMaxHover')\"></div></td>";
      html[html.length]="          <td class=\"chrome12\" width=\"100%\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"table-layout:fixed\"><tr><td class=\"chromeTitle\" id=\""+wName+"_wTitle\">"+titulo+"</td></tr></table></td>";
      if (drWindow.hasHelp({params:wParams}))
         html[html.length]="          <td><div class=\"chromeBtnHelp\" onmouseover=\"xDT.prop('"+wName+"','wIcon','I1'); $(this).addClass('chromeBtnHelpHover')\" onmouseout=\"xDT.prop('"+wName+"','wIcon','I0'); $(this).removeClass('chromeBtnHelpHover')\"></div></td>";
      else
         html[html.length]="          <td><div class=\"chromeBtnHelp chromeBtnHelpHover\"></div></td>";
      html[html.length]="          <td><div class=\"chrome13\"></div></td>";
      html[html.length]="        </tr>";
      html[html.length]="      </table>";
      html[html.length]="    </td>";
      html[html.length]="  </tr>";
      html[html.length]="  <tr>";
      html[html.length]="    <td>";
      html[html.length]="      <table width=\"100%\" height=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="        <tr>";
      html[html.length]="          <td><div class=\"chrome21\"></div></td>";
      html[html.length]="          <td class=\"chrome22\" width=\"100%\"><div id=\""+wName+"iTD\" style='width:100%; height:100%'></div></td>"; //un div para que el background sea opaco al arrastrar otras
      html[html.length]="          <td><div class=\"chrome23\"></div></td>";
      html[html.length]="        </tr>";
      html[html.length]="      </table>";
      html[html.length]="    </td>";
      html[html.length]="  </tr>";
      html[html.length]="  <tr>";
      html[html.length]="    <td height=\"1\">";
      html[html.length]="      <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
      html[html.length]="        <tr>";
      html[html.length]="          <td><div class=\"chrome31\"></div></td>";
      html[html.length]="          <td class=\"chrome32\" width=\"100%\"></td>";
      html[html.length]="          <td><div class=\"chrome33\"></div></td>";
      html[html.length]="        </tr>";
      html[html.length]="      </table>";
      html[html.length]="    </td>";
      html[html.length]="  </tr>";
      html[html.length]="  </table>";
      return html.join('')
   }


function getHtmlTaskbarButtonsAsIcons (wins){
      var registroVentanas = drWindow.getRegistry()
      var indiceAplicaciones = drWindow.getApplicationsConfig()
      html=[];
      for (var i=0;i<wins.length;i++) {
         var app=null;
         var winName = wins[i];
         var winTitle = xDT.prop(winName,"wTitle");
         var contenidoBtn="" //=winTitle
         var datosApl=registroVentanas.getVentana(winName)
         if (datosApl!=null) //NULL para las recien creadas???
            var app=datosApl.application
         if (app!=null) {
            var iconPath=drWindow.getDesktopWindow().urlRaizIconos() + "/" +indiceAplicaciones[app].icon;
         } else   {
            var iconPath=drWindow.getDesktopWindow().urlRaizIconos() + "/php.gif";
         }

         contenidoBtn="<img title=\""+winTitle+"\" src=\""+iconPath+"\" width=\"32\" height=\"32\" align=\"absmiddle\" style=\"margin-right:4px\">"

         if (this.taskbarButtonImagePosition!=null)   { //FIX para caso Firefox + buttonHeight<32
            html[html.length]="<div id=\"taskbarButton_" + winName + "\" onclick=\"xDT.taskbar('"+winName+"')\" class=\"taskbarButton\" style=\"height:"+this.bh+"px;float:left;position:relative;background-position:0px "+this.taskbarButtonImagePosition+"px;background-repeat: no-repeat;\">";	
            //html[html.length]="  <div title=\""+winTitle+"\" class=\"taskbarButtonLabel\" style=\"margin-top:"+this.taskbarButtonImagePosition+"px\">"+contenidoBtn+"</div>"; //TO-DO: escapar esto bien
            html[html.length]=contenidoBtn
            html[html.length]="</div>";
         } else   {
            html[html.length]="<div id=\"taskbarButton_" + winName + "\" onclick=\"xDT.taskbar('"+winName+"')\" class=\"taskbarButton\" style=\"height:"+this.bh+"px;float:left;position:relative;\">";
            //html[html.length]="  <div title=\""+winTitle+"\" class=\"taskbarButtonLabel\">"+contenidoBtn+"</div>"; //TO-DO: escapar esto bien
            html[html.length]=contenidoBtn
            html[html.length]="</div>";
         }
      }
      return html.join("")
   }


function OSX_SKIN_onWindowChangeFocusStatus(args)   {
   if (args.focus)   {
      $("#"+args.id+"_wTitle").addClass("chromeTitleFocused").removeClass("chromeTitle")
      $("#taskbarButton_"+args.id).addClass("taskbarButtonFocused").removeClass("taskbarButton")
   } else {
      $("#"+args.id+"_wTitle").addClass("chromeTitle").removeClass("chromeTitleFocused")
      $("#taskbarButton_"+args.id).addClass("taskbarButton").removeClass("taskbarButtonFocused")
   }
}