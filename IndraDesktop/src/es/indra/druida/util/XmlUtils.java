package es.indra.druida.util;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * The Class XmlUtils.
 */
public class XmlUtils {

	/**
	 * Gets the document from file.
	 * 
	 * @param xmlFile
	 *            the xml file
	 * 
	 * @return the document from file
	 * 
	 * @throws ParserConfigurationException
	 *             the parser configuration exception
	 * @throws SAXException
	 *             the SAX exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static Document getDocumentFromFile(File xmlFile) throws ParserConfigurationException, SAXException, IOException {
		//File xmlFile = new File(xmlFileName);
		Document doc=null;
		if (xmlFile.exists()){
			//URL xmlURL=xmlFile.toURL();
			//	obtenemos el documento XML
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			dbFactory.setExpandEntityReferences(true);
			DocumentBuilder parser = dbFactory.newDocumentBuilder();
			//Document doc = (Document)parser.parse(xmlURL);
			doc = (Document)parser.parse(xmlFile);
		}
		return doc;
	}

}
