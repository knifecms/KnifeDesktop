package es.indra.druida.desktop.ant;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Logger;

import javax.servlet.jsp.JspWriter;

import org.apache.tools.ant.BuildEvent;
import org.apache.tools.ant.BuildLogger;

/**
 * The Class AntLauncherJspLogger.
 */
public class AntLauncherJspLogger implements BuildLogger {

	/** The jsp writer. */
	private JspWriter jspWriter; 
	
	/** The message output level. */
	private int messageOutputLevel;

	/**
	 * Gets the message output level.
	 * 
	 * @return the message output level
	 */
	public int getMessageOutputLevel() {
		return messageOutputLevel;
	}
	
	/* (non-Javadoc)
	 * @see org.apache.tools.ant.BuildLogger#setMessageOutputLevel(int)
	 */
	public void setMessageOutputLevel(int messageOutputLevel) {		//MSG_ERR, MSG_WARN, MSG_INFO, MSG_VERBOSE, MSG_DEBUG
		this.messageOutputLevel=messageOutputLevel;
	}	

	/* (non-Javadoc)
	 * @see org.apache.tools.ant.BuildLogger#setEmacsMode(boolean)
	 */
	public void setEmacsMode(boolean arg0) {
	}

	/* (non-Javadoc)
	 * @see org.apache.tools.ant.BuildLogger#setErrorPrintStream(java.io.PrintStream)
	 */
	public void setErrorPrintStream(PrintStream arg0) {
	}



	/* (non-Javadoc)
	 * @see org.apache.tools.ant.BuildLogger#setOutputPrintStream(java.io.PrintStream)
	 */
	public void setOutputPrintStream(PrintStream arg0) {
	}

	/* (non-Javadoc)
	 * @see org.apache.tools.ant.BuildListener#buildFinished(org.apache.tools.ant.BuildEvent)
	 */
	public void buildFinished(BuildEvent arg0) {
	}

	/* (non-Javadoc)
	 * @see org.apache.tools.ant.BuildListener#buildStarted(org.apache.tools.ant.BuildEvent)
	 */
	public void buildStarted(BuildEvent arg0) {
	}

	/* (non-Javadoc)
	 * @see org.apache.tools.ant.BuildListener#messageLogged(org.apache.tools.ant.BuildEvent)
	 */
	public void messageLogged(BuildEvent ev) {
		try {
			if (ev.getPriority()>this.getMessageOutputLevel())
				return;
			jspWriter.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");			
			if (ev.getTask()!=null)
				jspWriter.print("[<span class=\"taskName\">"+ev.getTask().getTaskName()+"</span>] ");
			jspWriter.print(ev.getMessage());
			jspWriter.print("<br>");			
			jspWriter.flush();
		} catch (IOException e) {
			StringWriter sWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(sWriter));
			Logger.getLogger(AntLauncherJspLogger.class.getName()).severe(sWriter.getBuffer().toString());
		}
	}

	/* (non-Javadoc)
	 * @see org.apache.tools.ant.BuildListener#targetFinished(org.apache.tools.ant.BuildEvent)
	 */
	public void targetFinished(BuildEvent be) {
		try {
			if (be.getException()!=null)
				jspWriter.print("<br>TARGET <span class=\"targetName\">"+be.getTarget()+":</span> FINISHED<br>Cause: <span class=\"errorMessage\">"+be.getException().getLocalizedMessage() +"</span><br>");
			if (be.getMessage()!=null)
				jspWriter.print("<br>TARGET <span class=\"targetName\">"+be.getTarget()+":</span> FINISHED, Message: "+be.getMessage() +"<br>");			
			jspWriter.print("<br>");
			jspWriter.flush();			
		} catch (IOException e) {
			StringWriter sWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(sWriter));
			Logger.getLogger(AntLauncherJspLogger.class.getName()).severe(sWriter.getBuffer().toString());
		}
	}

	/* (non-Javadoc)
	 * @see org.apache.tools.ant.BuildListener#targetStarted(org.apache.tools.ant.BuildEvent)
	 */
	public void targetStarted(BuildEvent be) {
		try {
			jspWriter.print("<span class=\"targetName\">"+be.getTarget()+":</span><br>");
			/*
			//jspWriter.print(be.getTarget()+":<br>");
			jspWriter.print("TARGETSTARTED "+be.getTarget()+":<br>");
			jspWriter.print("&nbsp;getPriority "+be.getPriority()+"<br>");
			*/
			jspWriter.flush();			
		} catch (IOException e) {
			StringWriter sWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(sWriter));
			Logger.getLogger(AntLauncherJspLogger.class.getName()).severe(sWriter.getBuffer().toString());
		}
	}

	/* (non-Javadoc)
	 * @see org.apache.tools.ant.BuildListener#taskFinished(org.apache.tools.ant.BuildEvent)
	 */
	public void taskFinished(BuildEvent be) {
	}

	/* (non-Javadoc)
	 * @see org.apache.tools.ant.BuildListener#taskStarted(org.apache.tools.ant.BuildEvent)
	 */
	public void taskStarted(BuildEvent be) {
	}

	/**
	 * Instantiates a new ant launcher jsp logger.
	 */
	public AntLauncherJspLogger() {
		super();
	}	
 
	/**
	 * Instantiates a new ant launcher jsp logger.
	 * 
	 * @param jspWriter
	 *            the jsp writer
	 */
	public AntLauncherJspLogger(JspWriter jspWriter) {
		super();
		this.jspWriter = jspWriter;
	}

	/**
	 * Gets the jsp writer.
	 * 
	 * @return the jsp writer
	 */
	public JspWriter getJspWriter() {
		return jspWriter;
	}

	/**
	 * Sets the jsp writer.
	 * 
	 * @param jspWriter
	 *            the new jsp writer
	 */
	public void setJspWriter(JspWriter jspWriter) {
		this.jspWriter = jspWriter;
	}

}
