package es.indra.druida.desktop.ant;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.servlet.jsp.JspWriter;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;

//import es.indra.druida.desktop.ant.AntLauncherJspLogger;

/**
 * The Class AntJspLauncher. It launches an ANT script.
 */
public class AntJspLauncher {
	
	/** The build file name. */
	private String buildFileName=null;
	
	/** The target name. */
	private String targetName=null;
	
	/** The properties. */
	private Map properties=null;
	
	/** The jsp out. */
	private JspWriter jspOut=null;
	
	/** The message outputlevel. */
	private int messageOutputlevel;
	
	/*
	public boolean launchDefaultTarget(String buildFileName, JspWriter out, int messageOutputlevel) throws IOException	{	// messageOutputlevel:Project.MSG_ERR, MSG_WARN, MSG_INFO, MSG_VERBOSE, MSG_DEBUG
		return launchTarget(buildFileName, null, null, out, messageOutputlevel);
	}	
	
	public boolean launchDefaultTarget(String buildFileName, Map props, JspWriter out, int messageOutputlevel) throws IOException	{	// messageOutputlevel:Project.MSG_ERR, MSG_WARN, MSG_INFO, MSG_VERBOSE, MSG_DEBUG
		return launchTarget(buildFileName, null, props, out, messageOutputlevel);
	}
	
	public boolean launchTarget(String buildFileName, String targetName, JspWriter out, int messageOutputlevel) throws IOException	{	// messageOutputlevel:Project.MSG_ERR, MSG_WARN, MSG_INFO, MSG_VERBOSE, MSG_DEBUG
		return launchTarget(buildFileName, targetName, null, out, messageOutputlevel);
	}
	
	public boolean launchTarget(String buildFileName, String targetName, Map props, JspWriter out, int messageOutputlevel) throws IOException	{	// messageOutputlevel:Project.MSG_ERR, MSG_WARN, MSG_INFO, MSG_VERBOSE, MSG_DEBUG
		//Buildfile
		File buildFile = new File(buildFileName);
		//Inicio mi logger
		AntLauncherJspLogger myLogger=new AntLauncherJspLogger();
		myLogger.setJspWriter(out);
		myLogger.setMessageOutputLevel(messageOutputlevel);  
		//Construyo el project de ant
		Project project = new Project();
		project.addBuildListener(myLogger);
		project.setSystemProperties();
		project.init();
		ProjectHelper.configureProject(project, buildFile);
		project.setProperty("ant.file", buildFile.getAbsolutePath());
		//A�ado los par�metros
		if (props!=null)	{
			Object [] nProps= props.keySet().toArray();
			for (int i=0; i<nProps.length; i++)	
				project.setProperty((String)nProps[i],(String)props.get(nProps[i]));	
		}
		try {
			out.write("Buildfile: "+buildFile.getName()+"<br><br>");
			if (targetName==null)
				targetName=project.getDefaultTarget();
			project.executeTarget(targetName);
		} catch(Exception e) {
			out.write("BUILD FAILED<br>");
			out.write("Cause: <span class=\"errorMessage\">"+e.toString()+"</span><br>");
			return false;
		}	 
		out.write("BUILD SUCCESSFUL<br>");
		//out.write("Total time: 2 seconds");		
		return true;
	}*/
	
	/**
	 * Launch the script.
	 * 
	 * @return true, if successful
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public boolean launch() throws IOException	{	// messageOutputlevel:Project.MSG_ERR, MSG_WARN, MSG_INFO, MSG_VERBOSE, MSG_DEBUG
		//Buildfile
		File buildFile = new File(buildFileName);
		//Inicio mi logger
		AntLauncherJspLogger myLogger=new AntLauncherJspLogger();
		myLogger.setJspWriter(jspOut);
		myLogger.setMessageOutputLevel(messageOutputlevel);  
		//Construyo el project de ant
		Project project = new Project();
		project.addBuildListener(myLogger);
		project.setSystemProperties();
		project.init();
		ProjectHelper.configureProject(project, buildFile);
		project.setProperty("ant.file", buildFile.getAbsolutePath());
		//A�ado los par�metros
		if (properties!=null)	{
			Object [] nProps= properties.keySet().toArray();
			for (int i=0; i<nProps.length; i++)	
				project.setProperty((String)nProps[i],(String)properties.get(nProps[i]));	
		}
		try {
			jspOut.write("Buildfile: "+buildFile.getName()+"<br><br>");
			if (targetName==null)
				targetName=project.getDefaultTarget();
			project.executeTarget(targetName);
		} catch(Exception e) {
			jspOut.write("BUILD FAILED<br>");
			jspOut.write("Cause: <span class=\"errorMessage\">"+e.toString()+"</span><br>");
			return false;
		}	 
		jspOut.write("BUILD SUCCESSFUL<br>");
		//out.write("Total time: 2 seconds");		
		return true;
	}
	
	
	
	
	/**
	 * Gets the builds the file name.
	 * 
	 * @return the builds the file name
	 */
	public String getBuildFileName() {
		return buildFileName;
	}

	/**
	 * Sets the builds the file name.
	 * 
	 * @param buildFileName
	 *            the new builds the file name
	 */
	public void setBuildFileName(String buildFileName) {
		this.buildFileName = buildFileName;
	}

	/**
	 * Gets the message outputlevel.
	 * 
	 * @return the message outputlevel
	 */
	public int getMessageOutputlevel() {
		return messageOutputlevel;
	}

	/**
	 * Sets the message outputlevel.
	 * 
	 * @param messageOutputlevel
	 *            the new message outputlevel
	 */
	public void setMessageOutputlevel(int messageOutputlevel) {
		this.messageOutputlevel = messageOutputlevel;
	}


	/**
	 * Gets the target name.
	 * 
	 * @return the target name
	 */
	public String getTargetName() {
		return targetName;
	}

	/**
	 * Sets the target name.
	 * 
	 * @param targetName
	 *            the new target name
	 */
	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}




	/**
	 * Gets the jsp out.
	 * 
	 * @return the jsp out
	 */
	public JspWriter getJspOut() {
		return jspOut;
	}




	/**
	 * Sets the jsp out.
	 * 
	 * @param jspOut
	 *            the new jsp out
	 */
	public void setJspOut(JspWriter jspOut) {
		this.jspOut = jspOut;
	}




	/**
	 * Gets the properties.
	 * 
	 * @return the properties
	 */
	public Map getProperties() {
		return properties;
	}




	/**
	 * Sets the properties.
	 * 
	 * @param properties
	 *            the new properties
	 */
	public void setProperties(Map properties) {
		this.properties = properties;
	}
}