package es.indra.druida.desktop.fileaccess;

import es.indra.druida.desktop.remoting.ServerResponse;

/**
 * The Class FileAccessResponse.
 */
public class FileAccessResponse extends ServerResponse {
	
	/** The text content. */
	private String textContent;

	/**
	 * Gets the text content.
	 * 
	 * @return the text content
	 */
	public String getTextContent() {
		return textContent;
	}

	/**
	 * Sets the text content.
	 * 
	 * @param textContent
	 *            the new text content
	 */
	public void setTextContent(String textContent) {
		this.textContent = textContent;
	}
	
}
