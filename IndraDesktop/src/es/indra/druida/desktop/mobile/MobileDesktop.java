package es.indra.druida.desktop.mobile;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.PageContext;

import es.indra.druida.desktop.configurator.DesktopConfig;
import es.indra.druida.desktop.configurator.DesktopUserConfig;
import es.indra.druida.desktop.configurator.ResourcesService;
import es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader;
import es.indra.druida.desktop.configurator.beans.AppGroup;
import es.indra.druida.desktop.configurator.beans.AppParameter;
import es.indra.druida.desktop.configurator.beans.Application;
import es.indra.druida.desktop.configurator.beans.ConfigProperty;
import es.indra.druida.desktop.configurator.beans.Configuration;
import es.indra.druida.desktop.configurator.beans.Engine;
import es.indra.druida.desktop.configurator.beans.Icon;
import es.indra.druida.desktop.configurator.beans.ResolvedIcon;

public class MobileDesktop {
	public static void startDesktop(HttpServletRequest request, HttpServletResponse response, DesktopConfig _config, PageContext pageContext) throws Exception{
		DesktopUserConfig desktopUserConfig=(DesktopUserConfig)(request.getSession().getAttribute("userConfig"));
		//String usuario=desktopUserConfig.getId();
		AbstractLoader configLoader=_config.getCustomConfigurationLoader(request, response);
		ConfigProperty[] props=(ConfigProperty[])configLoader.getObjects();
		request.getSession().setAttribute("configProperties",props); //Suponemos que hay pocos y que asi me ahorro leer luego del XML
		Locale selectedLocale=null;
		for (int i=0; i<props.length; i++)	{
			ConfigProperty prop=props [i];
			if (prop.getName().equals("locale") && prop.getValue()!=null && !prop.getValue().equals("default")){
				//selectedLocale=prop.getValue();
				String[] selectedLocaleParts=prop.getValue().split("_");
				if (selectedLocaleParts.length==0)
					selectedLocale=null;
				if (selectedLocaleParts.length==1)
					selectedLocale=new Locale(selectedLocaleParts[0]);
				else if (selectedLocaleParts.length==2)
					selectedLocale=new Locale(selectedLocaleParts[0],selectedLocaleParts[1]);
				else if (selectedLocaleParts.length>2)
					selectedLocale=new Locale(selectedLocaleParts[0],selectedLocaleParts[1],selectedLocaleParts[2]);					
			}
		}
		request.getSession().setAttribute("selectedLocale",selectedLocale);
		//Ahora cargo los iconos del usuario 
		AbstractLoader loader=DesktopConfig.getSingleton().getIconLoader(request, response); 
		Icon[] iconos=(Icon[])loader.getObjects();		
		//Hay que procesar los iconos para transformarlos de aplicacion a URLs
			//--> URL relativa a absoluta
			//--> Multiidioma
			//--> Motores-->URL
			//Parametros de la aplicaci�n se transforman tb
		HashMap hmApplications=new HashMap<String, Application>();
		Application[] applications= desktopUserConfig.getApplications();
		for (int j=0; j<applications.length; j++){
			Application apl=applications[j];
			hmApplications.put(apl.getId(), apl);
		}	
		Configuration deskConf=DesktopConfig.getSingleton().getConfiguration();		
		Engine[] engines=deskConf.getEngines();
		HashMap hmEngines=new HashMap<String, Engine>();
		for (int k=0; k<engines.length; k++){
			Engine eng=engines[k];
			hmEngines.put(eng.getId(),eng);
		}
		
		ArrayList<ResolvedIcon> iconosResueltosAL = new ArrayList<ResolvedIcon>();
		//ResolvedIcon[] iconosResueltos=new 
		
		for (int i=0; i<iconos.length; i++){
			//1) Busco la aplicaci�n relacionada
			Application apl=(Application)hmApplications.get(iconos[i].getApplication());
			if (apl!=null){
				String finalUrl=null;
				String iconUrl=null;
				String iconText="";

				if (apl.getParameters()!=null)
					System.out.print("["+apl.getParameters()[0].getName()+"]");
				//2) Si la aplicacion tiene engine tomo el URL del engine

				if (apl.getEngine()!=null && !apl.getEngine().equals("")){
					if (hmEngines.containsKey(apl.getEngine())){
						Engine eng=(Engine)hmEngines.get(apl.getEngine());
						finalUrl=eng.getUrl();
					}else{
						//TO-DO: send exception? el engine no existe
						finalUrl="";
					}
				} else if(apl.getRelativeUrl()!=null && !apl.getRelativeUrl().equals("")){
					//3.1) Si no, toma la URL que tenga la aplicacion, si es relativa la convierte
					/*
		var idGrupoAplicacion=appCfg.grupo
         var nombreGrupoApp= this.getApplicationsGroupsConfig()[idGrupoAplicacion].name
         appCfg["url"]=this.getDesktopProperties()["urlAppRoot"] +"/"+ nombreGrupoApp +"/"+appCfg["relativeUrl"]
					 */
					//System.out.println("<"+apl.getGroup()+","+apl.getGroupId()+">");
					finalUrl=DesktopConfig.getSingleton().get("urlAppRoot") + "/" + apl.getGroupId()+ "/" + apl.getRelativeUrl();
					
				} else {
					//3.2 si no hay URL relativa la deja como est�
					finalUrl=apl.getUrl();
				}
				//4) resuelvo los parametros escapando para la URL
				AppParameter[] params = (AppParameter[])apl.getParameters();
				if (params!=null && params.length>0){
			         if (finalUrl.indexOf("?")==-1)
			        	 finalUrl=finalUrl+"?";
			         else
			        	 finalUrl=finalUrl+"&";
			         for (int r=0;r<params.length;r++)	{
			        	 if (r>0)
			        		 finalUrl=finalUrl+"&";
			        	 finalUrl=finalUrl + URLEncoder.encode(params[r].getName(), "UTF-8") + "=" + URLEncoder.encode(params[r].getValue(), "UTF-8");
			         }
				}		
				//5) Tomo el icono y le a�ado lo que le falta para convertirlo en una URL
				if (apl.getIcon()!=null){
					iconUrl=DesktopConfig.getSingleton().get("urlIcons") + "/" + apl.getIcon();
				}else{
					iconUrl=DesktopConfig.getSingleton().get("urlIcons") + "/aucun.gif";
				}
				//6) Tomo el texto y luego lo transformo seg�n el idioma
				if (apl.getIconText()!=null && !apl.getIconText().equals("")){
					iconText=apl.getIconText();
				} else if (apl.getTitle()!=null && !apl.getTitle().equals("")){
					iconText=apl.getTitle();
				} else if (apl.getDescription()!=null && !apl.getDescription().equals("")){
					iconText=apl.getDescription();
				} else {
					iconText=apl.getId();
				}
				iconText=replaceI18NValues(iconText, request);
				if (finalUrl!=null && !finalUrl.equals("")) //Puede ser de tipo js="", lo ignoramos en m�viles
					iconosResueltosAL.add(new ResolvedIcon(finalUrl,iconText,iconUrl));
			} else {
				// TO-DO: send exception? la aplic no existe
			}
		}

		//pageContext.setAttribute("iconos", iconos);
		pageContext.setAttribute("iconos", iconosResueltosAL.toArray(new ResolvedIcon[iconosResueltosAL.size()]));
		
	}
	

	
	private static String replaceI18NValues(String value, HttpServletRequest request){
		//Duplicado en JS, posible unificaci�n
		if (value!=null && value.length()>4 && value.charAt(0)=='$' && value.charAt(1) == '{' && value.charAt(value.length()-1) == '}'){
			String code=value.substring(2,value.length()-1);
			int indice=code.indexOf(",");
			if (indice==-1)
				return value;
			else{
				String bundleName=code.substring(0,indice);
				String key=code.substring(indice+1);
				ResourceBundle resBundle=ResourcesService.getResourceBundle(bundleName,request);
				String valueModif=resBundle.getString(key);
				if (valueModif!=null)
					return valueModif;
				else
					return value;
			}	
		}
		return value;
	}
}
