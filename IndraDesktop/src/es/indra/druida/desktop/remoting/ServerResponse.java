package es.indra.druida.desktop.remoting;

/**
 * The Class ServerResponse.
 */
public class ServerResponse {
	//
	/** The error de operacion. */
	private boolean errorDeOperacion=false;	
	
	/** The excepcion error. */
	private Exception excepcionError;
	
	/**
	 * Checks if is error de operacion.
	 * 
	 * @return true, if is error de operacion
	 */
	public boolean isErrorDeOperacion() {
		return errorDeOperacion;
	}
	
	/**
	 * Sets the error de operacion.
	 * 
	 * @param errorDeOperacion
	 *            the new error de operacion
	 */
	public void setErrorDeOperacion(boolean errorDeOperacion) {
		this.errorDeOperacion = errorDeOperacion;
	}
	
	/**
	 * Gets the excepcion error.
	 * 
	 * @return the excepcion error
	 */
	public Exception getExcepcionError() {
		return excepcionError;
	}
	
	/**
	 * Sets the excepcion error.
	 * 
	 * @param excepcionError
	 *            the new excepcion error
	 */
	public void setExcepcionError(Exception excepcionError) {
		this.excepcionError = excepcionError;
	}	
}
