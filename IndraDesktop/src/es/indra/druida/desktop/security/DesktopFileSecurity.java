package es.indra.druida.desktop.security;

import java.io.File;

import javax.servlet.http.HttpSession;

import es.indra.druida.desktop.configurator.DesktopUserConfig;
import java.util.ResourceBundle;
import es.indra.druida.desktop.configurator.ResourcesService;
/**
 * The Class DesktopFileSecurity.
 */
public class DesktopFileSecurity {

	/**
	 * Gets the file.
	 * 
	 * @param filePath
	 *            the file path
	 * @param sesion
	 *            the sesion
	 * 
	 * @return the file
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public static File getFile(String filePath, HttpSession sesion) throws Exception {
		File baseDir=getBasedir(sesion); //verifica tb. la sesion
		File file= new File(baseDir, filePath);
		String fileCanonicalPath=file.getCanonicalPath();
		String dirBaseCanonicalPath=baseDir.getCanonicalPath();
		if (fileCanonicalPath.indexOf(dirBaseCanonicalPath)!=0)
			throw new Exception(ResourcesService.getResourceBundle("core",sesion).getString("fileSecurity.txt1")); //"Security error: attempt to access file beyond user FileSystem."
		return file;
	} 
	
	/**
	 * Gets the basedir.
	 * 
	 * @param sesion
	 *            the sesion
	 * 
	 * @return the basedir
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public static File getBasedir(HttpSession sesion) throws Exception{
	
		if (sesion==null)	
			throw new Exception(ResourcesService.getResourceBundle("core",sesion).getString("fileSecurity.txt2"));//"Session expired."
		//Object basePathObj=sesion.getAttribute("fileSystemRoot");
		Object userConfigObj = sesion.getAttribute("userConfig");//TODO: unificar las llamadas a sesi�n en el loader 
		if (userConfigObj==null)
			throw new Exception(ResourcesService.getResourceBundle("core",sesion).getString("fileSecurity.txt3"));// "User not in session."
		DesktopUserConfig userConfig = (DesktopUserConfig) sesion.getAttribute("userConfig"); 
		//if (basePathObj==null)
		if (userConfig.getFileSystemRoot()==null)
			throw new Exception(ResourcesService.getResourceBundle("core",sesion).getString("fileSecurity.txt4"));// "No fileSystemRoot info for the user."
		//String basePath=(String)basePathObj;
		File baseDir= new File(userConfig.getFileSystemRoot());
		if (!baseDir.exists())	
			throw new Exception(ResourcesService.getResourceBundle("core",sesion).getString("fileSecurity.txt5"));// "Base directory doesn't exist."
		return baseDir;
	}
	
}
