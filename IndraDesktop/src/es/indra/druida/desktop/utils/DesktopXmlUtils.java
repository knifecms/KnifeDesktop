package es.indra.druida.desktop.utils;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * The Class DesktopXmlUtils.
 */
public class DesktopXmlUtils {
	
	/**
	 * Gets the document from file.
	 * 
	 * @param fileInput
	 *            the file input
	 * 
	 * @return the document from file
	 * 
	 * @throws ParserConfigurationException
	 *             the parser configuration exception
	 * @throws SAXException
	 *             the SAX exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static Document getDocumentFromFile(File fileInput) throws ParserConfigurationException, SAXException, IOException	{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setExpandEntityReferences(true);
		DocumentBuilder parser = dbFactory.newDocumentBuilder();
		Document doc = (Document) parser.parse(fileInput);
		return doc;
	}
	
	/**
	 * Gets the first child with attribute.
	 * 
	 * @param e
	 *            the e
	 * @param attr
	 *            the attr
	 * @param value
	 *            the value
	 * 
	 * @return the first child with attribute
	 */
	public static Element getFirstChildWithAttribute(Element e, String attr, String value)	{
		NodeList childNodes=e.getChildNodes();
		for (int i=0; i<childNodes.getLength(); i++){
			Node childNode=childNodes.item(i);
			/*if (childNode.getNodeType()==Node.ELEMENT_NODE){
				Element childElem=(Element)childNode;
				if (childElem.hasAttribute(attr)){
					if (childElem.getAttribute(attr).equals(value))
						return childElem;
				}
			}*/
			if (childNode.getNodeType()==Node.ELEMENT_NODE && ((Element)childNode).hasAttribute(attr) && ((Element)childNode).getAttribute(attr).equals(value)){
				return (Element)childNode;
			}
		}
		return null;		
	}	
	
	/**
	 * Gets the first child with tag name.
	 * 
	 * @param e
	 *            the e
	 * @param tagName
	 *            the tag name
	 * 
	 * @return the first child with tag name
	 */
	public static Element getFirstChildWithTagName(Element e, String tagName)	{
		//System.out.println("Busco "+tagName);
		NodeList childNodes=e.getChildNodes();
		for (int i=0; i<childNodes.getLength(); i++){
			Node childNode=childNodes.item(i);
			if (childNode.getNodeType()==Node.ELEMENT_NODE){
				Element childElem=(Element)childNode;
				//System.out.println("SERA "+childElem.getTagName()+"?");
				if (childElem.getTagName().equals(tagName)){
					return childElem;
				}
			}
		}
		return null;		
	}		
	
	/**
	 * Escribe dom en disco.
	 * 
	 * @param doc
	 *            the doc
	 * @param fichero
	 *            the fichero
	 * 
	 * @throws TransformerException
	 *             the transformer exception
	 */
	public static void escribeDOMEnDisco(Document doc, String fichero) throws TransformerException {
		DOMSource domSource = new DOMSource(doc);
		StreamResult streamResult = new StreamResult(new File( fichero ));
		TransformerFactory tf = TransformerFactory.newInstance();
		//tf.setAttribute("indent-number", new Integer(3));
		Transformer serializer = tf.newTransformer();
		serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
		serializer.setOutputProperty(OutputKeys.ENCODING,"ISO-8859-1");
		//serializer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,"users.dtd");
		serializer.setOutputProperty(OutputKeys.INDENT,"yes");
		serializer.transform(domSource, streamResult); 
	}	
}
