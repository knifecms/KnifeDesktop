package es.indra.druida.desktop.utils;

import java.io.File;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

/**
 * The Class DesktopWriter.
 */
public class DesktopWriter {

	/**
	 * Genera cadena javascript.
	 * 
	 * @param s
	 *            the s
	 * 
	 * @return the string
	 */
	static public String generaCadenaJavascript(String s){
		return generaCadenaJavascript(s, true);
	}
	
	/**
	 * Genera cadena javascript.
	 * 
	 * @param s
	 *            the s
	 * @param esComillaDoble
	 *            the es comilla doble
	 * 
	 * @return the string
	 */
	static public String generaCadenaJavascript(String s, boolean esComillaDoble){
		if (s==null)
			return "null";
	    String regExpSlash = "\\\\";     //En realidad pone "\\" que es la expresi髇 regular de \
		s = s.replaceAll(regExpSlash, "\\\\\\\\");
		s = s.replaceAll("\n", "\\\\n");
		String c=((esComillaDoble)?"\"":"'");
		s = s.replaceAll(c, "\\\\"+c);
		return c + s + c;
	}
	
	/**
	 * Genera cadena html.
	 * 
	 * @param s
	 *            the s
	 * 
	 * @return the string
	 */
	static public String generaCadenaHtml(String s){
		//s=s.replaceAll("&","X");
		s=s.replaceAll("&","&amp;");
		s=s.replaceAll("\"","&quot;");
		return "\"" + s + "\"";
	}	
	
	/**
	 * Genera codigo html escapado.
	 * 
	 * @param s
	 *            the s
	 * 
	 * @return the string
	 */
	static public String generaCodigoHtmlEscapado(String s){
		s=s.replaceAll("&","&amp;");
		s=s.replaceAll("\n","<br>");
		s=s.replaceAll("<","&lt;");
		s=s.replaceAll("�","&euro;");
		
		//OJO: &#x20AC;
		
		return s ;
	}
	
	/**
	 * Genera codigo html escapado para text area.
	 * 
	 * @param s
	 *            the s
	 * 
	 * @return the string
	 */
	static public String generaCodigoHtmlEscapadoParaTextArea(String s){
		s=s.replaceAll("&","&amp;");
		s=s.replaceAll("\n","<br>");
		s=s.replaceAll("<","&lt;");
		return s ;
	}	
	
	/**
	 * Escribe dom en disco.
	 * 
	 * @param doc
	 *            the doc
	 * @param fichero
	 *            the fichero
	 * 
	 * @throws TransformerException
	 *             the transformer exception
	 
	public static void escribeDOMEnDisco(Document doc, String fichero) throws TransformerException {
		DOMSource domSource = new DOMSource(doc);
		StreamResult streamResult = new StreamResult(new File( fichero ));
		TransformerFactory tf = TransformerFactory.newInstance();
		//tf.setAttribute("indent-number", new Integer(3));
		Transformer serializer = tf.newTransformer();
		serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
		serializer.setOutputProperty(OutputKeys.ENCODING,"ISO-8859-1");
		//serializer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM,"users.dtd");
		serializer.setOutputProperty(OutputKeys.INDENT,"yes");
		serializer.transform(domSource, streamResult); 
	}*/
}
