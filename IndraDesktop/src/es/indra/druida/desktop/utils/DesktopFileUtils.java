package es.indra.druida.desktop.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * The Class DesktopFileUtils.
 */
public class DesktopFileUtils {
	
	/**
	 * Copy directory.
	 * 
	 * @param srcDir
	 *            the src dir
	 * @param dstDir
	 *            the dst dir
	 * 
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void copyDirectory(File srcDir, File dstDir) throws IOException {
		if (srcDir.isDirectory()) {
			if (!dstDir.exists()) {
				dstDir.mkdir();
			}
			String[] hijos = srcDir.list();
			for (int i = 0; i < hijos.length; i++) {
				copyDirectory(new File(srcDir, hijos[i]), new File(dstDir,hijos[i]));
			}
		} else {
			InputStream in = new FileInputStream(srcDir);
			OutputStream out = new FileOutputStream(dstDir);

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		}
	}
	
}
