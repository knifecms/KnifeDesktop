package es.indra.druida.desktop.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * The Class SHA1Encoder.
 */
public class SHA1Encoder {    
	
	/** The md. */
	private MessageDigest md;    
	
	/** The digest. */
	private byte[] buffer, digest;    
	
	/** The hash. */
	private String hash = "";    
	
	/**
	 * Gets the hash.
	 * 
	 * @param message
	 *            the message
	 * 
	 * @return the hash
	 * 
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 */
	public String getHash(String message) throws NoSuchAlgorithmException {
		buffer = message.getBytes();
		md = MessageDigest.getInstance("SHA1");
		md.update(buffer);
		digest = md.digest();
		for(byte aux : digest) {
			int b = aux & 0xff;
			if (Integer.toHexString(b).length() == 1) 
				hash += "0";
			hash += Integer.toHexString(b);
		}        
		return hash;
	}
}