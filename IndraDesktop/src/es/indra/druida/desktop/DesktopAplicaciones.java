package es.indra.druida.desktop;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;

import es.indra.druida.desktop.configurator.DesktopConfig;
import es.indra.druida.desktop.configurator.DesktopUserConfig;
import es.indra.druida.desktop.configurator.beans.AppGroup;
import es.indra.druida.desktop.configurator.beans.Application;
import es.indra.druida.desktop.configurator.beans.Skin;

/*Maneja el salvar / cargar el estado de las ventanas de un usuario*/
/*Entiende el XML de la ventana y entiende el JSON asociado */

/**
 * The Class DesktopAplicaciones. Trata con las aplicaciones registradas.
 */
public class DesktopAplicaciones {
	
	/**
	 * Write script object.
	 * 
	 * @param out
	 *            the out
	 * @param request
	 *            the request
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public static void writeScriptObject(JspWriter out, HttpServletRequest request) throws Exception	{
		
		StringBuffer _appJS = new StringBuffer();	
		StringBuffer _grpAppJS = new StringBuffer();	
		// SKINS
		_appJS.append(DesktopAplicaciones.dameHtmlInicializarAplicacionesDesdeXml(true,request));
		_grpAppJS.append(dameHtmlInicializarGruposAplicacionesDesdeXml());
		out.print("\n oAplicaciones={"+_appJS.toString()+"}");
		out.print("\n oGruposAplicaciones=["+_grpAppJS.toString()+"]");
		out.print("\n");		
		
	}
	
	/**
	 * Dame html inicializar aplicaciones desde xml.
	 * 
	 * @param primeroSinComillas
	 *            primero sin comillas
	 * @param request
	 *            the request
	 * 
	 * @return the string
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public static String dameHtmlInicializarAplicacionesDesdeXml(boolean primeroSinComillas, HttpServletRequest request) throws Exception	{
		StringBuffer _appJS = new StringBuffer();
		
		//Application[] appssOld = DesktopConfig.getSingleton().getApplications();
		
		DesktopUserConfig userConfig = (DesktopUserConfig) request.getSession().getAttribute("userConfig");
		//Application[] appss = userConfig.getApplicationsI18N();
		Application[] appss = userConfig.getApplications();
		
		//System.out.println(">>El usuario tiene "+appss.length+" aplicaciones sobre un total de "+appssOld.length);
		//XXX TODO: Solo las aplicaciones que el usuario pueda ver con su perfil ...
		int longo = appss.length;
		for ( int i = 0 ; i < longo; i++) {
			if (!(primeroSinComillas && i==0))
				_appJS.append(",");
			Application app=appss[i];
			_appJS.append(app.toString());
		}
		return _appJS.toString();
	}

	/**
	 * Dame html inicializar skins desde xml.
	 * 
	 * @param primeroSinComillas
	 *            primero sin comillas
	 * 
	 * @return the string
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public static String dameHtmlInicializarSkinsDesdeXml(boolean primeroSinComillas) throws Exception	{
		StringBuffer _skinJS = new StringBuffer();
		Skin[] skins = DesktopConfig.getSingleton().getSkins();
		int longo = skins.length;
		for ( int i = 0 ; i < longo; i++) {
			if (!(primeroSinComillas && i==0))
				_skinJS.append(",");
			Skin skin=skins[i];
			_skinJS.append(skin.toString());
		}
		return _skinJS.toString();
	}	
	
	/**
	 * Dame html inicializar grupos aplicaciones desde xml.
	 * 
	 * @return the string
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public static String dameHtmlInicializarGruposAplicacionesDesdeXml() throws Exception	{
		StringBuffer _appJS = new StringBuffer();
		AppGroup[] appGroup = DesktopConfig.getSingleton().getAppGroups();
		int longo = appGroup.length;
		for ( int i = 0 ; i < longo; i++) {
			
			AppGroup appG=appGroup[i];
			_appJS.append(appG.toString());
			if (i!=(longo-1)){
				_appJS.append(",");
			}
		}
		//_appJS.append("]");
		//System.out.println(_appJS);
		return _appJS.toString();
	}		

	/**
	 * Dame html aplicacion.
	 * 
	 * @param _aplicacion the _aplicacion
	 * @param indiceGrupo the indice grupo
	 * 
	 * @return the string
	 */
	/*private static String dameHtmlAplicacion(Element _aplicacion, int indiceGrupo)	{ //Da el de UNA apl
		//Escribo "<id>": {
		StringBuffer _appJS = new StringBuffer();
		_appJS.append(DesktopWriter.generaCadenaJavascript(_aplicacion.getAttribute("id")));
		_appJS.append(":{");
		//Escribo los atributos menos el ID ...
		NamedNodeMap losAtribs = _aplicacion.getAttributes();
		boolean escritoElPrimero=false;
		for (int j=0;j<losAtribs.getLength();j++)	{
			Node elAtrib = losAtribs.item(j);				
			if ( !elAtrib.getNodeName().equals("id") )	{
				if (escritoElPrimero)
					_appJS.append(",");
				_appJS.append("\""+elAtrib.getNodeName()+"\":");
				_appJS.append(DesktopWriter.generaCadenaJavascript(elAtrib.getFirstChild().getNodeValue()));
				escritoElPrimero=true;
			}
		}
		//a�adimos como atributo el indice de grupo
		_appJS.append(",grupo:"+indiceGrupo);
		if (_aplicacion.getNodeName().equals("WIDGET"))
			_appJS.append(",widget:true");	
		NodeList _daemons= _aplicacion.getElementsByTagName("DAEMON");
		if (_daemons.getLength()>0){
			_appJS.append(",daemons:[");
			for ( int j = 0 ; j < _daemons.getLength(); j++) {
				if (j>0)
					_appJS.append(",");
				Element unDaemon=(Element) _daemons.item(j);
				_appJS.append("{");
				_appJS.append("milliseconds:"+unDaemon.getAttribute("milliseconds"));
				if (unDaemon.hasAttribute("script"))	{
					_appJS.append(",script:\""+unDaemon.getAttribute("script")+"\"");
				}
				_appJS.append("}");
			}
			_appJS.append("]");
		}
		//ahora si tiene parametros los sacamos tambi�n
		NodeList _param = _aplicacion.getElementsByTagName("PARAMETER");
		if (_param.getLength()>0){
			_appJS.append(",\"parametros\":[");
			for ( int j = 0 ; j < _param.getLength(); j++) {
				if (j>0)
					_appJS.append(",");
				Element unparam=(Element) _param.item(j);
				_appJS.append("{\"nombre\":\""+unparam.getAttribute("name")+"\",");
				_appJS.append("\"valor\":"+DesktopWriter.generaCadenaJavascript(unparam.getFirstChild().getNodeValue()));
				_appJS.append("}");
			}
			_appJS.append("]");
		}
		_appJS.append("}");		
		return _appJS.toString();
	}*/
}
