package es.indra.druida.desktop.configurator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * The Class AbstractDesktopConfigurationClass.
 */
public abstract class AbstractDesktopConfigurationClass {

	/** The request. */
	private HttpServletRequest request;
	
	/** The response. */
	private HttpServletResponse response;
		
	/**
	 * Instantiates a new abstract desktop configuration class.
	 * 
	 * @param req
	 *            the req
	 * @param res
	 *            the res
	 */
	public AbstractDesktopConfigurationClass(HttpServletRequest req, HttpServletResponse res) {
		super();
		
		request = req;
		response = res;
	}
	
	/**
	 * Checks if is user logged.
	 * 
	 * @return true, if is user logged
	 */
	public boolean isUserLogged(){
		return (request.getSession().getAttribute("userConfig") != null);
	}
	
	/**
	 * Gets the login id.
	 * 
	 * @param req
	 *            the req
	 * 
	 * @return the login id
	 */
	public abstract String getLoginID(HttpServletRequest req);
	
	/**
	 * Gets the user object.
	 * 
	 * @return the user object
	 */
	public DesktopUserConfig getUserObject(){
		if (request.getSession().getAttribute("userConfig") != null)
			return ((DesktopUserConfig) request.getSession().getAttribute("userConfig"));
		else
			return null;
	}
	
	/**
	 * Sets the user object.
	 * 
	 * @param _user
	 *            the new user object
	 */
	public void setUserObject( DesktopUserConfig _user ){
		request.getSession().setAttribute("userConfig", _user);
	}
	
	/**
	 * Creates the user.
	 * 
	 * @return the desktop user config
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public abstract DesktopUserConfig createUser() throws Exception;
	//public abstract boolean existUser( DesktopUserConfig _user );
	/**
	 * Connect user.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public abstract void connectUser() throws Exception;
	
	/**
	 * Disconnect user.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public abstract void disconnectUser() throws Exception;
	
	/**
	 * Process user.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public abstract void processUser (HttpServletRequest request, HttpServletResponse response) throws Exception;

	/**
	 * Gets the request.
	 * 
	 * @return the request
	 */
	public HttpServletRequest getRequest() {
		return request;
	}

	/**
	 * Gets the response.
	 * 
	 * @return the response
	 */
	public HttpServletResponse getResponse() {
		return response;
	}
	
	/**
	 * Process.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public void process() throws Exception {
		processUser(request, response);
	}

	public void reloadUser() throws Exception {
				
	}
		
}
