package es.indra.druida.desktop.configurator;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
//import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ResourcesService {
	
	public static ResourceBundle getResourceBundle(String bundleName, HttpServletRequest request){
		Locale locale=null;
		Object oSelectedLocale=request.getSession().getAttribute("selectedLocale");
		if (oSelectedLocale==null)	{
			locale=request.getLocale();
		} else	{
			locale=(Locale)oSelectedLocale;
		}	
		bundleName="resources."+bundleName;
		return ResourceBundle.getBundle(bundleName, locale);
	}
	
	public static ResourceBundle getResourceBundle(String bundleName, HttpSession session){
		Locale locale=null;
		Object oSelectedLocale=session.getAttribute("selectedLocale");
		if (oSelectedLocale==null)	{
			locale=new Locale("en");
		} else	{
			locale=(Locale)oSelectedLocale;
		}	
		bundleName="resources."+bundleName;
		return ResourceBundle.getBundle(bundleName, locale);
	}	
	
	//	Este m�todo ser� llamado por DWR
	public static HashMap<String, String> getBundle(String bundleName, HttpServletRequest request)	{ 
		ResourceBundle bundle = getResourceBundle(bundleName, request);
		Enumeration<String> keys=bundle.getKeys();
		HashMap<String, String> ret=new HashMap<String, String>();
		while( keys.hasMoreElements() ){
			String key=(String)keys.nextElement();		
			ret.put(key, bundle.getString(key));
		}	
		return ret;	
	}

	/*
	public static String replaceI18NValues(String value, HttpServletRequest request) {
		//${drExplorer,drExplorer.desc.fse} --> valor internacionalizado
		if (value==null)
			return null;
		if (value.length()>4 && value.charAt(0)=='$' && value.charAt(1) == '{' && value.charAt(value.length()-1) == '}'){
			String code=value.substring(2,value.length()-1);
			int indice=code.indexOf(",");
			if (indice==-1)
				return value;
			else{
				String bundle=code.substring(0,indice);
				String key=code.substring(indice+1);
				//System.out.println(">>>>>>>>>>>>>>>"+bundle+"--"+key);
				try {
					return ResourcesService.getResourceBundle(bundle,request).getString(key);
				} catch(MissingResourceException e){
					return value;
				}
			}
		}
		return value;
	}*/
	
}
