package es.indra.druida.desktop.configurator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader;
import es.indra.druida.desktop.configurator.beans.ConfigProperty;
import es.indra.druida.desktop.configurator.beans.Icon;
import es.indra.druida.desktop.configurator.beans.SkinChanges;
import es.indra.druida.desktop.configurator.beans.Window;
import es.indra.druida.desktop.configurator.beans.converters.CustomConfigurationConverter;
import es.indra.druida.desktop.configurator.beans.converters.IconConverter;
import es.indra.druida.desktop.configurator.beans.converters.SkinChangesConverter;
import es.indra.druida.desktop.configurator.beans.converters.WindowConverter;
import es.indra.druida.desktop.remoting.ServerResponse;

public class SaveSessionService {

	public ServerResponse saveSession(Boolean cerrarSesion, Boolean guardar, String jsonVentanas, String jsonIconos, String jsonConfig, String jsonSkin, HttpServletRequest request, HttpServletResponse response)	{
		ServerResponse resp=new ServerResponse();		
		try	{
			if (request.getSession().getAttribute("userConfig")!=null){
				DesktopUserConfig userConfig = (DesktopUserConfig) request.getSession().getAttribute("userConfig");
				//Verifico rol especial nosessionsave
				String[] roles=userConfig.getRoles();
				if (roles!=null){
					for (int i=0; i<roles.length && guardar; i++)
						if (roles[i].toLowerCase().equals("nosessionsave"))
							guardar=false;
				}
				if (guardar){
					if (jsonVentanas!=null)	{ //VENTANAS
						Window[] windows=WindowConverter.jsonToBeanArray(jsonVentanas);
						AbstractLoader windowLoader=DesktopConfig.getSingleton().getWindowLoader(request, response);
						windowLoader.setObjects(windows);
					}
					if (jsonIconos!=null)	{ //ICONOS
						Icon[] icons=IconConverter.jsonToBeanArray(jsonIconos);
						AbstractLoader iconLoader=DesktopConfig.getSingleton().getIconLoader(request, response);
						iconLoader.setObjects(icons);
					}
					if (jsonConfig!=null)	{ //CONFIGURACIÓN
						ConfigProperty[] props=CustomConfigurationConverter.jsonToBeanArray(jsonConfig);
						AbstractLoader customConfigLoader=DesktopConfig.getSingleton().getCustomConfigurationLoader(request, response);
						customConfigLoader.setObjects(props);
					}	
					if (jsonSkin!=null)	{ //CONFIGURACIÓN SKIN
						SkinChanges[] cambios=new SkinChanges[1];
						cambios[0]=SkinChangesConverter.jsonToBean(jsonSkin);
						if (cambios[0]!=null)	{
							//AbstractLoader customConfigLoader=DesktopConfig.getSingleton().getSkinChangesLoader(request, response);
							AbstractLoader customLoader=DesktopConfig.getSingleton().getCustomSkinParametersLoader(request, response);
							customLoader.setObjects(cambios);
						}
					}
				}
				//Y VACIAMOS LA SESIÓN
				if (cerrarSesion){
					request.getSession().removeAttribute("userConfig");
				}				
			}
		} catch (Exception e)	{
			resp.setErrorDeOperacion(true);
			resp.setExcepcionError(e);
		}
		return resp;	
	}
	
}
