package es.indra.druida.desktop.configurator.checks;

import java.io.File;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import es.indra.druida.desktop.configurator.DesktopConfig;
import java.util.ResourceBundle;
import es.indra.druida.desktop.configurator.ResourcesService;
/**
 * The Class ConfigurationChecks.
 */
public class ConfigurationChecks {
	
	/** The check result. */
	public CheckResult checkResult=null;
	
	/**
	 * Instantiates a new configuration checks.
	 */
	public ConfigurationChecks()	{
		checkResult = new CheckResult();
	}
	
	private HttpServletRequest request;
	
	
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	/**
	 * Do desktop config check.
	 * 
	 * @return the check result
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public CheckResult doDesktopConfigCheck() throws Exception{
		ResourceBundle bundle=ResourcesService.getResourceBundle("core",getRequest());
		DesktopConfig desktopConfig = DesktopConfig.getSingleton();
		
		verificaPropiedadExiste(desktopConfig, "root");
		verificaPropiedadExiste(desktopConfig, "userRoot");
		verificaPropiedadExiste(desktopConfig, "appDefRoot");
		verificaPropiedadExiste(desktopConfig, "appRoot");
		verificaPropiedadExiste(desktopConfig, "tmpDir");
		verificaPropiedadExiste(desktopConfig, "scriptDir");
		verificaPropiedadExiste(desktopConfig, "configurationFile");
		verificaPropiedadExiste(desktopConfig, "securityFile");
		verificaPropiedadExiste(desktopConfig, "iconsDir");
		verificaPropiedadExiste(desktopConfig, "helpDir");
		verificaPropiedadExiste(desktopConfig, "skinsDir");
		verificaPropiedadExiste(desktopConfig, "classesDir");		
		
		if (checkResult.hasErrors())
			return checkResult;
		
		verificaRutaExiste(desktopConfig.get("root"), bundle.getString("loginMessages.txt4")+ " \"root\"."); //Related desktop property:
		verificaRutaExiste(desktopConfig.get("root")+"/WEB-INF", bundle.getString("loginMessages.txt4")+ " \"root\".");
		verificaRutaExiste(desktopConfig.get("root")+"/WEB-INF/templates/newUser", bundle.getString("loginMessages.txt4")+ " \"root\".");
		verificaRutaExiste(desktopConfig.get("userRoot"), bundle.getString("loginMessages.txt4")+ " \"userRoot\".");			
		verificaRutaExiste(desktopConfig.get("appDefRoot"), bundle.getString("loginMessages.txt4")+ " \"appDefRoot\".");
		verificaRutaExiste(desktopConfig.get("appRoot"), bundle.getString("loginMessages.txt4")+ " \"appRoot\".");
		verificaRutaExiste(desktopConfig.get("tmpDir"), bundle.getString("loginMessages.txt4")+ " \"tmpDir\".");
		verificaRutaExiste(desktopConfig.get("scriptDir"), bundle.getString("loginMessages.txt4")+ " \"scriptDir\".");
		verificaFicheroExiste(desktopConfig.get("configurationFile"), bundle.getString("loginMessages.txt4")+ " \"configurationFile\".");
		verificaFicheroExiste(desktopConfig.get("securityFile"), bundle.getString("loginMessages.txt4")+ " \"securityFile\".");
		verificaRutaExiste(desktopConfig.get("iconsDir"), bundle.getString("loginMessages.txt4")+ " \"iconsDir\".");
		verificaRutaExiste(desktopConfig.get("helpDir"), bundle.getString("loginMessages.txt4")+ " \"helpDir\".");
		verificaRutaExiste(desktopConfig.get("skinsDir"), bundle.getString("loginMessages.txt4")+ " \"skinsDir\".");
		verificaRutaExiste(desktopConfig.get("classesDir"), bundle.getString("loginMessages.txt4")+ " \"classesDir\".");
		verificaRutaExiste(desktopConfig.get("classesDir")+"/resources", bundle.getString("loginMessages.txt4")+ " \"classesDir\".");

		return checkResult;
	}

	private void verificaPropiedadExiste(DesktopConfig desktopConfig, String nombreProp) {
		if (desktopConfig.get(nombreProp)==null){
			ResourceBundle bundle=ResourcesService.getResourceBundle("core",getRequest());
			checkResult.setErrors(true);
			//checkResult.setCheckMessage( bundle.getString("loginMessages.txt5") +path+ bundle.getString("loginMessages.txt6")+" "+errorHints);//doesn't exist on disk.
			checkResult.setCheckMessage( bundle.getString("loginMessages.txt10") + nombreProp + "'");//doesn't exist on disk.
		}
		
	}

	/**
	 * Verifica ruta existe.
	 * 
	 * @param path
	 *            the path
	 * @param errorHints
	 *            the error hints
	 */
	public void verificaRutaExiste(String path, String errorHints) {
		File dir=new File(path);
		if (!dir.exists()){	
			ResourceBundle bundle=ResourcesService.getResourceBundle("core",getRequest());
			checkResult.setErrors(true);
			checkResult.setCheckMessage( bundle.getString("loginMessages.txt5") +path+ bundle.getString("loginMessages.txt6")+" "+errorHints);//doesn't exist on disk.
		} else if (!dir.isDirectory()){
			ResourceBundle bundle=ResourcesService.getResourceBundle("core",getRequest());
			checkResult.setErrors(true);			
			checkResult.setCheckMessage(bundle.getString("loginMessages.txt5") +path+ bundle.getString("loginMessages.txt7") +" "+errorHints);//it's not a directory.
		}
	}
	
	/**
	 * Verifica fichero existe.
	 * 
	 * @param path
	 *            the path
	 * @param errorHints
	 *            the error hints
	 */
	public void verificaFicheroExiste (String path, String errorHints) {
		File fich=new File(path);
		if (!fich.exists()){	
			ResourceBundle bundle=ResourcesService.getResourceBundle("core",getRequest());
			checkResult.setErrors(true);
			checkResult.setCheckMessage(bundle.getString("loginMessages.txt8") +path+ bundle.getString("loginMessages.txt6")+" "+errorHints);// doesn't exist on disk. 
		} else if (fich.isDirectory()){
			ResourceBundle bundle=ResourcesService.getResourceBundle("core",getRequest());
			checkResult.setErrors(true);			
			checkResult.setCheckMessage( bundle.getString("loginMessages.txt5") +path+ bundle.getString("loginMessages.txt9") +" "+errorHints);// it's a directory, not a file.
		}
	}	
}
