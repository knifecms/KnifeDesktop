package es.indra.druida.desktop.configurator.checks;

import java.util.ArrayList;

/**
 * The Class CheckResult.
 */
public class CheckResult {
	
	/** The errors. */
	private boolean errors;
	
	/** The check messages. */
	private ArrayList<String> checkMessages;
	
	/**
	 * Instantiates a new check result.
	 */
	public CheckResult() {
		super();
		checkMessages=new ArrayList<String>();
	}
	
	/**
	 * Gets the check messages.
	 * 
	 * @return the check messages
	 */
	public String[] getCheckMessages() {
		return (String[]) checkMessages.toArray(new String[checkMessages.size()]);
	}
	
	/**
	 * Sets the check message.
	 * 
	 * @param checkMessage
	 *            the new check message
	 */
	public void setCheckMessage(String checkMessage) {
		this.checkMessages.add(checkMessage);
	}
	
	/**
	 * Checks for errors.
	 * 
	 * @return true, if successful
	 */
	public boolean hasErrors() {
		return errors;
	}
	
	/**
	 * Sets the errors.
	 * 
	 * @param errors
	 *            the new errors
	 */
	public void setErrors(boolean errors) {
		this.errors = errors;
	}
}
