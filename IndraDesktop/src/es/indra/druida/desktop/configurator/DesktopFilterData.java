package es.indra.druida.desktop.configurator;

/**
 * The Class DesktopFilterData.
 */
public class DesktopFilterData {
	
	/** The config class. */
	private String configClass = "es.indra.druida.desktop.configurator.DefaultConfigurationClass";
	
	/** The app loader. */
	private String appLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.ApplicationLoader";
	
	/** The icon loader. */
	private String iconLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.IconLoader";
	
	/** The window loader. */
	private String windowLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.WindowLoader";	
	
	/** The skin loader. */
	private String skinLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.SkinLoader";
	
	/** The menu loader. */
	private String menuLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.MenuLoader";
	
	/** The app group loader. */
	private String appGroupLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.AppGroupLoader";
	
	/** The custom skin parameters loader. */
	private String customSkinParametersLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.CustomSkinParametersLoader";
	
	/** The configuration loader. */
	private String configurationLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.ConfigurationLoader";
	
	/** The custom configuration loader. */
	private String customConfigurationLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.CustomConfigurationLoader";
	
	private String securityLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.CustomConfigurationLoader";
	
	/**
	 * Gets the app group loader.
	 * 
	 * @return the app group loader
	 */
	public String getAppGroupLoader() {
		return appGroupLoader;
	}
	
	/**
	 * Sets the app group loader.
	 * 
	 * @param appGroupLoader
	 *            the new app group loader
	 */
	public void setAppGroupLoader(String appGroupLoader) {
		this.appGroupLoader = appGroupLoader;
	}
	
	/**
	 * Gets the app loader.
	 * 
	 * @return the app loader
	 */
	public String getAppLoader() {
		return appLoader;
	}
	
	/**
	 * Sets the app loader.
	 * 
	 * @param appLoader
	 *            the new app loader
	 */
	public void setAppLoader(String appLoader) {
		this.appLoader = appLoader;
	}
	
	/**
	 * Gets the config class.
	 * 
	 * @return the config class
	 */
	public String getConfigClass() {
		return configClass;
	}
	
	/**
	 * Sets the config class.
	 * 
	 * @param configClass
	 *            the new config class
	 */
	public void setConfigClass(String configClass) {
		this.configClass = configClass;
	}
	
	/**
	 * Gets the configuration loader.
	 * 
	 * @return the configuration loader
	 */
	public String getConfigurationLoader() {
		return configurationLoader;
	}
	
	/**
	 * Sets the configuration loader.
	 * 
	 * @param configurationLoader
	 *            the new configuration loader
	 */
	public void setConfigurationLoader(String configurationLoader) {
		this.configurationLoader = configurationLoader;
	}
	
	/**
	 * Gets the custom configuration loader.
	 * 
	 * @return the custom configuration loader
	 */
	public String getCustomConfigurationLoader() {
		return customConfigurationLoader;
	}
	
	/**
	 * Sets the custom configuration loader.
	 * 
	 * @param customConfigurationLoader
	 *            the new custom configuration loader
	 */
	public void setCustomConfigurationLoader(String customConfigurationLoader) {
		this.customConfigurationLoader = customConfigurationLoader;
	}
	
	/**
	 * Gets the custom skin parameters loader.
	 * 
	 * @return the custom skin parameters loader
	 */
	public String getCustomSkinParametersLoader() {
		return customSkinParametersLoader;
	}
	
	/**
	 * Sets the custom skin parameters loader.
	 * 
	 * @param customSkinParametersLoader
	 *            the new custom skin parameters loader
	 */
	public void setCustomSkinParametersLoader(String customSkinParametersLoader) {
		this.customSkinParametersLoader = customSkinParametersLoader;
	}
	
	/**
	 * Gets the icon loader.
	 * 
	 * @return the icon loader
	 */
	public String getIconLoader() {
		return iconLoader;
	}
	
	/**
	 * Sets the icon loader.
	 * 
	 * @param iconLoader
	 *            the new icon loader
	 */
	public void setIconLoader(String iconLoader) {
		this.iconLoader = iconLoader;
	}
	
	/**
	 * Gets the menu loader.
	 * 
	 * @return the menu loader
	 */
	public String getMenuLoader() {
		return menuLoader;
	}
	
	/**
	 * Sets the menu loader.
	 * 
	 * @param menuLoader
	 *            the new menu loader
	 */
	public void setMenuLoader(String menuLoader) {
		this.menuLoader = menuLoader;
	}
	
	/**
	 * Gets the skin loader.
	 * 
	 * @return the skin loader
	 */
	public String getSkinLoader() {
		return skinLoader;
	}
	
	/**
	 * Sets the skin loader.
	 * 
	 * @param skinLoader
	 *            the new skin loader
	 */
	public void setSkinLoader(String skinLoader) {
		this.skinLoader = skinLoader;
	}
	
	/**
	 * Gets the window loader.
	 * 
	 * @return the window loader
	 */
	public String getWindowLoader() {
		return windowLoader;
	}
	
	/**
	 * Sets the window loader.
	 * 
	 * @param windowLoader
	 *            the new window loader
	 */
	public void setWindowLoader(String windowLoader) {
		this.windowLoader = windowLoader;
	}

	public String getSecurityLoader() {
		return securityLoader;
	}

	public void setSecurityLoader(String securityLoader) {
		this.securityLoader = securityLoader;
	}
}
