package es.indra.druida.desktop.configurator;

//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.Properties;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader;
import es.indra.druida.desktop.configurator.DataLoaders.AbstractSecurityLoader;
import es.indra.druida.desktop.configurator.beans.AppGroup;
import es.indra.druida.desktop.configurator.beans.Application;
import es.indra.druida.desktop.configurator.beans.Configuration;
import es.indra.druida.desktop.configurator.beans.Engine;
import es.indra.druida.desktop.configurator.beans.Parameter;
import es.indra.druida.desktop.configurator.beans.Skin;

//Ejemplo de uso de esta clase: DesktopConfig.getSingleton().get("userRoot")

/**
 * The Class DesktopConfig.
 */
public class DesktopConfig {
	
	/** The singleton. */
	static private DesktopConfig singleton = null;
	/*
	static private String configClass = "es.indra.druida.desktop.configurator.DefaultConfigurationClass";
	static private String appLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.ApplicationLoader";
	static private String iconLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.IconLoader";
	static private String windowLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.WindowLoader";	
	static private String skinLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.SkinLoader";
	static private String menuLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.MenuLoader";
	static private String appGroupLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.AppGroupLoader";
	static private String customSkinParametersLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.CustomSkinParametersLoader";
	static private String configurationLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.ConfigurationLoader";
	static private String customConfigurationLoader = "es.indra.druida.desktop.configurator.DataLoaders.filesystem.CustomConfigurationLoader";
	*/
	/** The desktop filter data. */
	static private DesktopFilterData desktopFilterData=new DesktopFilterData();
	
	/** The propiedades. */
	static private Properties propiedades;//Est� bien puesto esto aqu�? XXX
	
	/** The skins. */
	private Skin[] skins;
	
	/** The applications. */
	private Application[] applications;
	
	/** The app groups. */
	private AppGroup[] appGroups;
	
	/** The configuration. */
	private Configuration configuration;
	
	/**
	 * Gets the singleton.
	 * 
	 * @return the singleton
	 * 
	 * @throws Exception
	 *             the exception
	 */
	static public DesktopConfig getSingleton() throws Exception {

		if (singleton == null) {
			return getSingleton(desktopFilterData);
		}
		return singleton;
	}

/**
 * Gets the singleton.
 * 
 * @param _desktopFilterData
 *            the _desktop filter data
 * 
 * @return the singleton
 * 
 * @throws Exception
 *             the exception
 */
static public DesktopConfig getSingleton( DesktopFilterData _desktopFilterData) throws Exception {
        if (singleton == null && _desktopFilterData.getConfigClass()!=null) {
        	desktopFilterData=_desktopFilterData;
        	singleton = new DesktopConfig(desktopFilterData);
        }else if (singleton == null){
        	singleton = new DesktopConfig(desktopFilterData);
        }
    	return singleton;
    }
    
    /**
	 * Gets the single instance of DesktopConfig.
	 * 
	 * @return single instance of DesktopConfig
	 * 
	 * @throws Exception
	 *             the exception
	 */
    static public DesktopConfig getInstance() throws Exception {
        return getSingleton();
    } 

	//private DesktopConfig(String _configClass, String _appLoader, String _iconLoader, String _windowLoader, String _skinLoader, String _menuLoader, String _appGroupLoader, String _customSkinParametersLoader, String _configurationLoader, String _customConfigurationLoader) {
    /**
	 * Instantiates a new desktop config.
	 * 
	 * @param _desktopFilterData
	 *            the _desktop filter data
	 */
	private DesktopConfig(DesktopFilterData _desktopFilterData){
		propiedades=new Properties();
		//configClass = cfgClass;
		desktopFilterData=_desktopFilterData;
		try {
			InputStream is = getClass().getResourceAsStream("/DrDesktop.properties");
			propiedades.load(is);
			Class[] classesSkins = new Class[1];
			classesSkins[0]=String.class;
			Object[] objectsSkins = new Object[1];
			objectsSkins[0]=this.get("appDefRoot");
			skins = (Skin[])((AbstractLoader) getLoader(desktopFilterData.getSkinLoader(), classesSkins, objectsSkins)).getObjects();
			this.reloadApplications(); //appGroups y applications
			objectsSkins[0]=this.get("configurationFile");
			configuration = ((Configuration[])((AbstractLoader) getLoader(desktopFilterData.getConfigurationLoader(), classesSkins, objectsSkins)).getObjects())[0];			
		} catch (Exception e){
			Logger.getLogger(DesktopConfig.class.getName()).severe("Error inicializando propiedades: "+e.toString());
		}
	}
	
	public void reloadApplications() throws Exception {
		Class[] classesSkins = new Class[1];
		classesSkins[0]=String.class;
		Object[] objectsSkins = new Object[1];
		objectsSkins[0]=propiedades.get("appDefRoot"); 
		appGroups = (AppGroup[])((AbstractLoader) getLoader(desktopFilterData.getAppGroupLoader(), classesSkins, objectsSkins)).getObjects();
		applications = (Application[])((AbstractLoader) getLoader(desktopFilterData.getAppLoader(), classesSkins, objectsSkins)).getObjects();
	}

	/**
	 * Gets the custom configuration loader.
	 * 
	 * @param req
	 *            the req
	 * @param res
	 *            the res
	 * 
	 * @return the custom configuration loader
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public static AbstractLoader getCustomConfigurationLoader (HttpServletRequest req, HttpServletResponse res) throws Exception {
		String userRoot=(String)propiedades.get("userRoot");
		DesktopUserConfig userConfig = (DesktopUserConfig) req.getSession().getAttribute("userConfig");
		String rutaCompletaFich=userRoot+"/"+userConfig.getId()+"/drdesktop-user-config.xml";
		Class loaderClass = Class.forName( desktopFilterData.getCustomConfigurationLoader());
		Class[] classesArgs = {String.class};
		Object[] args = {rutaCompletaFich};
		Constructor loaderConstructor = loaderClass.getConstructor(classesArgs);
		AbstractLoader loader=(AbstractLoader) loaderConstructor.newInstance( args );
		return loader;
	}
	
	public static AbstractLoader getAppGroupLoader(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Class loaderClass = Class.forName( desktopFilterData.getAppGroupLoader());
		Class[] classesArgs = {String.class};
		Object[] args = {(String)propiedades.get("appDefRoot")};		
		Constructor loaderConstructor = loaderClass.getConstructor(classesArgs);
		AbstractLoader loader=(AbstractLoader) loaderConstructor.newInstance( args );
		return loader;		
	}
	
	public static AbstractLoader getAppLoader(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Class loaderClass = Class.forName( desktopFilterData.getAppLoader());
		Class[] classesArgs = {String.class};
		Object[] args = {(String)propiedades.get("appDefRoot")};		
		Constructor loaderConstructor = loaderClass.getConstructor(classesArgs);
		AbstractLoader loader=(AbstractLoader) loaderConstructor.newInstance( args );
		return loader;		
	}	
	

	/**
	 * Gets the custom skin parameters loader.
	 * 
	 * @param req
	 *            the req
	 * @param res
	 *            the res
	 * 
	 * @return the custom skin parameters loader
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public static AbstractLoader getCustomSkinParametersLoader(HttpServletRequest req, HttpServletResponse res) throws Exception {
    	//customSkinParametersLoader
		String userRoot=(String)propiedades.get("userRoot");
		DesktopUserConfig userConfig = (DesktopUserConfig) req.getSession().getAttribute("userConfig");
		String rutaCompletaFich=userRoot+"/"+userConfig.getId()+"/drdesktop-user-skins.xml";
		Class loaderClass = Class.forName( desktopFilterData.getCustomSkinParametersLoader());
		Class[] classesArgs = {String.class};
		Object[] args = {rutaCompletaFich};		
		Constructor loaderConstructor = loaderClass.getConstructor(classesArgs);
		AbstractLoader loader=(AbstractLoader) loaderConstructor.newInstance( args );
		return loader;
    }
    
    /**
	 * Gets the menu loader.
	 * 
	 * @param req
	 *            the req
	 * @param res
	 *            the res
	 * 
	 * @return the menu loader
	 * 
	 * @throws Exception
	 *             the exception
	 */
    public static AbstractLoader getMenuLoader(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String userRoot=(String)propiedades.get("userRoot");
		DesktopUserConfig userConfig = (DesktopUserConfig) req.getSession().getAttribute("userConfig");
		String rutaCompletaXml=userRoot+"/"+userConfig.getId()+"/Menu/programs.xml";
		Class loaderClass = Class.forName( desktopFilterData.getMenuLoader());
		Class[] classesArgs = {String.class};
		Object[] args = {rutaCompletaXml};		
		Constructor loaderConstructor = loaderClass.getConstructor(classesArgs);
		AbstractLoader loader=(AbstractLoader) loaderConstructor.newInstance( args );
		return loader;
    }
    
    
    /**
	 * Gets the security loader.
	 * 
	 * @param req
	 *            the req
	 * @param res
	 *            the res
	 * 
	 * @return the security loader
	 * 
	 * @throws Exception
	 *             the exception
	 */
    public static AbstractSecurityLoader getSecurityLoader(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String root=(String)propiedades.get("root");
		//DesktopUserConfig userConfig = (DesktopUserConfig) req.getSession().getAttribute("userConfig");
		String rutaCompletaXml=root+"/WEB-INF/drdesktop-security.xml";
		Class loaderClass = Class.forName( desktopFilterData.getSecurityLoader());
		Class[] classesArgs = {String.class};
		Object[] args = {rutaCompletaXml};		
		Constructor loaderConstructor = loaderClass.getConstructor(classesArgs);
		AbstractSecurityLoader loader=(AbstractSecurityLoader) loaderConstructor.newInstance( args );
		return loader;
    }    
    
    /**
	 * Gets the icon loader.
	 * 
	 * @param req
	 *            the req
	 * @param res
	 *            the res
	 * 
	 * @return the icon loader
	 * 
	 * @throws Exception
	 *             the exception
	 */
    public static AbstractLoader getIconLoader(HttpServletRequest req, HttpServletResponse res) throws Exception { //XXX debe ser esta static? si no, me da un error el jsp al invocar ...
		String userRoot=(String)propiedades.get("userRoot");
		DesktopUserConfig userConfig = (DesktopUserConfig) req.getSession().getAttribute("userConfig"); //XXX �Esta es la mejor forma de tomar el obj del usuario?
		String rutaCompletaXmlIconos=userRoot+"/"+userConfig.getId()+"/Desktop/icons.xml";
		//Busco la clase del iconLoader y construyo un objeto 
		Class loaderClass = Class.forName( desktopFilterData.getIconLoader() );//XXX le paso la ruta o el req y res y dejo que la calcule? no se que es mas gen�rico
		Class[] classesArgs = {String.class};
		Object[] args = {rutaCompletaXmlIconos};		
		Constructor loaderConstructor = loaderClass.getConstructor(classesArgs);
		AbstractLoader loader=(AbstractLoader) loaderConstructor.newInstance( args );
		return loader;
    }
    
    /**
	 * Gets the window loader.
	 * 
	 * @param req
	 *            the req
	 * @param res
	 *            the res
	 * 
	 * @return the window loader
	 * 
	 * @throws Exception
	 *             the exception
	 */
    public static AbstractLoader getWindowLoader(HttpServletRequest req, HttpServletResponse res) throws Exception { 
		String userRoot=(String)propiedades.get("userRoot");
		DesktopUserConfig userConfig = (DesktopUserConfig) req.getSession().getAttribute("userConfig"); 
		String rutaCompletaXml=userRoot+"/"+userConfig.getId()+"/Desktop/windows.xml";
		Class loaderClass = Class.forName( desktopFilterData.getWindowLoader() );
		Class[] classesArgs = {String.class};
		Object[] args = {rutaCompletaXml};		
		Constructor loaderConstructor = loaderClass.getConstructor(classesArgs);
		AbstractLoader loader=(AbstractLoader) loaderConstructor.newInstance( args );
		return loader;
    }    
    
    /**
	 * Invoke desktop configuration class.
	 * 
	 * @param req
	 *            the req
	 * @param res
	 *            the res
	 * 
	 * @return the abstract desktop configuration class
	 * 
	 * @throws Exception
	 *             the exception
	 */
    public static AbstractDesktopConfigurationClass invokeDesktopConfigurationClass(HttpServletRequest req, HttpServletResponse res) throws Exception{
        Object objNegocio = null;

        StringBuffer objNegocioClass = new StringBuffer();
        
        objNegocioClass = objNegocioClass.append(desktopFilterData.getConfigClass());

		// carga la clase
		Class objNegClass = null;

		objNegClass = Class.forName( objNegocioClass.toString() );
        // Crear los argumentos del constructor
        Class[] tiposParam = new Class[2];
        tiposParam[0]=javax.servlet.http.HttpServletRequest.class;
        tiposParam[1]=javax.servlet.http.HttpServletResponse.class;

        //Obtengo el constructor
        Constructor constructor = objNegClass.getConstructor(tiposParam);
  
        //Argumentos del constructor
        Object[] args = new Object[2];
        args[0]=req;
        args[1]=res;
  
        // construyo el objeto
        objNegocio = constructor.newInstance( args );
                      
        return ((AbstractDesktopConfigurationClass) objNegocio);
    }
    
	/**
	 * Gets the loader.
	 * 
	 * @param strLoader
	 *            the str loader
	 * @param classes
	 *            the classes
	 * @param params
	 *            the params
	 * 
	 * @return the loader
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private Object getLoader(String strLoader, Class[] classes, Object[] params) throws Exception{
		//System.out.println("getLoader("+strLoader+", ..., ...)");
		Object objNegocio = null;
        StringBuffer objNegocioClass = new StringBuffer();
        objNegocioClass = objNegocioClass.append(strLoader);
		Class objNegClass = null;
		objNegClass = Class.forName( objNegocioClass.toString() );
        Class[] tiposParam = classes;
        Constructor constructor = objNegClass.getConstructor(tiposParam);
        Object[] args = params;
        objNegocio = constructor.newInstance( args );
                      
        return objNegocio;
	}


	/**
	 * Gets the.
	 * 
	 * @param nombreProp
	 *            the nombre prop
	 * 
	 * @return the string
	 */
	public String get(String nombreProp){
		return propiedades.getProperty(nombreProp);
	}
	
	/**
	 * Gets the skins.
	 * 
	 * @return the skins
	 */
	public Skin[] getSkins(){
		return skins;
	}	
	
	/**
	 * Sets the.
	 * 
	 * @param nombreProp
	 *            the nombre prop
	 * @param valorProp
	 *            the valor prop
	 * 
	 * @return the object
	 */
	public Object set(String nombreProp,String valorProp){
		return propiedades.setProperty(nombreProp,valorProp);
	}	

	/*
	public static String getConfigClass() {
		return configClass;
	}

	public static void setConfigClass(String cfgClass) {
		DesktopConfig.configClass = cfgClass;
		configClass=cfgClass;    	
		singleton = new DesktopConfig(cfgClass, appLoader, iconLoader, windowLoader, skinLoader, menuLoader, appGroupLoader, customSkinParametersLoader, configurationLoader, customConfigurationLoader);
	}


	public static String getAppLoader() {
		return appLoader;
	}


	public static void setAppLoader(String appLoader) {
		DesktopConfig.appLoader = appLoader;
	}


	public static String getIconLoader() {
		return iconLoader;
	}


	public static void setIconLoader(String iconLoader) {
		DesktopConfig.iconLoader = iconLoader;
	}


	public static String getSkinLoader() {
		return skinLoader;
	}


	public static void setSkinLoader(String skinLoader) {
		DesktopConfig.skinLoader = skinLoader;
	}


	public static String getMenuLoader() {
		return menuLoader;
	}


	public static void setMenuLoader(String menuLoader) {
		DesktopConfig.menuLoader = menuLoader;
	}
	 */

	/**
	 * Gets the app groups.
	 * 
	 * @return the app groups
	 */
	public AppGroup[] getAppGroups() {
		return appGroups;
	}


	/**
	 * Sets the app groups.
	 * 
	 * @param appGroups
	 *            the new app groups
	 */
	public void setAppGroups(AppGroup[] appGroups) {
		this.appGroups = appGroups;
	}


	/**
	 * Gets the applications.
	 * 
	 * @return the applications
	 */
	public Application[] getApplications() {
		return applications;
	}


	/**
	 * Sets the applications.
	 * 
	 * @param applications
	 *            the new applications
	 */
	public void setApplications(Application[] applications) {
		this.applications = applications;
	}

	/*
	public static String getWindowLoader() {
		return windowLoader;
	}


	public static void setWindowLoader(String windowLoader) {
		DesktopConfig.windowLoader = windowLoader;
	}*/


	/**
	 * Gets the configuration.
	 * 
	 * @return the configuration
	 */
	public Configuration getConfiguration() {
		return configuration;
	}
	
	public String getEngineConfigParam(String engineName, String parameterName) {
		Engine[] engines = configuration.getEngines();
		for (int i=0; i<engines.length; i++){
			Engine unMotor=engines[i];
			if (unMotor.getId().equals(engineName)){
				Parameter[] configParams=unMotor.getConfigParameters();
				for (int j=0; j<configParams.length; j++){
					Parameter unParam = configParams[j];
					if (unParam.getName().equals(parameterName))
						return unParam.getValue();
				}
			}
		}	
		return null;
	}	


	/**
	 * Sets the configuration.
	 * 
	 * @param configuration
	 *            the new configuration
	 */
	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	/**
	 * Gets the desktop filter data.
	 * 
	 * @return the desktop filter data
	 */
	public static DesktopFilterData getDesktopFilterData() {
		return desktopFilterData;
	}

	/**
	 * Sets the desktop filter data.
	 * 
	 * @param desktopFilterData
	 *            the new desktop filter data
	 */
	public static void setDesktopFilterData(DesktopFilterData desktopFilterData) {
		DesktopConfig.desktopFilterData = desktopFilterData;
	}
}


