package es.indra.druida.desktop.configurator;

import es.indra.druida.desktop.configurator.beans.Application;

/**
 * The Class DesktopUserConfig.
 */
public class DesktopUserConfig {
	
	/** The id. */
	private String id;   
	
	/** The name. */
	private String name;
	
	/** The surname. */
	private String surname;
	
	/** The mother surname. */
	private String motherSurname;
	
	/** The file system root. */
	private String fileSystemRoot;
	
	/** The roles. */
	private String[] roles;
	
	/** The applications. */
	private Application[] applications;
	
	private String email;
	
	private String city;
	
	//private Application[] applicationsI18N;		
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the file system root.
	 * 
	 * @return the file system root
	 */
	public String getFileSystemRoot() {
		return fileSystemRoot;
	}
	
	/**
	 * Sets the file system root.
	 * 
	 * @param fileSystemRoot
	 *            the new file system root
	 */
	public void setFileSystemRoot(String fileSystemRoot) {
		this.fileSystemRoot = fileSystemRoot;
	}
	
	/**
	 * Gets the roles.
	 * 
	 * @return the roles
	 */
	public String[] getRoles() {
		return roles;
	}
	
	/**
	 * Sets the roles.
	 * 
	 * @param rolesArr
	 *            the new roles
	 */
	public void setRoles(String[] rolesArr) {
		this.roles = rolesArr;
	}
	
	/**
	 * Gets the applications.
	 * 
	 * @return the applications
	 */
	public Application[] getApplications() {
		return applications;
	}
	
	/**
	 * Sets the applications.
	 * 
	 * @param applications
	 *            the new applications
	 */
	public void setApplications(Application[] applications) {
		this.applications = applications;
	}
	
	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * Gets the mother surname.
	 * 
	 * @return the mother surname
	 */
	public String getMotherSurname() {
		return motherSurname;
	}
	
	/**
	 * Sets the mother surname.
	 * 
	 * @param motherSurname
	 *            the new mother surname
	 */
	public void setMotherSurname(String motherSurname) {
		this.motherSurname = motherSurname;
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the surname.
	 * 
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}
	
	/**
	 * Sets the surname.
	 * 
	 * @param surname
	 *            the new surname
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}
	/*
	public Application[] getApplicationsI18N() {
		return applicationsI18N;
	}

	public void setApplicationsI18N(Application[] applicationsI18N) {
		this.applicationsI18N = applicationsI18N;
	}*/	
}
