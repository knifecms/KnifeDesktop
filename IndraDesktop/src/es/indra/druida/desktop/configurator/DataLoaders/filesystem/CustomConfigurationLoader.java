package es.indra.druida.desktop.configurator.DataLoaders.filesystem;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader;
import es.indra.druida.desktop.configurator.beans.ConfigProperty;
import es.indra.druida.desktop.utils.DesktopWriter;
import es.indra.druida.desktop.utils.DesktopXmlUtils;

/**
 * The Class CustomConfigurationLoader.
 */
public class CustomConfigurationLoader extends AbstractLoader{
	
	/** The xml file name. */
	private String xmlFileName = null;
	
	/**
	 * Instantiates a new custom configuration loader.
	 * 
	 * @param xmlFileName
	 *            the xml file name
	 */
	public CustomConfigurationLoader(String xmlFileName){
		super();
		this.xmlFileName = xmlFileName;
	}	
	
	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#getObjects()
	 */
	public Object[] getObjects() throws Exception { //usa el nombre del fichero XML para leerlo y retornar una lista de beans
		File xmlFile=new File(xmlFileName);
		if (xmlFile.exists())	{
			Document doc = DesktopXmlUtils.getDocumentFromFile(xmlFile);
			NodeList atribs = doc.getDocumentElement().getChildNodes();
			ArrayList <ConfigProperty> propsRet=new ArrayList<ConfigProperty>();	
			for ( int i = 0 ; i < atribs.getLength(); i++) {
				Node node=atribs.item(i);
				if(node.getNodeType()==Node.ELEMENT_NODE && node.getFirstChild()!=null && node.getFirstChild().getNodeValue()!=null)	{
					Element atrib = (Element) node;
					String nombre= atrib.getNodeName();
					String clase=null;
					if (atrib.hasAttribute("clase"))
						clase=atrib.getAttribute("clase");
					String valor=atrib.getFirstChild().getNodeValue();
					propsRet.add(new ConfigProperty(nombre,clase,valor));
				}
			}
			return propsRet.toArray(new ConfigProperty[0]);
		}else{
			System.out.println("ERROR: CustomConfigurationLoader Cant find "+xmlFileName);
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#setObjects(java.lang.Object[])
	 */
	public void setObjects(Object[] objects) throws Exception {//Usando un array de beans (y el nombre del fichero) escribe un XML
		ConfigProperty[] props=(ConfigProperty[])objects;
		Document doc=DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		Element root=doc.createElement("drdesktop-user-config");		
		doc.appendChild(root);
		for (int r=0;r<props.length; r++){	
			ConfigProperty prop=props[r];
			if (prop==null)
				System.out.println("NULL1");
			else if (prop.getName()==null)
				System.out.println("NULL2");
			Element elemento=doc.createElement(prop.getName());			
			if (prop.getPropClass()!=null && !prop.getPropClass().equals("String"))	
				elemento.setAttribute("clase", prop.getPropClass());
			if (prop.getValue()!=null)
				elemento.appendChild(doc.createTextNode(prop.getValue()));
			root.appendChild(elemento);	
		}	
		DesktopXmlUtils.escribeDOMEnDisco(doc, xmlFileName);
	}

	@Override
	public void addObjects(Object[] objects) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
}
