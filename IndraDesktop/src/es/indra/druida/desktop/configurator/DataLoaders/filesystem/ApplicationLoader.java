package es.indra.druida.desktop.configurator.DataLoaders.filesystem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import es.indra.druida.desktop.configurator.DesktopConfig;
import es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader;
import es.indra.druida.desktop.configurator.beans.AppGroup;
import es.indra.druida.desktop.configurator.beans.AppParameter;
import es.indra.druida.desktop.configurator.beans.Application;
import es.indra.druida.desktop.configurator.beans.Daemon;
import es.indra.druida.desktop.utils.DesktopWriter;
import es.indra.druida.desktop.utils.DesktopXmlUtils;

/**
 * The Class ApplicationLoader.
 */
public class ApplicationLoader extends AbstractLoader{
	
	/** The applications. */
	private Application[] applications;
	
	/** The xml apps files. */
	private String xmlAppsFiles = null;
		
	/**
	 * Instantiates a new application loader.
	 * 
	 * @param xmlAppsFiles
	 *            the xml apps files
	 */
	public ApplicationLoader(String xmlAppsFiles){
		super();
		//System.out.println("Estoy en constructor ApplicationLoader "+xmlAppsFiles);
		this.xmlAppsFiles = xmlAppsFiles;
	}
	
	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#getObjects()
	 */
	public Object[] getObjects() throws Exception {
		File elDir=new File(xmlAppsFiles);
		List arr = new ArrayList();
		int numFich=0;
		
		if (!elDir.exists() || !elDir.canRead()){
			return new Application[0];
		}

		//boolean primeroSinComillas = true;
		String[]losFich=elDir.list();
		int indice=0; //indice del grupo
		for (int i=0;i<losFich.length;i++){
			int ii=losFich[i].lastIndexOf(".");
			String extension=((ii!=-1)? losFich[i].substring(ii+1).toUpperCase() :"");
			String xmlFileApp = xmlAppsFiles + "/" + losFich[i];
			File ficheroXml=new File(xmlFileApp);
			//System.out.println(ficheroXml.getName());
			if (extension.equals("XML") && ficheroXml.exists()){
				try	{
					Document doc = DesktopXmlUtils.getDocumentFromFile(ficheroXml);
					String fileNameNoExt=ficheroXml.getName();	 //Por ejemplo "drConsoleTest.xml"
					int ind=fileNameNoExt.lastIndexOf(".");

					if (ind!=-1)	
						fileNameNoExt=fileNameNoExt.substring(0,ind); //Por ejemplo "drConsoleTest" 				

					//arr.add(new Application(fileNameNoExt,doc.getDocumentElement().getAttribute("description")));
					//System.out.println("Indice: "+i);
					arr=addApplicationsFromFile(arr, doc, indice, fileNameNoExt);
					arr=addWidgetsFromFile(arr, doc, indice, fileNameNoExt);
					//_appJS.append(DesktopAplicaciones.dameHtmlInicializarAplicacionesDesdeXml(doc, indice, primeroSinComillas));
					//primeroSinComillas=false;
					indice++;				
				} catch(Exception e){
					Logger.getLogger(DesktopConfig.class.getName()).severe("Error loading Applications ('"+ficheroXml.getName()+"'): \n  "+e.toString());
				}
			}
		}

		//System.out.println("Applications: "+arr.toString());
		numFich=arr.size();
		applications = new Application[numFich];
		for (int i=0;i<numFich;i++){
			applications[i]=(Application)arr.get(i);
		}
		return applications;
	}
	
	/**
	 * Adds the widgets from file.
	 * 
	 * @param arr
	 *            the arr
	 * @param doc
	 *            the doc
	 * @param indice
	 *            the indice
	 * @param fileNameNoExt 
	 * 
	 * @return the list
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private List addWidgetsFromFile(List arr, Document doc, int indice, String appGrpId) throws Exception{
		//System.out.println("addWidgetsFromFile");
		List<Application> miArr = arr;
		NodeList _appWidget = doc.getElementsByTagName("WIDGET");
		int numWidgets = _appWidget.getLength();
		
		for (int i=0;i<numWidgets;i++){
			Element _elAct = (Element) _appWidget.item(i);
			Application _wdgtAct = new Application();
			_wdgtAct.setWidget(true);
			miArr.add(fillAppObject(_wdgtAct, _elAct, indice, appGrpId));			
		}
		//System.out.println("addWidgetsFromFile="+arr);
		return miArr;
	}
	
	/**
	 * Adds the daemons.
	 * 
	 * @param act
	 *            the act
	 * 
	 * @return the daemon[]
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private Daemon[] addDaemons(Element act) throws Exception{
		//System.out.println("addDaemons");
		Daemon[] myDaemons = null;
		NodeList nl = act.getElementsByTagName("DAEMON");
		int numDae = nl.getLength();
		//System.out.println("Demonios: "+numDae);
		if (numDae!=0)
			myDaemons = new Daemon[numDae];
		
		for (int i=0 ; i<numDae ; i++){
			Element _elDae=(Element)nl.item(i);
			//System.out.println("miliseconds: "+_elDae.getAttribute("milliseconds"));
			//System.out.println("script: "+_elDae.getAttribute("script"));
			Daemon daemonAct = new Daemon(Integer.parseInt(_elDae.getAttribute("milliseconds")), _elDae.getAttribute("script"));
			myDaemons[i]=daemonAct;
		}
		
		return myDaemons;
	}
	
	/**
	 * Adds the parameters.
	 * 
	 * @param act
	 *            the act
	 * 
	 * @return the app parameter[]
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private AppParameter[] addParameters(Element act) throws Exception{
		//System.out.println("addParameters");
		AppParameter[] myParams = null;
		NodeList nl = act.getElementsByTagName("PARAMETER");
		int numPar = nl.getLength();
		if (numPar!=0)
			myParams = new AppParameter[numPar];
		
		for (int i=0 ; i<numPar ; i++){
			Element _elPar=(Element)nl.item(i);
			//AppParameter parAct = new AppParameter(_elPar.getAttribute("name"), DesktopWriter.generaCadenaJavascript(_elPar.getFirstChild().getNodeValue()));
			AppParameter parAct = new AppParameter(_elPar.getAttribute("name"), _elPar.getFirstChild().getNodeValue() );
			myParams[i]=parAct;
		}
		
		return myParams;
	}
	
	/**
	 * Fill app object.
	 * 
	 * @param _wdgtAct
	 *            the _wdgt act
	 * @param _elAct
	 *            the _el act
	 * @param indice
	 *            the indice
	 * @param fileNameNoExt 
	 * 
	 * @return the application
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private Application fillAppObject(Application _wdgtAct, Element _elAct, int indice, String appGrpId) throws Exception{
		//System.out.println("1");
		_wdgtAct.setId(_elAct.getAttribute("id"));
		//System.out.println("2");
		//_wdgtAct.setWidget(false);
		//System.out.println("3");
		_wdgtAct.setDescription(_elAct.getAttribute("description"));
		//System.out.println("4");
		_wdgtAct.setGroup(Integer.toString(indice));
		_wdgtAct.setGroupId(appGrpId);
		//System.out.println("5");
		//_wdgtAct.setHeight(_elAct.getAttribute("height").length()>0?Integer.parseInt(_elAct.getAttribute("height")):100);
		_wdgtAct.setHeight(_elAct.getAttribute("height"));
		//System.out.println("6");
		_wdgtAct.setIcon(_elAct.getAttribute("icon"));
		//System.out.println("7");
		_wdgtAct.setIconText(_elAct.getAttribute("iconText"));
		//System.out.println("8");
		_wdgtAct.setIgnoreMargins(Boolean.parseBoolean(_elAct.getAttribute("ignoreMargins")));
		//System.out.println("9");
		_wdgtAct.setInnerHeight(_elAct.getAttribute("innerHeight"));
		//System.out.println("10");
		_wdgtAct.setInnerWidth(_elAct.getAttribute("innerWidth"));
		//System.out.println("11");
		_wdgtAct.setJs(_elAct.getAttribute("js"));
		_wdgtAct.setEngine(_elAct.getAttribute("engine"));
		_wdgtAct.setHelpUrl(_elAct.getAttribute("helpUrl"));
		_wdgtAct.setRelativeHelpUrl(_elAct.getAttribute("relativeHelpUrl"));		
		//System.out.println("12");
		_wdgtAct.setNoResize(Boolean.parseBoolean(_elAct.getAttribute("noResize")));
		//System.out.println("13");
		_wdgtAct.setPosition(_elAct.getAttribute("position"));
		//System.out.println("14");
		_wdgtAct.setSessionSave(_elAct.getAttribute("sessionSave").length()>0?Boolean.parseBoolean(_elAct.getAttribute("sessionSave")):true);
		//System.out.println("15");
		_wdgtAct.setShowInAllDesktops(Boolean.parseBoolean(_elAct.getAttribute("showInAllDesktops")));
		//System.out.println("16");
		_wdgtAct.setSingle(Boolean.parseBoolean(_elAct.getAttribute("single")));
		//System.out.println("17");
		_wdgtAct.setStatus(_elAct.getAttribute("status"));
		//System.out.println("18");
		_wdgtAct.setTitle(_elAct.getAttribute("title"));
		//System.out.println("19");
		_wdgtAct.setUrl(_elAct.getAttribute("url"));
		_wdgtAct.setRelativeUrl(_elAct.getAttribute("relativeUrl"));
		//System.out.println("20");
		_wdgtAct.setWidth(_elAct.getAttribute("width"));
		//System.out.println("21");
		_wdgtAct.setZIndex(_elAct.getAttribute("zIndex"));
		//System.out.println("22");
		// new fields
		//Integer
		//_wdgtAct.setMinHeight(_elAct.getAttribute("minHeight").length()>0?Integer.parseInt(_elAct.getAttribute("minHeight")):0);
		//_wdgtAct.setMinWidth(_elAct.getAttribute("minWidth").length()>0?Integer.parseInt(_elAct.getAttribute("minWidth")):0);
		//_wdgtAct.setMaxHeight(_elAct.getAttribute("maxHeight").length()>0?Integer.parseInt(_elAct.getAttribute("maxHeight")):0);
		//_wdgtAct.setMaxWidth(_elAct.getAttribute("maxWidth").length()>0?Integer.parseInt(_elAct.getAttribute("maxWidth")):0);
		//String
		_wdgtAct.setOnClose(_elAct.getAttribute("onClose"));
		_wdgtAct.setChromeType(_elAct.getAttribute("chromeType"));
		_wdgtAct.setTestigo(_elAct.getAttribute("testigo"));
		_wdgtAct.setTestigoJson(_elAct.getAttribute("testigoJson"));
		_wdgtAct.setSkin(_elAct.getAttribute("skin"));
		_wdgtAct.setImageFamily(_elAct.getAttribute("imageFamily"));
		_wdgtAct.setVirtualDesktop(_elAct.getAttribute("virtualDesktop"));
		
		// boolean
		_wdgtAct.setNoTaskbar(_elAct.getAttribute("noTaskbar").length()>0?Boolean.parseBoolean(_elAct.getAttribute("noTaskbar")):false);
		_wdgtAct.setNoHide(_elAct.getAttribute("noHide").length()>0?Boolean.parseBoolean(_elAct.getAttribute("noHide")):false);
		_wdgtAct.setNoDrag(_elAct.getAttribute("noDrag").length()>0?Boolean.parseBoolean(_elAct.getAttribute("noDrag")):false);
		
		
		// complex fields
		_wdgtAct.setParameters(addParameters(_elAct));
		//System.out.println("23");
		_wdgtAct.setDaemons(addDaemons(_elAct));
		//System.out.println("24");
		
		return _wdgtAct; 
	}
	
	/**
	 * Adds the applications from file.
	 * 
	 * @param arr
	 *            the arr
	 * @param doc
	 *            the doc
	 * @param indice
	 *            the indice
	 * @param fileNameNoExt 
	 * 
	 * @return the list
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private List addApplicationsFromFile(List arr, Document doc, int indice, String appGrpId) throws Exception{
		//System.out.println("addApplicationsFromFile");
		List<Application> miArr = arr;
		//Document miDoc = doc;
		
		NodeList _appWidget = doc.getElementsByTagName("APPLICATION");
		int numWidgets = _appWidget.getLength();
		
		for (int i=0;i<numWidgets;i++){
			Element _elAct = (Element) _appWidget.item(i);
			Application _wdgtAct = new Application();
			_wdgtAct.setWidget(false);
			miArr.add(fillAppObject(_wdgtAct, _elAct, indice, appGrpId));			
		}
		
		return miArr;
	}
	
	/**
	 * Dame html inicializar aplicaciones desde xml.
	 * 
	 * @param doc
	 *            the doc
	 * @param indiceGrupo
	 *            the indice grupo
	 * @param primeroSinComillas
	 *            the primero sin comillas
	 * 
	 * @return the string
	 */
	public String dameHtmlInicializarAplicacionesDesdeXml(Document doc, int indiceGrupo, boolean primeroSinComillas)	{
		StringBuffer _appJS = new StringBuffer();
		NodeList _app = doc.getElementsByTagName("APPLICATION");
		NodeList _appWidget = doc.getElementsByTagName("WIDGET");
		if ((_app.getLength()>0 || _appWidget.getLength()>0)&&!primeroSinComillas)
			_appJS.append(",");
		boolean escribeComas=false;
		for ( int i = 0 ; i < _app.getLength(); i++) {
			if (escribeComas)
				_appJS.append(",");
			else
				escribeComas=true;
			_appJS.append(dameHtmlAplicacion((Element)_app.item(i), indiceGrupo));
		}		
		for ( int i = 0 ; i < _appWidget.getLength(); i++) {
			if (escribeComas)
				_appJS.append(",");
			else
				escribeComas=true;
			_appJS.append(dameHtmlAplicacion((Element)_appWidget.item(i), indiceGrupo));
		}
		return _appJS.toString();
	}
	
	/**
	 * Dame html aplicacion.
	 * 
	 * @param _aplicacion
	 *            the _aplicacion
	 * @param indiceGrupo
	 *            the indice grupo
	 * 
	 * @return the string
	 */
	private String dameHtmlAplicacion(Element _aplicacion, int indiceGrupo)	{ //Da el de UNA apl
		//Escribo "<id>": {
		StringBuffer _appJS = new StringBuffer();
		_appJS.append(DesktopWriter.generaCadenaJavascript(_aplicacion.getAttribute("id")));
		_appJS.append(":{");
		//Escribo los atributos menos el ID ...
		NamedNodeMap losAtribs = _aplicacion.getAttributes();
		boolean escritoElPrimero=false;
		for (int j=0;j<losAtribs.getLength();j++)	{
			Node elAtrib = losAtribs.item(j);				
			if ( !elAtrib.getNodeName().equals("id") )	{
				if (escritoElPrimero)
					_appJS.append(",");
				_appJS.append("\""+elAtrib.getNodeName()+"\":");
				_appJS.append(DesktopWriter.generaCadenaJavascript(elAtrib.getFirstChild().getNodeValue()));
				escritoElPrimero=true;
			}
		}
		//a�adimos como atributo el indice de grupo
		_appJS.append(",grupo:"+indiceGrupo);
		if (_aplicacion.getNodeName().equals("WIDGET"))
			_appJS.append(",widget:true");	
		NodeList _daemons= _aplicacion.getElementsByTagName("DAEMON");
		if (_daemons.getLength()>0){
			_appJS.append(",daemons:[");
			for ( int j = 0 ; j < _daemons.getLength(); j++) {
				if (j>0)
					_appJS.append(",");
				Element unDaemon=(Element) _daemons.item(j);
				_appJS.append("{");
				_appJS.append("milliseconds:"+unDaemon.getAttribute("milliseconds"));
				if (unDaemon.hasAttribute("script"))	{
					_appJS.append(",script:\""+unDaemon.getAttribute("script")+"\"");
				}
				_appJS.append("}");
			}
			_appJS.append("]");
		}
		//ahora si tiene parametros los sacamos tambi�n
		NodeList _param = _aplicacion.getElementsByTagName("PARAMETER");
		if (_param.getLength()>0){
			_appJS.append(",\"parametros\":[");
			for ( int j = 0 ; j < _param.getLength(); j++) {
				if (j>0)
					_appJS.append(",");
				Element unparam=(Element) _param.item(j);
				_appJS.append("{\"nombre\":\""+unparam.getAttribute("name")+"\",");
				_appJS.append("\"valor\":"+DesktopWriter.generaCadenaJavascript(unparam.getFirstChild().getNodeValue()));
				_appJS.append("}");
			}
			_appJS.append("]");
		}
		_appJS.append("}");		
		return _appJS.toString();
	}

	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#setObjects(java.lang.Object[])
	 */
	public void setObjects(Object[] objects) throws Exception {
		// TODO Auto-generated method stub
	}


	public void addObjects(Object[] objects) throws Exception {	
		File elDir=new File(xmlAppsFiles);
		if (elDir.exists() && elDir.canWrite()){
			for (int i=0; i<objects.length; i++){
				Application aplicacion=(Application)objects[i];			
				if (aplicacion.getGroupId()!=null){//TO-DO: caso que me manden getGroup()
					String xmlGroupFileName=xmlAppsFiles + "/" + aplicacion.getGroupId() + ".xml";
					File fichXml=new File(xmlGroupFileName);
					if (fichXml.exists() && fichXml.canWrite()){ //TO-DO: Si existe actualizarlo ...				
						Document doc = DesktopXmlUtils.getDocumentFromFile(fichXml);
						if (aplicacion.getId()==null){ //Busco un ID que no colisione
							aplicacion.setId(dameIdQueNoColisione(doc));
						} else{//TO-DO: Confirmo que no existe y si existe borro el elemento
							
						}
						//NodeList _raiz = doc.getDocumentElement()//getElementsByTagName("APPLICATIONS");
						//Element _elemRaiz=DesktopXmlUtils.getFirstChildWithTagName(doc.getDocumentElement(), "APPLICATIONS");
						Element _elemRaiz=doc.getDocumentElement();
						//TO-DO: resto de atributos
						Element nuevoAcceso=doc.createElement("APPLICATION");	
						nuevoAcceso.setAttribute("description",aplicacion.getDescription());
						nuevoAcceso.setAttribute("icon",aplicacion.getIcon());
						nuevoAcceso.setAttribute("iconText",aplicacion.getIconText());
						nuevoAcceso.setAttribute("id",aplicacion.getId());
						nuevoAcceso.setAttribute("innerHeight",aplicacion.getInnerHeight());
						nuevoAcceso.setAttribute("innerWidth",aplicacion.getInnerWidth());
						nuevoAcceso.setAttribute("status",aplicacion.getStatus());
						nuevoAcceso.setAttribute("title",aplicacion.getTitle());
						nuevoAcceso.setAttribute("url",aplicacion.getUrl());					
						_elemRaiz.appendChild(nuevoAcceso);
						DesktopXmlUtils.escribeDOMEnDisco(doc, xmlGroupFileName);


/*
getAttribute("id"));
getAttribute("description"));
_wdgtAct.setHeight(_elAct.getAttribute("height"));
_wdgtAct.setIcon(_elAct.getAttribute("icon"));
_wdgtAct.setIconText(_elAct.getAttribute("iconText"));
_wdgtAct.setIgnoreMargins(Boolean.parseBoolean(_elAct.getAttribute("ignoreMargins")));
_wdgtAct.setInnerHeight(_elAct.getAttribute("innerHeight"));
_wdgtAct.setInnerWidth(_elAct.getAttribute("innerWidth"));
_wdgtAct.setJs(_elAct.getAttribute("js"));
_wdgtAct.setEngine(_elAct.getAttribute("engine"));
_wdgtAct.setHelpUrl(_elAct.getAttribute("helpUrl"));
_wdgtAct.setRelativeHelpUrl(_elAct.getAttribute("relativeHelpUrl"));		
_wdgtAct.setNoResize(Boolean.parseBoolean(_elAct.getAttribute("noResize")));
_wdgtAct.setPosition(_elAct.getAttribute("position"));
_wdgtAct.setSessionSave(_elAct.getAttribute("sessionSave").length()>0?Boolean.parseBoolean(_elAct.getAttribute("sessionSave")):true);
_wdgtAct.setShowInAllDesktops(Boolean.parseBoolean(_elAct.getAttribute("showInAllDesktops")));
_wdgtAct.setSingle(Boolean.parseBoolean(_elAct.getAttribute("single")));
_wdgtAct.setStatus(_elAct.getAttribute("status"));
_wdgtAct.setTitle(_elAct.getAttribute("title"));
_wdgtAct.setUrl(_elAct.getAttribute("url"));
_wdgtAct.setRelativeUrl(_elAct.getAttribute("relativeUrl"));
_wdgtAct.setWidth(_elAct.getAttribute("width"));
_wdgtAct.setZIndex(_elAct.getAttribute("zIndex"));
_wdgtAct.setOnClose(_elAct.getAttribute("onClose"));
_wdgtAct.setChromeType(_elAct.getAttribute("chromeType"));
_wdgtAct.setTestigo(_elAct.getAttribute("testigo"));
_wdgtAct.setTestigoJson(_elAct.getAttribute("testigoJson"));
_wdgtAct.setSkin(_elAct.getAttribute("skin"));
_wdgtAct.setImageFamily(_elAct.getAttribute("imageFamily"));
_wdgtAct.setVirtualDesktop(_elAct.getAttribute("virtualDesktop"));
_wdgtAct.setNoTaskbar(_elAct.getAttribute("noTaskbar").length()>0?Boolean.parseBoolean(_elAct.getAttribute("noTaskbar")):false);
_wdgtAct.setNoHide(_elAct.getAttribute("noHide").length()>0?Boolean.parseBoolean(_elAct.getAttribute("noHide")):false);
_wdgtAct.setNoDrag(_elAct.getAttribute("noDrag").length()>0?Boolean.parseBoolean(_elAct.getAttribute("noDrag")):false);
_wdgtAct.setParameters(addParameters(_elAct));
_wdgtAct.setDaemons(addDaemons(_elAct));
						 * 
						 */
						/*		NodeList _appWidget = doc.getElementsByTagName("APPLICATION");
		int numWidgets = _appWidget.getLength();
		
		for (int i=0;i<numWidgets;i++){
			Element _elAct = (Element) _appWidget.item(i);
			Application _wdgtAct = new Application();
			_wdgtAct.setWidget(false);
			miArr.add(fillAppObject(_wdgtAct, _elAct, indice));			
		}
		
		return miArr;*/
					}
				}
				/*
				String xmlGroupFileName=xmlAppsFiles + "/" + grupo.get() + ".xml";
				File nuevoFichXml=new File(xmlFileName);
				if (!nuevoFichXml.exists()){ //TO-DO: Si existe actualizarlo ...
					//DesktopWriter.escribeDOMEnDisco();
					Document doc=DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
					Element root=doc.createElement("APPLICATIONS");	
					root.setAttribute("author", grupo.getAuthor());
					root.setAttribute("category", grupo.getCategory());
					root.setAttribute("description", grupo.getDescription());
					root.setAttribute("license", grupo.getLicense());
					root.setAttribute("version", grupo.getVersion());
					root.appendChild(doc.createTextNode(""));
					doc.appendChild(root);
					DesktopXmlUtils.escribeDOMEnDisco(doc, xmlFileName);
					//System.out.println(">>>>>>>>>>"+xmlFileName);
				}*/
				
			}
		}
	}

	private String dameIdQueNoColisione(Document doc) {
		String ret;
		do {
			ret="ID"+Math.round(Math.random()*10000000);
		}while (idColisiona(ret,doc));
		return ret;
	}

	private boolean idColisiona(String id, Document doc) {
		// TODO Auto-generated method stub
		NodeList _appWidget = doc.getElementsByTagName("WIDGET");
		for (int w=0;w<_appWidget.getLength();w++){
			Element _elmAct = (Element) _appWidget.item(w);
			if (_elmAct.getAttribute("id").equals(id))
				return true;
		}
		NodeList _app = doc.getElementsByTagName("APPLICATION");
		for (int w=0;w<_app.getLength();w++){
			Element _elmAct = (Element) _app.item(w);
			if (_elmAct.getAttribute("id").equals(id))
				return true;
		}	
		return false;
	}


}
