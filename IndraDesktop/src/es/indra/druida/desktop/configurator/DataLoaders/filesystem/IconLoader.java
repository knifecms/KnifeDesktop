package es.indra.druida.desktop.configurator.DataLoaders.filesystem;

import java.io.File;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader;
import es.indra.druida.desktop.configurator.beans.Icon;
import es.indra.druida.desktop.utils.DesktopWriter;
import es.indra.druida.desktop.utils.DesktopXmlUtils;

/**
 * The Class IconLoader.
 */
public class IconLoader extends AbstractLoader{
	
	/** The xml icons file name. */
	private String xmlIconsFileName = null; //Como de gen�rico es esto?
	//private Skin[] icons;
	
	/**
	 * Instantiates a new icon loader.
	 * 
	 * @param xmlIconsFile
	 *            the xml icons file
	 */
	public IconLoader(String xmlIconsFile){
		super();
		this.xmlIconsFileName = xmlIconsFile;
	}	
	
	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#getObjects()
	 */
	public Object[] getObjects() throws Exception { //usa el nombre del fichero XML para leerlo y retornar una lista de beans
		File xmlIconsFile=new File(xmlIconsFileName);
		if (xmlIconsFile.exists())	{
			// obtenemos el documento XML del usuario
			Document doc = DesktopXmlUtils.getDocumentFromFile(xmlIconsFile);
			NodeList _iconos = doc.getElementsByTagName("ICON");
			Icon[] ret=new Icon[_iconos.getLength()];
			for (int i=0; i<_iconos.getLength(); i++){
				Element _iconoActual = (Element) _iconos.item(i);				
				int x=0,y=0;
				String application=null, file=null;
				if (_iconoActual.hasAttribute("x"))
					x=Integer.parseInt(_iconoActual.getAttribute("x").trim());
				if (_iconoActual.hasAttribute("y"))
					y=Integer.parseInt(_iconoActual.getAttribute("y").trim());	
				if (_iconoActual.hasAttribute("application"))
					application=_iconoActual.getAttribute("application");	
				if (_iconoActual.hasAttribute("file"))
					application=_iconoActual.getAttribute("file");					
				ret[i]=new Icon(x,y,application,file);
			}
			return ret;
		}else{
			Logger.getLogger(IconLoader.class.getName()).severe("IconLoader Cant find "+xmlIconsFile);
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#setObjects(java.lang.Object[])
	 */
	public void setObjects(Object[] objects) throws Exception {//Usando un array de beans (y el nombre del fichero) escribe un XML
		Icon[] icons=(Icon[])objects;
		Document doc=DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		Element root=doc.createElement("ICONS");
		doc.appendChild(root);
		for (int r=0;r<icons.length; r++){
			Icon icon=icons[r];
			Element elemento=doc.createElement("ICON");
			elemento.setAttribute("x",""+icon.getX());
			elemento.setAttribute("y",""+icon.getY());
			if (icon.getApplication()!=null && !icon.getApplication().equals("null"))
				elemento.setAttribute("application",icon.getApplication());
			if (icon.getFile()!=null && !icon.getFile().equals("null"))
				elemento.setAttribute("file",icon.getFile());	
			root.appendChild(elemento);
		}
		DesktopXmlUtils.escribeDOMEnDisco(doc, xmlIconsFileName);
	}

	@Override
	public void addObjects(Object[] objects) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
}
