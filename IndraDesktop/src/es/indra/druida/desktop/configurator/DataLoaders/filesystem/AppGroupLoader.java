package es.indra.druida.desktop.configurator.DataLoaders.filesystem;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import es.indra.druida.desktop.configurator.DesktopConfig;
import es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader;
import es.indra.druida.desktop.configurator.beans.AppGroup;
import es.indra.druida.desktop.utils.DesktopXmlUtils;

/**
 * The Class AppGroupLoader.
 */
public class AppGroupLoader extends AbstractLoader{
	
	/** The app group. */
	private AppGroup[] appGroup;
	
	/** The xml groups files. */
	private String xmlGroupsFiles = null;
		
	/**
	 * Instantiates a new app group loader.
	 * 
	 * @param xmlGroupsFiles
	 *            the xml groups files
	 */
	public AppGroupLoader(String xmlGroupsFiles){
		super();
		//System.out.println("Constructor");
		this.xmlGroupsFiles = xmlGroupsFiles;
		//System.out.println("Constructor OK");
	}

	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#getObjects()
	 */
	public Object[] getObjects() throws Exception {
		//System.out.println("getObjects");
		File elDir=new File(xmlGroupsFiles);
		ArrayList<AppGroup> arr = new ArrayList<AppGroup>();
		int numFich=0;
		//System.out.println("getObjects1");
		if (elDir.exists() && elDir.canRead()){
			String[]losFich=elDir.list();
			//int indice=0; //indice del grupo
			numFich=losFich.length;			
			
			for (int i=0;i<numFich;i++){
				int ii=losFich[i].lastIndexOf(".");
				String extension=((ii!=-1)? losFich[i].substring(ii+1).toUpperCase() :"");
				String xmlFileApp = xmlGroupsFiles + "/" + losFich[i];
				File ficheroXml=new File(xmlFileApp);
				if (extension.equals("XML") && ficheroXml.exists()){
					try	{
						Document doc = DesktopXmlUtils.getDocumentFromFile(ficheroXml);
						String fileNameNoExt=ficheroXml.getName();	 //Por ejemplo "drConsoleTest.xml"
						int ind=fileNameNoExt.lastIndexOf(".");
						if (ind!=-1)	
							fileNameNoExt=fileNameNoExt.substring(0,ind); //Por ejemplo "drConsoleTest"
						//tambi�n hago una lista con los nombres de las aplicaciones
						ArrayList <String>applicationsAL=new ArrayList <String>();
						NodeList _appWidget = doc.getElementsByTagName("WIDGET");
						for (int w=0;w<_appWidget.getLength();w++){
							Element _elmAct = (Element) _appWidget.item(w);
							applicationsAL.add(_elmAct.getAttribute("id"));
						}
						NodeList _app = doc.getElementsByTagName("APPLICATION");
						for (int w=0;w<_app.getLength();w++){
							Element _elmAct = (Element) _app.item(w);
							applicationsAL.add(_elmAct.getAttribute("id"));
						}					
						arr.add(new AppGroup(fileNameNoExt,doc.getDocumentElement().getAttribute("description"), applicationsAL.toArray(new String[applicationsAL.size()])));
					} catch(Exception e){
						Logger.getLogger(DesktopConfig.class.getName()).severe("Error loading Application Groups ('"+ficheroXml.getName()+"'): \n  "+e.toString());
					}
				}
			}
			//System.out.println("FIN Grupos");
		}
		
		//System.out.println("FIN2 Grupos");
		//System.out.println("Grupos: "+arr.toString());
		numFich=arr.size();
		appGroup = new AppGroup[numFich];
		for (int i=0;i<numFich;i++){
			appGroup[i]=(AppGroup)arr.get(i);
		}
		
		//System.out.println("FIN GroupLoader");
		return appGroup;
	}
	
	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#setObjects(java.lang.Object[])
	 */
	public void setObjects(Object[] objects) throws Exception {
		// TODO Auto-generated method stub
	}

	public void addObjects(Object[] objects) throws Exception {//A�ade grupos nuevos
		//Tengo que crear los ficheros si no existen
		File elDir=new File(xmlGroupsFiles);
		if (elDir.exists() && elDir.canWrite()){
			for (int i=0; i<objects.length; i++){
				AppGroup grupo=(AppGroup)objects[i];
				String xmlFileName=xmlGroupsFiles + "/" + grupo.getName() + ".xml";
				File nuevoFichXml=new File(xmlFileName);
				if (!nuevoFichXml.exists()){ //TO-DO: Si existe actualizarlo ...
					//DesktopWriter.escribeDOMEnDisco();
					Document doc=DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
					Element root=doc.createElement("APPLICATIONS");	
					root.setAttribute("author", grupo.getAuthor());
					root.setAttribute("category", grupo.getCategory());
					root.setAttribute("description", grupo.getDescription());
					root.setAttribute("license", grupo.getLicense());
					root.setAttribute("version", grupo.getVersion());
					root.appendChild(doc.createTextNode(""));
					doc.appendChild(root);
					DesktopXmlUtils.escribeDOMEnDisco(doc, xmlFileName);
					//System.out.println(">>>>>>>>>>"+xmlFileName);
				}
				
			}
		}
	}	


}