package es.indra.druida.desktop.configurator.DataLoaders.filesystem;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import es.indra.druida.desktop.configurator.DesktopConfig;
import es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader;
import es.indra.druida.desktop.configurator.beans.Skin;
import es.indra.druida.desktop.configurator.beans.SkinParameter;
import es.indra.druida.desktop.utils.DesktopXmlUtils;

/**
 * The Class SkinLoader.
 */
public class SkinLoader extends AbstractLoader{
	
	/** The app def root. */
	private String appDefRoot = null;
	
	/**
	 * Instantiates a new skin loader.
	 * 
	 * @param appDefRoot
	 *            the app def root
	 */
	public SkinLoader(String appDefRoot){
		super();
		this.appDefRoot = appDefRoot;
	}
	
	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#getObjects()
	 */
	public Object[] getObjects() throws Exception {
		ArrayList <Skin> ret=new ArrayList<Skin>();		
		File elDir=new File(appDefRoot);
		int indiceGrupo=0; //indice del grupo
		if (elDir.exists() && elDir.canRead()){
			File[] ficheros=elDir.listFiles();
			for (int i=0;i<ficheros.length;i++){		
				File fichero=ficheros[i];			
				int indExt=fichero.getName().lastIndexOf(".");
				String extension=((indExt!=-1)? fichero.getName().substring(indExt+1).toUpperCase() :"");
				if (extension.equals("XML")){
					try{
						Document doc = DesktopXmlUtils.getDocumentFromFile(fichero);
						NodeList skins = doc.getElementsByTagName("SKIN");
						for (int j=0;j<skins.getLength();j++){
							Element elemento = (Element)skins.item(j);
							ret.add(getSkinBeanFromXmlElement(elemento,""+indiceGrupo));
						}
						indiceGrupo++; //La contabilidad de grupos debe cuadrar con la de aplicaciones
					}catch(Exception e){
						Logger.getLogger(DesktopConfig.class.getName()).severe("Error loading Skins ('"+fichero.getName()+"'): \n  "+e.toString());
					}
				}
			}
		}else{
			System.out.println("ERROR: SkinLoader Cant find "+appDefRoot);
			return null;
		}
		return ret.toArray(new Skin[0]);
	}
	
	/**
	 * Gets the skin bean from xml element.
	 * 
	 * @param elemento
	 *            the elemento
	 * 
	 * @return the skin bean from xml element
	 */
	public static Skin getSkinBeanFromXmlElement(Element elemento) {
		return getSkinBeanFromXmlElement(elemento,null);
	}	
	
	/**
	 * Gets the skin bean from xml element.
	 * 
	 * @param elemento
	 *            the elemento
	 * @param grupo
	 *            the grupo
	 * 
	 * @return the skin bean from xml element
	 */
	public static Skin getSkinBeanFromXmlElement(Element elemento, String grupo) {
		Skin ret= new Skin (elemento.getAttribute("id"));
		if (grupo!=null)
			ret.setGroup(grupo);
		if (elemento.getAttribute("description")!=null)
			ret.setDescription(elemento.getAttribute("description"));
		NodeList parameters = elemento.getElementsByTagName("parameter");
		int lengthParam = parameters.getLength();
		SkinParameter[] skinParameters = new SkinParameter[lengthParam];
		for (int j=0;j<lengthParam;j++) {
			Element parameter=(Element)parameters.item(j);
			SkinParameter sPar = new SkinParameter(
					(parameter.getAttribute("name")),
					(parameter.getTextContent())		
				);
			if (parameter.hasAttribute("values"))
				sPar.setPosibleValues(parameter.getAttribute("values"));
			if (parameter.hasAttribute("valueDescriptions"))
				sPar.setPosibleValueDescriptions(parameter.getAttribute("valueDescriptions"));
			if (parameter.hasAttribute("noEditable"))
				sPar.setNoEditable(parameter.getAttribute("noEditable"));
			if (parameter.hasAttribute("description"))
				sPar.setDescription(parameter.getAttribute("description"));
			if (parameter.hasAttribute("type"))
				sPar.setType(parameter.getAttribute("type"));				
			skinParameters[j]=sPar;
		}
		ret.setParameters(skinParameters);
		return ret;
	}

	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#setObjects(java.lang.Object[])
	 */
	public void setObjects(Object[] objects) throws Exception {

	}

	@Override
	public void addObjects(Object[] objects) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
