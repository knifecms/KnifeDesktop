package es.indra.druida.desktop.configurator.DataLoaders;

public abstract class AbstractSecurityLoader extends AbstractLoader {

	public abstract void setPassword(String userId, String oldPasswd, String newPasswd) throws Exception;

}
