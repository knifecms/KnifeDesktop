package es.indra.druida.desktop.configurator.DataLoaders.filesystem;

import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader;
import es.indra.druida.desktop.configurator.beans.Parameter;
import es.indra.druida.desktop.configurator.beans.Skin;
import es.indra.druida.desktop.configurator.beans.SkinChanges;
import es.indra.druida.desktop.utils.DesktopWriter;
import es.indra.druida.desktop.utils.DesktopXmlUtils;

/**
 * The Class CustomSkinParametersLoader.
 */
public class CustomSkinParametersLoader extends AbstractLoader{
	
	/** The xml file name. */
	private String xmlFileName = null; 
	
	/**
	 * Instantiates a new custom skin parameters loader.
	 * 
	 * @param rutaCompletaDirUsu
	 *            the ruta completa dir usu
	 */
	public CustomSkinParametersLoader(String rutaCompletaDirUsu){
		super();
		this.xmlFileName = rutaCompletaDirUsu;
	}	
	
	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#getObjects()
	 */
	public Object[] getObjects() throws Exception { //usa el nombre del fichero XML para leerlo y retornar una lista de beans Skin[]
		File elFich=new File(xmlFileName);
		if (elFich.exists() && elFich.canRead()){
			ArrayList <Skin> ret=new ArrayList<Skin>();	
			Document doc = DesktopXmlUtils.getDocumentFromFile(elFich);
			NodeList skins = doc.getElementsByTagName("SKIN");
			for (int j=0;j<skins.getLength();j++){
				Element elemento = (Element)skins.item(j);
				ret.add(SkinLoader.getSkinBeanFromXmlElement(elemento));
			}			
			return ret.toArray(new Skin[0]);
		}else{
			return new Skin[0];
		}
	}

	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#setObjects(java.lang.Object[])
	 */
	public void setObjects(Object[] objects) throws Exception {//graba en drdesktop-user-skins.xml los cambios usando un array de beans SkinChanges[].
		SkinChanges SCBean=(SkinChanges)(objects[0]);
		Parameter[] paramACamb=SCBean.getParameters();
		if (paramACamb==null || paramACamb.length==0)
			return;
		File fichXml=new File(xmlFileName);
		Document doc;
		Element root;
		if (fichXml.exists()){ //Si existe el fichero lo cargo
			doc=DesktopXmlUtils.getDocumentFromFile(fichXml);
			root=(Element)doc.getElementsByTagName("drdesktop-user-skins").item(0);//Si no existiera ser�a un error en el xml 
		} else { //Si no existe el fichero lo creo nuevo
			doc=DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();	
			root=doc.createElement("drdesktop-user-skins");
			doc.appendChild(root);
		}
		//Existe el tag skin cuyo id es el deseado?
		Element skinAModificar=getFirstChildWithAttribute(root,"id",SCBean.getId());
		if (skinAModificar==null){
			skinAModificar=doc.createElement("SKIN");
			skinAModificar.setAttribute("id", SCBean.getId());
			root.appendChild(skinAModificar);
		}
		//Ahora por cada propiedad, existe en ese skin ?
		//TO-DO: optimizar eliminando las propiedades que sean iguales que las generales
		for (int i=0; i<paramACamb.length; i++){
			Element paramElemACamb=doc.createElement("parameter");
			paramElemACamb.setAttribute("name", paramACamb[i].getName());
			paramElemACamb.appendChild(doc.createTextNode(paramACamb[i].getValue()));
			Element oldParamElemACamb=getFirstChildWithAttribute(skinAModificar,"name",paramACamb[i].getName());
			if (oldParamElemACamb==null){
				skinAModificar.appendChild(paramElemACamb);
			} else{
				skinAModificar.replaceChild(paramElemACamb, oldParamElemACamb);
			}
		}
		DesktopXmlUtils.escribeDOMEnDisco(doc, xmlFileName);
	}
	
	/**
	 * Gets the first child with attribute.
	 * 
	 * @param e
	 *            the e
	 * @param attr
	 *            the attr
	 * @param value
	 *            the value
	 * 
	 * @return the first child with attribute
	 */
	private Element getFirstChildWithAttribute(Element e, String attr, String value)	{
		NodeList childNodes=e.getChildNodes();
		for (int i=0; i<childNodes.getLength(); i++){
			Node childNode=childNodes.item(i);
			if (childNode.getNodeType()==Node.ELEMENT_NODE){
				Element childElem=(Element)childNode;
				if (childElem.hasAttribute(attr)){
					if (childElem.getAttribute(attr).equals(value))
						return childElem;
				}
			}
		}
		return null;		
	}

	@Override
	public void addObjects(Object[] objects) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
}
