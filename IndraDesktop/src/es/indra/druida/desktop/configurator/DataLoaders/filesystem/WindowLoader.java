package es.indra.druida.desktop.configurator.DataLoaders.filesystem;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader;
import es.indra.druida.desktop.configurator.beans.Window;
import es.indra.druida.desktop.utils.DesktopWriter;
import es.indra.druida.desktop.utils.DesktopXmlUtils;

/**
 * The Class WindowLoader.
 */
public class WindowLoader extends AbstractLoader{
	
	/** The xml file name. */
	private String xmlFileName = null; //Como de gen�rico es esto?
	
	/**
	 * Instantiates a new window loader.
	 * 
	 * @param xmlFile
	 *            the xml file
	 */
	public WindowLoader(String xmlFile){
		super();
		this.xmlFileName = xmlFile;
	}
	
	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#getObjects()
	 */
	public Object[] getObjects() throws Exception { //usa el nombre del fichero XML para leerlo y retornar una lista de beans
		File xmlFile=new File(xmlFileName);
		if (xmlFile.exists())	{
			Document doc = DesktopXmlUtils.getDocumentFromFile(xmlFile);
			NodeList _ventanas = doc.getElementsByTagName("WINDOW");
			Window[] ret=new Window[_ventanas.getLength()];
			for ( int i = 0 ; i < _ventanas.getLength(); i++) {
				HashMap<String, String> hashMapAtrib=new HashMap<String, String>();
				NamedNodeMap losAtribs = ((Element)_ventanas.item(i)).getAttributes();
				for (int j=0;j<losAtribs.getLength();j++)	{
					Node elAtrib = losAtribs.item(j);
					hashMapAtrib.put(elAtrib.getNodeName(), elAtrib.getFirstChild().getNodeValue());
				}
				ret[i]=new Window(hashMapAtrib);
			}			
			return ret;
		}else{
			System.out.println("ERROR: WindowLoader Cant find "+xmlFile);
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#setObjects(java.lang.Object[])
	 */
	public void setObjects(Object[] objects) throws Exception {//Usando un array de beans (y el nombre del fichero) escribe un XML
		Window[] windows=(Window[])objects;
		Document doc=DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		Element root=doc.createElement("WINDOWS");
		doc.appendChild(root);
		for (int r=0;r<windows.length; r++)	{
			Window window=windows[r];
			Element elemento=doc.createElement("WINDOW");
			HashMap<String, String> propiedades = window.get();
			Iterator<String> propNames = propiedades.keySet().iterator();
		    while(propNames.hasNext()){
		    	String propName=propNames.next();
		    	if (propiedades.get(propName)!=null)
		    		elemento.setAttribute(propName,propiedades.get(propName));
		    }
		    root.appendChild(elemento);
		}
		DesktopXmlUtils.escribeDOMEnDisco(doc, xmlFileName);
	}

	@Override
	public void addObjects(Object[] objects) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	
}
