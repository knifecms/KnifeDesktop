package es.indra.druida.desktop.configurator.DataLoaders.filesystem;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader;
import es.indra.druida.desktop.configurator.beans.MenuItem;
import es.indra.druida.desktop.utils.DesktopWriter;
import es.indra.druida.desktop.utils.DesktopXmlUtils;

/**
 * The Class MenuLoader.
 */
public class MenuLoader extends AbstractLoader{

	/** The xml file name. */
	private String xmlFileName = null;
	
	/**
	 * Instantiates a new menu loader.
	 * 
	 * @param xmlFileName
	 *            the xml file name
	 */
	public MenuLoader(String xmlFileName){
		super();
		this.xmlFileName = xmlFileName;
	}	
	
	/* setObjects  ------------------------------- */
	
	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#setObjects(java.lang.Object[])
	 */
	public void setObjects(Object[] objects) throws Exception {
		MenuItem[] menuItems= (MenuItem[])objects;
		Document doc=DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		Element root=doc.createElement("PROGRAMS");
		doc.appendChild(root);
		HashMap <String, MenuItem>nodosArbol= new HashMap();
		for (int i=0; i<menuItems.length; i++){ //Indexo en un hash
			nodosArbol.put(menuItems[i].getId(),menuItems[i]);
		}
		//Tomo INICIO_RAIZ
		MenuItem rootItem=nodosArbol.get("INICIO_RAIZ");
		String[] hijos=rootItem.getChildren();
		for (int j=0; j<hijos.length; j++){
			setObjects_creaHijoDe(root,hijos[j],nodosArbol, doc);			
		}
		DesktopXmlUtils.escribeDOMEnDisco(doc, xmlFileName);
	}
	
	/**
	 * Sets the objects_crea hijo de.
	 * 
	 * @param elementoXmlPadre
	 *            the elemento xml padre
	 * @param idHijo
	 *            the id hijo
	 * @param nodosArbol
	 *            the nodos arbol
	 * @param doc
	 *            the doc
	 */
	private void setObjects_creaHijoDe(Element elementoXmlPadre, String idHijo, HashMap <String, MenuItem> nodosArbol, Document doc) {
		MenuItem item = nodosArbol.get(idHijo);
		if (item==null)
			return; 
		Element elemento=null;
		if (item.getIsRule()!=null && item.getIsRule()){
			elemento=doc.createElement("LINE");
			elementoXmlPadre.appendChild(elemento);
			return;
		}else if (item.getChildren()==null){
			elemento=doc.createElement("PROGRAM");
		}else{
			elemento=doc.createElement("FOLDER");
		}
		if (item.getImageFamily()!=null)
			elemento.setAttribute("imageFamily", item.getImageFamily());
		if (item.getImg()!=null)
			elemento.setAttribute("img", item.getImg());
		if (item.getJs()!=null)
			elemento.setAttribute("js", item.getJs());
		if (item.getName()!=null)
			elemento.setAttribute("name", item.getName());
		if (item.getTitle()!=null)
			elemento.setAttribute("title", item.getTitle());		
		if (item.getChildren()!=null){
			String[] hijos=item.getChildren();
			for (int j=0; j<hijos.length; j++){
				setObjects_creaHijoDe(elemento,hijos[j],nodosArbol, doc);			
			}
		}
		elementoXmlPadre.appendChild(elemento);		
	}
	
	
	
	/* getObjects  ------------------------------- */
	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#getObjects()
	 */
	public Object[] getObjects() throws Exception {
		File xmlFile=new File(xmlFileName);
		if (xmlFile.exists())	{
			Document doc = DesktopXmlUtils.getDocumentFromFile(xmlFile);
			ponIdUnicoANodos(doc);
			ArrayList <MenuItem> opciones = new ArrayList();
			//Nodo raiz
			MenuItem NodoRaiz= new MenuItem();
			String[]children=getListaHijos(doc.getDocumentElement());
			NodoRaiz.setId("INICIO_RAIZ");
			NodoRaiz.setChildren(children);
			opciones.add(NodoRaiz);			
			//Nodo barra horizontal
			MenuItem NodoBarra=new MenuItem();
			NodoBarra.setId("BARRA");
			NodoBarra.setIsRule(true);
			opciones.add(NodoBarra);			
			addMenuItemsWithTag("FOLDER",doc,opciones);
			addMenuItemsWithTag("PROGRAM",doc,opciones);
			addMenuItemsWithTag("FILE",doc,opciones);	
			MenuItem[] ret=opciones.toArray(new MenuItem[0]);
			return ret;
		}else{
			System.out.println("ERROR: MenuLoader Cant find "+xmlFile);
			return null;
		}
	}
	
	/**
	 * Adds the menu items with tag.
	 * 
	 * @param tag
	 *            the tag
	 * @param doc
	 *            the doc
	 * @param opciones
	 *            the opciones
	 */
	private void addMenuItemsWithTag(String tag, Document doc, ArrayList<MenuItem> opciones) { //A�ado los menuItems con ese tag
		NodeList nodes = doc.getElementsByTagName(tag);
		boolean isFile=tag.equals("FILE");
		boolean isRule=false;		
		for ( int i = 0 ; i < nodes.getLength() ; i++) {
			Node node = nodes.item(i);
			if (node.getNodeType()==Node.ELEMENT_NODE){
				Element element = (Element)node;
				String id=null,name=null,imageFamily=null,title=null,img=null,js=null;
				String[] children=null;
				if (element.hasAttribute("id"))
					id=element.getAttribute("id");				
				if (element.hasAttribute("name"))
					name=element.getAttribute("name");
				if (element.hasAttribute("imageFamily"))
					imageFamily=element.getAttribute("imageFamily");				
				if (element.hasAttribute("title"))
					title=element.getAttribute("title");
				if (element.hasAttribute("img"))
					img=element.getAttribute("img");	
				if (element.hasAttribute("js"))
					js=element.getAttribute("js");
				if (tag.equals("FOLDER"))	
					children=getListaHijos(element);
				MenuItem menuItem= new MenuItem();
				menuItem.setId(id);
				menuItem.setName(name);
				menuItem.setImageFamily(imageFamily);
				menuItem.setTitle(title);
				menuItem.setImg(img);
				menuItem.setJs(js);
				menuItem.setIsRule(isRule);
				menuItem.setIsFile(isFile);
				menuItem.setChildren(children);
				opciones.add(menuItem);		
			}
		}
			
	}
	
	/**
	 * Pon id unico a nodos.
	 * 
	 * @param doc
	 *            the doc
	 * 
	 * @return the document
	 */
	private static Document ponIdUnicoANodos(Document doc){
		int numNodo=0;
		NodeList _nodosFolder = doc.getElementsByTagName("FOLDER");		
		NodeList _nodosProgram = doc.getElementsByTagName("PROGRAM");
		NodeList _nodosFile = doc.getElementsByTagName("FILE");		
		for ( int i = 0 ; i < _nodosFile.getLength() ; i++) {
			Element _nodo = (Element)_nodosFile.item(i);
			_nodo.setAttribute("id", "ID_"+ (numNodo++) );
		}
		for ( int i = 0 ; i < _nodosProgram.getLength() ; i++) {
			Element _nodo = (Element)_nodosProgram.item(i);
			_nodo.setAttribute("id", "ID_"+ (numNodo++) );
		}
		for ( int i = 0 ; i < _nodosFolder.getLength() ; i++) {
			Element _nodo = (Element)_nodosFolder.item(i);
			_nodo.setAttribute("id", "ID_"+ (numNodo++) );
		}		
		return doc;
	}	

	/**
	 * Gets the lista hijos.
	 * 
	 * @param element
	 *            the element
	 * 
	 * @return the lista hijos
	 */
	private String[] getListaHijos(Element element) {
		NodeList _listaHijos = element.getChildNodes();
		ArrayList <String> hijos = new ArrayList<String>();
		for ( int i = 0 ; i < _listaHijos.getLength() ; i++) {
			Node _nodoActual = _listaHijos.item(i);
			if (_nodoActual.getNodeType()==Node.ELEMENT_NODE){
				Element _elHijoActual = (Element) _nodoActual;
				if (_elHijoActual.getNodeName().equals("FOLDER") ||	_elHijoActual.getNodeName().equals("PROGRAM") ||	_elHijoActual.getNodeName().equals("FILE")) {
					hijos.add(_elHijoActual.getAttribute("id"));
				} else if (_elHijoActual.getNodeName().equals("LINE")) {
					hijos.add("BARRA");					
				}				
			}
		}
		
		String[] ret= hijos.toArray(new String[0]);
		
		return ret;
	}

	@Override
	public void addObjects(Object[] objects) throws Exception {
		// TODO Auto-generated method stub
		
	}
}
