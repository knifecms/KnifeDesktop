package es.indra.druida.desktop.configurator.DataLoaders;

/**
 * The Class AbstractLoader.
 */
public abstract class AbstractLoader {

	/**
	 * Gets the objects.
	 * 
	 * @return the objects
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public abstract Object[] getObjects() throws Exception;
	
	/**
	 * Sets the objects.
	 * 
	 * @param objects
	 *            the new objects
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public abstract void setObjects(Object[] objects) throws Exception;

	public abstract void addObjects(Object[] objects) throws Exception;

}
