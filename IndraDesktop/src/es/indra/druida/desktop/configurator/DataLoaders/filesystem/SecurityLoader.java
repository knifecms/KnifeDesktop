package es.indra.druida.desktop.configurator.DataLoaders.filesystem;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import es.indra.druida.desktop.configurator.ResourcesService;
import es.indra.druida.desktop.configurator.DataLoaders.AbstractSecurityLoader;
import es.indra.druida.desktop.configurator.beans.SecurityConfiguration;
import es.indra.druida.desktop.configurator.beans.User;
import es.indra.druida.desktop.configurator.beans.SecurityConstraint;
import es.indra.druida.desktop.utils.DesktopXmlUtils;
import es.indra.druida.desktop.utils.SecurityUtils;

/**
 * The Class SecurityLoader.
 */
public class SecurityLoader extends AbstractSecurityLoader {


	/** The xml file name. */
	private String xmlFileName = null;
	
	/**
	 * Instantiates a new security loader.
	 * 
	 * @param xmlFileName
	 *            the xml file name
	 */
	public SecurityLoader(String xmlFileName){
		super();
		this.xmlFileName = xmlFileName;
	}	
	
	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#getObjects()
	 */
	@Override
	public Object[] getObjects() throws Exception {
		SecurityConfiguration securityConfiguration=new SecurityConfiguration();
		File xmlFile=new File(xmlFileName);
		if (xmlFile.exists())	{
			Document doc = DesktopXmlUtils.getDocumentFromFile(xmlFile);
			NodeList users = doc.getDocumentElement().getElementsByTagName("user");
			NodeList constraints = doc.getDocumentElement().getElementsByTagName("constraint");
			ArrayList <User> usersRet=new ArrayList<User>();
			ArrayList <SecurityConstraint> constrRet=new ArrayList<SecurityConstraint>();
			for ( int i = 0 ; i < users.getLength(); i++) {
				User user=new User();
				Element element = (Element)users.item(i);
				if (element.hasAttribute("id"))
					user.setId(element.getAttribute("id"));	
				if (element.hasAttribute("name"))
					user.setName(element.getAttribute("name"));	
				if (element.hasAttribute("surname"))
					user.setSurname(element.getAttribute("surname"));	
				if (element.hasAttribute("motherSurname"))
					user.setMotherSurname(element.getAttribute("motherSurname"));	
				if (element.hasAttribute("password") || element.hasAttribute("passwordHash") )
					user.setHasPassword(true);	
				if (element.hasAttribute("fileSystemRoot"))
					user.setFileSystemRoot(element.getAttribute("fileSystemRoot"));					
				if (element.hasAttribute("roles")){
					String rolesStr=element.getAttribute("roles");
					user.setRoles( rolesStr.replaceAll("^\\s+","").replaceAll("\\s+$","").split("\\s*,\\s*") ); 
				}
				usersRet.add(user);
			}
			for ( int i = 0 ; i < constraints.getLength(); i++) {
				SecurityConstraint securityConstraint = new SecurityConstraint();
				Element element = (Element)constraints.item(i);	
				if (element.hasAttribute("authorizedRoles")){
					securityConstraint.setAuthorizedRoles( element.getAttribute("authorizedRoles").replaceAll("^\\s+","").replaceAll("\\s+$","").split("\\s*,\\s*") ); 
				}
				if (element.hasAttribute("id"))
					securityConstraint.setId(element.getAttribute("id"));	
				if (element.hasAttribute("description"))
					securityConstraint.setDescription(element.getAttribute("description"));					
				NodeList ApplicationGroups = element.getElementsByTagName("application-groups");
				NodeList Applications = element.getElementsByTagName("applications");					
				ArrayList <String> ApplicationGroupsRet=new ArrayList<String>();
				ArrayList <String> ApplicationsRet=new ArrayList<String>();
				for ( int j = 0 ; j < ApplicationGroups.getLength(); j++) {
					Element element2 = (Element)ApplicationGroups.item(j);
					String appGrStr=element2.getTextContent();
					String[] appGrArr=appGrStr.replaceAll("^\\s+","").replaceAll("\\s+$","").split("\\s*,\\s*");
					for (int k=0; k<appGrArr.length; k++){
						if (!ApplicationGroupsRet.contains(appGrArr[k]))
							ApplicationGroupsRet.add(appGrArr[k]);
					}
				}
				for ( int j = 0 ; j < Applications.getLength(); j++) {
					Element element2 = (Element)Applications.item(j);
					String appStr=element2.getTextContent();
					String[] appArr=appStr.replaceAll("^\\s+","").replaceAll("\\s+$","").split("\\s*,\\s*");
					for (int k=0; k<appArr.length; k++){
						if (!ApplicationsRet.contains(appArr[k]))
							ApplicationsRet.add(appArr[k]);
					}
				}				
				securityConstraint.setApplicationGroups(ApplicationGroupsRet.toArray(new String[0]));				
				securityConstraint.setApplications(ApplicationsRet.toArray(new String[0]));
				constrRet.add(securityConstraint);
			}
			securityConfiguration.setUsers( usersRet.toArray(new User[0]) );
			securityConfiguration.setSecurityConstraints( constrRet.toArray(new SecurityConstraint[0]) );
		} else {
			throw new Exception ("Security file don't exists."); //"Security file don't exists."
		}
		SecurityConfiguration[] ret={securityConfiguration};
		return ret;
	}

	@Override
	public void setPassword(String userId, String oldPasswd, String newPasswd) throws Exception {
		File xmlFile=new File(xmlFileName);
		if (xmlFile.exists())	{
			Document doc = DesktopXmlUtils.getDocumentFromFile(xmlFile);
			DesktopXmlUtils.escribeDOMEnDisco(doc, xmlFile+".last");
			NodeList usersNodes = doc.getDocumentElement().getElementsByTagName("user");
			ArrayList<Element> usersNodesToDelete=new ArrayList<Element>();			
			for (int i=0; i<usersNodes.getLength(); i++){
				Element element = (Element)usersNodes.item(i);
				if (element.hasAttribute("id") && element.getAttribute("id").equals(userId)){
					String passwdHash="";
					String passwd="";					
					if (element.hasAttribute("passwordHash"))
						passwdHash=element.getAttribute("passwordHash");	
					if (element.hasAttribute("password"))
						passwd=element.getAttribute("password");	
					if (!passwdHash.equals("") || !passwd.equals("")){ //Si ya habia password
						boolean noAuthError=SecurityUtils.comparePasswords(oldPasswd, passwd, passwdHash);
						if (!noAuthError){
							//ResourcesService.getResourceBundle("drExplorer",sesion).getString("drExplorer.txt43");
							//throw new Exception("No se ha podido salvar la informaci�n. El password no coincide para el siguiente usuario: "+usuario.getId());
							throw new Exception("Information not saved. Password do not match for user"+": "+userId); //"Information not saved. Password do not match for user: "
						}
					}
					aplicaValorAElement(element,"passwordHash",SecurityUtils.generateHash(newPasswd));
					if (element.hasAttribute("password"))
						element.removeAttribute("password");
				}
			}
			DesktopXmlUtils.escribeDOMEnDisco(doc, xmlFileName);
		}
	}	
	
	
	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader#setObjects(java.lang.Object[])
	 */
	@Override
	public void setObjects(Object[] objects) throws Exception { // Salva "respetuosamente" respetando lo que haya a�adido en los tags
		File xmlFile=new File(xmlFileName);
		SecurityConfiguration securityConfiguration	=(SecurityConfiguration)objects[0];
		if (xmlFile.exists())	{
			User[] users=securityConfiguration.getUsers();
			HashMap<String, User> userHashMap= new HashMap<String, User>();
			for (int i=0; i<users.length; i++){
				userHashMap.put(users[i].getId(), users[i]);
			}
			Document doc = DesktopXmlUtils.getDocumentFromFile(xmlFile);
			DesktopXmlUtils.escribeDOMEnDisco(doc, xmlFile+".last");
			//USUARIOS ----------------------------------------------			
			//Se podr�a pensar en transmitir solo las modificaciones y no toda la informaci�n. TO-DO [ ]
			//modifico los usuarios que ya existian y quito los usuarios que no existan
			NodeList usersNodes = doc.getDocumentElement().getElementsByTagName("user");
			ArrayList<Element> usersNodesToDelete=new ArrayList<Element>();			
			for (int i=0; i<usersNodes.getLength(); i++){
				Element element = (Element)usersNodes.item(i);
				if (element.hasAttribute("id")){
					String idXmlNode=element.getAttribute("id");
					if (userHashMap.containsKey(idXmlNode)){ //Si el nodo estaba ya ...
						User usuario = userHashMap.get(idXmlNode);
						if (!userHashMap.get(idXmlNode).isRefresh()){//... y no tengo que refrescar los valores
							//Si hay un cambio en el password verifico la seguridad
							String newPasswd=usuario.getNewPassword();
							if(newPasswd!=null && !newPasswd.equals("")){ //Quiero poner un password
								String passwdHash="";
								String passwd="";
								if (element.hasAttribute("passwordHash"))
									passwdHash=element.getAttribute("passwordHash");
								if (element.hasAttribute("password"))
									passwd=element.getAttribute("password");
								if (!passwdHash.equals("") || !passwd.equals("")){ //Si ya habia password
									boolean noAuthError=SecurityUtils.comparePasswords(usuario.getPassword(), passwd, passwdHash);
									if (!noAuthError){
										throw new Exception("Information not saved. Password do not match for user"+": "+usuario.getId());  //"No se ha podido salvar la informaci�n. El password no coincide para el siguiente usuario: "
									}
								}
								aplicaValorAElement(element,"passwordHash",SecurityUtils.generateHash(newPasswd));
								if (element.hasAttribute("password"))
									element.removeAttribute("password");
							}
							aplicaValorAElement(element,"name",usuario.getName());
							aplicaValorAElement(element,"surname",usuario.getSurname());
							aplicaValorAElement(element,"motherSurname",usuario.getMotherSurname());
							aplicaValorAElement(element,"roles",joinArray(usuario.getRoles()));
							aplicaValorAElement(element,"fileSystemRoot",usuario.getFileSystemRoot());
							userHashMap.remove(idXmlNode);
						} else	{ //Si tengo que refrescar elimino pero lo dejo para que sea creado nuevo luego
							usersNodesToDelete.add(element);
						}
					} else { //elimino
						//element.getParentNode().removeChild(element);
						usersNodesToDelete.add(element);
						userHashMap.remove(idXmlNode);
					}
					
				}
			}
			//Elimino
			for (int i=0; i<usersNodesToDelete.size(); i++){
				Element element=usersNodesToDelete.get(i);
				element.getParentNode().removeChild(element);
			}
			//Se crean los usuarios nuevos
			Element userGroupElem = (Element)doc.getDocumentElement().getElementsByTagName("users").item(0);
			if (userHashMap.size()>0){
				Iterator <String>itKeys=userHashMap.keySet().iterator();
				while (itKeys.hasNext()){
					String idXmlNode=itKeys.next();
					User usuario = userHashMap.get(idXmlNode);
					Element newUser=doc.createElement("user");
					aplicaValorAElement(newUser,"id",usuario.getId());					
					aplicaValorAElement(newUser,"name",usuario.getName());
					aplicaValorAElement(newUser,"surname",usuario.getSurname());
					aplicaValorAElement(newUser,"motherSurname",usuario.getMotherSurname());
					aplicaValorAElement(newUser,"roles",joinArray(usuario.getRoles()));
					aplicaValorAElement(newUser,"fileSystemRoot",usuario.getFileSystemRoot());
					String newPasswd=usuario.getNewPassword();
					if (newPasswd!=null && newPasswd.length()>0){
						aplicaValorAElement(newUser,"passwordHash",SecurityUtils.generateHash(newPasswd));
					}
					userGroupElem.appendChild(newUser);
				}
			}
			//RESTRICCIONES ----------------------------------------------			
			SecurityConstraint[] securityConstraints=securityConfiguration.getSecurityConstraints();						
			HashMap<String, SecurityConstraint> constrHashMap= new HashMap<String, SecurityConstraint>();
			for (int i=0; i<securityConstraints.length; i++){
				constrHashMap.put(securityConstraints[i].getId(), securityConstraints[i]);
			}
			//modifico las restricciones que ya existian y quito las que se han eliminado
			NodeList constrNodes = doc.getDocumentElement().getElementsByTagName("constraint");
			ArrayList<Element> constrNodesToDelete=new ArrayList<Element>();
			for (int i=0; i<constrNodes.getLength(); i++){
				Element element = (Element)constrNodes.item(i);
				if (element.hasAttribute("id")){
					String idXmlNode=element.getAttribute("id");
					if (constrHashMap.containsKey(idXmlNode)){ //modifico
						SecurityConstraint constr = constrHashMap.get(idXmlNode);
						aplicaValorAElement(element,"description",constr.getDescription());						
						aplicaValorAElement(element,"authorizedRoles",joinArray(constr.getAuthorizedRoles()));
						NodeList appNodes = element.getElementsByTagName("applications");
						NodeList appGrpNodes = element.getElementsByTagName("application-groups");
						for (int j=0; j<appNodes.getLength(); j++){
							appNodes.item(j).getParentNode().removeChild(appNodes.item(j));
						}
						for (int j=0; j<appGrpNodes.getLength(); j++){
							appGrpNodes.item(j).getParentNode().removeChild(appGrpNodes.item(j));
						}
						element.setTextContent(null);
						if (constr.getApplicationGroups()!=null && constr.getApplicationGroups().length>0){
							Element appgroupElem=doc.createElement("application-groups");
							appgroupElem.setTextContent( joinArray(constr.getApplicationGroups()) );
							element.appendChild(appgroupElem);
						}
						if (constr.getApplications()!=null && constr.getApplications().length>0){
							Element appElem=doc.createElement("applications");
							appElem.setTextContent( joinArray(constr.getApplications()) );
							element.appendChild(appElem);
						}						
					} else {
						//element.getParentNode().removeChild(element);
						constrNodesToDelete.add(element);
					}
					constrHashMap.remove(idXmlNode);
				}
			}
			//Elimino
			for (int i=0; i<constrNodesToDelete.size(); i++){
				Element element=constrNodesToDelete.get(i);
				element.getParentNode().removeChild(element);
			}
			//A�ado las restricciones nuevas
			Element securityConstrElem = (Element)doc.getDocumentElement().getElementsByTagName("security-constraints").item(0);
			if (constrHashMap.size()>0){
				Iterator <String>itKeys=constrHashMap.keySet().iterator();
				while (itKeys.hasNext()){
					String idXmlNode=itKeys.next();

					SecurityConstraint constr=constrHashMap.get(idXmlNode);
					Element newConstr=doc.createElement("constraint");
					aplicaValorAElement(newConstr,"id",constr.getId());
					aplicaValorAElement(newConstr,"description",constr.getDescription());
					String[] roles=constr.getAuthorizedRoles();
					if (roles!=null && roles.length>0){
						aplicaValorAElement(newConstr,"authorizedRoles",join(roles,", "));
					}
					String[] appGrp=constr.getApplicationGroups();
					if (appGrp!=null && appGrp.length>0){
						Element appGrpElem=doc.createElement("application-groups");
						appGrpElem.setTextContent(join(appGrp,", "));
						newConstr.appendChild(appGrpElem);
					}					
					String[] app=constr.getApplications();
					if (app!=null && app.length>0){
						Element appElem=doc.createElement("applications");
						appElem.setTextContent(join(app,", "));
						newConstr.appendChild(appElem);
					}
					securityConstrElem.appendChild(newConstr);
				}
			}
			//salvo el fichero
			//DesktopWriter.escribeDOMEnDisco(doc, xmlFile+".tmp");
			DesktopXmlUtils.escribeDOMEnDisco(doc, xmlFileName);
		} else {
			throw new Exception ("Security file don't exists."); //"Security file don't exists."
		}
	}

	private String joinArray(String[] array) {
		if (array==null)
			return null;
		StringBuffer ret=new StringBuffer();
		if (array.length>0)
			ret.append(array[0]);
		for (int i=1; i<array.length; i++)
			ret.append(", "+array[i]);
		return ret.toString();
	}

	private void aplicaValorAElement(Element element, String atrName, String value) {
		if (value==null || value.equals("")){
			if (element.hasAttribute(atrName))
				element.removeAttribute(atrName);
		} else	{
			element.setAttribute(atrName, value);
		}
	}
	
	private String join(String[] strings, String separator) {
	    StringBuffer sb = new StringBuffer();
	    for (int i=0; i < strings.length; i++) {
	        if (i != 0) sb.append(separator);
	  	    sb.append(strings[i]);
	  	}
	  	return sb.toString();
	}

	@Override
	public void addObjects(Object[] objects) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
