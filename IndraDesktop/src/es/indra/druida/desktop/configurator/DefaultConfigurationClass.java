package es.indra.druida.desktop.configurator;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import es.indra.druida.desktop.configurator.beans.AppGroup;
import es.indra.druida.desktop.configurator.beans.Application;
import es.indra.druida.desktop.utils.DesktopFileUtils;
import es.indra.druida.desktop.utils.DesktopXmlUtils;
import es.indra.druida.desktop.utils.SecurityUtils;

/**
 * The Class DefaultConfigurationClass. 
 * Esta clase 
 * 	- redirige a login.html si no vienen datos ni esta logado el usuario
 *  - si no esta logado pero vienen datos inicia la sesion de usuario verificando con el fichero xml de seguridad y creando el usuario si es necesario. luego va a xx_index.jsp  
 *  - si el usuario est� logado redirige a xx_index.jsp
 * 	
 */
public class DefaultConfigurationClass extends AbstractDesktopConfigurationClass {

	private boolean authenticatingError=false;
	

	/**
	 * Instantiates a new default configuration class.
	 * 
	 * @param req
	 *            the req
	 * @param res
	 *            the res
	 */
	public DefaultConfigurationClass(HttpServletRequest req, HttpServletResponse res) {
		super(req, res);		
	}

	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.AbstractDesktopConfigurationClass#getLoginID(javax.servlet.http.HttpServletRequest)
	 */
	public String getLoginID(HttpServletRequest request) {
		String login=(String)request.getParameter("login");
		if (login=="") 
			login="usuario";
		return login;
	}
	
	public String getLoginPassword(HttpServletRequest request) {
		String passwd=(String)request.getParameter("password");
		if (passwd!=null)
			passwd=passwd.replaceAll("^\\s*","").replaceAll("\\s*$","");
		return passwd;
	}	
	
	
	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.AbstractDesktopConfigurationClass#createUser()
	 */
	public DesktopUserConfig createUser() throws Exception { //creates a user using request
		setAuthenticatingError(false);
		
		String loginId=getLoginID(getRequest());
		String loginPassword=getLoginPassword(getRequest());		
		
		//Obtenemos los datos b�sicos id, name, surname, motherSurname
		DesktopUserConfig userConfig= new DesktopUserConfig();
		userConfig.setId(loginId);
		
		//Validamos el password y cargamos los datos del usuario
		loadUserData(userConfig, loginId, true, loginPassword); //Puede llamar a setAuthenticatingError(true)
		if(isAuthenticatingError())
			return null;
		return userConfig;
	}	
	
	public void reloadUser() throws Exception { //reloads user data
		DesktopUserConfig userConfig=getUserObject();
		String loginId=userConfig.getId();
		//recargamos los datos del usuario
		loadUserData(userConfig, loginId, false, null); //Puede llamar a setAuthenticatingError(true)
	}
	
	private void loadUserData(DesktopUserConfig userConfig, String loginId, Boolean validarPassw, String loginPassword) throws Exception {
		//		Preparamos las variables
		DesktopConfig desktopConfig = DesktopConfig.getInstance();
		Application[] todasLasAplic = desktopConfig.getApplications();
		AppGroup[] gruposApl= desktopConfig.getAppGroups();

		//Cargo la informaci�n de seguridad del usuario ---------------------------------
		// leyendo primero el filesystemroot y los roles
		// luego proceso las restricciones cargandolas en el hashmap
		// lo comparo con las aplicaciones y asocio al usuario la lista de aplicaciones accesibles.
		File securityFile=new File(desktopConfig.get("securityFile"));
		HashMap<String, String[]> restricciones= new HashMap<String, String[]>();	
		
		if (securityFile.exists())	{
			//Primero busco los datos asociados al usuario 
			//(roles, nombre y apellidos, filesystemroot)
			Document doc = DesktopXmlUtils.getDocumentFromFile(securityFile);
			Element users= DesktopXmlUtils.getFirstChildWithTagName(doc.getDocumentElement(),"users");
			if (users!=null){		
				Element user=DesktopXmlUtils.getFirstChildWithAttribute(users,"id",loginId);
				if (user!=null)	{
					
					if (validarPassw && (user.hasAttribute("password") || user.hasAttribute("passwordHash"))){
						String password=user.getAttribute("password");
						String passwordHash=user.getAttribute("passwordHash");
						boolean noAuthError=SecurityUtils.comparePasswords(loginPassword, password, passwordHash);
						if (!noAuthError){
							setAuthenticatingError(true);
							return;
						}
					}					
					if (user.hasAttribute("roles")){
						String[]rolesArr=divideCadenaDeComas(user.getAttribute("roles"));
						userConfig.setRoles(rolesArr);
					}
					if (user.hasAttribute("fileSystemRoot")){
						userConfig.setFileSystemRoot(user.getAttribute("fileSystemRoot"));
					}
					if (user.hasAttribute("name")){
						userConfig.setName(user.getAttribute("name"));
					} else {
						userConfig.setName(loginId);
					}
					if (user.hasAttribute("surname")){
						userConfig.setSurname(user.getAttribute("surname"));
					} else {
						userConfig.setSurname("");	
					}
					if (user.hasAttribute("motherSurname")){
						userConfig.setMotherSurname(user.getAttribute("motherSurname"));
					} else {
						userConfig.setMotherSurname("");
					}
					if (user.hasAttribute("email")){
						userConfig.setEmail(user.getAttribute("email"));
					} else {
						userConfig.setEmail("");
					}
					if (user.hasAttribute("city")){
						userConfig.setCity(user.getAttribute("city"));
					} else {
						userConfig.setCity("");
					}					
				} else{ //El usuario no existe
					setAuthenticatingError(true);
					return;
				}
			}
			//Luego proceso las restricciones
			Element securityConstraints= DesktopXmlUtils.getFirstChildWithTagName(doc.getDocumentElement(),"security-constraints");
			if (securityConstraints!=null){			
				NodeList xmlConstraints = securityConstraints.getElementsByTagName("constraint");
				for (int i=0; i<xmlConstraints.getLength(); i++){
					Element xmlConstraint=(Element) xmlConstraints.item(i);
					//Resuelvo la "constraint"
					String[] roles=divideCadenaDeComas(xmlConstraint.getAttribute("authorizedRoles"));
					String[] applications=resuelveAplicacionesConstraint(todasLasAplic,gruposApl,xmlConstraint);
					for (int n=0; n<applications.length; n++){ //A�ado al hashmap aplicacion --> rolesQueLaAcceden[]
						if (restricciones.containsKey(applications[n]))	{
							String[] rolesAnt=(String[]) restricciones.get(applications[n]);
							String[] mergedArr= mergeArrays(rolesAnt,roles);
							restricciones.put(applications[n],mergedArr);
						}else{
							restricciones.put(applications[n],roles);
						}
					}
				}
			}
		}
		// Me quedo con los nombres de las aplicaciones que no salen en ninguna restricci�n
		// Para cada aplicacion prohibida, si tengo un rol que me permita segun alguna regla verla la a�ado
		ArrayList<String> rolesUsuArrL=new ArrayList<String>();//userConfig.getRoles();
		String[] rolesUsuArr=userConfig.getRoles();
		if (rolesUsuArr!=null){
			for (int i=0; i<rolesUsuArr.length; i++){
				rolesUsuArrL.add(rolesUsuArr[i]);
			}
		}
		//TODO: que sea un array de applications y no de Strings
		ArrayList <Application>aplAccesibles=new ArrayList<Application>();
		for (int a=0;a<todasLasAplic.length; a++){
			String idAplic = todasLasAplic[a].getId();
			if (restricciones.containsKey(idAplic)){
				String[] rolesPermitidos=restricciones.get(idAplic);
				if(elementoPasaRestriccion(rolesPermitidos,rolesUsuArrL))
					aplAccesibles.add(todasLasAplic[a]);
			}else{
				aplAccesibles.add(todasLasAplic[a]);
			}			
		}
		userConfig.setApplications(aplAccesibles.toArray(new Application[aplAccesibles.size()]));
	}
	
	
	
	
	

	/*
	private void resuelveI18NAplicaciones(Application[] aplicaciones) {
		//ResourceBundle bundle=ResourcesService.getResourceBundle("xxx",request); //bundle.getString("xxxx.txt1")
		for (int i=0; i<aplicaciones.length; i++){
			Application app=aplicaciones[i];
			app.setTitle(ResourcesService.replaceI18NValues(app.getTitle(), getReq()));
		}
	}
	*/

	/**
	 * Resuelve aplicaciones constraint.
	 * 
	 * @param todasLasAplic
	 *            todas las aplic
	 * @param gruposApl
	 *            grupos apl
	 * @param xmlConstraint
	 *            xml constraint
	 * 
	 * @return the string[]
	 */
	private String[] resuelveAplicacionesConstraint(Application[] todasLasAplic, AppGroup[] gruposApl, Element xmlConstraint) {
		ArrayList <String> ret=new ArrayList<String>();
		NodeList appNodes= xmlConstraint.getElementsByTagName("applications");
		for (int i=0; i<appNodes.getLength(); i++){
			Element appNode=(Element)appNodes.item(i);
			String [] appInElement=divideCadenaDeComas(appNode.getTextContent());
			for (int n=0; n<appInElement.length; n++){
				if (!ret.contains(appInElement[n]))
					ret.add(appInElement[n]); 
			}
		}
		NodeList appGroupsNodes= xmlConstraint.getElementsByTagName("application-groups");		
		for (int i=0; i<appGroupsNodes.getLength(); i++){
			Element appGroupsNode=(Element)appGroupsNodes.item(i);
			String [] appGrpInElement=divideCadenaDeComas(appGroupsNode.getTextContent());
			//Resolvemos cada grupo ...
			for (int n=0; n<appGrpInElement.length; n++){
				AppGroup grupo=buscaGrupoConNombre(appGrpInElement[n],gruposApl);
				if (grupo!=null){
					for (int a=0; a<grupo.getApplications().length; a++){
						String aplic=grupo.getApplications()[a];
						if (!ret.contains(aplic))
							ret.add(aplic); 
					}
				}
			}			
		}		
		return ret.toArray(new String[ret.size()]);
	}	
	

	private HttpServletRequest req;
	private HttpServletResponse resp;

	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.AbstractDesktopConfigurationClass#processUser(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void processUser (HttpServletRequest request, HttpServletResponse response) throws Exception {
		setReq(request);
		setResp(response);
		boolean heRecibidoUnLogin=request.getParameter("logar")!=null;
		if (heRecibidoUnLogin){
			DesktopUserConfig newUser=createUser(); //esto establece authenticatingError
			if (isAuthenticatingError()){
				response.sendRedirect("login.html?authFailed=S");
			}else{
				setUserObject(newUser);
				generateUserFilesIfNecesary(); //Creo el directorio del usuario si no existe   ---------------------------------			
				response.sendRedirect("DrDesktop_index.jsp");
			}
		}else if (isUserLogged()){
			response.sendRedirect("DrDesktop_index.jsp");
		} else	{//Si no he recibido un login y tampoco estoy en sesi�n pido login
			response.sendRedirect("login.html"); //pido usuario
		}
	}

	/*
	private boolean userAuthenticates() {
		HttpServletRequest req=getRequest();
		boolean ret=true;
		return ret;
	}*/
	
	private void generateUserFilesIfNecesary() throws Exception {
		DesktopConfig desktopConfig = DesktopConfig.getInstance();
		String dirUsuario=desktopConfig.get("userRoot")+"/"+getUserObject().getId();
		File fDirUsuario = new File(dirUsuario);
		if (!fDirUsuario.exists())	{
			File fDirNewUser = new File(desktopConfig.get("root")+"/WEB-INF/templates/newUser");
			DesktopFileUtils.copyDirectory(fDirNewUser,fDirUsuario);
		}
	}

	private void setUserDataFromXml() throws Exception {
		
		
	}

	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.AbstractDesktopConfigurationClass#connectUser()
	 */
	@Override
	public void connectUser() {
		
		
	}

	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.AbstractDesktopConfigurationClass#disconnectUser()
	 */
	@Override
	public void disconnectUser() {
		// TODO Auto-generated method stub
		
	}

	
	
	/**
	 * Busca grupo con nombre.
	 * 
	 * @param nombre
	 *            the nombre
	 * @param gruposApl
	 *            the grupos apl
	 * 
	 * @return the app group
	 */
	private AppGroup buscaGrupoConNombre(String nombre, AppGroup[] gruposApl) {
		for (int i=0; i<gruposApl.length; i++){
			String name= gruposApl[i].getName();
			if (name!=null && name.equals(nombre))
				return gruposApl[i];
		}
		return null;
	}
	
	/**
	 * Divide cadena de comas.
	 * 
	 * @param cadena
	 *            the cadena
	 * 
	 * @return the string[]
	 */
	private String[]divideCadenaDeComas(String cadena)	{
		if (cadena==null)
			return new String[0];
		return cadena.replaceAll("^\\s*","").replaceAll("\\s*$","").split("\\s*,\\s*");
	}
	
	/**
	 * Merge arrays.
	 * 
	 * @param arr1
	 *            the arr1
	 * @param arr2
	 *            the arr2
	 * 
	 * @return the string[]
	 */
	private String[] mergeArrays(String[] arr1, String[] arr2) {
		ArrayList<String> ret=new ArrayList<String>();
		for (int i=0; i<arr1.length; i++){
			if (!ret.contains(arr1[i]))
				ret.add(arr1[i]);
		}
		for (int i=0; i<arr2.length; i++){
			if (!ret.contains(arr2[i]))
				ret.add(arr2[i]);
		}		
		return ret.toArray(new String[ret.size()]);
	}	
	
	/**
	 * Elemento pasa restriccion.
	 * 
	 * @param rolesPermitidos
	 *            the roles permitidos
	 * @param rolesUsuArrL
	 *            the roles usu arr l
	 * 
	 * @return true, if successful
	 */
	private boolean elementoPasaRestriccion(String[] rolesPermitidos, ArrayList<String> rolesUsuArrL) {
		for (int i=0; i<rolesPermitidos.length; i++){
			if (rolesUsuArrL.contains(rolesPermitidos[i]))
				return true;
		}
		return false;
	}

	public boolean isAuthenticatingError() {
		return authenticatingError;
	}

	public void setAuthenticatingError(boolean authenticatingError) {
		this.authenticatingError = authenticatingError;
	}

	public HttpServletRequest getReq() {
		return req;
	}

	public void setReq(HttpServletRequest req) {
		this.req = req;
	}

	public HttpServletResponse getResp() {
		return resp;
	}

	public void setResp(HttpServletResponse resp) {
		this.resp = resp;
	}
	
	
}
