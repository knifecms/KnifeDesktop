package es.indra.druida.desktop.configurator.beans;

import es.indra.druida.desktop.utils.DesktopWriter;

/**
 * The Class Application.
 */
public class Application extends WindowBase {

	/** The group. */
	private String group; 
	
	/** The group Id. */
	private String groupId; 	
	
	/** The daemons. */
	private Daemon[] daemons;
	
	/** The parameters. */
	private AppParameter[] parameters;
	
	/** The icon. */
	private String icon;
	
	/** The icon text. */
	private String iconText;	
	
	/** The engine. */
	private String engine;	

	/*public Application(String id, String group, boolean widget, Daemon[] daemons, AppParameter[] parameters) {
		super();
		this.id=id;
		this.group=group;
		this.widget=widget;
		this.daemons=daemons;
		this.parameters=parameters;
	}*/

	/**
	 * Instantiates a new application.
	 */
	public Application() {
		super();
	}
	
	/**
	 * Clone an application
	 */
	/*
	public Application(Application app) {
		super(app);
		setGroup(app.getGroup());
		setDaemons(app.getDaemons());
		setParameters(app.getParameters());
		setIcon(app.getIcon());
		setIconText(app.getIconText());
		setEngine(app.getEngine());
	}*/	

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		
		String str=DesktopWriter.generaCadenaJavascript(id)+":";
		str += "{"
		+"\"grupo\":"+group;
		if (widget)
			str +=",\"widget\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(widget));
		if (title!=null && title.length()>0)
			str +=",\"title\":"+DesktopWriter.generaCadenaJavascript(title);
		if (url!=null && url.length()>0)
			str +=",\"url\":"+DesktopWriter.generaCadenaJavascript(url);
		if (relativeUrl!=null && relativeUrl.length()>0)
			str +=",\"relativeUrl\":"+DesktopWriter.generaCadenaJavascript(relativeUrl);		
		if (width!=null && width.length()>0)
			str +=",\"width\":"+DesktopWriter.generaCadenaJavascript(width);
		if (height!=null && height.length()>0)		
			str +=",\"height\":"+DesktopWriter.generaCadenaJavascript(height);
		
		str += ",\"position\":"+DesktopWriter.generaCadenaJavascript(position)
		+",\"icon\":"+DesktopWriter.generaCadenaJavascript(icon);
		if (iconText!=null && iconText.length()>0)
			str +=",\"iconText\":"+DesktopWriter.generaCadenaJavascript(iconText);
		str +=",\"single\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(single))
		+",\"ignoreMargins\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(ignoreMargins))
		+",\"sessionSave\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(sessionSave))
		+",\"showInAllDesktops\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(showInAllDesktops))
		+",\"noResize\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(noResize));
		if (status!=null && status.length()>0)
			str += ",\"status\":"+DesktopWriter.generaCadenaJavascript(status);
		if (engine!=null && engine.length()>0)
			str += ",\"engine\":"+DesktopWriter.generaCadenaJavascript(engine);
		if (helpUrl!=null && helpUrl.length()>0)
			str += ",\"helpUrl\":"+DesktopWriter.generaCadenaJavascript(helpUrl);
		if (relativeHelpUrl!=null && relativeHelpUrl.length()>0)
			str += ",\"relativeHelpUrl\":"+DesktopWriter.generaCadenaJavascript(relativeHelpUrl);
		if (js!=null && js.length()>0)
			str +=",\"js\":"+DesktopWriter.generaCadenaJavascript(js);
		
		if (description!=null && description.length()>0)
		str += ",\"description\":"+DesktopWriter.generaCadenaJavascript(description);
		
		if (innerWidth!=null && innerWidth.length()>0)	
			str +=",\"innerWidth\":"+DesktopWriter.generaCadenaJavascript(innerWidth);
		if (innerHeight!=null && innerHeight.length()>0)			
			str +=",\"innerHeight\":"+DesktopWriter.generaCadenaJavascript(innerHeight);
		if (zIndex!=null && zIndex.length()>0)		
			str +=",\"zIndex\":"+DesktopWriter.generaCadenaJavascript(zIndex);
		
		////////////////////////////
		// String
		if (onClose!=null && onClose.length()>0)
			str += ",\"onClose\":"+DesktopWriter.generaCadenaJavascript(onClose);
		if (chromeType!=null && chromeType.length()>0)
			str += ",\"chromeType\":"+DesktopWriter.generaCadenaJavascript(chromeType);
		if (testigo!=null && testigo.length()>0)
			str += ",\"testigo\":"+DesktopWriter.generaCadenaJavascript(testigo);
		if (testigoJson!=null && testigoJson.length()>0)
			str += ",\"testigoJson\":"+DesktopWriter.generaCadenaJavascript(testigoJson);
		if (skin!=null && skin.length()>0)
			str += ",\"skin\":"+DesktopWriter.generaCadenaJavascript(skin);
		if (imageFamily!=null && imageFamily.length()>0)
			str += ",\"imageFamily\":"+DesktopWriter.generaCadenaJavascript(imageFamily);
		if (virtualDesktop!=null && virtualDesktop.length()>0)
			str += ",\"virtualDesktop\":"+DesktopWriter.generaCadenaJavascript(virtualDesktop);
		
		
		// boolean
		if (noTaskbar)
			str +=",\"noTaskbar\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(noTaskbar));
		if (noHide)
			str +=",\"noHide\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(noHide));
		if (noDrag)
			str +=",\"noDrag\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(noDrag));

		
		///////////////////////////////
		// daemons
		if (daemons != null && daemons.length>0){
			str +=",\"daemons\":[";
			int longDaemons = daemons.length;
			for (int i=0;i<longDaemons;i++){
				Daemon daem=daemons[i];
				str += daem.toString();
				if (i!=(longDaemons-1))
					str+= ",";
			}
			str +="]";
		}
		
		// app params
		if (parameters != null && parameters.length>0){
			str +=",\"parameters\":[";
			int longAppParams = parameters.length;
			for (int i=0;i<longAppParams;i++){
				AppParameter appPar=parameters[i];
				str += appPar.toString();
				if (i!=(longAppParams-1))
					str+= ",";
			}
			str +="]";
		}
		
		str += "}";
	
		return str;
	}

	/**
	 * Gets the daemons.
	 * 
	 * @return the daemons
	 */
	public Daemon[] getDaemons() {
		return daemons;
	}

	/**
	 * Sets the daemons.
	 * 
	 * @param daemons
	 *            the new daemons
	 */
	public void setDaemons(Daemon[] daemons) {
		this.daemons = daemons;
	}

	/**
	 * Gets the group.
	 * 
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}

	/**
	 * Sets the group.
	 * 
	 * @param group
	 *            the new group
	 */
	public void setGroup(String group) {
		this.group = group;
	}

	/**
	 * Gets the parameters.
	 * 
	 * @return the parameters
	 */
	public AppParameter[] getParameters() {
		return parameters;
	}

	/**
	 * Sets the parameters.
	 * 
	 * @param parameters
	 *            the new parameters
	 */
	public void setParameters(AppParameter[] parameters) {
		this.parameters = parameters;
	}
	
	/**
	 * Gets the icon.
	 * 
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}


	/**
	 * Sets the icon.
	 * 
	 * @param icon
	 *            the new icon
	 */
	public void setIcon(String icon) {
		this.icon = icon;
		if (this.icon==null)
			this.icon="iconNULL";
	}


	/**
	 * Gets the icon text.
	 * 
	 * @return the icon text
	 */
	public String getIconText() {
		return iconText;
	}


	/**
	 * Sets the icon text.
	 * 
	 * @param iconText
	 *            the new icon text
	 */
	public void setIconText(String iconText) {
		this.iconText = iconText;
		if (this.iconText==null)
			this.iconText="No description";
	}
	
	/**
	 * Gets the engine.
	 * 
	 * @return the engine
	 */
	public String getEngine() {
		return engine;
	}

	/**
	 * Sets the engine.
	 * 
	 * @param engine
	 *            the new engine
	 */
	public void setEngine(String engine) {
		this.engine = engine;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}	
	
}
