package es.indra.druida.desktop.configurator.beans;

import es.indra.druida.desktop.utils.DesktopWriter;

/**
 * The Class SkinParameter.
 */
public class SkinParameter extends Parameter{
	
	/** The posible values. */
	private String posibleValues;
	
	/** The no editable. */
	private String noEditable;
	
	/** The description. */
	private String description;
	
	/** The posible value descriptions. */
	private String posibleValueDescriptions;
	
	/** The type. */
	private String type;	
	
	/**
	 * Instantiates a new skin parameter.
	 * 
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 */
	public SkinParameter(String name, String value){
		super(name,value);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return DesktopWriter.generaCadenaJavascript(name)+":"+DesktopWriter.generaCadenaJavascript(value);
	}
	
	/**
	 * Gets the posible values.
	 * 
	 * @return the posible values
	 */
	public String getPosibleValues() {
		return posibleValues;
	}
	
	/**
	 * Sets the posible values.
	 * 
	 * @param posibleValues
	 *            the new posible values
	 */
	public void setPosibleValues(String posibleValues) {
		this.posibleValues = posibleValues;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the no editable.
	 * 
	 * @return the no editable
	 */
	public String getNoEditable() {
		return noEditable;
	}

	/**
	 * Sets the no editable.
	 * 
	 * @param noEditable
	 *            the new no editable
	 */
	public void setNoEditable(String noEditable) {
		this.noEditable = noEditable;
	}

	/**
	 * Gets the posible value descriptions.
	 * 
	 * @return the posible value descriptions
	 */
	public String getPosibleValueDescriptions() {
		return posibleValueDescriptions;
	}

	/**
	 * Sets the posible value descriptions.
	 * 
	 * @param posibleValueDescriptions
	 *            the new posible value descriptions
	 */
	public void setPosibleValueDescriptions(String posibleValueDescriptions) {
		this.posibleValueDescriptions = posibleValueDescriptions;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
}
