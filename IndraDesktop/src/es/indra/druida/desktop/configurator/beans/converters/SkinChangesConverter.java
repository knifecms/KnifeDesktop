package es.indra.druida.desktop.configurator.beans.converters;

import java.util.Iterator;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import es.indra.druida.desktop.configurator.beans.Parameter;
import es.indra.druida.desktop.configurator.beans.SkinChanges;


/**
 * The Class SkinChangesConverter.
 */
public class SkinChangesConverter {
	
	/**
	 * Json to bean.
	 * 
	 * @param jsonElem
	 *            the json elem
	 * 
	 * @return the skin changes
	 * 
	 * @throws JSONException
	 *             the JSON exception
	 */
	public static SkinChanges jsonToBean (String jsonElem) throws JSONException	{
		if(jsonElem.equals(""))
			return null;
		JSONObject aElem = new JSONObject(jsonElem);
		if (!aElem.has("skin")||aElem.getString("skin")==null)
			return null;
		SkinChanges ret= new SkinChanges();
		ret.setId(aElem.getString("skin"));
		JSONObject aChanges = aElem.getJSONObject("changes");
		Set props= aChanges.getKeySet();
		Parameter[] retParam= new Parameter[props.size()];
		Iterator iterator=props.iterator();
		int i=0;		
	    while (iterator.hasNext()){ 
	        String propName=(String)iterator.next();
	        String propValue=aChanges.getString(propName);
	        retParam[i]=new Parameter(propName,propValue);
	        i++;
	    }
		ret.setParameters(retParam);
		return ret;
	}	
}

