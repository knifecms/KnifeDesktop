package es.indra.druida.desktop.configurator.beans;

/**
 * The Class Parameter.
 */
public class Parameter implements java.io.Serializable {
	
	/** The name. */
	protected String name;
	
	/** The value. */
	protected String value;
	
	/**
	 * Instantiates a new parameter.
	 * 
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 */
	public Parameter(String name, String value){
		super();
		this.name=name;
		this.value=value;
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
