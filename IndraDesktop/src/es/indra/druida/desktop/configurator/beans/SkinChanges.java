package es.indra.druida.desktop.configurator.beans;

/**
 * The Class SkinChanges.
 */
public class SkinChanges {
	//Representa los cambios sobre los valores de parámetros de un skin que queremos guardar
	//crear upperclass para esta y skin ¿?
	/** The id. */
	String id;
	
	/** The parameters. */
	Parameter[] parameters;
	
	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * Gets the parameters.
	 * 
	 * @return the parameters
	 */
	public Parameter[] getParameters() {
		return parameters;
	}
	
	/**
	 * Sets the parameters.
	 * 
	 * @param parameters
	 *            the new parameters
	 */
	public void setParameters(Parameter[] parameters) {
		this.parameters = parameters;
	}
}
