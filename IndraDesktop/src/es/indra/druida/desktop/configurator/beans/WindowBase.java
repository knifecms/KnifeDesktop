package es.indra.druida.desktop.configurator.beans;

import java.util.HashMap;

//import es.indra.druida.desktop.utils.DesktopWriter;

/**
 * The Class WindowBase.
 */
public class WindowBase {

	/** The id. */
	protected String id;
	
	/** The title. */
	protected String title;
	
	/** The url. */
	protected String url;
	
	/** The width. */
	protected String width;
	
	/** The height. */
	protected String height;
	
	/** The position. */
	protected String position;
	
	/** The status. */
	protected String status;
	
	/** The relative url. */
	protected String relativeUrl;
	
	/** The relative help url. */
	protected String relativeHelpUrl;
	
	/** The inner width. */
	protected String innerWidth; //XXX convierto a String porque necesito que pueda valer NULL
	
	/** The inner height. */
	protected String innerHeight;
	
	/** The z index. */
	protected String zIndex;
	
	/** The js. */
	protected String js;
	
	/** The description. */
	protected String description;
	
	/** The help url. */
	protected String helpUrl;	
	
	/** The on close. */
	protected String onClose;
	
	/** The chrome type. */
	protected String chromeType;
	
	/** The testigo. */
	protected String testigo;
	
	/** The testigo json. */
	protected String testigoJson;
	
	/** The skin. */
	protected String skin;
	
	/** The image family. */
	protected String imageFamily;
	
	/** The virtual desktop. */
	protected String virtualDesktop;	
	
	/** The no taskbar. */
	protected boolean noTaskbar;	
	
	/** The no hide. */
	protected boolean noHide;
	
	/** The widget. */
	protected boolean widget;	
	
	/** The no resize. */
	protected boolean noResize;
	
	/** The no drag. */
	protected boolean noDrag;
	
	/** The session save. */
	protected boolean sessionSave;
	
	/** The show in all desktops. */
	protected boolean showInAllDesktops;
	
	/** The single. */
	protected boolean single;
	
	/** The ignore margins. */
	protected boolean ignoreMargins;	
		

	/**
	 * Instantiates a new window base.
	 * 
	 * @param id
	 *            the id
	 * @param widget
	 *            the widget
	 */
	public WindowBase(String id, boolean widget) {
		super();
		this.id=id;
		this.widget=widget;
	}
	
	/**
	 * Instantiates a new window base.
	 * 
	 * @param valores
	 *            the valores
	 */
	public WindowBase(HashMap <String,String> valores) { //Constructor usando un HashMap creado por conveniencia
		setHeight(valores.get("height"));
		setWidth(valores.get("width"));	
		setInnerHeight(valores.get("innerHeight"));
		setInnerWidth(valores.get("innerWidth"));	
		setZIndex(valores.get("zIndex"));			
		//Boolean ---
		setWidget(Boolean.parseBoolean(valores.get("widget")));		
		setIgnoreMargins(Boolean.parseBoolean(valores.get("ignoreMargins")));
		setNoResize(Boolean.parseBoolean(valores.get("noResize")));
		setSessionSave(Boolean.parseBoolean(valores.get("sessionSave")));	
		setShowInAllDesktops(Boolean.parseBoolean(valores.get("showInAllDesktops")));	
		setSingle(Boolean.parseBoolean(valores.get("single")));
		setNoTaskbar(Boolean.parseBoolean(valores.get("noTaskbar")));
		setNoHide(Boolean.parseBoolean(valores.get("noHide")));
		setNoDrag(Boolean.parseBoolean(valores.get("noDrag")));
		//String ---
		setId(valores.get("id"));
		setDescription(valores.get("description"));		
		setJs(valores.get("js"));
		setHelpUrl(valores.get("helpUrl"));
		setRelativeHelpUrl(valores.get("relativeHelpUrl"));
		setPosition(valores.get("position"));
		setStatus(valores.get("status"));
		setTitle(valores.get("title"));	
		setUrl(valores.get("url"));
		setRelativeUrl(valores.get("relativeUrl"));
		setOnClose(valores.get("onClose"));	
		setChromeType(valores.get("chromeType"));	
		setTestigo(valores.get("testigo"));	 
		setTestigoJson(valores.get("testigoJson"));	 
		setSkin(valores.get("skin"));	
		setImageFamily(valores.get("imageFamily"));	
		setVirtualDesktop(valores.get("virtualDesktop"));	
	}	

	/**
	 * Gets the.
	 * 
	 * @return the hash map< string, string>
	 */
	public HashMap <String,String> get()	{ //Prepara un hashmap con todos los valores como strings, creado por conveniencia
		HashMap <String,String> valores= new HashMap <String,String>();
		valores.put("height",getHeight());
		valores.put("width",getWidth());	
		valores.put("innerHeight",getInnerHeight());
		valores.put("innerWidth",getInnerWidth());	
		valores.put("zIndex",getZIndex());			
		//		Boolean ---
		valores.put("widget",""+isWidget());
		valores.put("ignoreMargins",""+ isIgnoreMargins());
		valores.put("noResize",""+isNoResize());
		valores.put("sessionSave",""+isSessionSave());	
		valores.put("showInAllDesktops",""+isShowInAllDesktops());	
		valores.put("single",""+isSingle());
		valores.put("noTaskbar",""+isNoTaskbar());
		valores.put("noHide",""+isNoHide());
		valores.put("noDrag",""+isNoDrag());
		//		String ---
		valores.put("id",getId());
		valores.put("description",getDescription());		
		valores.put("js",getJs());
		valores.put("helpUrl",getHelpUrl());
		valores.put("relativeHelpUrl",getRelativeHelpUrl());
		valores.put("position",getPosition());
		valores.put("status",getStatus());
		valores.put("title",getTitle());	
		valores.put("url",getUrl());
		valores.put("relativeUrl",getRelativeUrl());
		valores.put("onClose",getOnClose());	
		valores.put("chromeType",getChromeType());	
		valores.put("testigo",getTestigo());	 
		valores.put("testigoJson",getTestigoJson());	 
		valores.put("skin",getSkin());	
		valores.put("imageFamily",getImageFamily());	
		valores.put("virtualDesktop",getVirtualDesktop());
		return valores;
	}
	
	/**
	 * Instantiates a new window base.
	 */
	public WindowBase() {
		super();
	}
	/*
	public WindowBase(WindowBase win) {
		setId(win.getId());
		setTitle(win.getTitle());
		setUrl(win.getUrl());
		setWidth(win.getWidth());
		setHeight(win.getHeight());
		setPosition(win.getPosition());
		setStatus(win.getStatus());
		setRelativeUrl(win.getRelativeUrl());
		setRelativeHelpUrl(win.getRelativeHelpUrl());
		setInnerHeight(win.getInnerHeight());
		setInnerWidth(win.getInnerWidth());
		setZIndex(win.getZIndex());
		setJs(win.getJs());
		setDescription(win.getDescription());
		setHelpUrl(win.getHelpUrl());
		setOnClose(win.getOnClose());
		setChromeType(win.getChromeType());
		setTestigo(win.getTestigo());
		setTestigoJson(win.getTestigoJson());
		setSkin(win.getSkin());
		setImageFamily(win.getImageFamily());
		setVirtualDesktop(win.getVirtualDesktop());
		setNoTaskbar(win.isNoTaskbar());
		setNoHide(win.isNoHide());
		setWidget(win.isWidget());
		setNoResize(win.isNoResize());
		setNoDrag(win.isNoDrag());
		setSessionSave(win.isSessionSave());
		setShowInAllDesktops(win.isShowInAllDesktops());
		setSingle(win.isSingle());
		setIgnoreMargins(win.isIgnoreMargins());		

	}*/

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}


	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}






	/**
	 * Checks if is widget.
	 * 
	 * @return true, if is widget
	 */
	public boolean isWidget() {
		return widget;
	}


	/**
	 * Sets the widget.
	 * 
	 * @param widget
	 *            the new widget
	 */
	public void setWidget(boolean widget) {
		this.widget = widget;
	}

	
	


	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}


	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
		if (this.title==null)
			this.title="";
	}


	/**
	 * Gets the url.
	 * 
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}


	/**
	 * Sets the url.
	 * 
	 * @param url
	 *            the new url
	 */
	public void setUrl(String url) {
		this.url = url;
		if (this.url==null)
			this.url="about:blank";
	}


	/**
	 * Gets the width.
	 * 
	 * @return the width
	 */
	public String getWidth() {
		return width;
	}


	/**
	 * Sets the width.
	 * 
	 * @param width
	 *            the new width
	 */
	public void setWidth(String width) {
		this.width = width;		
	}


	/**
	 * Gets the height.
	 * 
	 * @return the height
	 */
	public String getHeight() {
		return height;
	}


	/**
	 * Sets the height.
	 * 
	 * @param height
	 *            the new height
	 */
	public void setHeight(String height) {
		this.height = height;
	}


	/**
	 * Gets the position.
	 * 
	 * @return the position
	 */
	public String getPosition() {
		return position;
	}


	/**
	 * Sets the position.
	 * 
	 * @param position
	 *            the new position
	 */
	public void setPosition(String position) {
		this.position = position;
		if (this.position==null)
			this.position="100,100";
	}


	


	/**
	 * Checks if is single.
	 * 
	 * @return true, if is single
	 */
	public boolean isSingle() {
		return single;
	}


	/**
	 * Sets the single.
	 * 
	 * @param single
	 *            the new single
	 */
	public void setSingle(boolean single) {
		this.single = single;
	}


	/**
	 * Checks if is ignore margins.
	 * 
	 * @return true, if is ignore margins
	 */
	public boolean isIgnoreMargins() {
		return ignoreMargins;
	}


	/**
	 * Sets the ignore margins.
	 * 
	 * @param ignoreMargins
	 *            the new ignore margins
	 */
	public void setIgnoreMargins(boolean ignoreMargins) {
		this.ignoreMargins = ignoreMargins;
	}


	/**
	 * Gets the status.
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}


	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            the new status
	 */
	public void setStatus(String status) {
		this.status = status;
		if (this.status==null)
			this.status="No status";
	}


	/**
	 * Checks if is session save.
	 * 
	 * @return true, if is session save
	 */
	public boolean isSessionSave() {
		return sessionSave;
	}


	/**
	 * Sets the session save.
	 * 
	 * @param sessionSave
	 *            the new session save
	 */
	public void setSessionSave(boolean sessionSave) {
		this.sessionSave = sessionSave;
	}


	/**
	 * Checks if is show in all desktops.
	 * 
	 * @return true, if is show in all desktops
	 */
	public boolean isShowInAllDesktops() {
		return showInAllDesktops;
	}


	/**
	 * Sets the show in all desktops.
	 * 
	 * @param showInAllDesktops
	 *            the new show in all desktops
	 */
	public void setShowInAllDesktops(boolean showInAllDesktops) {
		this.showInAllDesktops = showInAllDesktops;
	}


	/**
	 * Gets the inner width.
	 * 
	 * @return the inner width
	 */
	public String getInnerWidth() {
		return innerWidth;
	}


	/**
	 * Sets the inner width.
	 * 
	 * @param innerWidth
	 *            the new inner width
	 */
	public void setInnerWidth(String innerWidth) {
		this.innerWidth = innerWidth;		
	}


	/**
	 * Gets the inner height.
	 * 
	 * @return the inner height
	 */
	public String getInnerHeight() {
		return innerHeight;
	}


	/**
	 * Sets the inner height.
	 * 
	 * @param innerHeight
	 *            the new inner height
	 */
	public void setInnerHeight(String innerHeight) {
		this.innerHeight = innerHeight;
	}


	/**
	 * Gets the z index.
	 * 
	 * @return the z index
	 */
	public String getZIndex() {
		return zIndex;
	}


	/**
	 * Sets the z index.
	 * 
	 * @param index
	 *            the new z index
	 */
	public void setZIndex(String index) {
		zIndex = index;
	}


	/**
	 * Checks if is no resize.
	 * 
	 * @return true, if is no resize
	 */
	public boolean isNoResize() {
		return noResize;
	}


	/**
	 * Sets the no resize.
	 * 
	 * @param noResize
	 *            the new no resize
	 */
	public void setNoResize(boolean noResize) {
		this.noResize = noResize;
	}


	/**
	 * Gets the js.
	 * 
	 * @return the js
	 */
	public String getJs() {
		return js;
	}


	/**
	 * Sets the js.
	 * 
	 * @param js
	 *            the new js
	 */
	public void setJs(String js) {
		this.js = js;
	}


	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the help url.
	 * 
	 * @return the help url
	 */
	public String getHelpUrl() {
		return helpUrl;
	}

	/**
	 * Sets the help url.
	 * 
	 * @param helpUrl
	 *            the new help url
	 */
	public void setHelpUrl(String helpUrl) {
		this.helpUrl = helpUrl;
	}

	/**
	 * Gets the on close.
	 * 
	 * @return the on close
	 */
	public String getOnClose() {
		return onClose;
	}

	/**
	 * Sets the on close.
	 * 
	 * @param onClose
	 *            the new on close
	 */
	public void setOnClose(String onClose) {
		this.onClose = onClose;
	}

	/**
	 * Gets the chrome type.
	 * 
	 * @return the chrome type
	 */
	public String getChromeType() {
		return chromeType;
	}

	/**
	 * Sets the chrome type.
	 * 
	 * @param chromeType
	 *            the new chrome type
	 */
	public void setChromeType(String chromeType) {
		this.chromeType = chromeType;
	}

	/**
	 * Gets the testigo.
	 * 
	 * @return the testigo
	 */
	public String getTestigo() {
		return testigo;
	}

	/**
	 * Sets the testigo.
	 * 
	 * @param testigo
	 *            the new testigo
	 */
	public void setTestigo(String testigo) {
		this.testigo = testigo;
	}

	/**
	 * Gets the testigo json.
	 * 
	 * @return the testigo json
	 */
	public String getTestigoJson() {
		return testigoJson;
	}

	/**
	 * Sets the testigo json.
	 * 
	 * @param testigoJson
	 *            the new testigo json
	 */
	public void setTestigoJson(String testigoJson) {
		this.testigoJson = testigoJson;
	}

	/**
	 * Gets the skin.
	 * 
	 * @return the skin
	 */
	public String getSkin() {
		return skin;
	}

	/**
	 * Sets the skin.
	 * 
	 * @param skin
	 *            the new skin
	 */
	public void setSkin(String skin) {
		this.skin = skin;
	}

	/**
	 * Checks if is no taskbar.
	 * 
	 * @return true, if is no taskbar
	 */
	public boolean isNoTaskbar() {
		return noTaskbar;
	}

	/**
	 * Sets the no taskbar.
	 * 
	 * @param noTaskbar
	 *            the new no taskbar
	 */
	public void setNoTaskbar(boolean noTaskbar) {
		this.noTaskbar = noTaskbar;
	}

	/**
	 * Gets the image family.
	 * 
	 * @return the image family
	 */
	public String getImageFamily() {
		return imageFamily;
	}

	/**
	 * Sets the image family.
	 * 
	 * @param imageFamily
	 *            the new image family
	 */
	public void setImageFamily(String imageFamily) {
		this.imageFamily = imageFamily;
	}

	/**
	 * Checks if is no hide.
	 * 
	 * @return true, if is no hide
	 */
	public boolean isNoHide() {
		return noHide;
	}

	/**
	 * Sets the no hide.
	 * 
	 * @param noHide
	 *            the new no hide
	 */
	public void setNoHide(boolean noHide) {
		this.noHide = noHide;
	}

	/**
	 * Gets the virtual desktop.
	 * 
	 * @return the virtual desktop
	 */
	public String getVirtualDesktop() {
		return virtualDesktop;
	}

	/**
	 * Sets the virtual desktop.
	 * 
	 * @param virtualDesktop
	 *            the new virtual desktop
	 */
	public void setVirtualDesktop(String virtualDesktop) {
		this.virtualDesktop = virtualDesktop;
	}

	/**
	 * Checks if is no drag.
	 * 
	 * @return true, if is no drag
	 */
	public boolean isNoDrag() {
		return noDrag;
	}

	/**
	 * Sets the no drag.
	 * 
	 * @param noDrag
	 *            the new no drag
	 */
	public void setNoDrag(boolean noDrag) {
		this.noDrag = noDrag;
	}

	/**
	 * Gets the relative help url.
	 * 
	 * @return the relative help url
	 */
	public String getRelativeHelpUrl() {
		return relativeHelpUrl;
	}

	/**
	 * Sets the relative help url.
	 * 
	 * @param relativeHelpUrl
	 *            the new relative help url
	 */
	public void setRelativeHelpUrl(String relativeHelpUrl) {
		this.relativeHelpUrl = relativeHelpUrl;
	}

	/**
	 * Gets the relative url.
	 * 
	 * @return the relative url
	 */
	public String getRelativeUrl() {
		return relativeUrl;
	}

	/**
	 * Sets the relative url.
	 * 
	 * @param relativeUrl
	 *            the new relative url
	 */
	public void setRelativeUrl(String relativeUrl) {
		this.relativeUrl = relativeUrl;
	}
	
}
