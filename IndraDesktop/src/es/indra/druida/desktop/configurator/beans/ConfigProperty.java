package es.indra.druida.desktop.configurator.beans;

/**
 * The Class ConfigProperty.
 */
public class ConfigProperty extends Parameter implements java.io.Serializable {
	
	/** The prop class. */
	private String propClass;

	/**
	 * Instantiates a new config property.
	 * 
	 * @param name
	 *            the name
	 * @param propClass
	 *            the prop class
	 * @param value
	 *            the value
	 */
	public ConfigProperty(String name, String propClass, String value) {
		super(name,value);
		this.propClass = propClass;
	}	
	
	/**
	 * Gets the prop class.
	 * 
	 * @return the prop class
	 */
	public String getPropClass() {
		return propClass;
	}
	
	/**
	 * Sets the prop class.
	 * 
	 * @param propClass
	 *            the new prop class
	 */
	public void setPropClass(String propClass) {
		this.propClass = propClass;
	}

}
