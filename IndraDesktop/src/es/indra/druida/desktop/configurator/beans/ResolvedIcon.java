package es.indra.druida.desktop.configurator.beans;


public class ResolvedIcon {
	

	private String url;
	private String text;
	private String iconUrl;
	
	public ResolvedIcon(String url, String text, String iconUrl) {
		super();
		this.url = url;
		this.text = text;
		this.iconUrl = iconUrl;
	}
	
	public String getIconUrl() {
		return iconUrl;
	}
	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

}
