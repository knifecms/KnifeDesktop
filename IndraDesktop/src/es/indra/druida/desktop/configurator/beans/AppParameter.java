package es.indra.druida.desktop.configurator.beans;
import es.indra.druida.desktop.utils.DesktopWriter;

/**
 * The Class AppParameter.
 */
public class AppParameter extends Parameter {

	
	/**
	 * Instantiates a new app parameter.
	 * 
	 * @param name
	 *            the name
	 * @param value
	 *            the value
	 */
	public AppParameter (String name, String value){
		super(name,value);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return "{\"nombre\":\""+this.name+"\",\"valor\":"+ DesktopWriter.generaCadenaJavascript(this.value) +"}";
	}
}
