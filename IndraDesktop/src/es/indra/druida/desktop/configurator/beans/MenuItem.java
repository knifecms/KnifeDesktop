package es.indra.druida.desktop.configurator.beans;

/**
 * The Class MenuItem.
 */
public class MenuItem {
	
	/** The is rule. */
	private Boolean isRule=false;
	
	/** The is file. */
	private Boolean isFile=false;
	
	/** The img. */
	private String img;
	
	/** The id. */
	private String id;
	
	/** The children. */
	private String[] children;
	
	/** The image family. */
	private String imageFamily;
	
	/** The js. */
	private String js;
	
	/** The name. */
	private String name;
	
	/** The title. */
	private String title;
	
	/**
	 * Gets the children.
	 * 
	 * @return the children
	 */
	public String[] getChildren() {
		return children;
	}
	
	/**
	 * Sets the children.
	 * 
	 * @param children
	 *            the new children
	 */
	public void setChildren(String[] children) {
		this.children = children;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * Gets the image family.
	 * 
	 * @return the image family
	 */
	public String getImageFamily() {
		return imageFamily;
	}
	
	/**
	 * Sets the image family.
	 * 
	 * @param imageFamily
	 *            the new image family
	 */
	public void setImageFamily(String imageFamily) {
		this.imageFamily = imageFamily;
	}
	
	/**
	 * Gets the js.
	 * 
	 * @return the js
	 */
	public String getJs() {
		return js;
	}
	
	/**
	 * Sets the js.
	 * 
	 * @param js
	 *            the new js
	 */
	public void setJs(String js) {
		this.js = js;
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Gets the checks if is rule.
	 * 
	 * @return the checks if is rule
	 */
	public Boolean getIsRule() {
		return isRule;
	}
	
	/**
	 * Sets the checks if is rule.
	 * 
	 * @param isRule
	 *            the new checks if is rule
	 */
	public void setIsRule(Boolean isRule) {
		this.isRule = isRule;
	}
	
	/**
	 * Instantiates a new menu item.
	 */
	public MenuItem() {
		super();
	}
	
	/*
	public MenuItem(String id, String name, String imageFamily, String title, String img, String js, Boolean isRule, Boolean isFile, String[] children) {
		super();
		this.isRule = isRule;
		this.isFile = isFile;		
		this.id = id;
		this.img=img;
		this.children = children;
		this.imageFamily = imageFamily;
		this.js = js;
		this.name = name;
		this.title = title;
	}*/
	
	/**
	 * Gets the checks if is file.
	 * 
	 * @return the checks if is file
	 */
	public Boolean getIsFile() {
		return isFile;
	}
	
	/**
	 * Sets the checks if is file.
	 * 
	 * @param isFile
	 *            the new checks if is file
	 */
	public void setIsFile(Boolean isFile) {
		this.isFile = isFile;
	}
	
	/**
	 * Gets the img.
	 * 
	 * @return the img
	 */
	public String getImg() {
		return img;
	}
	
	/**
	 * Sets the img.
	 * 
	 * @param img
	 *            the new img
	 */
	public void setImg(String img) {
		this.img = img;
	}
}
