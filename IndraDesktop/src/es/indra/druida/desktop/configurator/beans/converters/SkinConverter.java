package es.indra.druida.desktop.configurator.beans.converters;

import es.indra.druida.desktop.configurator.beans.Skin;
import es.indra.druida.desktop.configurator.beans.SkinParameter;
import es.indra.druida.desktop.utils.DesktopWriter;

/**
 * The Class SkinConverter.
 */
public class SkinConverter {
	
	/**
	 * Bean array to json.
	 * 
	 * @param skins
	 *            the skins
	 * 
	 * @return the string
	 */
	public static String beanArrayToJson(Skin[] skins)	{
		StringBuffer jsonRet=new StringBuffer();
		jsonRet.append("[");
		for ( int i = 0 ; i < skins.length ; i++) {
			if (i>0)
				jsonRet.append(",");			
			Skin skin=skins[i];
			jsonRet.append("{");
			jsonRet.append( "id:"+DesktopWriter.generaCadenaJavascript(skin.getId()) );
			if (skin.getDescription()!=null)
				jsonRet.append( ",description:"+DesktopWriter.generaCadenaJavascript(skin.getDescription()) );
			if (skin.getGroup()!=null)			
				jsonRet.append( ",group:"+DesktopWriter.generaCadenaJavascript(skin.getGroup()) );
			SkinParameter[] params=skin.getParameters();
			if (params!=null && params.length>0)	{
				jsonRet.append( ",parameters:{");
				for (int j=0;j<params.length;j++){
					if (j>0)
						jsonRet.append(",");	
					jsonRet.append( DesktopWriter.generaCadenaJavascript(params[j].getName()) );
					jsonRet.append(":");
					jsonRet.append( DesktopWriter.generaCadenaJavascript(params[j].getValue()) );
				}	
				jsonRet.append( "}");
				//Tipos de los parámetros
				jsonRet.append( ",paramMeta:{");
				boolean esPrimero=true;
				for (int j=0;j<params.length;j++){
					if (params[j].getPosibleValues()!=null|| params[j].getNoEditable()!=null || params[j].getDescription()!=null){
						if (esPrimero)
							esPrimero=false;
						else
							jsonRet.append(",");
						jsonRet.append( DesktopWriter.generaCadenaJavascript(params[j].getName()) );
						jsonRet.append(":{values:"+DesktopWriter.generaCadenaJavascript(params[j].getPosibleValues()) );
						if (params[j].getNoEditable()!=null)
							jsonRet.append(",noEditable:"+DesktopWriter.generaCadenaJavascript(params[j].getNoEditable()) );
						if (params[j].getPosibleValueDescriptions()!=null)
							jsonRet.append(",valueDescriptions:"+DesktopWriter.generaCadenaJavascript(params[j].getPosibleValueDescriptions()) );
						if (params[j].getDescription()!=null)
							jsonRet.append(",description:"+DesktopWriter.generaCadenaJavascript(params[j].getDescription()) );
						if (params[j].getType()!=null)
							jsonRet.append(",type:"+DesktopWriter.generaCadenaJavascript(params[j].getType()) );							
						jsonRet.append("}");						
					}
				}	
				jsonRet.append( "}");				
			}
			jsonRet.append("}");			
		}
		jsonRet.append("]");
		return jsonRet.toString();
	}
	
	/*public static Skin[] JsonToBeanArray (String jsonIcons) throws JSONException	{
		return null;
	}*/	
}

