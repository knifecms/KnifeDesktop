package es.indra.druida.desktop.configurator.beans;

/**
 * The Class FileType.
 */
public class FileType {
	
	/** The extension. */
	private String extension;
	
	/** The application. */
	private String application;
	
	/**
	 * Instantiates a new file type.
	 * 
	 * @param extension
	 *            the extension
	 * @param application
	 *            the application
	 */
	public FileType(String extension, String application) {
		super();
		this.extension = extension;
		this.application = application;
	}
	
	/**
	 * Gets the application.
	 * 
	 * @return the application
	 */
	public String getApplication() {
		return application;
	}
	
	/**
	 * Sets the application.
	 * 
	 * @param application
	 *            the new application
	 */
	public void setApplication(String application) {
		this.application = application;
	}
	
	/**
	 * Gets the extension.
	 * 
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}
	
	/**
	 * Sets the extension.
	 * 
	 * @param extension
	 *            the new extension
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}
	
}
