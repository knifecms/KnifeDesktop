package es.indra.druida.desktop.configurator.beans;

/**
 * The Class Engine.
 */
public class Engine {
	
	/** The id. */
	private String id;
	
	/** The url. */
	private String url;
	
	/** The description. */
	private String description;
	
	/** The permitted parameters. */
	private AppParameter[] permittedParameters;
	
	/** The permitted parameters. */
	private Parameter[] configParameters;	
	
	/**
	 * Instantiates a new engine.
	 * 
	 * @param id
	 *            the id
	 */
	public Engine(String id) {
		super();
		this.id = id;
	}
	
	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * Gets the permitted parameters.
	 * 
	 * @return the permitted parameters
	 */
	public AppParameter[] getPermittedParameters() {
		return permittedParameters;
	}
	
	/**
	 * Sets the permitted parameters.
	 * 
	 * @param permittedParameters
	 *            the new permitted parameters
	 */
	public void setPermittedParameters(AppParameter[] permittedParameters) {
		this.permittedParameters = permittedParameters;
	}
	
	/**
	 * Gets the url.
	 * 
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * Sets the url.
	 * 
	 * @param url
	 *            the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	public Parameter[] getConfigParameters() {
		return configParameters;
	}

	public void setConfigParameters(Parameter[] configParameters) {
		this.configParameters = configParameters;
	}
	
}
