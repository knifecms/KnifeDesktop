package es.indra.druida.desktop.configurator.beans;

import java.util.HashMap;

/**
 * The Class Window.
 */
public class Window extends WindowBase{

	/** The application. */
	String application;
	
	/** The id padre. */
	String idPadre;
	
	/** The parameters. */
	String parameters; //No los salvo en realidad ya que para cuando se salva la ventana los parameters est�n aplicados a la URL
	
	/** The daemons. */
	String daemons;
	
	/**
	 * Instantiates a new window.
	 * 
	 * @param valores
	 *            the valores
	 */
	public Window(HashMap <String,String> valores) {//HashMap de strings
		super(valores);		
		setDaemons(valores.get("daemons"));
		setIdPadre(valores.get("idPadre"));
		setParameters(valores.get("parameters"));
		setApplication(valores.get("application"));
	}
	
	/* (non-Javadoc)
	 * @see es.indra.druida.desktop.configurator.beans.WindowBase#get()
	 */
	public HashMap <String,String> get()	{ //Prepara un hashmap con todos los valores como strings
		HashMap <String,String> valores= super.get();
		valores.put("daemons",getDaemons());	 
		valores.put("idPadre",getIdPadre());	
		valores.put("parameters",getParameters());	
		valores.put("application",getApplication());		
		return valores;
	}
	
	/**
	 * Gets the daemons.
	 * 
	 * @return the daemons
	 */
	public String getDaemons() {
		return daemons;
	}
	
	/**
	 * Sets the daemons.
	 * 
	 * @param daemons
	 *            the new daemons
	 */
	public void setDaemons(String daemons) {
		this.daemons = daemons;
	}
	
	/**
	 * Gets the id padre.
	 * 
	 * @return the id padre
	 */
	public String getIdPadre() {
		return idPadre;
	}
	
	/**
	 * Sets the id padre.
	 * 
	 * @param idPadre
	 *            the new id padre
	 */
	public void setIdPadre(String idPadre) {
		this.idPadre = idPadre;
	}
	
	/**
	 * Gets the parameters.
	 * 
	 * @return the parameters
	 */
	public String getParameters() {
		return parameters;
	}
	
	/**
	 * Sets the parameters.
	 * 
	 * @param parameters
	 *            the new parameters
	 */
	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

	/**
	 * Gets the application.
	 * 
	 * @return the application
	 */
	public String getApplication() {
		return application;
	}

	/**
	 * Sets the application.
	 * 
	 * @param application
	 *            the new application
	 */
	public void setApplication(String application) {
		this.application = application;
	}
}
