package es.indra.druida.desktop.configurator.beans.converters;

import es.indra.druida.desktop.configurator.beans.MenuItem;
import es.indra.druida.desktop.utils.DesktopWriter;

/**
 * The Class MenuConverter.
 */
public class MenuConverter {
	
	/**
	 * Bean array to json.
	 * 
	 * @param opciones
	 *            the opciones
	 * 
	 * @return the string
	 */
	public static String beanArrayToJson(MenuItem[] opciones)	{
		StringBuffer jsonRet=new StringBuffer();
		jsonRet.append("[");
		for ( int i = 0 ; i < opciones.length ; i++) {
			MenuItem opcion=opciones[i];
			if (i>0)
				jsonRet.append(",");
			if (opcion.getIsRule())	{//Eliminar este nodo... TO-DO
				jsonRet.append("{tipo:cc.BARRA, id:\"BARRA\"}");
			} else	{
				jsonRet.append("{tipo:cc.NODO");
				jsonRet.append( ",id:"+DesktopWriter.generaCadenaJavascript(opcion.getId()) );
				if (opcion.getName()!=null)	{
					if (opcion.getIsFile())
						jsonRet.append( ",fichero:"+DesktopWriter.generaCadenaJavascript( opcion.getName()) );
					else
						jsonRet.append( ",aplicacion:"+DesktopWriter.generaCadenaJavascript( opcion.getName()) );
				}
				if (opcion.getTitle()!=null)
					jsonRet.append( ",texto:"+DesktopWriter.generaCadenaJavascript( opcion.getTitle()) );
				if (opcion.getImageFamily()!=null)
					jsonRet.append( ",imageFamily:"+DesktopWriter.generaCadenaJavascript( opcion.getImageFamily()) );
				if (opcion.getImg()!=null)
					jsonRet.append( ",img:"+DesktopWriter.generaCadenaJavascript( opcion.getImg()) );	
				if (opcion.getJs()!=null)
					jsonRet.append( ",js:"+DesktopWriter.generaCadenaJavascript( opcion.getJs()) );	
				if (opcion.getChildren()!=null){
					jsonRet.append( ",hijos:[");
					for (int j=0; j<opcion.getChildren().length; j++){
						if (j>0)
							jsonRet.append(",");
						jsonRet.append(DesktopWriter.generaCadenaJavascript(opcion.getChildren()[j]));
					}
					jsonRet.append( "]");
				}
				jsonRet.append("}");
			}
		}
		jsonRet.append("]");
		return jsonRet.toString();
		
	}
	
	/*public static Icon[] jsonToBeanArray (String jsonIcons) throws JSONException	{
		JSONArray aIconos = new JSONArray(jsonIcons);
		Icon[] retIcons=new Icon[aIconos.length()];
		for (int r=0;r<aIconos.length(); r++){
			int x=0,y=0;
			String application=null,file=null;
			JSONObject oIcono= aIconos.getJSONObject(r);
			if (oIcono.has("x"))
				x=oIcono.getInt("x");
			if (oIcono.has("y"))
				y=oIcono.getInt("y");
			if(oIcono.has("application"))
				application=oIcono.getString("application");
			if(oIcono.has("file"))
				file=oIcono.getString("file");	
			retIcons[r]=new Icon(x,y,application,file);
		}
		return retIcons;
	}	*/
}

