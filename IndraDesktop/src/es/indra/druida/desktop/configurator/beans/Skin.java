package es.indra.druida.desktop.configurator.beans;

import es.indra.druida.desktop.utils.DesktopWriter;

/**
 * The Class Skin.
 */
public class Skin {
	
	/** The id. */
	String id; 
	
	/** The description. */
	String description;
	
	/** The group. */
	String group;
	
	/** The parameters. */
	SkinParameter[] parameters;
	
	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 * 
	 * @param Id
	 *            the new id
	 */
	public void setId(String Id) {
		this.id = Id;
	}
	
	/**
	 * Instantiates a new skin.
	 * 
	 * @param Id
	 *            the id
	 * @param description
	 *            the description
	 * @param group
	 *            the group
	 */
	public Skin(String Id, String description, String group) {
		super();
		this.id = Id;
		this.description = description;
		this.group = group;
	}	
	
	/**
	 * Instantiates a new skin.
	 * 
	 * @param Id
	 *            the id
	 */
	public Skin(String Id) {
		super();
		this.id = Id;
	}	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String str="{id:"+DesktopWriter.generaCadenaJavascript(id)+",description:"+DesktopWriter.generaCadenaJavascript(description)+",group:"+group;
		
		int longo=parameters.length;
		if (longo!=0){
			str += ",parameters:{";
			for (int i=0;i<longo;i++){
				str += parameters[i].toString();
				if (i!=(longo-1))
					str+=",";
			}
			str += "}";
		}
		str += "}";
		//System.out.println("SKIIIIN:"+str);
		return str;
	}
	
	/**
	 * Gets the group.
	 * 
	 * @return the group
	 */
	public String getGroup() {
		return group;
	}
	
	/**
	 * Sets the group.
	 * 
	 * @param group
	 *            the new group
	 */
	public void setGroup(String group) {
		this.group = group;
	}
	
	/**
	 * Gets the parameters.
	 * 
	 * @return the parameters
	 */
	public SkinParameter[] getParameters() {
		return parameters;
	}
	
	/**
	 * Sets the parameters.
	 * 
	 * @param parameters
	 *            the new parameters
	 */
	public void setParameters(SkinParameter[] parameters) {
		this.parameters = parameters;
	}
}
