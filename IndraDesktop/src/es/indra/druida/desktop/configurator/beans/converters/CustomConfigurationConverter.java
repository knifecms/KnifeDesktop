package es.indra.druida.desktop.configurator.beans.converters;

import java.util.Iterator;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import es.indra.druida.desktop.configurator.beans.ConfigProperty;
import es.indra.druida.desktop.utils.DesktopWriter;

/**
 * The Class CustomConfigurationConverter.
 */
public class CustomConfigurationConverter {
	/*
	 * 
	 * 		StringBuffer _configJS = new StringBuffer();
		NodeList atribs = doc.getDocumentElement().getChildNodes();
		boolean primero=true;
		for ( int i = 0 ; i < atribs.getLength(); i++) {
			//System.out.println (atribs.item(i).getNodeName() + ": "+atribs.item(i).getTextContent()) ;						
			if (atribs.item(i).getNodeType()!= Node.TEXT_NODE && atribs.item(i).getFirstChild() != null && atribs.item(i).getFirstChild().getNodeValue()!=null)	{
				//System.out.println (atribs.item(i).getNodeName()) ; //TEXT_NODE
				Element atrib = (Element) atribs.item(i);
				if (!primero)
					_configJS.append(",");
				primero=false;				
				_configJS.append(DesktopWriter.generaCadenaJavascript(atrib.getNodeName()));
				_configJS.append(":");
				if (!atrib.hasAttribute("clase")||atrib.getAttribute("clase").equals("String"))	{
					_configJS.append(DesktopWriter.generaCadenaJavascript(atrib.getFirstChild().getNodeValue()));
				} else
					_configJS.append(atrib.getFirstChild().getNodeValue());
			}
		}
		return _configJS.toString();
	 */
	/**
	 * Bean array to json.
	 * 
	 * @param elems
	 *            the elems
	 * 
	 * @return the string
	 */
	public static String beanArrayToJson(ConfigProperty[] elems)	{
		if (elems==null){
			return "{tema:'XP',modoSinErrores:false,mensajesDelSistema:true,animacionesActivas:false}";
		}
		StringBuffer jsonRet=new StringBuffer();
		jsonRet.append("{");
		for ( int i = 0 ; i < elems.length ; i++) {		
			if (i>0)
				jsonRet.append(",");
			ConfigProperty elem=elems[i];
			jsonRet.append( DesktopWriter.generaCadenaJavascript(elem.getName()) );
			jsonRet.append( ":" );
			if (elem.getPropClass()==null || elem.getPropClass().equals("String"))	
				jsonRet.append(DesktopWriter.generaCadenaJavascript(elem.getValue()));
			else
				jsonRet.append(elem.getValue());		
		}
		jsonRet.append("}");
		return jsonRet.toString();
	}
	
	/**
	 * Json to bean array.
	 * 
	 * @param jsonElem
	 *            the json elem
	 * 
	 * @return the config property[]
	 * 
	 * @throws JSONException
	 *             the JSON exception
	 */
	public static ConfigProperty[] jsonToBeanArray (String jsonElem) throws JSONException	{
		JSONObject aElem = new JSONObject(jsonElem);
		Set props= aElem.getKeySet();
		ConfigProperty[] retElems=new ConfigProperty[props.size()];
		Iterator iterator=props.iterator();
		int i=0;
	    while (iterator.hasNext()){  
	        String propName=(String)iterator.next();
	        String propClass=null;
	        Object valor=aElem.get(propName);
			String nombreClase=valor.getClass().getName();
			if (nombreClase.equals("java.lang.String"))	{
				retElems[i]=new ConfigProperty(propName,null,aElem.getString(propName));
			} else if (nombreClase.equals("java.lang.Boolean"))	{
				retElems[i]=new ConfigProperty(propName,"Boolean",aElem.getString(propName));
			} else if (nombreClase.equals("java.lang.Integer"))	{
				retElems[i]=new ConfigProperty(propName,"Integer",aElem.getString(propName));
			} else {
				retElems[i]=new ConfigProperty(propName,"Integer",null);
			}
			i++;	        	
	    } 
		return retElems;
	}	
}

