package es.indra.druida.desktop.configurator.beans;

import es.indra.druida.desktop.utils.DesktopWriter;

/**
 * The Class AppGroup.
 */
public class AppGroup {
	
	/** The name. */
	private String name;
	
	/** The description. */
	private String description;
	
	/** The version. */
	private String version="";
	
	/** The author. */
	private String author="";
	
	/** The license. */
	private String license="";
	
	/** The category. */
	private String category="";
	
	/** The applications. */
	private String [] applications;
	
	/**
	 * Instantiates a new app group.
	 * 
	 * @param name
	 *            the name
	 * @param description
	 *            the description
	 */
	public AppGroup(String name, String description){
		super();
		this.name=name;
		this.description=description;
	}
	
	/**
	 * Instantiates a new app group.
	 * 
	 * @param name
	 *            the name
	 * @param description
	 *            the description
	 * @param applications
	 *            the applications
	 */
	public AppGroup(String name, String description, String[] applications){
		super();
		this.name=name;
		this.description=description;
		this.applications=applications;		
	}	

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String str="{\"name\":"+DesktopWriter.generaCadenaJavascript(name)
			+",\"descripcion\":"+DesktopWriter.generaCadenaJavascript(description)
			+",\"version\":"+DesktopWriter.generaCadenaJavascript(version)
			+",\"author\":"+DesktopWriter.generaCadenaJavascript(author)
			+",\"license\":"+DesktopWriter.generaCadenaJavascript(license)
			+",\"category\":"+DesktopWriter.generaCadenaJavascript(category)
			+"}";
		return str;
	}

	/**
	 * Gets the version.
	 * 
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Sets the version.
	 * 
	 * @param version
	 *            the new version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * Gets the author.
	 * 
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * Sets the author.
	 * 
	 * @param author
	 *            the new author
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * Gets the license.
	 * 
	 * @return the license
	 */
	public String getLicense() {
		return license;
	}

	/**
	 * Sets the license.
	 * 
	 * @param license
	 *            the new license
	 */
	public void setLicense(String license) {
		this.license = license;
	}

	/**
	 * Gets the category.
	 * 
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * Sets the category.
	 * 
	 * @param category
	 *            the new category
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * Gets the applications.
	 * 
	 * @return the applications
	 */
	public String[] getApplications() {
		return applications;
	}

	/**
	 * Sets the applications.
	 * 
	 * @param applications
	 *            the new applications
	 */
	public void setApplications(String[] applications) {
		this.applications = applications;
	}
}
