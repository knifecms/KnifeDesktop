package es.indra.druida.desktop.configurator.beans;

/**
 * The Class SecurityConstraint.
 */
public class SecurityConstraint {

	/** The id. */
	private String id;
	
	/** The description. */
	private String description;	
	
	/** The authorized roles. */
	private String [] authorizedRoles;
	
	/** The application groups. */
	private String [] applicationGroups;
	
	/** The applications. */
	private String [] applications;
	
	/**
	 * Gets the application groups.
	 * 
	 * @return the application groups
	 */
	public String[] getApplicationGroups() {
		return applicationGroups;
	}
	
	/**
	 * Sets the application groups.
	 * 
	 * @param applicationGroups
	 *            the new application groups
	 */
	public void setApplicationGroups(String[] applicationGroups) {
		this.applicationGroups = applicationGroups;
	}
	
	/**
	 * Gets the applications.
	 * 
	 * @return the applications
	 */
	public String[] getApplications() {
		return applications;
	}
	
	/**
	 * Sets the applications.
	 * 
	 * @param applications
	 *            the new applications
	 */
	public void setApplications(String[] applications) {
		this.applications = applications;
	}
	
	/**
	 * Gets the authorized roles.
	 * 
	 * @return the authorized roles
	 */
	public String[] getAuthorizedRoles() {
		return authorizedRoles;
	}
	
	/**
	 * Sets the authorized roles.
	 * 
	 * @param authorizedRoles
	 *            the new authorized roles
	 */
	public void setAuthorizedRoles(String[] authorizedRoles) {
		this.authorizedRoles = authorizedRoles;
	}

	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}	
	

}
