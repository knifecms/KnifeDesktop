package es.indra.druida.desktop.configurator.beans;

/**
 * The Class Icon.
 */
public class Icon {
	
	/** The x. */
	private int x;
	
	/** The y. */
	private int y;
	
	/** The application. */
	private String application;
	
	/** The file. */
	private String file;
	
	/**
	 * Instantiates a new icon.
	 * 
	 * @param x
	 *            the x
	 * @param y
	 *            the y
	 * @param application
	 *            the application
	 * @param file
	 *            the file
	 */
	public Icon(int x, int y, String application, String file) {
		super();
		this.x = x;
		this.y = y;
		this.application = application;
		this.file = file;
	}
	
	/**
	 * Gets the application.
	 * 
	 * @return the application
	 */
	public String getApplication() {
		return application;
	}
	
	/**
	 * Sets the application.
	 * 
	 * @param application
	 *            the new application
	 */
	public void setApplication(String application) {
		this.application = application;
	}
	
	/**
	 * Gets the file.
	 * 
	 * @return the file
	 */
	public String getFile() {
		return file;
	}
	
	/**
	 * Sets the file.
	 * 
	 * @param file
	 *            the new file
	 */
	public void setFile(String file) {
		this.file = file;
	}
	
	/**
	 * Gets the x.
	 * 
	 * @return the x
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Sets the x.
	 * 
	 * @param x
	 *            the new x
	 */
	public void setX(int x) {
		this.x = x;
	}
	
	/**
	 * Gets the y.
	 * 
	 * @return the y
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Sets the y.
	 * 
	 * @param y
	 *            the new y
	 */
	public void setY(int y) {
		this.y = y;
	}	
	
	//XXX no meto el toString porque se me queda corto (a js, a xml , ... �? )
}
