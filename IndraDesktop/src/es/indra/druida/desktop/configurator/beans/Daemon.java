package es.indra.druida.desktop.configurator.beans;

/**
 * The Class Daemon.
 */
public class Daemon {
	
	/** The milliseconds. */
	private int milliseconds=0;
	
	/** The script. */
	private String script;
	
	/**
	 * Instantiates a new daemon.
	 * 
	 * @param milliseconds
	 *            the milliseconds
	 * @param script
	 *            the script
	 */
	public Daemon(int milliseconds, String script){
		super();
		this.milliseconds=milliseconds;
		this.script=script;
	}

	/**
	 * Gets the milliseconds.
	 * 
	 * @return the milliseconds
	 */
	public int getMilliseconds() {
		return milliseconds;
	}

	/**
	 * Sets the milliseconds.
	 * 
	 * @param milliseconds
	 *            the new milliseconds
	 */
	public void setMilliseconds(int milliseconds) {
		this.milliseconds = milliseconds;
	}

	/**
	 * Gets the script.
	 * 
	 * @return the script
	 */
	public String getScript() {
		return script;
	}

	/**
	 * Sets the script.
	 * 
	 * @param script
	 *            the new script
	 */
	public void setScript(String script) {
		this.script = script;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return "{\"milliseconds\":"+Integer.toString(milliseconds)+",\"script\":\""+script+"\"}";
	}
}
