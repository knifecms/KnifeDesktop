package es.indra.druida.desktop.configurator.beans;

/**
 * The Class User.
 */
public class User {
	
	/** The id. */
	private String id;
	
	/** The name. */
	private String name;
	
	/** The surname. */
	private String surname;
	
	/** The mother surname. */
	private String motherSurname;
	
	/** The file system root. */
	private String fileSystemRoot;
	
	/** The roles. */
	private String [] roles;
	
	/** The password. */
	private String password;
	
	/** The password hash. */
	private String passwordHash;
	
	/** The new password. */
	private String newPassword;
	
	private boolean hasPassword;
	
	private boolean refresh;
	
	public boolean isRefresh() {
		return refresh;
	}

	public void setRefresh(boolean refresh) {
		this.refresh = refresh;
	}

	public boolean isHasPassword() {
		return hasPassword;
	}

	public void setHasPassword(boolean hasPassword) {
		this.hasPassword = hasPassword;
	}

	/**
	 * Gets the file system root.
	 * 
	 * @return the file system root
	 */
	public String getFileSystemRoot() {
		return fileSystemRoot;
	}
	
	/**
	 * Sets the file system root.
	 * 
	 * @param fileSystemRoot
	 *            the new file system root
	 */
	public void setFileSystemRoot(String fileSystemRoot) {
		this.fileSystemRoot = fileSystemRoot;
	}
	
	/**
	 * Gets the id.
	 * 
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the id.
	 * 
	 * @param id
	 *            the new id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * Gets the mother surname.
	 * 
	 * @return the mother surname
	 */
	public String getMotherSurname() {
		return motherSurname;
	}
	
	/**
	 * Sets the mother surname.
	 * 
	 * @param motherSurname
	 *            the new mother surname
	 */
	public void setMotherSurname(String motherSurname) {
		this.motherSurname = motherSurname;
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the roles.
	 * 
	 * @return the roles
	 */
	public String[] getRoles() {
		return roles;
	}
	
	/**
	 * Sets the roles.
	 * 
	 * @param roles
	 *            the new roles
	 */
	public void setRoles(String[] roles) {
		this.roles = roles;
	}
	
	/**
	 * Gets the surname.
	 * 
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}
	
	/**
	 * Sets the surname.
	 * 
	 * @param surname
	 *            the new surname
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * Gets the new password.
	 * 
	 * @return the new password
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * Sets the new password.
	 * 
	 * @param newPassword
	 *            the new new password
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * Gets the password.
	 * 
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 * 
	 * @param password
	 *            the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the password hash.
	 * 
	 * @return the password hash
	 */
	public String getPasswordHash() {
		return passwordHash;
	}

	/**
	 * Sets the password hash.
	 * 
	 * @param passwordHash
	 *            the new password hash
	 */
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}
}
