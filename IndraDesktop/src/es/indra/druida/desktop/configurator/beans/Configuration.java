package es.indra.druida.desktop.configurator.beans;

/**
 * The Class Configuration.
 */
public class Configuration {
	
	/** The engines. */
	private Engine[] engines;
	
	/** The file types. */
	private FileType[] fileTypes;
	
	/** The no session save. */
	private String noSessionSave;
	
	/** The sole user. */
	private String soleUser;
	
	/**
	 * Gets the engines.
	 * 
	 * @return the engines
	 */
	public Engine[] getEngines() {
		return engines;
	}
	
	/**
	 * Sets the engines.
	 * 
	 * @param engines
	 *            the new engines
	 */
	public void setEngines(Engine[] engines) {
		this.engines = engines;
	}
	
	/**
	 * Gets the file types.
	 * 
	 * @return the file types
	 */
	public FileType[] getFileTypes() {
		return fileTypes;
	}
	
	/**
	 * Sets the file types.
	 * 
	 * @param fileTypes
	 *            the new file types
	 */
	public void setFileTypes(FileType[] fileTypes) {
		this.fileTypes = fileTypes;
	}
	
	/**
	 * Gets the no session save.
	 * 
	 * @return the no session save
	 */
	public String getNoSessionSave() {
		return noSessionSave;
	}
	
	/**
	 * Sets the no session save.
	 * 
	 * @param noSessionSave
	 *            the new no session save
	 */
	public void setNoSessionSave(String noSessionSave) {
		this.noSessionSave = noSessionSave;
	}
	
	/**
	 * Gets the sole user.
	 * 
	 * @return the sole user
	 */
	public String getSoleUser() {
		return soleUser;
	}
	
	/**
	 * Sets the sole user.
	 * 
	 * @param soleUser
	 *            the new sole user
	 */
	public void setSoleUser(String soleUser) {
		this.soleUser = soleUser;
	}
}
