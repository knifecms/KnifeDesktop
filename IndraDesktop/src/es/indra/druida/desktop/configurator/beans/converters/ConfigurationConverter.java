package es.indra.druida.desktop.configurator.beans.converters;

import es.indra.druida.desktop.configurator.beans.Configuration;
import es.indra.druida.desktop.configurator.beans.Engine;
import es.indra.druida.desktop.configurator.beans.FileType;
import es.indra.druida.desktop.utils.DesktopWriter;

/**
 * The Class ConfigurationConverter.
 */
public class ConfigurationConverter {
	
	/**
	 * Bean to json.
	 * 
	 * @param configuration
	 *            the configuration
	 * 
	 * @return the string
	 */
	public static String beanToJson(Configuration configuration)	{
		StringBuffer jsonRet=new StringBuffer();
		jsonRet.append("{");
		jsonRet.append("engines:{");
		if (configuration.getEngines()!=null){
			Engine[] engines=configuration.getEngines();
			for ( int i = 0 ; i < engines.length ; i++) {
				if (i>0)
					jsonRet.append(",");
				jsonRet.append(DesktopWriter.generaCadenaJavascript(engines[i].getId()) );
				jsonRet.append(":{id:"+DesktopWriter.generaCadenaJavascript(engines[i].getId()) );
				if (engines[i].getDescription()!=null)
					jsonRet.append(",descripcion:"+DesktopWriter.generaCadenaJavascript(engines[i].getDescription()) );
				if (engines[i].getUrl()!=null)
					jsonRet.append(",url:"+DesktopWriter.generaCadenaJavascript(engines[i].getUrl()) );				
				jsonRet.append("}");
			}
		}	
		jsonRet.append("}");
		jsonRet.append(",fileTypes:{");
		if (configuration.getFileTypes()!=null){
			FileType[] fileTypes=configuration.getFileTypes();
			for ( int i = 0 ; i < fileTypes.length ; i++) {
				if (i>0)
					jsonRet.append(",");
				jsonRet.append(DesktopWriter.generaCadenaJavascript(fileTypes[i].getExtension()) );
				jsonRet.append(":{aplicacion:"+DesktopWriter.generaCadenaJavascript(fileTypes[i].getApplication())+"}");
			}
		}
		jsonRet.append("}");		
		if (configuration.getNoSessionSave()!=null)
			jsonRet.append( ",noSessionSave:"+DesktopWriter.generaCadenaJavascript(configuration.getNoSessionSave()) );
		if (configuration.getSoleUser()!=null)
			jsonRet.append( ",soleUser:"+DesktopWriter.generaCadenaJavascript(configuration.getSoleUser()) );		
		jsonRet.append("}");		
		return jsonRet.toString();
	}	
}

