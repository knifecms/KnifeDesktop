package es.indra.druida.desktop.configurator.beans;

/**
 * The Class SecurityConfiguration.
 */
public class SecurityConfiguration {
	
	/** The users. */
	private User [] users;
	
	/** The Security constraints. */
	private SecurityConstraint [] SecurityConstraints;
	
	/**
	 * Gets the security constraints.
	 * 
	 * @return the security constraints
	 */
	public SecurityConstraint[] getSecurityConstraints() {
		return SecurityConstraints;
	}
	
	/**
	 * Sets the security constraints.
	 * 
	 * @param securityConstraints
	 *            the new security constraints
	 */
	public void setSecurityConstraints(SecurityConstraint[] securityConstraints) {
		SecurityConstraints = securityConstraints;
	}
	
	/**
	 * Gets the users.
	 * 
	 * @return the users
	 */
	public User[] getUsers() {
		return users;
	}
	
	/**
	 * Sets the users.
	 * 
	 * @param users
	 *            the new users
	 */
	public void setUsers(User[] users) {
		this.users = users;
	}
}
