package es.indra.druida.desktop.configurator.beans;

import es.indra.druida.desktop.utils.DesktopWriter;

public class CopyOfApplication {

	private String id;
	private String group; 
	private boolean widget;
	private Daemon[] daemons;
	private AppParameter[] parameters;
	private String title;
	private String url;
	private int width;
	private int height;
	private String position;
	private String icon;
	private String iconText;
	private boolean single=false;
	private boolean ignoreMargins;
	private String status;
	private boolean sessionSave=true;
	private boolean showInAllDesktops=false;
	private int innerWidth=0;
	private int innerHeight=0;
	private int zIndex;
	private boolean noResize=false;
	private String js;
	private String description;
	private String engine;
	private String helpUrl;	
	private int minWidth;
	private int minHeight;
	private int maxWidth;
	private int maxHeight;
	private String onClose;
	private String chromeType;
	private boolean resizable;
	private String testigo;
	private String testigoJson;
	private String skin;
	private boolean noTaskbar;
	private String imageFamily;
	private boolean noHide;
	private String virtualDesktop;
	private boolean noDrag;
	private String relativeUrl;
	private String relativeHelpUrl;	

	public CopyOfApplication(String id, String group, boolean widget, Daemon[] daemons, AppParameter[] parameters) {
		super();
		this.id=id;
		this.group=group;
		this.widget=widget;
		this.daemons=daemons;
		this.parameters=parameters;
	}

	public CopyOfApplication() {
		super();
	}

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getGroup() {
		return group;
	}


	public void setGroup(String group) {
		this.group = group;
	}


	public boolean isWidget() {
		return widget;
	}


	public void setWidget(boolean widget) {
		this.widget = widget;
	}


	public Daemon[] getDaemons() {
		return daemons;
	}


	public void setDaemons(Daemon[] daemons) {
		this.daemons = daemons;
	}


	public AppParameter[] getParameters() {
		return parameters;
	}


	public void setParameters(AppParameter[] parameters) {
		this.parameters = parameters;
	}
	
	public String toString(){
		
		String str=DesktopWriter.generaCadenaJavascript(id)+":";
		str += "{"
		+"\"grupo\":"+group;
		if (widget)
			str +=",\"widget\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(widget));
		if (title!=null && title.length()>0)
			str +=",\"title\":"+DesktopWriter.generaCadenaJavascript(title);
		if (url!=null && url.length()>0)
			str +=",\"url\":"+DesktopWriter.generaCadenaJavascript(url);
		if (relativeUrl!=null && relativeUrl.length()>0)
			str +=",\"relativeUrl\":"+DesktopWriter.generaCadenaJavascript(relativeUrl);		
		if (innerWidth==0)
			str +=",\"width\":"+DesktopWriter.generaCadenaJavascript(Integer.toString(width));
		if (innerHeight==0)
			str +=",\"height\":"+DesktopWriter.generaCadenaJavascript(Integer.toString(height));
		str += ",\"position\":"+DesktopWriter.generaCadenaJavascript(position)
		+",\"icon\":"+DesktopWriter.generaCadenaJavascript(icon);
		if (iconText!=null && iconText.length()>0)
			str +=",\"iconText\":"+DesktopWriter.generaCadenaJavascript(iconText);
		str +=",\"single\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(single))
		+",\"ignoreMargins\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(ignoreMargins))
		+",\"sessionSave\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(sessionSave))
		+",\"showInAllDesktops\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(showInAllDesktops))
		+",\"noResize\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(noResize));
		if (status!=null && status.length()>0)
			str += ",\"status\":"+DesktopWriter.generaCadenaJavascript(status);
		if (engine!=null && engine.length()>0)
			str += ",\"engine\":"+DesktopWriter.generaCadenaJavascript(engine);
		if (helpUrl!=null && helpUrl.length()>0)
			str += ",\"helpUrl\":"+DesktopWriter.generaCadenaJavascript(helpUrl);
		if (relativeHelpUrl!=null && relativeHelpUrl.length()>0)
			str += ",\"relativeHelpUrl\":"+DesktopWriter.generaCadenaJavascript(relativeHelpUrl);
		if (js!=null && js.length()>0)
			str +=",\"js\":"+DesktopWriter.generaCadenaJavascript(js);
		
		if (description!=null && description.length()>0)
			str += ",\"description\":"+DesktopWriter.generaCadenaJavascript(description);
		if (innerWidth!=0)
			str +=",\"innerWidth\":"+DesktopWriter.generaCadenaJavascript(Integer.toString(innerWidth));
		if (innerHeight!=0)
			str +=",\"innerHeight\":"+DesktopWriter.generaCadenaJavascript(Integer.toString(innerHeight));
		if (zIndex!=-1)
			str +=",\"zIndex\":"+DesktopWriter.generaCadenaJavascript(Integer.toString(zIndex));
		
		////////////////////////////
		// String
		if (onClose!=null && onClose.length()>0)
			str += ",\"onClose\":"+DesktopWriter.generaCadenaJavascript(onClose);
		if (chromeType!=null && chromeType.length()>0)
			str += ",\"chromeType\":"+DesktopWriter.generaCadenaJavascript(chromeType);
		if (testigo!=null && testigo.length()>0)
			str += ",\"testigo\":"+DesktopWriter.generaCadenaJavascript(testigo);
		if (testigoJson!=null && testigoJson.length()>0)
			str += ",\"testigoJson\":"+DesktopWriter.generaCadenaJavascript(testigoJson);
		if (skin!=null && skin.length()>0)
			str += ",\"skin\":"+DesktopWriter.generaCadenaJavascript(skin);
		if (imageFamily!=null && imageFamily.length()>0)
			str += ",\"imageFamily\":"+DesktopWriter.generaCadenaJavascript(imageFamily);
		if (virtualDesktop!=null && virtualDesktop.length()>0)
			str += ",\"virtualDesktop\":"+DesktopWriter.generaCadenaJavascript(virtualDesktop);
		
		
		// boolean
		if (resizable)
			str +=",\"resizable\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(resizable));
		if (noTaskbar)
			str +=",\"noTaskbar\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(noTaskbar));
		if (noHide)
			str +=",\"noHide\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(noHide));
		if (noDrag)
			str +=",\"noDrag\":"+DesktopWriter.generaCadenaJavascript(Boolean.toString(noDrag));

		
		// Integer
		if (minWidth!=0)
			str +=",\"minWidth\":"+DesktopWriter.generaCadenaJavascript(Integer.toString(minWidth));
		if (minHeight!=0)
			str +=",\"minHeight\":"+DesktopWriter.generaCadenaJavascript(Integer.toString(minHeight));
		if (maxWidth!=0)
			str +=",\"maxWidth\":"+DesktopWriter.generaCadenaJavascript(Integer.toString(maxWidth));
		if (maxHeight!=0)
			str +=",\"maxHeight\":"+DesktopWriter.generaCadenaJavascript(Integer.toString(maxHeight));

		
		///////////////////////////////
		// daemons
		if (daemons != null && daemons.length>0){
			str +=",\"daemons\":[";
			int longDaemons = daemons.length;
			for (int i=0;i<longDaemons;i++){
				Daemon daem=daemons[i];
				str += daem.toString();
				if (i!=(longDaemons-1))
					str+= ",";
			}
			str +="]";
		}
		
		// app params
		if (parameters != null && parameters.length>0){
			str +=",\"parameters\":[";
			int longAppParams = parameters.length;
			for (int i=0;i<longAppParams;i++){
				AppParameter appPar=parameters[i];
				str += appPar.toString();
				if (i!=(longAppParams-1))
					str+= ",";
			}
			str +="]";
		}
		
		str += "}";
	
		return str;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
		if (this.title==null)
			this.title="";
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
		if (this.url==null)
			this.url="about:blank";
	}


	public int getWidth() {
		return width;
	}


	public void setWidth(int width) {
		this.width = width;		
	}


	public int getHeight() {
		return height;
	}


	public void setHeight(int height) {
		this.height = height;
	}


	public String getPosition() {
		return position;
	}


	public void setPosition(String position) {
		this.position = position;
		if (this.position==null)
			this.position="100,100";
	}


	public String getIcon() {
		return icon;
	}


	public void setIcon(String icon) {
		this.icon = icon;
		if (this.icon==null)
			this.icon="iconNULL";
	}


	public String getIconText() {
		return iconText;
	}


	public void setIconText(String iconText) {
		this.iconText = iconText;
		if (this.iconText==null)
			this.iconText="No description";
	}


	public boolean isSingle() {
		return single;
	}


	public void setSingle(boolean single) {
		this.single = single;
	}


	public boolean isIgnoreMargins() {
		return ignoreMargins;
	}


	public void setIgnoreMargins(boolean ignoreMargins) {
		this.ignoreMargins = ignoreMargins;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
		if (this.status==null)
			this.status="No status";
	}


	public boolean isSessionSave() {
		return sessionSave;
	}


	public void setSessionSave(boolean sessionSave) {
		this.sessionSave = sessionSave;
	}


	public boolean isShowInAllDesktops() {
		return showInAllDesktops;
	}


	public void setShowInAllDesktops(boolean showInAllDesktops) {
		this.showInAllDesktops = showInAllDesktops;
	}


	public int getInnerWidth() {
		return innerWidth;
	}


	public void setInnerWidth(int innerWidth) {
		this.innerWidth = innerWidth;		
	}


	public int getInnerHeight() {
		return innerHeight;
	}


	public void setInnerHeight(int innerHeight) {
		this.innerHeight = innerHeight;
	}


	public int getZIndex() {
		return zIndex;
	}


	public void setZIndex(int index) {
		zIndex = index;
	}


	public boolean isNoResize() {
		return noResize;
	}


	public void setNoResize(boolean noResize) {
		this.noResize = noResize;
	}


	public String getJs() {
		return js;
	}


	public void setJs(String js) {
		this.js = js;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	public String getEngine() {
		return engine;
	}

	public void setEngine(String engine) {
		this.engine = engine;
	}

	public String getHelpUrl() {
		return helpUrl;
	}

	public void setHelpUrl(String helpUrl) {
		this.helpUrl = helpUrl;
	}

	public int getMinWidth() {
		return minWidth;
	}

	public void setMinWidth(int minWidth) {
		this.minWidth = minWidth;
	}

	public int getMinHeight() {
		return minHeight;
	}

	public void setMinHeight(int minHeight) {
		this.minHeight = minHeight;
	}

	public int getMaxWidth() {
		return maxWidth;
	}

	public void setMaxWidth(int maxWidth) {
		this.maxWidth = maxWidth;
	}

	public int getMaxHeight() {
		return maxHeight;
	}

	public void setMaxHeight(int maxHeight) {
		this.maxHeight = maxHeight;
	}

	public String getOnClose() {
		return onClose;
	}

	public void setOnClose(String onClose) {
		this.onClose = onClose;
	}

	public String getChromeType() {
		return chromeType;
	}

	public void setChromeType(String chromeType) {
		this.chromeType = chromeType;
	}

	public boolean isResizable() {
		return resizable;
	}

	public void setResizable(boolean resizable) {
		this.resizable = resizable;
	}

	public String getTestigo() {
		return testigo;
	}

	public void setTestigo(String testigo) {
		this.testigo = testigo;
	}

	public String getTestigoJson() {
		return testigoJson;
	}

	public void setTestigoJson(String testigoJson) {
		this.testigoJson = testigoJson;
	}

	public String getSkin() {
		return skin;
	}

	public void setSkin(String skin) {
		this.skin = skin;
	}

	public boolean isNoTaskbar() {
		return noTaskbar;
	}

	public void setNoTaskbar(boolean noTaskbar) {
		this.noTaskbar = noTaskbar;
	}

	public String getImageFamily() {
		return imageFamily;
	}

	public void setImageFamily(String imageFamily) {
		this.imageFamily = imageFamily;
	}

	public boolean isNoHide() {
		return noHide;
	}

	public void setNoHide(boolean noHide) {
		this.noHide = noHide;
	}

	public String getVirtualDesktop() {
		return virtualDesktop;
	}

	public void setVirtualDesktop(String virtualDesktop) {
		this.virtualDesktop = virtualDesktop;
	}

	public boolean isNoDrag() {
		return noDrag;
	}

	public void setNoDrag(boolean noDrag) {
		this.noDrag = noDrag;
	}

	public String getRelativeHelpUrl() {
		return relativeHelpUrl;
	}

	public void setRelativeHelpUrl(String relativeHelpUrl) {
		this.relativeHelpUrl = relativeHelpUrl;
	}

	public String getRelativeUrl() {
		return relativeUrl;
	}

	public void setRelativeUrl(String relativeUrl) {
		this.relativeUrl = relativeUrl;
	}
	
}
