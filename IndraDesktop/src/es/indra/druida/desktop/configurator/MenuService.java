package es.indra.druida.desktop.configurator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader;
import es.indra.druida.desktop.configurator.beans.MenuItem;
import es.indra.druida.desktop.remoting.ServerResponse;

/**
 * The Class MenuService.
 */
public class MenuService {

	/**
	 * Sets the menu object.
	 * 
	 * @param opcMenu
	 *            the opc menu
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @return the server response
	 */
	public ServerResponse setMenuObject(MenuItem[] opcMenu, HttpServletRequest request, HttpServletResponse response)	{
		ServerResponse resp=new ServerResponse();		
		try	{
			//boolean estoyEnSesion=sesion.getAttribute("userConfig")!=null;
			//System.out.println("ECOOO "+opcMenu.length);
			AbstractLoader loader=DesktopConfig.getSingleton().getMenuLoader(request, response);
			loader.setObjects(opcMenu);
			//DesktopConfig.getSingleton().getWindowLoader(request, response);
		
		return null;
		} catch (Exception e)	{
			resp.setErrorDeOperacion(true);
			resp.setExcepcionError(e);
		}
		//if (estoyEnSesion)	{
		//	DesktopUserConfig usuario=((DesktopUserConfig)sesion.getAttribute("userConfig"));
			//return usuario;
		//}		
		return resp;	
	}


	
}
