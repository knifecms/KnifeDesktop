package es.indra.druida.desktop.configurator;

import javax.servlet.http.HttpSession;

/**
 * The Class ConfigurationService.
 */
public class ConfigurationService {
	
	/**
	 * Gets the user config.
	 * 
	 * @param sesion
	 *            the sesion
	 * 
	 * @return the user config
	 */
	public DesktopUserConfig getUserConfig(HttpSession sesion)	{
		boolean estoyEnSesion=sesion.getAttribute("userConfig")!=null;
		if (estoyEnSesion)	{
			DesktopUserConfig usuario=((DesktopUserConfig)sesion.getAttribute("userConfig"));
			return usuario;
		}		
		return null;	
	}
	
	public boolean isUserInSession(HttpSession sesion)	{
		return (sesion.getAttribute("userConfig")!=null);
	}
	
}
