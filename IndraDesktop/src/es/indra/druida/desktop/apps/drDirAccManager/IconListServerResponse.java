package es.indra.druida.desktop.apps.drDirAccManager;

import es.indra.druida.desktop.remoting.ServerResponse;
import java.util.ArrayList;

public class IconListServerResponse extends ServerResponse {
	/** The icons. */
	private ArrayList<DrIconInfo> icons;

	public ArrayList<DrIconInfo> getIcons() {
		return icons;
	}

	public void setIcons(ArrayList<DrIconInfo> icons) {
		this.icons = icons;
	}
}
