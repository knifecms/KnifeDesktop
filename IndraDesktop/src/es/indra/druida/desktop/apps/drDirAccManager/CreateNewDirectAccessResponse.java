package es.indra.druida.desktop.apps.drDirAccManager;

import es.indra.druida.desktop.configurator.beans.AppGroup;
import es.indra.druida.desktop.configurator.beans.Application;
import es.indra.druida.desktop.remoting.ServerResponse;

public class CreateNewDirectAccessResponse extends ServerResponse {
	private Application[] applications;
	private AppGroup[] appGroups;
	private String newAppId;
	
	public AppGroup[] getAppGroups() {
		return appGroups;
	}
	public void setAppGroups(AppGroup[] appGroups) {
		this.appGroups = appGroups;
	}
	public Application[] getApplications() {
		return applications;
	}
	public void setApplications(Application[] applications) {
		this.applications = applications;
	}
	public String getNewAppId() {
		return newAppId;
	}
	public void setNewAppId(String newAppId) {
		this.newAppId = newAppId;
	}
	
}
