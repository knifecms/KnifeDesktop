package es.indra.druida.desktop.apps.drDirAccManager;

import java.io.File;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import es.indra.druida.desktop.configurator.AbstractDesktopConfigurationClass;
import es.indra.druida.desktop.configurator.DesktopConfig;
import es.indra.druida.desktop.configurator.DesktopUserConfig;
import es.indra.druida.desktop.configurator.beans.AppGroup;
import es.indra.druida.desktop.configurator.beans.Application;
import es.indra.druida.desktop.remoting.ServerResponse;

public class DrDAManagerService {
	
	public IconListServerResponse getIcons(HttpSession sesion){
		IconListServerResponse resp=new IconListServerResponse();
		try	{
			if (sesion==null)	{
				//throw new Exception(ResourcesService.getResourceBundle("drExplorer",sesion).getString("drExplorer.txt52"));//Session expired.
				throw new Exception("Session expired.");//Session expired.
			}
			DesktopConfig config = DesktopConfig.getSingleton();
			String urlIcons=config.get("urlIcons"); // /DruidaDesktopV2/drDesktop/img/icons
			String iconsDir=config.get("iconsDir");	 // /D:/ECLIPSE/WORK_SPACES/CALLISTO_2/DruidaDesktopV2/WebContent/drDesktop/img/icons		

			File iconsDirFile=new File(iconsDir);
			if (!iconsDirFile.exists())
				//throw new Exception(ResourcesService.getResourceBundle("drExplorer",sesion).getString("drExplorer.txt55"));//"Base directory doesn't exist."
				throw new Exception("No existe el directorio de iconos '"+iconsDir+"'.");//"Base directory doesn't exist."
			
			ArrayList<DrIconInfo> iconos=new ArrayList<DrIconInfo>();
			File[] listfiles=iconsDirFile.listFiles();
			for (int i=0; i<listfiles.length; i++){
				if (!listfiles[i].isDirectory()){
					/*
					String nombreCompleto=listfiles[i].getName();
					String extension="";
					int indicePunto=nombreCompleto.lastIndexOf(".");
					if (indicePunto!=-1){
						extension=nombreCompleto.substring(indicePunto+1).toLowerCase();
					}
					if (extension!=null && extension.equals("gif")||extension.equals("jpg")||extension.equals("png")){
						DrIconInfo iconInfo=new DrIconInfo();
						iconInfo.setName(nombreCompleto);
						iconInfo.setUrl(urlIcons+"/"+nombreCompleto);
						iconos.add(iconInfo);
					}*/
					meteIconoEnLista(listfiles[i],iconos, urlIcons, null);
				}else if (! listfiles[i].getName().equals("dev")){//Era un directorio y no es el directorio reservado "dev"
					File[] listFilesSubDir=listfiles[i].listFiles();
					for (int j=0; j<listFilesSubDir.length; j++){
						meteIconoEnLista(listFilesSubDir[j],iconos, urlIcons, listfiles[i].getName());
					}
				}
			}
			
			resp.setIcons(iconos);
			
		}catch (Exception e)	{
			resp.setErrorDeOperacion(true);
			resp.setExcepcionError(e);
		}
		return resp;
	}

	private void meteIconoEnLista(File file, ArrayList<DrIconInfo> iconos, String urlIcons, String subDir) {
		if (!file.isDirectory()){
			String nombreCompleto=file.getName();
			String extension="";
			int indicePunto=nombreCompleto.lastIndexOf(".");
			if (indicePunto!=-1){
				extension=nombreCompleto.substring(indicePunto+1).toLowerCase();
			}
			if (extension!=null && extension.equals("gif")||extension.equals("jpg")||extension.equals("png")){
				DrIconInfo iconInfo=new DrIconInfo();
				if (subDir!=null){
					iconInfo.setName(subDir+"/"+nombreCompleto);
					iconInfo.setUrl(urlIcons+"/"+subDir+"/"+nombreCompleto);					
				}else{
					iconInfo.setName(nombreCompleto);
					iconInfo.setUrl(urlIcons+"/"+nombreCompleto);	
				}
				iconos.add(iconInfo);
			}
		}
	}

	public CreateNewDirectAccessResponse createNewDirectAccess(DirectAccess newDirectAcess, HttpServletRequest request, HttpServletResponse response)	{
		CreateNewDirectAccessResponse resp=new CreateNewDirectAccessResponse();
		try	{
			DesktopConfig config = DesktopConfig.getSingleton();
			//Creo el grupo si no existe
			AppGroup grupo=new AppGroup("customDirectAccesses", "Accesos Directos Personalizados");
			Object[] datosParaLoaderAppGrp = {grupo};
			config.getAppGroupLoader(request, response).addObjects(datosParaLoaderAppGrp);
			//Creo la aplicacion nueva
			newDirectAcess.setDescription("Acceso Directo Personalizado: "+newDirectAcess.getTitle());
			newDirectAcess.setIconText(newDirectAcess.getTitle());	
			newDirectAcess.setGroupId("customDirectAccesses");
			Object[] datosParaLoaderApp = {newDirectAcess};
			config.getAppLoader(request, response).addObjects(datosParaLoaderApp);//A�ade el acceso directo
			//Y RECARGAMOS
			config.reloadApplications(); //Recarga la lista general de aplicaciones (comun para todos los users) 
			AbstractDesktopConfigurationClass configUser = config.invokeDesktopConfigurationClass(request, response);
			configUser.reloadUser();//Recarga la lista que se creo con las aplicaciones permitidas al usuario
			//Copiamos la lista de aplicaciones y grupos del usuario en la respuesta
			resp.setAppGroups(config.getAppGroups());
			resp.setNewAppId(newDirectAcess.getId());
			DesktopUserConfig userConfig = (DesktopUserConfig) request.getSession().getAttribute("userConfig");
			resp.setApplications(userConfig.getApplications());
		} catch (Exception e)	{
			resp.setErrorDeOperacion(true);
			resp.setExcepcionError(e);
		}
		return resp;	
	}

	private boolean existeGrupoDeAplicaciones(String id, DesktopConfig config) {
		AppGroup[] appGroup = config.getAppGroups();
		int l = appGroup.length;
		for ( int i = 0 ; i < l; i++) {
			if (appGroup[i].getName().equals(id))
				return true;
		}
		return false;
	}
	
}
