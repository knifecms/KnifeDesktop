package es.indra.druida.desktop.apps.drDirAccManager;


/**
 * The Class DrFileInfo.
 */
public class DrIconInfo {
	
	/** The name. */
	private String name;
	
	/** The relative path. */
	private String url;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	
}
