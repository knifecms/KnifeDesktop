package es.indra.druida.desktop.apps.uploadAppWizard;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.tools.ant.Project;

import es.indra.druida.desktop.ant.AntJspLauncher;
import es.indra.druida.desktop.configurator.DesktopConfig;

/**
 * The Class UploadApplicationWizard.
 */
public class UploadApplicationWizard {
	
	/** The app name. */
	private String appName;
	
	/** The app file. */
	private String appFile;
	
	/**
	 * Ejecuta script sobre fichero.
	 * 
	 * @param out
	 *            the out
	 * 
	 * @return the boolean
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public Boolean ejecutaScriptSobreFichero(JspWriter out, ResourceBundle bundle) throws Exception	{
		DesktopConfig dConfig= DesktopConfig.getInstance();
		AntJspLauncher antLauncher= new AntJspLauncher();
		int messageLevel=Project.MSG_INFO; //Project.MSG_INFO - - >  //MSG_ERR, MSG_WARN, MSG_INFO, MSG_VERBOSE, MSG_DEBUG
		//String buildFileName="/ECLIPSE/WORK_SPACES/CALLISTO/antLauncher/WebContent/WEB-INF/scripts/InstallApplication.ant.xml";
		String buildFileName=dConfig.get("scriptDir")+"/InstallApplication.ant.xml";
		HashMap<String, String> props=new HashMap<String, String>();
		props.put("tmpDir",dConfig.get("tmpDir"));// "/ECLIPSE/WORK_SPACES/CALLISTO/DruidaDesktopV2/scripts/tmp"
		props.put("appFile",getAppFile());//"/ECLIPSE/WORK_SPACES/CALLISTO/DruidaDesktopV2/aplicaciones/drConsoleTest.dap"
		props.put("appName",getAppName());//"drConsoleTest"
		props.put("applicationsXmlDir",dConfig.get("appDefRoot")); // "/ECLIPSE/WORK_SPACES/CALLISTO/DruidaDesktopV2/WebContent/WEB-INF/applications/"
		props.put("applicationsDir",dConfig.get("appRoot"));//"/ECLIPSE/WORK_SPACES/CALLISTO/DruidaDesktopV2/WebContent/apps"
		props.put("iconsDir",dConfig.get("iconsDir") ); //"/ECLIPSE/WORK_SPACES/CALLISTO/antLauncher/DruidaDesktopV2/drDesktop/img/icons"		
		props.put("urlAppRoot",dConfig.get("urlAppRoot") ); //"/ECLIPSE/WORK_SPACES/CALLISTO/antLauncher/DruidaDesktopV2/drDesktop/img/icons"
		props.put("appRegisterFile",dConfig.get("root")+"/WEB-INF/drDesktop-registeredApp.xml" ); //"/ECLIPSE/WORK_SPACES/CALLISTO/antLauncher/DruidaDesktopV2/drDesktop/img/icons"		
		props.put("skinsDir",dConfig.get("skinsDir") ); 
		props.put("skinsFile",dConfig.get("skinsFile") );
		props.put("urlHelpRoot",dConfig.get("urlHelpRoot") );
		props.put("helpDir",dConfig.get("helpDir") );
		
		//Propiedades relacionadas con la instalación de portlets: 
		String plutoDeployPath=DesktopConfig.getSingleton().getEngineConfigParam("portletEngine","plutoDeployPath");
		if (plutoDeployPath!=null)
			props.put("plutoDeployPath",plutoDeployPath);
		
		//ResourcesDir
		String classesDir= dConfig.get("classesDir");
		if (classesDir==null)
			props.put("resourcesDir",dConfig.get("root")+"/WEB-INF/classes/resources" );
		else
			props.put("resourcesDir",classesDir+"/resources" );
		
		//I18N
		props.put("drUploadDapWizard.scriptTxt1", bundle.getString("drUploadDapWizard.scriptTxt1"));
		props.put("drUploadDapWizard.scriptTxt2", bundle.getString("drUploadDapWizard.scriptTxt2"));
		
		//int messageLevel=Project.MSG_VERBOSE;
		antLauncher.setBuildFileName(buildFileName);
		antLauncher.setMessageOutputlevel(messageLevel);
		antLauncher.setProperties(props);
		antLauncher.setJspOut(out);
		//Boolean ok=antLauncher.launchDefaultTarget(buildFileName, props, out, messageLevel);
		Boolean ok=antLauncher.launch();
		return ok;
	}
	
	
	/**
	 * Procesa fichero.
	 * 
	 * @param request
	 *            the request
	 * 
	 * @return the boolean
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public Boolean procesaFichero(HttpServletRequest request) throws Exception	{
		Boolean ok=procesaUploadFichero(request); //Graba el fichero
		return ok;
	}
	
	/**
	 * Procesa upload fichero.
	 * 
	 * @param request
	 *            the request
	 * 
	 * @return the boolean
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public Boolean procesaUploadFichero(HttpServletRequest request) throws Exception	{ //Graba el fichero
		//Check that we have a file upload request
		if (!ServletFileUpload.isMultipartContent(request))
			return false;
		//----------		
		//Tomo las variables de configuración
		String tmpDir=DesktopConfig.getInstance().get("tmpDir");
		//----------		
		//Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		//Set factory constraints
		//factory.setSizeThreshold(yourMaxMemorySize);
		factory.setRepository(new File(tmpDir));
		//Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		//Set overall request size constraint
		//upload.setSizeMax(yourMaxRequestSize);
		//Parse the request
		List /* FileItem */ items = upload.parseRequest(request);
		//----------
		//Process the uploaded items
		Iterator iter = items.iterator();
		while (iter.hasNext()) {
		    FileItem item = (FileItem) iter.next();
		    if (item.isFormField()) {
		        processFormField(item);
		    } else {
		        processUploadedFile(tmpDir,item);
		    }
		}
		return true;
	}

	/**
	 * Process uploaded file.
	 * 
	 * @param tmpDir
	 *            the tmp dir
	 * @param item
	 *            the item
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private void processUploadedFile(String tmpDir,FileItem item) throws Exception { //Cualquier fichero que envie es guardado
	    //String fieldName = item.getFieldName();
	    //String contentType = item.getContentType();
	    //boolean isInMemory = item.isInMemory();
	    //long sizeInBytes = item.getSize();		
	    String completeFileName = item.getName(); //Por ejemplo "C:\drConsoleTest.dap" 
	    String[] pathPieces=completeFileName.split("/|\\\\");
	    String fileName=pathPieces[pathPieces.length-1];
	    
	    //File originalFile=new File(completeFileName);
	    //String fileName=originalFile.getName();//Por ejemplo "drConsoleTest.dap"
	    //Eso no funciona por conflictos unix/windows
	    String fileNameNoExt=fileName;	    
	    int i=fileName.lastIndexOf(".");
	    if (i!=-1)	{
	    	fileNameNoExt=fileName.substring(0,i); //Por ejemplo "drConsoleTest" 
	    	this.setAppName(fileNameNoExt); //appName queda con el nombre del último fichero subido, ojo si subimos varios
	    }
	    File uploadedFile = new File(tmpDir+"/"+fileName);
	    this.setAppFile(tmpDir+"/"+fileName);//appFile queda con el nombre del último fichero subido, ojo si subimos varios
	    item.write(uploadedFile);
		/*Otra forma: 
		InputStream uploadedStream = item.getInputStream();
		......
   		uploadedStream.close();*/
	}

	/**
	 * Process form field.
	 * 
	 * @param item
	 *            the item
	 */
	private void processFormField(FileItem item) {
		
	}


	/**
	 * Gets the app name.
	 * 
	 * @return the app name
	 */
	public String getAppName() {
		return appName;
	}


	/**
	 * Sets the app name.
	 * 
	 * @param appName
	 *            the new app name
	 */
	public void setAppName(String appName) {
		this.appName = appName;
	}


	/**
	 * Gets the app file.
	 * 
	 * @return the app file
	 */
	public String getAppFile() {
		return appFile;
	}


	/**
	 * Sets the app file.
	 * 
	 * @param appFile
	 *            the new app file
	 */
	public void setAppFile(String appFile) {
		this.appFile = appFile;
	}
}
