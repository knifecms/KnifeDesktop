package es.indra.druida.desktop.apps;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import es.indra.druida.desktop.configurator.DesktopConfig;

/**
 * The Class RssServlet.
 */
public class RssServlet extends HttpServlet {

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	public void init(ServletConfig conf) throws ServletException {
		super.init(conf);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void service(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		DesktopConfig dc=null;
		try {
			dc = DesktopConfig.getSingleton();
		} catch (Exception e) {
			StringWriter sWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(sWriter));
			Logger.getLogger(RssServlet.class.getName()).severe(sWriter.getBuffer().toString());
		}
		String root = dc.get("root");
		
		res.setContentType("text/html");
		
		PrintWriter out = res.getWriter();
		
		Reader in= new FileReader(root + "/drDesktop/html/salida.html");
	    char[] buffer= new char[256];
	    while (true) {
	      int n= in.read(buffer);
	      if (n < 0)
		break;
	      out.write(buffer, 0, n);
	    }
	    in.close();
	    out.close();
	}

}
