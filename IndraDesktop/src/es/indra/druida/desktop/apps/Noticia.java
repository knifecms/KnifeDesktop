package es.indra.druida.desktop.apps;

/**
 * The Class Noticia.
 */
public class Noticia {

	/** The title. */
	private String title = "";
	
	/** The description. */
	private String description = "";
	
	/** The link. */
	private String link = "";
	
	/** The pub date. */
	private String pubDate = "";
	
	/** The author. */
	private String author = "";
	
	/**
	 * Gets the author.
	 * 
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	
	/**
	 * Sets the author.
	 * 
	 * @param author
	 *            the new author
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	
	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Gets the link.
	 * 
	 * @return the link
	 */
	public String getLink() {
		return link;
	}
	
	/**
	 * Sets the link.
	 * 
	 * @param link
	 *            the new link
	 */
	public void setLink(String link) {
		this.link = link;
	}
	
	/**
	 * Gets the pub date.
	 * 
	 * @return the pub date
	 */
	public String getPubDate() {
		return pubDate;
	}
	
	/**
	 * Sets the pub date.
	 * 
	 * @param pubDate
	 *            the new pub date
	 */
	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}
	
	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
}
