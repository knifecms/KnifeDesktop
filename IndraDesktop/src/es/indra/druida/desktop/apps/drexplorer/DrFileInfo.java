package es.indra.druida.desktop.apps.drexplorer;


/**
 * The Class DrFileInfo.
 */
public class DrFileInfo {
	
	/** The name. */
	private String name;
	
	/** The relative path. */
	private String relativePath;	
	
	/** The is directory. */
	private boolean isDirectory;
	
	/** The is hidden. */
	private boolean isHidden;
	
	/** The can read. */
	private boolean canRead;
	
	/** The can write. */
	private boolean canWrite;	
	
	/** The last modified. */
	private long lastModified;
	
	/** The length. */
	private long length;
	
	/**
	 * Checks if is can read.
	 * 
	 * @return true, if is can read
	 */
	public boolean isCanRead() {
		return canRead;
	}
	
	/**
	 * Sets the can read.
	 * 
	 * @param canRead
	 *            the new can read
	 */
	public void setCanRead(boolean canRead) {
		this.canRead = canRead;
	}
	
	/**
	 * Checks if is can write.
	 * 
	 * @return true, if is can write
	 */
	public boolean isCanWrite() {
		return canWrite;
	}
	
	/**
	 * Sets the can write.
	 * 
	 * @param canWrite
	 *            the new can write
	 */
	public void setCanWrite(boolean canWrite) {
		this.canWrite = canWrite;
	}
	
	/**
	 * Checks if is directory.
	 * 
	 * @return true, if is directory
	 */
	public boolean isDirectory() {
		return isDirectory;
	}
	
	/**
	 * Sets the directory.
	 * 
	 * @param isDirectory
	 *            the new directory
	 */
	public void setDirectory(boolean isDirectory) {
		this.isDirectory = isDirectory;
	}
	
	/**
	 * Checks if is hidden.
	 * 
	 * @return true, if is hidden
	 */
	public boolean isHidden() {
		return isHidden;
	}
	
	/**
	 * Sets the hidden.
	 * 
	 * @param isHidden
	 *            the new hidden
	 */
	public void setHidden(boolean isHidden) {
		this.isHidden = isHidden;
	}
	
	/**
	 * Gets the last modified.
	 * 
	 * @return the last modified
	 */
	public long getLastModified() {
		return lastModified;
	}
	
	/**
	 * Sets the last modified.
	 * 
	 * @param lastModified
	 *            the new last modified
	 */
	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the relative path.
	 * 
	 * @return the relative path
	 */
	public String getRelativePath() {
		return relativePath;
	}
	
	/**
	 * Sets the relative path.
	 * 
	 * @param relativePath
	 *            the new relative path
	 */
	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}
	
	/**
	 * Gets the length.
	 * 
	 * @return the length
	 */
	public long getLength() {
		return length;
	}
	
	/**
	 * Sets the length.
	 * 
	 * @param length
	 *            the new length
	 */
	public void setLength(long length) {
		this.length = length;
	}

	/*listfiles[i].lastModified(); //long
	*/
	
}
