package es.indra.druida.desktop.apps.drexplorer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;

import es.indra.druida.desktop.configurator.DesktopUserConfig;

import java.util.ResourceBundle;
import es.indra.druida.desktop.configurator.ResourcesService;

/**
 * The Class DrExplorerService.
 */
public class DrExplorerService {
	
	/* este no se llama con DWR */
	/**
	 * Gets the file.
	 * 
	 * @param path
	 *            the path
	 * @param fileName
	 *            the file name
	 * @param sesion
	 *            the sesion
	 * 
	 * @return the file
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public File getFile(String path, String fileName, HttpSession sesion) throws Exception {
		File baseDir=getBasedir(sesion); //verifica tb. la sesion
		File directorioDestino=getSubdir(baseDir,path,sesion); //Verifica tb que existe
		dameRutaRelativa(baseDir,directorioDestino,sesion); //Verifica la seguridad
		File fileToDownload = new File(directorioDestino, fileName);
		if (!fileToDownload.exists()){
			throw new Exception ( ResourcesService.getResourceBundle("drExplorer",sesion).getString("drExplorer.txt43") );
		}
		if (fileToDownload.isDirectory()){
			throw new Exception (ResourcesService.getResourceBundle("drExplorer",sesion).getString("drExplorer.txt44"));
		}
		return fileToDownload;
	} 
	
	/* este no se llama con DWR */
	/**
	 * Upload file.
	 * 
	 * @param path
	 *            the path
	 * @param fichero
	 *            the fichero
	 * @param sesion
	 *            the sesion
	 * 
	 * @return true, if successful
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public boolean uploadFile(String path,FileItem fichero, HttpSession sesion) throws Exception	{ //DWR2.0 no me permite llamar a esto desde js, lo llamo desde UploadFile
		File baseDir=getBasedir(sesion); //verifica tb. la sesion
		File directorioDestino=getSubdir(baseDir,path,sesion); //Verifica tb que existe
		dameRutaRelativa(baseDir,directorioDestino,sesion); //Verifica la seguridad
	    String completeFileName = fichero.getName(); //Por ejemplo "C:\drConsoleTest.dap" 

	    //Ciertas verificaciones ya se realizan al llamar a getBasedir as� que no se repiten
		DesktopUserConfig userConfig = (DesktopUserConfig) sesion.getAttribute("userConfig"); 
		String[] roles=userConfig.getRoles();
		if (roles!=null){
			for (int i=0; i<roles.length; i++)
				if (roles[i].toLowerCase().equals("nofileupload"))
					throw new Exception(ResourcesService.getResourceBundle("drExplorer",sesion).getString("drExplorer.txt57"));//"User don't have upload privileges."
		}
	    
	    //Esto no funciona por conflictos unix/windows
	    //File originalFile=new File(completeFileName);
	    //String fileName=originalFile.getName();//Por ejemplo "drConsoleTest.dap"
	    String[] pathPieces=completeFileName.split("/|\\\\");
	    String fileName=pathPieces[pathPieces.length-1];
	    
	    File uploadedFile = new File(directorioDestino, fileName);
	    fichero.write(uploadedFile);
		return true;
	}
	

	/**
	 * Copy move elements.
	 * 
	 * @param srcPath
	 *            the src path
	 * @param destpath
	 *            the destpath
	 * @param names
	 *            the names
	 * @param isCut
	 *            the is cut
	 * @param sesion
	 *            the sesion
	 * 
	 * @return the dr explorer response
	 */
	public DrExplorerResponse copyMoveElements (String srcPath, String destpath, String names [] ,boolean isCut, HttpSession sesion)	{
		DrExplorerResponse resp=new DrExplorerResponse();
		try	{
			File baseDir=getBasedir(sesion); //verifica tb. la sesion
			File directorioOrigen=getSubdir(baseDir,srcPath,sesion); //Verifica tb que existe			
			File directorioDestino=getSubdir(baseDir,destpath,sesion); //Verifica tb que existe
			//String relativePathSrcDir=dameRutaRelativa(baseDir,directorioOrigen);//Verifica tb la seguridad
			String relativePathDestDir=dameRutaRelativa(baseDir,directorioDestino,sesion);//Verifica tb la seguridad	
			// verifico primero que existen todos los elementos 
			File listaFicherosOrigen []=new File[names.length];
			File listaFicherosDestino []=new File[names.length];
			for (int i=0; i<names.length; i++){
				String name=names[i];
				listaFicherosDestino[i]=new File(directorioDestino,name);
				listaFicherosOrigen[i]=new File(directorioOrigen,name);				
				dameRutaRelativa(baseDir,listaFicherosDestino[i],sesion);//Verifica la seguridad
				dameRutaRelativa(baseDir,listaFicherosOrigen[i],sesion);//Verifica la seguridad				
				if (listaFicherosDestino[i].exists())	{//Renombro
					listaFicherosDestino[i]=dameNuevoFichParaCopia(directorioDestino,name,sesion);
				}
				if (!listaFicherosOrigen[i].exists()){
					throw new Exception("'"+name+ResourcesService.getResourceBundle("drExplorer",sesion).getString("drExplorer.txt45"));
				}
			}
			//Ya tengo verificado que los ficheros de origen existen y estan dentro de la zona segura 
			// y que los ficheros de destino no existen aun y estar�n en la zona segura
			if (isCut){
				moveElements(listaFicherosOrigen,listaFicherosDestino,sesion);
			}else{
				copyElements(listaFicherosOrigen,listaFicherosDestino,sesion);
			}
			resp.setRelativePath(relativePathDestDir);
			resp.setFiles( getFilesOf(directorioDestino) );	
		} catch (Exception e){
			resp.setErrorDeOperacion(true);
			resp.setExcepcionError(e);
		}
		return resp;
	}
	
	/**
	 * Dame nuevo fich para copia.
	 * 
	 * @param directorioDestino
	 *            the directorio destino
	 * @param name
	 *            the name
	 * @param sesion 
	 * 
	 * @return the file
	 */
	private File dameNuevoFichParaCopia(File directorioDestino, String name, HttpSession sesion) {
        ResourceBundle bundle=ResourcesService.getResourceBundle("drExplorer",sesion);
		int indice=2;
		File ret=new File(directorioDestino,bundle.getString("drExplorer.txt45_2")+" "+bundle.getString("drExplorer.txt45_3")+" "+name);
		while (ret.exists())	{
			ret=new File(directorioDestino,bundle.getString("drExplorer.txt45_2")+" ("+indice+") "+bundle.getString("drExplorer.txt45_3")+" "+name);
			indice++;
		}
		return ret;
	}

	/**
	 * Copy elements.
	 * 
	 * @param listaFicherosOrigen
	 *            the lista ficheros origen
	 * @param listaFicherosDestino
	 *            the lista ficheros destino
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private void copyElements(File[] listaFicherosOrigen, File[] listaFicherosDestino, HttpSession sesion) throws Exception {
		verificaQueNoCopiamosSobreUnSubdirectorio(listaFicherosOrigen,listaFicherosDestino, sesion);
		for (int i=0; i<listaFicherosOrigen.length; i++){
			if (listaFicherosOrigen[i].isDirectory())
				copyDirectory(listaFicherosOrigen[i],listaFicherosDestino[i]);
			else
				copyFile(listaFicherosOrigen[i],listaFicherosDestino[i]);
		}
	}





	/**
	 * Move elements.
	 * 
	 * @param listaFicherosOrigen
	 *            the lista ficheros origen
	 * @param listaFicherosDestino
	 *            the lista ficheros destino
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private void moveElements(File[] listaFicherosOrigen, File[] listaFicherosDestino, HttpSession sesion) throws Exception {
		verificaQueNoCopiamosSobreUnSubdirectorio(listaFicherosOrigen,listaFicherosDestino, sesion);
		for (int i=0; i<listaFicherosOrigen.length; i++){
			if (listaFicherosOrigen[i].getCanonicalPath() != listaFicherosDestino[i].getCanonicalPath())	{
				if (listaFicherosOrigen[i].isDirectory())	{
					copyDirectory(listaFicherosOrigen[i],listaFicherosDestino[i]);
				} else
					copyFile(listaFicherosOrigen[i],listaFicherosDestino[i]);
				deleteFile(listaFicherosOrigen[i]); 
			}
		}		
	}
	
	/**
	 * Verifica que no copiamos sobre un subdirectorio.
	 * 
	 * @param listaFicherosOrigen
	 *            the lista ficheros origen
	 * @param listaFicherosDestino
	 *            the lista ficheros destino
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private void verificaQueNoCopiamosSobreUnSubdirectorio(File[] listaFicherosOrigen, File[] listaFicherosDestino, HttpSession sesion) throws Exception {
		for (int i=0; i<listaFicherosOrigen.length; i++){
			String pathFichOrig=listaFicherosOrigen[i].getCanonicalPath();
			String pathFichDest=listaFicherosDestino[i].getCanonicalPath();
			int ind=pathFichDest.indexOf(pathFichOrig); //Si ==0 entonces dest es subdir de orig
			if ( pathFichOrig!= pathFichDest && listaFicherosOrigen[i].isDirectory() && ind==0)	{
				ResourceBundle bundle=ResourcesService.getResourceBundle("drExplorer",sesion);
				//No he de permitir mover una carpeta dentro de si misma.
				throw new Exception (bundle.getString("drExplorer.txt46")+listaFicherosOrigen[i].getName()+bundle.getString("drExplorer.txt47"));
			}
		}
	}
	
	
	/**
	 * Copy file.
	 * 
	 * @param source
	 *            the source
	 * @param dest
	 *            the dest
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public static void copyFile(File source, File dest) throws Exception{
		if(!dest.exists()){
			dest.createNewFile();
	    }
		InputStream in = null;
		OutputStream out = null;
	    try{
	    	in = new FileInputStream(source);
	        out = new FileOutputStream(dest);
	        byte[] buf = new byte[1024];
	        int len;
	        while((len = in.read(buf)) > 0){
	        	out.write(buf, 0, len);
            }
	    } catch (Exception e) {
	    	throw e;
	    }  finally{
	    	in.close();
		    out.close();
		}
	}
	
	 /**
	 * Copy directory.
	 * 
	 * @param sourceDir
	 *            the source dir
	 * @param destDir
	 *            the dest dir
	 * 
	 * @throws Exception
	 *             the exception
	 */
 	public static void copyDirectory(File sourceDir, File destDir) throws Exception{
		 if(!destDir.exists()){
			 destDir.mkdir();
		 }
		 File[] children = sourceDir.listFiles();
		 for(File sourceChild : children){
			 String name = sourceChild.getName();
			 File destChild = new File(destDir, name);
			 if(sourceChild.isDirectory()){
				 copyDirectory(sourceChild, destChild);
			 } else {
				 copyFile(sourceChild, destChild);
			 }
		 }
	 }	
	

	/**
	 * Rename elements.
	 * 
	 * @param path
	 *            the path
	 * @param newNamesData
	 *            the new names data
	 * @param sesion
	 *            the sesion
	 * 
	 * @return the dr explorer response
	 */
	public DrExplorerResponse renameElements(String path, String newNamesData [][] , HttpSession sesion)	{
		DrExplorerResponse resp=new DrExplorerResponse();
		try	{
			File baseDir=getBasedir(sesion); //verifica tb. la sesion
			File directorioDestino=getSubdir(baseDir,path,sesion); //Verifica tb que existe
			resp.setRelativePath(dameRutaRelativa(baseDir,directorioDestino,sesion));//Verifica tb la seguridad
			//Error si un nuevo nombre ya existe o si se sale del entorno seguro
			File listaNuevosFicheros []=new File[newNamesData.length];
			for (int i=0; i<newNamesData.length; i++){
				String newName=newNamesData[i][1];
				listaNuevosFicheros[i]=new File(directorioDestino,newName);
				dameRutaRelativa(baseDir,listaNuevosFicheros[i],sesion);//Verifica la seguridad
				if (listaNuevosFicheros[i].exists())
					throw new Exception("'"+newName+ ResourcesService.getResourceBundle("drExplorer",sesion).getString("drExplorer.txt48"));//
			}
			//Error si un viejo nombre esta fuera del entorno seguro
			File listaFicheros []=new File[newNamesData.length];			
			for (int i=0; i<newNamesData.length; i++){
				String name=newNamesData[i][0];
				listaFicheros[i]=new File(directorioDestino,name);
				dameRutaRelativa(baseDir,listaFicheros[i],sesion);//Verifica la seguridad
			}
			for (int i=0; i<newNamesData.length; i++){
				listaFicheros[i].renameTo(listaNuevosFicheros[i]);
			}
			resp.setFiles( getFilesOf(directorioDestino) );				
		} catch (Exception e){
			resp.setErrorDeOperacion(true);
			resp.setExcepcionError(e);
		}
		return resp;
	}
	
	/**
	 * Creates the new directory.
	 * 
	 * @param path
	 *            the path
	 * @param newDirectory
	 *            the new directory
	 * @param sesion
	 *            the sesion
	 * 
	 * @return the dr explorer response
	 */
	public DrExplorerResponse createNewDirectory(String path, String newDirectory, HttpSession sesion)	{
		DrExplorerResponse resp=new DrExplorerResponse();
		try	{
			File baseDir=getBasedir(sesion);
			File directorio=getSubdir(baseDir,path,sesion);
			String relativePath=dameRutaRelativa(baseDir,directorio,sesion);
			//resp.setRelativePath(relativePath);
			//Ahora verifico el posible nuevo directorio
			//verifico: que no existe, que se puede crear, que no est� fuera del l�mite (no permito / ni \ ni "..")
			File nuevoDir=new File(directorio,newDirectory);
			if (nuevoDir.exists()){
				ResourceBundle bundle=ResourcesService.getResourceBundle("drExplorer",sesion);
				throw new Exception(bundle.getString("drExplorer.txt49") +newDirectory+ bundle.getString("drExplorer.txt48"));
			}
			//Esto verifica que no nos salimos del entorno seguro
			dameRutaRelativa(baseDir,nuevoDir,sesion);
			//boolean error=nuevoDir.mkdirs();
			nuevoDir.mkdirs();
			//Construyo la lista de ficheros y directorios			
			resp.setRelativePath(relativePath);
			resp.setFiles( getFilesOf(directorio) );			
		} catch (Exception e){
			resp.setErrorDeOperacion(true);
			resp.setExcepcionError(e);
		}
		return resp; 
	}
	
	/**
	 * Delete elements.
	 * 
	 * @param path
	 *            the path
	 * @param names
	 *            the names
	 * @param sesion
	 *            the sesion
	 * 
	 * @return the dr explorer response
	 */
	public DrExplorerResponse deleteElements (String path, String[] names, HttpSession sesion)	{
		DrExplorerResponse resp=new DrExplorerResponse();
		try	{
			File baseDir=getBasedir(sesion);
			File directorio=getSubdir(baseDir,path,sesion);
			
			for (int t=0; t<names.length; t++){
				String elemento=names[t];
				File fileABorrar=new File(directorio,elemento);
				if (!fileABorrar.canWrite()){
					ResourceBundle bundle=ResourcesService.getResourceBundle("drExplorer",sesion);
					throw new Exception ( bundle.getString("drExplorer.txt50") +elemento+"'");
				}
				//boolean error=deleteFile(fileABorrar); //controlar si no lo borra �?
				deleteFile(fileABorrar); //controlar si no lo borra �?
			}
			String relativePath=dameRutaRelativa(baseDir,directorio,sesion);
			resp.setRelativePath(relativePath);
			resp.setFiles( getFilesOf(directorio) );	
		} catch (Exception e){
			resp.setErrorDeOperacion(true);
			resp.setExcepcionError(e);
		}
		return resp;
		
	}
	
	/**
	 * Gets the directory.
	 * 
	 * @param path
	 *            the path
	 * @param sesion
	 *            the sesion
	 * 
	 * @return the directory
	 */
	public DrExplorerResponse getDirectory(String path, HttpSession sesion)	{
		DrExplorerResponse resp=new DrExplorerResponse();
		try	{
			File baseDir=getBasedir(sesion);
			File directorio=getSubdir(baseDir,path,sesion);
			String relativePath=dameRutaRelativa(baseDir,directorio,sesion);
			//Tomo el canonical path, verifico la seguridad y calculo el relative path
			resp.setRelativePath(relativePath);
			//Construyo la lista de ficheros y directorios
			resp.setFiles( getFilesOf(directorio) );
		} catch (Exception e){
			resp.setErrorDeOperacion(true);
			resp.setExcepcionError(e);
		}
		return resp;
	}	
	
	/**
	 * Dame ruta relativa.
	 * 
	 * @param baseDir
	 *            the base dir
	 * @param directorio
	 *            the directorio
	 * 
	 * @return the string
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private String dameRutaRelativa(File baseDir, File directorio, HttpSession sesion) throws Exception {
		String dirCanonicalPath=directorio.getCanonicalPath();
		String dirBaseCanonicalPath=baseDir.getCanonicalPath();
		if (dirCanonicalPath.indexOf(dirBaseCanonicalPath)!=0){
			ResourceBundle bundle=ResourcesService.getResourceBundle("drExplorer",sesion);
			throw new Exception(bundle.getString("drExplorer.txt51"));
		}
		String relativePath=dirCanonicalPath.substring(dirBaseCanonicalPath.length());
		return relativePath;
	}

	/**
	 * Gets the basedir.
	 * 
	 * @param sesion
	 *            the sesion
	 * 
	 * @return the basedir
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private File getBasedir(HttpSession sesion) throws Exception{
		if (sesion==null)	{
			throw new Exception(ResourcesService.getResourceBundle("drExplorer",sesion).getString("drExplorer.txt52"));//Session expired.
		}
		//Object basePathObj=sesion.getAttribute("fileSystemRoot");
		Object userConfigObj = sesion.getAttribute("userConfig");//TODO: unificar las llamadas a sesi�n en el loader 
		if (userConfigObj==null)
			throw new Exception(ResourcesService.getResourceBundle("drExplorer",sesion).getString("drExplorer.txt53"));//"User not in session."
		DesktopUserConfig userConfig = (DesktopUserConfig) sesion.getAttribute("userConfig"); 
		//if (basePathObj==null)
		if (userConfig.getFileSystemRoot()==null)
			throw new Exception(ResourcesService.getResourceBundle("drExplorer",sesion).getString("drExplorer.txt54"));//"No fileSystemRoot info for the user."
		//String basePath=(String)basePathObj;
		File baseDir=new File(userConfig.getFileSystemRoot());
		if (!baseDir.exists())	
			throw new Exception(ResourcesService.getResourceBundle("drExplorer",sesion).getString("drExplorer.txt55"));//"Base directory doesn't exist."
	   return baseDir;
		
	}
	
	/**
	 * Gets the subdir.
	 * 
	 * @param baseDir
	 *            the base dir
	 * @param path
	 *            the path
	 * 
	 * @return the subdir
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private File getSubdir(File baseDir, String path, HttpSession sesion) throws Exception{
		File directorio= new File(baseDir, path);
		ResourceBundle bundle=ResourcesService.getResourceBundle("drExplorer",sesion);
		if (!directorio.exists())
			throw new Exception( bundle.getString("drExplorer.txt49") +" '"+path+"' "+ bundle.getString("drExplorer.txt56"));//Directory doesn't exist.
		return directorio;
	}
	
	
	/**
	 * Gets the files of.
	 * 
	 * @param directorio
	 *            the directorio
	 * 
	 * @return the files of
	 */
	private ArrayList<DrFileInfo> getFilesOf(File directorio){
		ArrayList<DrFileInfo> resp=new ArrayList<DrFileInfo>();
		File[] listfiles=directorio.listFiles();
		for (int i=0; i<listfiles.length; i++){
			DrFileInfo fileInfo=new DrFileInfo();
			fileInfo.setName(listfiles[i].getName());
			fileInfo.setRelativePath(listfiles[i].getPath());
			fileInfo.setCanRead(listfiles[i].canRead());				
			fileInfo.setCanWrite(listfiles[i].canWrite());	
			fileInfo.setDirectory(listfiles[i].isDirectory());
			fileInfo.setHidden(listfiles[i].isHidden());
			fileInfo.setLastModified(listfiles[i].lastModified());
			fileInfo.setLength(listfiles[i].length());
			resp.add(fileInfo);
		}		
		return resp;
	}

	/*
    public static boolean deleteDir(File dir) {//Recursivo
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // The directory is now empty so delete it
        return dir.delete();
    }

    static public boolean deleteDirectory(File path) {
        if( path.exists() ) {
          File[] files = path.listFiles();
          for(int i=0; i<files.length; i++) {
             if(files[i].isDirectory()) {
               deleteDirectory(files[i]);
             }
             else {
               files[i].delete();
             }
          }
        }
        return( path.delete() );
      }
    */
	/**
	 * Delete file.
	 * 
	 * @param path
	 *            the path
	 * 
	 * @return true, if successful
	 */
	public static boolean deleteFile(File path) {
		if( path.exists() ) {
			if (path.isDirectory()) {
				File[] files = path.listFiles();
				for(int i=0; i<files.length; i++) {
					if(files[i].isDirectory()) {
						deleteFile(files[i]);
					} else {
						files[i].delete();
					}
				}
			}
		}
		return(path.delete());
	}


    
    
    
}
