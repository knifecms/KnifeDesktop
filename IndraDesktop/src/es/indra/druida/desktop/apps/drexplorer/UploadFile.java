package es.indra.druida.desktop.apps.drexplorer;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import java.util.ResourceBundle;
import es.indra.druida.desktop.configurator.ResourcesService;
/**
 * The Class UploadFile.
 */
public class UploadFile {
	
	/** The ruta destino. */
	private String rutaDestino;
	
	/** The sesion. */
	private HttpSession sesion;
	
	/** The explorer service. */
	private DrExplorerService explorerService;	

	/**
	 * Procesa upload fichero.
	 * 
	 * @param request
	 *            the request
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public void procesaUploadFichero(HttpServletRequest request) throws Exception	{ //Graba el fichero
		//Check that we have a file upload request
		if (!ServletFileUpload.isMultipartContent(request)){
			throw new Exception ( ResourcesService.getResourceBundle("core",request).getString("drUploadDapWizard.txt8") ); //"Error t�cnico: no se ha recibido un paquete de datos con el tipo adecuado (multipart/form-data)."
		}
		//----------		
		//Tomo las variables de configuraci�n
		//String tmpDir=DesktopConfig.getInstance().get("tmpDir");
		//----------		
		//Create a factory for disk-based file items
		DiskFileItemFactory factory = new DiskFileItemFactory();
		//Set factory constraints
		//factory.setSizeThreshold(yourMaxMemorySize);
		//factory.setRepository(new File(tmpDir));
		//Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		//Set overall request size constraint
		//upload.setSizeMax(yourMaxRequestSize);
		//Parse the request
		List items = upload.parseRequest(request);
		//----------
		//Process the uploaded items
		Iterator iter = items.iterator();
		while (iter.hasNext()) {
		    FileItem item = (FileItem) iter.next();
		    if (item.isFormField()) {
		        processFormField(item);
		    } 
		}
		//Verifico que tengo los datos de la ruta
		if (getRutaDestino()==null){
			throw new Exception (ResourcesService.getResourceBundle("core",request).getString("drUploadDapWizard.txt9")); // "Error t�cnico: no se ha recibido el par�metro con la ruta de destino."
		}
		setSesion(request.getSession());
		setExplorerService(new DrExplorerService());

		iter = items.iterator();
		while (iter.hasNext()) {
		    FileItem item = (FileItem) iter.next();
		    if (!item.isFormField()) {
		    	processUploadedFile(item);
		    }
		}			
	}
	

	/**
	 * Process form field.
	 * 
	 * @param item
	 *            the item
	 */
	private void processFormField(FileItem item) {
		if (item.getFieldName().equalsIgnoreCase("rutaDestino"))
			this.setRutaDestino(item.getString());
	}		
	
	/**
	 * Process uploaded file.
	 * 
	 * @param item
	 *            the item
	 * 
	 * @throws Exception
	 *             the exception
	 */
	private void processUploadedFile(FileItem item) throws Exception { 
		//try	{
			explorerService.uploadFile(getRutaDestino(), item, getSesion());
		//} catch (Exception e){
		//	System.out.println("ERROR: "+e.getMessage());
		//	return false;
		//}
		//return true;
	}	
	
	
	
	// -------------------
	
	
	/**
	 * Gets the explorer service.
	 * 
	 * @return the explorer service
	 */
	public DrExplorerService getExplorerService() {
		return explorerService;
	}

	/**
	 * Sets the explorer service.
	 * 
	 * @param explorerService
	 *            the new explorer service
	 */
	public void setExplorerService(DrExplorerService explorerService) {
		this.explorerService = explorerService;
	}

	/**
	 * Gets the sesion.
	 * 
	 * @return the sesion
	 */
	public HttpSession getSesion() {
		return sesion;
	}

	/**
	 * Sets the sesion.
	 * 
	 * @param sesion
	 *            the new sesion
	 */
	public void setSesion(HttpSession sesion) {
		this.sesion = sesion;
	}
	
	/**
	 * Gets the ruta destino.
	 * 
	 * @return the ruta destino
	 */
	public String getRutaDestino() {
		return rutaDestino;
	}
	
	/**
	 * Sets the ruta destino.
	 * 
	 * @param rutaDestino
	 *            the new ruta destino
	 */
	public void setRutaDestino(String rutaDestino) {
		this.rutaDestino = rutaDestino;
	}	
}
