package es.indra.druida.desktop.apps.drexplorer;

import java.util.ArrayList;

/**
 * The Class DrExplorerResponse.
 */
public class DrExplorerResponse {
	
	/** The relative path. */
	String relativePath;
	
	/** The parent relative path. */
	String parentRelativePath;	
	
	/** The files. */
	ArrayList<DrFileInfo> files;
	//
	/** The error de operacion. */
	boolean errorDeOperacion=false;	
	
	/** The excepcion error. */
	Exception excepcionError;	

	
	
	
	
	/**
	 * Gets the files.
	 * 
	 * @return the files
	 */
	public ArrayList<DrFileInfo> getFiles() {
		return files;
	}
	
	/**
	 * Sets the files.
	 * 
	 * @param files
	 *            the new files
	 */
	public void setFiles(ArrayList<DrFileInfo> files) {
		this.files = files;
	}
	
	/**
	 * Gets the relative path.
	 * 
	 * @return the relative path
	 */
	public String getRelativePath() {
		return relativePath;
	}
	
	/**
	 * Sets the relative path.
	 * 
	 * @param relativePath
	 *            the new relative path
	 */
	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}
	
	/**
	 * Checks if is error de operacion.
	 * 
	 * @return true, if is error de operacion
	 */
	public boolean isErrorDeOperacion() {
		return errorDeOperacion;
	}
	
	/**
	 * Sets the error de operacion.
	 * 
	 * @param errorDeOperacion
	 *            the new error de operacion
	 */
	public void setErrorDeOperacion(boolean errorDeOperacion) {
		this.errorDeOperacion = errorDeOperacion;
	}
	
	/**
	 * Gets the excepcion error.
	 * 
	 * @return the excepcion error
	 */
	public Exception getExcepcionError() {
		return excepcionError;
	}
	
	/**
	 * Sets the excepcion error.
	 * 
	 * @param excepcionError
	 *            the new excepcion error
	 */
	public void setExcepcionError(Exception excepcionError) {
		this.excepcionError = excepcionError;
	}
}
