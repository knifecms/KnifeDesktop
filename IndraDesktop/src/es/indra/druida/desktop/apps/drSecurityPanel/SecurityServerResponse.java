package es.indra.druida.desktop.apps.drSecurityPanel;

import es.indra.druida.desktop.configurator.beans.SecurityConfiguration;
import es.indra.druida.desktop.remoting.ServerResponse;

public class SecurityServerResponse extends ServerResponse {
	
   private SecurityConfiguration securityConfiguration;
   private boolean hasPassword;

	public SecurityConfiguration getSecurityConfiguration() {
		return securityConfiguration;
	}
	
	public void setSecurityConfiguration(SecurityConfiguration securityConfiguration) {
		this.securityConfiguration = securityConfiguration;
	}

	public boolean isHasPassword() {
		return hasPassword;
	}

	public void setHasPassword(boolean hasPassword) {
		this.hasPassword = hasPassword;
	} 
	
}
