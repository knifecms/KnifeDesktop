package es.indra.druida.desktop.apps.drSecurityPanel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import es.indra.druida.desktop.configurator.DesktopConfig;
import es.indra.druida.desktop.configurator.DesktopUserConfig;
import es.indra.druida.desktop.configurator.ResourcesService;
import es.indra.druida.desktop.configurator.DataLoaders.AbstractLoader;
import es.indra.druida.desktop.configurator.DataLoaders.AbstractSecurityLoader;
import es.indra.druida.desktop.configurator.beans.SecurityConfiguration;
import es.indra.druida.desktop.configurator.beans.User;
import es.indra.druida.desktop.remoting.ServerResponse;


/**
 * The Class SecurityService.
 */
public class SecurityService {

	/**
	 * Gets the security object.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @return the security object
	 */
	public SecurityServerResponse getSecurityObject(HttpServletRequest request, HttpServletResponse response)	{
		SecurityServerResponse resp=new SecurityServerResponse();
		try	{
			AbstractLoader loader=DesktopConfig.getSingleton().getSecurityLoader(request, response);
			Object [] objetos=loader.getObjects();
			if (objetos!=null || objetos.length>0){
				resp.setSecurityConfiguration(((SecurityConfiguration[])objetos)[0]);
			}
		} catch (Exception e)	{
			resp.setErrorDeOperacion(true);
			resp.setExcepcionError(e);
		}
		return resp;	
	}
	
	
	private User getUserSecurityData(HttpServletRequest request, HttpServletResponse response) throws Exception {
		DesktopUserConfig userConfig = (DesktopUserConfig) request.getSession().getAttribute("userConfig"); 		
		AbstractLoader loader=DesktopConfig.getSingleton().getSecurityLoader(request, response);
		Object [] objetos=loader.getObjects();
		if (objetos!=null || objetos.length>0){
			SecurityConfiguration conf=((SecurityConfiguration[])objetos)[0];
			User[] usus = conf.getUsers();
			for (int i=0; i<usus.length; i++){
				if (usus[i].getId().equals(userConfig.getId())){						
					return usus[i];
				}
			}
		}	
		return null;
	}
	
	public SecurityServerResponse getUserHasPassword(HttpServletRequest request, HttpServletResponse response)	{
		SecurityServerResponse resp=new SecurityServerResponse();
		try	{
			User userData=getUserSecurityData(request, response);
			resp.setHasPassword(userData.isHasPassword());
		} catch (Exception e)	{
			resp.setErrorDeOperacion(true);
			resp.setExcepcionError(e);
		}
		return resp;	
	}	
	
	/**
	 * Sets the security object.
	 * 
	 * @param objSeg
	 *            the obj seg
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * 
	 * @return the server response
	 */
	public ServerResponse setSecurityObject(SecurityConfiguration securityConfiguration, HttpServletRequest request, HttpServletResponse response)	{
		ServerResponse resp=new ServerResponse();
		try	{
			boolean estoyEnSesion=request.getSession().getAttribute("userConfig")!=null;
			if (!estoyEnSesion)	
				throw new Exception (ResourcesService.getResourceBundle("core",request.getSession()).getString("security.txt1")); //"User not in session"
			DesktopUserConfig desktopUserConfig=(DesktopUserConfig)(request.getSession().getAttribute("userConfig"));
			String[] roles=desktopUserConfig.getRoles();
			if (!stringInArray("userAdmin",roles))
				throw new Exception (ResourcesService.getResourceBundle("core",request.getSession()).getString("security.txt2"));	//"User does not have user modification privileges."			
			AbstractLoader loader=DesktopConfig.getSingleton().getSecurityLoader(request, response);
			SecurityConfiguration[] args={securityConfiguration};
			loader.setObjects(args);
		} catch (Exception e)	{
			resp.setErrorDeOperacion(true);
			resp.setExcepcionError(e);
		}
		return resp;	
	}
	
	public ServerResponse setPassword(String oldPasswd, String newPasswd, HttpServletRequest request, HttpServletResponse response)	{
		ServerResponse resp=new ServerResponse();
		try	{
			boolean estoyEnSesion=request.getSession().getAttribute("userConfig")!=null;
			if (!estoyEnSesion)
				throw new Exception (ResourcesService.getResourceBundle("core",request.getSession()).getString("security.txt1"));
			DesktopUserConfig userConfig = (DesktopUserConfig) request.getSession().getAttribute("userConfig"); 
			AbstractSecurityLoader loader= DesktopConfig.getSingleton().getSecurityLoader(request, response);
			loader.setPassword(userConfig.getId(), oldPasswd, newPasswd);
		} catch (Exception e)	{
			resp.setErrorDeOperacion(true);
			resp.setExcepcionError(e);
		}
		return resp;	
	}	

	private boolean stringInArray(String string, String[] arr) {
		for (int i=0; i<arr.length; i++)
			if (arr[i].equals(string))
				return true;
		return false;
	}
	
}
