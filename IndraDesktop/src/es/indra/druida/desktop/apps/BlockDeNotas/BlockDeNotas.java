package es.indra.druida.desktop.apps.BlockDeNotas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ResourceBundle;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;

import es.indra.druida.desktop.configurator.ResourcesService;
import es.indra.druida.desktop.security.DesktopFileSecurity;
import es.indra.druida.desktop.utils.DesktopWriter;

/**
 * The Class BlockDeNotas.
 */
public class BlockDeNotas {


	/**
	 * Graba el fichero.
	 * 
	 * @param fichero
	 *            fichero
	 * @param texto
	 *            texto
	 * @param response
	 *            response
	 */
	public static void grabaFichero(String fichero, String texto, JspWriter response ) {
		
	}
	
	/**
	 * Genera html contenido fichero.
	 * 
	 * @param fichero
	 *            fichero
	 * @param sesion
	 *            sesion
	 * @param response
	 *            response
	 * 
	 * @throws Exception
	 *             exception
	 */
	public static void generaHtmlContenidoFichero(String fichero, HttpSession sesion, JspWriter response) throws Exception	{
		File ff=DesktopFileSecurity.getFile(fichero, sesion);//Esto ya lanza excepciones con los errores.
		if (!ff.exists() || !ff.canRead()){
			ResourceBundle bundle=ResourcesService.getResourceBundle("drNotePad",sesion);
			throw new Exception(bundle.getString("drNotepad.txt27") +ff+ bundle.getString("drNotepad.txt28"));
		}
		BufferedReader fr=new BufferedReader(new FileReader(ff));
		String s="";
		int linea=1;
		while ((s=fr.readLine())!=null)	{
			response.print(DesktopWriter.generaCodigoHtmlEscapadoParaTextArea(s)+"\n");
			linea++;
		}
	}


	

}
