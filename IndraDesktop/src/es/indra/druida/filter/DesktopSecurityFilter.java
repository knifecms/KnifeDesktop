package es.indra.druida.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import es.indra.druida.desktop.configurator.DesktopConfig;
import es.indra.druida.desktop.configurator.DesktopFilterData;

/**
 * The Class DesktopSecurityFilter.
 */
public class DesktopSecurityFilter implements Filter {
	

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		//System.out.println("doFilter");
		chain.doFilter(req, res);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public void init(FilterConfig filter) throws ServletException {
		// TODO Auto-generated method stub
		
		Logger.getLogger(DesktopSecurityFilter.class.getName()).info(
			  "\n*********************************"
			+ "\n*INDRADesktop WebOS             *"
			+ "\n*(C) INDRA SISTEMAS, S.A.       *"
			+ "\n*********************************"
			+ "\n  Configuration class: "+filter.getInitParameter("config_class")
			+ "\n  Application Loader: "+filter.getInitParameter("application_loader")
			+ "\n  Icon Loader: "+filter.getInitParameter("icon_loader")
			+ "\n  window Loader: "+filter.getInitParameter("window_loader")
			+ "\n  Skin Loader: "+filter.getInitParameter("skin_loader")
			+ "\n  Menu Loader: "+filter.getInitParameter("menu_loader")
			+ "\n  customSkinParameters loader: "+filter.getInitParameter("customSkinParameters_loader")
			+ "\n  config loader: "+filter.getInitParameter("configuration_loader")
			+ "\n  customConfig loader: "+filter.getInitParameter("customConfiguration_loader")
		);		
		
		DesktopFilterData desktopFilterData=new DesktopFilterData();
		desktopFilterData.setConfigClass(filter.getInitParameter("config_class"));
		desktopFilterData.setAppLoader(filter.getInitParameter("application_loader"));
		desktopFilterData.setIconLoader(filter.getInitParameter("icon_loader"));
		desktopFilterData.setWindowLoader(filter.getInitParameter("window_loader"));
		desktopFilterData.setSkinLoader(filter.getInitParameter("skin_loader"));
		desktopFilterData.setMenuLoader(filter.getInitParameter("menu_loader"));
		desktopFilterData.setAppGroupLoader(filter.getInitParameter("group_loader"));
		desktopFilterData.setCustomSkinParametersLoader(filter.getInitParameter("customSkinParameters_loader"));
		desktopFilterData.setConfigurationLoader(filter.getInitParameter("configuration_loader"));
		desktopFilterData.setCustomConfigurationLoader(filter.getInitParameter("customConfiguration_loader"));
		desktopFilterData.setSecurityLoader(filter.getInitParameter("security_loader"));
		
		try {
			DesktopConfig.getSingleton(desktopFilterData);
		} catch (Exception e) {
			StringWriter sWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(sWriter));
			Logger.getLogger(DesktopSecurityFilter.class.getName()).severe(sWriter.getBuffer().toString());
		}
	}

}
