package es.indra.druida.events;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * The listener interface for receiving scriptEvent events. The class that is
 * interested in processing a scriptEvent event implements this interface, and
 * the object created with that class is registered with a component using the
 * component's <code>addScriptEventListener<code> method. When
 * the scriptEvent event occurs, that object's appropriate
 * method is invoked.
 * 
 * @see ScriptEventEvent
 */
public class ScriptEventListener {
	
	/** The singleton. */
	static private ScriptEventListener singleton = new ScriptEventListener();	
	
	/** The xml cache. */
	Map xmlCache=new HashMap();

	/**
	 * Instantiates a new script event listener.
	 */
	private ScriptEventListener() {
	}
	
    /**
	 * Gets the single instance of ScriptEventListener.
	 * 
	 * @return single instance of ScriptEventListener
	 */
    static public ScriptEventListener getInstance() { 
        return singleton;
    }
    
    
    //Hashmap con el cont de los XML, dentro hashmap con los ID y por cada ID un arrayList de eventos
	/**
	 * Generate register script.
	 * 
	 * @param xmlFileName
	 *            the xml file name
	 * @param id
	 *            the id
	 * 
	 * @return the string
	 * 
	 * @throws ParserConfigurationException
	 *             the parser configuration exception
	 * @throws SAXException
	 *             the SAX exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
    public String generateRegisterScript(String xmlFileName, String id) throws ParserConfigurationException, SAXException, IOException	{ //Puede esto causar problemas de rendimiento? TO-DO
		StringBuffer ret=new StringBuffer();
		File xmlFile=new File(xmlFileName);
		Object oListeners=xmlCache.get(xmlFile.getAbsolutePath());
		Map losListeners=null;
		if (oListeners!=null){
			losListeners=(Map)oListeners;
		} 
		else { //Si no hab�a procesado el XML todav�a ...
			Document doc=es.indra.druida.util.XmlUtils.getDocumentFromFile(xmlFile);
			if (doc!=null){
				NodeList listeners = doc.getElementsByTagName("listener");
				losListeners=new HashMap();
				for ( int i = 0 ; i < listeners.getLength() ; i++) {
					Element listener=(Element)listeners.item(i);
					//NodeList events = listener.getElementsByTagName("event");
					losListeners.put(listener.getAttribute("id"),listener); //Guardo los nodelist
				}
				xmlCache.put(xmlFile.getAbsolutePath(),losListeners);
			}
		}
		Object oListener=losListeners.get(id);
		if (oListener!=null){
			Element listener=(Element)oListener;
			//System.out.println(listener.getAttribute("id")+" ECOO");
			NodeList events = listener.getElementsByTagName("event");
			for ( int i = 0 ; i < events.getLength() ; i++) {
				Element event=(Element)events.item(i);
				if (event.getAttribute("grouped")!=null && event.getAttribute("grouped")!="" && event.getAttribute("grouped")!="false")	{
					ret.append("registraReceptorEventosSoloFamilia("+generaCadenaJavascript(event.getAttribute("id"),true)+","+generaCadenaJavascript(event.getAttribute("script"),true)+",window);");
				}else{
					ret.append("registraReceptorEventos("+generaCadenaJavascript(event.getAttribute("id"),true)+","+generaCadenaJavascript(event.getAttribute("script"),true)+",window);");
					}
			}
			
			
		}
		return ret.toString();
	}
	
	/**
	 * Genera cadena javascript.
	 * 
	 * @param s
	 *            the s
	 * @param esComillaDoble
	 *            the es comilla doble
	 * 
	 * @return the string
	 */
	static public String generaCadenaJavascript(String s, boolean esComillaDoble){
	    String regExpSlash = "\\\\";     //En realidad pone "\\" que es la expresi�n regular de \
		s = s.replaceAll(regExpSlash, "\\\\\\\\");
		s = s.replaceAll("\n", "\\\\n");
		String c=((esComillaDoble)?"\"":"'");
		s = s.replaceAll(c, "\\\\"+c);
		return c + s + c;
	}
	
}


